({
	doInit : function(component, event, helper) {
		helper.updateFilters(component, true); 
	},

    doSecondaryFilterChange : function(component, event, helper){
        var _filterLabel = event.getSource().get("v.label");
        component.set("v.currentSelection", _filterLabel);

        helper.updateData(component);
        helper.updateFilters(component, false);

		var dataUpdateEvent = $A.get("e.c:Grid_DataUpdateEvent");
        dataUpdateEvent.fire();
    },

    doReset : function(component, event, helper){
        var _viewData = component.get("v.viewData");
        var _filters = _viewData.listOfSecondaryFilters;
         for(var i=0; i<_filters.length; i++){
         	_filters[i].stringValue = '';
         }
        _viewData.listOfSecondaryFilters = _filters;
        _viewData.searchKeyword = ''; // Added by MB - Bug 70808 - 3/4/19
        component.set("v.viewData", _viewData);
        helper.updateData(component);
        helper.updateFilters(component, false);
        component.set("v.currentSelection", null);

        var dataUpdateEvent = $A.get("e.c:Grid_DataUpdateEvent");
        dataUpdateEvent.fire();
    },

    doDone : function(component, event, helper){
		var modalEvent = $A.get("e.c:Grid_ModalEvent");
        modalEvent.fire();
    }
})