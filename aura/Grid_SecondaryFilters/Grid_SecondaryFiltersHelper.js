({
    updateFilters : function(component, init) {
        
        var _viewData = component.get("v.viewData");
        var _filters = _viewData.listOfSecondaryFilters;
        var _listOfData = _viewData.listOfData;
        var _currentSelection = component.get("v.currentSelection");
        var _selectedValues;
        _viewData.activeFilterCount = 0;
        //filter unqiue values form listOfData for only first filter
        console.log('### Grid_SecondaryFiltersHelper (updateFilters) _filters: '+ JSON.stringify(_filters));
        // Start of Code - MB - Bug 70808 - 2/28/19
        if(_viewData.searchKeyword !== undefined && _viewData.searchKeyword !== '' && _viewData.searchKeyword.length > 2){
            _viewData.activeFilterCount++;
        }
        // End of Code - MB - Bug 70808 - 2/28/19
        for(var y=0;y<_filters.length;y++){
            _filters[y].listOfSelectedValues = [];
            _selectedValues = [];
            if(_filters[y].stringValue != ''){
                _viewData.activeFilterCount++;
                if(_filters[y].stringValue.indexOf(';') > -1)
                    _selectedValues = _filters[y].stringValue.split(';');
                else
                    _selectedValues.push(_filters[y].stringValue);
                
                for(var i=0; i<_selectedValues.length; i++){
                    _filters[y].listOfSelectedValues.push({
                        "label" : ''+_selectedValues[i],
                        "value" : ''+_selectedValues[i],
                        "selected" : true
                    })
                }
            }
            if(_currentSelection == _filters[y].label && _filters[y].stringValue != '' && _filters[y].stringValue != null)
                continue;
            var firstFilterUniqueValues = [];
            var filterOptions = [];
            for(var x=0;x<_listOfData.length;x++){
                var fieldValue = this.getFieldValue(_listOfData[x],_filters[y].fieldName);
                /* Start of Code - MB - 8/3/18 - Bug 62716 */
                console.log('FieldName: ' + _filters[y].fieldName);
                if(_filters[y].fieldName === 'Product__r.Display__c' && !$A.util.isUndefinedOrNull(fieldValue)){
                    var valueList = fieldValue.split(';');
                    for(var i=0;i<valueList.length;i++){
                        if(!$A.util.isUndefinedOrNull(valueList[i]) && firstFilterUniqueValues.indexOf(valueList[i]) == -1){
                            firstFilterUniqueValues.push(valueList[i]);
                            filterOptions.push({
                                "label" : ''+valueList[i],
                                "value" : ''+valueList[i],
                                "selected" : _selectedValues.indexOf(valueList[i]) > -1
                            });
                        }
                    }
                }else{
                    /* End of Code - MB - 8/3/18 - Bug 62716 */
                    if(!$A.util.isUndefinedOrNull(fieldValue) && firstFilterUniqueValues.indexOf(fieldValue) == -1){
                        firstFilterUniqueValues.push(fieldValue);
                        filterOptions.push({
                            "label" : ''+fieldValue,
                            "value" : ''+fieldValue,
                            "selected" : _selectedValues.indexOf(fieldValue) > -1
                        });
                    }
                }
            }
            _filters[y].listOfPicklistValues = filterOptions;
            if(init)
                _filters[y].listOfPicklistValues_Server = filterOptions;
        }
        _viewData.listOfSecondaryFilters = _filters;
        component.set("v.viewData",_viewData);
    }
})