({
	doInit: function (component, event, helper) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.getSalesInfo");
        
       /*  component.set('v.columns', [
                {label: 'Territory Name', fieldName: 'Name', type: 'text'},
                {label: 'Plan Goal', fieldName: 'goal', type: 'currency'},
                {label: 'YTD Sales', fieldName: 'YTDSales', type: 'currency'},            
                {label: 'Open Orders', fieldName: 'OpenOrders', type: 'currency'},
                {label: 'Pipeline', fieldName: 'Pipeline', type: 'currency'},
                {label: 'Forecasted Revenue', fieldName: 'Revenue', type: 'currency'}              
          ]); */
        /*Start of Code - Added by MB - 06/05/18 - Bug 61384 */
        var columns = [];
        columns[0] = $A.get("$Label.c.Segment");
        columns[1] = $A.get("$Label.c.Plan_Goal");
        columns[2] = $A.get("$Label.c.YTD_Sales");
        columns[3] = $A.get("$Label.c.Open_Orders");
        columns[4] = $A.get("$Label.c.Pipeline");
        columns[5] = $A.get("$Label.c.Forecasted_Sales");
        component.set("v.columns", columns);
		console.log(columns);        
        /*End of Code - Added by MB - 06/05/18 - Bug 61384 */
        
        action.setParams({'recordId': recordId});
        console.log(recordId);
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(state);
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                console.log(result.isValid);              
                if (response.getReturnValue().length > 0){
                    component.set('v.UserValid', true);
                    component.set('v.SalesInfo', response.getReturnValue());                    
                }
                
               /* component.set('v.UserValid', result.isValid );
                component.set('v.Account', result.acc);
                //component.set('v.AccountMap', result); */
            }
            
             
           /* var acc = component.get('v.Account');
            if(acc.Total_Pipeline__c == ''){
                acc.Total_Pipeline__c = '0.00';
            }
            component.set('v.Account', acc);
            console.log(component.get('v.UserValid')); */ 
        });
        $A.enqueueAction(action);
	}
})