({
  doInit: function (component, event, helper) {
    helper.getMajorMetorsPickListvalues(component, event);
    helper.getStatePickListvalues(component, event);
    // helper.getTerrUserOwnerlist(component, event);
    // debugger
    var recordId = component.get('v.recordId');
    if (recordId) {
      if(recordId.substring(0, 3) !== '006') {
        helper.getTerritoryValue(component, event);
      } else {
        component.set('v.isCommercial', true);
        // helper.getTerrUserOwnerlist(component, event, helper);
      }

    } else {
      component.set('v.isCommercial', true);
      // helper.getTerrUserOwnerlist(component, event, helper);
    }
      console.log(component.get('v.isCommercial'));
    helper.getTerrUserOwnerlistWithTerritory(component, event);

  },
  search: function (component, event, helper) {
    component.set('v.hasInfo', true);
    component.set('v.hasMoreRecords', false); // Added by MB - 06/20/17
    var territoryValue = component.get('v.territoryValue');
    console.log('territoryValue before', territoryValue);
    if (territoryValue == undefined) {
      territoryValue = '';
    }
    console.log('territoryValue after', territoryValue);
    // var name = document.getElementById('name').value;
    var name = component.find('input-name').elements[0].value;
    var areaCode = component.find('area-code').elements[0].value;
    var state = component.find('state').elements[0].value;
    var metro = component.find('metro').elements[0].value;
    // var areaCode = document.getElementById('area-code').value;
    // var state = document.getElementById('state').value;
    // var metro = document.getElementById('metro').value;
    // var territory = document.getElementById('territory').value;
    // var territory = document.querySelector('input[name="radio-options"]:checked').value;
    var territoryList = component.find('territory');
    var territory = '';
    // for(var i = 0; i<territory.length; i++) {
    if(territoryList[0].elements[0].checked) {
      territory = 'All Users'
    } else if(territoryList[1].elements[0].checked){
      territory = 'All Users Within My Territory'
    }
    // }
    console.log(component.find('territory'), name, areaCode, metro, state, territory);
    var action = component.get("c.getTerrUserOwnerlist");
    action.setParams({'Name': name, 'Territory': territory, 'AreacodeValue': areaCode, 'StateValue': state, 'Majormetrosvalue': metro, 'Territoryvalue': territoryValue});
    console.log(action.getParams());
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        console.log(response.getReturnValue());
        // console.log(response.getReturnValue())
        if (response.getReturnValue().length == 0) {
          component.set('v.hasInfo', false);
        }
        component.set('v.accountList', response.getReturnValue());
		//Added by MB - 06/20/17
        if(response.getReturnValue().length === 100){
            component.set('v.hasMoreRecords', true);
        }
        // var resultTable = component.find('result-table');
        // $A.util.addClass(resultTable, 'slds-show');
        // $A.util.removeClass(resultTable, 'slds-hide');
      }
    });
    $A.enqueueAction(action);
  },
  fireSendUserIdEvt: function (component, event, helper) {
    // debugger
    var sendUserIdEvent = component.getEvent("SendUserIdEvt");
    //var sendUserIdEvent = $A.get("e.c:SendUserIdEvt");
    var username = event.currentTarget.dataset.name;
    var userId = event.currentTarget.dataset.id;
    //var territoryId = event.currentTarget.dataset.territory; - Commented by MB - 06/20/17
    var territoryId = ''; // Added by MB - 06/20/17
    console.log(territoryId)
    // var territoryId =  event.
    sendUserIdEvent.setParams({"userId": userId, "username": username, "territoryId": territoryId});
    sendUserIdEvent.fire();

    // var myExternalEvent;
    // if(window.opener.$A &&
    //   (myExternalEvent = window.opener.$A.get("e.c:SendUserIdEvt"))) {
    //   myExternalEvent.setParams({isOauthed:true});
    //   myExternalEvent.fire();
    // }
  },
  preventEnterEvent: function (component, event, helper) {
    if (event.keyCode == 13) {
      event.preventDefault();
      // this.search(component, event);
      helper.search(component, event);
    }
  },
})