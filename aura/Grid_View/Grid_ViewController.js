({
    doInit : function(component, event, helper) {
		//Start of Code MB - Bug 70808 - 2/20/19
        if(component.get('v.gridType') === 'Merchandizing_Vehicle_Price_List'){
            component.set('v.screenMessage','Select a Product Category');
            component.set('v.displaySearchBox', false);
        }
        //End of Code MB - Bug 70808 - 2/20/19
        console.log("### Grid_View doInit");
        //Start of Code - MB - 07/30/18 - Bug 63360
        var _spinner = component.find("chartSpinner");
        $A.util.removeClass(_spinner, "slds-hide");
        $A.util.addClass(_spinner, "slds-show");
        //End of Code - MB - 07/30/18 - Bug 63360
        helper.getProductCategories(component);
        helper.handlePageMessage(component,
                                 //'Select a Product Category',
                                 component.get('v.screenMessage'), //Added by MB - Bug 70808 - 2/20/19
                                 helper.DEFAULT_TIP);
        //Start of Code - MB - 07/30/18 - Bug 63360
        $A.util.removeClass(_spinner, "slds-show");
        $A.util.addClass(_spinner, "slds-hide");
        //End of Code - MB - 07/30/18 - Bug 63360
        console.log('isDesktop: ' + $A.get("$Browser.isDesktop"));

    },
    
    onCategorySelection : function(component, event, helper){
        var _prodCategoryId = component.get("v.viewData.productCategoryId"); 
        var _prodCatName = helper.getSelectedProductCategoryName(component); 
        
        component.set("v.productCategoryName", _prodCatName);
        
        var _viewData = component.get("v.viewData");
        console.log('### Grid_View - onCategorySelection - numberOfRecords: ' + _viewData.numberOfRecords);
        
        _viewData.listOfData = [];
        _viewData.listOfPrimaryFilters = [];
        _viewData.isMobile = ($A.get("$Browser.formFactor") == 'PHONE');
        console.log('### Grid_View - onCategorySelection - isMobile: ' + _viewData.isMobile);
        if(_prodCategoryId && _prodCategoryId != null && _prodCategoryId != ''){ 
            if(!_viewData.isMobile && (_viewData.globalSearchText === undefined || _viewData.globalSearchText === '')){//Added condition globalSearchText - MB - Bug 70808 - 2/20/19
                component.set('v.disableSearchBox', true); //Added by MB - Bug 70808 - 2/21/19
            }
            helper.handlePageMessage(component,'', helper.DEFAULT_TIP);
            helper.getFieldsAndFilters(component, _prodCategoryId, _viewData.sObjectName);
        }else if(_prodCategoryId && _prodCategoryId != null && _prodCategoryId != '' && _viewData.globalSearchText &&
           _viewData.globalSearchText !== undefined && _viewData.globalSearchText !== ''){ //Added by MB - Bug 70808 - 2/20/19
            //helper.fetchDataForAllCategories(component, event, helper);
        }else{
            helper.handlePageMessage(component,
                                     //'Select a Product Category',
                                     component.get('v.screenMessage'), //Added by MB - Bug 70808 - 2/20/19
                                     helper.DEFAULT_TIP);
            helper.clearData(component);
        }
        component.set("v.viewData", _viewData);
    },
    
    onPrimaryFilterChange : function(component, event, helper){
        //Start of Code - MB - Bug 71461 - 3/11/19
        var _viewData = component.get("v.viewData");
        if(_viewData !== undefined){
            _viewData.searchKeyword = '';
            _viewData.activeFilterCount = '';
            component.set('v.count', 0);
        }
        component.set("v.viewData", _viewData);
        //End of Code - MB - Bug 71461 - 3/11/19
        helper.checkFiltersAndGetData(component);
    },
    
    doSearch : function(component, event, helper){
        var _viewData = component.get("v.viewData");
        console.log('### Grid_View - doSearch - numberOfRecords: ' + _viewData.numberOfRecords);
        //if(_viewData.searchKeyword && _viewData.searchKeyword.length > 2){
        if(_viewData.globalSearchText && _viewData.globalSearchText.length > 2){ // Added by MB - Bug 70808 - 2/22/19
            helper.getViewData(component);
        }else {
            _viewData.listOfData = [];
            helper.handlePageMessage(component, 'Please enter 3 or more characters', 'Try something more specific.');
            component.set("v.viewData", _viewData);
        }
    },
    
    onFilterKeywordChange : function(component, event, helper){
        var dataTable = component.find("dataTable");
        if(dataTable !== undefined){
            component.find("dataTable").updateView();
        }
    },
    //Start of Code MB - Bug 70808 - 2/20/19
    onGlobalSearchTextChange : function(component, event, helper){
        var _viewData = component.get("v.viewData");
        console.log('keyCode: ' + event.keyCode);
        console.log('_viewData.globalSearchText: ' + _viewData.globalSearchText);
        console.log('gridType: ' + component.get('v.gridType'));
        
        if((event.keyCode === 13 ) && _viewData.globalSearchText && _viewData.globalSearchText.length > 2){
            component.set('v.displaySpinner', true);
            helper.fetchDataForAllCategories(component, event, helper);
        }else if(_viewData.globalSearchText || event.keyCode === 13 && (_viewData.globalSearchText === undefined || _viewData.globalSearchText && 
                                          (_viewData.globalSearchText.length === 0 || (_viewData.globalSearchText.length > 0 && _viewData.globalSearchText.length < 3)))){
            _viewData.cplIds = [];
            _viewData.listOfData = [];
            _viewData.listOfPrimaryFilters = [];
            _viewData.productCategoryId = '';
            _viewData.productCategory = '';
            helper.handlePageMessage(component, 'Please enter 3 or more characters', 'Try something more specific. Press Enter key after finished typing.');
            component.set("v.viewData", _viewData);
        }else if(_viewData.globalSearchText !== undefined && _viewData.globalSearchText === ''){
            _viewData.cplIds = [];
            _viewData.listOfData = [];
            _viewData.listOfPrimaryFilters = [];
            _viewData.globalSearchText = '';
        	_viewData.productCategoryId = '';
            _viewData.productCategory = '';
            component.set('v.multipleProductCategories', false);
            component.set('v.disableSearchBox', false);
            component.set('v.count', 0);
            helper.getProductCategories(component);
            helper.handlePageMessage(component,
                                     component.get('v.screenMessage'),
                                     helper.DEFAULT_TIP);
        }
        component.set("v.viewData", _viewData);
    },
    
    doReset : function(component, event, helper){
        var _viewData = component.get("v.viewData");
        if(_viewData !== undefined){
            if(_viewData.cplIdList !== undefined){_viewData.cplIdList = [];}
            _viewData.listOfData = [];
            _viewData.listOfPrimaryFilters = [];
            _viewData.globalSearchText = '';
            _viewData.searchKeyword = '';
            _viewData.productCategoryId = '';
            _viewData.productCategory = '';
            _viewData.productCategoryZonesMap = []; //Added by MB - Bug 71631 - 3/21/19
            //_viewData.productCategoryIdMap = '';
            _viewData.activeFilterCount = '';
            component.set("v.viewData", _viewData);
        }
            component.set('v.multipleProductCategories', false);
            component.set('v.count', 0);
            component.set('v.disableFilterBtn',true);
            component.set('v.disableSearchBox', false);
        helper.getProductCategories(component);
        helper.handlePageMessage(component,
                                 component.get('v.screenMessage'),
                                 helper.DEFAULT_TIP);
        
    },
    
    doMobileSearch: function(component, event, helper){
        var _viewData = component.get("v.viewData");
        console.log('### Grid_View - doSearch - numberOfRecords: ' + _viewData.numberOfRecords);
        if(_viewData.globalSearchText && _viewData.globalSearchText.length > 2){
            helper.fetchDataForAllCategories(component, event, helper);
        }else {
            _viewData.listOfData = [];
            helper.handlePageMessage(component, 'Please enter 3 or more characters', 'Try something more specific.');
            component.set("v.viewData", _viewData);
        }
    },
    //End of Code MB - Bug 70808 - 2/20/19
    checkFilterLength : function(component, event, helper){
        var searchBox = component.find("searchDesktop");
        var searchVal = searchBox.get("v.value");
        var validityVal = (searchVal == null || searchVal.length == 0 || searchVal.length > 2) ? {valid:true} : {valid:false, badInput :true};
        searchBox.focus();
        searchBox.set("v.validity",validityVal);
        searchBox.showHelpMessageIfInvalid();
        searchBox = component.find("searchDesktop");
        console.log(validityVal);
        console.log(searchBox.get("v.validity").valid);
    },
    
    doSecondaryFilterDisplay: function(component, event, helper){
        var modalBody;
        $A.createComponent("c:Grid_SecondaryFilters", {
            "viewData" : component.getReference("v.viewData")
        },function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                var _gridModalPromise = component.find('secondaryFilterOverlay').showCustomModal({
                    header: "Filters",
                    body: modalBody,
                    showCloseButton: true,
                    cssClass: "secondary-filter-modal"
                });
                component.set("v.gridModalPromise", _gridModalPromise);
            }
        });
    },
    handleDataUpdateEvent: function(component, event, helper){
        component.set("v.timeStamp", Date.now());
        console.log('### inside handleDataUpdateEvent');
    },
    handleModalEvent: function(component, event, helper){
        component.get('v.gridModalPromise').then(
            function (modal) {
                modal.close();
            }
        );
        console.log('### closing the secondary filter window');
		//component.set("v.timeStamp", Date.now());        
    },
    backToRecord: function(component, event, helper){
        
        var gridType = component.get("v.gridType");
        console.log('### gridType: ' + gridType);
        
        if (gridType == 'Merchandizing_Vehicle_Price_List') {
            window.history.back();
        }
        else {
            //Force the navigation back to the account record instead of the previous window.history
            var _accountId = component.get('v.accountId');
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": _accountId
            });
            navEvt.fire();
        }

    },
    openAccountReport: function(component, event, helper){
        var _accountId = component.get('v.accountId');
        var _prodCatName = encodeURI(helper.getSelectedProductCategoryName(component));
        var _reportUrl = '/c/ProductPriceRecordsByAccount.app?accountId='+_accountId+'&category='+_prodCatName;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": _reportUrl
        });
        urlEvent.fire();
    },
    
    openAccountReportComponent : function(component, event, helper) {
        
        var _accountId = component.get('v.accountId');
        var _prodCatName = helper.getSelectedProductCategoryName(component);//encodeURI(helper.getSelectedProductCategoryName(component));
        console.log('### _accountId: ' + _accountId);
        console.log('### _prodCatName: ' + _prodCatName);
        
        console.log('### before opening chart');
        helper.openLEModal(component,helper,"c:Product_Price_Records_by_Account",{
            accountId : _accountId,
            category : _prodCatName,
            timeStamp : Date.now()
        });
    }
})