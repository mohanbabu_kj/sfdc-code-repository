({
	checkFiltersAndGetData : function(component) {
		var _viewData = component.get("v.viewData");
		console.log('### Grid_View (helper) - checkFiltersAndGetData - numberOfRecords: ' + _viewData.numberOfRecords);
        _viewData.hasUnselectedPrimaryFilters = false;
		_viewData.listOfData = [];
		var _unselectedFieldNames = [];
        console.log('### Grid_View (helper) - checkFiltersAndGetData - primaryFilters: ' + JSON.stringify(_viewData.listOfPrimaryFilters));
		for(var i=0; i < _viewData.listOfPrimaryFilters.length; i++){
            /* Start of Code - MB - Bug 69642 - 1/24/19 */
            if((_viewData.listOfPrimaryFilters[i].label.indexOf('Price Zone') > -1 || _viewData.listOfPrimaryFilters[i].label.indexOf('District') > -1) && 
               (_viewData.grid === 'Price Grid' || _viewData.grid === 'Buying Price Grid') && 
               (_viewData.listOfPrimaryFilters[i].stringValue === '' || _viewData.listOfPrimaryFilters[i].stringValue === undefined)){
                var pricezoneValues = _viewData.listOfPrimaryFilters[i].listOfFilterOptions;
                console.log('List of prizezoneValues.length: ' + JSON.stringify(pricezoneValues));
                if(pricezoneValues !== undefined && pricezoneValues.length > 0){
                    _viewData.listOfPrimaryFilters[i].stringValue = pricezoneValues[0].value;
                }
            }
            /* End of Code - MB - Bug 69642 - 1/24/19 */
			if(_viewData.listOfPrimaryFilters[i].hasFilterOptions
				&& (_viewData.listOfPrimaryFilters[i].stringValue == null
					|| _viewData.listOfPrimaryFilters[i].stringValue == '')){

				_unselectedFieldNames.push(_viewData.listOfPrimaryFilters[i].label);
			}
			//checking for warehouse value
			if(_viewData.listOfPrimaryFilters[i].label.indexOf('Warehouse') > -1){
				_viewData.warehouseValue = _viewData.listOfPrimaryFilters[i].stringValue;
			}
		}
		_viewData.hasUnselectedPrimaryFilters = _unselectedFieldNames.length > 0;
		var _msg = 'Select a ';
		for(var i=0; i<_unselectedFieldNames.length; i++){
			_msg += _unselectedFieldNames[i]
			if(i < _unselectedFieldNames.length - 2){
				_msg += ', ';
			}else if(i == _unselectedFieldNames.length - 2){
				_msg += ' and ';
			}
		}
        console.log('### Grid_View (helper) - checkFiltersAndGetData -  _unselectedFieldNames.length: ' + _unselectedFieldNames.length);
        if(_unselectedFieldNames.length > 0){
			this.handlePageMessage(component, _msg, this.DEFAULT_TIP);
        	this.clearData(component);
        }
		else {
			this.handlePageMessage(component, '', null);
		}
		component.set("v.viewData", _viewData);
        
		if(_viewData.hasUnselectedPrimaryFilters == false){
			if(_viewData.isMobile == false || (_viewData.isMobile == true && _viewData.globalSearchText && _viewData.globalSearchText !== undefined && _viewData.globalSearchText !== '')){
				this.getViewData(component);
			}else if(_viewData.globalSearchText === undefined || _viewData.globalSearchText === ''){//Added condition for Global Search Text - MB - Bug 70808 - 2/22/19
				this.handlePageMessage(component, 'Please enter 3 or more characters', 'Try something more specific.');
				component.set("v.viewData", _viewData);
			}
		}

	},
    
    //Start of Code MB - Bug 70808 - 2/20/19
    fetchDataForAllCategories : function(component, event, helper){
        var _viewData = component.get("v.viewData");
        var start = performance.now();
        var elapsed = performance.now() - start;
		console.log('### fetchCPLDataUsingSOSL - elapsed time: ' + elapsed);
        console.log('### fetchCPLDataUsingSOSL - prodCategory: ' + component.get("v.gridType"));
        console.log('### fetchCPLDataUsingSOSL - gridType: ' + _viewData.gridType);
        console.log('### fetchCPLDataUsingSOSL - accountId: ' + component.get('v.accountId'));
        this.doCallout(component, "c.fetchCPLDataUsingSOSL", {
            "viewDataJSONString": JSON.stringify(_viewData),
            "accountId" : component.get('v.accountId'),
            "productId" : component.get('v.productId'),
            "grid" : component.get('v.gridType')
        }, function(response) {
		elapsed = performance.now() - start;
		console.log('### fetchCPLDataUsingSOSL - elapsed time: ' + elapsed);
            var _status = response.getState();
            if (_status === 'SUCCESS') {
                var _resp = response.getReturnValue();
                var _viewData = component.get("v.viewData");
                console.log('### fetchCPLDataUsingSOSL - resp: ' + JSON.stringify(_resp.productCategoryIdMap));
                if (_resp.productCategoryIdMap === undefined || _resp.productCategoryIdMap === null || 
                    _resp.productCategoryIdMap.length === 0) {
                    component.set('v.productCategories', _resp.productCategories);
                    this.handlePageMessage(component, this.NO_RESULTS, this.NO_RESULTS_TIP);
                } else {
                    console.log('filtered Product Categories: ' + JSON.stringify(_resp.productCategories));
                    _viewData.productCategoryIdMap = _resp.productCategoryIdMap;
                    _viewData.hasMessage = false;
                    console.log('globalSearchText: ' + _viewData.globalSearchText);
                    _viewData.searchKeyword = _viewData.globalSearchText;
                    if(_resp.productCategories && _resp.productCategories.length > 0){
                        component.set('v.disableSearchBox', true);
                        component.set('v.productCategories', _resp.productCategories);
                        _viewData.productCategoryZonesMap = _resp.productCategoryZonesMap; //Added by MB - Bug 71631 - 3/20/19
                        if(_resp.productCategoryId && _resp.productCategoryId !== undefined){
                            console.log('productCategoryId: ' + JSON.stringify(_resp.productCategoryId));
                            _viewData.productCategoryId = _resp.productCategoryId;
                            component.set('v.multipleProductCategories', false);
                            component.set('v.count', 1);
                        }else{
                            component.set('v.multipleProductCategories', true);
                            component.set('v.count', _resp.productCategories.length);
                            helper.handlePageMessage(component, 'Please select a Product Category', 'Result contains multiple Product Categories');
                        }
                    }
                    component.set("v.viewData", _viewData);
                    if(_resp.productCategoryId && _resp.productCategoryId !== undefined){
                        // Start of code - MB - Bug 71461 - Set Product Category Name for default Product Category - 3/11/19
                        var _prodCatName = helper.getSelectedProductCategoryName(component); 
        				component.set("v.productCategoryName", _prodCatName);
                        // Start of code - MB - Bug 71461 - 3/11/19
                        this.getFieldsAndFilters( component, component.get("v.viewData.productCategoryId"), component.get("v.viewData.sObjectName") );
                    }
                }
                component.set('v.displaySpinner', false);
            } else {
                this.handlePageMessage(component, this.NO_RESULTS, this.NO_RESULTS_TIP);
                component.set('v.displaySpinner', false);
                component.set("v.viewData", _viewData);
            }
            
            /*component.set("v.timeStamp", Date.now());
            
            //Hide the spinner after completing the data fetch
            var spinner = component.find("chartSpinner");
            if (spinner != undefined)
                $A.util.toggleClass(spinner, "slds-hide");
            
            elapsed = performance.now() - start;
            console.log('### getViewData - elapsed time: ' + elapsed);  */              
            
        });
    },
    //End of Code MB - Bug 70808 - 2/20/19
})