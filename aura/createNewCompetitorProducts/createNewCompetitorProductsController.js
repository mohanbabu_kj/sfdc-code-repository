({
    closeModal : function(component, event, helper) {
        //Clearing Data
        var componentFields = component.get("v.CompetitorProduct");
        componentFields.Name = "";
        componentFields.Product_Description__c = "";
        componentFields.Construction__c = "";
        componentFields.Style_Number__c = "";
        componentFields.Brand_Description__c = "";
        componentFields.Fiber_Type__c = "";
        componentFields.Fiber_Description__c = "";
        componentFields.Face_Weight__c= "";
        componentFields.Roll_Price__c = null;
        componentFields.Cut_Price__c = null;
        componentFields.Sales_Velocity__c = "";
        component.set('v.CompetitorProduct', componentFields);
        
        component.set("v.ErrorMsg","");
        component.set("v.selectedAccount",[]);
        component.set("v.selectedCompetitorProduct",[]);
        component.set("v.toggleModal",false);
        
        var device = $A.get("$Browser.formFactor");
        if( device != 'DESKTOP' ){
            var mobile = component.find('mobileDiv');
            $A.util.removeClass(mobile, 'slds-hide');
            $A.util.addClass(mobile, 'slds-show');
            
            var cmpEventFire = component.getEvent("newlyCreatedCompetitorProduct");
            cmpEventFire.setParams({
                "isClose" : true
            });
            cmpEventFire.fire();
        }
        
    },
    doSave : function(component, event, helper) {
        component.set("v.toggleSpinner",true);
        var CompetitorProduct = component.get("v.CompetitorProduct");
        console.log( CompetitorProduct.Competitor__c );
        console.log( CompetitorProduct.Competitor_Product__c );
        if( component.get("v.isDesktop") && ( $A.util.isEmpty( CompetitorProduct.Competitor__c ) || $A.util.isUndefined( CompetitorProduct.Competitor__c ) )){
            component.set("v.ErrorMsg","These required fields must be completed: Competitor");
            component.set("v.toggleSpinner",false);
            return;
        }
        
        if( !component.get("v.isDesktop") && ( $A.util.isEmpty( component.get("v.selectedAccount.val") ) || $A.util.isUndefined( component.get("v.selectedAccount.val") )  ) ){
            component.set("v.ErrorMsg","These required fields must be completed: Competitor");
            component.set("v.toggleSpinner",false);
            return;
        }
        /*
        if( component.get("v.isDesktop") && ( $A.util.isEmpty( CompetitorProduct.Competitor_Product__c ) || $A.util.isUndefined( CompetitorProduct.Competitor_Product__c ) )  ){
            component.set("v.ErrorMsg","These required fields must be completed: Product");
            component.set("v.toggleSpinner",false);
            return;
        }
        
        if( !component.get("v.isDesktop") && ( $A.util.isEmpty( component.get("v.selectedCompetitorProduct.val") ) || $A.util.isUndefined( component.get("v.selectedCompetitorProduct.val") ) ) ){
            component.set("v.ErrorMsg","These required fields must be completed: Product");
            component.set("v.toggleSpinner",false);
            return;
        }
        */
        if( $A.util.isEmpty( CompetitorProduct.Name ) || $A.util.isUndefined( CompetitorProduct.Name ) ){
            component.set("v.ErrorMsg","These required fields must be completed: Style Name");
            component.set("v.toggleSpinner",false);
            return;
        }
        if( $A.util.isEmpty( CompetitorProduct.Style_Number__c ) || $A.util.isUndefined( CompetitorProduct.Style_Number__c ) ){
            component.set("v.ErrorMsg","These required fields must be completed: Style Number");
            component.set("v.toggleSpinner",false);
            return;
        }
        //if( $A.util.isEmpty( CompetitorProduct.Name ) || $A.util.isUndefined( CompetitorProduct.Name ) ){
        //component.set("v.ErrorMsg","These required fields must be completed: Style Name");
        //component.set("v.toggleSpinner",false);
        // }else{
        if( !component.get("v.isDesktop") ){
            //component.set("v.CompetitorProduct.Competitor__c",component.get("v.selectedAccount.Id"));
            //component.set("v.CompetitorProduct.Competitor_Product__c",component.get("v.selectedCompetitorProduct.Id"));    
            component.set("v.CompetitorProduct.Competitor__c",component.get("v.selectedAccount.val"));
            component.set("v.CompetitorProduct.Competitor_Product__c",component.get("v.selectedCompetitorProduct.val"));
        }
        
        //if( !$A.util.isEmpty( CompetitorProduct.Name ) && !$A.util.isUndefined( CompetitorProduct.Name ) && !$A.util.isEmpty( CompetitorProduct.Style_Number__c ) || !$A.util.isUndefined( CompetitorProduct.Style_Number__c ) ){
        
        var action = component.get("c.createNewCompetitorProduct");
        action.setParams({
            'CP' : JSON.stringify( component.get("v.CompetitorProduct") ),
            "recordTypeName" : component.get("v.recordTypeName")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                if( response.status == 'SUCCESS' ){
                    var cmpEventFire = component.getEvent("newlyCreatedCompetitorProduct");
                    cmpEventFire.setParams({
                        "newCompetitorProduct" : {
                            //"Id":response.getReturnValue(),
                            "Id":response.value,
                            //"Name":CompetitorProduct.Name
                            "Name":CompetitorProduct.Name+' - '+CompetitorProduct.Style_Number__c
                        },
                        "isClose" : true
                    });
                    component.set("v.toggleModal",false);
                    component.set("v.toggleSpinner",false);
                    //Empty Complete Form
                    // component.set("v.CompetitorProduct","");
                    component.set("v.selectedAccount","");
                    component.set("v.selectedCompetitorProduct","");
                    component.set("v.ErrorMsg","");
                    CompetitorProduct.Name = "";
                    CompetitorProduct.Product_Description__c = "";
                    CompetitorProduct.Construction__c = "";
                    CompetitorProduct.Style_Number__c = "";
                    CompetitorProduct.Brand_Description__c = "";
                    CompetitorProduct.Fiber_Type__c = "";
                    CompetitorProduct.Fiber_Description__c = "";
                    CompetitorProduct.Face_Weight__c= "";
                    CompetitorProduct.Roll_Price__c = null;
                    CompetitorProduct.Cut_Price__c = null;
                    CompetitorProduct.Sales_Velocity__c = "";
                    
                    CompetitorProduct.Category__c = "";
                    CompetitorProduct.Fiber__c = "";
                    CompetitorProduct.Texture__c = "";
                    CompetitorProduct.Plank_Design__c = "";
                    CompetitorProduct.Appearance__c = "";
                    CompetitorProduct.Gauge__c = "";
                    CompetitorProduct.Thickness__c = "";
                    CompetitorProduct.Density__c = "";
                    CompetitorProduct.Moisture_Barrier__c = "";
                    CompetitorProduct.Antimicrobial__c = "";
                    CompetitorProduct.Width__c = "";
                    CompetitorProduct.Species__c = "";
                    CompetitorProduct.Application__c = "";
                    CompetitorProduct.Size__c = "";
                    CompetitorProduct.Wear_Layer__c = "";
                    component.set('v.CompetitorProduct', CompetitorProduct);
                    cmpEventFire.fire();
                }else{
                    component.set("v.ErrorMsg",response.value);
                    component.set("v.toggleSpinner",false);
                }
            }                
        });
        $A.enqueueAction(action);
        
        //}
    },
    handleGenericFormData : function( component, event, helper ){
        console.log( 'HANDLING CHANGE' );
        var selectedCP = event.getParam("values");
        var CompetitorProductNew = component.get("v.CompetitorProduct");
        console.log( selectedCP );
        for (var i = 0; i < selectedCP.length; i++) {
            CompetitorProductNew[selectedCP[i].fieldname] = selectedCP[i].value;
            console.log( CompetitorProductNew[selectedCP[i].fieldname] );
        }
        
        component.set("v.CompetitorProduct",CompetitorProductNew);
    },
})