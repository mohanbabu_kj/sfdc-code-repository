({
	getMAT : function(component, event, helper) {
         var action = component.get("c.getMAT");
         action.setParams({ 
            "accountProfileId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state ==='SUCCESS'){
                component.set("v.matList",response.getReturnValue()); 
            }
        });
        $A.enqueueAction(action);
	}
})