({
    doInit : function(component, event, helper) {        
        helper.getMAT(component, event, helper);
    },
    openPage : function(component, event, helper) {
        var device = $A.get("$Browser.formFactor");
        var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:mohawkAccountTeamCustomDetail",
                componentAttributes: {
                    recordId :event.currentTarget.dataset.id
                }
            });
            evt.fire();
    },
})