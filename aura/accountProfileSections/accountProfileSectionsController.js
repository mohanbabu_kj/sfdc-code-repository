({
	onInit : function(component, event, helper) {
        var randomNumber = Math.floor(1000 + Math.random() * 9000);
        component.set('v.idNumber', randomNumber);
        component.set("v.openMenu",component.get("v.isActive"));
        var alreadyOpen = component.get("v.openMenu");
        component.set("v.spinnerSection",true);
        if( alreadyOpen ){
            helper.onSectionOpenHelper( component, event, helper );    
        }
        helper.deviceOrentationHandler( component, event, helper );
	},
    handleClickSection : function(component, event, helper) {        
        var toggleSection = component.get("v.openMenu");        
        if( toggleSection ){
            component.set("v.openMenu",false);
        }else{
        	component.set("v.openMenu",true);  
            component.set("v.spinnerSection",true);
            helper.onSectionOpenHelper( component, event, helper );    
        }
	},
    openPage : function(component, event, helper) {
        /********* NAV SERVICE *********/
        var targetName = event.currentTarget.dataset.href;
        // var obj = targetId.substring(0,3);
        if( targetName === 'Primary Business' ){
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    recordId:event.currentTarget.dataset.id,
                    objectApiName: 'Account_Profile_Settings__c',
                    actionName: 'view'
                }
            };
            event.preventDefault();
            navService.navigate( pageReference );    
        }else{
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__component',
                attributes: {
                    //componentName:"c:accountProfileCustomDetail"
                    componentName:"c:accountProfileCustomDetailPage"
                },
                state:{
                    //"recordId":targetId,
                    "apRecordId":event.currentTarget.dataset.id,
                    "showOriginalAP":true
                }
            };
            event.preventDefault();
            navService.navigate( pageReference);
        }
        
        
        /********* NAV SERVICE *********/
        
        /*********** PREVIOUS CODE ***********
        var targetId = event.currentTarget.dataset.href;
        console.log('targetId', targetId);
        var device = $A.get("$Browser.formFactor");
        if( targetId !='' && targetId !=undefined && targetId != 'null' && device == 'DESKTOP'  ){            
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": targetId,
                'slideDevName': 'detail'
            });
            console.log('navEvt', navEvt);            
            navEvt.fire();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            //alert( targetId );
            //sforce.one.navigateToSObject(targetId, "detail");
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": targetId,
                'slideDevName': 'detail'
            });
            console.log('navEvt', navEvt);            
            navEvt.fire();
            //window.location.href='/' + targetId;
            //var test = 'https://'+ window.location.hostname+'/lightning/r/Account_Profile_Settings__c/'+targetId+'/view';
            //alert( test );
            //window.location.href='https://'+ window.location.hostname+'/lightning/r/Account_Profile_Settings__c/'+targetId+'/view';
        }
        else {
            alert('No Record Id found. Unable to load the target page');
        }
        ***********/
    },
    handleNavigation : function(component, event, helper) {
        helper.navigationHandler( component, event, helper );
    },
})