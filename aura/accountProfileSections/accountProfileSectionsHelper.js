({
    onSectionOpenHelper : function(component, event, helper) {
        if( component.get("v.displayType") != 'Block'){
        	this.rollupOnLoadHelper( component, event, helper );                
        }        
        var action = component.get("c.createBlockSectionsContent");
        action.setParams(
            {
                'fields' : component.get('v.blockSectionContent'),
                'recordId' : component.get('v.recordId')
                
            }
        );
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                console.log( result );
                component.set("v.dataToShow",this.sortData( component,true,'fieldSequence',result ));
                component.set("v.spinnerSection",false);
            }            
        });
        
        $A.enqueueAction(action);
        this.createTable(component, event, helper,component.get("v.header"));
    },
    createTable : function (component, event, helper,header) {
        //console.log( component.get('v.tableSectionContent') );
        var action = component.get("c.createTableSectionsContent");
        action.setParams(
            {
                'fieldsData' : JSON.stringify( component.get('v.tableSectionContent') ),
                'recordId' : component.get('v.recordId'),
                'headerName':header
            }
        );
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                console.log( result );                    
                component.set("v.tableData",result);
                component.set("v.spinner",false);                
            }            
        });
        
        $A.enqueueAction(action);
    },
    sortData : function (component, sortAsc,sortField,data) {
        data.sort(function(a,b){
            var t1 = a[sortField] == b[sortField],
                t2 = (!a[sortField] && b[sortField]) || (a[sortField] < b[sortField]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        return data;
    }, 
    navigationHandler : function (component, event, helper) {
        var URL = event.currentTarget.dataset.href;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": URL
        });
        urlEvent.fire();
    },
    deviceOrentationHandler : function( component,event,helper ){
        //alert(window.orientation);
        component.set("v.deviceOrentation",window.orientation);
        window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            //alert(window.orientation);
            //return window.orientation;
            component.set("v.deviceOrentation",window.orientation);
        }, false);
        
    },
    rollupOnLoadHelper : function( component,event,helper ){
        component.set("v.spinner",true);
        var action = component.get("c.rollupOnLoad");
        action.setParams( { 'recordId' : component.get('v.recordId') } );
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.spinner",false);                
            }            
        });
        $A.enqueueAction(action);
    },
})