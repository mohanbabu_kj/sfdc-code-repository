({
    showAPORMATHelper : function(component, event, helper) {
        var action = component.get("c.showMatScreenOnAP");
        action.setParams({ 'recordId' : component.get('v.recordId') });
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                if( result == null || result == '' ){
                    //SHOW AP
                    helper.apNameHelper( component, event, helper );
                }else{
                    //showMAT
                    console.log( 'MATID == '+ result );                    
                    component.set("v.MATId",result);
                    component.set("v.showMat",true);
                    component.set("v.spinner",false);
                }
            }            
        });        
        $A.enqueueAction(action);
    },
    apNameHelper : function(component, event, helper) {
        var action = component.get("c.getAccountProfileDetail");
        action.setParams({ 'recordId' : component.get('v.recordId') });
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                console.log( result );
                component.set("v.accountProfileDetail",result);
                this.onInitHelper( component, event, helper );   
            }            
        });        
        $A.enqueueAction(action);
    },
    onInitHelper : function(component, event, helper) {
        
        if( !component.get("v.accountProfileDetail.Multi_Channel__c") ){
            /*** PREVIOUS CODE - MUDIT - 18-7-2018 
            component.set("v.BusinessType",component.get("v.accountProfileDetail.Primary_Business__r.Name"));
        	PREVIOUS CODE - MUDIT - 18-7-2018 **/
            var builder = component.get("v.accountProfileDetail.Builder_Multi_Channel_split__c");
            var Retail =  component.get("v.accountProfileDetail.Retail_Channel_Split__c");
            console.log( '--1-->' + builder );
            console.log( '--2-->' + Retail );
            if( builder != null && builder != 0 ){
                console.log( '--3-->' );
                component.set("v.BusinessType","Builder");
                console.log( '--4-->' + component.get("v.BusinessType") );
            }
            if( Retail != null && Retail != 0 ){
                console.log( '--5-->' );
                component.set("v.BusinessType","Retail");
                console.log( '--6-->' + component.get("v.BusinessType") );
            }
            console.log( '-7--->' + component.get("v.BusinessType") );
        }
        var action = component.get("c.createExpandableSections");
        action.setParams(
            {
                'BusinessType':component.get("v.BusinessType"),
                //'sectionType':( component.get("v.BusinessType") != 'Multi Channel' ? component.get("v.selectedProductType") : '' ),
                //'sectionType':'',
                'device':component.get("v.device"),
                'recordId' : component.get('v.recordId')
            }
        );
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                console.log( result );
                component.set("v.data",this.sortData( component,true,'sectionSequence',result ));
                component.set("v.spinner",false);
            }            
        });
        
        $A.enqueueAction(action);
    },
    sortData : function (component, sortAsc,sortField,data) {
        data.sort(function(a,b){
            var t1 = a[sortField] == b[sortField],
                t2 = (!a[sortField] && b[sortField]) || (a[sortField] < b[sortField]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        return data;
    },
    onRadioClickHelper : function (component, event, helper) {
        var radioClicked = event.currentTarget.dataset.val;
        component.set("v.BusinessType",radioClicked);
        this.onInitHelper(component, event, helper);
    },
    picklistValueHelper : function (component, event, helper) {
        var action = component.get("c.getPickListValuesIntoList");
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                component.set("v.productTypePickList",result);
            }            
        });        
        $A.enqueueAction(action);
    },
    onChangeProductTypeHelper : function (component, event, helper) {
        this.onInitHelper( component, event, helper );   
    }
    
})