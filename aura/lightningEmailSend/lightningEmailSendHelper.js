({
    sendHelper: function (component, mailParams) {
        // sendHelper: function(component, getEmail, getSubject, getbody) {
        // call the server side controller method   
        var action = component.get("c.sendMailMethod");
        // set the 3 params to sendMailMethod method   
        action.setParams({
            'mMail': mailParams.to,
            'mSubject': mailParams.subject,
            'mbody': mailParams.body,
            'eventId': component.get('v.recordId'),
            'attachmentString': mailParams.attachments,
            'cc': mailParams.cc ? mailParams.cc : '',
            'bcc': mailParams.bcc ? mailParams.bcc : ''
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if state of server response is comes "SUCCESS",
                // display the success message box by set mailStatus attribute to true
                component.set("v.mailStatus", true);
            }

        });
        $A.enqueueAction(action);
    },
    mergeAppointWithTemplateAndAutoFillForm: function (component, appoinmentData, templateData, actionList) {
        if (templateData != '')
            component.set('v.subject', templateData.Subject);

        var startTime = appoinmentData.StartDateTime;
        var endTime = appoinmentData.EndDateTime;

        var startDateTime = startTime.substring(0, startTime.length-1);
        var endDateTime = endTime.substring(0, endTime.length-1);



        // var end = endDateTime.split('T');
        // var endTime = end[1].split('.');
        // var endTimeFormatted = moment(endDateTime).format('MM-dd-yyyy hh:mm a');
        var startTimeFormatted = moment(startDateTime).format('MM/DD/YYYY h:mm A');
        var endTimeFormatted = moment(endDateTime).format('MM/DD/YYYY h:mm A');


        // var startDatetime = new Date(appoinmentData.StartDateTime);
        // var endDateTime = new Date(appoinmentData.EndDateTime);
        // var startDatetimeFormatted = startDatetime.Format('MM-dd-yyyy hh:mm a')
        // var endDateTimeFormatted = endDateTimeFormatted.Format('MM-dd-yyyy hh:mm ')

        // var startDatetimeFormatted = startDatetime.getDate()  + "-" + (startDatetime.getMonth()+1) + "-" + startDatetime.getFullYear() + " " +
        //     startDatetime.getHours() + ":" + startDatetime.getMinutes();
        // var endDateTimeFormatted = endDateTime.getDate()  + "-" + (endDateTime.getMonth()+1) + "-" + endDateTime.getFullYear() + " " +
        //     endDateTime.getHours() + ":" + endDateTime.getMinutes();

        //var dateFormat = "MMMM d, yyyy h:mm a";
        //var dateFormat =  $A.get("$Locale.dateFormat");
        // console.log(DateUtil.formatDateTime(startDateTime,'MMMM d, yyyy h:mm a'));
        // var userLocaleLang = $A.get("$Locale.langLocale");
        // var timezone = $A.get("$Locale.timezone");
        //
        // var appoinmentDataStartDateTime = '';
        // var appoinmentDataEndDateTime = '';

        // var startDateField = '';
        // debugger
        // $A.createComponent(
        //     'ui:inputDatetime',
        //     {
        //         'label': 'Time',
        //         'value': startDateTime,
        //         'format': 'MMMM d, yyyy h:mm a'
        //     },
        //     function(element, status, errorMessage){
        //         if(status == 'SUCCESS'){
        //             startDateField = element;
        //         }
        //     });
        // var appoinmentDataStartDateTime = $A.localizationService.formatDateTimeUTC(startDateTime, dateFormat,userLocaleLang);
        //  var appoinmentDataEndDateTime   = $A.localizationService.formatDateTimeUTC(endDateTime, dateFormat,userLocaleLang);
        // console.log(startDateTime)
        //
        // $A.localizationService.UTCToWallTime(startDateTime, timezone, function (walltime) {
        //     appoinmentDataStartDateTime = $A.localizationService.formatDateTime(walltime, dateFormat, userLocaleLang);
        // });
        // console.log('after', appoinmentDataStartDateTime)
        // var appoinmentDataStartDateTime = $Locale.dateFormat()
        // var appoinmentDataStartDateTime = $A.localizationService.formatDate(endDateTime, dateFormat);
        // var appoinmentDataEndDateTime = $A.localizationService.formatDate(endDateTime, dateFormat);

        //var appoinmentDataStartDateTime =  $A.localizationService.formatDateTime(startDateTime, dateFormat, userLocaleLang);
        //var appoinmentDataEndDateTime = $A.localizationService.formatDateTime(endDateTime, dateFormat, userLocaleLang);

        

        var appoinmentDataObjectivesDisplay = appoinmentData.Objectives__c?appoinmentData.Objectives__c:'';

        appoinmentDataObjectivesDisplay += "<ul>";
        if(this.isNull(appoinmentData.Objectives__c)==false){
            console.log('test', appoinmentData.Objectives__c.split(/\r?\n/));
            appoinmentDataObjectivesDisplay = appoinmentData.Objectives__c + "<br />";
        }
        if(this.isNull(appoinmentData.Action_List_Items__c)==false){
            var appoinmentDataObjectives = appoinmentData.Action_List_Items__c;
            // var appoinmentDataObjectives = appoinmentData.Objectives__c;
            console.log(appoinmentDataObjectives)
            if (!appoinmentDataObjectives) {
                appoinmentDataObjectives = "";
            }
            // console.log(appoinmentDataObjectives);
            var appoinmentDataObjectivesList = appoinmentDataObjectives.split("・ ");
            appoinmentDataObjectivesList.shift();

            for (var i = 0; i < appoinmentDataObjectivesList.length; i++) {
                appoinmentDataObjectivesDisplay += "<li>" + appoinmentDataObjectivesList[i] + "</li>";
            }
        }
        appoinmentDataObjectivesDisplay += "</ul>";
        
        /*var appoinmentDataObjectivesDisplay = "";
        if(actionList.length>0) {
            appoinmentDataObjectivesDisplay = "<ul>";
            for(var i in actionList) {
                appoinmentDataObjectivesDisplay += "<li>" + actionList[i].Name + "</li>";
            }
            appoinmentDataObjectivesDisplay += "</ul>";
        }*/

        // '<p>Dear ' + appoinmentData.What.Name + ',</p>'
        /*
        '<tr>' +
            '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Subject</td>' +
            '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + appoinmentData.Subject + '</td>' +
            '</tr>' +
        */
        var richTextTable = '<p style="font-weight: bold;">Appointment Details</p>' +
            '<table cellpadding="0" cellspacing="0" border="0" style="border: none;">' +
            
            '<tr>' +
            '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Start</td>' +
            '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + startTimeFormatted + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">End</td>' +
            '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + endTimeFormatted + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Objectives</td>' +
            '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + appoinmentDataObjectivesDisplay + '</td>' +
            '</tr>' +
            '</table>';

        var table = richTextTable;
        var bodyVal = component.get('v.body');
        // console.log(bodyVal)
        component.set("v.body", bodyVal + (templateData != '' ? templateData.HtmlValue + '<br />' : '') + table);
    },
    mergeActionListItemsToBody: function (component, salItems, sectionTitle) {
        if (salItems && salItems != '' && salItems.length > 0) {
            var sectionHtml = '<div style="height: 30px;line-height: 30px;font-weight:bold;">' + sectionTitle + '</div>' +
                '<table cellpadding="0" cellspacing="0" border="0" style="border: none;"' +
                '<tr>' +
                '<th style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">S.No</th>' +
                '<th style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Name</th>' +
                '<th style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Description</th>' +
                '</tr>';

            var richTextTableEnd = '</table>';

            for (var i = 0; i < salItems.length; i++) {
                sectionHtml += '<tr>' +
                    '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + (i + 1) + '</td>' +
                    '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + salItems[i].Action_List__r.Name + '</td>' +
                    '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + (salItems[i].Action_List__r.Description__c ? salItems[i].Action_List__r.Description__c : '') + '</td>' +
                    '</tr>';
            }
            sectionHtml += '</table>';
            var body = component.get("v.body");
            component.set("v.body", body + sectionHtml);
        }
    },
    removeItem: function (component, index) {
        var salAttachList = component.get("v.salAttachList");
        salAttachList.splice(index, 1);
        component.set("v.salAttachList", salAttachList);
    },
    getAttachList: function (component, eventId) {
        var salAction = component.get("c.salAttachments");
        salAction.setParams({"eventId": eventId, 'fetch': 'id-name'});
        salAction.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // console.dir(response.getReturnValue())
                component.set('v.salAttachList', response.getReturnValue());
            }
        });
        $A.enqueueAction(salAction);
    },
    isNull : function(text){// 05/08/2017
        return (text == undefined || text == '' || text == null ? true : false);
    },
    getEventContact : function(component, event, helper) {
        var action = component.get("c.getEventContactEmail"); 
            action.setParams({
                'eventId': component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === "SUCCESS" && response.getReturnValue()!=null){
                   // document.getElementById('toemail').value = response.getReturnValue();
                    // component.set('v.email', response.getReturnValue());
                }
            });
        $A.enqueueAction(action);
    }
   
})