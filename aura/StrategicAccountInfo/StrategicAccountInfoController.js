({
    init: function(component, event, helper) {
        helper.initHelperMethod( component, event, helper );        
    },
	save : function(component, event, helper) {
        component.set('v.statusMessage', '');
        var account = component.get("v.account");
        var Facilities = account.Facilities__c;
        var Est_of_Orders_in_the_next_6_months = account.Est_of_Orders_in_the_next_6_months__c;
        
        //Start Mohan Chander US-51134/Bug-49897 
        var realEstateServiceProvider = component.get("v.selectedLookUpRecord");        
        if( $A.util.isEmpty( realEstateServiceProvider ) ){
            component.set("v.account.Real_Estate_Service_Provider__c",null);
        } else {
            component.set("v.account.Real_Estate_Service_Provider__c",realEstateServiceProvider.Id);
        }
        
        helper.setStatusEmpty(component);        
		console.log( account.Account_Geography__c );
        console.log( Facilities );
        console.log( Est_of_Orders_in_the_next_6_months );
        if( ( account.Account_Geography__c != null && account.Account_Geography__c != '' ) && 
            ( Facilities != null && Facilities != '') &&
            ( Est_of_Orders_in_the_next_6_months != null && Est_of_Orders_in_the_next_6_months != '' )
          ){        
            if (account.Strategic_Account__c || component.get('v.isCurrentUserAdmin') == true) {
                if(component.get('v.isCurrentUserAdmin') == false){
                    component.set('v.statusMessage', 'You don\'t have permission to edit record');
                } else {
                    //component.set("v.account.Service_Needs__c", component.get("v.SelectedServiceNeeds") );
                    //component.set("v.account.Purchasing_Plans__c", component.get("v.SelectedPurchasingPlans") );
                    //component.set("v.account.Real_Estate_Service_Provider__c", component.find("Real_Estate_Service_Provider__c").get("v.value") );
                    helper.validateContractDate(component, event, helper);
                    if( component.get('v.errorOccurred') == false ){
                        helper.createVote( component, account );
                    }
                }
            } else {
                component.set('v.statusMessage', 'No Strategic Account. You can not save the record.')
            }
        }else{
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Please enter required fields');
            component.set('v.errorOccurred', true);
        }
    },
    submit : function(component, event, helper) {
        helper.setStatusEmpty(component);
        debugger;
        var account = component.get("v.account");
        var Facilities = account.Facilities__c;
        var Est_of_Orders_in_the_next_6_months = account.Est_of_Orders_in_the_next_6_months__c;
        
        if( ( account.Account_Geography__c != null && account.Account_Geography__c != '' ) && 
            ( Facilities != null && Facilities != '') &&
            ( Est_of_Orders_in_the_next_6_months != null && Est_of_Orders_in_the_next_6_months != '' )
          ){           
            if ( account.Strategic_Account__c ) {
                component.set('v.statusMessage', 'Strategic Account - You can not submit for approval again')
            } else {
                //component.set("v.account.Facilities__c",Facilities);
                //component.set("v.account.Est_of_Orders_in_the_next_6_months__c",Est_of_Orders_in_the_next_6_months);
                //component.set("v.account.Service_Needs__c", component.get("v.SelectedServiceNeeds") );
                //component.set("v.account.Purchasing_Plans__c", component.get("v.SelectedPurchasingPlans") );
                helper.validateContractDate( component, event, helper );
                if( component.get('v.errorOccurred') == false ){ //Added by MB - 10/18/17
                	helper.createApproval(component, account); 
                }
            }
        }else{
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Please enter required fields');
            component.set('v.errorOccurred', true);
        }
    },
    edit : function(component, event, helper) {
        component.set("v.status", "edit");
    },
    handleValueChange: function(component, event, helper) {
        
    },
    handleFieldChange: function(component, event, helper) {
        component.set("v.toggleSpinner",true);
        var fieldName = event.getSource().getLocalId();
        if( fieldName == 'MVP_Coordinator__c' ){
            if( event.getSource().get('v.value') != '' )
                helper.getMVPInfoHelper(component, event);
            else{
                component.set("v.account.MVP_Phone__c",'');
                component.set("v.account.MVP_Email__c",'');
                component.set("v.toggleSpinner",false);
            }
        }else if( fieldName == 'Contract_Start_date__c' || fieldName == 'Contract_End_Date__c'){
            helper.validateContractDate(component, event, helper);
        }
    },
    // Start of Code by MB - 08/10/17
    handleServiceNeedChange: function(component, event, helper){
        
    },
    closeAlertMsg: function(component, event, helper){
        component.set("v.isErrorContractDate",false);
        component.set('v.contractErrorMsg', '');
        component.set('v.errorOccurred', false);
    },
    cancel : function(component, event, helper) {
        /*var navigateEvent = $A.get("e.force:navigateToSObject");
        navigateEvent.setParams({ "recordId": component.get('v.recordId') });
        navigateEvent.fire();*/
        
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    //Start Mohan Chander US-51134/Bug-49897 
    toggleLookup : function(component, event, helper) {
        
    },
    //End Mohan Chander US-51134/Bug-49897 
    showRequiredFields: function(component, event, helper){
		$A.util.removeClass(component.find("Facilities__c"), "none");
        $A.util.removeClass(component.find("Account_Geography__c"), "none");
        $A.util.removeClass(component.find("Est_of_Orders_in_the_next_6_months__c"), "none");
    },
    handleSelectChangeEventPurchasingPlans: function(component, event, helper) {
        var previousVal = component.get("v.SelectedPurchasingPlans");
        var items = event.getParam("values");
        component.set("v.SelectedPurchasingPlans", items);
    },
    handleSelectChangeEventServiceNeeds: function(component, event, helper) {
        var previousVal = component.get("v.SelectedServiceNeeds");
        var items = event.getParam("values");
        component.set("v.SelectedServiceNeeds", items);
    }
})