({
    doInit: function (cmp, evt, helper) {
        helper.getObjName(cmp, evt, helper);     
        helper.deviceOrentationHandler( cmp, event, helper );

    },
    backToRecord: function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            window.history.back();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            window.location.reload();
        }
    },
    cancel : function(component, event, helper) {
        
        
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
   /* handleClick: function(component, event, helper){
        console.log("test");
        var appEvent = $A.get("e.c:handleOnClick");
        appEvent.fire();


    },*/
    // save data to backend
    saveToBackend: function (component, event, helper) {
        helper.updateInfo(component, event);
    },
    // set all checkbox status of SAL
    selectAll: function (cmp, evt, helper) {
        var selectAll = cmp.get("v.selectAllChecked");
        cmp.set("v.isChecked", "");
        if (selectAll == "") {
            cmp.set("v.selectAllChecked", "checked");
            cmp.set("v.isChecked", "checked");
        } else if (selectAll == "checked") {
            cmp.set("v.selectAllChecked", "")

        }
    },
    // set all checkbox status of recommended project SAL - start
    setRecommendSelectAll: function (cmp, evt, helper) {
        cmp.set("v.setSelectRecommendAllChecked", "");
    },
    selectRecommendAll: function (cmp, evt, helper) {
        var selectAll = cmp.get("v.setSelectRecommendAllChecked");
        cmp.set("v.isRecommendChecked", "");
        if (selectAll == "") {
            cmp.set("v.setSelectRecommendAllChecked", "checked");
            cmp.set("v.isRecommendChecked", "checked");
        } else if (selectAll == "checked") {
            cmp.set("v.setSelectRecommendAllChecked", "")
            cmp.set("v.isRecommendChecked", "");
        }
    },
    // set all checkbox status of recommended project SAL - end
    // set all checkbox status of Fundamental
    selectFundamental: function (cmp, evt, helper) {
        var selectFundamental = cmp.get("v.setSelectFundamentalChecked");
        cmp.set("v.fundamentalIsChecked", "");
        if (selectFundamental == "") {
            cmp.set("v.setSelectFundamentalChecked", "checked");
            cmp.set("v.fundamentalIsChecked", "checked");
        } else {
            cmp.set("v.setSelectFundamentalChecked", "");
            cmp.set("v.fundamentalIsChecked", "");
        }
    },
    // set Select SAL checkbox Status
    setSelectAll: function (cmp, evt, helper) {
        cmp.set("v.selectAllChecked", "");
    },
    // set Select Fundamental checkbox Status
    setSelectFundamental: function (cmp, evt, helper) {
        cmp.set("v.setSelectFundamentalChecked", "");
    },
    //event of click disabled checkbox
    returnFalse: function (cmp, evt, helper) {
        return false;
    },
    /**
    searchFilter: function (component, event, helper) {
        var brandlist = component.get('v.selectedBrand');
        var segmentList = component.get('v.selectedSegment');
        helper.retrieveSAL(component, event, helper, brandlist, segmentList);
    },
    */
    handleSelectChange: function(component, event, helper) {
        var selectedSegment = event.getParam("data");
        var brandlist=[];
        var segmentList=[];
        if( selectedSegment[0].label == 'Channel' ){
           // alert(selectedSegment[0].values);
            component.set( "v.selectedSegment",selectedSegment[0].values );
        }
        if( selectedSegment[0].label == 'Brand Family' ){
            component.set( "v.selectedBrand",selectedSegment[0].values );
        }
         brandlist = component.get('v.selectedBrand');
         segmentList = component.get('v.selectedSegment');
       /* if(selectedSegment[0].label == 'Brand Family' && selectedSegment[0].values.includes('All')){
          // alert('entry 1');
            brandlist=component.get('v.brandOptions');
        }
         if(selectedSegment[0].label == 'Channel' && selectedSegment[0].values.includes('All')){
         //alert('entry 2')
             segmentList=component.get('v.segmentOptions');
        }*/
        
        component.set("v.moveTabletoDown",true);
        helper.retrieveSAL(component, event, helper, brandlist, segmentList);
    },
    sortingSLA : function (component, event, helper) {
        var fieldName = event.currentTarget.title;
        helper.sortData(component,fieldName,component.get("v.actionItems.checkSAL"),'checkActionListObj','actionItems.checkSAL','sortAscSLA','sortFieldSLA');
    },
    sortingFundamental : function (component, event, helper) {
        var fieldName = event.currentTarget.title;
        helper.sortData(component,fieldName,component.get("v.actionItems.checkFundamentalList"),'checkActionListObj','actionItems.checkFundamentalList','sortAscFundamental','sortFieldFundamental');
    },
    sortingProject : function (component, event, helper) {
        //var fieldName = event.currentTarget.title;
        //helper.sortData(component,fieldName,component.get("v.actionItems.checkRecommendedSalesActionList"),'actionItems.checkRecommendedSalesActionList','sortAscProject','sortFieldProject');
    },
    openPage : function(component, event, helper) {
        var targetId = event.currentTarget.dataset.id;
        var device = $A.get("$Browser.formFactor");
        if( targetId !='' && device == 'DESKTOP'  ){            
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": targetId,
                'slideDevName': 'detail'
            });
            navEvt.fire();
        }else if( targetId != '' && ( device == 'PHONE' || device == 'TABLET' ) ){
            var navLink = component.find("navLink");
            var pageRef = {
                type: 'standard__recordPage',
                attributes: {
                    actionName: 'view',
                    //objectApiName: 'Action_List__c',
                    recordId : targetId // change record id. 
                },
        };
        navLink.navigate(pageRef, true);
            //sforce.one.navigateToSObject( targetId ,"detail"); 
        }
        else {
            alert('No Record Id found. Unable to load the target page');
        }
    },
})