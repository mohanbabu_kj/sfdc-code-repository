({
	handleSubmit : function(component, event, helper) {
        console.log('Submit');
	},
    
    handleSuccess : function(component, event, helper){
        console.log('Success');
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    handleError : function(component, event, helper){
        console.log('Error Occurred');
        component.set('v.isError', true);
    },
    
    cancel : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
    
})