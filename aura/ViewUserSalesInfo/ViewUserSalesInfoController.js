({
    doInit: function (component, event, helper) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.getUserSalesInfo");
        action.setParams({'recordId': recordId});
        console.log(recordId);
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(state);
            if (component.isValid() && state === "SUCCESS") {
                console.log('Return Value' + response.getReturnValue());
                console.log('Result' + result.loggedInUserProf);
                console.log(result.isValid);
                console.log(result.user);
                component.set('v.UserValid', result.isValid );
                component.set('v.User', result.user);
                component.set('v.UserMap', result);
                if(result.loggedInUserProf == 'Commercial Sales User'){
                    component.set('v.SalesAE', true);
                    component.set('v.edit', false);
                }else if(result.isValid && result.loggedInUserProf != 'Commercial Sales User'){
                    component.set('v.SalesAE', false);
                    component.set('v.edit', true);
                }
            }
        });
        $A.enqueueAction(action);
        helper.setPicklistValues(component, event, helper);
    },
    
    onEdit: function(component, event, helper){
        console.log(component.get('v.User.Industry_Experience__c'));
        console.log(component.get('v.User.Addition_or_Replacement__c'));
        component.set('v.indExpSelected', component.get('v.User.Industry_Experience__c'));
        component.set('v.addReplSelected', component.get('v.User.Addition_or_Replacement__c'));
        component.set('v.save', true);
        component.set('v.edit', false);
    },
    
    onSave: function(component, event, helper){
        component.set('v.isErrorOccurred', false);
        component.set('v.User.Industry_Experience__c', component.find("indExp").get("v.value"));
        component.set('v.User.Addition_or_Replacement__c ',component.find("addRepl").get("v.value"));
        var action = component.get("c.setUserSalesInfo");
        action.setParams({'salesInfo': component.get('v.User')});
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(result);
            if (component.isValid() && state === "SUCCESS") {
                component.set('v.save', false);
        		component.set('v.edit', true);
            }else{
                component.set('v.isErrorOccurred', true);
                component.set('v.errorMessage', 'Error Occurred while saving the record. Please try after some time.');
            }
        });
        $A.enqueueAction(action);
        
    },

    onChangeIndExp: function(component){
        var indExpList = component.get('v.indExpList');
        var addReplList = component.get('v.addReplList');
        var selected = false;
        debugger;
        var indExp = component.find("indExp").get("v.value");
        component.set('v.indExpSelected', indExp);
    },
    
    onChangeAddRepl: function(component){
        var addRepl = component.find("addRepl").get("v.value");
        component.set('v.addReplSelected', addRepl);
    },
    
    onCancel: function(component, event, helper){
        component.set('v.save', false);
        component.set('v.edit', true);

    },
    
    closeAlertMsg: function(component, event, helper){
        component.set("v.isErrorOccurred",false);
    }
})