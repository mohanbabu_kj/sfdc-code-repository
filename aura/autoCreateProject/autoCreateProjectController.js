({
    doInit : function(component, event, helper) {
    },
    confirm : function(component, event, helper) {
    	var prompt = component.find('confirm');
    	$A.util.toggleClass(prompt, 'hide');

    	component.set('v.showLoader', 'block');

    	var action = component.get("c.generateProject");
		action.setParams({
			"reqQuoteId" : component.get('v.recordId'),
			"oppStage" : 'Bidding'
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			component.set('v.showLoader', 'none');
			if (state === "SUCCESS" && response.getReturnValue()=='success') {
				component.set('v.statusMessage', 'Project created successfully. Current record will be converted automatically');
				setTimeout(function(){
					helper.closePanel();
				}, 3000);
			} else {
				if(state === "SUCCESS" && response.getReturnValue()!='success')
					component.set('v.statusMessage', response.getReturnValue());
				else
					helper.showErrorMessage(component, response.getError());
				// component.set('v.loadingMsg', 'Unknown error occurred');
			}			
		});
		$A.enqueueAction(action);
    },
    cancel : function(component, event, helper) {
    	helper.closePanel();
    }
})