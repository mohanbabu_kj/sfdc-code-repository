({
    closePanel : function() {
    	var dismissActionPanel = $A.get("e.force:closeQuickAction");
		dismissActionPanel.fire();
		$A.get('e.force:refreshView').fire();
    },

    isNull : function(text){// 03/18/2017
        return (text == undefined || text == '' ? true : false);
    },

    showErrorMessage : function(component, errorObj){// 03/18/2017
        console.dir(errorObj);
        var message = 'Unable to process your request.';
        if (errorObj) {
            if (errorObj[0] && errorObj[0].fieldErrors){
                var fErrors = errorObj[0].fieldErrors;
                for(field in fErrors){
                    message += ' Reason: Missing ' + field + ' - ' + fErrors[field][0].message;
                }
            }
            if (errorObj[0] && errorObj[0].pageErrors && !this.isNull(errorObj[0].pageErrors[0])) {
                if(!this.isNull(errorObj[0].pageErrors[0].message))
                    message += ' Reason: ' + errorObj[0].pageErrors[0].message;
            }
            component.set('v.statusMessage', message);
        } else {
            component.set('v.statusMessage', message);
        }
    }
})