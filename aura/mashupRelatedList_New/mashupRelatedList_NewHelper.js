({
    
    generateHtml : function(component) {
    	var RecordsByGroup = component.get('v.RecordsByGroup');
    	// console.log('RecordsByGroup', RecordsByGroup);
    	var container = component.find('dynamicBlock');
    	var htmlCon = '';
        var recordCount = component.get('v.recordCount');
    	var iconPath = component.get('v.iconPath');

        // iteration based on response received with MashupResultHandler object structure
    	for(var level0 in RecordsByGroup) {
    		// console.log('level0', level0);
    		for(var level1 in RecordsByGroup[level0]){
    			// console.log('level1', level1);
    			for(var level2 in RecordsByGroup[level0][level1]) {
                    var items = RecordsByGroup[level0][level1][level2];
                    // console.log(level2+'-items', items)
                    if(items.length>0){
                        $A.createComponent('div', 
                                { class: 'slds-size--1-of-1 slds-p-around--small'}, 
                                function(wrapElement, wrapStatus, error){
                                    if(wrapStatus == 'SUCCESS'){
                                        var wrapBody = wrapElement.get('v.body');
                                        var headElement='';

                                        // section header
                                        $A.createComponent('aura:html', 
                                            { 
                                                tag: 'h3', 
                                                HTMLAttributes: { class: 'slds-text-heading--small groupTitle slds-m-bottom--small slds-truncate'}, body: level2.replace(/[0-9]./g,'') 
                                            }, 
                                            function(headTag, headStatus, headError){
                                                if(headStatus == 'SUCCESS'){
                                                    wrapBody.push(headTag);
                                                }
                                            });

                                        // section list of items iteration
                                        $A.createComponent('ul',
                                            { class: 'slds-has-dividers_bottom-space'},
                                            function(ulWrapper, ulStatus, ulError){
                                                if(ulStatus == 'SUCCESS') {
                                                    var ulBody = ulWrapper.get('v.body');
                                                    for(var i=0; i<items.length; i++) {
                                                        
                                                        $A.createComponent('li', 
                                                            { 
                                                                'class': 'slds-item slds-truncate',
                                                                'data-url': items[i].Target_URL__c,
                                                                "onclick": component.getReference("c.navigateComponent")
                                                            },
                                                            function(liTag, liStatus, liError){
                                                                if(liStatus == 'SUCCESS'){
                                                                    ulBody = ulWrapper.get('v.body');

                                                                    // internal component call to generate anchor tag
                                                                    $A.createComponent('c:mashupRelatedListItem', { 'currentRecord': items[i] },
                                                                        function(aTag){
                                                                            liTag.set('v.body', aTag);
                                                                        });
                                                                    
                                                                    ulBody.push(liTag);
                                                                    ulWrapper.set('v.body', ulBody);
                                                                }
                                                            });
                                                        recordCount++;
                                                    }
                                                    wrapBody.push(ulWrapper);
                                                }
                                            });
                                        wrapElement.set('v.body', wrapBody);

                                        var body = container.get('v.body');
                                        body.push(wrapElement);
                                        container.set('v.body', body);
                                    }
                                });
                    }
    			}
    		}
    	}
    	
        // document.getElementById(component.get('v.mashupType')+'-dynamicBlock').innerHTML = htmlCon;
        component.set('v.recordCount', recordCount);
    }
})