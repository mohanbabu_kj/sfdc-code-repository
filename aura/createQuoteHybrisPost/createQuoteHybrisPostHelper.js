({
  loadIframe : function(component, responseJson) {
   
    // to use dynamic URL from Custom Setting "Mashup"
    var container = component.find("iframeDiv");
    /*$A.createComponent("c:hybrisIframe", {
        mashupName: 'View Quote - Hybris',
        paramList: { recordId: responseJson.hybrisQuoteId }},
        function(cmp) {
            container.set("v.body", [cmp]);
        }
    );*/
      var recordId = component.get('v.recordId');
      $A.createComponent("c:mashupTabItem", {
          mashupName: 'Submit a Quote',
          recordId: recordId,
          paramList: {}
      }, function(cmp) { container.set("v.body", [cmp]); }
                        );
  }
})