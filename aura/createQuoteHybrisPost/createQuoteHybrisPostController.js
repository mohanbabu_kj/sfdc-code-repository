({
    doInit : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        var opts = [
                    { label: "Select an Account", value: "", selected: "true"}
                    ];
        if(recordId.substring(0,3)=='006'){
            var selectAccount = component.find('selectAccount');
            var selectContact = component.find('selectContact');
            var selectForm = component.find('selectForm');
            
            selectAccount.set('v.options', opts);
            selectContact.set('v.options', [
                    { label: "Select Contact", value: "", selected: "true"}
                    ]);

            var action = component.get("c.getRelatedAccList");
            action.setParams({ "opportunityId" : recordId });
            action.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === "SUCCESS") {
                    var data = response.getReturnValue();
                    // console.log(data)
                    component.set('v.relatedList', data);
                    
                    if(data.length>0){
                        for(key in data){
                            opts.push({label: data[key].Account__r.Name, value: data[key].Id});
                        }
                        selectAccount.set('v.options', opts);
                    } else {
                        //alert('No Related Dealer / End user Accounts found');
                        component.find('submitBtn').set("v.disabled",true);
                    }

                }
            });
            $A.enqueueAction(action);
            component.set('v.showLoader', 'none');
            
            $A.util.removeClass(selectForm, 'hideMessage');
            $A.util.addClass(selectForm, 'showMessage'); 
        } else
            component.set('v.callQuoteEve', 'yes');
    },
    validateForm : function(component, event, helper) {
        var selectAccount = component.find('selectAccount');
        var notifyBlock = component.find('responseMessage');
        // console.log(selectAccount.get('v.value'))
        if(selectAccount.get('v.value') && selectAccount.get('v.value')!='') {
            var btn = event.getSource();
            btn.set("v.disabled",true);
            component.set('v.callQuoteEve', 'yes');
        } else{
            component.set('v.responseMessage', "Please select Account to continue");
            $A.util.removeClass(notifyBlock, 'hideMessage');
            $A.util.addClass(notifyBlock, 'showMessage');
        }
    },
    doCreateQuote : function(component, event, helper) {
        component.set('v.showLoader', 'block');
    	var resMessage='';
        var notifyBlock = component.find('responseMessage');
        var loadingMsg = component.find('loadingMsg');
        var selectForm = component.find('selectForm');
        var submitBtn = component.find('submitBtn');

        var recordId = component.get('v.recordId');
        var contactId = '';
        if(recordId.substring(0,3)=='006')
            recordId = component.find('selectAccount').get('v.value');
        contactId = component.find('selectContact').get('v.value');
        console.log('contactId', contactId);

    	var action = component.get("c.doCreateQuotePost");
        // action.setParams({ "relAcId" : component.find('selectAccount').get('v.value') });
        action.setParams({ "relAcId" : recordId, 'contactId': contactId });
        action.setCallback(this, function(response) {
        	$A.util.removeClass(loadingMsg, 'showMessage');
            $A.util.addClass(loadingMsg, 'hideMessage'); 
            
            $A.util.removeClass(selectForm, 'showMessage');
            $A.util.addClass(selectForm, 'hideMessage'); 

            component.set('v.showLoader', 'none');

            if (component.isValid() && response.getState() === "SUCCESS") {
                // console.log(response.getReturnValue())
            	var responseJson = JSON.parse(response.getReturnValue());
                console.dir(responseJson);
               
                if(responseJson.responseStatus=='Success'){
                	resMessage = 'Quote created successfully. View Quote in Hybris will be loaded now...';
                	//setTimeout(function(){
                		helper.loadIframe(component, responseJson);
                	//}, 1000);
                	setTimeout(function(){
                		component.set('v.responseMessage', '');
                	}, 1000);
                } else {
                	resMessage = 'Quote creation failed. Please try again later OR contact your administrator';
                    submitBtn.set("v.disabled",false);
                }
                component.set('v.responseMessage', resMessage);
                $A.util.removeClass(notifyBlock, 'hideMessage');
                $A.util.addClass(notifyBlock, 'showMessage');               
            } else {
                submitBtn.set("v.disabled",false);
            	component.set('v.responseMessage', 'Unknown error occurred. Unable to complete the request');
                $A.util.removeClass(notifyBlock, 'hideMessage');
                $A.util.addClass(notifyBlock, 'showMessage');
            }
        });
        if(recordId!='' && contactId!=''){
            $A.enqueueAction(action);
        } else {
            component.set('v.showLoader', 'none');
            component.set('v.responseMessage', 'Please select Account and Contact');
            $A.util.removeClass(notifyBlock, 'hideMessage');
            $A.util.addClass(notifyBlock, 'showMessage');
        }
    },
    loadContacts: function(component, event, helper) {
        var selAccount = event.getSource();
        console.log('selected account', selAccount.get('v.value'));
        var selectContact = component.find('selectContact');

        var opts = [];
        var accId = '';

        selectContact.set('v.options', { label: "Select Contact", value: "", selected: "true"});

        var relAccounts = component.get('v.relatedList');
        for(i in relAccounts){
            if(relAccounts[i].Id == selAccount.get('v.value')){
                accId = relAccounts[i].Account__c;
            }
        }
        // console.log('accId', accId);

        if(accId!=''){
            var action = component.get("c.getAccountContacts");
            action.setParams({ "accId" : accId });
            action.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === "SUCCESS") {
                    var data = response.getReturnValue();
                    // console.log(data);
                    if(data.length>0){
                        for(key in data){
                            var record = data[key];
                            var contactInfo = '';
                            if(record.Email)
                                contactInfo += record.Email;
                            if(record.Contact_Phone__c)
                                contactInfo += (record.Email?', ':'') + record.Contact_Phone__c;

                            opts.push({label: record.Name + (contactInfo!=''?' ('+ contactInfo +')':''), value: record.Id});
                        }
                        selectContact.set('v.options', opts);
                    }
                }
            });
            $A.enqueueAction(action);
        }
    }
})