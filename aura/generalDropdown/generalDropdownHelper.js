({
	//Server Call to get the Data when the component load
    //Based on the Object type will recive the return data
    initHelper: function(component, event, helper) {
		
        var recId = component.get('v.recordId');
        var objName = component.get('v.ObjectName');
        var proId;
        var quoteId,noValue = false;
       
       
            var action1 = component.get("c.getsObjectList");
            action1.setParams({
                'objectName': objName,
                'IdValue' :recId
            });
            action1.setCallback(this, function(response) {
                var wrpperCls = response.getReturnValue();
                console.log(wrpperCls);
                if(wrpperCls.isError == false)
                {
                    component.set('v.AccProfSettingsList', wrpperCls.sObjectList);
                    
                }
               
            });
            $A.enqueueAction(action1);
        
        
    }
})