({
    doInit: function(component, event, helper) {
        console.log(':::::'+component.get('v.previousSelected'));
        helper.initHelper(component, event, helper);
        
                  
    
    },
    
    selectRecord : function(component, event, helper){  
        
    
        var objName = component.get('v.ObjectName');
        var getSelectRecord;
        var recId;
        recId=component.find("generalDropDwn").get("v.value");
        
        
        var action = component.get("c.getObject");
        
        action.setParams({'IdValue': recId,
                          'objectName':objName});
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            component.set('v.selectedRecord', response.getReturnValue());
            // get the selected record from list  
            getSelectRecord = component.get("v.selectedRecord");
            
            // call the event   
            var compEvent = component.getEvent("sendOutObjEvt");
            
            // set the Selected sObject Record to the event attribute.  
            compEvent.setParams({"recordByEvent" : getSelectRecord });  
            // fire the event  
            compEvent.fire();
        });
        $A.enqueueAction(action);
        
        
        
    }
})