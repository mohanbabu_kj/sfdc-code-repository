({
    scriptLoaded : function(component, event, helper) {
        // console.log('jquery loaded');
    },
    save : function(component, event, helper) {
        console.log(component.get("v.selectedLookUpRecord"));
        component.set('v.statusMessage', '');
        // component.find("edit").get("e.recordSave").fire();
        var account = component.get("v.account");
        
        //Start Mohan Chander US-51134/Bug-49897 
        var realEstateServiceProvider = component.get("v.selectedLookUpRecord");
        if($A.util.isEmpty(realEstateServiceProvider)){
            account.Real_Estate_Service_Provider__c = null;
        } else {
           account.Real_Estate_Service_Provider__c = realEstateServiceProvider.Id; 
        }                
        console.log('**' + account.Real_Estate_Service_Provider__c);
        //Start Mohan Chander US-51134/Bug-49897 
        
        var globalId = component.getGlobalId();
        helper.setStatusEmpty(component);        

        if (account.Strategic_Account__c || component.get('v.isCurrentUserAdmin')==true) {
            if(component.get('v.isCurrentUserAdmin')==false){
                component.set('v.statusMessage', 'You don\'t have permission to edit record');
            } else {
                var accountObj = helper.getFormData(component);
				//Start of Code -MB- 08/22/17
				accountObj['Service_Needs__c'] = account.Service_Needs__c;
                
                //Start Mohan Chander US-51134/Bug-49897 
                accountObj['Real_Estate_Service_Provider__c'] = account.Real_Estate_Service_Provider__c;
                //End Mohan Chander US-51134/Bug-49897 
                
				//End of code -MB- 08/22/17
                // console.log(account);
                helper.validateContractDate(component, accountObj); //Added by MB - 10/18/17
                console.log("Test 1" +component.get('v.errorOccurred'));
                console.log("Test 2" + ( component.get('v.errorOccurred')  == false) );
                if(component.get('v.errorOccurred') == false){ //Added by MB - 10/18/17
                	helper.createVote(component, accountObj);
                }
            }
        } else {
            component.set('v.statusMessage', 'No Strategic Account. You can not save the record.')
        }
    },
    submit : function(component, event, helper) {
        helper.setStatusEmpty(component);
        // component.find("edit").get("e.recordSave").fire();
        var account = component.get("v.account");
        // console.log(account);
        component.set('v.statusMessage', '');
        // Start of Code by MB - 07/24/17
        var accountObj = helper.getFormData(component);
        if((accountObj.Account_Geography__c != null && accountObj.Account_Geography__c != '') && 
           (accountObj.Facilities__c != null && accountObj.Facilities__c != '') &&
           (accountObj.Commission_Type_Core_Only__c != null && accountObj.Commission_Type_Core_Only__c != '') ){
           //(accountObj.Contract_Start_date__c != null && accountObj.Contract_Start_date__c != '') && - Commented by MB - 10/18/17
           //(accountObj.Contract_End_Date__c != null && accountObj.Contract_End_Date__c != '') ){ - Commented by MB - 10/18/17
            //debugger;
            if (account.Strategic_Account__c) {
                component.set('v.statusMessage', 'Strategic Account - You can not submit for approval again')
            } else {
                //var accountObj = helper.getFormData(component); //Commented by MB - 07/24/17
                accountObj['Service_Needs__c'] = account.Service_Needs__c; // Added by MB - 08/22/17
                helper.validateContractDate(component, accountObj); //Added by MB - 10/18/17
                if(component.get('v.errorOccurred') == false){ //Added by MB - 10/18/17
                	helper.createApproval(component, accountObj); 
                }
            }
        }else{ // End of Code by MB - 07/24/17
            //debugger;
            component.set('v.statusMessage', 'Please enter required fields');
        }
    },
    edit : function(component, event, helper) {
        component.set("v.status", "edit");
        // :not([name="Approval_Status__c"]) added 04/13/2017 to always show readonly/disabled
        jQuery('.input-fields .slds-input, .input-fields .slds-textarea, .input-fields .slds-select:not([name="Approval_Status__c"]), .input-fields input[type="checkbox"]').each(function(){
            var elem = jQuery(this);
            elem.attr('readonly', false);
            elem.attr('disabled', false);
        });
        component.set("v.SNReadOnly", "false");
        console.log('SNReadOnly: ' + component.get('v.SNReadOnly'));
        // Start of Code by MB - 08/22/17
        //debugger;
        var SRInput = component.find('SRInput');
        var SROutput = component.find('SROutput');
        if(component.get('v.SNReadOnly') == true){
            $A.util.addClass(SRInput, 'slds-hide');
            $A.util.removeClass(SRInput, 'slds-show');
            $A.util.addClass(SROutput, 'slds-show');
            $A.util.removeClass(SROutput, 'slds-hide');
        }else{
            $A.util.removeClass(SROutput, 'slds-show');
            $A.util.addClass(SROutput, 'slds-hide');
            $A.util.removeClass(SRInput, 'slds-hide');
            $A.util.addClass(SRInput, 'slds-show');
        }// End of Code by MB - 08/22/17
        // console.log(component.get("v.status"))
    },
    handleValueChange: function(component, event, helper) {
        component.set('v.isFieldListChanged', true);
        // console.log('inn')
    },
    handleFieldChange: function(component, event, helper) {
        var field = event.getSource();
        console.log(field.get('v.value'));
        console.log(field.get('v.name'));
        // Added by MB - 07/13/17
        if(field.get('v.name') == 'MVP_Coordinator__c'){
            helper.getMVPInfo(component, event);
            component.set('v.MVPName', event.getSource().get("v.value")); // End of Code - Mb - 07/13/17
            /* Start of Code - 10/18/17 */
        }else if(field.get('v.name') == 'Contract_Start_date__c' || field.get('v.name') == 'Contract_End_Date__c'){
            helper.validateContractDate(component);
        }/* End of Code - 10/18/17 */
    },
    handleAccountLoaded: function(component, event, helper) {
        var fieldVisibleList = component.get('v.fieldVisibleList');
        var fieldList = new Array();

        var container = component.find('formFields');
        helper.buildFormFields(fieldVisibleList, component, container);
    },
    // Start of Code by MB - 08/10/17
    handleServiceNeedChange: function(component, event, helper){
        var serNeedsClass = component.find('SRNeeds');
        console.log(component.get('v.ServiceNeeds'));
        if(component.get('v.ServiceNeeds')){
            $A.util.removeClass(serNeedsClass,'slds-hide');
            $A.util.addClass(serNeedsClass,'slds-show');
        }else{
            $A.util.removeClass(serNeedsClass,'slds-show');
        	$A.util.addClass(serNeedsClass,'slds-hide');
        }
    },// End of Code by MB - 08/10/17
    init: function(component, event, helper) { 
        
        var recordId = component.get("v.recordId");
        var fieldList = '';
		
        var action = component.get("c.IsCurrentUseAdmin");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // debugger
                console.log(response.getReturnValue())
                component.set("v.isCurrentUserAdmin", response.getReturnValue());

            }
        });
        $A.enqueueAction(action);
        
        var fieldAction = component.get("c.getStrategicAccountFieldInfo");
        fieldAction.setParams({ "recordId" : recordId });
        fieldAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                fieldList = response.getReturnValue();
                component.set('v.fieldVisibleList', fieldList);
                // console.log(component.get('v.fieldVisibleList'));
            }
        });
        $A.enqueueAction(fieldAction);

        var action = component.get("c.getAccount");
        action.setParams({ "id" : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.account", response.getReturnValue());
                component.set("v.accountObj", $A.util.json.encode(component.get("v.account")));
                
                helper.checkIsLocked(component, event, helper);
                
                //Start Mohan Chander US-51134/Bug-49897 
                console.log('**response Object: ' + response.getReturnValue()); //Real_Estate_Service_Provider__c, Real_Estate_Service_Provider__r.Name
                console.log('**Real Estate Service Provider Account : ' + response.getReturnValue().Real_Estate_Service_Provider__c);
                if(response.getReturnValue().Real_Estate_Service_Provider__c != undefined){
                    console.log('**Real Estate Service Provider Account Name: ' + response.getReturnValue().Real_Estate_Service_Provider__r.Name);
                }                                 
                var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
                selectedLookUpRecord.Id = response.getReturnValue().Real_Estate_Service_Provider__c; 
                if(response.getReturnValue().Real_Estate_Service_Provider__c != undefined){
                    selectedLookUpRecord.Name = response.getReturnValue().Real_Estate_Service_Provider__r.Name;
                }                                 
                console.log('**selectedLookupRecord ' + selectedLookUpRecord);
                //Open lookup now
                component.set("v.openNow", true);
                console.log(component.get("v.openNow"));
                //End Mohan Chander US-51134/Bug-49897 
            }
        });
        $A.enqueueAction(action);

        
        
        var action = component.get("c.getMVPList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // debugger
                console.log(response.getReturnValue())
                component.set("v.MVPCoordinator", response.getReturnValue());

            }
        });
        $A.enqueueAction(action);                  
    },
    //BY MUDIT
    closeAlertMsg: function(component, event, helper){
        component.set("v.isErrorContractDate",false);
    },
    
     cancel : function(component, event, helper) {

        // Navigate back to the record view
        var navigateEvent = $A.get("e.force:navigateToSObject");
        navigateEvent.setParams({ "recordId": component.get('v.recordId') });
        navigateEvent.fire();
    },
    
    /* Start of Code by MB - 08/10/17 -->
    multipicklist: function(component, event, helper) { 
        
        var val = $('#multiple').val();
        var str = val.toString();
        //var re = /,/g;
        var foo = str.replace(/,/g,";");
        alert('successful: ' + foo);
    }
    // End of Code by MB - 08/10/17 -->*/
    
    //Start Mohan Chander US-51134/Bug-49897 
    toggleLookup : function(component, event, helper) {
		var inputLookup = component.find("inputLookup");
        var inputTextDiv = component.find("inputTextDiv");
        var account = component.get("v.account");
        
        $A.util.addClass(inputTextDiv, "slds-hide");
        $A.util.removeClass(inputLookup, "slds-hide");
        
        console.log("**Real Estate Account Id " + account.Real_Estate_Service_Provider__c);        
    }
    //End Mohan Chander US-51134/Bug-49897 

})