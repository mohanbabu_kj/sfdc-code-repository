({
    createVote : function(component, account) {
        this.upsertVote(component, account, function(a){
            var accs = component.get("v.accountUpdate");
            accs.push(a.getReturnValue());
            console.log('here'+accs);
            component.set("v.accountUpdate", accs);
        });
    },
    
    isNull : function(text){// 03/18/2017
        return (text == undefined || text == '' ? true : false);
    },
    
    setStatusEmpty : function(component){
        component.set('v.statusMessage',  '');
    },
    
    showErrorMessage : function(component, errorObj){// 03/18/2017
        console.dir(errorObj);
        var message = 'Unable to process your request.';
        if (errorObj) {
            if (errorObj[0] && errorObj[0].fieldErrors){
                var fErrors = errorObj[0].fieldErrors;
                for(field in fErrors){
                    //console.log(fErrors[field]);
                    message += ' Reason: Missing ' + field + ' - ' + fErrors[field][0].message;
                }
            }
            if (errorObj[0] && errorObj[0].pageErrors && !this.isNull(errorObj[0].pageErrors[0])) {
                if(!this.isNull(errorObj[0].pageErrors[0].message))
                    message += ' Reason: ' + errorObj[0].pageErrors[0].message;
            }
            // alert(message);
            component.set('v.statusMessage', message);
        } else {
            // alert(message);
            component.set('v.statusMessage', message);
        }
    },
    
    upsertVote : function(component, account, callback){
        
        var action = component.get("c.saveAccount");
        action.setParams({
            "existingAccount": account
        });
        //If upsert of the account is success I want to alert the account Id back to the user.
        action.setCallback(this, function(response) 
                           {
                               var state = response.getState();
                               if (state === "SUCCESS") {
                                   // alert("From server: " + response.getReturnValue());
                                   // console.log(response.getReturnValue())
                                   component.set('v.statusMessage', 'Record saved successfully');
                                   setTimeout(function(){
                                       var dismissActionPanel = $A.get("e.force:closeQuickAction");
                                       dismissActionPanel.fire();
                                       $A.get('e.force:refreshView').fire();
                                   }, 1500);
                               }
                               else if (state === "ERROR") {
                                   this.showErrorMessage(component, response.getError());
                               }
                           });
        $A.enqueueAction(action);
    },
    createApproval : function(component, account) {
        this.upsertApproval(component, account, function(a){
            var accs = component.get("v.accountUpdate");
            accs.push(a.getReturnValue());
            console.log('here'+accs);
            component.set("v.accountUpdate", accs);
        });
    },
    
    upsertApproval : function(component, account, callback){
        
        var action = component.get("c.runApproval");
        action.setParams({
            "approvalAccount": account
        });
        //If upsert of the account is success I want to alert the account Id back to the user.
        action.setCallback(this, function(response) 
                           {
                               var state = response.getState();
                               if (state === "SUCCESS") 
                               {
                                   // alert("From server: " + response.getReturnValue());
                                   // console.log(response.getReturnValue())
                                   component.set('v.statusMessage', 'Record submitted for Approval successfully');
                                   setTimeout(function(){
                                       var dismissActionPanel = $A.get("e.force:closeQuickAction");
                                       dismissActionPanel.fire();
                                       $A.get('e.force:refreshView').fire();
                                   }, 1500);
                               } else if (state === "ERROR") {
                                   this.showErrorMessage(component, response.getError());
                               }
                           });
        $A.enqueueAction(action);
    },
    
    checkIsLocked: function(component, event, helper) {
        console.log('inn');
        var account = component.get("v.account");
        var isLocked = false;              
        
        var lAction = component.get("c.isAccountLocked");
        lAction.setParams({ "id" : component.get("v.recordId") });
        lAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                console.log(response.getReturnValue())
                var result = response.getReturnValue();
                if (account.Strategic_Account__c || result==true) {
                    component.set("v.status", "view");
                } else {
                    component.set("v.status", "edit");
                }
                isLocked = result;
                component.set('v.isAccountLoaded', true);
                component.set('v.isLocked', isLocked);
            } else
                console.log('error')
                });
        $A.enqueueAction(lAction);                 
    },
    
    buildFormFields : function(fieldVisibleList, component, container){
        var fieldType='';
        var fieldData='';
        var chosenFields = new Array();
        
        var compStatus = component.get("v.status");
        var fieldPrefix = (compStatus == 'edit' ? 'ui:input' : 'ui:output');
        var readonly = (compStatus == 'edit' ? false : true);
        
        var accountObj = component.get("v.account");
        // var accountObj = $A.util.json.encode(component.get("v.account"));
        
        var globalId = component.getGlobalId();
        
        var componentType = 'lightning:input';
        console.log(fieldVisibleList);
        console.log(compStatus);
        for(i in fieldVisibleList) {
            fieldData = fieldVisibleList[i];
            fieldType='';
            
            // Start of Code - MB - 08/10/17
            if(fieldData.apiName == 'Service_Needs__c'){
                //component.set('v.ServiceNeeds', 'true');
                /*Start of Comment -MB- 08/22/17
                var serviceNeeds = accountObj[fieldData.apiName];
                var SRoptions = fieldData.options;
                var options = [];
                var selectedValues = [];
                console.log(serviceNeeds);
                if(serviceNeeds != undefined){
                	selectedValues = serviceNeeds.split(";");
                }
                for(i in SRoptions){
                    var selectValue = SRoptions[i];
                    if(serviceNeeds != undefined && serviceNeeds.includes(selectValue)){
                        options.push({
                            label: SRoptions[i],
                            value: SRoptions[i],
                            selected: "true"
                        });
                    }else{
                        options.push({
                            label: SRoptions[i],
                            value: SRoptions[i],
                            selected: "false"
                        });
                    }
                } 
                component.set('v.ServiceNeedsList', options);
                End of Comment -MB- 08/22/17 */
                component.set('v.ServiceNeeds', 'true');
                console.log('Service Needs: ' + component.get('v.ServiceNeeds'));
                //console.log(options);
                //if(component.get('v.isCurrentUserAdmin')){
                if(compStatus == 'view'){
                        component.set('v.SNReadOnly', 'true');
                }else{
                        component.set('v.SNReadOnly', 'false');
                }
                //}else if(compStatus == 'view'){
                  //  component.set('v.SNReadOnly', 'true');
                //}
                console.log('SNReadonly: ' + component.get('v.SNReadOnly'));
                // Start of Code by MB - 08/22/17
                //debugger;
                var SRInput = component.find('SRInput');
                var SROutput = component.find('SROutput');
                if(compStatus == 'view'){
                    $A.util.addClass(SRInput, 'slds-hide');
                    $A.util.removeClass(SRInput, 'slds-show');
                    $A.util.addClass(SROutput, 'slds-show');
                    $A.util.removeClass(SROutput, 'slds-hide');
                }else{
                    $A.util.removeClass(SROutput, 'slds-show');
                    $A.util.addClass(SROutput, 'slds-hide');
                    $A.util.removeClass(SRInput, 'slds-hide');
                    $A.util.addClass(SRInput, 'slds-show');
                }// End of Code by MB - 08/22/17
            }else{
                // End of Code - MB - 08/10/17
                
                switch(fieldData.type){
                    case 'TEXT':
                    case 'STRING':
                        fieldType='text';
                        break;
                    case 'TEXTAREA':
                        fieldType='textarea';
                        break;
                    case 'URL':
                        fieldType='url';
                        break;
                    case 'DATE':
                        fieldType='date';
                        break;
                    case 'DATETIME':
                        fieldType='datetime-local';
                        break;
                    case 'BOOLEAN':
                        fieldType='checkbox';
                        break;
                    case 'PICKLIST':
                    case 'MULTIPICKLIST':
                        fieldType='select';
                        break;
                        // Start of Code - MB - 07/13/17
                    case 'PHONE':
                        fieldType='tel';
                        break; 
                    case 'EMAIL':
                        fieldType='email';
                        break;
                        // End of Code MB - 07/13/17
                    default:
                        break;
                }
                if(fieldType!='' && (chosenFields.length==0 || chosenFields.indexOf(fieldData.apiName)==-1)){
                    var compName = componentType;
                    if(fieldType=='select')
                        compName = 'lightning:select';
                    else if(fieldType=='textarea')
                        compName = 'lightning:textarea';
                        else if(fieldType=='tel')
                            compName = 'ui:outputPhone';
                            else if(fieldType=='email')
                                compName = 'ui:outputEmail';
                    
                    this.genLightField(fieldData, compName, fieldType, component, container, accountObj, readonly);
                }
            } // Added by MB - 08/10/17
        }
    },
    
    genLightField: function(fieldData, componentType, fieldType, component, container, accountObj, readonly) {
        
        var fieldName = fieldData.apiName;
        var compStatus = component.get("v.status"); // Added by MB - 08/07/2017
        
        if(component.find(fieldName)==undefined){
            // console.log(fieldData.apiName, accountObj[fieldData.apiName]);
            if(fieldName == 'Approval_Status__c' || fieldName == 'MVP_Email__c' || fieldName == 'MVP_Phone__c')//  added 04/13/2017 to always show readonly/disabled
                readonly = true;
            // Start of Code - MB - 07/20/17
            //Start of Code - MB - 07/25/17
            if(fieldName=='MVP_Coordinator__c'){
                fieldType = 'select';
                componentType = 'lightning:select';
                // Start of Code by MB - 08/07/2017
                if(component.get('v.isCurrentUserAdmin') == false){
                    readonly = true;
                }else if(component.get('v.isCurrentUserAdmin') == true){
                    if(compStatus == 'view')
                        readonly = true;
                    else
                        readonly = false;
                }
                // End of Code by MB - 08/07/2017
            }
           // Start of Code by Susmitha - Dec 1-2017 for bug 46962
            if(fieldName=='Strategic_Account_Number__c'){
                if(component.get('v.isCurrentUserAdmin') == false){
                    readonly = true;
                }else if(component.get('v.isCurrentUserAdmin') == true){
                    if(compStatus == 'view')
                        readonly = true;
                    else
                        readonly = false;
                }
            }
           // End of Code by Susmitha - Dec 1-2017 for bug 46962

            //End of Code - MB - 07/25/17
            /*Start of Code - MB - 07/26/17
            if(fieldName=='Service_Needs__c'){
                componentType = 'ui:inputSelect';
            }
            //End of Code - MB - 07/26/17*/
            
            /* Start of Code - Rocky - 08/02/17 
            if(fieldName=='Service_Needs__c'){
                fieldType = 'select';
                componentType = 'lightning:select';
            }
            /* End of Code - Rocky - 08/02/17 */
          var attri =   {
                    'name': fieldName,
                    'type': fieldType,
                    'class': 'input-fields', 
                    'readonly': readonly,
                    'disabled': ((fieldType == 'select' || fieldType == 'checkbox') && readonly ? true : false),
                    //'multiple': (fieldData.type=='MULTIPICKLIST'? true : false),
                    //'multiple': (fieldName=='Service_Needs__c'? true : false), // Added by MB - 07/26/17
                    'onchange': ((fieldType == 'select' && fieldName == 'MVP_Coordinator__c') || (fieldType == 'date' && (fieldName == 'Contract_Start_date__c' || fieldName == 'Contract_End_Date__c' )) )? component.getReference("c.handleFieldChange") : '', // Added by MB - 07/13/17
                    'checked': accountObj[fieldName]?accountObj[fieldName]:false,
                    'value': accountObj[fieldName]?accountObj[fieldName]:'',
                    'required': (fieldName == 'Account_Geography__c' || fieldName == 'Commission_Type_Core_Only__c'
                                 || fieldName == 'Facilities__c') ? true : false, // Added by MB - 07/24/17
                    'aura:id': fieldName
                    //'id': fieldName // Added by MB - 07/20/17
                };
            //Start Added by LJ on Jan 9th 2018
            if(fieldType !='tel' && fieldType !='email')
            {
                console.log("attri : 2"+fieldType);
                attri['label'] = fieldData.fieldLabel;
                console.log("attri :Inside ");
            }
			//End Added by LJ on Jan 9th 2018
            $A.createComponent(
                componentType,
                attri,
                function(element, status, errorMessage){
                    if(status == 'SUCCESS'){
                        $A.createComponent('div', 
                                           { class: 'slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2'}, 
                                           function(wrapElement, wrapStatus, error){
                                               if(wrapStatus == 'SUCCESS'){
                                                   var wrapBody = wrapElement.get('v.body');
                                                   if(fieldType == 'select'){
                                                       element.set('v.body', []);
                                                       selectElem = element.get('v.body');
                                                       //var selectOptions = fieldData.options; - Commented by MB - 07/25/17
                                                       //Start of Code - MB - 07/25/17
                                                       var selectOptions = [];
                                                       if(fieldName == 'MVP_Coordinator__c'){
                                                           selectOptions = component.get('v.MVPCoordinator');
                                                       }else{
                                                           selectOptions = fieldData.options;
                                                       }//End of Code - MB - 07/25/17
                                                       console.log(fieldName, selectOptions);
                                                       //if(fieldName != 'Service_Needs__c'){ //Added by MB - 07/26/17
                                                       //Start of Code - MB - 07/05/17 - To Add blank value for select
                                                       $A.createComponent('aura:html', { tag: 'option', HTMLAttributes: { value: '', text: '' }},
                                                                          function(newOption){
                                                                              if(newOption){
                                                                                  selectElem.push(newOption);
                                                                                  element.set('v.body', selectElem);
                                                                              }
                                                                          });
                                                       //End of Code - MB - 07/05/17
                                                       selectOptions.forEach(function (opt) {
                                                           $A.createComponent('aura:html', { tag: 'option', HTMLAttributes: { value: opt, text: opt }},
                                                                              function(newOption){
                                                                                  if(newOption){
                                                                                      selectElem.push(newOption);
                                                                                      element.set('v.body', selectElem);
                                                                                  }
                                                                              });
                                                       });
                                                       /*Start of Code - MB - 07/26/17    
                                                       }else{
                                                           selectOptions.forEach(function (opt) {
                                                               $A.createComponent('aura:html', { tag: 'option', HTMLAttributes: { label: opt, value: opt }},
                                                                              function(newOption){
                                                                                  if(newOption){
                                                                                      selectElem.push(newOption);
                                                                                      element.set('v.body', selectElem);
                                                                                      console.log('multiselect created', selectElem);
                                                                                  }
                                                                              });
                                                       });
                                                       }//End of Code - MB - 07/26/17*/
                                                       //Commented by MB - 07/05/17
                                                       //element.set('v.value', accountObj[fieldName]?accountObj[fieldName]:selectOptions[0]);
                                                       element.set('v.value', accountObj[fieldName]?accountObj[fieldName]:''); //Added by MB - 07/05/17
                                                       //Added by MB - 07/14/17
                                                   }
                                                   if(fieldType == 'tel' || fieldType == 'email'){
                                                       $A.createComponent('ui:outputText', { class: 'custom-label slds-form-element__label', value: fieldData.fieldLabel }, 
                                                                          function(labelElem, labelSts, labelErr) {
                                                                              if(labelSts == 'SUCCESS') {
                                                                                  console.log('custom-label created', labelElem);
                                                                                  wrapBody.push(labelElem);
                                                                              }
                                                                          });
                                                   }
                                                   wrapBody.push(element);
                                                   wrapElement.set('v.body', wrapBody);
                                                   var body = container.get('v.body');
                                                   body.push(wrapElement);
                                                   container.set('v.body', body);
                                                   console.log(container.get('v.body'));
                                                   console.log(wrapStatus);
                                                   //}
                                               }
                                           }
                                          );
                        
                    } else
                        console.log(errorMessage)
                        }
            );
        }
    },
    
    getFormData: function(component) {
        var fieldVisibleList = component.get('v.fieldVisibleList');
        
        var accountObj = {};
        jQuery('.input-fields .slds-input, .input-fields .slds-textarea, .input-fields .slds-select, .input-fields input[type="checkbox"]').each(function(){
            var elem = jQuery(this);
            accountObj[elem.attr('name')] = elem.attr('type')=='checkbox'?elem.is(':checked'):elem.val();
        });
        accountObj['sobjectType'] = 'Account';
        // accountObj['Strategic_Account__c'] = component.get('v.account.Strategic_Account__c');
        accountObj['Id'] = component.get("v.recordId");
        accountObj['MVP_Phone__c'] = component.get("MVPPhone");
        accountObj['MVP_Email__c'] = component.get("MVPEmail");
        /* Start of Comment - MB - 08/22/17
        //Start of Code -MB- 08/10/17
        var str = $('#multiple').val();
        if(str != undefined){
            var selectedSR = str.toString();
            selectedSR = selectedSR.replace(/,/g, ";");
            accountObj['Service_Needs__c'] = selectedSR;
        }
        //End of Code -MB- 08/10/17
        End of Comment -MB- 08/22/17 */
        console.log(accountObj);
        return accountObj;
    },
    
    // added by MB - 07/13/17
    getMVPInfo: function(component, event){
        var fieldAction = component.get("c.getMVPInfo");
        var accobj = component.get("v.accountObj");
        fieldAction.setParams({ "MVPName" : event.getSource().get("v.value") });
        fieldAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                console.log(response.getReturnValue());
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.MVPPhone", result.MVP_Phone__c);
                component.set("v.MVPEmail", result.MVP_Email__c);
            } else
                console.log('error')
                });
        $A.enqueueAction(fieldAction);
    }, // End of code - MB - 07/13/17
    // Start of Code -MB- 10/18/17
    validateContractDate: function(component, accountObj){
        //debugger;
        if(accountObj == undefined){
        	accountObj = this.getFormData(component);
        }
        console.log(accountObj);
        if(accountObj.Contract_Start_date__c != null && accountObj.Contract_Start_date__c != '' &&
           accountObj.Contract_End_Date__c != null && accountObj.Contract_End_Date__c != '' &&
           accountObj.Contract_End_Date__c < accountObj.Contract_Start_date__c){
            component.set('v.statusMessage', 'Contract End Date should be greater than Contract Start Date');
            //alert('Contract End Date should be greater than Contract Start Date');
            
            //Edited By Lakshman 
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Contract End Date should be greater than Contract Start Date');
            component.set('v.errorOccurred', true);
            //console.log(component.get('v.statusMessage'));
        }else{
            component.set('v.errorOccurred', false);
        }
        if((accountObj.Contract_Start_date__c == null || accountObj.Contract_Start_date__c == '') &&
           accountObj.Contract_End_Date__c != null && accountObj.Contract_End_Date__c != ''){
            //alert('Please enter Contract Start Date');
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Please enter Contract Start Date');
            component.set('v.errorOccurred', true);
        }
        //Removed By Lakshman  
        /*else{
            component.set('v.errorOccurred', false);
        }*/
        
    },
    // End of Code -MB- 10/18/17
    
    
})