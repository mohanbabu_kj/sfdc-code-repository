/**
 * Created by rchen180 on 17/4/18.
 */
({
    doInit : function (component, event, helper) {
      helper.getRecordName(component, event);
    },
    showSearch : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:ChangeOwner",
            componentAttributes: {
                recordId : component.get('v.recordId'),
                territoryValue : component.get('v.territoryValue')
            }
        });
        evt.fire();
    }
})