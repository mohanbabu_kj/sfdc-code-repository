//Controller
({
    doInit: function(component, event, helper) {
        var options = component.get("v.options");
        if(options && options.length > 0){
            options.sort(function compare(a,b) {
                if (a.value == 'All'){
                    return -1;
                }
                else if (a.value < b.value){
                    return -1;
                }
                if (a.value > b.value){
                    return 1;
                }
                return 0;
            });
        }else{
            options = [];
        }
        component.set("v.options_",options);
        var _values = helper.getSelectedLabels(component);
        helper.setInfoText(component,_values);
        helper.initialized = true;
    },

    handleFocus: function(component, event, helper) {
        console.log('on focus');
        helper.initialized = false;
        component.set("v.showDropdown",true);
    },

    handleSelection: function(component, event, helper) {
        //
        component.find("inputBox").focus();
        //
        var item = event.currentTarget;
        helper.initialized = true;
        if (item && item.dataset) {
            var value = item.dataset.value;
            var selected = item.dataset.selected;

            var options = component.get("v.options_");

            options.forEach(function(element) {
                if (element.value == value) {
                    element.selected = selected == "true" ? false : true;
                }
            });
            component.set("v.showDropdown",true);
            component.set("v.options_", options);
            var _values = helper.getSelectedValues(component);
            var _labels = helper.getSelectedLabels(component);
            helper.setInfoText(component,_labels);
            helper.despatchSelectChangeEvent(component,_values);
        }
    },

    handleMouseLeave: function(component, event, helper) {
        console.log('on handleMouseLeave');
        
        component.set("v.showDropdown",false);
        
        //component.set("v.showDropdown",false);
    },
    
    handleMouseEnter: function(component, event, helper) {
        console.log('on handleMouseEnter');
        clearTimeout(helper.timeoutAction);
        component.set("v.showDropdown",true);
    },

    handleOffFocus: function(component, event, helper) {
         console.log('on handleOffFocus');
        helper.initialized = false;
        helper.timeoutAction = window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid() && !helper.initialized) {
                    component.set("v.showDropdown",false);
                }
                
            }), 200
        );
    }
})