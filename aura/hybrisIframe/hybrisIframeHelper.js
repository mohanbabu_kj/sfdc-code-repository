({
	getObjectName : function(component) {// not in use
		var paramList = component.get('v.paramList');
		var objAction = component.get('c.getObjectNameById');
        // console.log(paramList)
        objAction.setParams({
            recordId: paramList['recordId']
        });
        objAction.setCallback(this, function(response){
            if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                return response.getReturnValue();
            }
        });
        $A.enqueueAction(objAction);
	},
	buildQueryStringForUrl : function(resultSet, component) {
		var url = resultSet.Url__c;
		var queryString = resultSet.Query_String__c;
		
		var paramList = component.get('v.paramList');
        // console.log(paramList)
		if(queryString && queryString!=''){
            // console.log(queryString, '--')
            if(resultSet.No_Query_String__c == true){
                var qryStrArr = queryString.split(';;');
                var urlTemp = url;
                for(var i=0;i<qryStrArr.length;i++){
                    if(qryStrArr[i]!=''){
                        var valueArr = qryStrArr[i].split('=');
                        // console.log(valueArr)
                        if(urlTemp.indexOf(valueArr[0])!=-1){
                            // console.log(paramList[valueArr[1]])
                            urlTemp = urlTemp.replace(valueArr[0], paramList[valueArr[1]])
                            console.log(urlTemp)
                        }
                    }
                }
                component.set('v.IframeUrl', urlTemp);
                // component.set('v.IframeUrl', url.replace('RECORD_ID', paramList['recordId']));
            } else {
    	        if(queryString.indexOf('=RECORD_ID')!=-1){
                // if(url.indexOf('=RECORD_ID')!=-1){
                    // console.log('record id', paramList['recordId'], url);
                    /*url = url.replace('=RECORD_ID', '='+paramList['recordId']);
                    console.log('record id', paramList['recordId'], url);*/
                    queryString = queryString.replace('=RECORD_ID', '='+paramList['recordId']);
                }
                if(queryString.indexOf('RECORD_ID')!=-1){
    	            queryString = queryString.replace('RECORD_ID', '='+paramList['recordId']);
    	        }
                component.set('v.IframeUrl', url + (queryString && queryString!='' ? (url.indexOf('?')!=-1?'&':'?')+queryString : ''));
            }
    	} else 
            component.set('v.IframeUrl', url);
        // console.log(component.get('v.IframeUrl'));
        
        // component.set('v.IframeUrl', url + (queryString && queryString!='' ? (url.indexOf('?')!=-1?'&':'?')+queryString : ''));
        /*var objAction = component.get('c.getObjectNameById');
        console.log(paramList['recordId'])
        objAction.setParams({
            recordId: paramList['recordId']
        });
        objAction.setCallback(this, function(response){
            if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                console.log( response.getReturnValue() );
            }
        });
        $A.enqueueAction(objAction);*/
	}
})