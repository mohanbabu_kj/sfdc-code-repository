({
    doDataCellInit : function(component, event, helper) {
        var _map = component.get("v.record");
        var _key = component.get("v.field");
        var _type = component.get("v.type");
        var _outputCmp = 'aura:unescapedHtml';
        var _isKeyHierarchial = component.get("v.isKeyHierarchial");
        if(_map && _key){
            if(_isKeyHierarchial == true){
                var keyArray = [];
                if(component.get("v.isPrimaryGrid")){
                    while(_key.indexOf('.') > -1){
                        keyArray = _key.split('.');
                        _map = _map[keyArray[0]];
                        _key = keyArray[1];
                    }
                }
                
            }
            //if(_map && _key && _map[_key]){ // Commented to fix bug #62756 ("_map[_key]" was escaping "0" and 'false' values)
            if(_map && _key){
                var _attributeMap = {};
                _attributeMap['value'] = _map[_key];
                if (component.get("v.isSecondaryLink")){
                    //var _anchorAttributes = {};
                    //_anchorAttributes['id'] = 'link-' + _map.Id;
                    //_attributeMap['onclick'] = component.getReference("c.doSecondaryDataDisplay");
                    //_anchorAttributes['tag'] = 'a';
                    _attributeMap['class'] = 'link-secondary slds-text-align_right';
                    //_attributeMap['label'] = _map[_key];
                    //_outputCmp = 'lightning:button';
                }
                else if(_type == 'Currency'){
                    _outputCmp = 'ui:outputCurrency';
                }
                else if(_type == 'Currency Integer'){
                    _outputCmp = 'ui:outputCurrency';
                    _attributeMap['format'] = '$##,##,###,###.';
                }
                else if(_type == 'Number'){
                    _outputCmp = 'ui:outputNumber';
                }
                    else if(_type == 'Boolean'){
                        _outputCmp = 'ui:outputCheckbox';
                    }
                        else if(_type == 'Date'){
                            //_outputCmp = 'ui:outputDate';
                            if (_attributeMap['value'] == '4000-12-31')
                                _attributeMap['value'] = '';
                            else
                            	_attributeMap['value'] = $A.localizationService.formatDate(_attributeMap['value'],'M/d/yy');;
                        }
                            else if(_key == 'Indicators__c'){
                                _attributeMap['class'] = 'indicator-box';
                            }
                                else{
                //var start = performance.now();
                                    //component.set("v.body", '<lightning-formatted-text>' + _map[_key] + '</lightning-formatted-text>');
                        //var elapsed = performance.now() - start;
                        //console.log(elapsed);
                                }
                var cmpt;
                //cmpts.push(['aura:html',{'tag': 'td','scope': 'row'}]);
                //cmpts.push(['aura:html',{'tag': 'div','class': 'slds-text-align_right slds-truncate','style': 'font-size:16px'}]);
                cmpt = [_outputCmp,_attributeMap];
                /*if (component.get("v.isSecondaryLink")){
                    var _anchorAttributes = {};
                    //_anchorAttributes['id'] = 'link-' + _map.Id;
                    _anchorAttributes['onclick'] = component.getReference("c.doSecondaryDataDisplay");
                    //_anchorAttributes['tag'] = 'a';
                    _anchorAttributes['class'] = 'link-secondary slds-text-align_right';
                    _anchorAttributes['label'] = _map[_key];
                    cmpt = ['lightning:button',_anchorAttributes];
                }*/
                $A.createComponent(
                    cmpt[0],
                    cmpt[1],
                    function(cmp, status, errorMessage){
                        if (status === "SUCCESS") {
                            var body = component.get("v.body");
                            //var tdCmp = components[0];
                            //var divCmp = components[1];
                            //var dataCmp = components[0];
                            //if (components.length > 1){
                                //var anchorCmp = components[1];
                                //anchorCmp.set('v.body',dataCmp);
                            	//body.push(anchorCmp);
                                //divCmp.set('v.body',anchorCmp);
                            //}
                           // else{
                                //divCmp.set('v.body',dataCmp);
                            	body.push(cmp);
                            //}
                            //tdCmp.set('v.body',divCmp);
                            component.set("v.body", body);
                        }else{
                            console.log(errorMessage);
                        }
                    }
                )
            }
        }
    },
                    
    doSecondaryDataDisplay: function(component, event, helper){
        
        console.log('### Grid_DataCellController - doSecondaryDataDisplay');
        
        if (component.get("v.isSecondaryLink")){
            
            var recordMap = component.get("v.record");
            var sellingStyleNum = '';
            var sellingStyleName = '';
            var inventoryStyleNum = '';
            var inventoryStyleName = '';
            var productId = ''; //Added by MB - 07/30/18 - Bug 63005
            var _prodCatName = component.get("v.productCategoryName");

            console.log('### _prodCatName: ' + component.get("v.productCategoryName"));
            console.log('### gridType: ' + component.get("v.gridType"));
            console.log('### accountId: ' + component.get("v.accountId"));
            console.log('### objectName: ' + component.get("v.sObjectName"));
            console.log('### _prodCatId: ' + component.get("v.productCategoryId"));
            
            
            if (component.get("v.gridType") == 'Price_Grid' && component.get("v.productCategoryName") == 'Cushion') {
                //sellingStyleNum = recordMap['Inventory_Style_Num__c'];
                //sellingStyleName = recordMap['Product__r'].Inventory_Style_Name__c;
                
                inventoryStyleNum = recordMap['Inventory_Style_Num__c'];
                inventoryStyleName = recordMap['Product__r'].Inventory_Style_Name__c;
            }
            else {
                sellingStyleNum = recordMap['Product__r'].Product_Style_Number__c;
                sellingStyleName = recordMap['Product__r'].Name;
            }
            
            console.log('### sellingStyleNum: ' + sellingStyleNum);
            console.log('### sellingStyleName: ' + sellingStyleName);
            productId = recordMap['Product__r'].Id; //Added by MB - 07/30/18 - Bug 63005
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:Grid_DataSecondaryView",
                componentAttributes: {
                    sObjectName : component.get("v.sObjectName"),
                    recordId: component.get("v.recordId"),
                    record: recordMap,
                    productCategoryId: component.get("v.productCategoryId"),
                    productCategoryName: _prodCatName,
                    warehouseValue: component.get("v.warehouseValue"),
                    sellingStyleNum: sellingStyleNum,
                    sellingStyleName: sellingStyleName,
                    inventoryStyleNum: inventoryStyleNum,
                    inventoryStyleName: inventoryStyleName,
                    gridType: component.get("v.gridType"),
                    accountId: component.get("v.accountId"),
                    productId: productId,		//Added by MB - 07/30/18 - Bug 63005
                    header: sellingStyleName + ' - ' + sellingStyleNum,
                    gridHeader: sellingStyleName + ' (' + sellingStyleNum + ')'
                }
            });
            evt.fire();
        }
    }
})