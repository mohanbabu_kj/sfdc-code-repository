({
    doInitHelper : function (component, event, helper) {
        var action = component.get('c.initalAssessments'),recordId=component.get('v.recordId');
        action.setParams(
            { 'recordId' : recordId }
        );
        action.setCallback(this, function( resp ) {
            if (component.isValid() && resp.getState() === "SUCCESS") {
                var result = resp.getReturnValue();
                component.set('v.initialResults', result);
                component.set('v.productRecordId', result.ProductType);
                component.set('v.marketRecordId', result.ShareType);
                component.set('v.selectMap', { segment:result.Segement, brand: result.Brand } );
                
                var device = $A.get("$Browser.formFactor");
                if( device == 'DESKTOP' ){
                    //RENDRING PRODUCT TABLE 
                    component.set("v.maxPageProduct", Math.floor((component.get("v.initialResults.Product").length+9)/component.get("v.numOfRecordProduct")));
                    this.renderProductTable(component,component.get("v.segmentValue"));
                    //RENDRING PRODUCT TABLE 
                    
                    //RENDRING MARKET TABLE  
                    component.set("v.maxPageMarket", Math.floor((component.get("v.initialResults.Market").length+9)/component.get("v.numOfRecordMarket")));
                    this.renderMarketTable(component);
                    //RENDRING MARKET TABLE
                }else{
                    //RENDRING PRODUCT TABLE 
                    component.set("v.maxPageProduct", Math.floor((component.get("v.initialResults.Product").length+9)/10));
                    this.renderProductTable(component,component.get("v.segmentValue"));
                    //RENDRING PRODUCT TABLE
                    
                    //RENDRING MARKET TABLE
                    component.set("v.maxPageMarket", Math.floor((component.get("v.initialResults.Market").length+9)/10));
                    this.renderMarketTable(component);
                    //RENDRING MARKET TABLE
                }
                component.set('v.spinner',false);
            }
        });
        $A.enqueueAction(action); 
        this.gettingRecordTypeIds(component, 'Competitor_Products__c',component.get("v.segmentValue"),'CompetitorProductRecordTypeId');
        this.gettingRecordTypeIds(component, 'Account','Competitors','accountRecordTypeName');
        this.gettingRecordTypeIds(component, 'Product2','Residential Product','ProductRecordTypeName');
    },
    getFilterList: function(component, selectedSegment, selectedBrand){
        var recordId = component.get('v.recordId'),action = component.get('c.getFilterList');
        if(selectedSegment == 'All'){
            selectedSegment = '';
        }
        if(selectedBrand == 'All'){
            selectedBrand='';
        }
        action.setParams({
            recordId:recordId,
            brand:selectedBrand,
            segement:selectedSegment
        });
        
        action.setCallback(this, function(resp) {
            console.log(resp.getReturnValue());
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                component.set('v.initialResults', result);
                var device = $A.get("$Browser.formFactor");
                if( device == 'DESKTOP' ){
                    //RENDRING PRODUCT TABLE 
                    component.set("v.maxPageProduct", Math.floor((component.get("v.initialResults.Product").length+9)/component.get("v.numOfRecordProduct")));
                    this.renderProductTable(component,component.get("v.segmentValue"));
                    //RENDRING PRODUCT TABLE 
                    
                    //RENDRING MARKET TABLE  
                    component.set("v.maxPageMarket", Math.floor((component.get("v.initialResults.Market").length+9)/component.get("v.numOfRecordMarket")));
                    this.renderMarketTable(component);
                    //RENDRING MARKET TABLE
                }else{
                    //RENDRING PRODUCT TABLE 
                    component.set("v.maxPageProduct", Math.floor((component.get("v.initialResults.Product").length+9)/10));
                    this.renderProductTable(component,component.get("v.segmentValue"));
                    //RENDRING PRODUCT TABLE
                    
                    //RENDRING MARKET TABLE
                    component.set("v.maxPageMarket", Math.floor((component.get("v.initialResults.Market").length+9)/10));
                    this.renderMarketTable(component);
                    //RENDRING MARKET TABLE
                }
                component.set('v.spinner',false);
            }
        });
        $A.enqueueAction(action);
        this.gettingRecordTypeIds(component, 'Competitor_Products__c',component.get("v.segmentValue"),'CompetitorProductRecordTypeId');
    },
    gettingRecordTypeIds : function (component, objectName,RTName,setValueIn) {
        var action = component.get('c.getrecordtypeIdforAll');
        action.setParams({
            'objectAPIName':objectName,
            'RecordTypeName': RTName
        });
        action.setCallback( this, function( response ) {
            if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                component.set( "v."+setValueIn , response.getReturnValue() );
                
            }
        });
        $A.enqueueAction(action);
    },
    createProductTableDataHelper : function(component,segmentValue,data) {
        var tableData = [];
        var dataFromApex = data;
        console.log( dataFromApex );
        if( dataFromApex.length > 0 ){
            for( var i=0;dataFromApex.length > i;i++ ){
                if(  dataFromApex[i].Competitor_Product__c  != null ){

                var CompetitorVal;   
                if( this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__c ) != '' ){
                    CompetitorVal = this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__r.Name );
                }
                
                if( segmentValue == 'Carpet' ){
                    
                    tableData.push(
                        {
                            Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name )+'-'+this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Style_Number__c ),
                            ProductId:this.ternaryCheck( dataFromApex[i].Competitor_Product__c ),
                            //Competitor:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__r.Name ),
                            Competitor:CompetitorVal,
                            CompetitorId:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__c ),
                            Category:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Category__c ),
                            Fiber:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Fiber__c ),//Fiber__c
                            Construction:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Construction__c ),
                            Weight:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Weight__c ),//weight__c
                            Price:this.ternaryCheck( dataFromApex[i].Price__c ),
                            Velocity:this.ternaryCheck( dataFromApex[i].Velocity__c )
                        }
                    );
                }
                if( segmentValue == 'Cushion' ){
                    tableData.push(
                        {
                            Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name )+'-'+this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Style_Number__c ),
							//Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name ),
                            ProductId:this.ternaryCheck( dataFromApex[i].Competitor_Product__c ),
                            //Competitor:dataFromApex[i].Competitor_Product__r.Competitor__r.Name,
                            Competitor:CompetitorVal,
                            CompetitorId:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__c ),
                            Category:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Category__c ),
                            Thickness:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Thickness__c ),
                            Density:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Density__c ),
                            //Type:this.ternaryCheck( dataFromApex[i].Mohawk_Product_Type__c ),
                            Moisture_Barrier:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Moisture_Barrier__c ),
                            Antimicrobial:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Antimicrobial__c ),
                            Price:this.ternaryCheck( dataFromApex[i].Price__c ),
                            Velocity:this.ternaryCheck( dataFromApex[i].Velocity__c )
                        }
                    );
                }
                if( segmentValue == 'Hardwood' ){
                    tableData.push(
                        {
                            Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name )+'-'+this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Style_Number__c ),
                           // Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name ),
                            ProductId:this.ternaryCheck( dataFromApex[i].Competitor_Product__c ),
                            //Competitor:dataFromApex[i].Competitor_Product__r.Competitor__r.Name,
                            Competitor:CompetitorVal,
                            CompetitorId:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__c ),
                            Construction:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Construction__c ),
                            Width:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Width__c ),
                            Thickness:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Thickness__c ),
                            Species:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Species__c ),
                            Texture:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Texture__c ),
                            Price:this.ternaryCheck( dataFromApex[i].Price__c ),
                            Velocity:this.ternaryCheck( dataFromApex[i].Velocity__c )
                        }
                    );				
                }
                if( segmentValue == 'Tile' ){
                    tableData.push(
                        {
                            Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name )+'-'+this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Style_Number__c ),
                           // Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name ),
                            ProductId:this.ternaryCheck( dataFromApex[i].Competitor_Product__c ),
                            //Competitor:dataFromApex[i].Competitor_Product__r.Competitor__r.Name,
                            Competitor:CompetitorVal,
                            CompetitorId:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__c ),
                            //Type:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Mohawk_Product_Type__c ),
                            Category:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Category__c ),
                            Application:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Application__c ),
                            Size:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Size__c ),
                            Price:this.ternaryCheck( dataFromApex[i].Price__c ),
                            Velocity:this.ternaryCheck( dataFromApex[i].Velocity__c )
                        }
                    );
                }
                if( segmentValue == 'Laminate' ){
                    tableData.push(
                        {
                            Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name )+'-'+this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Style_Number__c ),
							//Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name ),
                            ProductId:this.ternaryCheck( dataFromApex[i].Competitor_Product__c ),
                            //Competitor:dataFromApex[i].Competitor_Product__r.Competitor__r.Name,
                            Competitor:CompetitorVal,
                            CompetitorId:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__c ),
                            Thickness:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Thickness__c ),
                            Species:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Species__c ),
                            Plank_design:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Plank_Design__c ),
                            //Locking_System:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Locking_System__c ),
                            Appearance:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Appearance__c ),
                            Price:this.ternaryCheck( dataFromApex[i].Price__c ),
                            Velocity:this.ternaryCheck( dataFromApex[i].Velocity__c )
                        }
                    );
                }
                if( segmentValue == 'Resilient' ){
                    tableData.push(
                        {
                            Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name )+'-'+this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Style_Number__c ),
                          //  Product:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Name ),
                            ProductId:this.ternaryCheck( dataFromApex[i].Competitor_Product__c ),
                            //Competitor:dataFromApex[i].Competitor_Product__r.Competitor__r.Name,
                            Competitor:CompetitorVal,
                            CompetitorId:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Competitor__c ),
                            Category:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Category__c ),
                            Gauge:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Gauge__c ),
                            Wear_Layer:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Wear_Layer__c ),
                            //Thickness:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Thickness__c ),
                            //Appearance:this.ternaryCheck( dataFromApex[i].Competitor_Product__r.Surface_Appearance__c ),
                            Price:this.ternaryCheck( dataFromApex[i].Price__c ),
                            Velocity:this.ternaryCheck( dataFromApex[i].Velocity__c )
                        }
                    );
                }
                }
            }
        }
        console.log( tableData );
        return tableData;
    },
    renderProductTable: function(component,segmentValue) {
        var productTableData = this.createProductTableDataHelper(component,segmentValue,component.get("v.initialResults.Product"));
        var pageNumber = component.get("v.pageNumberProduct");
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP'){
            var pageRecords = productTableData.slice((pageNumber-1)*component.get("v.numOfRecordProduct"), pageNumber*component.get("v.numOfRecordProduct"));    
            component.set("v.currentProductList", pageRecords);
        }else if ( device == 'TABLET' ){
            var pageRecords = productTableData.slice((pageNumber-1)*10, pageNumber*10);  
            component.set("v.currentProductList", pageRecords);
        }else{
            component.set("v.currentProductList", productTableData);
        }
    },
    createMarketTableHelper : function(component,data) {
        var tableData = [];
        var dataFromApex = data;
        if( dataFromApex.length > 0 ){
            for( var i=0;dataFromApex.length > i;i++ ){
                tableData.push(
                    {
                        
                        Competitor: dataFromApex[i].Competitor__r !== undefined ? dataFromApex[i].Competitor__r.Name : "",
                        CompetitorId: dataFromApex[i].Competitor__c,
                        Share:dataFromApex[i].Competitor_Share__c,
                        Estimated_Sales:dataFromApex[i].Competitor_Estimated_Sales__c
                    }
                );
            }
        }
        console.log( tableData );
        return tableData;
    },
    renderMarketTable:function(component){
        var marketTableData = this.createMarketTableHelper(component,component.get("v.initialResults.Market"));
        var pageNumber = component.get("v.pageNumberMarket");
        var device = $A.get("$Browser.formFactor");
        /*if( device == 'DESKTOP' ){
            var pageRecords = marketTableData.slice((pageNumber-1)*10, pageNumber*10);
            component.set("v.currentMarketList", pageRecords);
        }else{
            component.set("v.currentMarketList", marketTableData);
        }*/
        if( device == 'DESKTOP'){
            var pageRecords = marketTableData.slice((pageNumber-1)*component.get("v.numOfRecordMarket"), pageNumber*component.get("v.numOfRecordMarket"));    
            component.set("v.currentMarketList", pageRecords);
        }else if ( device == 'TABLET' ){
            var pageRecords = marketTableData.slice((pageNumber-1)*10, pageNumber*10);  
            component.set("v.currentMarketList", pageRecords);
        }else{
            component.set("v.currentMarketList", marketTableData);
        }
    },
    sortData: function ( component, field,data,setValueIn,sortVariable,sortFieldName ) {
        //var data = component.get("v.currentProductList");  
        var sortAsc = component.get( "v."+sortVariable ),
            sortField = component.get("v." + sortFieldName);
        sortAsc = sortField != field || !sortAsc;
        data.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        component.set("v."+sortVariable, sortAsc);
        component.set("v."+sortFieldName, field);
        component.set("v."+setValueIn, data);
    },
    showPopupHelper: function ( component,setValueIn ) {
        component.set("v."+setValueIn,true);
    },
    hidePopupHelper: function ( component,setValueIn,modalId ) {
        this.clearData( component,modalId );
        component.set("v."+setValueIn,false);
        var device = $A.get("$Browser.formFactor");
        if( device != 'DESKTOP' ){
            var desktop = component.find('mainDiv');
            $A.util.removeClass(desktop, 'slds-hide');
            $A.util.addClass(desktop, 'slds-show');
            
            var mobile = component.find('mobileDiv');
            $A.util.removeClass(mobile, 'slds-show');
            $A.util.addClass(mobile, 'slds-hide');
        }
    },
    clearData: function ( component,clearValueIn ) {
        component.find( clearValueIn ).set("v.body",[]);
    },
    createNewCompetitorProductHelper : function(component, event, helper) {
        var device = $A.get("$Browser.formFactor");
        //NEW CHANGES
        if( device != 'DESKTOP' ){
            var mobile = component.find('mobileDiv');
            $A.util.removeClass(mobile, 'slds-show');
            $A.util.addClass(mobile, 'slds-hide');
        }
        //NEW CHANGES
        component.set("v.CompetitorProductModalToggle",true);
    },
    saveProductHelper: function (component, event, helper) {
        console.log(component.get('v.selectedLookUpRecord1Product'));
        var inputDetails = component.get('v.addProductDetails'),
            recordId = component.get('v.productRecordId'),
            product = component.get('v.selectedLookUpRecord1Product.val'),
            productType = component.get('v.segmentValue'),
            accId = component.get('v.recordId');
        
        if (productType.length == 0) {
            productType = 'Carpet';
        }
        
        var action = component.get("c.insertAssessment");
        if( !product ){
            component.set('v.errMsg1', 'Product is Required !');
            component.set('v.spinner',false);
            return;
        }
        if( inputDetails.Price == null || inputDetails.Price == '' || inputDetails.Price == 0 || inputDetails.Price == '0' ){
            component.set('v.errMsg1', 'Price is Required !');
            component.set('v.spinner',false);
            return;
        }
        if( inputDetails.Velocity == null || inputDetails.Velocity == '' || inputDetails.Velocity == 0 || inputDetails.Velocity == '0' ){
            component.set('v.errMsg1', 'Velocity is Required !');
            component.set('v.spinner',false);
            return;
        }
        var postOfferData = {
            newAss: {
                "RecordTypeId": recordId,
                "Price__c": inputDetails.Price,
                "Velocity__c": inputDetails.Velocity,
                "Competitor_Product__c": product,
                "sobjectType": "Competitive_Assessment__c",
                "Product_Type__c": productType
            },recordId : accId
        };
        action.setParams(postOfferData);
        action.setCallback(this, function (resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                if (result === "SUCCESS") {
                    var seg = component.get('v.segmentValue');
                    component.set('v.errMsg1', '');
                    component.set("v.addProductDetails.Price",'');
                    component.set("v.addProductDetails.Velocity",'');
                    component.set("v.selectedLookUpRecord1Product",[]);
                    var device = $A.get("$Browser.formFactor");
        			if( device == 'DESKTOP' ){
                    	this.hidePopupHelper( component,'newProductModal','newModalProduct' );
                    }else{
                        console.log( 'MOBILE HERE' );
                        this.hidePopupHelper( component,'newProductModalMobile','newModalProductMobile' );
                    }
                    this.getFilterList(component, seg, '');
                    component.set('v.errMsg1', '');
                }else{
                    component.set('v.errMsg1', result);
                    component.set('v.spinner',false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveMarketHelper:function(component, event,helper){
        var inputDetails = component.get('v.addMarketDetails'),
            competitor = component.get('v.selectedLookUpRecord1Market.val'),
            recordId = component.get('v.marketRecordId'),
            productType = component.get('v.segmentValue'),
            accId = component.get('v.recordId');
        if (productType.length == 0) {
            productType = 'Carpet';
        }
        
        var action = component.get("c.insertAssessment");
        if( !competitor ){
            component.set('v.errMsg3', 'Competitor is Required !');
            component.set('v.spinner',false);
            return;
        }
        if( inputDetails.Shares == null || inputDetails.Shares == '' || inputDetails.Shares == 0 || inputDetails.Shares == '0' ){
            component.set('v.errMsg3', 'Shares is Required !');
            component.set('v.spinner',false);
            return;
        }else if( inputDetails.Shares < 0 || inputDetails.Shares > 100 ){
            component.set('v.errMsg3', '');
            component.set('v.spinner',false);
            return;
        }
        var params = {
            newAss: {
                "sobjectType": 'Competitive_Assessment__c',
                "RecordTypeId": recordId,
                "Competitor_Share__c": inputDetails.Shares,
                "Competitor__c": competitor,
                "Product_Type__c": productType
            },recordId : accId
        };
        action.setParams(params);
        action.setCallback(this, function (resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                if (result === 'SUCCESS') {
                    var seg = component.get('v.segmentValue');
                    component.set('v.errMsg3', '');
                    component.set("v.addMarketDetails.Shares",'');
                    component.set("v.selectedLookUpRecord1Market",[]);
                    var device = $A.get("$Browser.formFactor");
        			if( device == 'DESKTOP' ){
                    	this.hidePopupHelper( component,'newMarketModal','newModalMarket' );
                    }else{
                        console.log( 'MOBILE HERE' );
                        this.hidePopupHelper( component,'newMarketModalMobile','newModalMarketMobile' );
                    }
                    this.getFilterList(component, seg, '');                    
                    
                }else{
                    component.set('v.errMsg3', result);
                    component.set('v.spinner',false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    updateProductHelper:function(component, event,helper){
        var inputDetails = component.get('v.editProductDetails');
        var action = component.get("c.updateAssessments");
        
        if( !component.get("v.selectedLookUpRecordEDITProduct.val") ){
            component.set('v.errMsg2', 'Product is Required !');
            component.set('v.spinner',false);
            return;
        }
        if( inputDetails.Price__c == null || inputDetails.Price__c == '' || inputDetails.Price__c == 0 || inputDetails.Price__c == '0' ){
            component.set('v.errMsg2', 'Price is Required !');
            component.set('v.spinner',false);
            return;
        }
        if( inputDetails.Velocity__c == null || inputDetails.Velocity__c == '' || inputDetails.Velocity__c == 0 || inputDetails.Velocity__c == '0' ){
            component.set('v.errMsg2', 'Velocity is Required !');
            component.set('v.spinner',false);
            return;
        }
        
        
        var postOfferData = {
            modifiedCA: {
                "Price__c": inputDetails.Price__c,
                "Velocity__c": inputDetails.Velocity__c,
                "Competitor_Product__c": component.get("v.selectedLookUpRecordEDITProduct.val"),
                "sobjectType": "Competitive_Assessment__c",
                "Product_Type__c": inputDetails.Product_Type__c,
                "Id":inputDetails.Id
            }
        };
        action.setParams(postOfferData);
        action.setCallback(this, function (resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                if(resp.getReturnValue()){
                    var seg = component.get('v.segmentValue');
                    
                    var device = $A.get("$Browser.formFactor");
        			if( device == 'DESKTOP' ){
                    	this.hidePopupHelper( component,'editProductModal','editModalProduct' );
                    }else{
                        console.log( 'MOBILE HERE' );
                        this.hidePopupHelper( component,'editProductModalMobile','editModalProductMobile' );
                    }
                    this.getFilterList(component, seg, '');
                }
            }
        });
        $A.enqueueAction(action);
    },
    updateMarketHelper:function(component, event, helper){
        var inputDetails = component.get('v.editMarketDetails');
        var action = component.get("c.updateAssessments");        
        if( !component.get("v.selectedLookUpRecordEDITMarket.val") ){
            component.set('v.errMsg4', 'Competitor is Required !');
            component.set('v.spinner',false);
            return;
        }
        if( inputDetails.Competitor_Share__c == null || inputDetails.Competitor_Share__c == '' || inputDetails.Competitor_Share__c == 0 || inputDetails.Competitor_Share__c == '0' ){
            component.set('v.errMsg4', 'Shares is Required !');
            component.set('v.spinner',false);
            return;
        }else if( inputDetails.Competitor_Share__c < 0 || inputDetails.Competitor_Share__c > 100 ){
            component.set('v.errMsg4', '');
            component.set('v.spinner',false);
            return;
        }
        var postOfferData = {
            modifiedCA: {
                "Competitor_Share__c": inputDetails.Competitor_Share__c,
                "Competitor__c": component.get("v.selectedLookUpRecordEDITMarket.val"),
                "sobjectType": "Competitive_Assessment__c",
                "Product_Type__c": inputDetails.Product_Type__c,
                "Id":inputDetails.Id
            }
        };
        action.setParams(postOfferData);
        action.setCallback(this, function (resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                if(resp.getReturnValue()){
                    var seg = component.get('v.segmentValue');
                    var device = $A.get("$Browser.formFactor");
        			if( device == 'DESKTOP' ){
                    	this.hidePopupHelper( component,'editMarketModal','editModalMarket' );
                    }else{
                        console.log( 'MOBILE HERE' );
                        this.hidePopupHelper( component,'editMarketModalMobile','editModalMarketMobile' );
                    }
                    
                    
                    this.getFilterList(component, seg, '');
                }
            }
        });
        $A.enqueueAction(action);
    },
    ternaryCheck:function( whatToCheck ){
        if( whatToCheck === '' || whatToCheck === null || whatToCheck === undefined || whatToCheck == null )
            return '';
        else
            return whatToCheck;
    },
    deviceOrentationHandler : function( component,event,helper ){
        //alert(window.orientation);
                    component.set("v.deviceOrentation",window.orientation);

       window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            //alert(window.orientation);
            //return window.orientation;
            component.set("v.deviceOrentation",window.orientation);
        }, false);
    }
})