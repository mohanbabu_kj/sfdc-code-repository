({
    validateData: function(component){
        var quote = component.get('v.quote');
        var formattr = component.get('v.formattributes');
        var dealer = component.find('dealerId').get("v.value");
        var contact = component.find('contactId').get("v.value");
        //var specifier = component.find('specifierId').get("v.value");
        var enduser = component.find('endUserId').get("v.value");
        var today = new Date().toISOString().slice(0, 10);
        var today_plus_90_days = new Date();
        
		today_plus_90_days.setDate(today_plus_90_days.getDate() + 90);
        today_plus_90_days = today_plus_90_days.toISOString().slice(0, 10);
        console.log('Today: ' + today + ', Exp. Date: ' + quote.ExpirationDate + ', Today+90: ' + today_plus_90_days);
        formattr.isError = formattr.quoteNameMissing = formattr.expDateMissing = formattr.accountMissing = formattr.contactMissing = formattr.endUserMissing = formattr.locationMissing = formattr.segmentMissing = false;
        console.log('Dealer: ' + dealer + ' End User: ' + enduser)
        if(quote.ExpirationDate === undefined || quote.ExpirationDate === ''){
            formattr.errorExpDateMessage = 'Please enter Expiration Date!';
            formattr.isError = true;
            formattr.expDateMissing = true;
            component.find('expDate').focus();
        }else if(quote.ExpirationDate <= today){
            formattr.errorExpDateMessage = 'Expiration Date should be in future!';
            formattr.isError = true;
            formattr.expDateMissing = true;
            component.find('expDate').focus();
        }else if(quote.ExpirationDate > today_plus_90_days){
            formattr.errorExpDateMessage = 'Expiration Date should be less than 90 days!';
            formattr.isError = true;
            formattr.expDateMissing = true;
            component.find('expDate').focus();
        }else if(quote.Job_Location__c === undefined || quote.Job_Location__c === ''){
            formattr.errorLocationMessage = 'Please enter Job Location!';
            formattr.isError = true;
            formattr.locationMissing = true;
        }else if(quote.Market_Segment__c === undefined || quote.Market_Segment__c === ''){
            formattr.errorSegmentMessage = 'Please enter Market Segment!';
            formattr.isError = true;
            formattr.segmentMissing = true;
        }else if(dealer === undefined || dealer === '' || dealer === '--None--'){
            formattr.errorAccountMessage = 'Please choose Account Name!';
            formattr.isError = true;
            formattr.accountMissing = true;
            component.find('dealerId').focus();
        }else if(contact === undefined || contact === '' || contact === '--None--'){
            formattr.errorContactMessage = 'Please choose Contact!';
            formattr.isError = true;
            formattr.contactMissing = true;
            component.find('contactId').focus();
        }/*else if(specifier === undefined || specifier === '' || specifier === '--None--'){
            formattr.errorMessage = 'Please choose Specifier';
            isError = true;
        }else if(enduser === undefined || enduser === '' || enduser === '--None--'){
            formattr.errorEndUserMessage = 'Please choose End User!';
            formattr.isError = true;
            formattr.endUserMissing = true;
            component.find('endUserId').focus();
        }*/
        
        if(formattr.isError){
            component.set('v.formattributes', formattr);
            //component.set('v.isErrorOccurred', true);
        }
    },
    
    validateItems: function(component){
        var formattr = component.get('v.formattributes');
        var quoteLineItems = formattr.quoteInfo.quoteLineItems;
        var formFieldMap = component.get('v.formFieldMap');
        var products = [];
        
        if(quoteLineItems.length>0){
            for(var i = 0; i < quoteLineItems.length; i++){
                console.log('Line Item: ' + quoteLineItems[i]);
                if(quoteLineItems[i].Quantity === undefined || quoteLineItems[i].Quantity === null || 
                   quoteLineItems[i].Quantity === '' || quoteLineItems[i].Quantity === '0' || quoteLineItems[i].Quantity < '0')
                {
                    formattr.errorMessage = 'Please enter valid Quantity for ' + quoteLineItems[i].Product_Name__c ;
                    formattr.isError = true;
                    break;
                    //component.find('endUserId').focus();
                }
                if(quoteLineItems[i].UnitPrice === undefined || quoteLineItems[i].UnitPrice === null || 
                   quoteLineItems[i].UnitPrice === '' || quoteLineItems[i].UnitPrice === '0' || quoteLineItems[i].UnitPrice < '0')
                {
                    formattr.errorMessage = 'Please enter valid Price for ' + quoteLineItems[i].Product_Name__c ;
                    formattr.isError = true;
                    break;
                    //component.find('endUserId').focus();
                }
                if(quoteLineItems[i].Floor__c !== undefined){
                    var rvp_svp_price =  quoteLineItems[i].Floor__c.substr(1);
                    var unitPrice = quoteLineItems[i].UnitPrice;
                    var n = Number.parseFloat(unitPrice);
                    unitPrice = n.toFixed(2);
                    unitPrice = unitPrice.toLocaleString('en-US', {style: 'currency', currency: 'USD'});
                    if(quoteLineItems[i].UnitPrice <= rvp_svp_price && !quoteLineItems[i].Promotion__c){
                        formFieldMap.rvp_svp_price_confirm = true;
                        products.push({
                            Name: quoteLineItems[i].Product_Name__c,
                            AE_Low: quoteLineItems[i].Low_Market__c,
                            Price: unitPrice
                        });
                    }
                }
                /** CODE START ADDED BY Mudit Bug 64290 - 1/22/19 **/
                if( quoteLineItems[i].Custom_Product__c && ( quoteLineItems[i].Product_Name__c === undefined || quoteLineItems[i].Product_Name__c === null || quoteLineItems[i].Product_Name__c === '' ) ){
                    formattr.errorMessage = 'Please enter valid Style Name';
                    formattr.isError = true;
                    break;
                }
                /** CODE END ADDED BY Mudit Bug 64290 - 1/22/19 **/
            }
        }
        console.log('Products Need Appr: ' + JSON.stringify(products));
        if(formattr.isError || formFieldMap.rvp_svp_price_confirm){
            if(formFieldMap.rvp_svp_price_confirm){
                formFieldMap.rvp_svp_appr_prodlist = products;  
                formattr.showConfirmationMessage = true;
                formattr.titletext = 'Price Approval Confirmation';
                formattr.confirmationMessage = 'These products will require pricing approval by your RVP/SVP after order entry:';
                component.set('v.formFieldMap', formFieldMap);
            }
            component.set('v.formattributes', formattr);
            component.set('v.isErrorOccurred', true);
            console.log('Field Map: ' + JSON.stringify(formFieldMap));
        }
    },
    
    save : function(component, event, action) {
        var quote = component.get('v.quote');
        var quoteLineItemList = component.get('v.quoteLineItemList');
        var formattr = component.get('v.formattributes');
        var callbackaction = component.get('c.saveQuote');
        var quoteInfo = formattr.quoteInfo;
        var itemDetails = component.find('itemDetails');
        var iframe = component.find('iframe');
        var prodAccMapOnLoad = formattr.productAccessoryMapOnLoad;
        var prodAccMap = formattr.productAccessoryMap;
        
        var prodAccMapSelected = [];
        
        $A.util.removeClass(itemDetails, 'slds-show');
        $A.util.addClass(itemDetails, 'slds-hide');
        formattr.onSave = true;
        component.set('v.formattributes', formattr);
        formattr = component.get('v.formattributes');
        quote.Dealer__c = component.find('dealerId').get("v.value");
        quote.ContactId = component.find('contactId').get("v.value");
        quote.End_User__c = component.find('endUserId').get("v.value");
        quote.Strategic_Account__c = component.find('strAccId').get("v.value");
        
        console.log(quote.Name + ',' + quote.Dealer__c + ',' );
        quote.Account_Number__c = formattr.accNumber;
        quoteInfo.quote = quote;
        quoteInfo.quoteLineItems = formattr.quoteInfo.quoteLineItems; //quoteLineItemList;
        
        console.log('ProductAccMap: ' + JSON.stringify(prodAccMap));
        for(var idx in prodAccMap){
            var count = 0;
            var spliceind;
            var prodAccList = prodAccMapOnLoad[prodAccMap[idx].key];
            var prodAccListSelected = new Object();
            console.log('ProductAccList: ' + JSON.stringify(prodAccList));
            var prodAccListUpd = prodAccMap[idx].list;

            console.log('ProductAccListUpd: ' + JSON.stringify(prodAccListUpd));
            console.log('International User: ' + quoteInfo.userInfo.International_User__c);
            for(var i in prodAccListUpd){
                if(prodAccListUpd[i].isChecked === false){
                    if(count === 0){
                        spliceind = i;
                    }else{
                        spliceind = i - count;
                    }
                    prodAccList.splice(spliceind,1);
                    count += 1;
                }else{
                    /* Start of Code - MB - Bug 68960 - 1/15/19 */
                    if(quoteInfo.userInfo.International_User__c){
                        for(var j in prodAccList){
                            if(prodAccList[j].Id === prodAccListUpd[i].Id){
                                prodAccList[j].Accessory_Price__c = prodAccListUpd[i].Accessory_Price__c;
                            }
                        }
                    }
                    /* End of Code - MB - Bug 68960 - 1/15/19 */
                }
            }
            prodAccMapOnLoad[prodAccMap[idx].key] = prodAccList;
            //prodAccMapSelected.push(prodAccMap[idx].key, prodAccList);
        }
        
        console.log('ProductAccSelected: ' + JSON.stringify(prodAccMapOnLoad));
        quoteInfo.productAccessoryMap = prodAccMapOnLoad;
        quoteInfo.error = quoteInfo.errorPDF = false;
        quoteInfo.errorMessage = '';
        console.log('Quote Info: ' + JSON.stringify(quoteInfo));
        callbackaction.setParams({
            'quoteInfoStr': JSON.stringify(quoteInfo)
            //'quoteLineItemList': quoteLineItemList
        });
        callbackaction.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                var result = response.getReturnValue();
                quoteInfo = result;
                if(result.error === true){
                    console.log(response.getReturnValue());
                    formattr.showMessage = true;
                    formattr.errorMessage = result.errMessage;
                    component.set('v.isErrorOccurred', true);
                    formattr.onSave = false;
                    component.set('v.formattributes', formattr);
                    //alert(result.errMessage);
                }else if(result.errorPDF === true){
                    formattr.showMessage = true;
                    formattr.errorMessage = result.errMessage;
                    component.set('v.isErrorOccurred', true);
                    formattr.errorPDF = true;
                    formattr.onSave = false;
                    formattr.errorMessage = formattr.errorMessage + '. PDF generation is failed. Open the Quote created and try sending Email manually again.';
                    component.set('v.formattributes', formattr);
                }else{
                    if(action === 'SaveAndSendEmail'){
                        formattr.viewPDF = '/apex/QuotesSendEmail?Id=' + result.quote.Id;
                        console.log(result.viewPDF);
                        $A.util.removeClass(iframe, 'slds-hide');
                        $A.util.addClass(iframe, 'slds-show');
                        $A.util.removeClass(itemDetails, 'slds-show');
                        $A.util.addClass(itemDetails, 'slds-hide');
                        formattr.isQuoteDetailsPage = false;
                        formattr.isQuoteItemsPage = false;
                        formattr.isIFramePage = true;
                        formattr.onSave = false;
                        console.log(formattr.viewPDF);
                        formattr.createPDF = true;
                        console.log(formattr.createPDF);
                        component.set('v.formattributes', formattr);
                        this.removeEventListener(event);
                    }else{
                        this.removeEventListener(event);
                        formattr.onSave = false;
                        component.set('v.formattributes', formattr);
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        this.showMyToast();
                    }
                }
            }
        });
        $A.enqueueAction(callbackaction); 
    },
    
    showMyToast : function() {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: 'success',
            mode: 'dismissible',
            message: 'Quote record is created.'
        });
        toastEvent.fire();
    },
    
    setSelectedAccessories : function(component){
        var formattr = component.get('v.formattributes');
        var prodAccMap = formattr.productAccessoryMap;
        var prodAccListUpd = formattr.productAccessoriesList;
        
        if(prodAccListUpd !== undefined){
            for(var idx in prodAccMap){
                if(prodAccMap[idx].key === formattr.selectedProduct){
                    prodAccMap[idx].list = prodAccListUpd;
                    for(var i in prodAccListUpd){
                        if(formattr.selectAll && prodAccListUpd[i].isChecked !== undefined){
                            if(!prodAccListUpd[i].isChecked){
                                formattr.selectAll = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
        formattr.productAccessoryMap = prodAccMap;
        console.log('Updated ProdAccMap: ' + JSON.stringify(formattr.productAccessoryMap));
        component.set('v.formattributes', formattr);
    },
    
    setOrientation: function(component){
        var formFieldMap = component.get('v.formFieldMap');
        if(formFieldMap !== undefined){
            if(window.innerHeight < window.innerWidth){
                if(formFieldMap.iPadPortrait !== undefined)
                    formFieldMap.iPadPortrait = true;
                
                if(formFieldMap.iPadLandscape !== undefined)
                    formFieldMap.iPadLandscape = false;
            }else{
                if(formFieldMap.iPadLandscape !== undefined)
                    formFieldMap.iPadLandscape = true;
                
                if(formFieldMap.iPadPortrait !== undefined)
                    formFieldMap.iPadPortrait = false;
            }
            component.set('v.formFieldMap', formFieldMap);
        }
    },
    
    removeEventListener : function(event){
        document.removeEventListener(
            "orientationchange", 
            $A.getCallback(function(event) {
                
            })
        );
    }
})