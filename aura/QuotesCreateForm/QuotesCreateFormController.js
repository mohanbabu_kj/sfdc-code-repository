({
    doInit : function(component, event, helper) {
        var formattributes = new Object();
        var formFieldMap = new Map();
        var recordId = component.get('v.recordId');
        var action = component.get("c.getOppInfo");
        
        console.log(recordId);
        formattributes.countryNotSelected = true;
        formattributes.createPDF = false;
        formattributes.onLoad = true;
        formattributes.isQuoteDetailsPage = true;
        formattributes.isQuoteItemsPage = false;
        formattributes.isIFramePage = false;
        formattributes.isPreviewPage = false;
        formattributes.backText = 'Back';
        formattributes.selectedAccessories = [];
        formFieldMap.width = window.screen.width;
        if(component.get('v.isTablet')){
            if(window.screen.width <= 834){
                formFieldMap.iPadPortrait = true;
                formFieldMap.iPadLandscape = false;
            }else{
                formFieldMap.iPadPortrait = false;
                formFieldMap.iPadLandscape = true;
            }
        }
        //alert(window.screen.width);
        component.set('v.formattributes',formattributes);
        
        action.setParams({'recordId': recordId});
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(state);
            if (component.isValid() && state === "SUCCESS") {
                var map = result.dealersListMap;
                var dealersMapKey = result.dealersListMap;
                var endUserMapKey = result.endUserListMap;
                var strAccMapKey = result.strAccListMap;
                var options = [];
                var selected = true;
                var key, label;
                console.log('Return Value' + response.getReturnValue());
                console.log(result.quote);
                console.log(result.dealersListMap);
                console.log(result.accNumberMap);
                
                if(result.error){
                    formattributes.showMessage = true;
                    formattributes.errorMessage = result.errMessage;
                    formattributes.onLoad = false;
                    component.set('v.formattributes',formattributes);
                }else{
                    component.set('v.quoteLineItemList', result.quoteLineItems);
                    console.log('QuoteLineItems:  ' + JSON.stringify(result.quoteLineItems));
                    console.log(result.quote);
                    formattributes.quoteInfo = result;
                    formattributes.userInfo = result.userInfo;
                    console.log(JSON.stringify(formattributes.userInfo));
                    formattributes.dealersListMap = result.dealersListMap;
                    formattributes.oppName = result.oppName;
                    formattributes.accNumberMap = result.accNumberMap;
                    formattributes.accContactMap = result.accContactMap;
                    formattributes.accIdContIdMap = result.accIdContIdMap;
                    formattributes.productAccessoryMapOnLoad = result.productAccessoryMap;
                    formattributes.disableContact = true;
                    console.log(formattributes.accContactMap);
                    if(result.userCountry !== undefined || result.userCountry !== ''){
                        formattributes.userCountry = result.userCountry;
                        //formattributes.currencyNotSeleceted = false;
                    }
                    for(key in dealersMapKey){
                        label = map[key];
                        options.push({
                            value: key,
                            label: label});
                    }
                    console.log(options);
                    formattributes.dealersList = options; 
                    
                    map = result.endUserListMap;
                    options = [];
                    for(key in endUserMapKey){
                        label = map[key];
                        if(selected && (result.quote.End_User__c === undefined || result.quote.End_User__c === null)){
                            result.quote.End_User__c = key;
                        }
                        options.push({
                            value: key,
                            label: label,
                            selected: selected});
                    }
                    console.log(options);
                    component.find('endUserId').set("v.value", result.quote.End_User__c);
                    formattributes.endUserList = options;
                    
                    selected = true;
                    map = result.strAccListMap;
                    options = [];
                    for(key in strAccMapKey){
                        label = map[key];
                        if(selected && (result.quote.Strategic_Account__c === undefined || result.quote.Strategic_Account__c === null)){
                            result.quote.Strategic_Account__c = key;
                        }
                        options.push({
                            value: key,
                            label: label,
                            selected: selected});
                        selected = false;
                    }
                    console.log(options);
                    component.find('strAccId').set("v.value", result.quote.Strategic_Account__c);
                    formattributes.strAccList = options;
                    
                    var myMap = []; //component.get('v.productAccessoryMap');
                    for(key in result.productAccessoryMap){
                        var productAccessoriesList = [];
                        var prodAccList = result.productAccessoryMap[key];
                        console.log('Key: ' + key);
                        console.log('List: ' + JSON.stringify(prodAccList));
                        for(var idx in prodAccList){
                            var prodAcc = new Object();
                            prodAcc.Id = prodAccList[idx].Id;
                            prodAcc.Name = prodAccList[idx].Name;
                            prodAcc.Description = prodAccList[idx].Description;
                            prodAcc.Size_Description__c = prodAccList[idx].Size_Description__c;
                            prodAcc.Spread_Rate__c = prodAccList[idx].Spread_Rate__c;
                            prodAcc.Accessory_Price__c = (prodAccList[idx].Accessory_Price__c !== undefined ? prodAccList[idx].Accessory_Price__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : '$0.00');
                            prodAcc.UOM__c = prodAccList[idx].UOM__c;
                            prodAcc.isChecked = false;
                            productAccessoriesList.push(prodAcc);
                        }
                        var temp = { "key": key, "list": productAccessoriesList };
                        myMap.push(temp);
                        console.log('Init Prod Acc List: ' + JSON.stringify(productAccessoriesList));
                        //myMap.set(key, productAccessoriesList);
                        console.log('Accessory Map: ' + JSON.stringify(myMap));
                    }
                    //
                    formattributes.productAccessoryMap = myMap;
                    
                    formattributes.onLoad = false;
                    formattributes.quotePage = true;
                    //result.quote.Status = 'Pending';
                    component.set('v.formattributes',formattributes);
                    component.set('v.formFieldMap', formFieldMap);
                    component.set('v.quote',result.quote);
                }
            }
        });
        $A.enqueueAction(action);
        
        document.addEventListener(
            "orientationchange", 
            $A.getCallback(function(event) {
                helper.setOrientation(component);
            })
        );


    },
    
    onNext: function(component, event, helper){
        var formattr = component.get('v.formattributes');
        var quoteDetails = component.find('quoteDetails');
        var itemDetails = component.find('itemDetails');
        //var productList = formattr.quoteInfo.quoteLineItems;
        
        helper.validateData(component);
        if(!formattr.isError){
            formattr.isQuoteDetailsPage = false;
            formattr.isQuoteItemsPage = true;
            $A.util.removeClass(quoteDetails, 'slds-show');
            $A.util.addClass(quoteDetails, 'slds-hide');
            $A.util.removeClass(itemDetails, 'slds-hide');
            $A.util.addClass(itemDetails, 'slds-show');
            /*for(var i in productList){
                if(productList[i].Promotion__c){
                    formattr.fieldText1 = '1.0 Pricing';
                    formattr.fieldText2 = '1.5 Pricing';
                    formattr.fieldText3 = '2.0 Pricing';
                }else{
                    formattr.fieldText1 = 'AE Low';
                    formattr.fieldText2 = 'Base';
                    formattr.fieldText3 = 'Stretch (3.7%)';
                }
            }*/
        }
        component.set('v.formattributes',formattr);
        
    },
    
    onBack: function(component, event, helper){
        var formattr = component.get('v.formattributes');
        var quoteDetails = component.find('quoteDetails');
        var productAccessories = component.find('productAccessories');
        var itemDetails = component.find('itemDetails');
        
        if(formattr.isQuoteItemsPage){
            formattr.isQuoteDetailsPage = true;
            formattr.isQuoteItemsPage = false;
            $A.util.removeClass(itemDetails, 'slds-show');
            $A.util.addClass(itemDetails, 'slds-hide');
            $A.util.removeClass(quoteDetails, 'slds-hide');
            $A.util.addClass(quoteDetails, 'slds-show');
        }else if(formattr.isProductAccessoriesPage){
            formattr.isProductAccessoriesPage = false;
            formattr.isQuoteItemsPage = true;
            $A.util.removeClass(productAccessories, 'slds-show');
            $A.util.addClass(productAccessories, 'slds-hide');
            $A.util.removeClass(itemDetails, 'slds-hide');
            $A.util.addClass(itemDetails, 'slds-show');
            formattr.selectAll = false;
            formattr.backText = 'Back';
            helper.setSelectedAccessories(component);
        }
        formattr.isIFramePage = false;
        component.set('v.formattributes',formattr); 
        
    },
    
    save: function(component, event, helper){
        var formattr = component.get('v.formattributes');
        component.set('v.isErrorOccurred', false);
        formattr.action = 'Save';
        helper.validateItems(component);
        if(!component.get('v.isErrorOccurred')){
            helper.save(component, event, 'Save');
        }
        component.set('v.formattributes', formattr);
    },
    
    saveandsend: function(component, event, helper){
        var formattr = component.get('v.formattributes');
        component.set('v.isErrorOccurred', false);
        formattr.action = 'SaveAndSendEmail';
        helper.validateItems(component);
        if(!component.get('v.isErrorOccurred')){
            helper.save(component, event, 'SaveAndSendEmail');
        }
        component.set('v.formattributes', formattr);
    },
    
    cancel: function(component, event, helper){
        helper.removeEventListener(event);
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    close: function(component){
        var formattr = component.get('v.formattributes');
        formattr.isError = false;
        component.set('v.formattributes', formattr);
    },
    
    closeAlertMsg: function(component, event, helper){
        var formattr = component.get('v.formattributes');
        formattr.errorMessage = '';
        formattr.showMessage = false;
        helper.removeEventListener(event);
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        component.set('v.formattributes',formattr);
    },
    
    onDealerChange: function(component){
        var dealerSelected = component.find('dealerId').get('v.value');
        var formattributes = component.get('v.formattributes');
        var map = formattributes.accNumberMap;
        var acc = new Object();
        var contactMap = [];
        var contactKey = [];
        var options = [];
        var selected = false;
        var quote = component.get('v.quote');
        
        formattributes.contactEmail = '';
        formattributes.accNumber = '';
        quote.Dealer__c = dealerSelected;
        acc = map[dealerSelected];
        if(acc !== undefined){
            formattributes.accNumber = quote.Account_Number__c = acc.CAMS_Account_number__c;
            quote.ShippingName = acc.Name;
            quote.ShippingCity = acc.ShippingCity;
            quote.ShippingStreet = acc.ShippingStreet;
            quote.ShippingCountry = acc.ShippingCountry;
            quote.ShippingCountryCode = acc.ShippingCountryCode;
            quote.ShippingState = acc.ShippingState;
            quote.ShippingStateCode = acc.ShippingStateCode;
            quote.ShippingPostalCode = acc.ShippingPostalCode;
            component.find('dealerId').set('v.value', dealerSelected);
        }else{
            formattributes.disableContact = true;
            quote.ContactId = '';
            component.find('contactId').set("v.value", quote.ContactId);
            formattributes.accNumber = quote.Account_Number__c = '';
            quote.ShippingName =  quote.ShippingCity = quote.ShippingStreet = '';
            quote.ShippingCountry = quote.ShippingCountryCode = quote.ShippingState = '';
            quote.ShippingStateCode = quote.ShippingPostalCode = '';
        }
        var contId = formattributes.accIdContIdMap[dealerSelected];
        map = formattributes.accContactMap;
        contactMap = map[dealerSelected];
        //map = contactMap;
        //contactKey = contactMap;
        console.log(contId);
        quote.ContactId = '';
        
        for(var key in contactMap){
            selected = false;
            var contact = contactMap[key];
            console.log('Contact: ' + JSON.stringify(contact));
            if(contact.Id === contId && quote.ContactId === ''){
                console.log('Contact Id: ' + contId);
                quote.ContactId = contId;
                formattributes.contactEmail = contact.Email;
                selected = true;
            }
            options.push({
                value: key,
                label: contact.Name,
                selected: selected});
            formattributes.disableContact = false;
        }
        component.find('contactId').set("v.value", quote.ContactId);
        console.log(quote.ContactId);
        formattributes.contactList = options;
        console.log(options);
        component.set('v.formattributes', formattributes);
        component.set('v.quote', quote);
    },
    
    onContactChange: function(component){
        var dealerSelected = component.find('dealerId').get('v.value');
        var contactSelected = component.find('contactId').get('v.value');
        var formattr = component.get('v.formattributes');
        var map = formattr.accContactMap;
        var contactMap = [];
        var contact = new Object();
        
        contactMap = map[dealerSelected];
        map = contactMap;
        contact = map[contactSelected];
        formattr.contactEmail = contact.Email;
        component.set('v.formattributes', formattr);
    },
    
    showAccessories: function(component, event){
        var formattr = component.get('v.formattributes');
        var productAccPage = component.find('productAccessories');
        var itemDetails = component.find('itemDetails');
        var selectAll = true;
        var map = formattr.productAccessoryMap;
        var productAccessoriesList = [];
        var productList = [];
        var selectedProduct = event.getSource().get('v.value');
        
        console.log('Map: ' + JSON.stringify(map));
        formattr.isQuoteDetailsPage = false;
        formattr.isQuoteItemsPage = false;
        formattr.isProductAccessoriesPage = true;
        formattr.backText = 'Done';
        $A.util.removeClass(productAccPage, 'slds-hide');
        $A.util.addClass(productAccPage, 'slds-show');
        $A.util.removeClass(itemDetails, 'slds-show');
        $A.util.addClass(itemDetails, 'slds-hide');
        
        for(var key in map){
            if(map[key].key === selectedProduct){
                productAccessoriesList = map[key].list;
            }
        }
        if(productAccessoriesList.length === 0){
            formattr.hasAccessories = false;
            component.set('v.formattributes',formattr);
            return;
        }

        formattr.hasAccessories = true;
        console.log('Product Accessories: ' + JSON.stringify(productAccessoriesList));
        for(var idx in productAccessoriesList){
            var productAccessories = new Object();
            console.log('Product Accessory: ' + JSON.stringify(productAccessoriesList[idx]));
            productAccessories.Name = productAccessoriesList[idx].Name;
            productAccessories.Id = productAccessoriesList[idx].Id;
            productAccessories.Description = productAccessoriesList[idx].Description;
            productAccessories.Size_Description__c = productAccessoriesList[idx].Size_Description__c;
            productAccessories.Spread_Rate__c = productAccessoriesList[idx].Spread_Rate__c;
            productAccessories.UOM__c = productAccessoriesList[idx].UOM__c;
            productAccessories.isChecked = productAccessoriesList[idx].isChecked;
            productAccessories.Accessory_Price__c = productAccessoriesList[idx].Accessory_Price__c;
            console.log(productAccessories);
            productList.push(productAccessories);
            if(productAccessories.isChecked === false){
                selectAll = false;
            }
        }
        //product = map[selectedProduct];
        console.log('product list: ' + JSON.stringify(productList));
        formattr.productAccessoriesList = productList;
        formattr.selectedProduct = selectedProduct;
        formattr.selectAll = selectAll;
        component.set('v.formattributes',formattr);
    },
    
    selectAllAccessory : function(component){
        var formattr = component.get('v.formattributes');
        var prodAccListUpd = [];
        prodAccListUpd = formattr.productAccessoriesList;
        
        for(var i in prodAccListUpd){
            if(prodAccListUpd[i].isChecked !== undefined){
                if(formattr.selectAll){
                    prodAccListUpd[i].isChecked = true;
                }else{
                    prodAccListUpd[i].isChecked = false;
                }
            }
        }
        //formattr.deselectAll = false;
        formattr.productAccessoriesList = prodAccListUpd;
        console.log('Updated ProdAccList: ' + JSON.stringify(formattr.productAccessoriesList));
        component.set('v.formattributes', formattr);
    },
    
    checkSelectAll : function(component){
        var formattr = component.get('v.formattributes');
        var prodAccListUpd = formattr.productAccessoriesList;
        
        for(var i in prodAccListUpd){
            if(formattr.selectAll){
                if(!prodAccListUpd[i].isChecked){
                    formattr.selectAll = false;
                    break;
                }
            }
        }
        component.set('v.formattributes', formattr);
    },
    
    deleteLineItem : function(component, event){
        var formattr = component.get('v.formattributes');
        //var quoteLineItemList = formattr.quoteInfo.quoteLineItems;
        var selectedProduct = event.getSource().get('v.value');
        
        formattr.showConfirmationMessage = true;
        formattr.confirmationMessage = 'Are you sure to delete the line item?';
        formattr.prodId = selectedProduct;
        formattr.titletext = 'Delete?';
		formattr.deleteLineItem = true;
        component.set('v.formattributes', formattr);
    },
    
    deleteLI : function(component, event){
        var formattr = component.get('v.formattributes');
        var quoteLineItemList = formattr.quoteInfo.quoteLineItems;
        
        for(var i in quoteLineItemList){
            if(quoteLineItemList[i].Product2Id === formattr.prodId){
                formattr.quoteInfo.quoteLineItems.splice(i,1);
            }
        }
        formattr.showConfirmationMessage = false;
        formattr.prodId = '';
        formattr.deleteLineItem = false;
        component.set('v.formattributes', formattr);
    },
    
    doNotDelete : function(component){
        var formattr = component.get('v.formattributes');
        formattr.showConfirmationMessage = false;
        formattr.prodId = '';
        formattr.deleteLineItem = false;
        component.set('v.formattributes', formattr);
    },
    
    proceed: function(component, event, helper){
        var formattr = component.get('v.formattributes');
        var formFieldMap = component.get('v.formFieldMap');
        formattr.confirmationMessage = formattr.titletext = '';
        formattr.rvp_svp_price_error = formFieldMap.rvp_svp_price_confirm = formattr.showConfirmationMessage = false;
        component.set('v.isErrorOccurred', false);
        component.set('v.formFieldMap', formFieldMap);
        helper.save(component, event, formattr.action);
        component.set('v.formattributes', formattr);
        
    },
    
    closeConfirmationMessage: function(component){
        var formattr = component.get('v.formattributes');
        var formFieldMap = component.get('v.formFieldMap');
        formattr.confirmationMessage = formattr.titletext = '';
        formattr.rvp_svp_price_error = formFieldMap.rvp_svp_price_confirm = formattr.showConfirmationMessage = false;
        component.set('v.formattributes', formattr);
        component.set('v.formFieldMap', formFieldMap);
        component.set('v.isErrorOccurred', false);
    }
    
})