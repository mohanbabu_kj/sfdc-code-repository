({
    itemSelected : function(component, event, helper) {
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");  
        console.log('SelIndex: ' + SelIndex);
        if(SelIndex){
            var serverResult = component.get("v.server_result");
            var selItem = serverResult[SelIndex];
            //if(selItem.val){
            if(selItem !== undefined){
                component.set("v.selItem",selItem);
                component.set("v.last_ServerResult",serverResult);
                var appEvent = $A.get("e.c:relatedAccountEvent");
                appEvent.setParams({
                    "data" : selItem
                });
                appEvent.fire();
            } 
            component.set("v.server_result",null); 
        } 
    }, 
    serverCall : function(component, event, helper) {  
        var target = event.target;  
        var searchText = component.find("combobox-unique-id").get("v.value");
        var last_SearchText = component.get("v.last_SearchText");
        var device = $A.get("$Browser.formFactor");
        //Escape button pressed 
        //if (event.keyCode == 27 || !searchText.trim()) {
        if(searchText!=null){ 
            if (event.keyCode == 27 ) {
                helper.clearSelection(component, event, helper);
            }else if( event.keyCode == 32 || ( searchText.trim().length == 0 ) ){
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result",noResult); 
                component.set("v.last_ServerResult",noResult);
                var appEvent = $A.get("e.c:closeMenuForAccountLookupEvent");
        		appEvent.fire();
            }else if(searchText.trim() != last_SearchText){ 
                console.log('searchText: ' + searchText);
                var objectName = component.get("v.objectName");
                var field_API_text = component.get("v.field_API_text");
                var field_API_val = component.get("v.field_API_val");
                var field_API_search = component.get("v.field_API_search");
                var limit = component.get("v.limit");
                var filterField = component.get("v.filterField");
                var filter_Val = component.get("v.filterVal");
                //var action = component.get('c.searchDB');
                var action = component.get('c.searchDBMultipleFields');
                var formFieldMap = component.get('v.formFieldMap');
                action.setStorable();
                console.log('filterField: ' + filterField);
                /*action.setParams({
                    objectName : objectName,
                    fld_API_Text : field_API_text,
                    fld_API_Val : field_API_val,
                    lim : limit, 
                    fld_API_Search : field_API_search,
                    searchText : searchText,
                    filterField:filterField,
                    filterVal:filter_Val
                });*/
                // Start of Code - MB - Bug 70377 - 2/7/19
                var whereClause = '';
                whereClause = ' Name LIKE \'%'+ searchText + '%\'';
                if(filterField !== undefined && filterField === 'Role_C__c' && filter_Val !== undefined && filter_Val !== ''){
                    whereClause += ' AND Role_C__c = \'' + filter_Val + '\' AND Strategic_Account__c = false';
                }else if(filterField !== undefined && filterField === 'Strategic_Account__c' && filter_Val !== undefined && filter_Val !== ''){
                    whereClause += ' AND Strategic_Account__c = ' + filter_Val;
                }
                console.log('whereClause: ' + whereClause);
                action.setParams({
                    objectName : objectName,
                    fields: 'Id, Name, Role_C__c, Strategic_Account__c ',
                    whereClause: whereClause,
                    lim : limit
                });
                // End of Code - MB - Bug 70377 - 2/7/19
                action.setCallback(this,function(a){
                    this.handleResponse(a,component,helper);
                });
                formFieldMap.oldFilterVal = filter_Val;
                component.set("v.last_SearchText",searchText.trim());
                component.set('v.formFieldMap', formFieldMap);
                console.log('Server call made');
                $A.enqueueAction(action); 
            }else if(searchText && last_SearchText && searchText.trim() == last_SearchText.trim()){ 
                var formFieldMap = component.get('v.formFieldMap');
                if(formFieldMap.oldFilterVal !== undefined && formFieldMap.oldFilterVal === formFieldMap.newFilterVal){
                    component.set("v.server_result",component.get("v.last_ServerResult"));
                    console.log('Server call saved');
                }
            }         
        }
    },
    handleResponse : function (res,component,helper){
        if (res.getState() === 'SUCCESS') {
            var retObj = JSON.parse(res.getReturnValue());
            console.log( JSON.stringify(retObj) );
            if(retObj.length < 0){
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result",noResult); 
                component.set("v.last_ServerResult",noResult);
            }else{
                var filterField = component.get("v.filterField");
                var filterVal = component.get("v.filterVal");
                console.log( filterField );
                console.log( filterVal );
                if( filterField != '' && filterField != null && filterVal != null && filterVal != '' ){
                    var filterResult = [];
                    for( var i=0;i<retObj.length;i++ ){
                        /*if( retObj[i].filterVal == filterVal ){
                            console.log( retObj[i] );
                            filterResult.push( retObj[i] );
                        }*/
                        // Start of Code - MB - Bug 70377 - 2/7/19
                        if(filterField === 'Role_C__c' && retObj[i].Role_C__c === filterVal){
                            filterResult.push( retObj[i] );
                        }else if(filterField === 'Strategic_Account__c' && retObj[i].Strategic_Account__c === filterVal){
                            filterResult.push( retObj[i] );
                        }
                        // End of Code - MB - Bug 70377 - 2/7/19
                    }
                    component.set("v.server_result",filterResult); 
                    component.set("v.last_ServerResult",filterResult);
                }else{
                    component.set("v.server_result",retObj); 
                    component.set("v.last_ServerResult",retObj);
                }
            }  
        }else if (res.getState() === 'ERROR'){
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    alert(errors[0].message);
                }
            } 
        }
    },
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    clearSelection: function(component, event, helper){
        var appEvent = $A.get("e.c:clearLookups");
        appEvent.setParams({
            "data" : component.get("v.selItem")
        });
        component.set("v.selItem",null);
        component.set("v.server_result",null);
        
        appEvent.fire();
    },
    addNewRecord: function(component, event, helper) {
        if ( !component.get('v.allowNewRecords') ) {
            return;
        }
        var addRecordEvent = component.getEvent('addNewRecord');
        var dataToSend = [];
        dataToSend.push({
            "buttonLabel":"Add New " + component.get("v.Label"),
            "buttonId":component.get("v.fromWhere")
        });
        addRecordEvent.setParams({
            "data":dataToSend
        });
        console.log( addRecordEvent.getParams() );
        addRecordEvent.fire();
        component.set("v.server_result",null); 
        document.getElementById("combobox-unique-id").value = '';
        component.find("combobox-unique-id").set("v.value",null);
        component.set("v.inputTextVal",'');
    }
})