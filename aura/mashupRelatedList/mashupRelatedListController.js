({
    doInit : function(component, event, helper) {
    	var mashupType = component.get('v.mashupType');
    	var listHeight = component.get('v.listHeight');
    	listHeight = parseInt(listHeight)+10;
    	component.set('v.listHeight', listHeight);

    	component.set('v.pageTitle', (mashupType=='Hybris'?'Mohawk Xchange':'Related Reports'));

    	var action = component.get('c.getRecordsByGroup');
    		action.setParams({
    			'recordId': component.get('v.recordId'),
    			'mashupType': mashupType
    		});
    		action.setCallback(this, function(response) {
    			if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
		            component.set('v.RecordsByGroup', JSON.parse(response.getReturnValue()));
		            component.set('v.isListLoaded', true);
		            helper.generateHtml(component);
		        }
		        console.log(response.getReturnValue());
	        });

    	$A.enqueueAction(action);
    },
    navigateComponent : function(component, event, helper) {
    	console.log('url', event.currentTarget.dataset.url);
    	var evt = $A.get("e.force:navigateToComponent");
			evt.setParams({
				componentDef : "c:mashupTabItem",
				componentAttributes: {
					recordId : component.get('v.recordId'),
					IframeUrl : event.currentTarget.dataset.url,
					mashupName : '',
					isFromRecordPage : 'true',
					minHeight : '400'
				}
			});
			evt.fire();
    }
})