({
    // Begin of Pagination attributes and handlers 
    dataChanged: function(component, event, helper) {
        helper.dataChanged(component);
        helper.renderPage(component);
    },    
    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    },    
    // End of Pagination attributes and handlers 
    
     
    
	updateTable : function(component, event, helper) {
		console.log('### updateTable');
        
        var region = 		component.get("v.region");
        var district = 		component.get("v.district");
        var territory = 	component.get("v.territory");
        var priceLevel = 	component.get("v.priceLevel");
        var sourceId = 		component.get("v.sourceId");
        var objMetaName = 	component.get("v.objMetaName");
        var styleNum = 		component.get("v.styleNum");
        var inventoryNum = 	component.get("v.inventoryNum");
        var category = 		component.get("v.category");
        var accountId = 	component.get("v.accountId");
        var timePeriod = 	component.get("v.timePeriod");
        
        
        console.log('### updateTable - region: ' + region);
        console.log('### updateTable - district: ' + district);
        console.log('### updateTable - territory: ' + territory);
        console.log('### updateTable - priceLevel: ' + priceLevel);
        console.log('### updateTable - sourceId: ' + sourceId);
        console.log('### updateTable - objMetaName: ' + objMetaName);
        console.log('### updateTable - styleNum: ' + styleNum);
        console.log('### updateTable - inventoryNum: ' + inventoryNum);
        console.log('### updateTable - accountId: ' + accountId);
        console.log('### updateTable - category: ' + category);
        
        if (priceLevel == '') {
            component.set("v.headers", null);
            component.set("v.data", null);
        } 
        else {
            
            var params = {objName : objMetaName, 
                          styleNum: styleNum,
                          inventoryNum: inventoryNum,
                          chartId: sourceId, 
                          territory : territory, 
                          region : region, 
                          district : district, 
                          priceLevel : priceLevel, 
                          accountId : accountId,
                          category: category,
                          timePeriod: timePeriod};
            
            console.log('### params: ' + params);
            //Get the chart data
            helper.callToServer(
                component,
                "c.getData",
                function(response)
                {	
			        console.log('### updateTable - response.currencyDataFields: ' + response.currencyDataFields);
                    
                    console.log('apex response :'+JSON.stringify(response));
                    component.set("v.headers", response.dataLabels);
                    component.set("v.currencyFieldNames", response.currencyDataFields);
                    //component.set("v.data", response.dataValues);
                    //Start of code - Mudit - 08/02/18 - Bug 62906
                    //var isInit = component.get("v.isCallfromInitFlag");
                    //if( isInit ){
                        var finalTimePeriod = ( timePeriod == 'YTD' ? 'YTD Sales' : ( timePeriod == 'MTD' ? 'MTD Sales' : 'R12 Sales' ) )
                        component.set("v.sortField",finalTimePeriod);
                        component.set("v.sortAsc",false);
                        var dataToSort = response.dataValues;
                        if( dataToSort != null && dataToSort.length > 0  ){
                            helper.sortData(component,finalTimePeriod,dataToSort,'sorteddata','sortAsc','sortField');    
                            component.set("v.data",component.get("v.sorteddata"));
                        }    
                    //}                    
                    //End of code - Mudit - 08/02/18 - Bug 62906
                    helper.renderPage(component);
                }, 
                params
            ); 	
        }
    },
    //Start of code - Mudit - 08/02/18 - Bug 62906
    tableSorting : function(component, event, helper) {
        /*var sourceId = 		component.get("v.sourceId");        
        if( sourceId == 1 || sourceId == 2 ){
            component.set("v.isCallfromInitFlag",false);
            var cmp2 = component.get("c.updateTable");
            $A.enqueueAction(cmp2);
        }*/
        component.set("v.sortField",event.currentTarget.title);
        var dataToSort = component.get("v.data");
    	helper.sortData(component,event.currentTarget.title,dataToSort,'sorteddata','sortAsc','sortField');    
        dataToSort = [];
        component.set("v.data",'');
        component.set("v.data",component.get("v.sorteddata"));
        helper.dataChanged(component);
        helper.renderPage(component);
    },
    //End of code - Mudit - 08/02/18 - Bug 62906
})