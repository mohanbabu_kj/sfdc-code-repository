({
	callToServer : function(component, method, callback, params) {
        console.log('Calling helper callToServer function');
        //Start of Code - MB - 07/27/18 - Bug 62905
        var _spinner = component.find("spinner");
        $A.util.removeClass(_spinner, "slds-hide");
        $A.util.addClass(_spinner, "slds-show");
        //End of Code - MB - 07/27/18 - Bug 62905
		var action = component.get(method);
        if(params){
            action.setParams(params);
        }
        console.log('****param to controller:'+JSON.stringify(params));
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback.call(this,response.getReturnValue());
            }else if(state === "ERROR"){
                alert('Problem with connection. Please try again.');
            }
            console.log('End of Server call');
            //Start of Code - MB - 07/27/18 - Bug 62905
            //var _spinner = component.find("spinner");
            $A.util.removeClass(_spinner, "slds-show");
            $A.util.addClass(_spinner, "slds-hide");
            //End of Code - MB - 07/27/18 - Bug 62905
        });
		$A.enqueueAction(action);
    },
    //Start of code - Mudit - 08/02/18 - Bug 62906
    sortData: function ( component, field,data,setValueIn,sortVariable,sortFieldName ) {
        var sortAscVar = component.get( "v."+sortVariable ),
            sortField = component.get("v." + sortFieldName);
        sortAscVar = sortField != field || !sortAscVar;
        data.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAscVar?-1:1)*(t2?1:-1);
        });
        component.set("v."+sortVariable, sortAscVar);
        component.set("v."+sortFieldName, field);
        component.set("v."+setValueIn, data);
    },
    //End of code - Mudit - 08/02/18 - Bug 62906

    
    //Begin Pagination helper methods
    dataChanged: function(component) {
        var pageNumber = 1;
        component.set("v.pageNumber", pageNumber);
    },
	renderPage: function(component) {
        console.log('renderPage');
        var records = component.get("v.data");
                   
        console.log('Records: ' + JSON.stringify(records));
        var device = $A.get("$Browser.formFactor");
        if( device != 'PHONE' ){
            
            var pageSize = component.get("v.pageSize");

            if (records != null && records.length > 0) {
                var pageNumber = component.get("v.pageNumber");
                var pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);
                var maxPage = Math.floor((records.length+(pageSize - 1))/pageSize);
                
                console.log('### pagination - pageNumber: ' + pageNumber);
                console.log('### pageRecords - pageRecords: ' + pageRecords);
                console.log('### pageRecords - maxPage: ' + maxPage);
                
                component.set("v.currentData", pageRecords);
                component.set("v.maxPage", maxPage);
            }
        }
        else {
            component.set("v.currentData", records);
        }
	}  
    //End Pagination helper methods
})