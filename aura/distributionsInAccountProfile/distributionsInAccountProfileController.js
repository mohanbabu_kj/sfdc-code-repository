({
    doInit : function(component, event, helper) {
        
        component.set("v.toggleSpinner",true);
        var action = component.get("c.getAllDistributions");
        var recId = component.get("v.recordId");
        action.setParams({
            recId : recId,
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                component.set('v.accountProfile', response.getReturnValue());
                var apSettingObj;
                // if( response.getReturnValue() != null){
                apSettingObj = {
                    Id: response.getReturnValue().Primary_Business__c,  
                    Name: response.getReturnValue().Primary_Business__c ? response.getReturnValue().Primary_Business__r.Name : '',
                    sobjectType: "Account_Profile_Settings__c",
                };
                //alert('apSettingObj::'+apSettingObj);
                // }
                component.set('v.selectedAPSetting', apSettingObj);//added by susmitha for bug 53357 on jan 15/2018
                // component.find('disId').setRecord(apSettingObj);// commented by susmitha for bug 53357 on jan 15/2018
            }
        });
        $A.enqueueAction(action);  
        
        //PAGINATION        
        helper.getHistory(component);
        helper.creatingPicklistHelper(component, event, helper);
        
    },
    updateVal : function(component, event, helper){
        component.set("v.toggleSpinner",true);
        component.set("v.isValidationError",false);
        //debugger;
        var apSetting = component.get('v.selectedAPSetting');
        //Added by Susmitha on Jan 17 for bug 53359
        var isTotal100forStockAndSpecialOrder=helper.totalValueBooleanHelper( component);
        var isTotal100=helper.totalValueBooleanHelper( component);
        if( isTotal100 == true ){
            var action = component.get("c.updateDistributions");
            if(apSetting.Name!=''){
                component.set('v.accountProfile.Primary_Business__c', apSetting.Id);
            }
            var ap = component.get('v.accountProfile');
            action.setParams({
                ap : ap,
            });
            //  alert(ap.Cut_Order__c);
            action.setCallback(this, function(response){
                var state = response.getState();
                // alert('entry');
                if ( component.isValid() && state === "SUCCESS" ) {
                    var res = response.getReturnValue()
                    if( res ){
                        //ERROR OCCURE
                        component.set("v.isValidationError",true);
                        component.set("v.errorMsg",$A.get("$Label.c.UpdatePrimaryInputError") );
                    }else{
                        var device = $A.get("$Browser.formFactor");
                		if( device == 'PHONE' ){
                            var appEvent = $A.get("e.c:DistributionInAPEvent");
                            appEvent.fire();
                        }
                        /* COMMENT BY MUDIT CALLING ROLLUP MAT HERE
                    	var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    	dismissActionPanel.fire();
                    	$A.get('e.force:refreshView').fire();
                    	*/
                        helper.checkChainNumberHelper( component, event, helper );
                        component.set("v.toggleSpinner",false);
                        //component.set("v.finishFirstAPCall",true);   
                    }
                }
            });
            $A.enqueueAction(action);  
            
        }else{
            component.set("v.isValidationError",true);
            component.set("v.toggleSpinner",false);
        }
    },
    setUpdatedVal : function(component, event, helper){
        var apSetting = component.get('v.selectedAPSetting');
        var Retail=component.get('v.Retail__c');
        var Multi_Family=component.get('v.Multi_Family__c');
        var Builder=component.get('v.Builder__c');
        var Aladdin_Commercial=component.get('v.Aladdin_Commercial__c');
        
        if(apSetting!=null)
        {
            component.set('v.accountProfile.Product_Category_Allocation_Carpet__c', apSetting.Distribution_Carpet__c);
            component.set('v.accountProfile.Product_Category_Allocation_Cushion__c', apSetting.Distribution_Cushion__c);
            component.set('v.accountProfile.Product_Category_Allocation_Wood__c', apSetting.Distribution_Hardwood__c);
            component.set('v.accountProfile.Product_Category_Allocation_Laminate__c', apSetting.Distribution_Laminate__c);
            component.set('v.accountProfile.Product_Category_Allocation_Tile__c', apSetting.Distribution_Tile__c);
            component.set('v.accountProfile.Product_Category_Allocation_Resilient__c', apSetting.Distribution_Resilient__c);
            
            /*Added by susmitha to pull default values for channel allocation from Account Profile settings*/
            if(apSetting.Retail__c !=null && apSetting.Multi_Family__c!=null && apSetting.Builder__c!=null && apSetting.Aladdin_Commercial__c!=null){
                
                component.set('v.accountProfile.Retail__c', apSetting.Retail__c);
                component.set('v.accountProfile.Multi_Family__c', apSetting.Multi_Family__c);
                component.set('v.accountProfile.Builder__c', apSetting.Builder__c);
                component.set('v.accountProfile.Aladdin_Commercial__c', apSetting.Aladdin_Commercial__c);     
            }
            else{
                component.set('v.accountProfile.Retail__c', 0);
                component.set('v.accountProfile.Multi_Family__c',0);
                component.set('v.accountProfile.Builder__c', 0);
                component.set('v.accountProfile.Aladdin_Commercial__c',0);     
            }
            /*End added by susmitha */
            
        }
    },
    //Added By susmitha on Jan 17 for bug 53357
    closeMessage: function(component, event, helper) {
        var recordId = component.get('v.recordId');
        console.log(recordId);
        
        var appEvent = $A.get("e.c:DistributionInAPEvent");
        appEvent.fire();
		
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        
    },
    prevPage: function(component, event, helper) {
        helper.previousPageHelper(component, event, helper);
    },
    nextPage: function(component, event, helper) {
        helper.nextPageHelper(component, event, helper);
    },
})