({
    doInit : function(component, event, helper) {
        /*alert( 'DO ROLLUP CALL' );
        alert( component.get("v.recordId") );*/
        var action = component.get("c.rollupMat");
        action.setParams({
            recordId : component.get("v.recordId"),
        });
        action.setCallback(this, function(response){
            var state = response.getState();           
            if (component.isValid() && state === "SUCCESS") {
                
                var refreshEvent = $A.get("e.c:APRefreshEvt");
                //refreshEvent.setParams({"isRefresh" : true,"AccountProfileId":component.get("v.recordId") });
                refreshEvent.setParams(
                    {
                        "AccountProfileId":component.get("v.recordId"),
                        "AccountId":component.get("v.accountId")
                    }                    
                );
                refreshEvent.fire();
                
                component.set("v.toggleSpinnerRollUp",false);
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                //$A.get('e.force:refreshView').fire();
                //location.reload();
                
            }
        });
        $A.enqueueAction(action);  
        
    }
})