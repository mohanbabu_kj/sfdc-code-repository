({
    initHelper : function(component, event, helper){
        var recId = component.get('v.recordId');
        var objName = component.get('v.sObjectName');
        var proId;
        var mashupName='';
        var businessOverView = false;
        /*if(objName == 'Account'){
            businessOverView = false;
            //component.set('v.isAccountRecord',true); 
        }
        else{
            businessOverView = true;
            component.set('v.isAccountRecord',false);
            component.set('v.recordId','');
        }*/
        var action = component.get("c.getMashupRecordDisplayNames");
        var recordId = null;
        //Added by MB - 3/23/18
        /*if(businessOverView == false){
            recordId = component.get('v.recordId');
        }*/
        action.setParams({
            'recordId': recordId
        });//End of code - MB - 3/23/18
        action.setCallback(this, function(response) {
            var wrapperClassValue = response.getReturnValue();
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                component.set('v.mashupRecords', wrapperClassValue.BOCommercialMashupRecords);
            }
            var mashupMap=component.get('v.mashupRecords');
            var mapTemp=[];
            var i=0;
            var selected = false;
            for(i=0;i<mashupMap.length;i++){
                if(mashupMap[i].Name == 'Business Overview Page -- Commercial'){
                    component.set("v.mashupName",mashupMap[i].Name);
                	component.set('v.iframeUrl', mashupMap[i].Target_URL__c);
                    component.set("v.selectedValue", mashupMap[i].Name);
                    component.set("v.selected", true);
                }
                mapTemp.push({
                    'Name': mashupMap[i].Name,
                    'Target_URL__c': mashupMap[i].Target_URL__c
                });
                
            }
            component.set('v.mashupNameUrlMap',mapTemp);
        });
        $A.enqueueAction(action);
    },
    
    
    setMashupNameMethodHelper: function(component, event, helper){
        var mashupMap=component.get('v.mashupNameUrlMap');
        var key= component.find("Select").get("v.value");
        console.log(mashupMap);
        for(var i=0;i<mashupMap.length;i++){
            if(mashupMap[i].Name==key){
               component.set('v.iframeUrl',mashupMap[i].Target_URL__c);
                //component.set('v.mashupName',mashupMap[i].Name);
                console.log(mashupMap[i].Target_URL__c);
                /*if(component.get('v.isPhone')){
                  //  sforce.one,navigateToURL("https://mohawkdev--pwcdev--c.cs59.visual.force.com/apex/businessOverview", true)
                   // window.open("https://mohawkdev--pwcdev--c.cs59.visual.force.com/apex/businessOverview", "_self");
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                      "isredirect" : true,
                      "url": 'https://mohawkdev--pwcdev--c.cs59.visual.force.com/apex/businessOverview'
                    });
                    urlEvent.fire();
                } else {
                    component.set('v.iframeUrl',mashupMap[i].Target_URL__c);
                }*/
            }
        }
        //console.log(mashupMap);
        //this.forAccountRecordHelper(component, event, helper,key);
    }
})