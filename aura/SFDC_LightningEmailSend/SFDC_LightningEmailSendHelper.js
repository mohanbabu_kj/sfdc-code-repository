({
    sendHelper: function(component, mailParams) {
    // sendHelper: function(component, getEmail, getSubject, getbody) {
        // call the server side controller method 	
        var action = component.get("c.sendMailMethod");
        // set the 3 params to sendMailMethod method   
        action.setParams({
            'mMail': mailParams.to,
            'mSubject': mailParams.subject,
            'mbody': mailParams.body,
            'eventId': component.get('v.recordId'),
            'attachmentString': mailParams.attachments,
            'cc': mailParams.cc?mailParams.cc:'',
            'bcc': mailParams.bcc?mailParams.bcc:''
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if state of server response is comes "SUCCESS",
                // display the success message box by set mailStatus attribute to true
                component.set("v.mailStatus", true);
            }
 
        });
        $A.enqueueAction(action);
    },
    mergeAppointWithTemplateAndAutoFillForm: function(component, appoinmentData, templateData){
        if(templateData!='')
            component.set('v.subject', templateData.Subject);

        var startDateTime = appoinmentData.StartDateTime;
        var endDateTime = appoinmentData.EndDateTime;
        var dateFormat = "MMMM d, yyyy h:mm a";
        var userLocaleLang = $A.get("$Locale.langLocale");
        var appoinmentDataStartDateTime =  $A.localizationService.formatDate(startDateTime, dateFormat, userLocaleLang);
        var appoinmentDataEndDateTime = $A.localizationService.formatDate(endDateTime, dateFormat, userLocaleLang);

        var appoinmentDataObjectives = appoinmentData.Objectives__c;
        console.log(appoinmentDataObjectives)
        if (!appoinmentDataObjectives) {
            appoinmentDataObjectives = ""; 
        }
        // console.log(appoinmentDataObjectives);
        var appoinmentDataObjectivesList = appoinmentDataObjectives.split("・ ");
        appoinmentDataObjectivesList.shift();
        var appoinmentDataObjectivesDisplay = "<ul>";
        for (var i = 0; i<appoinmentDataObjectivesList.length; i++) {
            appoinmentDataObjectivesDisplay += "<li>" + appoinmentDataObjectivesList[i] + "</li>";
        }
        appoinmentDataObjectivesDisplay += "</ul>";
        // '<p>Dear ' + appoinmentData.What.Name + ',</p>'
        var richTextTable = '<p style="font-weight: bold;">Appoinment Details</p>' +
                            '<table cellpadding="0" cellspacing="0" border="0" style="border: none;">'+
                                '<tr>' +
                                    '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Subject</td>' +
                                    '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">'+ appoinmentData.Subject + '</td>'+
                                '</tr>' +
                                '<tr>' + 
                                    '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Start</td>' +
                                    '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">'+ appoinmentDataStartDateTime + '</td>'+
                                '</tr>' + 
                                '<tr>' + 
                                    '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">End</td>' +
                                    '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">'+ appoinmentDataEndDateTime  + '</td>'+
                                '</tr>' + 
                                '<tr>' + 
                                    '<td style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Objectives</td>' +
                                    '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">'+ appoinmentDataObjectivesDisplay + '</td>'+
                                '</tr>' + 
                            '</table>';

        var table = richTextTable;
        var bodyVal = component.get('v.body');
        // console.log(bodyVal)
        component.set("v.body", bodyVal + (templateData!='' ? templateData.HtmlValue + '<br />' : '') + table);
    },
    mergeActionListItemsToBody: function(component, salItems, sectionTitle) {
        if(salItems && salItems!='' && salItems.length>0){
            var sectionHtml = '<div style="height: 30px;line-height: 30px;font-weight:bold;">'+ sectionTitle +'</div>' +
                                '<table cellpadding="0" cellspacing="0" border="0" style="border: none;"'+
                                    '<tr>' +
                                        '<th style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">S.No</th>' +
                                        '<th style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Name</th>' +
                                        '<th style="border-width: 1px;padding: 4px;border-style: solid;border-color: #000000;background-color: #FFEECC;">Description</th>' +
                                    '</tr>';
                
            var richTextTableEnd = '</table>';

            for(var i = 0; i < salItems.length; i++) {
                sectionHtml += '<tr>' + 
                                '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">'+ (i+1) + '</td>'+ 
                                '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">'+ salItems[i].Action_List__r.Name + '</td>'+
                                '<td style="color: #000000;border-width: 1px ;padding: 4px ;border-style: solid ;border-color: #000000;background-color: #FFFFF0;">' + (salItems[i].Action_List__r.Description__c?salItems[i].Action_List__r.Description__c:'') + '</td>' + 
                                '</tr>';
            }
            sectionHtml += '</table>';
            var body = component.get("v.body");
            component.set("v.body", body + sectionHtml);
        }
    },
    removeItem : function(component, index) {
        var salAttachList = component.get("v.salAttachList");
        salAttachList.splice(index, 1);
        component.set("v.salAttachList", salAttachList);
    },
    getAttachList : function(component, eventId) {
        var salAction = component.get("c.salAttachments");
        salAction.setParams({ "eventId" : eventId, 'fetch' : 'id-name' });
        salAction.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // console.dir(response.getReturnValue())
                component.set('v.salAttachList', response.getReturnValue());
            }
        });
        $A.enqueueAction(salAction);
    }
})