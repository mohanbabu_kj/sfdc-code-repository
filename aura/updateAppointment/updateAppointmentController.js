({
    scriptLoaded : function(component, event, helper) {
        // console.log('jquery loaded');'
        /*$('.modal-body').css({
            'height':'50vh',
            'max-height':'50vh'
        })*/
    },
    doInit : function(component, event, helper) {
        var action = component.get("c.getObjectRecord"); 
        console.log(component.get("v.recordId"));
        var fieldMap = new Map();
        fieldMap.toggleModal = false;
        fieldMap.message = '';
        fieldMap.title = '';
        //component.set("v.toggleModal",false); // ADDED BY MUDIT ON 04/03/2018 - BUG - 57395
        component.set("v.fieldMap", fieldMap);
        action.setParams({
            'apiName': 'Event',
            'recordId': component.get("v.recordId"),
            'fields': 'Id, Subject, Status__c,Confirmation1__c,Confirmation2__c,Confirmation3__c,Confirmation4__c,Confirmation5__c,WhatId,Account__r.Name,Account__c,Account.Name,Accountid'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS" && result!=null && result.length>0){
                if(result[0].Status__c != 'Completed'){
                    result[0].Status__c = 'Completed';
                    console.log('ii');
                }
                component.set('v.eventObject', result[0]);
                component.set('v.isVisible',false);
                if( !component.get("v.eventObject.Account.Name") ){                    
                	component.set("v.showError",true);
                }
            } else {
                console.log(component.isValid() + ': '+ state+ ': ' + result)
                //component.set("v.toggleModal",true); // ADDED BY MUDIT ON 04/03/2018 - BUG - 57395
                //component.set("v.message", 'No Appointment(s) found for today!');
                fieldMap.toggleModal = true;
                fieldMap.title = 'Error';
                fieldMap.message = 'No Appointment(s) found for today!';
                component.set('v.fieldMap', fieldMap);
                $A.util.addClass(component.find('main-content'),'slds-hide');
            }
            
        });
        $A.enqueueAction(action);

        //Start of Code -  MB - 3/1/18
        var confirmationAction = component.get("c.getBusinessType");
        confirmationAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                component.set("v.businessType", response.getReturnValue());
                 var businessType = component.get("v.businessType");
                //var ConfText = JSON.parse(component.get("v.ConfirmationTexts"));
                console.log(response.getReturnValue());
                console.log($A.get("$Label.c.C_Confirmation_1"));
                if (businessType == 'Commercial'){
                    component.set('v.Confirmation1', $A.get("$Label.c.C_Confirmation_1"));
                    component.set('v.Confirmation2', $A.get("$Label.c.C_Confirmation_2"));
                    component.set('v.Confirmation3', $A.get("$Label.c.C_Confirmation_3"));
                    component.set('v.Confirmation4', $A.get("$Label.c.C_Confirmation_4"));
                    component.set('v.Confirmation5', $A.get("$Label.c.C_Confirmation_5"));
                }
                else if (businessType == 'Residential'){
                    component.set('v.Confirmation1', $A.get("$Label.c.R_Confirmation_1"));
                    component.set('v.Confirmation2', $A.get("$Label.c.R_Confirmation_2"));
                    component.set('v.Confirmation3', $A.get("$Label.c.R_Confirmation_3"));
                    component.set('v.Confirmation4', $A.get("$Label.c.R_Confirmation_4"));
                    component.set('v.Confirmation5', $A.get("$Label.c.R_Confirmation_5"));
                }
                
                console.log(component.get("v.ConfirmationTexts"));
            }else{
                alert('Error Occurred');
            }
        });
        $A.enqueueAction(confirmationAction);
        //End of Code - MB - 3/1/18
        // var recordId = component.get("v.recordId");
        var fieldList = '';

        var fieldAction = component.get("c.getContactFieldInfo");
        // fieldAction.setParams({ "recordId" : recordId });
        fieldAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                fieldList = response.getReturnValue();
                console.log(fieldList)
                component.set('v.fieldVisibleList', fieldList);
                var container = component.find('formFields');
                if( component.get("v.eventObject.Account.Name") ){                    
                	helper.buildFormFields(fieldList, component, container);    
                }
                // console.log(component.get('v.fieldVisibleList'));
            }
        });
        $A.enqueueAction(fieldAction);
    }, 
    
    createContact : function(component, event, helper) {
        helper.clearFormData(component);
        var checkout = component.find("checkout");
        var create = component.find("create");
        var header = component.find("header");
        $A.util.toggleClass(checkout, 'hide');
        $A.util.toggleClass(create, 'hide');
        $A.util.toggleClass(header, 'hide');
    },
    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper, true);
    },
    showRelatedContract:function(component, event, helper){

        component.set('v.isVisible',true);
    },
    saveContact : function(component, event, helper) {
        var checkout = component.find("checkout");
        var create = component.find("create");
        
        var contactObj = helper.getFormData(component);
        var eventWhatId = component.get('v.eventObject.WhatId');
        //contactObj.AccountId = eventWhatId.substring(0,3)=='001' ? eventWhatId : '';
        contactObj.AccountId = component.get("v.eventObject.AccountId");
        
        component.set( 'v.statusMessage', '');
        // Begin of Changes - SS - 71609.
        if ( contactObj.LastName.length == 0 ){
            component.set( 'v.statusMessage', 'Please enter First and Last Name.');
            return false;
        } else if (contactObj.MobilePhone.length == 0 || contactObj.Email.lenght == 0) {
            component.set( 'v.statusMessage', 'Please enter Phone or email address.');
            return false;
        }      
     
        var saveAction = component.get('c.saveContactRecord');
        saveAction.setParams({
            "contactObj": contactObj,
            "eventId": component.get('v.recordId')
        });
        saveAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS"){
                var result = response.getReturnValue();
                if(result == 'success'){
                    component.set('v.statusMessage', 'Contact saved successfully and updated to Appointment');
                    helper.goBack(component, event, helper, true);
                    component.find('relatedContactCmp').updateRelatedContacts();
                } else {
                    //component.set('v.statusMessage', 'Internal error: Unable to save Contact');
                    component.set( 'v.statusMessage',result );
                }
            } else {
                helper.showErrorMessage(component, response.getError());
            }
        });
        $A.enqueueAction(saveAction);
    },
    updateItem : function(component, event, helper) {
        var updateAction = component.get('c.updateAppoinment');
        var actionStatus = '';
        var eventObject = component.get('v.eventObject');
        var whatAccountId = eventObject.WhatId;
		var fieldMap = component.get('v.fieldMap');
        var relContactFunc = component.find('relatedContactCmp').updateContacts;
        relContactFunc();

        var appointmentObject = eventObject;

        // console.dir(appointmentObject)
        updateAction.setParams({
            "eventObject": appointmentObject,
            'notes': component.find("inputnote").get("v.value")
        });
        console.dir(updateAction.getParams());
        // new fields inclusion in the component 12/21/2016
        updateAction.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(response.getReturnValue());
            if (component.isValid() && state === "SUCCESS"){
                if(!result.error){
                    actionStatus = 'success';
                    var isNotMap = component.get('v.isNotMapAnything');
                    if(isNotMap){
                        helper.showToast(component, actionStatus, 'Event record updated.');
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        $A.get('e.force:refreshView').fire();
                    }else{
                        fieldMap.toggleModal = true;
                        fieldMap.title = 'Success';
                    	fieldMap.message = 'Event record updated.';
                    }
                }else{
                    actionStatus = 'error';
                    fieldMap.toggleModal = true;
                    fieldMap.title = 'Error';
                    fieldMap.message = result.message;
                }
            }else{
                actionStatus = 'error';
                fieldMap.toggleModal = true;
                fieldMap.title = 'Error';
                fieldMap.message = 'Error occurred while checkout process. Please try again after sometime. If error still persist, contact Salesforce Administrator';
            }
            component.set('v.fieldMap', fieldMap);
            //component.set('v.actionStatus', actionStatus);
            
            /*setTimeout(function() {
                // Close the action panel
                var isNotMap = component.get('v.isNotMapAnything');
                if(isNotMap)
                {
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                     $A.get('e.force:refreshView').fire();
                }
               
            }, 1000); */
        });
        $A.enqueueAction(updateAction);
    },
    
    dismissModal:function(component, event, helper){
        if(component.get('v.isNotMapAnything')){
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
        }else{
            window.close();
        }
    },
    
    //ADDED BY MUDIT ON 04/03/2018 - BUG - 57395
    closeErrorModal:function(cmp, evt, helper){
        cmp.set("v.fieldMap.toggleModal",false);
        window.close();
    }
})