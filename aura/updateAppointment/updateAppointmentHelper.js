({
    buildFormFields : function(fieldVisibleList, component, container){
        var fieldType='';
        var fieldData='';
        var chosenFields = new Array();
        
        var compStatus = 'edit';
        var readonly = (compStatus == 'edit' ? false : true);
        
        var componentType = 'lightning:input';
        
        for(var i in fieldVisibleList) {
            fieldData = fieldVisibleList[i];
            fieldType='';
            // console.log(fieldData.apiName, fieldData.type)
            switch(fieldData.type){
                case 'TEXT':
                case 'STRING':
                    fieldType='text';
                    break;
                case 'TEXTAREA':
                    fieldType='textarea';
                    break;
                case 'URL':
                    fieldType='url';
                    break;
                case 'DATE':
                    fieldType='date';
                    break;
                case 'DATETIME':
                    fieldType='datetime-local';
                    break;
                case 'BOOLEAN':
                    fieldType='checkbox';
                    break;
                case 'PHONE':
                    fieldType='tel';
                    break;
                case 'EMAIL':
                    fieldType='email';
                    break;
                case 'PICKLIST':
                case 'MULTIPICKLIST':
                    fieldType='select';
                    break;
                default:
                    break;
            }
            if(fieldType!='' && (chosenFields.length==0 || chosenFields.indexOf(fieldData.apiName)==-1)){
                var compName = componentType;
                if(fieldType=='select')
                    compName = 'lightning:select';
                else if(fieldType=='textarea')
                    compName = 'lightning:textarea';
                
                this.genLightField(fieldData, compName, fieldType, component, container, readonly);
            }
        }
    },
    
    genLightField: function(fieldData, componentType, fieldType, component, container, readonly) {
        
        var fieldName = fieldData.apiName;
        
        if(component.find(fieldName)==undefined){
            $A.createComponent(
                componentType,
                {
                    'name': fieldName,
                    'type': fieldType,
                    'class': 'input-fields',
                    'label': fieldData.fieldLabel,
                    'readonly': readonly,
                    'checked': false,
                    'value':'',
                    'aura:id': 'fieldId'
                    // 'pattern': (fieldType=='tel'?'([0-9]{3}) [0-9]{3}-[0-9]{4}':''),
                    // 'aura:id': fieldName
                },
                function(element, status, errorMessage){
                    if(status == 'SUCCESS'){
                        $A.createComponent('div', 
                                           { class: 'slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2'}, 
                                           function(wrapElement, wrapStatus, error){
                                               if(wrapStatus == 'SUCCESS'){
                                                   var wrapBody = wrapElement.get('v.body');
                                                   
                                                   if(fieldType == 'select'){
                                                       element.set('v.body', []);
                                                       var selectElem = element.get('v.body');
                                                       
                                                       var selectOptions = fieldData.options;
                                                       selectOptions.forEach(function (opt) {
                                                           $A.createComponent('aura:html', { tag: 'option', HTMLAttributes: { value: opt, text: opt }},
                                                                              function(newOption){
                                                                                  if(newOption){
                                                                                      selectElem.push(newOption);
                                                                                      element.set('v.body', selectElem);
                                                                                  }
                                                                              });
                                                       });
                                                       element.set('v.value', selectOptions[0]);
                                                   }
                                                   
                                                   wrapBody.push(element);
                                                   wrapElement.set('v.body', wrapBody);
                                                   var body = container.get('v.body');
                                                   body.push(wrapElement);
                                                   container.set('v.body', body);
                                               }
                                           }
                                          );
                        
                    } else
                        console.log(errorMessage)
                        }
            );
        }
    },
    
    getFormData: function(component) {
        var fieldVisibleList = component.get('v.fieldVisibleList');
        
        var contactObj = {};
        
        /*jQuery('.input-fields .slds-input, .input-fields .slds-textarea, .input-fields .slds-select, .input-fields input[type="checkbox"]').each(function(){
                var elem = jQuery(this);
             //  alert('test');
                contactObj[elem.attr('name')] = elem.attr('type')=='checkbox'?elem.is(':checked'):elem.val();
            });*/
        /* $('.input-fields .slds-input, .input-fields .slds-textarea, .input-fields .slds-select, .input-fields input[type="checkbox"]').each(function(){
                var elem = $(this);
                alert('test');
                contactObj[elem.attr('name')] = elem.attr('type')=='checkbox'?elem.is(':checked'):elem.val();
            });*/
        var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            // inputCmp.showHelpMessageIfInvalid();
            console.log(inputCmp);
            console.log(inputCmp.get("v.value"));
            console.log(inputCmp.get("v.name"));
            console.log(inputCmp.get("v.type"));
            contactObj[inputCmp.get("v.name")] = inputCmp.get("v.type")=='checkbox'?true:inputCmp.get("v.value");
            
            return true;
        }, true);
        console.log(contactObj);
        contactObj['sobjectType'] = 'Contact';
        // console.log(contactObj);
        return contactObj;
    },
    
    clearFormData: function(component) {// 03/21/2017
        jQuery('.contactForm')[0].reset();
    },
    
    isNull : function(text){// 03/21/2017
        return (text == undefined || text == '' ? true : false);
    },
    
    goBack : function(component, event, helper, isSuccess) {
        var isSuccess = $A.util.isUndefinedOrNull(isSuccess) ? false : isSuccess;
        var checkout = component.find("checkout");
        var header = component.find("header");
        var create = component.find("create");
        // Begin of changes - 71609
        if(isSuccess){           
                $A.util.toggleClass(checkout, 'hide');
                $A.util.toggleClass(create, 'hide');
                $A.util.toggleClass(header, 'hide');
                component.set('v.statusMessage', '');
            }
        
        
    },
    
    showErrorMessage : function(component, errorObj){// 03/21/2017
        // console.dir(errorObj);
        var message = 'Unable to process your request.';
        if (errorObj) {
            /*if (errorObj[0] && errorObj[0].fieldErrors && !this.isNull(errorObj[0].fieldErrors[0])) {
                    if(!this.isNull(errorObj[0].fieldErrors[0].message))
                        message += ' Reason: ' + errorObj[0].fieldErrors[0].message;
                }*/
            if (errorObj[0] && errorObj[0].fieldErrors){
                var fErrors = errorObj[0].fieldErrors;
                for(field in fErrors){
                    //console.log(fErrors[field]);
                    message += ' Reason: Missing ' + field + ' - ' + fErrors[field][0].message;
                }
            }
            if (errorObj[0] && errorObj[0].pageErrors && !this.isNull(errorObj[0].pageErrors[0])) {
                if(!this.isNull(errorObj[0].pageErrors[0].message))
                    message += ' Reason: ' + errorObj[0].pageErrors[0].message;
            }
            component.set('v.statusMessage', message);
        } else {
            component.set('v.statusMessage', message);
        }
    },
    
    showToast : function(component, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            mode: 'dismissible',
            message: message
        });
        toastEvent.fire();
    }
})