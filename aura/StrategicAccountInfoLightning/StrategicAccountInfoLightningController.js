({
    init : function(component, event, helper){
        try{
            var recordsUi = event.getParam('recordUi');
        	var fields;
        	var selectedRE = new Object();
           
            if( !$A.util.isUndefinedOrNull(recordsUi)){
                var record = recordsUi.record;
                fields = record.fields;
                console.log('fields: ' + JSON.stringify(fields));
                component.set("v.SelectedMVPCoordinator", fields.MVP_Coordinator__c.value);
                if(!$A.util.isUndefinedOrNull(fields.MVP_Coordinator__c.value)){
                   helper.getMVPInfoHelper(component, event,fields.MVP_Coordinator__c.value); 
                }
                component.set('v.isStrategicAccount', fields.Strategic_Account__c.value);
               if(!$A.util.isUndefinedOrNull(fields.Real_Estate_Service_Provider__c.value))
                 selectedRE.Id = fields.Real_Estate_Service_Provider__c.value;
                
                console.log('log::'+JSON.stringify(recordsUi.record.fields["Real_Estate_Service_Provider__c"]));
                if(!$A.util.isUndefinedOrNull(selectedRE.Id) && !$A.util.isUndefinedOrNull(recordsUi.record.fields["Real_Estate_Service_Provider__c"].displayValue))
                  selectedRE.Name = fields.Real_Estate_Service_Provider__r.displayValue;
                //console.log(recordsUi.record.fields["Real_Estate_Service_Provider__c"].value.fields);
                
                //selectedRE.Name = recordsUi.record.fields["Real_Estate_Service_Provider__c"].value.fields.Name;
                if(!$A.util.isUndefinedOrNull(selectedRE))
                 component.set('v.selectedLookUpRecord', selectedRE);
                
                console.log('Str.Acc: ' + fields.Strategic_Account__c.value);
                
                console.log('selectedLookUpRecord: ' + JSON.stringify(component.get('v.selectedLookUpRecord')));
            }
            
            if(component.get('v.status') === 'view' || fields.Approval_Status__c.value === ''){
                helper.initHelperMethod(component, event, helper);
            }
        }catch(err){
            console.log('Error::'+err);
        }
        console.log('Submit for Approval: ' + component.get('v.submitForApproval'));
        //$A.util.removeClass(component.find("Account_Geography__c"), "none");
        /*if(recUi.record.id !==null){
            component.set("v.status", "edit");
        }else{
           component.set("v.status", "view"); 
        }
        alert( component.get("v.status"));*/
        /*var fieldMap = new Map();
        fieldMap.isError = false;
        fieldMap.isErrorContractDate = false;
        fieldMap.contractErrorMsg = '';
        component.set('v.fieldMap', fieldMap);*/
        //component.set('v.isCurrentUserAdmin', true);
        //component.set('v.status', view);
        //alert('message');
        /*var recordsUi = event.getParam('recordUi');
        var record = recordsUi.record;
      var fields = record.fields;
            console.log(JSON.stringify(recordsUi));
      console.log('field: ' + JSON.stringify(fields));*/
		//alert(fields.Account_Geography__c);
    },
    
    onEdit: function(component, event, helper){
        component.set('v.status', 'edit');
    },
    
	handleSubmit : function(component, event, helper) {
        
        event.preventDefault();
        var fields = event.getParam('fields');
        if(fields !== undefined){
            if(component.get('v.isCurrentUserAdmin')){
                fields.MVP_Coordinator__c = component.find('MVP_Coordinator__c').get("v.value");
            }
            console.log('Handle Submit fields: ' + JSON.stringify(fields));
            if(component.get('v.submitForApproval')){
                fields.Approval_Status__c = 'Submitted';
            }
            
        }
        if(helper.ValidateContractDate(component, event, fields) &&
           helper.ValidateRecord(component, event, fields)){
           var realEstateServiceProvider = component.get("v.selectedLookUpRecord"); 
            console.log('realEstateServiceProvider: ' + JSON.stringify(realEstateServiceProvider));
            if( ! $A.util.isEmpty( realEstateServiceProvider.Id ) && fields !== undefined){
                console.log('realEstateServiceProvider.Id: ' + realEstateServiceProvider.Id);
                fields.Real_Estate_Service_Provider__c=realEstateServiceProvider.Id;
            } 
           component.find("recordEditForm").submit(fields); 
            console.log('Submit');
        }
        
        //alert(fields.Contract_Start_date__c);
        // alert(fields.Contract_End_Date__c);
        //alert(component.find('accountGeo').get('v.value'));
        
	},
    
    handleSuccess : function(component, event, helper){
        console.log('Success');
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    handleError : function(component, event, helper){
        console.log('Error Occurred'+JSON.stringify(event)+event.fieldErrors);
        component.set('v.isError', true);
    },
    
    handleFieldChange: function(component, event, helper) {
       
        //component.set("v.toggleSpinner",true);
        /*var fieldName = component.get("v.SelectedMVPCoordinator");
        if( fieldName != '' ){
            helper.getMVPInfoHelper(component, event,fieldName); 
        }    
        else{
            component.set("v.account.MVP_Phone__c",'');
            component.set("v.account.MVP_Email__c",'');
            component.set("v.toggleSpinner",false);
        }*/

    },
    
    cancel : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    closeAlertMsg: function(component, event, helper){
        component.set("v.isErrorContractDate",false);
        component.set('v.contractErrorMsg', '');
        //component.set('v.errorOccurred', false);
    }
    
})