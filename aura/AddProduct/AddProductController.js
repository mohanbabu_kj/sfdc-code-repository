({
    doInit: function (component, event, helper) {
        
        console.log("**Object Name: " + component.get("v.sObjectName"));
        /* Comment by Mohan
        //alert(component.get("v.sObjectName"));
        if(component.get("v.sObjectName") != "Opportunity" ){
            component.set("v.showBackButton",true);
        } */
        
        //Added by Mohan - Display the back button only when its invoked on the tab
        if(component.get("v.recordId") == "" ){
            component.set("v.showBackButton",false);
        }        
        
        // debugger
        console.log(' recId :: 1212##'+component.get("v.recordId"));
        if(component.get("v.recordId") === ""){
            component.set("v.disableNav", true);
        }        
        
        // document.body.setAttribute('style', 'overflow: hidden;');
        // var spinner = component.find('spinner');
        // $A.util.addClass(spinner, 'slds-show');
        // $A.util.removeClass(spinner, 'slds-hide');
        //alert(component.get('v.listButton'));
        helper.getSubCategoryPickListvalues(component, event);

        helper.getLoggedinuserBusinessTypeInfo(component, event);
        helper.getProductCollectionNamePicklistValues(component, event);
        helper.getProductCollectionBrandPicklistValues(component, event);
        helper.getProductProductTypePicklistValues(component, event);

        helper.initProductList(component, event);
        component.set("v.currency", "USD");
        
    },
    
    search: function (component, event, helper) {
        
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
        $A.util.removeClass(spinner, 'slds-hide');
        // debugger
        /*var productDetail = document.getElementById('productDetail');
        if(productDetail.className!=null){
            productDetail.className = 'hide';}*/
        //Start of Code - MB - 3/20/18
        var productDetail = component.find('productDetail');
        $A.util.removeClass(productDetail, 'slds-show');
        $A.util.addClass(productDetail, 'slds-hide');
        //End of Code - MB - 3/20/18
        // debugger
        //var name = document.getElementById('style-name').value; - Commented by MB - 06/21/17
        //var genericProduct = document.getElementById('genericProduct').checked;
        var genericProduct=component.get('v.GenericProduct');
        var userInfo = component.get('v.loginUserInfo');
        var params = {};
        var projectId = component.get('v.recordId');
        if(userInfo === 'Commercial') {
            //var name = document.getElementById('style-name-com').value; // Added by MB -06/21/17
            var name=component.get("v.Name");
            //var subCategory=document.getElementById('sub-category').value;//Added by Susmitha May 22,2018
            var subCategory=component.find('subcategory').get("v.value");
            //var number = document.getElementById('number').value;
            var number=component.get("v.StyleNum");
            //var quickShip = document.getElementById('quickShip').checked; // Added by MB -07/13/17
            var quickShip=component.get("v.quickShip");
            //var brandDescription = document.getElementById('brandDescription').value;
            var brandDescription=component.find('brandDescription').get("v.value");
            params = {
                'StyleNames': name,
                'StyleNumber': number,
                'Brand': '',
                'ProductType': '',
                'Collection': brandDescription,
                'GenericProduct': genericProduct,
                'QuickShip': quickShip,// Added by MB -07/13/17
                'recId':projectId,//Start By LJ Jan-31-2018(product.isExists to check if product already added to the project)
                'SubCategory':subCategory
            }
        } else if(userInfo === 'Residential') {
            var name = document.getElementById('style-name-res').value; // Added by MB -06/21/17
            var productType = document.getElementById('productType').value;
            var brand = document.getElementById('brand').value;
            params = {
                'StyleNames': name,
                'StyleNumber': '',
                'Brand': brand,
                'ProductType': productType,
                'Collection': '',
                'GenericProduct': genericProduct,
                'recId':projectId, //Start By LJ Jan-31-2018(product.isExists to check if product already added to the project)
                'SubCategory':''

                //'QuickShip': quickShip // Added by MB -07/13/17 - Commented by MB - 11/16/17
            }
        }
        // var name = document.getElementById('style-name').value;
        // var number = document.getElementById('number').value;
        // var brandDescription = document.getElementById('brandDescription').value;
        // var productType = document.getElementById('productType').value;
        // var brand = document.getElementById('brand').value;
        // var genericProduct = document.getElementById('genericProduct').checked;
        // var number = component.find('number').get('v.value');
        // var brandDescription = component.find('brandDescription').get('v.value');
        // var genericProduct = component.find('genericProduct').get('v.value');
        component.set('v.selectProductAllChecked', '')
        component.set('v.hasMoreRecords', false); // Added by MB - 06/21/17
        var action = component.get('c.getproductlist');
        action.setParams(params);
        console.log(action.getParams())
        action.setCallback(this, function (response) {
            // debugger
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                //Added by Susmitha May 24
                var productListReturned=response.getReturnValue();
                //console.log(productListReturned);
                var tempList=[];
                var productListPreviouslySelected=component.get('v.productListSelected');
              //  alert('productListPreviouslySelected'+productListPreviouslySelected.length);
                
                for (var i = 0; i < productListPreviouslySelected.length; i++) {
                    tempList.push(productListPreviouslySelected[i].pr.Id);
                }
                for (var i = 0; i < productListReturned.length; i++) {
                    if (tempList.indexOf(productListReturned[i].pr.Id) > -1) {
                        productListReturned[i].isSelected=true;
                    }
                }
                /* for (var i = 0; i < productListReturned.length; i++) {
                for (var j = 0; j < productListPreviouslySelected.length; j++) {
                    if(productListReturned[i].pr.Id==productListPreviouslySelected[j].pr.Id){
                        productListReturned[i].isSelected=true;
                    }
                }
                }*/
                
                    /*
                    if (productListPreviouslySelected.indexOf(productListReturned[i]) > -1) {
                        productListReturned[i].isSelected=true;
                        alert('entry');
                    }*/
                

                component.set('v.productList', productListReturned);
                //End Added By Susmitha May 24,2018
                
                console.log(response.getReturnValue());
                $A.util.addClass(spinner, 'slds-hide');
                $A.util.removeClass(spinner, 'slds-show');
                /*var productDetail = document.getElementById('productDetail');
                if(productDetail.className!=null){
                    
                    productDetail.className = 'show';
                }*/
                //Start of Code - MB - 3/20/18
                var productDetail = component.find('productDetail');
                $A.util.removeClass(productDetail, 'slds-hide');
                $A.util.addClass(productDetail, 'slds-show');
                //End of Code - MB - 3/20/18
                //Added by MB - 06/21/17
                if(response.getReturnValue().length === 20){
                    component.set('v.hasMoreRecords', true);
                }
                //console.log(component.get('v.hasMoreRecords'));
                //console.log(response.getReturnValue().length);
                // var first = document.getElementsByClassName('first-td');
                //
                // first[0].className= 'none';
            }
        });
        $A.enqueueAction(action);
    },
    selectAll: function (cmp, evt, helper) {
        // var selectAll = document.getElementById('selectAll');
        var productList = cmp.get('v.productList');
        var selectAll = cmp.get('v.selectProductAllChecked');
        var productListSelected=[];//
        var productListPreviouslySelected=cmp.get('v.productListSelected');
        cmp.set('v.selectProductAllChecked', 'checked');
        // cmp.set('v.isChecked', '');54
        if (selectAll == '') {
            cmp.set('v.selectProductAllChecked', 'checked');
            // cmp.set('v.isChecked', 'checked');
            for (var i = 0, len = productList.length; i < len; i++) {
                //Start By LJ Jan-31-2018(product.isExists to check if product already added to the project)
                if(productList[i].pr.IsActive && !productList[i].isExists)// added condition on 05/02/2017
                    productList[i].isSelected = true;
            }
            cmp.set('v.nextBtnDisplay', 'true');
            var mergedArrays=productList.concat(productListPreviouslySelected);
            for(var i=0; i<mergedArrays.length; ++i) {
                for(var j=i+1; j<mergedArrays.length; ++j) {
                    if(mergedArrays[i].pr.Id === mergedArrays[j].pr.Id)
                        mergedArrays.splice(j--, 1);
                    if(mergedArrays[i].isSelected===false)
                        mergedArrays.splice(i--, 1);
                    
                }
            }
            cmp.set('v.productList', productList);
            cmp.set('v.productListSelected', mergedArrays);

        } else {
            cmp.set('v.selectProductAllChecked', '')
            // cmp.set('v.isChecked', '');
            for (var i = 0, len = productList.length; i < len; i++) {
          var index = productListPreviouslySelected.indexOf(productList[i]);
                if (index > -1) {
                   // alert('remove');
                    productListPreviouslySelected.splice(index, 1);
				}
                productList[i].isSelected = false;
                cmp.set('v.nextBtnDisplay', 'false');
                
            }
            cmp.set('v.nextBtnDisplay', 'false');
            cmp.set('v.productList', productList);
        }
        
    },
    goToNextPage: function (cmp, evt, helper) {
        // var addProductPage = cmp.find('addProduct');
        // $A.util.addClass(addProductPage, 'hide');
        // $A.util.removeClass(addProductPage, 'show');
        // var saveProductPage = cmp.find('saveProduct');
        // $A.util.addClass(saveProductPage, 'slds-show');
        // $A.util.removeClass(saveProductPage, 'hide'); 
        // Added by MB - 06/21/17 - Condition to check if the product is selected
        console.log("**goToNextPage");
        var prodselected = cmp.get('v.nextBtnDisplay');
        console.log(prodselected);
        var productList = cmp.get('v.productListSelected');

        //if(prodselected == "false"){
        if(productList.length==0){
            alert('Please select at least one product');
        }else{
            localStorage.searchScrollTop = document.body.scrollTop;
            /*var addProductPage = document.getElementById('addProduct');
            if(addProductPage.className!=null){
                addProductPage.className = 'hide';}*/
            //Start of Code - MB - 3/20/18
            var addProduct = cmp.find('addProduct');
            $A.util.removeClass(addProduct, 'slds-show');
            $A.util.addClass(addProduct, 'slds-hide');
            //End of Code - MB - 3/20/18
            /*var saveProductPage = document.getElementById('saveProduct');
            if(saveProductPage.className!=null){
                
                saveProductPage.className = 'show';}*/
            //Start of Code - MB - 3/20/18
            var saveProduct = cmp.find('saveProduct');
            $A.util.removeClass(saveProduct, 'slds-hide');
            $A.util.addClass(saveProduct, 'slds-show');
            //End of Code - MB - 3/20/18
            //Added by MB - 06/21/17 - To display buttons dynamically
            cmp.set('v.cancelBtnDisplay', false);   
            cmp.set('v.hideNextButton', true);
            cmp.set('v.iframePage', false);
            // var productsSelected = cmp.get('v.productList');
            console.log(productList);
            // for(var productIndex = 0; productIndex < productsSelected.length; productIndex++ ) {
            // 	// console.log(productsSelected[productIndex])
            // 	if(productsSelected[productIndex].isSelected === false) {
            // 		productsSelected.splice(productIndex, 1);
            // 	}
            // }
            var productsSelected = productList.slice();
            for (var i = productsSelected.length - 1; i >= 0; i--) {
                if (productsSelected[i].isSelected === false) {
                    productsSelected.splice(i, 1);
                } else {
                    console.log("**Selected product" + productsSelected[i]);
                    /* Commented by Mohan so that Quantity and Price is displayed on the UI
                    productsSelected[i].Quantity = '';
                    productsSelected[i].Price = '';   */
                }
            }
            for(var i=0; i<productsSelected.length; ++i) {
                for(var j=i+1; j<productsSelected.length; ++j) {
                    if(productsSelected[i].pr.Id === productsSelected[j].pr.Id){
                        productsSelected.splice(j--, 1);
                    }
                    if(productsSelected[i].isSelected===false){
                        productsSelected.splice(i--, 1);
                    }

                }
            }
            cmp.set('v.productsSelected', productsSelected);
            console.log(productsSelected)
            // debugger
            document.body.scrollTop = 0;
        }
    },
    //Back to search page
    goBackToSearch: function (cmp, evt) {
        /*var addProductPage = document.getElementById('addProduct');
        if(addProductPage.className!=null){
            
            addProductPage.className = 'show';}*/
        //Start of Code - MB - 3/20/18
        var addProduct = cmp.find('addProduct');
        $A.util.removeClass(addProduct, 'slds-hide');
        $A.util.addClass(addProduct, 'slds-show');
        //End of Code - MB - 3/20/18
        /*var saveProductPage = document.getElementById('saveProduct');
        if(saveProductPage.className!=null){
            
            saveProductPage.className = 'hide';}*/
        //Start of Code - MB - 3/20/18
        var saveProduct = cmp.find('saveProduct');
        $A.util.removeClass(saveProduct, 'slds-show');
        $A.util.addClass(saveProduct, 'slds-hide');
        //End of Code - MB - 3/20/18
        document.body.scrollTop = localStorage.searchScrollTop;
        //Added by MB - 06/21/17 - To display buttons dynamically
        cmp.set('v.cancelBtnDisplay', true);
        cmp.set('v.hideNextButton', false);
        cmp.set('v.iframePage', false);
    },
    //set all selected status
    setSelectedTerritory: function (cmp, evt, helper) {
        cmp.set('v.selectProductAllChecked', '');
        console.log(cmp.get('v.productList'));
        var productListSelected=[];//
        var productListPreviouslySelected=cmp.get('v.productListSelected');
        var productList = cmp.get('v.productList');
        var checkedCount = 0;
                

        for (var i = 0; i < productList.length; i++) {
            if (productList[i].isSelected) {
                checkedCount++;
                productListSelected.push(productList[i]);
            }
            if (productList[i].isSelected===false) {
                var index = productListPreviouslySelected.indexOf(productList[i]);
                if (index > -1) {
                   // alert('remove');
                    productListPreviouslySelected.splice(index, 1);
				}
            }
        }
        var mergedArrays=productListSelected;
        if(productListPreviouslySelected.length > 0){
            mergedArrays=mergedArrays.concat(productListPreviouslySelected);
            for(var i=0; i<mergedArrays.length; ++i) {
                for(var j=i+1; j<mergedArrays.length; ++j) {
                    if(mergedArrays[i].pr.Id === mergedArrays[j].pr.Id)
                        mergedArrays.splice(j--, 1);
                    if(mergedArrays[i].isSelected===false)
                        mergedArrays.splice(i--, 1);
                    
                }
            }
        }
        cmp.set('v.productListSelected',mergedArrays);
       //alert(mergedArrays.length);
        
        
        if (checkedCount > 0) {
            cmp.set('v.nextBtnDisplay', 'true');
        } else {
            cmp.set('v.nextBtnDisplay', 'false');
        }
    },
    //save to backend
    validateField: function (cmp, evt, helper) {
        debugger
        var field = evt.getSource();
        console.log(field.get('v.value'));
        var submitBtn = cmp.find('submit');
        submitBtn.set('v.disabled', field.get('v.value') != '' ? false : true);
    },
    saveToBackend: function (cmp, evt) {
        
        console.log("**saveToBackend");
        var projectId = cmp.get('v.recordId');
        var spinner = cmp.find('spinner');
        var context = cmp.get("v.UserContext"); // Added by MB - 08/08/17
        // console.log('saId'+saId);
        //var saId = 'a092C000001LP4H';
        var product = cmp.get('v.productsSelected');
        console.log("**Product length: " + product.length);
        // debugger
        var continueSave = false;
        var missInputCount = 0;
        
        //Added By Krishnapriya ON OCT 25
        var checkQuantity = false;
        var checkPrice = false;
        var checkPriceQty = false;
        var blankPriceorQty=false;
        var blankPrice=false;
        var blankQty=false;
        
        console.log( product );
        console.log( product.length );
        
        if( product.length > 0 ){
            for ( var i = 0; i < product.length; i++ ) {
                // alert(String(product[i].quantity));
                
                if (( product[i].quantity == '' || product[i].quantity == null || String(product[i].quantity) =='0')&& (product[i].price == '' || product[i].price == null || parseInt( product[i].price ) < 1)){                        
                    cmp.set("v.isErrorQuantityPrice", true);
                    checkPriceQty=true;
                    cmp.set("v.QuantityPriceErrorMsg", 'Please enter Non Zero Quantity and Price for products selected..');
                    missInputCount++;
                    break;
                }
                else if ( product[i].quantity == '' || product[i].quantity == null || String(product[i].quantity) =='0') {                        
                    cmp.set("v.isErrorQuantityPrice", true);
                    checkQuantity=true;
                    cmp.set("v.QuantityPriceErrorMsg", 'Please enter Non Zero Quantity for products selected..');
                    missInputCount++;
                    break;
                }else if ( product[i].price == '' || product[i].price == null ) {                        
                    cmp.set("v.isErrorQuantityPrice", true);
                    checkPrice=true;
                    cmp.set("v.QuantityPriceErrorMsg", 'Please enter Non Zero Price for products selected..');
                    missInputCount++;
                    break;
                }else{
                    if((parseInt( product[i].quantity ) < 1 ||parseInt( product[i].quantity ) ==0) && parseInt( product[i].price ) < 1 ){						
                        cmp.set("v.isErrorQuantityPrice", true);
                        checkPriceQty = true;
                        cmp.set("v.QuantityPriceErrorMsg", 'Please enter Non Zero Quantity & Price for products selected..');
                        missInputCount++;
                        break;
                    }
                    else if( parseInt( product[i].quantity ) < 1 ||parseInt( product[i].quantity ) ==0 ){						 
                        cmp.set("v.isErrorQuantityPrice", true);
                        checkQuantity=true;
                        cmp.set("v.QuantityPriceErrorMsg", 'Please enter Non Zero Quantity for products selected..');
                        missInputCount++;
                        break;
                    }
                        else if( parseInt( product[i].price ) < 1  ){
                            cmp.set("v.isErrorQuantityPrice", true);
                            checkPrice=true;
                            cmp.set("v.QuantityPriceErrorMsg", 'Please enter Non Zero Price for products selected..');
                            missInputCount++;
                            break;
                        }
                }
            }
            if ( missInputCount === 0 )
                continueSave = true;
        }
        
        if (continueSave) {
            // Added by MB - 07/03/17
            $A.util.addClass(spinner, 'slds-show');
            $A.util.removeClass(spinner, 'slds-hide');
            // End of Code - MB - 07/03/17
            console.log(JSON.stringify(product));
            var action = cmp.get('c.AddProductstoProject');
            action.setParams({
                'ProjectId': projectId,
                'SelectedProducts': JSON.stringify(product)
            });
            //debugger;
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (cmp.isValid() && state === 'SUCCESS') {
                    // cmp.set('v.category', response.getReturnValue());
                    var result = response.getReturnValue();
                    if(result == true){
                        console.log(response.getReturnValue());
                        /* commented by MB - 08/08/2017
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        $A.get('e.force:refreshView').fire();
                        End of Comment -MB- 08/08/2017 */
                        // Start of Code -MB- 08/08/2017
                        if(cmp.get('v.listButton')){
                            if (context != undefined) {
                                if (context == 'Theme4t' || context == 'Theme4d') {
                                    console.log("**hello Mohan");
                                    var device = cmp.get('v.device');
                                    if(device == 'DESKTOP')
                                    {
                                        var navEvt = $A.get("e.force:navigateToSObject");
                                        console.log("**navEvt: " + navEvt);                                    
                                        navEvt.setParams({
                                            "isredirect": true,
                                            "recordId": projectId,                                        
                                        });
                                        navEvt.fire(); 
                                    }else
                                    {
                                        sforce.one.navigateToSObject(projectId,"RELATED"); 
                                    }
                                }
                            }
                        }else{
                            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                            dismissActionPanel.fire();
                            $A.get('e.force:refreshView').fire();
                        }
                        // End of Code -MB- 08/08/2017
                    } else {
                        alert('Error occurred while saving products');
                        /*var toast = $A.get('e.force:showToast');
                toast.setParams({
                  'title': 'Error',
                  'type': 'error',
                  'message': 'Error occurred while saving products'
                });
                toast.fire();*/
                    }
                } else {
                    //alert('SYSTEM ERROR')
                }
                // Added by MB - 07/03/17
                $A.util.addClass(spinner, 'slds-hide');
                $A.util.removeClass(spinner, 'slds-show');
                // End of Code - MB - 07/03/17
            });
            $A.enqueueAction(action);
        } else {
            // alert('No product selected');
        }
    },
    // click unabled checked box
    returnFalse: function (cmp, evt, helper) {
        return false;
    },
    gotoProductIframe: function (cmp, evt, helper) {
        // start Added By Lakshman oct-27-2017
        //var userInfo = cmp.get('v.loginUserInfo'); // Added by MB - 11/16/17
        var disableNav = cmp.get("v.disableNav");
        if(disableNav)
        {
            var productId = evt.target.dataset.index;
            helper.redirectToProductRecord(cmp,productId);
        }   
    },
    
    openMHKGroup: function(cmp, evt, helper){
        cmp.set("v.isviewPrice",false);
        cmp.set("v.iframeDisplay",true);
        debugger;
        var viewURL = evt.target.dataset.url;
        if(viewURL!=undefined && viewURL!=''){
            var iframeUrl = viewURL;
            if (cmp.get('v.iframeUrl') != iframeUrl)
                cmp.set('v.iframeUrl', iframeUrl);
            localStorage.scrollTop = document.body.scrollTop;
            
            /*var productDetail = cmp.find('productDetail');
            $A.util.removeClass(productDetail, 'slds-show');
            $A.util.addClass(productDetail, 'slds-hide');*/
            
            /*var productPage = document.getElementById('product-page');
            if(productPage != null && productPage.className!=null){
                productPage.className = 'hide';
            }*/
            
            var productPage = cmp.find('product-page');
            $A.util.removeClass(productPage, 'slds-show');
            $A.util.addClass(productPage, 'slds-hide');
            
            /*var viewPrice = document.getElementById('viewPrice-wrapper');
            if(viewPrice.className!=null){              
                viewPrice.className = 'hide';
            }*/
            
            /*var iframe = document.getElementById('iframe-wrapper');
            if(iframe.className!=null){
                iframe.className = 'show';
            }*/
            var iframe = cmp.find('iframe');
            $A.util.removeClass(iframe, 'slds-hide');
            $A.util.addClass(iframe, 'slds-show');
            
            var viewPrice = cmp.find('viewPrice');
            $A.util.addClass(viewPrice, 'slds-hide');
            $A.util.removeClass(viewPrice, 'slds-show');
            document.body.scrollTop = 0;
            //Added by MB - 06/21/17 - To display buttons dynamically
            cmp.set('v.cancelBtnDisplay', false);
            cmp.set('v.hideNextButton', true);
            cmp.set('v.iframePage', true);  
            
            console.log("**"+ cmp.get("v.recordId"));
            console.log("**" + cmp.get("v.disableNav"));
            if(cmp.get("v.disableNav")){
                cmp.set("v.showBackButton", true); //Added by MB - 3/20/18
            }
            // added by Mohan - 57603 Bug - To display the header when view specs is clicked
            /*if(cmp.get("v.recordId") === ""){
                cmp.set("v.disableNav", false);    
                cmp.set("v.hideCancelButton", true);
            }  -Commented by MB - 3/20/18*/            
            console.log(viewURL);
            
        } else {
            alert('No URL found');
        }
        
        /*var viewURL = event.target.dataset.url;
        if(viewURL!=undefined && viewURL!=''){
            var action = $A.get('e.force:navigateToURL');
            action.setParams({url: viewURL });
            console.log(viewURL);
            action.fire();
        } else {
            alert('No URL found');
        }*/
    },
    goBack: function (cmp, evt, helper) {
        //debugger;
        /*var productPage = document.getElementById('product-page');
        if(productPage != null && productPage.className!=null){
            productPage.className = 'show';}*/
        var productPage = cmp.find('product-page');
        $A.util.removeClass(productPage, 'slds-hide');
        $A.util.addClass(productPage, 'slds-show');
        /*var productDetail = document.getElementById('productDetail');
        if(productDetail.className!=null){
            //debugger; 
            productDetail.className = 'show';
        }*/
        /*var productDetail = cmp.find('productDetail');
            $A.util.removeClass(productDetail, 'slds-hide');
            $A.util.addClass(productDetail, 'slds-show');*/
        
        /*var viewPrice = document.getElementById('viewPrice-wrapper');
        if(viewPrice != null && viewPrice.className!=null){
            viewPrice.className = 'hide';
        }*/
        
        /*var iframe = document.getElementById('iframe-wrapper');
        if(iframe != null && iframe.className!=null){
            iframe.className = 'hide';}*/
        if(cmp.get('v.iframeDisplay') == true){
            var iframe = cmp.find('iframe');
            $A.util.removeClass(iframe, 'slds-show');
            $A.util.addClass(iframe, 'slds-hide');
            cmp.set("v.iframeDisplay",false);
        }
        //if(cmp.get('v.isViewPrice') == true){
            var viewPrice = cmp.find('viewPrice');
            $A.util.removeClass(viewPrice, 'slds-show');
            $A.util.addClass(viewPrice, 'slds-hide');
            cmp.set("v.isviewPrice",false);
        //}
        document.body.scrollTop = localStorage.scrollTop;
        //Added by MB - 06/21/17 - To display buttons dynamically
        cmp.set('v.cancelBtnDisplay', true);
        cmp.set('v.hideNextButton', false);
        cmp.set('v.iframePage', false)
        /*added by susmitha*/
        /*var addProductPage = document.getElementById('addProduct');
        if(addProductPage.className!=null){
            addProductPage.className = 'show';
        }
        
        var saveProductPage = document.getElementById('saveProduct');
        if(saveProductPage.className!=null){
            saveProductPage.className = 'hide';
        }*/
        //Start of Code - MB - 3/20/18
        var addProduct = cmp.find('addProduct');
        $A.util.removeClass(addProduct, 'slds-hide');
        $A.util.addClass(addProduct, 'slds-show');
        //End of Code - MB - 3/20/18
        //Start of Code - MB - 3/20/18
        var saveProduct = cmp.find('saveProduct');
        $A.util.removeClass(saveProduct, 'slds-show');
        $A.util.addClass(saveProduct, 'slds-hide');
        //End of Code - MB - 3/20/18
        //added by Mohan - to display Search area when view Price is clicked
        var cmpTarget = cmp.find('searchForm');
        $A.util.removeClass(cmpTarget, 'hide');   
        
        // added by Mohan - 57603 Bug - remove the header when back button is clicked when on products tab
        if(cmp.get("v.recordId") === ""){
            cmp.set("v.disableNav", true);         
        }         
        if(cmp.get("v.disableNav")){
        	cmp.set("v.showBackButton", false); //Added by MB - 3/20/18
        }
        cmp.set("v.titleName", "Add Products");//Added by MB - 5/29/18
    },
    
    closeMessage: function(component, event, helper) {
        var recordId = component.get('v.recordId');
        console.log(recordId);
        if(component.get('v.listButton')){
            var context = component.get("v.UserContext");
            if (context != undefined) {
                if (context == 'Theme4t' || context == 'Theme4d') {
                    console.log('VF in S1 or LEX');
                    sforce.one.navigateToSObject(recordId, 'detail');
                }
            }
            /*var navEvt = $A.get("e.force:navigateToSObject");
            console.log(navEvt);
            navEvt.setParams({ 
                "recordId": recordId,
                "isredirect": true });
            navEvt.fire();
            //$A.get('e.force:refreshView').fire();*/
        }else{
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
        }
        // $A.get('e.force:refreshView').fire();
    },
    preventEnterEvent: function (component, event, helper) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    },
    //BY SUSMITHA
    closeAlertMsg: function(component, event, helper){
        component.set("v.isErrorQuantityPrice",false);
    },
    //BY SUSMITHA
    viewPrice: function(cmp, evt, helper){
        console.log("**viewPrice function is fired");
        console.log("**showBackButton value: " + cmp.get("v.showBackButton"));
        cmp.set("v.isviewPrice",true);
        cmp.set("v.iframeDisplay", false);
        cmp.set("v.titleName","Price");
        if(cmp.get("v.disableNav")){
        	cmp.set("v.showBackButton", true); //Added by MB - 3/20/18
        }
        var IDvalue = evt.target.dataset.url;
        
        if(IDvalue!=undefined && IDvalue!=''){
            var productData = cmp.get("v.productList");
            var priceData = new Object();
            for (var i = 0; i < productData.length; i++) {
                console.log( productData[i].pr.Id == IDvalue );
                console.log(productData);
                if( productData[i].pr.Id == IDvalue ){	
                    priceData.StyleName = productData[i].pr.Name;
                    priceData.StretchUSD = (productData[i].pr.Stretch_USD__c != undefined ? productData[i].pr.Stretch_USD__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : '$0.00');
                    priceData.FloorUSD = (productData[i].pr.Floor_USD__c != undefined ? productData[i].pr.Floor_USD__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : '$0.00');
                    priceData.MidMarketUSD = (productData[i].pr.Mid_Market_USD__c != undefined ? productData[i].pr.Mid_Market_USD__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : '$0.00');
                    priceData.StretchCAN = (productData[i].pr.Stretch_CAN__c != undefined ? productData[i].pr.Stretch_CAN__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : '$0.00');
                    priceData.MidMarketCAN = (productData[i].pr.Mid_Market_CAN__c != undefined ? productData[i].pr.Mid_Market_CAN__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : '$0.00');
                    priceData.FloorCAN = (productData[i].pr.Mid_Market_CAN__c != undefined ? productData[i].pr.Floor_CAN__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : '$0.00');
                }
            }
            console.log( priceData );
            cmp.set("v.priceTable",priceData);
            var productPage = document.getElementById('productDetail');
            /*if(productPage.className!=null){
                productPage.className = 'hide';
            }*/
            var productPage = cmp.find('product-page');
            $A.util.removeClass(productPage, 'slds-show');
            $A.util.addClass(productPage, 'slds-hide');
            localStorage.scrollTop = document.body.scrollTop;
            /*var iframe = document.getElementById('viewPrice-wrapper');
            if(iframe.className!=null){
                iframe.className = 'show';
            }*/
            var viewPrice = cmp.find('viewPrice');
            $A.util.removeClass(viewPrice, 'slds-hide');
            $A.util.addClass(viewPrice, 'slds-show');
            
            /*var iframe = document.getElementById('iframe-wrapper');
            if(iframe.className!=null){
                iframe.className = 'hide';
            }*/
            var iframe = cmp.find('iframe');
            $A.util.removeClass(iframe, 'slds-show');
            $A.util.addClass(iframe, 'slds-hide');
            document.body.scrollTop = 0;
            window.scroll(0, 0)
            //Added by MB - 06/21/17 - To display buttons dynamically
            cmp.set('v.cancelBtnDisplay', false);
            cmp.set('v.hideNextButton', true);
            cmp.set('v.iframePage', true);
            
            //added by Mohan - to avoid showing Search area when view Price is clicked
            var cmpTarget = cmp.find('searchForm');
            $A.util.addClass(cmpTarget, 'hide');
            //$A.util.removeClass(cmpTarget, 'show');           
            
        } else {
            alert('No URL found');
        }
    },
    openURL: function(component, event, helper){
        var viewURL = event.target.dataset.url;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": viewURL,
            "isredirect": false
        });
		urlEvent.fire();
    },
    
    currChange: function(component, event, helper){
        //alert(component.get("v.currency"));
        //debugger;
        //var currency = document.getElementById('currency-val').value;
        //var currency = component.find("currency").get("value");
        //console.log(currency);
        //component.set("v.currSelected", currency);
    }
})