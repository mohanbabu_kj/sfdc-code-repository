({
    doInit: function(component, event, helper) {
        helper.deviceOrentationHandler( component, event, helper );
        
        /*var brand = component.find("brandId").get("v.value");
        var cugrp = component.find("cugrpId").get("v.value");
        helper.getSamplesfromAccount(component, brand, cugrp);
        helper.getPlacementStatus(component);
        helper.getBrandCollection(component);
        helper.getCustomerGroup(component);
        var inline_hide = component.find('inline_hide');
        // var inline_show = component.find('inline_show');
        // $A.util.removeClass(inline_show, 'slds-hide');
        $A.util.removeClass(inline_hide, 'slds-hide');*/
        
        helper.processDoInit(component, event);
        helper.getDisplayMashupUrl(component, event, helper, 'Hybris');
        helper.getDisplayMashupUrl(component, event, helper, 'Tableau');
    },
    applyFilter: function(component, event, helper) {
        var brand = component.find("brandId").get("v.value");
        var cugrp = component.find("cugrpId").get("v.value");
        helper.getSamplesfromAccount(component, brand, cugrp);
    },

    launchTableau: function(cmp, event, helper) {
        //alert('Pass the URL' + event.currentTarget.dataset.index);
        // create new iframe tag on click with product id dynamically added in URL
        var iframeUrl = 'https://myanalyticsqa.mohawkind.com/t/Flooring/views/SalesForce_Account_Report/Account_Report?:embed=y';

        iframeUrl = (cmp.get('v.TableauUrl')).replace(cmp.get('v.externalId'), event.currentTarget.dataset.idx);
        console.log('iframeUrl', iframeUrl);

        if (cmp.get('v.iframeUrl') != iframeUrl)
            cmp.set('v.iframeUrl', iframeUrl);
        localStorage.scrollTop = document.body.scrollTop;
        var samplePage = document.getElementById('sample-page');
        samplePage.className = 'hide';
        var iframe = document.getElementById('iframe-wrapper');
        iframe.className = 'show';
        document.body.scrollTop = 0;
    },
    launchHybris: function(cmp, event, helper) {
        //alert('Pass the URL' + event.currentTarget.dataset.index);
        // create new iframe tag on click with product id dynamically added in URL
        var iframeUrl = 'https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en?site=mhkflooring';

        iframeUrl = (cmp.get('v.HybrisUrl')).replace(cmp.get('v.externalId'), event.currentTarget.dataset.idx);
        console.log('iframeUrl', iframeUrl);

        if (cmp.get('v.iframeUrl') != iframeUrl)
            cmp.set('v.iframeUrl', iframeUrl);
        localStorage.scrollTop = document.body.scrollTop;
        var samplePage = document.getElementById('sample-page');
        samplePage.className = 'hide';
        var iframe = document.getElementById('iframe-wrapper');
        iframe.className = 'show';
        document.body.scrollTop = 0;
    },
    goBack: function(cmp, evt, helper) {
        var samplePage = document.getElementById('sample-page');
        samplePage.className = 'show';
        var iframe = document.getElementById('iframe-wrapper');
        iframe.className = 'hide';
        document.body.scrollTop = localStorage.scrollTop;
    },
    edit_status: function(component, event, helper) {
        var inline_show = component.find('inline_show');
        var inline_hide = component.find('inline_hide');
        $A.util.removeClass(inline_hide, 'slds-hide');
        $A.util.addClass(inline_show, 'slds-show');

    },
    saveData: function(component, event, helper) {
        component.set('v.statusMessage', '');
        if (component.get('v.saveRecords').length > 0)
            helper.save(component, event, helper);
        else
            component.set('v.statusMessage', 'No records modified'); //alert('No records modified');
        //var elemId = document.getElementById("statusId");
        //var statusSel = elemId.inputSelectOptions[elemId.selectedIndex].text;
        //var statusSelected = component.find("plstatus").get("v.value");
        //alert('Selected ' + statusSel);
    },
    changeHandler: function(component, event, helper) {
        console.log(event.getSource().get('v.value'), 'value')
        //console.log(event.getSource(), event.getSource().getLocalId());
        var selected = event.getSource().get('v.value');
        var selectArr = selected.split('__');

        var record = selectArr[0];
        var modifyRecordList = component.get('v.modifyRecordList');
        var saveRecords = component.get('v.saveRecords');
        var errorsList = component.get('v.errorsList');
        // console.log('saveRecords before', saveRecords);

        if (selectArr.length == 2) {
            if (selectArr[1] != 'Removed' && selectArr[1] != 'In Store') {
                /* Comment by Mudit - 14-6-2018. Using new attribute for error message.
                component.set('v.statusMessage', 'Please select Status: In Store or Removed.');
                */
                component.set("v.statusMessageModal","Please select Status: In Store or Removed.");
                for (i in saveRecords) {
                        if (saveRecords[i].Id == record) {
                            saveRecords.splice(i,1);
                        }
                    }
            } else {
                component.set("v.statusMessageModal","");
                if (modifyRecordList.indexOf(record) == -1) {
                    // var newRecord = {'sobjectType': 'Sample_Display_Inventory__c', 'Id': record, 'Placement_Status__c': selectArr[1]};
                    saveRecords.push({
                        'sobjectType': 'Sample_Display_Inventory__c',
                        'Id': record,
                        'Placement_Status__c': selectArr[1]
                    });
                    modifyRecordList.push(record);
                    component.set('v.modifyRecordList', modifyRecordList);
                } else {
                    for (i in saveRecords) {
                        if (saveRecords[i].Id == record) {
                            saveRecords[i].Placement_Status__c = selectArr[1];
                        }
                    }
                }
                console.log('saveRecords after', saveRecords);
                component.set('v.saveRecords', saveRecords);
            }
        }
    },
    //BY MUDIT on 25-10-2017
    onChangeFilter: function(component, event, helper) {
        component.set("v.isShowSpinner",true);
        var brand = component.find("brandId").get("v.value");
        var cugrp = component.find("cugrpId").get("v.value");
        helper.getSamplesfromAccount(component, brand, cugrp);        
    },
    //BY MUDIT on 25-10-2017
    //BY MUDIT on 26-10-2017
    sorting: function(component, event, helper) {        
        component.set( "v.selectedTab", event.currentTarget.id );
        helper.sortHelper(component, event, event.currentTarget.id);
    },
    //BY MUDIT on 26-10-2017
    //Added by MB - 05/07/18
    goBack: function(component, event, helper){
        var device = $A.get("$Browser.formFactor"); 
        if( device == 'DESKTOP' ){
            window.history.back();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            window.location.reload();
        }
    },
    openMVPL: function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        console.log('Component clicked : ' + event.currentTarget.dataset.comp);
        console.log('Product ID : ' + event.currentTarget.dataset.index);
        console.log('Account ID : ' + component.get('v.recordId'));
        //    if( device == 'DESKTOP' ){                    Bug : CPL all lihghting  components, no need to create component for mobile.
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:"+event.currentTarget.dataset.comp,
                componentAttributes: {
                    gridType:'Merchandizing_Vehicle_Price_List',
                    accountId :component.get('v.recordId'),
                    productId: event.currentTarget.dataset.index,
                    productName: event.currentTarget.dataset.productname
                }
            });
            evt.fire();
     //   }else if( device == 'PHONE' || device == 'TABLET' ){ 
     //       helper.navigateToOpenLE( component,component.get('v.recordId'),event.currentTarget.dataset.comp, event.currentTarget.dataset.index );
     //   }
    },
    //ADDED BY MUDIT - 14-6-2018
    handleEdit: function(component, event, helper){
        helper.handleEditHelper(component, event, helper);
    },
    //ADDED BY MUDIT - 14-6-2018
    closeEditModal: function(component, event, helper){
        helper.closeEditModalHelper(component, event, helper);
    },
    //ADDED BY MUDIT - 14-6-2018
    handleUpdate: function(component, event, helper){
        helper.handleUpdateHelper( component, event, helper );
    },
    handleCloseMsgClick: function(component, event, helper){
        component.set("v.statusMessage","");
    },
})