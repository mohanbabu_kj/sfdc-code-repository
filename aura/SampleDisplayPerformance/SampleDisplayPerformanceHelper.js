({
    getSamplesfromAccount: function(component, brand, cugrp) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getAccountDisplaySamples");
        console.log(brand, cugrp);
        action.setParams({
            "recordId": recordId,
            "brandCollection": brand,
            "customerGroup": cugrp
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result && result.length<=0){
                    component.set('v.noData', true);
                    
                }
                component.set("v.displaySamples", response.getReturnValue());
                console.log(response.getReturnValue());
                component.set("v.isShowSpinner",false);//BY MUDIT on 25-10-2017
            }
        });
        $A.enqueueAction(action);
    },
     deviceOrentationHandler : function( component,event,helper ){
        //alert(window.orientation);
                    component.set("v.deviceOrentation",window.orientation);

       window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            //alert(window.orientation);
            //return window.orientation;
            component.set("v.deviceOrentation",window.orientation);
        }, false);
    },
    getPlacementStatus: function(component) {
        var action = component.get("c.getPlacementStatusPicklistValues");
        var options = [];
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                for (var i = 0; i < response.getReturnValue().length; i++) {
                    options.push({
                        label: response.getReturnValue()[i],
                        value: response.getReturnValue()[i],
                    });
                }
                component.set("v.placementStatus", options);
                //console.log(component.get(v.placementStatus));
            }
        });
        $A.enqueueAction(action);
    },
    getBrandCollection: function(component) {
        var options = [];
        var action = component.get("c.getBrandCollectionPicklistValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                options.push({
                    label: "Select One",
                    value: ""
                });
                for (var i = 0; i < response.getReturnValue().length; i++) {
                    options.push({
                        label: response.getReturnValue()[i],
                        value: response.getReturnValue()[i]
                    });
                }
                options.push({
                    label: "All",
                    value: ""
                });
                component.set('v.brandCollectionList', options);
            }
        });
        $A.enqueueAction(action);
    },
    getCustomerGroup: function(component) {
        var options = [];
        var action = component.get("c.getCustomerGroupPicklistValues");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                /*options.push({
                    label: "--None--",     //Commented by SUSMITHA on NOV 2 for 41645 
                    value: ""
                });*/
                for (var i = 0; i < response.getReturnValue().length; i++) {
                    
                    options.push({
                        label: response.getReturnValue()[i],
                        value: response.getReturnValue()[i]
                    });
                }
                component.set('v.customerGroupList', options);
            }
            
        });
        $A.enqueueAction(action);
        
        
    },
    save: function(component, event, helper) {
        var action = component.get("c.saveSamplesData");
        action.setParams({
            // "updSampleList" : component.get("v.sampleDisplays")
            "samples": component.get("v.saveRecords")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(response.getReturnValue());
            if (component.isValid() && state === "SUCCESS") {
                component.set('v.statusMessage', 'Successfully Saved.');
                component.set('v.statusMessageClass','slds-theme_success');
                this.processDoInit(component, event);
                /*setTimeout(function(){
                    this.processDoInit(component, event);
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                }, 500);*/
                // component.set("v.displaySamples", response.getReturnValue());
            } else {
                component.set('v.statusMessage', 'Cannot be saved. Please try again after sometime');
            }
        });
        $A.enqueueAction(action);
    },
    getDisplayMashupUrl: function(component, event, helper, mashupName) {
        var newUrl = '';
        var action = component.get("c.getMashupUrl");
        action.setParams({
            "mashupName": 'View Display - ' + mashupName,
            'recordId': component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(mashupName, response.getReturnValue());
                component.set('v.' + mashupName + 'Url', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        // component.set('v.'+mashupName+'Url', newUrl);
    },
    processDoInit: function(component, event) {
        // component.set('v.statusMessage', '');
        var brand = component.find("brandId").get("v.value"); 
        var cugrp = component.find("cugrpId").get("v.value");
        this.getSamplesfromAccount(component, brand, cugrp); 
        this.getPlacementStatus(component);
        this.getBrandCollection(component);
        this.getCustomerGroup(component);
        
        var inline_hide = component.find('inline_hide');
        $A.util.removeClass(inline_hide, 'slds-hide');
    },
    //BY MUDIT on 26-10-2017
    sortHelper: function(component, event, sortFieldName) {
        console.log( component.get("v.statusSortOrder") );
        var currentDir = component.get("v.arrowDirection");
        var AllData = component.get("v.displaySamples");
        
        var sortData =[];
        for(i in AllData){
            sortData.push(
                {
                    "Inventory__r":{"Display__c":AllData[i].Inventory__r.Display__c,"Id":AllData[i].Inventory__r.Id},
                    "Brand__c":AllData[i].Brand__c != null ? AllData[i].Brand__c : "",
                    "Display_Performance__c":AllData[i].Display_Performance__c != null ? AllData[i].Display_Performance__c : "",
                    "Display_Version__c":AllData[i].Display_Version__c != null ? AllData[i].Display_Version__c : "",
                    "Fit__c":AllData[i].Fit__c != null ? AllData[i].Fit__c : "",
                    "Id":AllData[i].Id != null ? AllData[i].Id : "",
                    "MTD_Sales__c":AllData[i].MTD_Sales__c != null ? AllData[i].MTD_Sales__c : "",
                    "Name":AllData[i].Name != null ? AllData[i].Name : "",
                    "Placement_Status__c":AllData[i].Placement_Status__c != null ? AllData[i].Placement_Status__c : "",
                    "R12_Sales__c":AllData[i].R12_Sales__c != null ? AllData[i].R12_Sales__c : "",
                    "YTD_Sales__c":AllData[i].YTD_Sales__c != null ? AllData[i].YTD_Sales__c : ""
                }
            );
        }
        console.log( sortData );
        if ( currentDir == 'arrowdown' ) {
            // set the arrowDirection attribute for conditionally rendred arrow sign  
            component.set("v.arrowDirection", 'arrowup');
            // set the isAsc flag to true for sort in Assending order.  
            component.set("v.isAsc", true);
            sortData.sort(function(a,b){
                if( !isNaN( a[sortFieldName] ) || !isNaN( b[sortFieldName] ) )
                    return (a[sortFieldName] > b[sortFieldName]) ? 1 : ((a[sortFieldName] < b[sortFieldName]) ? -1 : 0);
                else
                    return (a[sortFieldName].toLowerCase() > b[sortFieldName].toLowerCase()) ? 1 : ((a[sortFieldName].toLowerCase() < b[sortFieldName].toLowerCase()) ? -1 : 0);
            });
        } else {
            component.set("v.arrowDirection", 'arrowdown');
            component.set("v.isAsc", false);
            sortData.sort(function(a,b){                
                if( !isNaN( a[sortFieldName] ) || !isNaN( b[sortFieldName] ) )
                    return (b[sortFieldName] > a[sortFieldName]) ? 1 : ((b[sortFieldName] < a[sortFieldName]) ? -1 : 0);
                else
                    return (b[sortFieldName].toLowerCase() > a[sortFieldName].toLowerCase()) ? 1 : ((b[sortFieldName].toLowerCase() < a[sortFieldName].toLowerCase()) ? -1 : 0);
            });            
        }
        component.set("v.displaySamples", sortData);
    },
    //BY MUDIT on 26-10-2017
    //Added by MB - 05/07/18
    navigateToOpenLE : function( component,Id,componentName, prodId ) {        
        var container = component.find("openLEDiv");
        $A.createComponent(
            "c:"+componentName,
            {                
                gridType:'Merchandizing_Vehicle_Price_List',
                accountId :component.get('v.recordId'),
                productId: prodId
            },
            function(cmp) {
                var cmpMain = component.find("mainDiv");
                console.log( cmpMain );
                $A.util.addClass(cmpMain, 'slds-hide');                
                container.set("v.body", [cmp]);
            }
        );        
    },
    //ADDED BY MUDIT - 14-6-2018
    handleEditHelper: function(component, event, helper){
        component.set("v.editRecordModal",true);
        var selectedRecordValue = event.getSource().get("v.value");
        var recordId = selectedRecordValue.substr(0,selectedRecordValue.indexOf('__'));
        var previousSelectedVal = selectedRecordValue.substr( selectedRecordValue.indexOf('__')+2,selectedRecordValue.length );
        component.set("v.recordIdForChange",recordId);
        component.set("v.previousSelectedValue",previousSelectedVal);
    },
    //ADDED BY MUDIT - 14-6-2018
    closeEditModalHelper: function(component, event, helper){
        component.set("v.editRecordModal",false);
    },
    //ADDED BY MUDIT - 14-6-2018
    handleUpdateHelper: function(component, event, helper){
        var previousRecordVal = component.get("v.displaySamples");
        var recordIdChanged = event.getSource().get("v.value");
        var selectedValue = component.get("v.selectedValue").substr( component.get("v.selectedValue").indexOf('__')+2,component.get("v.selectedValue").length );
        var matched = [];
        var unMatched = [];
        
        for( var i=0;i<previousRecordVal.length;i++ ){
            if( previousRecordVal[i].Id == recordIdChanged ){
                previousRecordVal[i].Placement_Status__c = selectedValue;
                matched.push( previousRecordVal[i] )
            }else{
                unMatched.push( previousRecordVal[i] );
            }  
        }
        
        component.set("v.displaySamples",matched.concat( unMatched ));
        this.closeEditModalHelper(component, event, helper);
    },
    
})