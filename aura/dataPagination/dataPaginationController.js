({
    firstPage: function(component, event, helper) {
        console.log('### firstPage - currentPageNumber: ' + component.get("v.currentPageNumber"));
        component.set("v.currentPageNumber", 1);
    },
    prevPage: function(component, event, helper) {
        console.log('### prevPage - currentPageNumber: ' + component.get("v.currentPageNumber"));
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber")-1, 1));
    },
    nextPage: function(component, event, helper) {
        console.log('### nextPage - currentPageNumber: ' + component.get("v.currentPageNumber"));
        console.log('### nextPage - maxPageNumber: ' + component.get("v.maxPageNumber"));
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber")+1, component.get("v.maxPageNumber")));
    },
    lastPage: function(component, event, helper) {
        console.log('### lastPage - currentPageNumber: ' + component.get("v.currentPageNumber"));
        console.log('### lastPage - maxPageNumber: ' + component.get("v.maxPageNumber"));
        component.set("v.currentPageNumber", component.get("v.maxPageNumber"));
    }
})