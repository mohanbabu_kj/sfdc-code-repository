({
	doGirdIfInit : function(component, event, helper) {
		var _map = component.get("v.record");
        var _key = component.get("v.field");
        var _value = component.get('v.isTrue');
        var _operator = component.get('v.operator');
        var _reverse = component.get('v.reverse');

        if(_map && _key){
            var keyArray = [];
            console.log('_key:' + _key);
            while(_key.indexOf('.') > -1){
                keyArray = _key.split('.');
                _map = _map[keyArray[0]];
                _key = keyArray[1];
            }
            if(_value == true){
                _value = _reverse?!_map[_key]:_map[_key];
            }
            component.set('v.isTrue', _value);

		}

	}
})