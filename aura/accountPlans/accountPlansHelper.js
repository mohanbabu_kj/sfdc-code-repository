({
    //Server call to get the Account plans based on account Id and Fiscal year.
    getAccountPlans : function(cmp,fiscalYear,firstTimeLoad) {
        console.log('getAccountPlans : Start ');
        var action = cmp.get('c.getAccountPlans');
        var accId = cmp.get('v.recordId');
        console.log('accId: '+accId);
        console.log('fiscalYear: '+fiscalYear);
        action.setParams({accId:accId,fYear:fiscalYear});
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
				cmp.set('v.accountPlans', res);
                console.log('res'+JSON.stringify(res));
                 //console.log('res length'+res.length );
                if(res && res.length>0){
                    
                    cmp.set('v.noData', false);
                }else if(firstTimeLoad)
                {
                    cmp.set('v.noData', true);
                }
                
                console.log('Action Completed ');
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
        console.log('getAccountPlans : End ');
    },
     deviceOrentationHandler : function( component,event,helper ){
        //alert(window.orientation);
                    component.set("v.deviceOrentation",window.orientation);

       window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            //alert(window.orientation);
            //return window.orientation;
            component.set("v.deviceOrentation",window.orientation);
        }, false);
    },
    //To get the pickList values of the Fiscal Year Field
    getFiscalYear: function(component) {
        console.log('getFiscalYear : Start ');
        var options = [];
        
        var action = component.get("c.getFiscalPicklistValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                
                var fYearList = ['All'];
                var newFYearList = fYearList.concat(response.getReturnValue());                
                component.set('v.fiscalYear',newFYearList );
            }
        });
        $A.enqueueAction(action);
        console.log('getFiscalYear : End ');
    },
    //Function To fire an Standard event to create Account Plan
    createAccountPlan : function(cmp, event, helper)
    {
        console.log('createAccountPlan : Start ');
        var createAcountContactEvent = $A.get("e.force:createRecord");
        var accId = cmp.get('v.recordId');
        var mhkTeamObj = cmp.get('v.mhkTeamObj');
        //Start of Code - MB - Bug 57164 - 4/4/18
        var usrObj = cmp.get('v.userObj');
        var accObj = cmp.get('v.accountObj');
        var accPlanName = 'Sales Plan - ' + accObj.Name + ' - ' + usrObj.FirstName + ' ' + usrObj.LastName;
        //End of Code - MB - Bug 57164 - 4/4/18
        if(mhkTeamObj!=null){
        createAcountContactEvent.setParams({
            "entityApiName": "My_Account_Plan__c",
            "defaultFieldValues": {
                'Name': accPlanName,
                'Account__c' : accId,
                'Mohawk_Account_Team__c' : mhkTeamObj.Id
            }
        });
        }
        else{
          createAcountContactEvent.setParams({
            "entityApiName": "My_Account_Plan__c",
            "defaultFieldValues": {
                'Name': accPlanName,
                'Account__c' : accId,
                'Mohawk_Account_Team__c' : null
            }
        });  
        }
        createAcountContactEvent.fire();
        console.log('createAccountPlan : End ');
    },
    createAccountPlanMob : function(cmp, event, helper)
    {
        console.log('createAccountPlanMob : Start ');
        var accId = cmp.get('v.recordId');
        var mhkTeamObj = cmp.get('v.mhkTeamObj');
        //Start of Code - MB - Bug 57164 - 4/10/18
        var usrObj = cmp.get('v.userObj');
        var accObj = cmp.get('v.accountObj');
        console.log('usrObj usr'+JSON.stringify(usrObj) );
        var accPlanName = 'Sales Plan - ';
        if(accObj!=null && accObj!='undefined' ){
       accPlanName=accPlanName + accObj.Name ;
        }
        if( usrObj!=null && usrObj!='undefined' ){
       accPlanName=accPlanName +  ' - ' + usrObj.FirstName + ' ' + usrObj.LastName;
        }
        //End of Code - MB - Bug 57164 - 4/10/18
        console.log('createAccountPlanMob : mhkTeamObj '+mhkTeamObj);
        console.log('createAccountPlanMob : accId : '+accId);
        sforce.one.createRecord("My_Account_Plan__c", null, {'Name': accPlanName, 'Account__c' : accId, 'Mohawk_Account_Team__c' : mhkTeamObj.Id });
        
        
        console.log('createAccountPlanMob : End ');
    },
    //Server call to get Mohawk Team members for the account
    getMHKTeamMember : function(cmp) {
        console.log('getMHKTeamMember : Start ');
        
        var action = cmp.get("c.getMohawkTeamMember");
        var accId = cmp.get('v.recordId');

        action.setParams({accountId:accId});
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                
                cmp.set('v.mhkTeamObj', res.mhkTeam);
                cmp.set('v.accountObj', res.acc);
                cmp.set('v.userObj', res.usr); //Added by MB - Bug 57164 - 4/4/18
                console.log('getMHKTeamMemebr : SUCCESS '+response.getReturnValue());
                console.log('getMHKTeamMemebr'+JSON.stringify(res.mhkTeam) );
                console.log('getMHKTeamMemebr'+JSON.stringify(res.acc) );
                console.log('getMHKTeamMemebr usr'+JSON.stringify(res.usr) );

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
        console.log('getMHKTeamMember : End ');
    }
})