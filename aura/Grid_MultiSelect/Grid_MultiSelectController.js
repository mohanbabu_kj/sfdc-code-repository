({
    doInit: function(component, event, helper) {
         var options = component.get("v.options");
        var label = component.get("v.label");
        
        if(options && options.length > 0){
            if (label != 'Price Level') {
                options.sort(function compare(a,b) {
                    if (a.value == 'All')
                        return -1;
                    else if (a.value < b.value)
                        return -1;
                    if (a.value > b.value)
                        return 1;
                    
                    return 0;
                });
            }
            else {
                options.sort(function(a, b) {
                  //Pattern for sorting values
                  var orderArray = ['> TM1','TM1','TM2','TM3','DM','RVP','< RVP','> BG','BG','< BG','N/A'];
                  
                  return orderArray.indexOf(a.value) - orderArray.indexOf(b.value);
                });                
            }
        }else{
            options = [];
        }

        
        component.set("v.options_",options);
        var _values = helper.getSelectedLabels(component);
        helper.setInfoText(component,_values);
        helper.initialized = true;
    },

    //********************************************************
    //Handle event for Desktop version
    //********************************************************
    
    handleFocus: function(component, event, helper) {
        console.log('on focus');
        helper.initialized = false;
        component.set("v.showDropdown",true);
    },

    handleSelection: function(component, event, helper) {
        var item = event.currentTarget;
        helper.initialized = true;
        if (item && item.dataset) {
            var value = item.dataset.value;
            var selected = item.dataset.selected;
            
            var options = component.get("v.options_");

            options.forEach(function(element) {
                if (element.value == value) {
                    element.selected = selected == "true" ? false : true;
                }
            });
            component.set("v.showDropdown",true);
            component.set("v.options_", options);
            var _values = helper.getSelectedValues(component);
            var _labels = helper.getSelectedLabels(component);

            helper.setInfoText(component,_labels);
            helper.despatchSelectChangeEvent(component,_values);
        }
    },

    handleMouseLeave: function(component, event, helper) {
        component.set("v.showDropdown",false);
    },

    handleMouseEnter: function(component, event, helper) {
        clearTimeout(helper.timeoutAction);
        component.set("v.showDropdown",true);
    },

    handleOffFocus: function(component, event, helper) {
        helper.timeoutAction = window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid() && !helper.initialized) {
                    component.set("v.showDropdown",false);
                }
            }), 200
        );
    },
    
    
    //********************************************************
    //Handle event for Mobile (iPad) version
    //********************************************************
    
    handleSelectionMobile : function(component, event, helper) {
        helper.initialized = true;
        var selectCmp = component.find("InputSelectMultiple");
        
        var selectedOptions = selectCmp.get("v.value").split(';');
        console.log('### selected options: ' + selectedOptions);
        
        var options = component.get("v.options_");
        
        options.forEach(function(element) {
            element.selected = selectedOptions.includes(element.value);
        });    
        
        component.set("v.options_", options);
	
        var _values = helper.getSelectedValues(component);
        var _labels = helper.getSelectedLabels(component);
        
        helper.setInfoText(component,_labels);
        helper.despatchSelectChangeEvent(component,_values);
        
        
    }
    
    
})