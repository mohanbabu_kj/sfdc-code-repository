({
    timeoutAction: null,
    initialized: false,
    setInfoText: function(component, labels) {
        
        if (labels.length == 0) {
            component.set("v.infoText", "Any");
        }
        if (labels.length == 1) {
            component.set("v.infoText", labels[0]);
        }
        else if (labels.length > 1) {
            component.set("v.infoText", labels.length + " options selected");
        }
    },

    
    getSelectedValues: function(component){
        var options = component.get("v.options_");
        var _values = [];
        options.forEach(function(element) {
            if (element.selected) {
               _values.push(element.value);
            }
        });
        return _values;
    },

    getSelectedLabels: function(component){
        var options = component.get("v.options_");
        var _labels = [];
        options.forEach(function(element) {
            if (element.selected) {
                _labels.push(element.label);
            }
        });
        return _labels;
    },

    despatchSelectChangeEvent: function(component,values){
        component.set("v.value", values.join(';'));
        var compEvent = component.getEvent("onchange");
        compEvent.setParams({ "values": values });
        compEvent.fire();
    }
})