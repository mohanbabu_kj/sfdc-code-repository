({
    doInit : function(component, event, helper) {
        var iddd=component.get("v.recordId");
        var str= window.location.href;
        //var pos = str.lastIndexOf("lkid%3D");
        var pos = str.lastIndexOf("lkid");
        pos += 5;
        var endpos = pos + 15;
        //var sli = str.slice(pos+7, str.length-2); 
        var sbstr= str.substring(pos,endpos);
        var action1 = component.get('c.getOpptyFromIdId');
        var formFieldMap = new Map();
        
        console.log(component.get('v.recordId'));
        if(component.get('v.device') === 'PHONE'){
            formFieldMap.isPhone = true;
        }else if(component.get('v.device') === 'TABLET'){
            formFieldMap.isTablet = true;
        }if(component.get('v.device') === 'DESKTOP'){
            formFieldMap.isDesktop = true;
        }
        formFieldMap.onSave = false;
        component.set("v.projectId",component.get('v.recordId'));
        action1.setParams({
            'optyId':component.get('v.recordId')
        });
        action1.setCallback( this, function( response ) {
            if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                component.set( "v.projectName" , response.getReturnValue() );
            }
        });
        $A.enqueueAction(action1);
        
        //document.getElementById("test").disabled = true;
        var action = component.get("c.getPickListValuesIntoList");
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                component.set("v.accRolePickList",result);
            }            
        });  
        
        var recID=component.get("v.recordId")
        if(recID!=null){
            console.log("inint"+component.get("v.recordId"));
            component.set("v.relatedAccountDetail", {'sobjectType': 'Related_Account__c','Opportunity__c': component.get("v.recordId")});
        }
        $A.enqueueAction(action);
        
        //formFieldMap.filterField = 'Role_C__c';
        component.set('v.formFieldMap', formFieldMap);
        console.log('formFieldMap' + JSON.stringify(formFieldMap));
    },
    closeMenu :function(component,event,helper){
        //  alert('1st entry');
        var appEvent = $A.get("e.c:closeMenuForAccountLookupEvent");
        appEvent.fire();
    },
    
    onChangeAccRole : function(component, event, helper) {
        console.log("onChangeAccRole");
        var relAcc = component.get('v.relatedAccountDetail');
        //document.getElementById("test").disabled = true;
        var message = component.get("v.selectedAccRole");
        //var message = component.get('v.relatedAccountDetail.Account_Role__c');
        var strAccSelected = component.get('v.formFieldMap.StrategicAccount');
        if(message!==undefined && message.startsWith('--None') && strAccSelected)
        {
            console.log("hi test"+message);
            component.set("v.currAbilityAccount",true);
            //component.set("v.selectedLookUpRecord1Account",undefined);
            var tempArr = component.get("v.selectedLookUpRecords");
            tempArr.pop();
            component.set("v.selectedLookUpRecords",tempArr);
            component.set("v.currAbilityContact",true);
        }
        else 
        {
            relAcc.Account_Role__c = message;
            if(relAcc.Account_Role__c === 'A & D'){relAcc.Account_Role__c = 'AD';} //Added by MB - Bug 70872 - 2/19/19
            console.log('Account role: ' + relAcc.Account_Role__c);
            if(relAcc.Account_Role__c !== undefined){
                component.set('v.formFieldMap.filterValue', relAcc.Account_Role__c);
                component.set('v.formFieldMap.filterField', 'Role_C__c');
                //component.set('v.relatedAccountDetail', relAcc);
            }
            var projRole = '';
            if(relAcc.Account_Role__c !== undefined && (relAcc.Account_Role__c === 'End User' || relAcc.Account_Role__c === 'Dealer' ||
                                                        relAcc.Account_Role__c === 'General Contractor' || relAcc.Account_Role__c === 'AD')){
                projRole = relAcc.Account_Role__c;
                component.find('projectRole').set('v.value', projRole);
            }else{
                component.find('projectRole').set('v.value', '');
            }
            component.set("v.currAbilityAccount",false);
            var appEvent = $A.get("e.c:closeMenuForAccountLookupEvent");
            appEvent.fire();
        }
        //component.set("v.relatedAccountDetail.Account_Role__c",message)
        
        //component.set("v.selectedRole",component.get("v.relatedAccountDetail.Account_Role__c"));
        console.log("selectedLookUpRecord1Account"+component.get("v.selectedLookUpRecord1Account"));
    },
    enableContactLookup : function(component, event, helper) {
        console.log("enableContactLookup");
        var message = event.getParam("data");
        component.set("v.currAbilityContact",false);
        console.log("accountid"+message.Id);
        component.set("v.selectedAcctId",message.Id);
        console.log("recs"+component.get("v.selectedLookUpRecords"));
        
        /* Start of Code - MB - 08/13/18 - Bug 63853 
        var action = component.get('c.checkAccountisStrategic');
        var fieldMap = component.get('v.formFieldMap');
        action.setParams({"recordId" : message.val});
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            console.log('state: ' + state);
            if (component.isValid() && state === "SUCCESS") {
                fieldMap.isStrategic = resp.getReturnValue();
                component.set("v.formFieldMap",fieldMap);
                console.log('isStrategic: ' + fieldMap.isStrategic);
            }else{
                console.log('Error');
            }
        }); 
        $A.enqueueAction(action);
        /* End of Code - MB - 08/13/18 - Bug 63853 */
        
        //Start of Code - MB - Bug 70377 - 2/8/19
        var selectedAcc = component.get('v.selectedLookUpRecord1Account');
        console.log('selectedAcc: ' + selectedAcc);
        if(selectedAcc !== undefined && selectedAcc.Strategic_Account__c){
            component.set("v.formFieldMap.isStrategic",true);
            component.set("v.relatedAccountDetail.Project_Role__c",'Strategic Account');
        }else if(selectedAcc !== undefined && !selectedAcc.Strategic_Account__c){
            component.set("v.formFieldMap.isStrategic",false);
        }
        //End of Code - MB - Bug 70377 - 2/8/19
        
    },
    setProjectRole : function(component, event, helper) {
        var message = event.getParam("isStrategic");
        if(message){
            component.set("v.relatedAccountDetail.Project_Role__c",'Strategic Account');
        }
    },
    returnMethod : function(component, event, helper) {
        var parentproid = component.get("v.projectId");//"v.selectedLookUpRecord2Project.val");
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": parentproid,
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    
    disableLookups : function(component, event, helper) {
        console.log("disableLookups");
        var message = event.getParam("data");
        if(message.Id !== undefined && message.Id.startsWith('001'))
        {
            component.set("v.currAbilityContact",true);
            var tempArr = component.get("v.selectedLookUpRecords");
            tempArr.pop();
            component.set("v.selectedLookUpRecords",tempArr);
        }
    },
    
    onSelect: function(component, event, helper) {
        var isChecked = component.get('v.formFieldMap.StrategicAccount');
        component.set("v.currAbilityContact",true);
        component.set("v.selectedLookUpRecord1Account",undefined);
        component.set("v.relatedAccountDetail.Project_Role__c",'');
        component.set("v.selectedLookUpRecords", []);
        if(isChecked){
            component.set('v.formFieldMap.disableRole', true);
            component.set('v.formFieldMap.filterField', 'Strategic_Account__c');
            component.set('v.formFieldMap.filterValue', true);
            component.set('v.selectedAccRole', '');
        }else{
            component.set('v.formFieldMap.disableRole', false);
            component.set('v.formFieldMap.filterField', 'Role_C__c');
            var accRole = component.get("v.selectedAccRole");
            if(accRole === 'A & D'){accRole = 'AD';} //Added by MB - Bug 70872 - 2/19/19
            component.set('v.formFieldMap.filterValue', accRole);
        }
    },
    
    createRelatedAccount : function(component, event, helper) {
        var formFieldMap = component.get('v.formFieldMap');
        //var accid = component.get("v.selectedLookUpRecord1Account.val");
        var accid = component.get("v.selectedLookUpRecord1Account.Id");
        var relAcc = component.get("v.relatedAccountDetail");
        console.log("accid"+accid);
        var parentproid = component.get("v.projectId");//"v.selectedLookUpRecord2Project.val");
        console.log("parentproid"+parentproid);
        var contList = component.get("v.selectedLookUpRecords");

        var accRole=component.get("v.selectedAccRole");
        
        console.log('isStrategic: ' + formFieldMap.isStrategic);
        if(accRole !== undefined){
            if(accRole === 'A & D'){accRole = 'AD';} //Added by MB - Bug 70872 - 2/19/19
            relAcc.Account_Role__c = accRole;
        }
        //Start of Code - MB - Bug 70506 - 2/19/19
        if(formFieldMap.isStrategic){
            relAcc.Strategic_Account__c = true;
        }
        //End of Code - MB - Bug 70506 - 2/19/19
        console.log('relaccRole: ' + relAcc.Account_Role__c);
        console.log('contList: ' + JSON.stringify(contList));
        if(((contList !== undefined && contList.length > 0 ) || formFieldMap.isStrategic || 
            (contList !== undefined && contList.length === 0 && relAcc.Account_Role__c !== 'Dealer')) && 
           (accid !== undefined && accid !== null)){
            formFieldMap.onSave = true;
            component.set('v.formFieldMap', formFieldMap);
            var action = component.get("c.createRARecord");
            action.setParams({
                "relAccRec" : relAcc,
                "parentAccId" :accid,
                "parentProjId" : parentproid,
                "contactList" :contList //,
                // "AccountRole" : accRole    
            });
            action.setCallback(this, function(response){
                if(response.getState()==='SUCCESS'){
                    var errorMsg = response.getReturnValue();
                    var toastEvent = $A.get("e.force:showToast");
                    if(errorMsg=='Success'){
                        toastEvent.setParams({
                            "title": "Success!",
                            "type":"Success",
                            "message": "Project Related Account is created."
                        });
                        toastEvent.fire();
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        $A.get('e.force:refreshView').fire();
                        /*var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": parentproid,
                            "slideDevName": "related"
                        });
                        navEvt.fire();*/
                    }
                    if(errorMsg.indexOf('Success')){
                        component.set("v.isError",true);
                        component.set("v.errorMsg","Contact: "+errorMsg+"  already exist");
                    }
                    formFieldMap.onSave = false
                    component.set('v.formFieldMap', formFieldMap);
                }else{
                    component.set("v.isError",true);
                    component.set("v.errorMsg","Error occurred while saving. Please try again or contact System Administrator");
                    formFieldMap.onSave = false
                    component.set('v.formFieldMap', formFieldMap);
                }
            });
            $A.enqueueAction(action);
        }else if(contList !== undefined && (contList === [] || contList.length > 0) && (accid === null || accid === undefined)){
            component.set("v.isError",true);
            component.set("v.errorMsg","Account and Contact fields are mandatory");
        }else if(accid === null || accid === undefined){
            component.set("v.isError",true);
            component.set("v.errorMsg","Account  field is mandatory");
        }else if(contList !== undefined && (contList.length === 0 || contList === [])){
            component.set("v.isError",true);
            component.set("v.errorMsg","Contact  field is mandatory");
        }
    }
})