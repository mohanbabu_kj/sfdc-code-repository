({
    doInit : function(component, event, helper) {      
        /* By susmitha on nov 29*/
        var device = $A.get("$Browser.formFactor");
        if(device == 'PHONE'){
            component.set("v.is_Phone",true);
        }
        else if(device != 'PHONE' ){
            
            component.set("v.is_notPhone",true);
        }
        
        /*----------------------*/
        
        var listHeight = component.get('v.listHeight');
        listHeight = parseInt(listHeight)+10;
        component.set('v.listHeight', listHeight);
        console.log('Print : '+component.get('v.recordId'));
        //var action = component.get('c.getRecordById'); - Commented by MB - 5/31/17
        var action = component.get('c.getAccountProfileId');
        action.setParams({
            'recordId': component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                //component.set('v.Account', response.getReturnValue()); - Commented by MB - 5/31/17
                component.set('v.Account_Profile_Id', response.getReturnValue());
            }
            console.log('account obj', response.getReturnValue());
        });
        
        $A.enqueueAction(action);
    },
    openPage : function(component, event, helper) {
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            var targetId = event.currentTarget.dataset.id;
            console.log('targetId', targetId);
            var device = $A.get("$Browser.formFactor");
            if( targetId !='' && targetId !=undefined && targetId != 'null' /* && device == 'DESKTOP'*/  ){            
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": targetId,
                    'slideDevName': 'detail'
                });
                console.log('navEvt', navEvt);            
                navEvt.fire();
            }
            else {
                alert('No Record Id found. Unable to load the target page');
            }
        }else{
            var evt = $A.get("e.force:navigateToComponent");
            console.log('Component clicked : ' + event.currentTarget.dataset.comp);
            console.log('Component clicked : ' + event.currentTarget.dataset.index);
            evt.setParams({
                componentDef : "c:"+event.currentTarget.dataset.comp,
                componentAttributes: {
                    apRecordId : component.get('v.Account_Profile_Id'),
                    accountId :component.get('v.recordId')
                }
            });
            evt.fire();
        }
        
        
    },
    
    openLEModal : function(component, event, helper) {
        //ADDED BY MUDIT - 02-11-2017
        //      var device = $A.get("$Browser.formFactor");
        //ADDED BY MUDIT - 02-11-2017
        //      if( device == 'DESKTOP' ){        
        var evt = $A.get("e.force:navigateToComponent");
        console.log('Component clicked : ' + event.currentTarget.dataset.comp);
        console.log('Component clicked : ' + event.currentTarget.dataset.index);
        evt.setParams({
            componentDef : "c:"+event.currentTarget.dataset.comp,
            componentAttributes: {
                recordId : component.get('v.recordId'),
                isFromRecordPage: 'true',
                gridType:'Customer_Price_List',
                accountId :component.get('v.recordId')
            }
        });
        evt.fire();
        //       }else if( device == 'PHONE' || device == 'TABLET' ){ //ADDED BY MUDIT - 02-11-2017
        //ADDED BY MUDIT - 02-11-2017
        //          helper.navigateToOpenLE( component,component.get('v.recordId'),event.currentTarget.dataset.comp );
        //       }
    },
    backToRecord: function(component, event, helper){
        //Force the navigation back to the account record instead of the previous window.history
        /*var _accountId = component.get('v.recordId');
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": _accountId
        });
        navEvt.fire(); */ 
        $A.get("e.force:closeQuickAction").fire();
        //window.history.back();
    },
    
    navigate : function(component, event, helper) {
        
    /* $A.get("e.force:closeQuickAction").fire(); */
   
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": '/apex/VFMobile' ,
        "isredirect" :false
    });
    urlEvent.fire(); 
        
    },
    
    openMashup : function(component, event, helper) {
        var device = $A.get("$Browser.formFactor"); //ADDED BY MUDIT - 02-11-2017
        console.log( 'device:::> ' + device ); //ADDED BY MUDIT - 02-11-2017
        if( device == 'DESKTOP' ){ //ADDED BY MUDIT - 02-11-2017
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:mashupTabItem",
                componentAttributes: {
                    recordId : component.get('v.recordId'),
                    mashupName : event.currentTarget.dataset.title,
                    isFromRecordPage : 'true'
                }
            });
            evt.fire();
        }else if( device == 'PHONE' || device == 'TABLET' ){ //ADDED BY MUDIT - 02-11-2017
            //ADDED BY MUDIT - 02-11-2017
            helper.navigateToMashup( component,component.get('v.recordId'),event.currentTarget.dataset.title );
        }
    }
})