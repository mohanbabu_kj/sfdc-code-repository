({
	setupChart : function(component, event, helper) {
        console.log('### --- Chart Library just loaded ---');
        //helper.generateChart(component, helper);
    },
	doInit : function(component, event, helper) {
        
        var objectName = 		 component.get("v.objMetaName");
        var styleNumVal = 		 component.get("v.styleNum");
        var inventoryNumVal = 	 component.get("v.inventoryNum");
        var chartIdVal = 		 component.get("v.chartId");
        var accountId = 		 component.get("v.accountId");
        var timePeriodVal = 	 component.get("v.timePeriodValue");
        var productCategoryVal = component.get("v.productCategory");
        
        if (productCategoryVal != null && productCategoryVal != '') {
	        component.set("v.categoryValue", productCategoryVal);
        }
        
        console.log('### ');
        console.log('### ********* doInit ***********');
        console.log('### chartIdVal        : ' + chartIdVal);
        console.log('### productCategoryVal: ' + productCategoryVal);
        console.log('### accountId         : ' + accountId);
        console.log('### styleNumVal       : ' + styleNumVal);
        console.log('### inventoryNumVal   : ' + inventoryNumVal);
        console.log('### timePeriodVal     : ' + timePeriodVal);
        console.log('### ****************************');
        console.log('### ');
		
        
        var params = {objName : 	objectName, 
                      styleNum: 	styleNumVal, 
                      chartId: 		chartIdVal, 
                      category: 	productCategoryVal,
                      accountId : 	accountId,
                      inventoryNum: inventoryNumVal};
        
        //Get the chart data
        helper.callToServer(
            component,
            "c.getData",
            function(response)
            {	
                console.log('apex response :'+JSON.stringify(response));
                component.set("v.chartData",response);
                helper.generateChart(component, helper);
            }, 
            params
        ); 	

        //Get the Filter options
        helper.callToServer(
            component,
            "c.loadFilterData",
            function(response)
            {	
                helper.processFilter(component, response, '', '', '', timePeriodVal, productCategoryVal);
                var spinner = component.find("chartSpinner");
        		$A.util.toggleClass(spinner, "slds-hide");
            }, 
            params
        );         
 
	},
    filterChanged: function(component, event, helper) {
        
		var sourceComponent = event.getSource();
        var controlName = sourceComponent.get("v.name");
        
        console.log('### source: ' + sourceComponent);
        console.log('### source.Name: ' + sourceComponent.get("v.name"));

        var regionObj = component.find("selectRegion");
        var districtObj = component.find("selectDistrict");
        var territoryObj = component.find("selectTerritory");
        var timePeriodObj = component.find("selectTimePeriod");
        var categoryObj = component.find("selectCategory");
        var productCategory = component.get("v.productCategory");
                
        var regionValue = (regionObj != null) ? regionObj.get("v.value") : '';
        var districtValue = (districtObj != null) ? districtObj.get("v.value") : '';
        var territoryValue = (territoryObj != null) ? territoryObj.get("v.value") : '';
        var timePeriodValue = (timePeriodObj != null) ? timePeriodObj.get("v.value") : '';    
        var categoryValue = (categoryObj != null) ? categoryObj.get("v.value") : productCategory; 
        
        if (categoryValue == '') categoryValue = productCategory; 
        
        //Reset Territory filter selection if category changes.
        if (controlName == 'selectCategory') {
            territoryValue = '';
        }
        
        component.set("v.regionValue", regionValue);
        component.set("v.distrcitValue", districtValue);
        component.set("v.territoryValue", territoryValue);
        component.set("v.timePeriodValue", timePeriodValue);
        component.set("v.categoryValue", categoryValue);
        component.set("v.productCategory", categoryValue);

        console.log("   ");
        console.log("***** FILTER SELECTED VALUES *****");
        console.log("### v.regionValue", regionValue);
        console.log("### v.distrcitValue", districtValue);
        console.log("### v.territoryValue", territoryValue);
        console.log("### v.timePeriodValue", timePeriodValue);
        console.log("### v.categoryValue", categoryValue);
        console.log("**********************************");
        console.log("   ");
        
        
        console.log('here 1');
        //Fire the event to clear the drilldown table
        var chartBarClickedEvent = component.getEvent("chartBarClicked");
        chartBarClickedEvent.setParams({
            priceLevelClicked: "",
            sourceChartId : "",
            region : "",
            district : "",
            territory : "",
            timePeriod: "",
            category : "",
        }).fire();
                
        var objectName = 		component.get("v.objMetaName");
        var styleNumVal = 		component.get("v.styleNum");
        var inventoryNumVal = 	component.get("v.inventoryNum");
        var chartIdVal = 		component.get("v.chartId");
        var accountId = 		component.get("v.accountId");
        
        var params ={ region : 		regionValue, 
                     district : 	districtValue, 
                     territory : 	territoryValue, 
                     objName : 		objectName, 
                     styleNum: 		styleNumVal, 
                     chartId: 		chartIdVal, 
                     timePeriod: 	timePeriodValue,
                     accountId: 	accountId,
                     category: 		categoryValue,
                     inventoryNum: 	inventoryNumVal};
        
        console.log('### getData for params: ' + params);
        
        //Get the chart data
        helper.callToServer(
            component,
            "c.getData",
            function(response)
            {	
                console.log('apex response :'+JSON.stringify(response));
                component.set("v.chartData",response);
                helper.updateChart(component, helper);
            }, 
            params
        ); 	        

        //Get the Filter options
        helper.callToServer(
            component,
            "c.loadFilterData",
            function(response)
            {	
                console.log('here 1');
                helper.processFilter(component, response, regionValue, districtValue, territoryValue, timePeriodValue, categoryValue);
            }, 
            params
        );         
    }

    
})