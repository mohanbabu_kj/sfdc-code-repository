({
    doInit: function(component, event, helper) {
        helper.initHelper( component, event, helper );
        
    },
    handleMenuSelect: function(component, event, helper) {
        var selectedMenuItemValue = event.getParam("value");
        if( selectedMenuItemValue == 'Contact' ){
            component.set("v.iconName","standard:contact");
            component.set("v.placeholder","Seach Contacts");
            component.set("v.selectedObject","Contact");
        }else{
            component.set("v.iconName","standard:user");
            component.set("v.placeholder","Seach Users");
            component.set("v.selectedObject","User");
        }
    },
    onKeyUpText: function(component, event, helper) {
        helper.onKeyUpTextHelper( component, event, helper );        
    },
    selectedEmails: function(component, event, helper) {        
        helper.selectedEmailHelper(component, event, helper);
    },
    removePills: function(component, event, helper) {        
        helper.removePillHelper(component, event, helper);
    },
    fireTheEvent: function(component, event, helper) {        
        helper.fireTheEventHelper(component, event, helper);
    },
})