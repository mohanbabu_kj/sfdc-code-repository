({
	doInit : function(cmp, evt, helper) {
		helper.getObjName(cmp, evt, helper);
		//helper.init(cmp, evt);
	},
	//change checkbox status of include attachment
	changeIncludeAtt :function (cmp, evt, helper){

		var status = evt.target.attributes[4].value;
		if (status == "checked") {
			evt.target.attributes[4].value = "false";
		} else {
			evt.target.attributes[4].value = "checked";
		}
		// debugger
		// console.log(status)
	},
	// save data to backend
	saveToBackend : function (component, event, helper) {
		helper.updateInfo(component, event);
	},
	// set all checkbox status of SAL 
	selectAll : function (cmp, evt, helper) {
		var selectAll = cmp.get("v.selectAllChecked");
		cmp.set("v.isChecked", "");
		if(selectAll == "") {
			cmp.set("v.selectAllChecked", "checked");
			cmp.set("v.isChecked", "checked");
		} else if (selectAll == "checked"){
			cmp.set("v.selectAllChecked", "")
			
		}
	},
	// set all checkbox status of recommended project SAL - start
	setRecommendSelectAll : function (cmp, evt, helper) {
		cmp.set("v.setSelectRecommendAllChecked", "");
	},
	selectRecommendAll : function (cmp, evt, helper) {
		var selectAll = cmp.get("v.setSelectRecommendAllChecked");
		cmp.set("v.isRecommendChecked", "");
		if(selectAll == "") {
			cmp.set("v.setSelectRecommendAllChecked", "checked");
			cmp.set("v.isRecommendChecked", "checked");
		} else if (selectAll == "checked"){
			cmp.set("v.setSelectRecommendAllChecked", "")
			cmp.set("v.isRecommendChecked", "");
		}
	},
	// set all checkbox status of recommended project SAL - end
	// set all checkbox status of Fundamental 
	selectFundamental : function (cmp, evt, helper) {
		var selectFundamental = cmp.get("v.setSelectFundamentalChecked");
		cmp.set("v.fundamentalIsChecked", "");
		if(selectFundamental == "") {
			cmp.set("v.setSelectFundamentalChecked", "checked");
			cmp.set("v.fundamentalIsChecked", "checked");
		} else {
			cmp.set("v.setSelectFundamentalChecked", "");
			cmp.set("v.fundamentalIsChecked", "");
		}
	},
	// set Select SAL checkbox Status
	setSelectAll : function (cmp, evt, helper) {
		cmp.set("v.selectAllChecked", "");
	},
	// set Select Fundamental checkbox Status
	setSelectFundamental : function (cmp, evt, helper) {
		cmp.set("v.setSelectFundamentalChecked", "");
	},
	// hyper-link to detail page
	gotoDetail : function(cmp, evt, helper) {
		// debugger;
		var id = evt.target.attributes[1].value;
		// window.location.hash = "#/sObject/" + id +"/view";
		var event = $A.get("e.force:navigateToSObject");
			event.setParams({"recordId": id});
			event.fire();
		
	},
	//event of click disabled checkbox 
	returnFalse : function (cmp, evt, helper) {
		return false;
	},
})