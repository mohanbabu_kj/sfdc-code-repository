({
    doInit : function(component, event, helper) {
        
        var recordId = component.get("v.recordId");
        var viewData = new Map();
        viewData.showDialog = viewData.error = false;
        var formFactor = $A.get("$Browser.formFactor");
        if(formFactor === 'PHONE' || formFactor === 'TABLET'){
            viewData.isDevice = true;
        }else{
            viewData.isDevice = false;
        }
        viewData.title = '';
        viewData.showSpinner = true;
        component.set('v.viewData', viewData);
        console.log('RecordId: ' + recordId);
        
        /*var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        console.log(relatedListEvent);
        relatedListEvent.setParams({
            "relatedListId": 'My_Appointment_List__r',
            "parentRecordId": 'a0B4C0000002NRzUAM'
        });
        relatedListEvent.fire();*/
        
        
        var action = component.get("c.getAppointmentSAL");
        action.setParams({ "recordId" : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log('Result: ' + result);
            if (component.isValid() && state === "SUCCESS" && !result.error) {
                viewData.showSpinner = false;
                viewData.listOfSALs = result.listOfSALs;
                component.set('v.viewData', viewData);
            }else{
                viewData.error = viewData.showDialog = true;
                viewData.title = 'Error';
                viewData.showSpinner = false;
                if(state === "ERROR"){
                    viewData.message = 'Error occurred while retrieving SAL. Please try again later or contact System Administrator';
                }else{
                    viewData.message = result.message;
                }
                component.set('v.viewData', viewData);
            }
        });
        $A.enqueueAction(action); 
        
    },
    
    onBack: function(component, event, helper){
        window.history.back();
    },
    
    openSAL: function(component, event, helper){
        var viewData = component.get('v.viewData');
        var id = event.target.dataset.id;
        
        if(viewData.isDevice){
            sforce.one.navigateToSObject(event.target.dataset.id);
        }else{
            //window.location.href = 'https://mohawkdev--rdev.lightning.force.com/' + id ;
            console.log('Id: ' + event.target.dataset.id);
            var navigate = $A.get( 'e.force:navigateToSObject' );
            if ( navigate ) {
                navigate.setParams({
                    'recordId' : event.target.dataset.id
                }).fire();
            }
        }
    },
    
    closeErrorModal:function(cmp, evt, helper){
        cmp.set("v.viewData.showDialog",false);
        /*var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();*/
    }

})