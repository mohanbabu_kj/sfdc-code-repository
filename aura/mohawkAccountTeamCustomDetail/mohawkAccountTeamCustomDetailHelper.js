({
    apNameHelper : function(component, event, helper) {
        var action = component.get("c.getMohawkAccountTeam");
        action.setParams({ 'recordId' : component.get('v.recordId') });
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                console.log( result );
                component.set("v.mhkAcctTeam",result);
                this.onInitHelper( component, event, helper );   
            }            
        });        
        $A.enqueueAction(action);
    },
    onInitHelper : function(component, event, helper) {
        var action = component.get("c.createExpandableSections");
        action.setParams(
            {
                
                //'BusinessType': component.get("v.mhkAcctTeam.Brands__c") == null ? '' : component.get("v.mhkAcctTeam.Brands__c"),
                //'sectionType':component.get("v.mhkAcctTeam.Product_Types__c"),
                'BusinessType': '',
                'device':component.get("v.device"),
                'recordId' : component.get('v.recordId')
            }
        );
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                console.log( result );
                var productType = component.get("v.mhkAcctTeam.Product_Types__c");
                /*var brandType = component.get("v.mhkAcctTeam.Brands__c");
                //var brands = component.get("v.mhkAcctTeam.Brands__c");
                //var stringBrandType;
                if( brands != null && brands != '' && brands != undefined ){
                    stringBrandType = brands.split(";");
                }*/
                var stringBrandType = component.get("v.mhkAcctTeam.Brands__c").split(";");
                var brandType=stringBrandType.toString();
                var newData = [];
                
				//PRODUCT TYPE != null and BRAND TYPE != null - START
				//if( productType != null && productType != '' && productType != undefined && brandType != null && brandType != '' && brandType != undefined ){
                  if( productType != null && productType != '' && productType != undefined ){  
                    //GET THE MANDATORY SECTION FIRST - START
                    for( var i=0;i<result.length;i++ ){
                        if( result[i].isSectionAlwaysShow ){
                            newData.push({
                                'blockSectionContent' : result[i].blockSectionContent,
                                'isSectionDefaultOn' : result[i].isSectionDefaultOn,
								'isSectionDefaultOn_PHONE' : result[i].isSectionDefaultOn_PHONE,
								'isSectionDefaultOn_TABLET' : result[i].isSectionDefaultOn_TABLET,
                                'sectionDisplayType' : result[i].sectionDisplayType,
                                'sectionHeader' : result[i].sectionHeader,
                                'sectionSequence' : result[i].sectionSequence,
                                'tableSectionContent' : result[i].tableSectionContent
                            });
                        }
                    }
                    //GET THE MANDATORY SECTION FIRST - START
                    
                    if( productType.includes('Hardwood') ){
                        productType = productType.replace("Hardwood","Wood");
                    }
                    if( brandType.includes('Portico') ){
                        brandType = brandType.replace("Portico","Portico / Properties");
                    }
                    if( brandType.includes('Properties') ){
                        brandType = brandType.replace("Properties","Portico / Properties");
                    } 
                    if( brandType.includes('Hard Surface') ){
                        brandType = brandType.replace("Hard Surface","Retail");
                    }  
					for( var i=0;i<result.length;i++ ){                    
                        if( result[i].sectionHeader == 'Total Overview' || result[i].sectionHeader == 'Overview'){
                            var newDataTable = [];                            
                            for( var j=0;j<result[i].tableSectionContent.length;j++ ){
                                if( productType.includes( result[i].tableSectionContent[j].APSectionMasterLabel ) ){
                                    newDataTable.push( result[i].tableSectionContent[j] );
                                }
                            }
                            newData.push({
                                'blockSectionContent' : result[i].blockSectionContent,
                                'isSectionDefaultOn' : result[i].isSectionDefaultOn,
								'isSectionDefaultOn_PHONE' : result[i].isSectionDefaultOn_PHONE,
								'isSectionDefaultOn_TABLET' : result[i].isSectionDefaultOn_TABLET,
                                'sectionDisplayType' : result[i].sectionDisplayType,
                                'sectionHeader' : result[i].sectionHeader,
                                'sectionSequence' : result[i].sectionSequence,
                                'tableSectionContent' : newDataTable
                            });
                        }else{
							if( brandType != null && brandType != '' && brandType != undefined ){
								var newDataTable = []; 
                                //Putting Aladdin Commercial Section only if it is present in Brand.
                                if( brandType.includes( 'Aladdin Commercial' ) ){
                                    productType += ';Aladdin Commercial';
                                }
                                //Putting Aladdin Commercial Section only if it is present in Brand.
								for( var j=0;j<result[i].tableSectionContent.length;j++ ){
									var tempSectionHeader = result[i].sectionHeader.replace(" Overview","");
									if( productType.includes( tempSectionHeader ) ){
                                        if( brandType.includes( result[i].tableSectionContent[j].APSectionMasterLabel ) ){
                                            newDataTable.push( result[i].tableSectionContent[j] );
                                            newData.push({
                                                'blockSectionContent' : result[i].blockSectionContent,
                                                'isSectionDefaultOn' : result[i].isSectionDefaultOn,
												'isSectionDefaultOn_PHONE' : result[i].isSectionDefaultOn_PHONE,
												'isSectionDefaultOn_TABLET' : result[i].isSectionDefaultOn_TABLET,
                                                'sectionDisplayType' : result[i].sectionDisplayType,
                                                'sectionHeader' : result[i].sectionHeader,
                                                'sectionSequence' : result[i].sectionSequence,
                                                'tableSectionContent' : newDataTable
                                            });
                                        }
									}
								}
							}else{
								var newDataTable = [];
								for( var j=0;j<result[i].tableSectionContent.length;j++ ){
									var tempSectionHeader = result[i].sectionHeader.replace(" Overview","");
									if( productType.includes( tempSectionHeader ) ){
										//if( brandType.includes( result[i].tableSectionContent[j].APSectionMasterLabel ) ){
										newDataTable.push( result[i].tableSectionContent[j] );
										newData.push({
											'blockSectionContent' : result[i].blockSectionContent,
											'isSectionDefaultOn' : result[i].isSectionDefaultOn,
											'isSectionDefaultOn_PHONE' : result[i].isSectionDefaultOn_PHONE,
											'isSectionDefaultOn_TABLET' : result[i].isSectionDefaultOn_TABLET,
											'sectionDisplayType' : result[i].sectionDisplayType,
											'sectionHeader' : result[i].sectionHeader,
											'sectionSequence' : result[i].sectionSequence,
											'tableSectionContent' : newDataTable
										});
										//}
									}
								}
							}
						}
                    }
                }				
				//PRODUCT TYPE != null and BRAND TYPE != null - START
                else{
                    component.set("v.displayMessage","Product Type is not assigned to the user.");
                }
                component.set("v.data",this.sortData( component,true,'sectionSequence',this.removeDuplicates(newData,'sectionHeader') ));
                component.set("v.spinner",false);
            }            
        });
        
        $A.enqueueAction(action);
    },
    sortData : function (component, sortAsc,sortField,data) {
        data.sort(function(a,b){
            var t1 = a[sortField] == b[sortField],
                t2 = (!a[sortField] && b[sortField]) || (a[sortField] < b[sortField]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        return data;
    },
    handleBackClickHelper : function (component, sortAsc,sortField,data) {
        window.history.back();
    },
    removeDuplicates : function (originalArray, prop) {
        var newArray = [];
        var lookupObject  = {};
        
        for(var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }
        
        for(i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    },
    replaceString : function (str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    },
    
})