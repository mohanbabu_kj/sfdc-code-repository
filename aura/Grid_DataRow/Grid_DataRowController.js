({
	doDataRowInit : function(component, event, helper) {
        var _map = component.get("v.record");
        var flds = component.get("v.fields");
        var _attributeMap = {};
        var values = [];
        if(_map && flds){
            for (var i=1; i<flds.length; i++){
                var keyArray = flds[i]['fieldName'].split('.');
                var fieldVal = (keyArray.length == 1 ? _map[keyArray[0]] : (keyArray.length == 2 ? (_map[keyArray[0]] != null ? _map[keyArray[0]][keyArray[1]] : '') : ''));
                var isBoolean = flds[i]['fieldType'] == 'Boolean';
                values.push([helper.renderDataByType(fieldVal,flds[i]['fieldType']),flds[i]['label'],flds[i]['field']['Truncate_At_Max_Width__c'] ? ' slds-truncate' : '',isBoolean,fieldVal]);
            }
            
            component.set("v.values",values);
            /*if(_map && _key && _map[_key]){
                var _attributeMap = {};
                _attributeMap['value'] = _map[_key];
                if(_type == 'Currency'){
                    _outputCmp = 'ui:outputCurrency';
                }
                else if(_type == 'Number'){
                    _outputCmp = 'ui:outputNumber';
                }
                    else if(_type == 'Boolean'){
                        _outputCmp = 'ui:outputCheckbox';
                    }
                        else if(_type == 'Date'){
                            _outputCmp = 'ui:outputDate';
                            _attributeMap['format'] = 'M/d/yy';
                            if (_attributeMap['value'] == '4000-12-31')
                                _attributeMap['value'] = '';
                        }
                            else if(_key == 'Indicators__c'){
                                _attributeMap['class'] = 'indicator-box';
                            }
                                else{
                //var start = performance.now();
                                    //component.set("v.body", '<lightning-formatted-text>' + _map[_key] + '</lightning-formatted-text>');
                        //var elapsed = performance.now() - start;
                        //console.log(elapsed);
                                }*/
            /*$A.createComponent(
                _outputCmp,
                _attributeMap,
                function(newCmp, status, errorMessage){
                    if (status === "SUCCESS") {
                        var body = component.get("v.body");
                        body.push(newCmp);
                        component.set("v.body", body);
                    }else{
                        console.log(errorMessage);
                    }
                }
            )*/
        }
    }
})