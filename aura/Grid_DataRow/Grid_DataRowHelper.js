({
	renderDataByType : function(data, datatype) {
        if (data == null)
            data = '';
        if (datatype == 'Currency'){
            if (data == '')
                return '';
            return $A.localizationService.formatCurrency(data);
        }
        else if (datatype == 'Currency Integer'){
            if (data == '')
                return '';
            var formatted_data = $A.localizationService.formatCurrency(data);
            return formatted_data.substring(0,formatted_data.length - 3);
        }
        else if (datatype == 'Date'){
            if (data == '4000-12-31' || data == '')
                return '';
            else
                return $A.localizationService.formatDate(data,'M/d/yy');
        }
        else if (datatype == 'Boolean')
        {
            return '';
        }
        return data;
	}
})