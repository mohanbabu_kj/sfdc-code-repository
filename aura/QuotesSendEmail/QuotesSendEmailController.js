({
	doInit : function(component, event, helper) {
        var formattr = new Object();
		var action = component.get("c.createPDF");
        var recordId = component.get('v.recordId');
        debugger;
        formattr.onSave = true;
        component.set('v.formattributes', formattr);
        formattr = component.get('v.formattributes');
        action.setParams({'recordId': recordId});
        console.log(recordId);
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(state);
            if (component.isValid() && state === "SUCCESS") {
                formattr.showMessage = true;
                formattr.onSave = false;
                if(result.error === true){
                    console.log(response.getReturnValue());
                    formattr.errorOccurred = true;
                    formattr.errorMessage = result.errMessage;
                    component.set('v.formattributes', formattr);
                }else if(result.errorPDF === true){
                    console.log(response.getReturnValue());
                    formattr.errorOccurred = true;
                    formattr.errorMessage = result.errMessage;
                    formattr.errorMessage = formattr.errorMessage + '. Failed generating PDF document. Please try again later opening quote created. If issue still persist, contact System Administrator.';
                    component.set('v.formattributes', formattr);
                }else if(result.errorSendEmail == true){
                	formattr.errorOccurred = true;
                    formattr.errorMessage = result.errMessage;
                    formattr.errorMessage = formattr.errorMessage + '. Failed sending email to dealer. Please try again later opening quote created. If issue still persist, pleaes contact System Administrator.';
                    component.set('v.formattributes', formattr);
            	}else if(result.errorUpdStatus == true){
                	formattr.errorOccurred = true;
                    formattr.errorMessage = result.errMessage;
                    formattr.errorMessage = formattr.errorMessage + '. Failed to update Quote status. Email sent to dealer was successful. Please update quote status manually';
                    component.set('v.formattributes', formattr);
            	}else{
                    formattr.errorOccurred = false;
                    formattr.successMessage = 'PDF is generated and email sent to Dealer.';
                    component.set('v.formattributes', formattr);
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    closeAlertMsg: function(component, event, helper){
        var formattr = component.get('v.formattributes');
        formattr.errorMessage = '';
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
        component.set('v.formattributes',formattr);
    }
})