({
    //Server Call to get the Data when the component load
    //Based on the Object type will recive the return data
    initHelper: function(component, event, helper) {
		
        var recId = component.get('v.recordId');
        var objName = component.get('v.sObjectName');
        var proId;
        var quoteId,noValue = false;
        if(objName == 'Opportunity')
            proId = recId;
        else if(objName == 'Quote')
            quoteId = recId;
        else
            noValue = true;
            
        if(noValue == false)
        {
            var action1 = component.get("c.getDealerAndQuotes");
            action1.setParams({
                'projectId': proId,
                'quoteId': quoteId,
            });
            action1.setCallback(this, function(response) {
                var wrpperCls = response.getReturnValue();
                if(wrpperCls.isError == false)
                {
                    component.set('v.dealerList', wrpperCls.accList);
                    if(objName == 'Quote')
                        component.set('v.quoteSelectedObj', wrpperCls.quote);
                    else
                        component.set('v.quoteList', wrpperCls.quoteList);
                }
               
            });
            $A.enqueueAction(action1);
        }
        
    },
    //On table to select the single quote 
    selectQuoteHelper: function(component, event, helper) {
        if(event.getSource().get('v.checked') == true)
        {
           component.set('v.ExternalQuoteId', event.getSource().get('v.value')); 
        }
    },
   	// Prepare the parameter list for the hybrisIframe component to display the hybris cmp
     hybrisMashup: function(component, event, helper) {
        //Modify the logic to pass the selected account Global id and the quoteNumber.
        var dealerAccountGlobId = component.get('v.selectedDealer');
        var ExternalQuoteId = component.get('v.ExternalQuoteId');
        component.set('v.paramList', {
            Global_Account_Id__c: dealerAccountGlobId,
            quoteNumber: ExternalQuoteId
        });
        component.set('v.isHybris', true);
    },
})