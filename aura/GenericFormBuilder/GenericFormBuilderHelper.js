({
    initHelper : function( component, event, helper ) {
        var action = component.get("c.getFields");
        action.setParams({objectName:component.get("v.objName"),identifierStr:component.get("v.identifier")});
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if ( component.isValid() && state === "SUCCESS" ) {
                var result = resp.getReturnValue();
                console.log( result );
                component.set("v.listOfFields",result.fields);                
            }            
        });
        $A.enqueueAction(action);
    },
    handleOnChange : function( component, event, helper ){
        //alert( 'CHANGE OCCURE' );
        //alert( event.getSource().get("v.name") + ' = ' + event.getSource().get("v.value") );
        //console.log( component.get("v.valueToPass") );
        //console.log( component.get("v.valueToPass").length );
        var previousValues = component.get("v.valueToPass");
        if( previousValues.length > 0 ){
            for (var i = 0; i < previousValues.length; i++) {
                if( previousValues[i].fieldname == event.getSource().get("v.name") ){
                    previousValues[i].value = event.getSource().get("v.value");
                }else{
                    
                    previousValues.push({
                        fieldname : event.getSource().get("v.name"),
                        value : event.getSource().get("v.value")
                    });
                    component.set("v.valueToPass",this.removeDuplicates(previousValues,'fieldname'));    
                }
            }  
        }else{
            var param = [];
            param.push({
                fieldname : event.getSource().get("v.name"),
                value : event.getSource().get("v.value")
            });
            component.set("v.valueToPass",this.removeDuplicates(param,'fieldname'));    
        }
        console.log( component.get("v.valueToPass") );
        var cmpEventFire = component.getEvent("newValuesfromFormBuilder");
        cmpEventFire.setParams({
            "values" : component.get("v.valueToPass")
        });
        cmpEventFire.fire();
    },
    removeDuplicates : function (originalArray, prop) {
        var newArray = [];
        var lookupObject  = {};
        
        for(var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }
        
        for(i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    },
})