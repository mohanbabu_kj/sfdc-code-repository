({
	doInit : function(component, event, helper) {
		helper.initHelper( component, event, helper );
	},
    handleChange : function(component, event, helper) {
        helper.handleOnChange( component, event, helper );
    }
})