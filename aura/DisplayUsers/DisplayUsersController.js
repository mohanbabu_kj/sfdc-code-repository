({
    doInit: function (component, event, helper) {
        helper.getMajorMetorsPickListvalues(component, event);
        helper.getStatePickListvalues(component, event);
        //helper.getInitialized(component, event);
        /*var action = component.get("c.getUserInfo");
  action.setParams({});
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        console.log(response.getReturnValue());
        component.set('v.userList', response.getReturnValue());
      }else{
    	  console.log(state);
      }
    });
    $A.enqueueAction(action);*/
    },
    /*handleSelectChange: function(component, event, helper) {
        var selectedSegment = event.getParam("data");
        //alert(selectedSegment[0].values);
        component.set('v.selectedStates', selectedSegment[0].values);
        
    },*/
    search: function (component, event, helper) {
        helper.getUserList(component, event);
        
    },
    enterKey: function (component, event, helper) {
        if(event.which == 13){
        	helper.getUserList(component, event);
        }
        
    },
    
    clear: function(component, event, helper){
        
    },
    
    openDetailPage: function (component, event, helper) {
        component.set('v.userId', event.currentTarget.dataset.id);
        var urlEvent = $A.get("e.force:navigateToSObject");
        urlEvent.setParams({
            "recordId" : event.currentTarget.dataset.id});
        urlEvent.fire();
        /*var listPage = component.find('list-page');
	  $A.util.addClass(listPage, 'slds-hide');
	  $A.util.removeClass(listPage, 'slds-show');
	  var detailPage = component.find('detail-page');
	  $A.util.addClass(detailPage, 'slds-show');
	  $A.util.removeClass(detailPage, 'slds-hide');
	  console.log(component.get('v.userId')); */
        //alert(event.currentTarget.dataset.id); 
    }
})