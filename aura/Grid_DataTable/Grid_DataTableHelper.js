({
    dataChanged: function(component) {
        var pageNumber = 1;
        component.set("v.pageNumber", pageNumber);
    },
	renderPage: function(component, helper) {
        console.log('renderPage');
        var viewData = component.get("v.viewData");
        var records = viewData.listOfData;            
        console.log('Grid Type: ' + component.get('v.gridType'));
        console.log('### renderPage - rowHeight: ' + viewData.rowHeight);
        console.log('### renderPage - rowHeightTablet: ' + viewData.rowHeightTablet);
        console.log('Records: ' + JSON.stringify(records));
        var device = $A.get("$Browser.formFactor");

        if( device != 'PHONE' ){
            
            var pageSize = viewData.numberOfRecords;
            
            if (pageSize == null || pageSize == undefined)
                pageSize=25;
            
            if (records != null && records.length > 0) {
                var pageNumber = component.get("v.pageNumber");
                var pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);
                component.set("v.currentList", pageRecords);
                component.set("v.maxPage", Math.floor((records.length+(pageSize - 1))/pageSize));
            }
        }
        else {
            component.set("v.currentList", records);
        }
        
	},

})