({
    // Begin of Pagination attributes and handlers 
	doInit: function(component, event, helper) {
        
        console.log('### Grid_DataTable doInit');
        console.log('Grid Type: ' + component.get('v.gridType'));
	},
    dataChanged: function(component, event, helper) {
        helper.dataChanged(component);
        helper.renderPage(component, helper); // Added Helper in function - MB - 08/08/18 - Bug 64134
    },    
    renderPage: function(component, event, helper) {
        helper.renderPage(component, helper); // Added Helper in function - MB - 08/08/18 - Bug 64134
    },    
    // End of Pagination attributes and handlers 
    
    
    doSort: function(component, event, helper) {
        var _sortField = event.getSource().get("v.value");
        var _currentSortField = component.get("v.viewData.sortField");
        var _currentSortOrder = component.get("v.viewData.sortOrder");
        var _sortOrder = (_sortField == _currentSortField && _currentSortOrder == 'asc') ? 'desc' : 'asc';
        component.set("v.viewData.sortField", _sortField);
        component.set("v.viewData.sortOrder", _sortOrder);
        console.log('Sort Field: ' + _sortField);
        helper.updateData(component);
    },
    
    doUpdateView: function(component, event, helper) {
        //component.find("dataTableRaw").updateView();
        helper.updateData(component);
        console.log('### doUpdateView');
    },
    
    doDataMenuAction: function(component, event, helper) {
        var value = event.getParam("value");
        
        var _viewData = component.get("v.viewData");
        var _index = event.getSource().get("v.value");
        var _data = _viewData.listOfData[_index];
        console.log(_data);
        
        if (value == 'View_Accessories') {
            var productCategoryId = component.get("v.viewData.productCategoryId");
            var modalBody;
            $A.createComponent("c:Grid_DataAccessoriesView", {
                "recordId": _data.Id,
                "productCategoryId": productCategoryId,
                "sObjectName": _viewData.sObjectName
            },
                               function(content, status) {
                                   if (status === "SUCCESS") {
                                       modalBody = content;
                                       component.find('overlayLib').showCustomModal({
                                           header: "Accessories",
                                           body: modalBody,
                                           showCloseButton: true,
                                           cssClass: "my-modal"
                                       })
                                       
                                   }
                               });
        }else if(value == 'Reports'){
            var _prodCatName = helper.getSelectedProductCategoryName(component);
            var _sellingStyleNum = _data.Product__r.Product_Style_Number__c;
            var _sellingStyleName = _data.Product__r.Name;
        	var evt = $A.get("e.force:navigateToComponent");
        	evt.setParams({
           	 componentDef : "c:Product_Price_Records",
            	componentAttributes: {
                	sellingStyleNum : _sellingStyleNum,
                	category : _prodCatName,
                    sellingStyleName : _sellingStyleName,
                    timeStamp : Date.now()
            	}/*,
            	isredirect : true*/
        	});
        	evt.fire();
        }else if(value == 'Mohawk_Xchange'){
            var accountId ;
            if(_viewData.account!=null){
                var account = _viewData.account;
                console.log('account id = >>>'+account.Id);
                accountId = account.Id;
            }else{
              accountId='NA';
            }
            
            helper.doCallout(component, "c.getProductId", {
                "recordId": _data.Id,                
                "sObjectName": _viewData.sObjectName
            }, function(response){
                console.log('in mohawk xchange call');
                //let set = new Set();
                var sectionValues=[];
                var _status = response.getState();
                if(_status === 'SUCCESS'){
                    
                    var productId = response.getReturnValue();
                    console.log('in return statement from call'+productId);
                    var productCategoryId = component.get("v.viewData.productCategoryId");
                    var modalBody;
                    console.log('gridtype===>'+component.get("v.gridType"));
                    var gridType = component.get("v.gridType");
                    var mashupName;
                    if(gridType=='Customer_Price_List')
                       mashupName = 'CPL Grid';
                    if(gridType=='Price_Grid')
                       mashupName = 'Price Grid';
                    if(gridType=='Buying_Group_Grid')
                       mashupName = 'BG Grid';
                    if(gridType=='Merchandizing_Vehicle_Price_List')
                       mashupName = 'MVPL Grid';
                    
                    $A.createComponent("c:mashupTabItem", {
                        "mashupName": mashupName,
                        "recordId": accountId,
                        "isFromRecordPage":true,
                        "gridProductId": productId
                    },
                                       function(content, status) {
                                           if (status === "SUCCESS") {
                                               
                                               modalBody = content;
                                               component.find('overlayLib').showCustomModal({
                                                   header: mashupName,
                                                   body: modalBody,
                                                   showCloseButton: true,
                                                   cssClass: "my-modal"
                                               })
                                               
                                           }
                                       });
                }
            });
        }
    },
                    
    doSecondaryDataDisplay: function(component, event, helper){
        console.log(component);
        console.log(event);
        console.log(event.getSource());
        component = component.find("datacell");
        console.log(component);
        var appEvent = $A.get("e.c:Grid_LaunchSecondaryModalEvent");
        appEvent.setParams({
            "sObjectName" : component.get("v.sObjectName"),
            "recordId": component.get("v.recordId"),
            "productCategoryId": component.get("v.productCategoryId"),
            "warehouseValue": component.get("v.warehouseValue")
        });
        appEvent.fire();
    },
    
    handleLaunchSecondaryEvent : function(component, event, helper){
        
        console.log('### Grid_DataTableController - handleLaunchSecondaryEvent');
        var eventComp = event.getSource();
        var viewData = component.get("v.viewData");
        var gridType = component.get("v.gridType");
        var currentList = component.get("v.currentList");
        
        var account = viewData[account];
        var accountId;
        if (account != undefined)
            accountId = account.Id;
        

        var sellingStyleNum = event.getParam("sellingStyleNum");
        var sellingStyleName = event.getParam("sellingStyleName");
        var record = event.getParam("record");
        var productCategoryName = viewData["productCategory"];
        var hasAccessories = viewData["hasAccessories"];
        var accessoryCheckField = viewData["accessoryCheckField"];
        
        helper.doCallout(component, "c.getSecondaryHeaderDisplay", {
            "recordId" : eventComp.get("v.recordId"),
            "sObjectName" : eventComp.get("v.sObjectName"),
            "productCategoryId" : eventComp.get("v.productCategoryId")
        }, function(response){
            var _status = response.getState();
            
            if(_status === 'SUCCESS'){
                var _label = response.getReturnValue();
                var modalBody;
                $A.createComponent("c:Grid_DataSecondaryView",
                                   {
                                       "sObjectName" : eventComp.get("v.sObjectName"),
                                       "recordId": eventComp.get("v.recordId"),
                                       "productCategoryId": eventComp.get("v.productCategoryId"),
                                       "warehouseValue": eventComp.get("v.warehouseValue"),
                                       "header" : _label,
                                       
                                       "productCategoryName": productCategoryName,
                                       "sellingStyleNum": sellingStyleNum,
                                       "sellingStyleName": sellingStyleName,
                                       "accountId": accountId,
                                       "gridType": gridType,
                                       "hasAccessories": hasAccessories
                                   },
                                   function(content, status) {
                                       if (status === "SUCCESS") {
                                           modalBody = content;
                                           component.find('overlayLib').showCustomModal({
                                               /*header: _label,*/
                                               body: modalBody,
                                               showCloseButton: true,
                                               cssClass: "grid-modal"
                                           })

                                       }

                                   });
            }
        });
    }
})