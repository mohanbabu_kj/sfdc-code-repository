/**
 * Created by rchen180 on 17/4/21.
 */
({
  getProjectTeammemberRolevalues : function (component, event, helper) {
    var action = component.get("c.getProjectTeammemberRolevalues");
    // action.setParams({'Name': name, 'Territory': territory});
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        // console.log(response.getReturnValue());
        component.set('v.teamRoleList', response.getReturnValue());
      }
    });
    $A.enqueueAction(action);
  },
  // getProjectAccessLevelvalues : function (component, event, helper) {
  //   var action = component.get("c.getProjectAccessLevelvalues");
  //   // action.setParams({'Name': name, 'Territory': territory});
  //   action.setCallback(this, function (response) {
  //     var state = response.getState();
  //     if (component.isValid() && state === "SUCCESS") {
  //       // console.log(response.getReturnValue());
  //       component.set('v.accessLevelList', response.getReturnValue());
  //     }
  //   });
  //   $A.enqueueAction(action);
  // },e
  // SSB 08-13-17 Start of code - for passing USer details and Team Role  to- Edit component
  	 getProjectTeamMember : function (component, event, helper) {
    	var projectteamId = component.get('v.recordId'); 
  //      console.log(projectteamId);
        if (projectteamId != undefined && projectteamId.substring(0,3) == '00q') {
        var action = component.get("c.getProjectTeamMember");
      	action.setParams({'ProjectTeamId': projectteamId});
     	action.setCallback(this, function (response) {
       	var state = response.getState();
        console.log(response.getState());
       	if (component.isValid() && state === "SUCCESS") {
          console.log(response.getReturnValue());
          var projectinfo = response.getReturnValue();
        //component.set('v.username', response.getReturnValue());
          component.set('v.username',projectinfo[0]);
          component.set('v.teamRole',projectinfo[1]);
       }
     });
     $A.enqueueAction(action);
      }  // end of outer if
 	},  // end of method
 /*   getProjectTeamMemberRole : function (component, event, helper) {    //SB 8-27-17 combined in one call - start
    	var projectteamId = component.get('v.recordId');
   //     console.log(projectteamId);
        if (projectteamId != undefined && projectteamId.substring(0,3) == '00q') {
        var action = component.get("c.getProjectTeamMemberRole");
      	action.setParams({'ProjectTeamId': projectteamId});
     	action.setCallback(this, function (response) {
       	var state = response.getState();
       	if (component.isValid() && state === "SUCCESS") {
          console.log(response.getReturnValue());
         component.set('v.teamRole', response.getReturnValue());
       }
     });
     $A.enqueueAction(action);
      }  // end of outer if
 	},  */															// SB 8-27-17 combined in one call - end				
    
       
  // SSB 08-13-17 end of code - for passing USer details and Team Role - Edit component  
    
    getProjectNameInfo : function (component, event, helper) {
      // Start of Code -MB- 08/09/2017
      var projectId = component.get('v.recordId');
      if(projectId == undefined){
          projectId = component.get('v.oppId');
      } // End of Code -MB- 08/09/2017
      console.log(projectId);
      // Start of Code -SSB- 08/13/2017
      var projectteamId = component.get('v.recordId');
        if(projectteamId == undefined){
          projectteamId = '';
        }
      // end of Code -SSB- 08/13/2017
      
    var action = component.get("c.getProjectNameInfo");
        action.setParams({'ProjectId': projectId ,'ProjectTeamId':projectteamId});  //SB 08-13-17 changed :Passing ProjectTeamId to Controller.
      action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        console.log(response.getReturnValue());
          var projectinfo = response.getReturnValue();
        component.set('v.projectId', projectinfo[0]);		// SSB 08-27-17 Added
        component.set('v.projectName', projectinfo[1]);		// SSB 08-27-17 Added
      }
    });
    $A.enqueueAction(action);
  },
})