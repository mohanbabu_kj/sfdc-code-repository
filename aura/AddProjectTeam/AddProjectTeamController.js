/**
 * Created by rchen180 on 17/4/21.
 */
({
    doInit: function (component, event, helper) {
        helper.getProjectTeammemberRolevalues(component, event);
    //    helper.getProjectAccessLevelvalues(component, event);
          helper.getProjectNameInfo(component, event);
    //    helper.getProjectTeamMember(component, event);
    //    helper.getProjectTeamMemberRole(component, event);  // SB 8-27-17 combined in one call
        console.log(component.get('v.recordId'));
        console.log(component.get('v.oppId'));
        component.set('v.teamRole', ''); 
        var editTeam = component.get('v.recordId');   // Added by SB 08-13-17
        if (editTeam != undefined && editTeam.substring(0,3) == '00q'){  // Added by SB 08-13-17 start
            component.set('v.editAction', true);
            console.log(component.get('v.editAction'));
  			helper.getProjectTeamMember(component, event);
   //     	helper.getProjectTeamMemberRole(component, event);
            console.log(component.get('v.teamRole'));
            console.log(component.get('v.username'));
            console.log(editTeam);
             var searchPage = component.find('search-page');
             var savePage = component.find('save-page');
             var backButton = component.find('back-button');
          //   var cancelButton = component.find('cancel-button');
            	 $A.util.addClass(backButton, 'slds-hide');
        	     $A.util.removeClass(backButton, 'slds-show');
            	 $A.util.addClass(searchPage, 'slds-hide');
        	     $A.util.removeClass(searchPage, 'slds-show');
    	         $A.util.removeClass(savePage, 'slds-hide');
      			 $A.util.addClass(savePage, 'slds-show');
           // 	 $A.util.removeClass(cancelButton, 'slds-hide');
      	  //	 $A.util.addClass(cancelButton, 'slds-show');
        }else{					// Added by SB 08-13-17 end
            component.set('v.teamRole', '');   
        }
    },
    handleSendUserIdEvt: function (component, event, helper) {
        // debugger
        // var urlEvent = $A.get("e.force:navigateToURL");
        // urlEvent.setParams({
        //     "url": 'mobike://'
        // });
        // urlEvent.fire();
        // location.hash = '#/sObject/0012C000008fYS7QAM/view';
        // var OwnerName = localStorage.OwnerName;
        /* ceHandlerController.js */
		component.set('v.teamRole', '');         
        var username = event.getParam('username');
        var userId = event.getParam('userId');
        var territoryId = event.getParam('territoryId');
        console.log(territoryId)
        component.set('v.username', username);
        component.set('v.userId', userId);
        component.set('v.territoryId', territoryId);
        var searchPage = component.find('search-page');
        $A.util.addClass(searchPage, 'slds-hide');
        $A.util.removeClass(searchPage, 'slds-show');
        var savePage = component.find('save-page');
        $A.util.addClass(savePage, 'slds-show');
        $A.util.removeClass(savePage, 'slds-hide');
        
    },
    goBack: function (component, event, helper) {
        var searchPage = component.find('search-page');
        $A.util.addClass(searchPage, 'slds-show');
        $A.util.removeClass(searchPage, 'slds-hide');
        var savePage = component.find('save-page');
        $A.util.addClass(savePage, 'slds-hide');
        $A.util.removeClass(savePage, 'slds-show');
        
    },
    save: function (component, event, helper) {
        // var username = event.getParam('username');
        var userId = component.get('v.userId');
        var territoryId = component.get('v.territoryId');
        console.log(territoryId)
        var projectId = component.get('v.recordId');
        var projectIdEdit = component.get('v.projectId');
        // Start of Code -MB- 08/08/2017
        if(projectId == undefined){
            projectId = component.get('v.oppId');
        } // End of Code -MB- 08/08/2017
        //alert(console.log(component.get('v.teamRole')));
        var teamRole = document.getElementById('teamRole').value;
        // Start of Code -MB- 09/21/2017
        if(teamRole == '' || teamRole == undefined){
            alert('Please select Team Role');
            return;
        }
        //alert(teamRole);
        // End of Code -MB- 09/21/2017
        var context = component.get("v.UserContext"); // Added by MB - 08/08/17
        //var accessLevel = document.getElementById('accessLevel').value;
        // console.log(OwnerName + ' ' + UserId)
        // Owner.value;
        // var methodName = '';
        // var recordId = component.get('v.recordId');
        // var recordIdCondition = recordId.substring(0, 3);
        // if (recordIdCondition === '001') {
        //   // var action = component.get("c.ChangeAccountOwner");
        //   methodName = 'c.ChangeAccountOwner';
        // } else if (recordIdCondition === '00Q') {
        //   methodName = 'c.ChangeLeadOwner';
        // }
        // console.log(recordIdCondition)
        var editTeam = component.get('v.recordId');
        var editAction = component.get('v.editAction');
        var action;                                       			// Added by SB 08-13-17 start 
        if (editAction == true){									// Added by SB 08-13-17
            //           var teamRole =  component.get('v.teamRole');    		// Added by SB 08-13-17  
            action = component.get('c.editProjectTeammember');	    // Added by SB 08-13-17
            action.setParams({'ProjectTeamId': editTeam, 'TeamMemberRole': teamRole}); // Added by SB 08-13-17
        }else {																							
            action = component.get('c.AddProjectProductTeammembers');
            action.setParams({'OpportunityId': projectId, 'UserId': userId, 'TeamMemberRole': teamRole, 'territoryId': territoryId});
            
        } 																 // Added by SB 08-13-17 end 
        console.log(action.getParams())
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                /* commented by MB - 08/08/2017
          var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
		End of Comment -MB- 08/08/2017 */          
                // Start of Code -MB- 08/08/2017
                if(component.get('v.editAction')){       // ssb start of code 
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": projectIdEdit
                    });
                    navEvt.fire(); // ssb end of code
                } else if(component.get('v.listButton')){
                    if (context != undefined) {
                        if (context == 'Theme4t' || context == 'Theme4d' || context == 'Theme3') {
                            sforce.one.navigateToSObject(projectId, 'detail');
                        }
                    }
                }else{
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                }
                // End of Code -MB- 08/08/2017
                console.log(projectIdEdit);
                console.log('suc');
            } else {
                alert('SYSTEM ERROR')
            }
        });
        $A.enqueueAction(action);
  },
    // Start of Code -MB- 08/08/2017
    cancel: function(component, event, helper) {
        var recordId = component.get('v.recordId');
        var projectIdEdit = component.get('v.projectId');
        console.log(recordId);
        console.log(component.get('v.listButton'));
        console.log(component.get('v.editAction'))
        if(recordId == undefined){
            recordId = component.get('v.oppId');
        }
        
        if(component.get('v.editAction')){       // ssb start of code 
            /*recordId= "0062C000002D9fxQAC";  		// SSB for testing ,
            console.log(recordId);   
            sforce.one.navigateToSObject(recordId, 'detail');     // ssb end of code */
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": projectIdEdit
            });
            navEvt.fire();
        } else if (component.get('v.listButton')) {          
            console.log(recordId);
            //   debugger;	
            var context = component.get("v.UserContext");
            console.log(context);
            if (context != undefined) {
                if (context == 'Theme4t' || context == 'Theme4d' || context == 'Theme3') {
                    sforce.one.navigateToSObject(recordId, 'detail');
                }
            }
        }else{
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
        }
        // $A.get('e.force:refreshView').fire();
    }// End of Code -MB- 08/08/2017
})