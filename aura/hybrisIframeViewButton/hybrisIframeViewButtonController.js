({
    triggerHybrisIframe : function(component, event, helper) {
    	// to navigate from one component to another
    	var evt = $A.get("e.force:navigateToComponent");
			evt.setParams({
				componentDef : "c:hybrisIframe",
				componentAttributes: {
					IframeUrl : component.get("v.IframeUrl"),
					isFromRecordPage : component.get('v.recordId')?'true':'false',
					recordId : component.get('v.recordId'),
					minHeight : component.get("v.minHeight")
				}
			});
			// console.dir(evt.getParams());
			evt.fire();
    }
})