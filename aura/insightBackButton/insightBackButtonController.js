({
	handleBackBtn : function(component, event, helper) {
		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:mohawkAdditionalInfoCL_New",
            componentAttributes: {
                recordId : component.get('v.accountId')
            }
        });
        evt.fire();
	}
})