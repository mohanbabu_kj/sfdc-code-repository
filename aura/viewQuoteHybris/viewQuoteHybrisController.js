({
    doInit : function(component, event, helper) {
    	var action = component.get('c.getObjectRecord');
    	action.setParams({
    		apiName: 'Quote',
    		recordId: component.get('v.recordId'),
    		fields: 'Id,External_Quote_Id__c'
    	});
    	action.setCallback(this, function(response) {
    		if (component.isValid() && response.getState() === "SUCCESS") {
    			var url = component.get('v.iframeUrl');
    			var result = response.getReturnValue();
    			component.set('v.iframeUrl', 'https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/quote/my-quote/' + result[0].External_Quote_Id__c + '?site=mhkflooring ');
                // console.log(component.get('v.iframeUrl'))
    			component.set('v.paramList', {
					recordId: result[0].External_Quote_Id__c
				});
                component.set('v.isDataLoaded', true);
                
                var container = component.find("iframeDiv");
                
                $A.createComponent("c:mashupTabItem", { 
                    mashupName: 'View Quote',
                    recordId: result[0].Id,
                    paramList: {}
                    }, function(cmp) { container.set("v.body", [cmp]); }
                );
                
    		}
    	});
    	$A.enqueueAction(action);
    }
})