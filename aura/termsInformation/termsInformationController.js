({
    doInit : function(component, event, helper) {
                helper.deviceOrentationHandler( component, event, helper );

        var accountId = component.get("v.recordId");
        // var myAccountProfileId = 'a042C000000vss6QAA';
        var action = component.get("c.handlePaymentTerms");
        action.setParams({ "accountId" : accountId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.termsInformation", response.getReturnValue());
                console.log(response.getReturnValue());

                if(response.getReturnValue().status !== '0') {
                    component.set("v.errorMsg", response.getReturnValue().exceptionMessage);
                }
                // } else if (response.getReturnValue().status === '6') {
                //     component.set("v.errorMsg", response.getReturnValue().exceptionMessage);
                // } else if (response.getReturnValue().status === '5') {
                //     component.set("v.errorMsg", response.getReturnValue().exceptionMessage);
                // }
                // 
            //    var tempData = response.getReturnValue().oDateTime.replace(',',' - ').replace(' - ',',').replace(',',' - ');
                component.set("v.dateVar", response.getReturnValue().oDateTime);
            }
        });
        $A.enqueueAction(action);

    },
    backToRecord: function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            window.history.back();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            window.location.reload();
        }
    },
     backToInsights: function(component, event, helper){
       var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:mohawkAdditionalInfoCL_New",
                componentAttributes: {
                    recordId : component.get('v.recordId'),
                    "isFromRecordPage":true
                }
            });
            evt.fire();
    },
    close : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
    }
})