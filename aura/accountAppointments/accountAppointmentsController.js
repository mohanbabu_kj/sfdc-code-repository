({
	doInit : function(cmp, evt, helper) {
		helper.initAccoutUser(cmp, evt);
		//helper.initUserAll(cmp, evt); - Commented by MB - Bug 67269 - 11/29/18
        
        /*Commented BY MUDIT - no USE
        helper.getTypeHelper(cmp, evt,helper);
		*/
	},

	saveToBackend : function (component, event, helper) {
		helper.saveSales(component, event);
		helper.saveFundamental(component, event);
	},
	goToAppoitment : function (cmp, evt, helper) {
		helper.goToAppoitment(cmp, evt);
	},
	goToUser : function (cmp, evt, helper) {
		helper.goToUser(cmp, evt);
	},
    backToRecord: function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            window.history.back();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            window.location.reload();
        }
    },
	
	selectUserName : function(cmp, evt, helper) {
        var allDataActivity = cmp.get("v.allDataActivity");
        cmp.set("v.selectedATMValue",evt.currentTarget.value);
        if(allDataActivity){
          helper.populateData(cmp, evt, helper,evt.currentTarget.value,cmp.get("v.selectedTypeValue")); 
        }
        /*Commented By Nagendra
		helper.selectUserName(cmp, evt, evt.currentTarget.value);
        */
	},
	
	sortByColumn : function(cmp, evt, helper) {
		var result = cmp.get('v.appointmentsDetails');
		//var orderBy = evt.currentTarget.dataset.sort;
		var newSortOrder = cmp.get('v.sortOrder')=='asc' ? 'desc' : 'asc';
		var container = cmp.find("sort");
		var icon = newSortOrder=='asc' ? 'arrowdown' : 'arrowup';
        $A.createComponent("c:svgIcon", {
	            class: "slds-is-sortable__icon",
	            svgPath: cmp.get('v.iconPath') + icon,
	            category: "utility",
	            size: "x-small",
	            name: icon
            },
            function(element) {
                container.set("v.body", [element]);
            }
        );
        cmp.set('v.sortOrder', newSortOrder);
		/***** COMMENTED BY MUDIT - ON 17-12-2018 - BUG-67269
        if(newSortOrder === 'asc'){
            result.sort(function(a,b){
                if(a.Owner.Name < b.Owner.Name) return -1;
                if(a.Owner.Name > b.Owner.Name) return 1;
                return 0;
            });
        }else{
            result.sort(function(a,b){
                if(a.Owner.Name > b.Owner.Name) return -1;
                if(a.Owner.Name < b.Owner.Name) return 1;
                return 0;
            });
        }
        *****/
        /***** NEW CODE BY MUDIT - ON 17-12-2018 - BUG-67269 *****/
        if(newSortOrder === 'asc'){
            result.sort(function(a,b){
                if(a.ownerName < b.ownerName) return -1;
                if(a.ownerName > b.ownerName) return 1;
                return 0;
            });
        }else{
            result.sort(function(a,b){
                if(a.ownerName > b.ownerName) return -1;
                if(a.ownerName < b.ownerName) return 1;
                return 0;
            });
        }
        /***** NEW CODE BY MUDIT - ON 17-12-2018 - BUG-67269 *****/
        cmp.set('v.appointmentsDetails', result);

	},
	sortDateTime:function(cmp,evt,helper){
     
		evt.stopPropagation();

		var result = cmp.get('v.appointmentsDetails'),
			container = cmp.find("sort2"),
        	newSortOrder = cmp.get('v.sortDate') === 'asc' ? 'desc' : 'asc',
            icon = newSortOrder === 'asc' ? 'arrowdown' : 'arrowup';

       /* $A.createComponent("c:svgIcon", {
                class: "slds-is-sortable__icon",
                svgPath: cmp.get('v.iconPath') + icon,
                category: "utility",
                size: "x-small",
                name: icon
            },
            function(element) {
                container.set("v.body", [element]);
            }
        );*/
        cmp.set('v.sortDate', newSortOrder);

        //sort function
        if(newSortOrder === 'asc'){
            console.log('inside sorting');
            result.sort(function(a,b){
                 
                return new Date(a.startDate) - new Date(b.startDate);
            });
		}else{
            result.sort(function(a,b){
                return new Date(b.startDate) - new Date(a.startDate);
            });
		}
        
        cmp.set('v.appointmentsDetails', result);
	},
    sortTime:function(cmp,evt,helper){
        var result = cmp.get('v.appointmentsDetails'),
            container = cmp.find("sortstart"),
            newSortOrder = cmp.get('v.sortStart') === 'asc' ? 'desc' : 'asc',
            icon = newSortOrder === 'asc' ? 'arrowdown' : 'arrowup';

        $A.createComponent("c:svgIcon", {
                class: "slds-is-sortable__icon",
                svgPath: cmp.get('v.iconPath') + icon,
                category: "utility",
                size: "x-small",
                name: icon
            },
            function(element) {
                container.set("v.body", [element]);
            }
        );
        cmp.set('v.sortStart', newSortOrder);

        //sort function
        if(newSortOrder === 'asc'){
            result.sort(function(a,b){
                return new Date(a.StartDateTime) - new Date(b.StartDateTime);
            });
        }else{
            result.sort(function(a,b){
                return new Date(b.StartDateTime) - new Date(a.StartDateTime);
            });
        }
        cmp.set('v.appointmentsDetails', result);
    },
    sortStatus : function(cmp, evt, helper) {
        var result = cmp.get('v.appointmentsDetails');
        //var orderBy = evt.currentTarget.dataset.sort;
        var newSortOrder = cmp.get('v.sortStatus')=='asc' ? 'desc' : 'asc';
        var container = cmp.find("sortstatus");
        var icon = newSortOrder=='asc' ? 'arrowdown' : 'arrowup';
        $A.createComponent("c:svgIcon", {
                class: "slds-is-sortable__icon",
                svgPath: cmp.get('v.iconPath') + icon,
                category: "utility",
                size: "x-small",
                name: icon
            },
            function(element) {
                container.set("v.body", [element]);
            }
        );
        cmp.set('v.sortStatus', newSortOrder);

        if(newSortOrder === 'asc'){
            result.sort(function(a,b){
                if(a.Status__c < b.Status__c) return -1;
                if(a.Status__c > b.Status__c) return 1;
                return 0;
            });
        }else{
            result.sort(function(a,b){
                if(a.Status__c > b.Status__c) return -1;
                if(a.Status__c < b.Status__c) return 1;
                return 0;
            });
        }
        cmp.set('v.appointmentsDetails', result);

    },
    sortSubject : function(cmp, evt, helper) {
        var result = cmp.get('v.appointmentsDetails');
        //var orderBy = evt.currentTarget.dataset.sort;
        var newSortOrder = cmp.get('v.sortSubject')=='asc' ? 'desc' : 'asc';
        var container = cmp.find("sortsubject");
        var icon = newSortOrder=='asc' ? 'arrowdown' : 'arrowup';
        $A.createComponent("c:svgIcon", {
                class: "slds-is-sortable__icon",
                svgPath: cmp.get('v.iconPath') + icon,
                category: "utility",
                size: "x-small",
                name: icon
            },
            function(element) {
                container.set("v.body", [element]);
            }
        );
        cmp.set('v.sortSubject', newSortOrder);

        if(newSortOrder === 'asc'){
            result.sort(function(a,b){
                if(a.Subject < b.Subject) return -1;
                if(a.Subject > b.Subject) return 1;
                return 0;
            });
        }else{
            result.sort(function(a,b){
                if(a.Subject > b.Subject) return -1;
                if(a.Subject < b.Subject) return 1;
                return 0;
            });
        }
        cmp.set('v.appointmentsDetails', result);

    },
    handleTypeChange : function( cmp, evt, helper ){
        //alert(evt.currentTarget.value);
        var typeVal = evt.currentTarget.value;
        cmp.set("v.selectedTypeValue",typeVal);
       
        //Start - Changes by Nagendra
         var allDataActivity = cmp.get("v.allDataActivity");
        if(allDataActivity){
          helper.populateData(cmp, evt,helper,cmp.get("v.selectedATMValue"),typeVal);
        }
        
        //End - Changes by Nagendra
    }
})