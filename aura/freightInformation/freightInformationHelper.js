({
	toTitleCase: function(str){
        var firstChar = str.substring(0,1).toUpperCase();
        var remainingChar = str.substring(1,str.length).toLowerCase();
        return firstChar+remainingChar;
    },
    deviceOrentationHandler : function( component,event,helper ){
        //alert(window.orientation);
                    component.set("v.deviceOrentation",window.orientation);

       window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            //alert(window.orientation);
            //return window.orientation;
            component.set("v.deviceOrentation",window.orientation);
        }, false);
    }
})