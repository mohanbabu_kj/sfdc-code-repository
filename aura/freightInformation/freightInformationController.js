({
    doInit : function(component, event, helper) {
        helper.deviceOrentationHandler( component, event, helper ); 	
        var accountId = component.get("v.recordId");
        // var myAccountProfileId = '133';
        var action = component.get("c.handleFreightRate");
        action.setParams({ "accountId" : accountId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.freightInformation", response.getReturnValue());
                console.log(response.getReturnValue());
                if(response.getReturnValue().status !== '0') {
                    component.set("v.errorMsg", response.getReturnValue().exceptionMessage);
                }
                // } else if (response.getReturnValue().status === '5') {
                //     component.set("v.errorMsg", response.getReturnValue().exceptionMessage);
                // } else if (response.getReturnValue().status === '6') {
                //     component.set("v.errorMsg", response.getReturnValue().exceptionMessage);
                // }
                            var tempVar=response.getReturnValue().carpetdlvdays;
                            var tempVar2=[];
                if(tempVar!=null && tempVar!='' && tempVar!="undefined"){
                            tempVar2=tempVar.split(",");
                }
                var days='';
                for(var i=0;i<tempVar2.length;i++){
                    //days+=tempVar2[i].substring(0,3)+', ';
                    //days+=(tempVar2[i].substring(0,3)).substring(1,3).toLowerCase()+', ';
                    days+= helper.toTitleCase( tempVar2[i].substring(0,3) )+', ';
                }
                days=days.slice(0,-2);
                component.set('v.daysVar',days);
                
                var tempVar1=response.getReturnValue().carpetdlvdays;
                            var tempVar3=[];
                if(tempVar1!=null && tempVar1!='' && tempVar1!="undefined"){
                            tempVar3=tempVar1.split(",");
                }
                var days1='';
                for(var i=0;i<tempVar3.length;i++){
                    days1+=tempVar3[i].substring(0,3)+',';
                }
                days1=days1.slice(0,-1);
                component.set('v.daysVar2',days1);
				//alert(days1);
                var tempData;
                if(response.getReturnValue().oDateTime!=null){
                tempData= response.getReturnValue().oDateTime.replace(',',' - ').replace(' - ',',');
                    component.set("v.dateVar",tempData);
                }
                
                
            }
        });
        $A.enqueueAction(action);
    },
    backToRecord: function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            window.history.back();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            window.location.reload();
        }
    },
    
})