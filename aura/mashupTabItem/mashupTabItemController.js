({
    doInit : function(component, event, helper) {
        console.log(component.get('v.mashupName'), '-Mashup');
        console.log(component.get('v.isFromRecordPage'),' Record Page ' );
        var action1 = component.get("c.getUIThemeDescription");
        action1.setCallback(this, function(a) {
            console.log('theme', a.getReturnValue());
            component.set('v.currentTheme', a.getReturnValue());
            if(a.getReturnValue()=='Theme4t')
                component.set('v.inDevice', true);
        });
        $A.enqueueAction(action1);
        
        if(component.get('v.mashupName')!=''){
            
            var mashup = component.find('mashup');
            var error = component.find('error');

            var action = component.get("c.getMashupRecordNew");
            action.setParams({ 
                "mashupName" : component.get('v.mashupName'),
                "recordId" : component.get('v.recordId'),
                "gridProductId" : component.get('v.gridProductId')
            });

            action.setCallback(this, function(response){
                if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                    var resultSet = response.getReturnValue();
                    component.set('v.IframeUrl', resultSet);
                    //component.set('v.IframeUrl', 'https://myanalyticsqa.mohawkind.com/t/Flooring/views/MyForecast-E/MyForecast?&:embed=y#3');
                    console.log('resultSet', resultSet);
                    if(resultSet == ''){
                        $A.util.addClass(mashup, 'slds-hide');
                        $A.util.removeClass(error, 'slds-hide');
                        $A.util.addClass(error, 'slds-show');
                    }
                    
             
                }
            });

            $A.enqueueAction(action); 
        } 
        
    },
    backToRecord: function(component, event, helper){
        // to navigate/go back to record page
             //alert ('test');
          /*   var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                      "url": 'https://myanalyticsdev.mohawkind.com/t/Flooring/views/CustomerSales_0/Sold-toSales'
                    });
                    urlEvent.fire();  */           
             window.open('https://myanalyticsdev.mohawkind.com/t/Flooring/views/CustomerSales_0/Sold-toSales', '_system');
        /*
        var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get('v.recordId'),
                'slideDevName': 'related'
            });
            navEvt.fire(); */
    } 
})