({
	apNameHelper : function(component, event, helper) {
        this.getRecordIdHandler( component );
        console.log( component.get("v.accountId") );
        var action = component.get("c.getAccountProfileDetail");
        //action.setParams({ 'recordId' : component.get('v.recordId') });        
        action.setParams({ 'recordId' : component.get('v.apRecordId') });
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                //console.log( result );
                component.set("v.accountProfileDetail",result); 
            }            
        });
        $A.enqueueAction(action);
    },
    handlePrimaryInputHelper : function(component, event, helper) {
        var container = component.find("primaryInputDiv");        
        $A.createComponent(
            "c:distributionsInAccountProfile",
            {
                //recordId:component.get("v.recordId")
                recordId:component.get("v.apRecordId")
            },
            function(cmp) {
                var cmpMain = component.find("mainDiv");
                console.log( cmpMain );
				$A.util.addClass(cmpMain, 'slds-hide');                
                container.set("v.body", [cmp]);
            }
        );        
    },
    handleRefreshHelper : function(component, event, helper) {
        var container = component.find("primaryInputDiv");
        $A.createComponent(
            "c:rollupMatToAP",
            {
                //recordId:component.get("v.recordId")
                recordId:component.get("v.apRecordId"),
                accountId:component.get("v.accountId")
            },
            function(cmp) {
                var cmpMain = component.find("mainDiv");
                console.log( cmpMain );
				$A.util.addClass(cmpMain, 'slds-hide');                
                container.set("v.body", [cmp]);
            }
        );        
    },
    getRecordIdHandler : function( component ){
        //var recordIdPass;
        if( component.get('v.apRecordId') ){
            //recordIdPass = component.get('v.apRecordId');
            component.set("v.apRecordId",component.get('v.apRecordId'));
        }else{
            //recordIdPass = component.get('v.recordId');
            component.set("v.apRecordId",component.get('v.recordId'));
        }
    },
})