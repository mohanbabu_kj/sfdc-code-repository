({
    initialize : function(component, event, helper){
        var action = component.get("c.initialize");
        var formFieldMap = component.get('v.formFieldMap');
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
                component.set('v.formFieldMap.fieldValuesMap', result.fieldValuesMap);
                component.set('v.formFieldMap.fieldBooleanMap', result.fieldBooleanMap);
                component.set('v.subCategoryList', result.subCategoryList);
                component.set('v.collectionList', result.collectionList);
                component.set('v.promotionList', result.promotionList);
                component.set('v.fiberTypeList', result.fiberTypeList);
                component.set('v.backingDescList', result.backingDescList);
				
                ////Added BY Mudit - BUG 67456 - 28-11-2018
                var FWOptions = [];
                for (var i = 0; i < result.faceWeightList.length; i++) {
                    FWOptions.push({
                            label: result.faceWeightList[i],
                            value: result.faceWeightList[i],
                            selected: false
                        });
                }
                component.set('v.faceWeightOptions',FWOptions);
                ////Added BY Mudit - BUG 67456 - 28-11-2018
                component.set('v.formFieldMap.userCountry', result.userInfo.CountryCode);
                component.set('v.formFieldMap.productInfo', result);
                component.set('v.formFieldMap.userInfo', result.userInfo); //Added BY MB - BUG 68960 - 1/8/19
                if(result.userInfo.International_User__c){
                    component.set('v.formFieldMap.UOMTitle', 'Sq Meters');
                }else{
                    component.set('v.formFieldMap.UOMTitle', 'UOM');
                }
            }else{
                console.log('Initialization Error');
            }
        });
        $A.enqueueAction(action);
    },
    
    validateFilter: function(component, event, helper){
        var formFieldMap = component.get('v.formFieldMap');
        var fieldValuesMap = component.get('v.formFieldMap.fieldValuesMap');
        var fieldBooleanMap = component.get('v.formFieldMap.fieldBooleanMap');
        var subCategory = component.find('subCategory').get("v.value");
        var collection = component.find('collName').get("v.value");
        var promotion = component.find('promotion').get("v.value");
        var minPrice = parseFloat(fieldValuesMap.minPrice);
        var maxPrice = parseFloat(fieldValuesMap.maxPrice);

        console.log('Min Price: ' + fieldValuesMap.minPrice)
        if(fieldValuesMap.minPrice === undefined || fieldValuesMap.minPrice === ''){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Please enter Min Base Price';
            component.set('v.formFieldMap', formFieldMap);
        }else if(fieldValuesMap.maxPrice === undefined || fieldValuesMap.maxPrice === ''){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Please enter Max Base Price';
            component.set('v.formFieldMap', formFieldMap);
        }else if(minPrice < $A.get("$Label.c.Min_Base_Price")){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Min Base Price should not be less than ' + $A.get("$Label.c.Min_Base_Price");
            component.set('v.formFieldMap', formFieldMap);
        }else if(maxPrice < $A.get("$Label.c.Min_Base_Price")){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Max Base Price should not be less than ' + $A.get("$Label.c.Min_Base_Price");
            component.set('v.formFieldMap', formFieldMap);
        }else if(minPrice > maxPrice){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Min Base Price should be less than Max Base Price';
            component.set('v.formFieldMap', formFieldMap);
        }else{
            formFieldMap.isError = formFieldMap.showMessage = false;
            formFieldMap.errorMessage = '';
            component.set('v.formFieldMap', formFieldMap);
        }
    },
    validateFaceWeightFilter: function(component, event, helper){
        var formFieldMap = component.get('v.formFieldMap');
        var fieldValuesMap = component.get('v.formFieldMap.fieldValuesMap');
        var fieldBooleanMap = component.get('v.formFieldMap.fieldBooleanMap');
        var minFaceweight = parseFloat(fieldValuesMap.minFaceweight);
        var maxFaceweight = parseFloat(fieldValuesMap.maxFaceweight);

        /*if(fieldValuesMap.minFaceweight === undefined || fieldValuesMap.minFaceweight === ''){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Please enter Min Face weight';
            component.set('v.formFieldMap', formFieldMap);
        }else if(fieldValuesMap.maxFaceweight === undefined || fieldValuesMap.maxFaceweight === ''){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Please enter Max Face weight';
            component.set('v.formFieldMap', formFieldMap);
        }else*/
        if(minFaceweight < $A.get("$Label.c.Min_Base_Price")){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Min Face weight should not be less than ' + $A.get("$Label.c.Min_Base_Price");
            component.set('v.formFieldMap', formFieldMap);
        }else if(maxFaceweight < $A.get("$Label.c.Min_Base_Price")){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Max Face weight should not be less than ' + $A.get("$Label.c.Min_Base_Price");
            component.set('v.formFieldMap', formFieldMap);
        }else if(minFaceweight > maxFaceweight){
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Min Face weight should be less than Max Face weight';
            component.set('v.formFieldMap', formFieldMap);
        }else{
            formFieldMap.isError = formFieldMap.showMessage = false;
            formFieldMap.errorMessage = '';
            component.set('v.formFieldMap', formFieldMap);
        }
    },
    
    getProductList: function(component, event, helper) {
        var recordId = component.get('v.recordId');
		var formFieldMap = component.get('v.formFieldMap');
        var action = component.get("c.getProductListNew");
        var prodList =[];
        var subCategory = component.find('subCategory').get("v.value");
        var collection = component.find('collName').get("v.value");
        var productInfo = formFieldMap.productInfo;
        var fieldValuesMap = formFieldMap.fieldValuesMap;
        var fieldBooleanMap = formFieldMap.fieldBooleanMap;
        var productsSelected = component.get("v.productsSelected");
        
        formFieldMap.productInfo.fieldValuesMap.subCategory = subCategory;
        formFieldMap.productInfo.fieldValuesMap.collection = collection;
        formFieldMap.productInfo.fieldValuesMap.promotion = component.find('promotion').get("v.value");
        formFieldMap.productInfo.fieldValuesMap.fiberType = component.find('fiberType').get("v.value");
        formFieldMap.productInfo.fieldValuesMap.backing = component.find('backing').get("v.value");
        formFieldMap.productInfo.fieldValuesMap.minFaceweight = fieldValuesMap.minFaceweight;
        formFieldMap.productInfo.fieldValuesMap.maxFaceweight = fieldValuesMap.maxFaceweight;
		formFieldMap.productInfo.selectedFaceWeightList = component.get("v.selectedfaceWeight");//Added BY Mudit - BUG 67456 - 28-11-2018
        
        formFieldMap.productInfo.fieldValuesMap.styleName = fieldValuesMap.styleName;
        formFieldMap.productInfo.fieldValuesMap.styleNumber = fieldValuesMap.styleNumber;
        formFieldMap.productInfo.fieldBooleanMap.quickShip = fieldBooleanMap.quickShip;
        formFieldMap.productInfo.fieldBooleanMap.customProduct = fieldBooleanMap.customProduct;
        formFieldMap.productInfo.fieldBooleanMap.genericProduct = fieldBooleanMap.genericProduct;
        formFieldMap.productInfo.fieldValuesMap.minPrice = fieldValuesMap.minPrice;
        formFieldMap.productInfo.fieldValuesMap.maxPrice = fieldValuesMap.maxPrice;
        formFieldMap.productInfo.prodList = [];
        formFieldMap.isSearchPage = false;
        formFieldMap.isProductDetailPage = true;
        formFieldMap.backText = 'Back';
        formFieldMap.spinner = true;
        component.set('v.formFieldMap', formFieldMap);
        if(recordId === undefined){
            recordId = '';
        }
        console.log('Product Info on Search: ' + JSON.stringify(productInfo));
        action.setParams({
            productInfoStr: JSON.stringify(productInfo),
            recId: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            
            if (component.isValid() && state === "SUCCESS") {
                prodList = result.prodList;
                component.set('v.productList', result.prodList);
            }else{
                console.log('Product fetching Error');
            }
            console.log('ProductList: ' + JSON.stringify(prodList));
            if(prodList.length>0){
                for(var i in prodList){
                    if(prodList[i].pr !== undefined){
                        //console.log('Floor USD: '  + prodList[i].pr.Floor_USD__c);
                        if(formFieldMap.userCountry === 'US'){
                            prodList[i].pr.Floor_USD__c = ((prodList[i].pr.Floor_USD__c !== undefined && prodList[i].pr.Floor_USD__c !== 0) ? prodList[i].pr.Floor_USD__c : 0); 
                            prodList[i].pr.Mid_Market_USD__c = ((prodList[i].pr.Mid_Market_USD__c !== undefined && prodList[i].pr.Mid_Market_USD__c !== 0) ? prodList[i].pr.Mid_Market_USD__c : 0); 
                            prodList[i].pr.Stretch_USD__c = ((prodList[i].pr.Stretch_USD__c !== undefined && prodList[i].pr.Stretch_USD__c !== 0) ? prodList[i].pr.Stretch_USD__c : 0);
                            prodList[i].pr.SVP_Roll_Price_USD__c = ((prodList[i].pr.SVP_Roll_Price_USD__c !== undefined && prodList[i].pr.SVP_Roll_Price_USD__c !== 0) ? prodList[i].pr.SVP_Roll_Price_USD__c : 0);
                            prodList[i].pr.Bonus_Price_USD__c = ((prodList[i].pr.Bonus_Price_USD__c !== undefined && prodList[i].pr.Bonus_Price_USD__c !== 0) ? prodList[i].pr.Bonus_Price_USD__c : 0);
                            prodList[i].pr.X1_0_Pricing_USD__c = ((prodList[i].pr.X1_0_Pricing_USD__c !== undefined && prodList[i].pr.X1_0_Pricing_USD__c !== 0) ? prodList[i].pr.X1_0_Pricing_USD__c : 0);
                            prodList[i].pr.X1_5_Pricing_USD__c = ((prodList[i].pr.X1_5_Pricing_USD__c !== undefined && prodList[i].pr.X1_5_Pricing_USD__c !== 0) ? prodList[i].pr.X1_5_Pricing_USD__c : 0);
                            prodList[i].pr.X2_0_Pricing_USD__c = ((prodList[i].pr.X2_0_Pricing_USD__c !== undefined && prodList[i].pr.X2_0_Pricing_USD__c !== 0) ? prodList[i].pr.X2_0_Pricing_USD__c : 0);
                            prodList[i].pr.X2_5_Pricing_USD__c = ((prodList[i].pr.X2_5_Pricing_USD__c !== undefined && prodList[i].pr.X2_5_Pricing_USD__c !== 0) ? prodList[i].pr.X2_5_Pricing_USD__c : 0);
                            if(prodList[i].price === 0 || prodList[i].price === '' || prodList[i].price === undefined){
                            	prodList[i].price = prodList[i].pr.Mid_Market_USD__c;
                            }
                        }else{
                            prodList[i].pr.Floor_CAN__c = ((prodList[i].pr.Floor_CAN__c !== undefined && prodList[i].pr.Floor_CAN__c !== 0) ? prodList[i].pr.Floor_CAN__c : 0);
                            prodList[i].pr.Mid_Market_CAN__c = ((prodList[i].pr.Mid_Market_CAN__c !== undefined && prodList[i].pr.Mid_Market_CAN__c !== 0) ? prodList[i].pr.Mid_Market_CAN__c : 0);
                            prodList[i].pr.Stretch_CAN__c = ((prodList[i].pr.Stretch_CAN__c !== undefined && prodList[i].pr.Stretch_CAN__c !== 0) ? prodList[i].pr.Stretch_CAN__c : 0);
                            prodList[i].pr.SVP_Roll_Price_CAN__c = ((prodList[i].pr.SVP_Roll_Price_CAN__c !== undefined && prodList[i].pr.SVP_Roll_Price_CAN__c !== 0) ? prodList[i].pr.SVP_Roll_Price_CAN__c : 0);
                            prodList[i].pr.Bonus_Price_CAN__c = ((prodList[i].pr.Bonus_Price_CAN__c !== undefined && prodList[i].pr.Bonus_Price_CAN__c !== 0) ? prodList[i].pr.Bonus_Price_CAN__c : 0);
                            prodList[i].pr.X1_0_Pricing_CAD__c = ((prodList[i].pr.X1_0_Pricing_CAD__c !== undefined && prodList[i].pr.X1_0_Pricing_CAD__c !== 0) ? prodList[i].pr.X1_0_Pricing_CAD__c : 0);
                            prodList[i].pr.X1_5_Pricing_CAD__c = ((prodList[i].pr.X1_5_Pricing_CAD__c !== undefined && prodList[i].pr.X1_5_Pricing_CAD__c !== 0) ? prodList[i].pr.X1_5_Pricing_CAD__c : 0);
                            prodList[i].pr.X2_0_Pricing_CAD__c = ((prodList[i].pr.X2_0_Pricing_CAD__c !== undefined && prodList[i].pr.X2_0_Pricing_CAD__c !== 0) ? prodList[i].pr.X2_0_Pricing_CAD__c : 0);
                            prodList[i].pr.X2_5_Pricing_CAD__c = ((prodList[i].pr.X2_5_Pricing_CAD__c !== undefined && prodList[i].pr.X2_5_Pricing_CAD__c !== 0) ? prodList[i].pr.X2_5_Pricing_CAD__c : 0);
                            if(prodList[i].price === 0 || prodList[i].price === '' || prodList[i].price === undefined){
                            	prodList[i].price = prodList[i].pr.Mid_Market_CAN__c;
                            }
                        }
                         
                        if(productsSelected.length > 0){
                            for(var idx=0; idx<productsSelected.length; idx++){
                                if(productsSelected[idx].pr !== undefined && prodList[i].pr.Id === productsSelected[idx].pr.Id){
                                    prodList[i].isSelected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                component.set('v.productList', prodList);
                console.log(JSON.stringify(component.get('v.productList')));
            }
            formFieldMap.spinner = false;
            component.set('v.formFieldMap', formFieldMap);
        });
        $A.enqueueAction(action);

	},
    
    validateItems: function(component){
        var formFieldMap = component.get('v.formFieldMap');
        var productsSelected = component.get('v.productsSelected');
        
        if(productsSelected.length>0){
            for(var i = 0; i < productsSelected.length; i++){
                console.log('Line Item: ' + JSON.stringify(productsSelected[i]));
                if(productsSelected[i].quantity === undefined || productsSelected[i].quantity === null || 
                   productsSelected[i].quantity === '' || productsSelected[i].quantity === 0 || productsSelected[i].quantity < 0)
                {
                    formFieldMap.errorMessage = 'Please enter valid Quantity for ' + productsSelected[i].pr.Name ;
                    formFieldMap.isError = true;
                    break;
                }
                if(productsSelected[i].price === undefined || productsSelected[i].price === null || 
                   productsSelected[i].price === '' || productsSelected[i].price === 0 || productsSelected[i].price < 0)
                {
                    formFieldMap.errorMessage = 'Please enter valid Price for ' + productsSelected[i].pr.Name ;
                    formFieldMap.isError = true;
                    break;
                }
            }
        }
        console.log('Products Selected: ' + JSON.stringify(productsSelected));
        if(formFieldMap.isError){
            formFieldMap.showMessage = true;
            component.set('v.formFieldMap', formFieldMap);
            //console.log('Field Map: ' + JSON.stringify(formFieldMap));
        }
    },
    
    save: function(component, event, helper){
        var productsSelected = component.get('v.productsSelected')
        var projectId = component.get('v.recordId');
        var action = component.get('c.AddProductstoProject');
		var formFieldMap = component.get('v.formFieldMap');
        
        formFieldMap.spinner = true;
        component.set('v.formFieldMap', formFieldMap);
        console.log('Selected Products: ' + JSON.stringify(productsSelected));
        action.setParams({
            'ProjectId': projectId,
            'SelectedProducts': JSON.stringify(productsSelected)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                var result = response.getReturnValue();
                if(result){
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                } else {
                    formFieldMap.isError = true;
                    formFieldMap.showMessage = true;
                    formFieldMap.errorMessage = 'Error occurred while saving products. Please try after sometime';
                }
            } else {
                formFieldMap.isError = true;
                formFieldMap.showMessage = true;
                formFieldMap.errorMessage = 'Error occurred while saving products. Please try after sometime';
            }
            formFieldMap.spinner = false;
        	component.set('v.formFieldMap', formFieldMap);
        });
        $A.enqueueAction(action);
    },
    
    closeAlertMsg: function(component, event, helper){
        var formFieldMap = component.get('v.formFieldMap');
        formFieldMap.errorMessage = formFieldMap.viewURL = formFieldMap.dialogMessage = '';
        formFieldMap.showNavigateMsg = formFieldMap.isError = formFieldMap.showMessage = false;
        component.set('v.formFieldMap',formFieldMap);
    }
})