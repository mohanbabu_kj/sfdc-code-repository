({
	doInit : function(component, event, helper) {
       

        //component.set()
		var formFieldMap = new Map();
        var recordId = component.get('v.recordId');
        if(component.get('v.device') === 'DESKTOP'){
            formFieldMap.isDesktop = true;
            formFieldMap.formGridSize = 'slds-size--1-of-2';
        }else if(component.get('v.device') === 'TABLET'){
            formFieldMap.isTablet = true;
            formFieldMap.formGridSize = 'slds-size--1-of-2';
        }else if(component.get('v.device') === 'PHONE'){
            formFieldMap.isPhone = true;
            formFieldMap.formGridSize = 'slds-size--1-of-1';
        }
        if(recordId !== undefined && recordId !== ''){
        	formFieldMap.title = 'Add Product';
            formFieldMap.isRecord = true;
            if(formFieldMap.isTablet){
                formFieldMap.isRecordTablet = true;
            }
        }else{
            formFieldMap.title = '';
            formFieldMap.isRecord = false;
        }
        formFieldMap.backText = 'Back';
        formFieldMap.isSearchPage = true;
        formFieldMap.isProductDetailPage = false;
        formFieldMap.isLineItemPage = false;
        formFieldMap.recordId = recordId;
        formFieldMap.productsSelected = [];
        component.set('v.formFieldMap', formFieldMap);
        
        helper.initialize(component, event, helper);
        console.log('FormFieldMap: ' + JSON.stringify(formFieldMap));
        
	},
    
    onReset:function(component,event,helper){
        component.find('promotion').set("v.value",'none');
        component.find('collName').set("v.value",'none');
        component.find('subCategory').set("v.value",'none');
        component.find('fiberType').set("v.value",'none');
        component.find('backing').set("v.value",'none');
        component.set('v.formFieldMap.fieldValuesMap.styleName','');
        component.set('v.formFieldMap.fieldValuesMap.styleNumber','');
        component.set('v.formFieldMap.fieldValuesMap.minPrice',$A.get("$Label.c.Min_Base_Price"));
        component.set('v.formFieldMap.fieldValuesMap.maxPrice',$A.get("$Label.c.Max_Base_Price"));
        component.set('v.formFieldMap.fieldValuesMap.minFaceweight',$A.get("$Label.c.Min_Base_Price"));
        component.set('v.formFieldMap.fieldValuesMap.maxFaceweight',$A.get("$Label.c.Max_Base_Price"));
        component.set('v.formFieldMap.fieldBooleanMap.genericProduct',false);
        component.set('v.formFieldMap.fieldBooleanMap.quickShip',false);
        component.set('v.formFieldMap.fieldBooleanMap.customProduct',false);
        ////Added BY Mudit - BUG 67456 - 06-12-2018
        var resetVal = [];
        var previousVal = component.get("v.faceWeightOptions");
        console.log( previousVal[0].label );
        for (var i = 0; i < previousVal.length; i++) {
            console.log( previousVal[i].label );
            console.log( previousVal[i].value );
            resetVal.push({
                label: previousVal[i].label,
                value: previousVal[i].value,
                selected: false
            });
        }
        component.set('v.faceWeightOptions',resetVal);
		////Added BY Mudit - BUG 67456 - 06-12-2018
    },
    onSearch : function(component, event, helper){
        var productDetails = component.find('productDetails');
        var searchPage = component.find('searchPage');
		var formFieldMap = component.get('v.formFieldMap');  
        
        helper.validateFilter(component, event, helper);
        helper.validateFaceWeightFilter(component, event, helper);

        if(formFieldMap.isError === false){
            $A.util.removeClass(searchPage, 'slds-show');
            $A.util.addClass(searchPage, 'slds-hide');
            $A.util.removeClass(productDetails, 'slds-hide');
            $A.util.addClass(productDetails, 'slds-show');
            helper.getProductList(component, event, helper);
        }
    },
    
    onSelect: function(component, event){
        var formFieldMap = component.get('v.formFieldMap');
        var productList = component.get('v.productList');
        var productsSelected = component.get('v.productsSelected');
        var prodList = component.get('v.productList'); //formFieldMap.prodList;
        var prodId = event.getSource().get('v.value');
        // Start of Comment - MB - Bug 69678 - 2/5/19
        /*for(var i in prodList){
            productsSelected = prodList;
            if(prodList[i].isSelected !== undefined && prodList[i].isSelected){
                for(var idx in productsSelected){
                    if(productsSelected[idx].pr !== undefined && prodList[i].pr !== undefined && productsSelected[idx].pr.Id === prodList[i].pr.Id){
                        productsSelected[idx].isSelected = true;
                    }
                }
            }else if(prodList[i].isSelected !== undefined && !prodList[i].isSelected){
                for(var idx in productsSelected){
                    if(productsSelected[idx].pr !== undefined && prodList[i].pr !== undefined && productsSelected[idx].pr.Id === prodList[i].pr.Id){
                        productsSelected[idx].isSelected = false;
                    }
                }
            }
        }
        prodList = productsSelected;
        productsSelected = component.get('v.productsSelected');*/
        // End of Comment - MB - Bug 69678 - 2/5/19
        for(var i in prodList){
            if(prodList[i].isSelected !== undefined && prodList[i].isSelected && prodId === prodList[i].pr.Id){
                console.log('Selected Product: ' + JSON.stringify(prodList[i]));
                productsSelected.push(prodList[i]);
            }else if(prodList[i].isSelected !== undefined && !prodList[i].isSelected && prodId === prodList[i].pr.Id){
                for(var idx=0; idx<productsSelected.length; idx++){
                    if(productsSelected[idx].pr !== undefined && prodId === productsSelected[idx].pr.Id){
                        productsSelected.splice(idx, 1);
                        //break;
                    }
                }
            }
        }
        //component.set('v.productList', productList);
        component.set('v.productsSelected', productsSelected);
        // Start of Comment - MB - Bug 69678 - 2/5/19
        /*var customProductExist = false;
        for(var idx in productsSelected){
            if(productsSelected[idx].pr !== undefined && productsSelected[idx].pr.Custom_Product__c){
                customProductExist = true;
            }
        }
        component.set('v.formFieldMap.customProductExist', customProductExist);*/
        // End of Comment - MB - Bug 69678 - 2/5/19
    },
    
    onNext : function(component){
        var formFieldMap = component.get('v.formFieldMap');
        var productDetails = component.find('productDetails');
        var lineItem = component.find('lineItem');
        //var prodList = formFieldMap.prodList;
        var productsSelected = component.get('v.productsSelected');
        
        
        console.log('On Next: ' + JSON.stringify(productsSelected));
        if(productsSelected !== undefined && productsSelected.length > 0){
            $A.util.removeClass(productDetails, 'slds-show');
            $A.util.addClass(productDetails, 'slds-hide');
            $A.util.removeClass(lineItem, 'slds-hide');
            $A.util.addClass(lineItem, 'slds-show');
            formFieldMap.backText = 'Back';
            formFieldMap.isProductDetailPage = false;
            formFieldMap.isLineItemPage = true;
            //component.set('v.productsSelected', productsSelected);
        }else{
            formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'Please select at least one Product.'
        }
        component.set('v.formFieldMap', formFieldMap);
        
    },
    
    onBack : function(component){
        var formFieldMap = component.get('v.formFieldMap');
        var searchPage = component.find('searchPage');
        var productDetails = component.find('productDetails');
        var lineItem = component.find('lineItem');

        formFieldMap.backText = 'Back';//Replaced Filter to Back for bug 65443 on Sep 26,2018
        if(formFieldMap.isProductDetailPage){
            formFieldMap.isSearchPage = true;
            formFieldMap.isProductDetailPage = false;
            $A.util.removeClass(productDetails, 'slds-show');
            $A.util.addClass(productDetails, 'slds-hide');
            $A.util.removeClass(searchPage, 'slds-hide');
            $A.util.addClass(searchPage, 'slds-show');
        }else if(formFieldMap.isLineItemPage){
            $A.util.removeClass(lineItem, 'slds-show');
            $A.util.addClass(lineItem, 'slds-hide');
            $A.util.removeClass(productDetails, 'slds-hide');
            $A.util.addClass(productDetails, 'slds-show');
            formFieldMap.isProductDetailPage = true;
            formFieldMap.isLineItemPage = false;
        }
        
        component.set('v.formFieldMap', formFieldMap);
    },
    
    onSave : function(component, event, helper){
        
        helper.validateItems(component, event, helper);
        var formFieldMap = component.get('v.formFieldMap');
        if(!formFieldMap.isError){
        	helper.save(component, event, helper);
        }
    },
    
    openURL: function(component, event, helper){
        //var formFieldMap = component.get('v.formFieldMap');
        var viewURL = event.target.dataset.url;
        //var msg = $A.get("$Label.c.Navigate_Message");
        if(viewURL !== undefined && viewURL !== ''){
            /*formFieldMap.viewURL = viewURL;
            formFieldMap.dialogTitle = $A.get("$Label.c.Navigate_Title");
            formFieldMap.dialogMessage = msg = msg.replace("URL", viewURL);
            //formFieldMap.showNavigateMsg = true;
            if(formFieldMap.recordId !== undefined && formFieldMap.recordId !== '' && (formFieldMap.isPhone || formFieldMap.isTablet)){
                formFieldMap.dialogMessage = msg.replace("deviceMsg", 'Opening the URL will close the current screen');
            }else{
                formFieldMap.dialogMessage = msg.replace("deviceMsg", 'Page will be open in new tab');
            }
            alert(formFieldMap.dialogMessage);*/
            var eUrl= $A.get("e.force:navigateToURL");
            eUrl.setParams({
                "url": viewURL 
            });
            eUrl.fire();
        }else{
            /*formFieldMap.isError = true;
            formFieldMap.showMessage = true;
            formFieldMap.errorMessage = 'No URL found!';*/
            alert('No URL found!');
        }
        //component.set('v.formFieldMap', formFieldMap);
    },
    
    /*navigateToURL: function(component, event, helper){
        var viewURL = component.get('v.formFieldMap.viewURL');
        var eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
            "url": viewURL 
        });
        eUrl.fire();
        helper.closeAlertMsg(component, event, helper);
    },*/
    
    handleMouseEnter: function(component){
        //component.set('v.formFieldMap.FaceWeightDivOpen', true);
    },
    
    handleMouseLeave: function(component){
        //component.set('v.formFieldMap.FaceWeightDivOpen', false);
    },
    
    cancel : function(component){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    closeAlertMsg: function(component, event, helper){
        helper.closeAlertMsg(component, event, helper);
    },
    handleSelectChange: function(component, event, helper) {
        var selectedFaceWeightVal = event.getParam("data");
        var selectedFaceWeight = selectedFaceWeightVal[0].values;
        component.set("v.selectedfaceWeight",selectedFaceWeight);
    },
    
    cloneLineItem: function(component, event, helper){
        var selectedProductId = event.getSource().get('v.value');
        var productsSelected = component.get('v.productsSelected');
        var prodList = component.get('v.productList');
        var str = JSON.stringify(prodList);
        var newItems = JSON.parse(str);
        for(var i in newItems){
            if(newItems[i].pr.Id !== undefined && selectedProductId !== undefined && newItems[i].pr.Id === selectedProductId){
                console.log('Selected Product: ' + JSON.stringify(prodList[i]));
                newItems[i].color = ''; newItems[i].price = 0; newItems[i].quantity = 0;
                productsSelected.push(newItems[i]);
                break;
            }
        }
        component.set('v.productsSelected', productsSelected);
    },
    
    deleteLineItem: function(component, event, helper){
        var selectedProductIndex = event.getSource().get('v.value');
        var productsSelected = component.get('v.productsSelected');
        var str = JSON.stringify(productsSelected);
		var newItems = JSON.parse(str);
        console.log('selectedProductIndex: ' + selectedProductIndex);
        newItems.splice(selectedProductIndex, 1);
        component.set('v.productsSelected', newItems);
        console.log('productsSelected: ' + JSON.stringify(newItems));
    }
})