/**************************************************************************

Name : SendHttpRequest 

===========================================================================
Purpose :   provide the reuse function for http request
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         9/Feb/2017     Created 
***************************************************************************/
public with sharing class SendHttpRequest { 
    
    public static String sendRequest(String globalAccountNumber, String CamsAccountNumber, String callOutStr){
      try {
            
            String strResBody = '';  
            // We have to use my Account Profile Id to get the globalAccountNumber field. 
            // Then we will send globalAccountNumber as a para 
            if (UtilityCls.isStrNotNull(globalAccountNumber) || UtilityCls.isStrNotNull(CamsAccountNumber)) {
                   String strReqBody ='';
                if(UtilityCls.isStrNotNull(globalAccountNumber)){
                     System.debug('SendHttpRequest : globalAccountNumber : ' + globalAccountNumber);
                     strReqBody = '{"buId":"1","erpId":"1","globalAccountNumber":"'+globalAccountNumber+'"}';                    
                } else if (UtilityCls.isStrNotNull(CamsAccountNumber)){
                     System.debug('SendHttpRequest : CamsAccountNumber : ' + CamsAccountNumber);
                     strReqBody = '{"camsAccountNumber":"'+CamsAccountNumber+'"}';    
                }
                
                // we use the Named Credential and label is Freight Service     
                // the below code will not templately work.
                HttpRequest req = new HttpRequest();
                req.setEndpoint('callout:'+callOutStr);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/json;charset=UTF-8');
              
                System.debug('SendHttpRequest : request Body : '+ strReqBody);
                req.setBody(strReqBody);

                Http http = new Http();
                HTTPResponse res = http.send(req);
                strResBody = res.getBody();
                System.debug('SendHttpRequest : response Body : ' + strResBody);                
            }

            return strResBody;
    }catch (Exception ex) {
            // try to record the error message
            String sourceFunction ='class to send HTTP request along with global account number. Parse the response as specified format. Iterate over the wrapper class list to hold response. Return the formatted response.';
            String logCode = 'the method is SendHttpRequest()' ;
            String referenceInfo = 'We use the Global_Account_Number__c field to retrive the information';
            UtilityCls.createExceptionLog(ex,  'SendHttpRequest', sourceFunction, logCode, globalAccountNumber, referenceInfo);
            
            return '';
        }
    }
}