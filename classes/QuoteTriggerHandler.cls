public class QuoteTriggerHandler {
    public static Boolean allow_status_change = false;
    
    /*public static void validateStatusChange(Map<Id, Quote> newMap, Map<Id, Quote> oldMap){
        Boolean adminUser = false;
        String profileName = [Select Name from Profile where Id =: UserInfo.getProfileid()].Name;
        profileName = profileName == null ? '' : profileName;
        System.debug('profileName : ' + profileName);
        if(profileName.equalsIgnoreCase(UtilityCls.SYSTEM_ADMINISTRATOR) || 
           profileName.equalsIgnoreCase(UtilityCls.ADMINISTRATOR) || 
           profileName.equalsIgnoreCase(UtilityCls.BU_IS_USERS) ||
           profileName.equalsIgnoreCase(UtilityCls.INTEGRATION_USER) ||
           profileName.equalsIgnoreCase(UtilityCls.COMM_SALES_OPS)){ 
               adminUser = true;
           }
        for(Quote q: newMap.values()){
            if(!allow_status_change && q.Status != oldMap.get(q.Id).Status && !adminUser ){
                if(!Test.isRunningTest()){
                    q.addError('Quote Status cannot be changed manually!');
                }else{
                    System.debug('Error occurred');
                }
            }
        }
    }*/
    
    public static void chatterPostForSAM(List<Quote> newList) {
        Map<Id, Mohawk_Account_Team__c> mhkTeamSAMMap = new Map<Id, Mohawk_Account_Team__c>();
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        Set<Id> strAccIds = new Set<Id>();
        Set<Id> oppIds = new Set<Id>();
        for (Quote q: newList) {
            if(q.Strategic_Account__c != null){
                strAccIds.add(q.Strategic_Account__c);
            }
            oppIds.add(q.OpportunityId);
        }
        System.debug('Str. Acc: ' + strAccIds);
        try{
            for(List<Mohawk_Account_Team__c> mhkTeamList : [SELECT Id, Account__c, Account__r.Name, User__c FROM Mohawk_Account_Team__c 
                                                            WHERE Account__c IN :strAccIds 
                                                            AND Role__c =: UtilityCls.STRATEGIC_ACCOUNT_MANAGER]){
                                                                for(Mohawk_Account_Team__c mhkTeam: mhkTeamList){
                                                                    mhkTeamSAMMap.put(mhkTeam.Account__c, mhkTeam);
                                                                }
                                                            }
        }
        catch(Exception ex){
            System.debug('No SAM available');
        }
        
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Name from Opportunity WHERE Id IN: oppIds]);
        System.debug('MHK Team Map: ' + mhkTeamSAMMap);
        for(Quote q: newList){
            if(q.Strategic_Account__c != null){
                Mohawk_Account_Team__c mhkTeam = mhkTeamSAMMap.get(q.Strategic_Account__c);
                if(mhkTeam != null){
                    Opportunity opp = oppMap.get(q.OpportunityId);
                    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                    ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                    ConnectApi.LinkCapabilityInput linkInput = new ConnectApi.LinkCapabilityInput();
                    
                    messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
                    mentionSegmentInput.id = mhkTeam.User__c; //Mention user 
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                    
                    textSegmentInput.text = ' ' + UserInfo.getName() + ' has created a new quote for ' + mhkTeam.Account__r.Name + ' on project ' +
                        Opp.Name + '. View the Quote: \n\n';
                    messageBodyInput.messageSegments.add(textSegmentInput);
                    textSegmentInput = new ConnectApi.TextSegmentInput();
                    textSegmentInput.text = q.URL_Label__c + q.Id + '/view';
                    messageBodyInput.messageSegments.add(textSegmentInput);
                    
                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = mhkTeam.User__c;
                    
                    ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
                    batchInputs.add(batchInput);       
                }
            }        
        }
        if(!Test.isRunningTest()){
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
        }
        
    }
}