public without sharing  class ProductCustGroupRelTriggerHandler {
 
	public static void updateIndependentAlignedBuyingGroup(List<Product_Customer_Group_Relationship__c> prodCustGrupRelObjects) {

		Map<String, String> productMap = new Map<String, String>();


		//Get the required fields from records in the trigger
		List<Product_Customer_Group_Relationship__c> pcgrList = [
			SELECT Id, Product__c, Customer_Group_Number__c
			FROM Product_Customer_Group_Relationship__c 
			WHERE Id IN : prodCustGrupRelObjects];

		System.Debug('updateIndependentAlignedBuyingGroup - pcgrList: ' + pcgrList);

		//Build the MAP with Customer Group as KEY and Product ID as value
		for (Product_Customer_Group_Relationship__c pcgr : pcgrList) 
			productMap.put(pcgr.Customer_Group_Number__c, pcgr.Product__c);
		
		System.Debug('updateIndependentAlignedBuyingGroup - productMap: ' + productMap);


		//Get Buying Groups records from the MAP key set
		List<Buying_Group__c> bgList = [
			SELECT Id, Buying_Group_Number__c
			FROM Buying_Group__c 
			WHERE Independent_Aligned_Group__c = true AND Buying_Group_Number__c IN : productMap.keySet()];

		System.Debug('updateIndependentAlignedBuyingGroup - bgList: ' + bgList);

		//Update Independent_Aligned_Buying_Group__c field in Product2 objects related 
		//to Independent Alligned Buying_Groups
		List<Product2> productsToUpdate = new List<Product2>();
        List<Product2> UniqueProductsToUpdate = new List<Product2>();
		for (Buying_Group__c bg : bgList) {
            String productID = (String) productMap.get(bg.Buying_Group_Number__c);
			Product2 p = new Product2();
			p.Id = productID;
			p.Independent_Aligned_Buying_Group__c = bg.Id;
            productsToUpdate.add(p); 
       }
        // 
        // Bug 64691  - updating unique product record with  buying group. 
        System.debug('productsToUpdate : ' + productsToUpdate);
		Map<Id,Product2> idProductMap = new Map<Id,Product2>();
        for (Product2 p : productsToUpdate){
            if (!idProductMap.containsKey(p.Id)){
                idProductMap.put(p.id,p);
            }
        }
        UniqueProductsToUpdate = idProductMap.values();

        if (UniqueProductsToUpdate.size() > 0){
            System.debug('UniqueProductsToUpdate : ' + UniqueProductsToUpdate); 
            update UniqueProductsToUpdate;
        }
	}   
}