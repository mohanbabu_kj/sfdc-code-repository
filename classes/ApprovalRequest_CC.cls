public with sharing class ApprovalRequest_CC {
	
    @AuraEnabled
    public static List<wrapApprovalRequest> getApprovalWorkitems(){
        List<wrapApprovalRequest> appReqList = new List<wrapApprovalRequest>();
        String logonuserId = UserInfo.getUserId();
        Set<Id> userIds = new Set<Id>();
        Set<Id> objIds = new Set<Id>();
        Map<Id, String> userIdNameMap = new Map<Id, String>();
        List<ProcessInstance> procInstanceList = new List<ProcessInstance>();
        
        for(ProcessInstance procInst : [SELECT Id, TargetObjectId, TargetObject.Name, Status, SubmittedById, SubmittedBy.FirstName, SubmittedBy.LastName, CreatedDate, 
                                        (SELECT Id, ActorId FROM Workitems WHERE ActorId =: logonuserId)
                                                     FROM ProcessInstance Where Status =: UtilityCls.Pending]){
                                                         for(ProcessInstanceWorkitem wI: procInst.Workitems){
                                                             wrapApprovalRequest appReq = new wrapApprovalRequest();
                                                             Date submitDate = procInst.CreatedDate.date();
                                                             appReq.submittedDate = submitDate.format();
                                                             appReq.name = procInst.TargetObject.Name;
                                                             appReq.id = wI.Id;
                                                             appReq.objectType = UtilityCls.getObjectLabelById(procInst.TargetObjectId);
                                                             appReq.submittedById = procInst.SubmittedById;
                                                             appReq.submittedBy = procInst.SubmittedBy.FirstName + ' ' + procInst.SubmittedBy.LastName;
                                                             //System.debug('Workitem ' + appReq);
                                                             appReqList.add(appReq);
                                                         }
                                                     }
        
        for(Claim_Approvers__c claimAppr : [SELECT Id, Approver__c, CreatedById, Submitter_Name__c, Claim__c, Claim__r.Name, Claim__r.Account__c,
                                            Claim__r.Account_Global_Id__c, Claim__r.Claim_Amount__c, Claim__r.Claim_Type__c, Claim__r.Claim_Submit_Date__c 
                                            FROM Claim_Approvers__c 
                                            Where Approver__c =: logonuserId
                                            AND Claim__c IN (SELECT Id FROM Claim__c Where Status__c IN ('INPROCESS' , 'ACTIONREQUIRED'))]){
                                                wrapApprovalRequest appReq = new wrapApprovalRequest();
                                                if(claimAppr.Claim__r.Claim_Submit_Date__c != null){
                                                	appReq.submittedDate = claimAppr.Claim__r.Claim_Submit_Date__c.format();
                                                }
                                                appReq.objectType = UtilityCls.getObjectLabelById(claimAppr.Claim__c);
                                                appReq.submittedBy = claimAppr.Submitter_Name__c;
                                                appReq.name = claimAppr.Claim__r.Name;
                                                appReq.submittedById = claimAppr.CreatedById;
                                                appReq.id = claimAppr.Claim__c;
                                                appReqList.add(appReq);
                                            }
        System.debug('processInstance' + appReqList);
        return appReqList;
    }
    
    public class wrapApprovalRequest{
        @AuraEnabled
        public String name;
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public Id submittedById;
        @AuraEnabled
        public String submittedDate;
        @AuraEnabled
        public String objectType;
        @AuraEnabled
        public String submittedBy;
    } 
}