/**************************************************************************

Name : Mashups_CC_Test


===========================================================================
Purpose : This tess class is used for Mashups_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         22/Feb/2017     Created 
1.1        Susmitha       07/Nov/2017     Modified      Added testMashupsMethod4
***************************************************************************/
@isTest
private class Mashups_CC_Test {
    static List<Mashup_Management__c> mashupManList; 
    static Mashup_Management__c data;
    static Mashup_Management__c comData;
    static Mashup_Management__c resData;
    static User comUser = Utility_Test.getTestUser('Commercial', 'Commercial Sales User');
    static User resUser = Utility_Test.getTestUser('Residential', 'Residential Sales User');

    public static void init(){
        mashupManList  = Utility_Test.getMashupManagementCusSetting();
        data = mashupManList.get(0);
        comData = mashupManList.get(1);
        resData = mashupManList.get(2);
    }

    static testMethod void testMashupsMethod1() {
        init();
        test.startTest();
            System.debug('data : ' + data);
            Mashups_CC.getUrlByName(data.Name);
            Mashups_CC.validateProfileName('Commerical');
            Mashups_CC.validateProfileName('Admin');
            Mashups_CC.getUIThemeDescription();
            Mashups_CC.getMashupRecordNew(data.Name, data.Id, '');
            Mashups_CC.getMashupRecordNew(comData.Name, comData.Id, '');
            
            //Mashups_CC.getMashupRecordNew(resData.Name, resData.Id);
            Mashups_CC.mashupUrl(data, false, false);
            Mashups_CC.mashupUrl(comData, false, true);
            //Mashups_CC.mashupUrl(resData, true, false);
            Mashups_CC.getMashupRecord(data.Name);
            Mashups_CC.getMashupRecord(comData.Name);
            Mashups_CC.getMashupRecord(resData.Name);
            Mashups_CC.getObjectNameById(String.valueOf(data.Id));
            Mashups_CC.getObjectRecord('Mashup_Management__c',String.valueOf(data.Id), 'Name' );
            System.assert(mashupManList != null);
        test.stopTest();        
    }

    static testMethod void testMashupsMethod2() {
        System.runAs(comUser) {
            init();
            test.startTest();

            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
            String mashups = Mashups_CC.getRecordsByGroup(accList[0].Id, 'Hybris');

            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order -- Commercial', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                             Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                              Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Commercial');

            insert data;

            data = new Mashup_Management__c( Name = 'Create Product Order -- Residential', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                             Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                              Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Residential');
            insert data;
            system.assert(data.Id!=null);

            String url = Mashups_CC.getMashupRecordNew('Create Product Order -- Residential', '', '');
            System.debug('url' + url);

            test.stopTest();
        }
    }

    static testMethod void testMashupsMethod3() {
        System.runAs(resUser) {
            init();
            test.startTest();

            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);

            SObject accountRecord = Mashups_CC.getRecordById(accList[0].Id);

            String mashups = Mashups_CC.getRecordsByGroup(accList[0].Id, 'Hybris');

            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order -- Commercial', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                             Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                              Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Commercial');

            insert data;

            data = new Mashup_Management__c( Name = 'Create Product Order -- Residential', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                             Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                              Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Residential');
            insert data;
            system.assert(data.Id!=null);

            String url = Mashups_CC.getMashupRecordNew('Create Product Order -- Commercial', '', '');
            System.debug('url' + url);

            test.stopTest();
        }
    }
 static testMethod void testMashupsMethod4() {
        System.runAs(resUser) {
            init();
            test.startTest();

            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);

            SObject accountRecord = Mashups_CC.getRecordById(accList[0].Id);

            String mashups = Mashups_CC.getRecordsByGroup(accList[0].Id, 'Hybris');

            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order -- Commercial', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                             Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                              Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Commercial');

            insert data;

            data = new Mashup_Management__c( Name = 'Create Product Order -- Residential', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                             Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                              Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Residential');
            insert data;

            String url = Mashups_CC.getMashupRecordNew('Create Product Order -- Commercial', '', '');
            System.debug('url' + url);
            Mashups_CC.getAccountProfileId(accList[0].Id);
            Boolean B=Mashups_CC.customerPriceListProfileBasedUrl(UtilityCls.RES_SALES_USER);
            system.assert(Mashups_CC.customerPriceListProfileBasedUrl(UtilityCls.RES_SALES_USER));
            
            List<Account> invAccList = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            invAccList[0].Global_Account_Number__c = '0000012345';
            invAccList[0].Customer_Group_Number__c = '900074';
            update invAccList;
            List<Product2> prodList = Utility_Test.createProductwithRTId(true, 1, 'Residential');
            List<Product_Customer_Group_Relationship__c> prodCusGrpRelList = Utility_Test.createProdCusGrpRelationship(true,1);
			prodCusGrpRelList[0].Product__c = prodList[0].Id;
            update prodCusGrpRelList;
            data = new Mashup_Management__c(Name = 'Price Grid', 
                                            Target_URL__c='https://botstmohawkexchange.mohawkind.com/mhkflooringstorefront/mashup/pdp/SALESMANACCOUNT/HYBRIS_ID',
                                            Query_String__c = 'HYBRIS_ID=Hybris_Product_Code__c', 
                                            Object__c='General', Group__c='General', Sort_Order__c = 1, No_Query_String__c = true,
                                            Is_Publisher_Action__c = false, 
                                            Type__c = 'Hybris', Is_Active__c = true, Grid__c = true,
                                            Display_Name__c = 'Price Grid', Profile__c = 'Residential');
            insert data;
            
            url = Mashups_CC.getMashupRecordNew('Price Grid', '', prodList[0].Id);
            //System.assertEquals(1, url);
            data = new Mashup_Management__c(Name = 'CPL Grid', 
                                            Target_URL__c='https://botstmohawkexchange.mohawkind.com/mhkflooringstorefront/mashup/pdp/GLOBAL_ACCOUNT_NUMBER/HYBRIS_ID',
                                            Query_String__c = 'GLOBAL_ACCOUNT_NUMBER=Global_Account_Number__c;;HYBRIS_ID=Hybris_Product_Code__c', 
                                            Object__c='Account', Group__c='General', Sort_Order__c = 1, No_Query_String__c = true,
                                            Is_Publisher_Action__c = false, 
                                            Type__c = 'Hybris', Is_Active__c = true, Grid__c = true,
                                            Display_Name__c = 'CPL Grid', Profile__c = 'Residential');
            insert data;
            url = Mashups_CC.getMashupRecordNew('Price Grid', invAccList[0].Id, prodList[0].Id);
            
            url = Mashups_CC.getMashupRecordNew('Price Grid', '', null);
            test.stopTest();
        }
    }
    
    //Added by Susmitha on March 26th
    static testMethod void testMashupsMethod5() {
        System.runAs(resUser) {
            init();
            test.startTest();

            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);


            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                             Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                              Is_Publisher_Action__c = false,Dropdown__c='Account Overview', Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Residential');
            insert data;

            Mashups_CC.getMashupRecordDisplayNames(accList[0].Id);
            system.assert(Mashups_CC.getMashupRecordDisplayNames(accList[0].Id)!=null);
            test.stopTest();
        }
    }
    
}