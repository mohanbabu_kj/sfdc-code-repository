/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/guide/*')
global class MAMD_RestResource {
    global MAMD_RestResource() {

    }
    @HttpGet
    global static void processGet() {

    }
    @HttpPost
    global static void processPost() {

    }
global class PostRequest {
    global mamd.MAMD_RestResource.ErrorClass error;
    global String jobId;
    global mamd.MAMD_RoutingUtilitiesMARE.NextRequestData requestData;
    global List<mamd.MAMD_RestResource.RouteData> routes;
    global PostRequest() {

    }
}
global class PostResponse {
    global String error;
    global String jobId;
    global mamd.MAMD_RoutingUtilitiesMARE.NextRequestData nextRequest;
    global mamd.MAMD_RestResource.PostResponseData responseData;
    global Boolean success;
    global PostResponse() {

    }
    global PostResponse(mamd.MAMD_RestResource.PostRequest input, String pError) {

    }
}
}
