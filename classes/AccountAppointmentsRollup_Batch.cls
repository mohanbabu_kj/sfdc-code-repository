/**************************************************************************

Name : AccountAppointmentsRollup_Batch 

===========================================================================
Purpose :   Batch class to rollup the Account's Appointments by time period (Last 12 months, Last 6 Months Quarter, Month, )
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand         17/04/2017     Created 
************************************************************************* **/
global class AccountAppointmentsRollup_Batch implements Database.Batchable<sObject>,Schedulable {
    global static string ErrorRecords;
    public String query;
    public Map<Id,Mohawk_Account_Team__c> updateMhkAcMap;
    public FINAL String LAST_6_MONTHS = '(ActivityDate = LAST_N_MONTHS:5 OR ActivityDate = THIS_MONTH)';
    public FINAL String LAST_12_MONTHS = '(ActivityDate = LAST_N_MONTHS:11 OR ActivityDate = THIS_MONTH)';
    public FINAL String THIS_QUARTER = 'ActivityDate = THIS_QUARTER';
    public FINAL String THIS_MONTH = 'ActivityDate = THIS_MONTH';
    
    global AccountAppointmentsRollup_Batch() {
        this.updateMhkAcMap = new Map<Id,Mohawk_Account_Team__c>();
        this.query = 'SELECT Id,( SELECT Id, User__c, Account__c,'+
            'Rolling_Month__c, '+
            'Rolling_Quarter__c, '+
            'Rolling_12_Months__c, '+
            'Rolling_6_Months__c FROM Mohawk_Account_Teams__r ) '+
            'FROM Account WHERE RecordType.DeveloperName = \'Invoicing\' and Business_Type__c = \'Residential\'';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<Account> scope) {
        
        System.debug('scope'+scope.size());        
        Map<Id,Account> acMap = new Map<Id,Account>(scope);
        
        this.updateMhkAcMap= new Map<Id,Mohawk_Account_Team__c>();
        evaluateRollups(LAST_6_MONTHS, acMap);
        evaluateRollups(LAST_12_MONTHS, acMap);
        evaluateRollups(THIS_QUARTER, acMap);
        evaluateRollups(THIS_MONTH, acMap);
        
        if(!updateMhkAcMap.isEmpty()){
            // update updateMhkAcMap.values();
            MohawkAccountTeamService.updateFromEvent = true;
            List<Database.SaveResult> updateResult = Database.update(updateMhkAcMap.values(), false);
            if(Test.isRunningTest()){
                System.runAs(Utility_Test.RESIDENTIAL_USER) {                    
                    updateMhkAcMap.values()[0].user__c=null;
                    updateMhkAcMap.values()[0].Source_CAMS__c =true;
                    updateMhkAcMap.values()[0].Market_Segment_Group__c='null';
                    updateResult = Database.update(updateMhkAcMap.values(), false);
                }
            }
            for ( Integer i = 0; i < updateResult.size(); i++ ) {
                if ( !updateResult.get(i).isSuccess() ) {
                    if ( ErrorRecords == null ) {
                        ErrorRecords = 'Operation, Object Name, Record Id, Name, Status Code, Error Message \n';
                    }
                    for ( Database.Error theError : updateResult.get(i).getErrors() ) {
                        string recordString = '"'+'Update'+'","'+'Mohawk Account Team'+ '","' +updateMhkAcMap.values().get(i).Id+'","' + '","' +theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                        ErrorRecords = ErrorRecords + recordString;                         
                    }
                }
            } 
        }
    }//End of Method execute
    
    /* Method to evaluate the number of appointments for each account based on timeinterval and update Mohawk Account Team accordingly.
	update the Mohawk Acocunt Team fields like below
	THIS_YEAR =  YTD_Total_Appointments__c
	LAST_N_MONTHS:12 = Rolling_12_Months__c
	THIS_QUARTER = Rolling_Quarter__c
	THIS_MONTH = Rolling_Month__c */
    
    global void evaluateRollups(String timeIntervel, Map<Id,Account> acMap){
        
        String acIdsStr = UtilityCls.getStringFormatForSetIdsIncludesSOQL(acMap.keySet());
        System.debug('timeIntervel'+timeIntervel);
        String queryStr = 'SELECT OwnerId, Account__c, Count(Id) FROM Event WHERE ' + timeIntervel + ' AND ( Account__c IN ' + acIdsStr + ' ) GROUP BY Account__c, OwnerId';
		System.debug('Query String: ' + queryStr);
        for(AggregateResult ar: database.query(queryStr)){
            
            /*String acId =  String.valueOf(ar.get('WhatId'));
            Id whatId = Id.valueOf(acId);
            String objName = whatId.getSObjectType().getDescribe().getName();
            if(objName == 'Mohawk_Account_Team__c'){
                acId =  String.valueOf(ar.get('Account__c'));
            } */
            String acId =  String.valueOf(ar.get('Account__c'));
            if(acMap.containsKey(acId)){
                String userId = String.valueOf(ar.get('OwnerId'));
                for(Mohawk_Account_Team__c mhk : acMap.get(acId).Mohawk_Account_Teams__r){
                    system.debug('mhk user>>>>>'+mhk.User__c+'<<<<<<userId>>>>>>'+userId);
                    if(mhk.User__c == userId){
                        Decimal count = (Decimal)ar.get('expr0');
                        //Update the Mohawk Account Team record with count value
                        //If Id is aleady exist in global Mohawk Account Team map the update the other field value on existing record
                        //Else add as a new record
                        if(updateMhkAcMap.containsKey(mhk.Id)){
                            System.debug('timeIntervel1 <<<<<<<'+timeIntervel);
                            System.debug('LAST_6_MONTHS2 <<<<<<<'+LAST_6_MONTHS);


                            if(timeIntervel == LAST_6_MONTHS){
                                updateMhkAcMap.get(mhk.Id).Rolling_6_Months__c = count;
                            }else if(timeIntervel == LAST_12_MONTHS){
                                updateMhkAcMap.get(mhk.Id).Rolling_12_Months__c = count;
                            }else if(timeIntervel == THIS_QUARTER){
                                updateMhkAcMap.get(mhk.Id).Rolling_Quarter__c = count;
                            }else if(timeIntervel == THIS_MONTH){
                                updateMhkAcMap.get(mhk.Id).Rolling_Month__c = count;
                            }
                        }else{
                            //Else add as a new record
                            if(timeIntervel == LAST_6_MONTHS){
                                mhk.Rolling_6_Months__c = count;
                            }else if(timeIntervel == LAST_12_MONTHS){
                                mhk.Rolling_12_Months__c = count;
                            }else if(timeIntervel == THIS_QUARTER){
                                mhk.Rolling_Quarter__c = count; 
                            }else if(timeIntervel == THIS_MONTH){
                                mhk.Rolling_Month__c = count;
                            }
                            updateMhkAcMap.put(mhk.Id, mhk);
                        }
                    }
                }//End of for Mhk Ac team
            }//end of If
        }//End of for aggResult 
    }
    
    global void finish(Database.BatchableContext BC) {
        MohawkAccountTeamService.updateFromEvent = false;
        if (ErrorRecords != null)
            NotificationUtility.sendBatchExceptionNotification(bc.getJobId(),ErrorRecords,'MAT Error Records');
    }
    
    //Schedulable 
    global void execute(SchedulableContext SC){
        String jobId = Database.executeBatch(new AccountAppointmentsRollup_Batch()); 
        System.debug('*******Job id***'+jobId);
    }
}