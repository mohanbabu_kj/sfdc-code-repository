/**************************************************************************

Name : customLookUpControllerTEST

===========================================================================
Purpose : This class is used for customLookUpController
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
2.0       Susmitha       12/March/2018     Modified
***************************************************************************/
@isTest
private class customLookUpControllerTEST{
    
    static List<Account> accountList;
    static List<Account> accountListInsert;
    static testMethod void testMethod1() {
        String AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-Invoicing').getRecordTypeId();   

        accountList = Utility_Test.createAccounts(false,3);
        accountListInsert = new List<Account>();
        for( Account acc : accountList ){
            acc.Role_C__c = 'End User';
            acc.RecordtypeId=AccRecordTypeId;
            accountListInsert.add( acc );
        }
        insert accountListInsert;
        system.assert(accountListInsert.size()>0);
        customLookUpController.fetchLookUpValues('Test Account','Account');
    }
    
}