/**************************************************************************

Name : AccountAppointmentsRollup_Batch_Test
===========================================================================
Purpose : Uitlity class to AccountAppointmentsRollup_Batch test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         28/April/2017    Create  
***************************************************************************/
@isTest
private class AccountAppointmentsRollup_Batch_Test {
    static List<Account> resAccForInvList;
    static List<Territory__c> terrList;
    static List<Mohawk_Account_Team__c>  mohAccTeamList;
    static Mohawk_Account_Team__c mohAccTeam;
    static List<Event> eventList; 
    static List<Account> scope;
    
    @TestSetup
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        //  resAccForInvList = Utility_Test.createResidentialAccountsForInvoicing(true, 3);
        //terrList = Utility_Test.createTerritories(true, 3);  
        
        //  mohAccTeamList = Utility_Test.createMohawkAccountTeam(true, 3, resAccForInvList, terrList, Utility_Test.ADMIN_USER);
        // eventList = Utility_Test.createEvents(true, 3, resAccForInvList);
        
        
        
    }
    
    static testMethod void testMethod1() {
        
        //System.runAs(Utility_Test.ADMIN_USER) {
        resAccForInvList = Utility_Test.createResidentialAccountsForInvoicing(true, 5);
        terrList = Utility_Test.createTerritories(true, 5);
        
        mohAccTeamList = Utility_Test.createMohawkAccountTeam(true, 5, resAccForInvList, terrList, Utility_Test.ADMIN_USER);
        
        eventList = Utility_Test.createEvents(false, 5, resAccForInvList);
        List<Event> eventList = Utility_Test.createEvents(false, 5, resAccForInvList);
        
        Account accRec = new Account(name='testName', Ownerid = Utility_Test.ADMIN_USER.id,Business_Type__c = 'Residential');
        List<Account> AccountList=new list<Account>();
        AccountList.add(accRec);
        insert accRec ;
        integer i = 0;
        
        for(Event e :eventList){
            i = i - 2;
            e.ownerId=Utility_Test.ADMIN_USER.id;
            e.ActivityDateTime = datetime.now().addMonths(i);
            system.debug('Even ActivityDate - ' + e.ActivityDate );
        }
        
        insert eventList;
        system.assert(eventList.size()==5);
        Mohawk_Account_Team__c  mhk= new Mohawk_Account_Team__c(Account__c=accRec.id,User__c=Utility_Test.ADMIN_USER.id);
        system.debug('Utility_Test.ADMIN_USER.id >>>>>>>>>>'+Utility_Test.ADMIN_USER.id);
        insert mhk;
        
        
        // eventList=utility_test.createEvents(true,1,AccountList);
        
        Test.startTest();
        
        AccountAppointmentsRollup_Batch rollupbatch = new AccountAppointmentsRollup_Batch();
        
        String result=Database.executeBatch(rollupbatch);
        
        System.assert(result!=null);  
        Test.stopTest();
        // } 
    }
    public static testmethod void main(){
        Test.startTest();
        AccountAppointmentsRollup_Batch sh1 = new  AccountAppointmentsRollup_Batch();
        String sch = '0 0 2 * * ?'; 
        system.assert(sch == '0 0 2 * * ?');
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();
        
    }
    
    
}