/**************************************************************************

Name : SalesInfo_CC_Test

===========================================================================
Purpose :   This class is used for SalesInfo_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit        27/September/2017     Created 
2.0        Krishnapriya   6/Nov/2017     Created         CSR: 1
3.0        Susmitha     29/Dec/2017      Modified
***************************************************************************/
@isTest
private class SalesInfo_CC_Test{
    
    
    
    static testMethod void testMethod2() {
        List<Territory_User__c>  terrUser;
        List<user>testUser = Utility_Test.createTestUsers( false , 2,  'Commercial Sales Manager' );
        testUser[0].MHKUserRole__c = 'Senior Vice President';
        testUser[1].MHKUserRole__c ='Senior Vice President';
        
        insert testUser;
        
        System.runAs( testUser[0] ) {
            SalesInfo_CC.getSalesInfo( Utility_Test.createAccounts( true,1 )[0].id );
            SalesInfo_CC.getUserSalesInfo( testUser[0].Id);
            List<Account>accList = Utility_Test.createAccounts( false,1 );
            List<Territory__c>terrList = Utility_Test.createTerritories( true, 1 );
            List<Territory_User__c> territoryUserList = new List<Territory_User__c>();
            territoryUserList.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Salesperson', User__c = testUser[0].Id)); 
            
            territoryUserList[0].Territory__c = terrList[0].Id;
            territoryUserList[0].Role__c = UtilityCls.AE;
            territoryUserList[0].user__c=testUser[0].Id;
            territoryUserList[0].Segments__c = 'Corporate';
            territoryUserList[0].Last_Lead_Received_Time__c = System.Now();
            territoryUserList[0].Millisecond__c = 1;
            insert territoryUserList;
            system.assert(territoryUserList.size()>0);
            for( Integer i=0; i < accList.size(); i++ ){                
                accList[i].Territory__c = terrList[i].Id;
                accList[i].Workplace_Retail_Territory__c=terrList[i].id;
            }
            try{
                insert accList;
                SalesInfo_CC.getSalesInfo( accList[0].id );
            }
            catch(exception e){  
                //system.debug('error e'+e);
            }
        }
        System.runAs( testUser[1] ) {
            SalesInfo_CC.getUserSalesInfo( testUser[0].id );
        }
    }
    static testMethod void testMethod1() {
        List<Territory_User__c>  terrUser;
        SalesInfo_CC.getSalesInfo( Utility_Test.createAccounts( true,1 )[0].id );
        SalesInfo_CC.getUserSalesInfo( Utility_Test.RESIDENTIAL_USER.id );
        List<user>testUser = Utility_Test.createTestUsers( false , 2,  'Commercial Sales Manager' );
        testUser[0].MHKUserRole__c = UtilityCls.AE;
        testUser[1].MHKUserRole__c = 'Senior Vice President';
        insert testUser;
        system.assert(testUser.size()>0);
        
        System.runAs( testUser[0] ) {           
            List<Account>accList = Utility_Test.createAccounts( false,1 );
            List<Territory__c>terrList = Utility_Test.createTerritories( true, 1 );
            /*Added by Susmitha*/
            List<Territory_User__c> territoryUserList = new List<Territory_User__c>();
            
            territoryUserList.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Salesperson', User__c = testUser[0].Id)); 
            
            territoryUserList[0].Territory__c = terrList[0].Id;
            territoryUserList[0].Role__c = UtilityCls.AE;
            territoryUserList[0].user__c=testUser[0].Id;
            territoryUserList[0].Segments__c = 'Corporate';
            territoryUserList[0].Last_Lead_Received_Time__c = System.Now();
            territoryUserList[0].Millisecond__c = 1;
            
            insert territoryUserList;
            system.assert(territoryUserList.size()>0);
            
            /*End Added by Susmitha*/
            
            for( Integer i=0; i < accList.size(); i++ ){                
                accList[i].Territory__c = terrList[i].Id;
                accList[i].Workplace_Retail_Territory__c=terrList[i].id;
            }
            insert accList;
            SalesInfo_CC.getSalesInfo( accList[0].id );
            SalesInfo_CC.WrapSalesInfo WrapSalesInfoclass=new SalesInfo_CC.WrapSalesInfo(accList[0],true);
            
        }
        System.runAs( testUser[1] ) {
            SalesInfo_CC.getUserSalesInfo( testUser[0].id );
        }
    }
    
    static testMethod void testMethod3() {
        List<Territory_User__c>  terrUser;
        
        SalesInfo_CC.getSalesInfo( Utility_Test.createAccounts( true,1 )[0].id );
        SalesInfo_CC.getUserSalesInfo( Utility_Test.RESIDENTIAL_USER.id );
        
        
        List<user>testUser = Utility_Test.createTestUsers( false , 2,  'Commercial Sales Manager' );
        testUser[0].MHKUserRole__c = UtilityCls.AE;
        testUser[1].MHKUserRole__c = UtilityCls.AE;
        
        insert testUser;
        system.assert(testUser.size()>0);
        
        System.runAs( testUser[0] ) {           
            List<Account>accList = Utility_Test.createAccounts( false,1 );
            List<Territory__c>terrList = Utility_Test.createTerritories( true, 1 );
            
            /*Added by Susmitha*/
            List<Territory_User__c> territoryUserList = new List<Territory_User__c>();
            
            
            territoryUserList.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Salesperson', User__c = testUser[0].Id)); 
            
            territoryUserList[0].Territory__c = terrList[0].Id;
            territoryUserList[0].Role__c = UtilityCls.AE;
            territoryUserList[0].user__c=testUser[0].Id;
            territoryUserList[0].Segments__c = 'Corporate';
            territoryUserList[0].Last_Lead_Received_Time__c = System.Now();
            territoryUserList[0].Millisecond__c = 1;
            
            insert territoryUserList;
            system.assert(territoryUserList.size()>0);
            
            /*End Added by Susmitha J*/
            
            for( Integer i=0; i < accList.size(); i++ ){                
                accList[i].Territory__c = terrList[i].Id;
                accList[i].Workplace_Retail_Territory__c=terrList[i].id;
            }
            insert accList;
            SalesInfo_CC.getSalesInfo( accList[0].id );
        }
        System.runAs( testUser[1] ) {
            SalesInfo_CC.getUserSalesInfo( testUser[0].id );
        }
    }
    
    
    static testMethod void testMethod4() {
        //user testUser = [SELECT Id,Email, ProfileId FROM User WHERE Profile.Name =: UtilityCls.RES_SALES_USER AND IsActive=true LIMIT 1 ];                
        List<user>testUser = Utility_Test.createTestUsers( true , 1,  UtilityCls.RES_SALES_USER );
        system.assert(testUser.size()>0);
        
        System.runAs( testUser[0] ) {
            SalesInfo_CC.getSalesInfo( Utility_Test.createAccounts( true,1 )[0].id );
        }
    }
    
    static testMethod void testMethod5() {
        //user testUser = [SELECT Id,Email, ProfileId FROM User WHERE Profile.Name =: UtilityCls.INTEGRATION_USER AND IsActive=true LIMIT 1 ];                
        List<user>testUser = Utility_Test.createTestUsers( true , 1,  UtilityCls.INTEGRATION_USER );
        system.assert(testUser.size()>0);
        
        System.runAs( testUser[0] ) {
            SalesInfo_CC.getSalesInfo( Utility_Test.createAccounts( true,1 )[0].id );
        }
    }
    
    //ADDED BY MUDIT
    static testMethod void testMethod6(){
        SalesInfo_CC.getPicklistValues();
        //user testUser = [SELECT Id,Email, ProfileId FROM User WHERE Profile.Name =: UtilityCls.INTEGRATION_USER AND IsActive=true LIMIT 1 ];                
        List<user>testUser = Utility_Test.createTestUsers( true , 1,  UtilityCls.INTEGRATION_USER );
        system.assert(testUser.size()>0);
        
        System.runAs( testUser[0] ) {
            SalesInfo_CC.setUserSalesInfo( testUser[0] );
            System.assertEquals(SalesInfo_CC.setUserSalesInfo( testUser[0] ), 'Success');
        }
        testUser[0].email = '';
        SalesInfo_CC.setUserSalesInfo( testUser[0] );
        System.assertEquals(SalesInfo_CC.setUserSalesInfo( testUser[0] ), 'Error Occurred while saving. Please contact Administrator');
        SalesInfo_CC.setUserSalesInfo( null );
        System.assertEquals(SalesInfo_CC.setUserSalesInfo( null ), 'Something went wrong. Please try again later. If issue continues, please contact Administrator');
    }
    
}