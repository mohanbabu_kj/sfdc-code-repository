/**************************************************************************

Name : AccountPlansController_CC
===========================================================================
Purpose : This class is used for the lightning components which will search the Account Plans Based on account and fiscal year.
===========================================================================
History
--------
VERSION      AUTHOR              DATE           DETAIL          DESCRIPTION
1.0        Lakshman Jaasti      25/Oct/2017    Created                             
***************************************************************************/

public with sharing class AccountPlansController_CC 
{
    //Method to get account Plans based on account and fiscal year.
    @AuraEnabled
    public static List<My_Account_Plan__c > getAccountPlans(String accId , String fYear) 
    {
        system.debug('accId :'+accId +' 1212TEst: '+UtilityCls.isStrNotNull(accId));
        if(!UtilityCls.isStrNotNull(accId))
        {
            return null;
        }
        String query = 'SELECT Id, Name, CreatedDate, Fiscal_Year__c FROM My_Account_Plan__c where Account__c =:accId ';
        if(fYear != 'All')
        {
            query += ' and Fiscal_Year__c  =: fYear';
        }
        try{
            system.debug('query :'+query);
            List<My_Account_Plan__c > li = Database.query(query);
            system.debug('li : '+li);
            return li;
        }catch(Exception ex){
            system.debug('ex :'+ex.getMessage());
            return null;
        }
    }
    
    //Method to get Picklist values for the Fiscal Year filter.
    @AuraEnabled
    public static List<String> getFiscalPicklistValues() 
    {
        List<String> PlStpickvalues = UtilityCls.getPicklistValues('My_Account_Plan__c','Fiscal_Year__c');
        return PlStpickvalues;
    }
    
    //Method to check weather logged in user is MohawkTeamMember for the perticular account.
    //if not return null otherwise return Record. 
    @AuraEnabled
    public static AccountTeamMemberObj getMohawkTeamMember(String accountId) 
    {
        Mohawk_Account_Team__c  mAccTeam;
        AccountTeamMemberObj accWapper = new AccountTeamMemberObj();
        try
        {
            mAccTeam = [Select Id,Account__c from Mohawk_Account_Team__c 
                        where Account__c =:accountId 
                        and User__r.Name =: userinfo.getName() limit 1];
        }catch(System.QueryException e)
        {
            System.debug('Error : '+e.getMessage());
            mAccTeam =null;
        }
        accWapper.acc = getAccount(accountId);
        accWapper.usr = getUser(userinfo.getUserId()); //Added by MB - Bug 57614 - to get User Name - 4/4/18
        if(mAccTeam != null)
            accWapper.mhkTeam = mAccTeam;
        else
            accWapper.mhkTeam = null;
        System.debug('mAccTeam : '+ mAccTeam);
        return accWapper;
    }
    
    //To get the account Record to display name of the account on the component.
    public static Account getAccount(String accountId) 
    {
        try{
            return [Select Id,Name from Account where id =:accountId limit 1];
        }catch(Exception ex){
            return null;
        }
        
    }
    
    //Added by MB - Bug 57614 - to get User Name - 4/4/18
    //To get the user Record to display name of the user on the component.
    public static User getUser(String userId) 
    {
        try{
            return [Select Id,FirstName, LastName from User where id =:userId limit 1];
        }catch(Exception ex){
            return null;
        } 
    }
    
    //Wrapper class to return the Account and MohawkTeamMember.
    public class AccountTeamMemberObj {
        @AuraEnabled
        public Account acc;
        @AuraEnabled
        public Mohawk_Account_Team__c mhkTeam;
        //Added by MB - Bug 57614 - to get User Name - 4/4/18
        @AuraEnabled
        public User usr;
    }
    
}