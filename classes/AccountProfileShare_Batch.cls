/**************************************************************************

Name : AccountProfileShare_Batch 

===========================================================================
Purpose : Batch class to Share account profile record with it's accounts mohawk account team members
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Anand       26/April/2017        Created          CSR:

***************************************************************************/
global class AccountProfileShare_Batch implements Database.Batchable<sObject>,Schedulable {
    global static string ErrorRecords;
	
    //public String query = 'SELECT Id, territory__c, Account__r.Account_Profile__c, User__c, Account__r.Account_Profile__r.ownerId ' +
    //                      'FROM Mohawk_Account_Team__c WHERE Account__r.Account_Profile__c != null AND User__c != null'; // Added User__c != null - MB - 11/14/17
                                        
                                         
    /*global AccountProfileShare_Batch() {
        this.query = query;
    }*/

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'AccountProfileShare_Batch'];
        If(QureyList.size() > 0)
            query = QureyList[0].Query_String__c ;
        else
            query = 'SELECT Id, territory__c, Account__r.Account_Profile__c, User__c, Account__r.Account_Profile__r.ownerId ' +
            'FROM Mohawk_Account_Team__c WHERE (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND Account__r.Account_Profile__c != null AND User__c != null';
            
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Mohawk_Account_Team__c> scope) {
        
        List<Account_Profile__Share> acProShareList = new List<Account_Profile__Share>();
        Set<Id> acProIds =  new Set<Id>();
        Set<Id> territoryIds =  new Set<Id>();
        Map<Id,List<Id>> terUserIdsMap = new Map<Id,List<Id>>();
        Default_Configuration__c  cs = Default_Configuration__c.getOrgDefaults();   
        List<String> resiRolesList = new List<String>();

        //Getting Territory user roles from custom setting 
        if(UtilityCls.isStrNotNull(cs.Residential_Invoicing_Roles__c)){
            resiRolesList.addAll(cs.Residential_Invoicing_Roles__c.split(','));
        }
        
        //Method to initialize the Account Profile Share records
        for(Mohawk_Account_Team__c mhk : scope){    
                                         
            territoryIds.add(mhk.Territory__c);

        }
        //Initialize the Map with Territory Id with list of users with roles  DM, SVP, RVP 
        for(Territory_User__c  tUser : [SELECT Id, territory__c,User__c FROM Territory_User__c 
                                        WHERE territory__c in : territoryIds
                                        AND Role__c in: resiRolesList]){
            
            if(terUserIdsMap.containsKey(tUser.territory__c)){
                 terUserIdsMap.get(tUser.territory__c).add(tUser.User__c);
            }else{
                List<Id> uIdList = new List<Id>();
                uIdList.add(tUser.User__c);
                terUserIdsMap.put(tUser.territory__c,uIdList);
            }
        }
        
        Set<Id> terAleadyProcessIds = new Set<Id>();
        //Method to initialize the Account Profile Share records
        for(Mohawk_Account_Team__c mhk : scope){                                         

          if( mhk.Account__r.Account_Profile__c != null && mhk.User__c != mhk.Account__r.Account_Profile__r.ownerId){

              Account_Profile__Share  apShare =  new Account_Profile__Share();
              apShare.Parentid      = mhk.Account__r.Account_Profile__c;
              apShare.AccessLevel   = 'Edit';
              apShare.UserorGroupId = mhk.User__c;
              acProShareList.add(apShare); 
          }
            // Avoid adding DM, SVP, RVP roles second time
            // This if condition needed if 2 or more mhk team members having same territory 
            if(mhk.Territory__c != null && !terAleadyProcessIds.contains(mhk.Territory__c)){ 

              if(terUserIdsMap.get(mhk.Territory__c) != null){

                    //Share account profile to  current Mohawk Team Territory DM, SVP, RVP 
                    for(Id  higherUserId : terUserIdsMap.get(mhk.Territory__c)){
                        Account_Profile__Share  apShare2 =  new Account_Profile__Share();
                        apShare2.Parentid      =  mhk.Account__r.Account_Profile__c;
                        apShare2.AccessLevel   = 'Edit';
                        apShare2.UserorGroupId = higherUserId;
                        acProShareList.add(apShare2); 
                    }

              }

                terAleadyProcessIds.add(mhk.Territory__c);
            }//End of the 


        }
        system.debug('************'+acProShareList);
          List<Database.UpsertResult> updateResult = Database.upsert(acProShareList, false);

         //Perform upsert to avoid inserting duplicate records
        // database.upsert(acProShareList);
          for ( Integer i = 0; i < updateResult.size(); i++ ) {
                    if ( !updateResult.get(i).isSuccess() ) {
                        if ( ErrorRecords == null ) {
                            ErrorRecords = 'Operation, Object Name, Record Id, Name, Status Code, Error Message \n';
                        }
                        for ( Database.Error theError : updateResult.get(i).getErrors() ) {
                            string recordString = '"'+'Upsert'+'","'+'Account Profile Share'+ '","' +acProShareList.get(i).Id+'","' + '","' +theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                            ErrorRecords = ErrorRecords + recordString;                         
                        }
                    }
                } 
        
    }

    global void finish(Database.BatchableContext BC) {
          if (ErrorRecords != null)
            NotificationUtility.sendBatchExceptionNotification(bc.getJobId(),ErrorRecords,'Account Profile share Error Records');
            
    }

    global void execute(SchedulableContext SC){
        database.executeBatch(new AccountProfileShare_Batch());
    }
}