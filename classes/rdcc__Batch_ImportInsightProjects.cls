/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Batch_ImportInsightProjects implements Database.AllowsCallouts, Database.Batchable<String>, Database.Stateful {
    global rdcc__Configurable_Settings__c config;
    global String idsType;
    global List<String> projectIds;
    global Set<Id> projIdSet;
    global rdcc__Search_Name__c searchId;
    global Id userId;
    global void execute(Database.BatchableContext info, List<String> scope) {

    }
    global void finish(Database.BatchableContext info) {

    }
    global System.Iterable start(Database.BatchableContext info) {
        return null;
    }
}
