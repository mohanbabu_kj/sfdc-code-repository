@isTest
private class CustomActivityTriggerHandler_Test {
    static List<Account> accountList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Territory__c> terrList;
    static List<Custom_Activities__c> caList = new List<Custom_Activities__c>();
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    @testSetup
    static void init(){   
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        AP_Trigger_Settings__c APSC1 = new AP_Trigger_Settings__c();
        APSC1.Name = 'Chain Number Exclude';
        APSC1.Exclude_Chain_Numbers_1__c = '10';
        APSC1.Exclude_Chain_Numbers_2__c = '13';
        insert APSC1;
        
        accountList = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        Id resTerrUserRecTypeId = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
        List<Territory_User__c> terrUserList = new List<Territory_User__c>();
        List<User>  userList = Utility_Test.createTestUsers(false, 5, 'Residential Sales User');
        insert userList;
        terrList = new List<Territory__c>();
        Territory__c terr = new Territory__c();
        terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
        terr.Territory_Code__c = '991';
        terr.Region_Code__c = '991';
        terr.Sector_Code__c = '991';
        terr.Name = 'District 991';
        terrList.add(terr);
        terr = new Territory__c();
        terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
        terr.Territory_Code__c = '992';
        terr.Region_Code__c = '992';
        terr.Sector_Code__c = '992';
        terr.Name = 'District 992';
        terrList.add(terr);
        insert terrList;
        
        Mohawk_Account_Team__c mhkTeam = new Mohawk_Account_Team__c();
        mhkTeam.RecordTypeId = resiInvRTId;
        mhkTeam.Account__c = accountList[0].Id;
        mhkTeam.User__c = userList[0].Id;
        mhkTeam.Role__c = 'Territory Manager';
		insert mhkTeam;
        //mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, accountList, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
        //mohAccTeam[0].Account__c = accountList[0].Id;
        /*for( Mohawk_Account_Team__c mat:mohAccTeam ){
mat.Account__c = accountList[0].id;
//mat.Primary_Profile_Business__c = apSetting[0].id;
mat.Actual_Mohawk_Purchases_Carpet__c =140;
mat.Green_Opportunity_Carpet__c =100;

}*/
        //insert mohAccTeam;
        terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-I8L', 'Territory Manager', userList[0].Id));
        terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[1].Id, resTerrUserRecTypeId, '05-K8L', 'Territory Manager', userList[0].Id));
        terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-K8L', 'District Manager', userList[4].Id));
        terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-H8L', 'Business Development Manager', userList[2].Id));
        terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-M8L', 'Regional Vice President', userList[3].Id));
        insert terrUserList; 
        
        //test.startTest();
        
        Custom_Activities__c ca = new Custom_Activities__c(
            WhatId__c = mhkTeam.Id,
            Account__c = accountList[0].id,
            Assigned_To__c = userList[0].Id
        );
        caList.add(ca);
        ca = new Custom_Activities__c(
            WhatId__c = accountList[0].id,
            Account__c = accountList[0].id,
            Assigned_To__c = userList[0].Id,
            Subject__c = 'Test Appointment'
        );
        caList.add(ca);
        insert caList;
    } 
    
    /*static testMethod void testMethod1() {
init();
accountList = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
terrList = Utility_Test.createTerritories(true, 2);

mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, accountList, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
for( Mohawk_Account_Team__c mat:mohAccTeam ){
mat.Account__c = accountList[0].id;
//mat.Primary_Profile_Business__c = apSetting[0].id;
mat.Actual_Mohawk_Purchases_Carpet__c =140;
mat.Green_Opportunity_Carpet__c =100;
mat.Brands__c = 'Aladdin;Horizon;Karastan;Aladdin Commercial;Portico;Properties;Hard Surface';
mat.Product_Types__c = 'Carpet;Cushion;Resilient;Hardwood;Laminate;Tile;';                
}
insert mohAccTeam;

Custom_Activities__c CA = new Custom_Activities__c(
WhatId__c = accountList[0].id,
Account__c = accountList[0].id,
Assigned_To__c = Utility_Test.ADMIN_USER.Id
);
insert CA;
}   

static testMethod void testMethod2() {
init();
accountList = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
terrList = Utility_Test.createTerritories(true, 2);

mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, accountList, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
for( Mohawk_Account_Team__c mat:mohAccTeam ){
mat.Account__c = accountList[0].id;
mat.Actual_Mohawk_Purchases_Carpet__c =140;
mat.Green_Opportunity_Carpet__c =100;
//mat.Brands__c = 'Aladdin;Horizon;Karastan;Aladdin Commercial;Portico;Properties;Hard Surface';
// mat.Product_Types__c = 'Carpet;Cushion;Resilient;Hardwood;Laminate;Tile;';                
}
insert mohAccTeam;

Custom_Activities__c CA = new Custom_Activities__c(
WhatId__c = mohAccTeam[0].id,
Account__c = accountList[0].id,
Assigned_To__c = Utility_Test.ADMIN_USER.Id
);
insert CA;
}*/
    @isTest
    static void test_insertCustomActivity() {
        //init();
        Custom_Activities__c cActivity = [select id,BDM_Ids__c,DM_Ids__c,RVP_Ids__c from Custom_Activities__c limit 1] ;
        System.assert(cActivity.BDM_Ids__c !=null);
        System.assert(cActivity.DM_Ids__c !=null);
        System.assert(cActivity.RVP_Ids__c !=null);
        //test.stopTest();
    }
    
    static Territory_User__c setTerritoryUserFields(Id terrId, Id recTypeId, String name, String role, String userId){
        Territory_User__c tUser = new Territory_User__c();
        tUser.RecordTypeId = recTypeId;
        tUser.Territory__c = terrId;
        tUser.Name = name;
        tUser.Role__c = role;
        tUser.User__c = userId;
        return tUser;
    }
}