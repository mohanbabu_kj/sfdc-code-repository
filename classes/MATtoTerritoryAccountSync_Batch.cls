/**************************************************************************
Name : MATtoTerritoryAccountSync_Batch 
===========================================================================
Purpose :           
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Nagendra       04/02/2019         Created          

Orginal Query -

select Id, Account__c, Territory_User_Number__c, RecordType.Name from Mohawk_Account_Team__c 
where RecordType.Name = 'Residential Invoicing' and LastModifiedBy.name != 'Batch User'
***************************************************************************/
global class MATtoTerritoryAccountSync_Batch implements Database.Batchable<sObject>,Database.Stateful,Schedulable{
   global Database.QueryLocator start(Database.BatchableContext bc) {
         String query;
         DateTime batchRunDate;
       MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('MATtoTerritoryAccountSync_Batch');
        if (custoSettingVal != null) {
            batchRunDate = custoSettingVal.Last_Run_At__c != null ? custoSettingVal.Last_Run_At__c : system.now();
            batchRunDate = (custoSettingVal.MinutesDelay__c !=null && batchRunDate !=null ) ? batchRunDate.addMinutes(Integer.valueof(custoSettingVal.MinutesDelay__c )) : batchRunDate ;
        }
        if(batchRunDate !=null){
            if(custoSettingVal.Batch_Query__c !=null){
               query = custoSettingVal.Batch_Query__c  +' and LastModifiedDate >= : batchRunDate ORDER BY Account__c DESC';  
            }else{
                query = 'select Id, Account__c, Territory_User_Number__c, RecordType.Name from Mohawk_Account_Team__c where RecordType.Name = \'' + 'Residential Invoicing' +' \' and LastModifiedBy.name != \'' + 'Batch User' + '\' and LastModifiedDate >= : batchRunDate ORDER BY  Account__c DESC';
            }
              
        }else{ 
            batchRunDate =System.today(); 
            query = 'Select Id, Account__c, Territory_User_Number__c, RecordType.Name from Mohawk_Account_Team__c where RecordType.Name = \'' + 'Residential Invoicing' +' \' and LastModifiedBy.name != \'' + 'Batch User' + '\' and LastModifiedDate >= : batchRunDate ORDER BY  Account__c DESC';
          
        }
       return Database.getQueryLocator(query);
   }
    global void execute(Database.BatchableContext BC, list<Mohawk_Account_Team__c> scope) {
         MATtoTerritoryAccountService.syncTerritoryAccounts(scope);
       
    }
    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [SELECT Id, Status, CompletedDate FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('MATtoTerritoryAccountSync_Batch');
        if (custoSettingVal != null) {
            custoSettingVal.Last_Run_At__c = a.CompletedDate;
            update custoSettingVal;
        }else{
            custoSettingVal = new MAT_Batch_Setting__c(name = 'MATtoTerritoryAccountSync_Batch', Last_Run_At__c = a.CompletedDate);
            insert custoSettingVal;
        }
    }
    global void execute(SchedulableContext SC){
        Id batchId=   Database.executeBatch(new MATtoTerritoryAccountSync_Batch());
    }
}