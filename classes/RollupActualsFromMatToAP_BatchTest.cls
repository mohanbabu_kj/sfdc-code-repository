/**************************************************************************

Name : RollupActualsFromMatToAP_BatchTest

===========================================================================
Purpose : Test class for batch class RollupActualsFromMatToAP_Batch to  rollup Actuals FROM Mohawk Account Team to Account Profile .
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha         23/Feb/2018    Created          CSR:

***************************************************************************/
@isTest
private class RollupActualsFromMatToAP_BatchTest {
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    static List<Territory_User__c>  terrUser;
    static List<Territory_User__c>  terrUser1;
    static List<Account_Profile__c> accProfile;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    public static void init(){   
        Utility_Test.getTeamCreationCusSetting();
        Utility_Test.getDefaultConfCusSetting();
    }
    
    static testMethod void testMethod0() {
        user intregrationUser = [ SELECT Id,Email, ProfileId FROM User WHERE Profile.Name = 'Integration User' AND IsActive=true LIMIT 1 ];
        System.runAs( intregrationUser ) {
            test.startTest(); 
            accProfile = Utility_Test.createAccountProfile(false, 1, 'R.900124', false);
            accProfile[0].multi_channel__c=true;
            accProfile[0].retail__c=100;
            accProfile[0].builder__c=100;
            accProfile[0].Multi_Family__c=100;
            
            
            insert accProfile;
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            
            terrList = Utility_Test.createTerritories(false, 1);
            
            for(Integer i=0;i<terrList.size();i++){
                terrList[i].Name = 'Testterr'+i;
                terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
            }
            
            insert terrList;
            terrUser = Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
            
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Chain_Name__c = 'R.752994';
                // resAccListForInvoicing[i].Chain_Number__c = 'R.900124';
                //  resAccListForInvoicing[i].AccountNumber = 'R.900124';
                
                resAccListForInvoicing[i].Business_Type__c = 'Commercial';
                resAccListForInvoicing[i].Territory__c = terrList[i].Id;
            }
            insert resAccListForInvoicing;
            
            
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
            //  mohAccTeam1 = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
            //  mohAccTeam.add(mohAccTeam1.get(0));
            List<Mohawk_Account_Team__c> mohawkAccTeam = new List<Mohawk_Account_Team__c>();
            
            Mohawk_Account_Team__c moht=new Mohawk_Account_Team__c(Account__c = resAccListForInvoicing[0].Id, User__c = Utility_Test.ADMIN_USER.id, Brands__c = 'Karastan;Aladdin;Horizon',
                                                                   Territory__c = terrList[0].Id,Actuals_Sent_from_BI__c=true,Account_Profile__c=accProfile[0].id);                 
            
            moht.Product_Types__c='Carpet';
            insert moht;
            
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Source_CAMS__c = true;
                //mohAccTeam[i].User__c = Utility_Test.ADMIN_USER.Id;
                mohAccTeam[i].RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL+' '+UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
                mohAccTeam[i].Account__c = resAccListForInvoicing.get(0).Id;
                mohAccTeam[i].Territory__c = terrList.get(0).Id;
                mohAccTeam[i].Actuals_Sent_from_BI__c=true;
                mohAccTeam[i].Account_Profile__c=accProfile[0].id;
                
                
            }
            try{
                Account_profile__c app=[select id,multi_channel__c from Account_profile__c where id=:mohAccTeam[0].Account_Profile__c LIMIT 1];
                app.multi_channel__c=true;
                update app;
                system.debug('passed moht ::::::'+ moht.Account_Profile__r.multi_channel__c);
                
                insert mohAccTeam;
                
            }
            catch(exception e){
                system.debug('error mht ::::::'+e);
            }
            List<Mohawk_account_team__c> mht=[SELECT Id,Account_Profile__c,Account_Profile__r.Multi_Channel__c,Actuals_Sent_from_BI__c,Brands__c,Product_Types__c,BMF_Team_Member__c,Actual_Mohawk_Purch_Aladdin_Comm_Total__c,Actual_Mohawk_Purchases_Aladdin__c,Actual_Mohawk_Purchases_Aladdin_Comm__c,Actual_Mohawk_Purchases_Carpet__c,Actual_Mohawk_Purchases_Cushion__c,Actual_Mohawk_Purchases_Hardwood__c,Actual_Mohawk_Purchases_Horizon__c,Actual_Mohawk_Purchases_Karastan__c,Actual_Mohawk_Purchases_Laminate__c,Actual_Mohawk_Purchases_Resilient__c,Actual_Mohawk_Purchases_Tile__c,Actual_Mohawk_Purchases_Total__c,Actual_Mohawk_Purch_Resilient_Comm__c,Actual_Mohawk_Purch_Resilient_Retail__c,Mohawk_Account_Team__c.User__c FROM Mohawk_Account_Team__c WHERE (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND Actuals_Sent_from_BI__c = true AND Account_Profile__r.Multi_Channel__c = true ALL ROWS];//Added Mohawk_Account_Team__c.User__c by Susmitha on Feb 23,2018  
            system.debug('mht :::::::'+mht.size());
            //Start - Code changes nagendra
        /*  RollupActualsFromMatToAP_Batch__c setting = new RollupActualsFromMatToAP_Batch__c();
            setting.Batch_Last_Run__c = SYstem.today().adddays(-5);
            setting.MinutesDelay__c = 5;
            setting.Name='Test';
            insert setting; */
            //End
            RollupActualsFromMatToAP_Batch  cls = new RollupActualsFromMatToAP_Batch(false,true);
            
            string jobId=database.executeBatch(cls,10);
            system.assert(jobId!=null);    
            test.stopTest();
        }    
    }    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest(); 
            //init();//Line Commented By Mudit - 26/09/17
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            
            terrList = Utility_Test.createTerritories(false, 2);
            
            for(Integer i=0;i<terrList.size();i++){
                terrList[i].Name = 'Testterr'+i;
                terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
                //terrList[i].User__c = Utility_Test.ADMIN_USER;
            }
            
            insert terrList;
            system.assert(terrList.size()>0);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Chain_Name__c = 'ABCD123456';
                resAccListForInvoicing[i].Business_Type__c = 'Residential';
                resAccListForInvoicing[i].Territory__c = terrList[i].Id;
            }
            insert resAccListForInvoicing;
            
            
            terrUser = Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,2);
            List<Territory__c> terrList1 = Utility_Test.createTerritories(false, 2);
            //terrUser1 = Utility_Test.createTerritoryWithUsers(true,terrList1,Utility_Test.RESIDENTIAL_USER,2);
            
            //Start - Code changes nagendra
            MAT_Batch_Setting__c custoSettingVal = new MAT_Batch_Setting__c(name = 'RollupActualsFromMatToAP_Batch', Last_Run_At__c = SYstem.today().adddays(-5), MinutesDelay__c = 5);
            insert custoSettingVal;

            //End
            RollupActualsFromMatToAP_Batch  cls = new RollupActualsFromMatToAP_Batch(false,true);
            
            
            string jobId=database.executeBatch(cls,10);
            system.assert(jobId!=null);
            
            
            
            //TerritoryCheckPublicGroup_Batch clss = new TerritoryCheckPublicGroup_Batch(); //Line Commented By Mudit - 26/09/17
            //database.executeBatch(clss,10);//Line Commented By Mudit - 26/09/17
            test.stopTest();
        }
    }
    
    
    static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest(); 
            //init();//Line Commented By Mudit - 26/09/17
            accProfile = Utility_Test.createAccountProfile(false, 1, 'R.900124', false);
            accProfile[0].multi_channel__c=true;
            accProfile[0].retail__c=100;
            accProfile[0].builder__c=100;
            accProfile[0].Multi_Family__c=100;
            
            
            insert accProfile;
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            
            terrList = Utility_Test.createTerritories(false, 1);
            
            for(Integer i=0;i<terrList.size();i++){
                terrList[i].Name = 'Testterr'+i;
                terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
            }
            
            insert terrList;
            terrUser = Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
            
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Chain_Name__c = 'R.900124';
                // resAccListForInvoicing[i].Chain_Number__c = 'R.900124';
                //  resAccListForInvoicing[i].AccountNumber = 'R.900124';
                
                resAccListForInvoicing[i].Business_Type__c = 'Commercial';
                resAccListForInvoicing[i].Territory__c = terrList[i].Id;
            }
            insert resAccListForInvoicing;
            
            
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
            //  mohAccTeam1 = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
            //  mohAccTeam.add(mohAccTeam1.get(0));
            List<Mohawk_Account_Team__c> mohawkAccTeam = new List<Mohawk_Account_Team__c>();
            
            Mohawk_Account_Team__c moht=new Mohawk_Account_Team__c(Account__c = resAccListForInvoicing[0].Id, User__c = Utility_Test.ADMIN_USER.id, Brands__c = 'Karastan;Aladdin;Horizon',
                                                                   Territory__c = terrList[0].Id,Actuals_Sent_from_BI__c=true,Account_Profile__c=accProfile[0].id);                 
            
            moht.Product_Types__c='Carpet';
            insert moht;
            
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Source_CAMS__c = true;
                //mohAccTeam[i].User__c = Utility_Test.ADMIN_USER.Id;
                mohAccTeam[i].RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL+' '+UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
                mohAccTeam[i].Account__c = resAccListForInvoicing.get(0).Id;
                mohAccTeam[i].Territory__c = terrList.get(0).Id;
                mohAccTeam[i].Actuals_Sent_from_BI__c=true;
                mohAccTeam[i].Account_Profile__c=accProfile[0].id;
                
                
            }
            try{
                Account_profile__c app=[select id,multi_channel__c from Account_profile__c where id=:mohAccTeam[0].Account_Profile__c LIMIT 1];
                app.multi_channel__c=true;
                update app;
                system.debug('passed moht ::::::'+ moht.Account_Profile__r.multi_channel__c);
                
                insert mohAccTeam;
                
            }
            catch(exception e){
                system.debug('error mht ::::::'+e);
            }
            List<Mohawk_account_team__c> mht=[SELECT Id,Account_Profile__c,Account_Profile__r.Multi_Channel__c,Actuals_Sent_from_BI__c,Brands__c,Product_Types__c,BMF_Team_Member__c,Actual_Mohawk_Purch_Aladdin_Comm_Total__c,Actual_Mohawk_Purchases_Aladdin__c,Actual_Mohawk_Purchases_Aladdin_Comm__c,Actual_Mohawk_Purchases_Carpet__c,Actual_Mohawk_Purchases_Cushion__c,Actual_Mohawk_Purchases_Hardwood__c,Actual_Mohawk_Purchases_Horizon__c,Actual_Mohawk_Purchases_Karastan__c,Actual_Mohawk_Purchases_Laminate__c,Actual_Mohawk_Purchases_Resilient__c,Actual_Mohawk_Purchases_Tile__c,Actual_Mohawk_Purchases_Total__c,Actual_Mohawk_Purch_Resilient_Comm__c,Actual_Mohawk_Purch_Resilient_Retail__c,Mohawk_Account_Team__c.User__c FROM Mohawk_Account_Team__c WHERE (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND Actuals_Sent_from_BI__c = true AND Account_Profile__r.Multi_Channel__c = true ALL ROWS];//Added Mohawk_Account_Team__c.User__c by Susmitha on Feb 23,2018  
            system.debug('mht :::::::'+mht.size());
            
            
            //  mohAccTeam1 = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.COMMERCAIL_USER);
            // for(Integer i=0;i<mohAccTeam1.size();i++){
            //     mohAccTeam1[i].Account_Access__c = 'Edit';
            //     mohAccTeam1[i].Source_CAMS__c = true;
            //     mohAccTeam[i].User__c = Utility_Test.COMMERCAIL_USER.Id;
            //    mohAccTeam1[i].RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL+' '+UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
            // }
            // insert mohAccTeam1;
            
            //TerritoryCheckMohawAccTeam_Batch  cls = new TerritoryCheckMohawAccTeam_Batch ();
            //database.executeBatch(cls,10);
            
            //TerritoryCheckPublicGroup_Batch clss = new TerritoryCheckPublicGroup_Batch();//Line Commented By Mudit - 26/09/17
            //database.executeBatch(clss,10);//Line Commented By Mudit - 26/09/17
            // Schedule the test job
            //String jobId = System.schedule('Test my class', '0 0 0 1 JAN,APR,JUL,OCT ? *', new TerritoryCheckMohawAccTeam_Batch());
            //System.assert(jobid != null);
              
            //Start - Code changes nagendra
            /*RollupActualsFromMatToAP_Batch__c setting = new RollupActualsFromMatToAP_Batch__c();
            setting.Batch_Last_Run__c = SYstem.today().adddays(-5);
            setting.MinutesDelay__c = 5;
            setting.Name='Test';
            insert setting;*/
            //End
            RollupActualsFromMatToAP_Batch  cls = new RollupActualsFromMatToAP_Batch(true,false);// Passed true,false` parameter to run Batch in Default Query or LastBatchRun mode- by Nagendra
            
            string jobId=database.executeBatch(cls,10);
            system.assert(jobId!=null);
            test.stopTest();
        }
    }
    
    
    
}