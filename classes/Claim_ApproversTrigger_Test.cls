@isTest(seeAllData=true)
public class Claim_ApproversTrigger_Test{    
    static testMethod void testMethod1(){
        System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
            
            Claim__c CL = new Claim__c(
                Name = 'TEST Claim',
                //Account__c = '',
                //Account_Global_Id__c = '',
                Claim_Amount__c = 10,
                Claim_Submit_Date__c = system.today(),
                Claim_Type__c = 'INSTALLED',
                Hybris_Claim_Id__c = '123',
                Status__c = 'INPROCESS'            
            );
            insert CL;
            system.assert(CL.id!=null);
            
            Claim_Approvers__c CA = new Claim_Approvers__c(
                Name = 'Test1',
                Approver__c = Utility_Test.ADMIN_USER.ID,
                Claim__c = CL.ID,
                Employee_Email__c = 'TEST@TEST.COM',
                Employee_ID__c = '123',
                Hybris_Claim_Id__c = '123'
            );
            insert CA;
            system.assert(CA.id!=null);
            
            
            Claim_Approvers__c cla = Utility_Test.createClaimApproval();
            Test.stopTest();
        }
    }
}