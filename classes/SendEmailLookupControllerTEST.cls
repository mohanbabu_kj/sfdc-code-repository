/**************************************************************************

Name : SendEmailLookupControllerTEST

===========================================================================
Purpose : This class is used for SendEmailLookupController
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha         26/March/2018     Modified    1
***************************************************************************/
@isTest
private class SendEmailLookupControllerTEST{
    
    static List<Account> accountList;
    
    
    static testMethod void testMethod1() {
        accountList = Utility_Test.createAccounts(true,3);
        List<Event> eventList = Utility_Test.createEvents(true, 3, accountList);        
        SendEmailLookupController.getData(eventList[0].id);
        system.assert(SendEmailLookupController.getData(eventList[0].id)!=null);

    }
    static testMethod void testMethod2() {
        SendEmailLookupController.getData('');
        system.assert(SendEmailLookupController.getData('')!=null);
    }
    
}