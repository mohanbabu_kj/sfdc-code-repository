/**************************************************************************
Name : NotificationUtility_Test
===========================================================================
Purpose : Test class for NotificationUtility
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mohan       21/March/2018    Create  
***************************************************************************/
@isTest
public class NotificationUtility_Test {

   static List<Territory_User__c> territoryUserList;
   static void init(){
        Utility_Test.getTeamCreationCusSetting();
    }
    @isTest
    public static void sendBatchExceptionNotificationTest(){
        test.startTest(); 
        init();
        territoryUserList = Utility_Test.createTerritoryUsers(false, 1);
        
        for(Integer i=0;i<territoryUserList.size();i++){
            territoryUserList[i].Name = 'Testterr'+i;
            territoryUserList[i].RecordTypeId = Schema.SObjectType.Territory_User__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
        }
        insert territoryUserList;
        system.assert(territoryUserList.size()>0);
        
        String jobId = database.executeBatch(new TerritoryCheckPublicGroup_Batch());
        System.assert(jobId != null);
        NotificationUtility.sendBatchExceptionNotification( jobId,'TEST RECORD 1', 'Error List');
        test.stopTest();
    }
}