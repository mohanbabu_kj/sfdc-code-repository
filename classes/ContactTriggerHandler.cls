public class ContactTriggerHandler {
    
    public static String CommRTId = '';
    public static String ResRTId = '';
    
    static{
        CommRTId = UtilityCls.getRecordTypeInfo('Contact', 'Commercial');
        ResRTId  = UtilityCls.getRecordTypeInfo('Contact', 'Residential');
    }
    public static void updateContact(List<Contact> newContactList, List<Contact> oldContactList, boolean insert_flag, boolean update_flag){
        Set<Id> accIds = new Set<Id>();
        Map<Id, Account> accMap = new Map<Id, Account>();
        Default_Configuration__c defConfig = Default_Configuration__c.getOrgDefaults();
        if(insert_flag){
            for(Contact newContact: newContactList){
                accIds.add(newContact.AccountId);
            }
            if(accIds.size()>0){
                for(Account acc: [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingStateCode, ShippingCountry, ShippingCountryCode, ShippingPostalCode
                                 FROM Account
                                  WHERE Id IN :accIds]){
                                      accMap.put(acc.Id, acc);
                                  }
            }
            for(Contact newContact: newContactList){
            if( accMap.ContainsKey( newContact.AccountId ) ){  //Added by krishnapriya on Nov 14 2017      
                Account acc = accMap.get(newContact.AccountId);
                                                          
                if(acc != null && newContact.MailingStreet == null && newContact.MailingCity == null &&
                   newContact.MailingStateCode == null && newContact.MailingPostalCode == null){
                       if(acc.ShippingStreet != null && acc.ShippingStreet.trim().length() >= 0){   //Added condition acc.ShippingStreet.trim().length() == 0  by krishnapriya on Nov 14 2017 
                           newContact.MailingStreet = acc.ShippingStreet;
                       }
                       
                       if( acc.ShippingCity != null){
                           newContact.MailingCity = acc.ShippingCity;
                       }
                       if( acc.ShippingStateCode != null){
                           newContact.MailingStateCode = acc.ShippingStateCode;
                       }
                       /*if(newContact.MailingState == null && acc.ShippingState != null){
                        newContact.MailingState = acc.ShippingState;
                        }*/
                       if(acc.ShippingCountryCode != null){
                           newContact.MailingCountryCode = acc.ShippingCountryCode;
                       }
                       if(acc.ShippingPostalCode != null){
                           newContact.MailingPostalCode = acc.ShippingPostalCode;
                       }
                   }
                 }
                /*if(newContact.RecordTypeId == CommRTId && defConfig.Commercial_Sales_Ops__c != null){
                    newContact.OwnerId = defConfig.Commercial_Sales_Ops__c;
                }else if(newContact.RecordTypeId == ResRTId && defConfig.Residential_Sales_Ops__c != null){
                    newContact.OwnerId = defConfig.Residential_Sales_Ops__c;
                }- Commented by MB - 10/12/17*/
            }
        }else if(update_flag){
            
        }
    }
    // Start of of Code - SSB- 9-28-17 - 
    // this code is needed for SFDC to LDS interface 
    // and should be deactivated once rollout is complete
    public static void ldsFlag(List<Contact> newList,Boolean update_flag,Boolean insert_flag ){
        User usr = [Select CAMS_Employee_Number__c from User where Id  =: UserInfo.getUserId()];
        for(Contact  cnt: newList){
            cnt.CAMS_Employee_ID_of_Logged_in_User__c = usr.CAMS_Employee_Number__c;   
            if(update_flag && cnt.RecordTypeId == CommRTId){
                System.Debug('Contact Update LDS Flags' + 'update flag' + update_flag +'insert flag' + insert_flag);  
            /*     if(cnt.LDS_Create__c){               // All updates will be handlled in batch mode. 10-16-17
                    cnt.LDS_Create__c = false; 
                }   
                if(!cnt.LDS_Update__c){
                    cnt.LDS_Update__c = true; 
                } */                                    // All updates will be handlled in batch mode. 10-16-17
            } else if(insert_flag && cnt.RecordTypeId == CommRTId) {
                System.Debug('Contact Update LDS Flags' + 'update flag' + update_flag +'insert flag' + insert_flag);
                cnt.LDS_Create__c = true;
                cnt.LDS_Update__c = false;
             }
        }
     }
    // End of Code - SSB- 9-28-17
    
    
}