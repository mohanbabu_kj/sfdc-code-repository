public class AccConOppUpdateBatchLDS {
  /*  
  global class AccConOppUpdateBatchLDS implements Database.Batchable<sObject>{
    global final String query;
    global final String updateField;
    global final String createField;
    global final Boolean updateFieldVal;
    global final Boolean createFieldVal;
    global final string bType;
    global final String RecTypeId;
    global final DateTime start;
    global final DateTime endDate1;

    global AccConOppUpdateBatchLDS(String q, String upda,String cre , Boolean v1,Boolean v2,string bType,String RecTypeId,DateTime startDateTime,DateTime endDateTime){
        
        updateField = upda;//LDS_Update__c field
        createField = cre; //LDS_Create__c
        updateFieldVal = v1;
        createFieldVal = v2;
        bType = bType;
        RecTypeId =RecTypeId;//Non-invoicing ID
        start =startDateTime ;
        endDate1 = endDateTime;
        
         query = q;
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject > scope)
    {        
        system.debug('scope : '+scope);
        for(SObject s : scope)
        {
            s.put(updateField,updateFieldVal);
            s.put(createField,createFieldVal);
        }
        system.debug('scope : '+scope);
        update scope;
        
        
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('Finish job');
    } */
}