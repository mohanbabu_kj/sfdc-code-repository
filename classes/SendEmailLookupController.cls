public class SendEmailLookupController {
	
    @AuraEnabled
    public static dataWrapper getData( String eventId ) {
    	dataWrapper DW = new dataWrapper();        
        list<Event> ev = [ SELECT ID, whatId FROM Event where id =: eventId limit 1 ];
        if( ev.size() > 0 ){            
            DW.contactList = [ SELECT Name,email FROM Contact where AccountId =: ev[0].WhatId Limit 50000 ];
        }
        DW.userList = [SELECT Name, email, Profile.Name FROM User WHERE ISACTIVE = TRUE and (Profile.Name='Residential Sales User') ];
        return DW;
    }
    
    public class dataWrapper{
        @AuraEnabled
        public List<contact>contactList;
        @AuraEnabled
        public List<user>userList;
        public dataWrapper(){
            this.contactList = new List<contact>();
            this.userList = new List<User>();
        }
    }
    
}