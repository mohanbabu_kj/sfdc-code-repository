public class OpportunityTeamMemberExtension {
	public Id oppId;
    
    public OpportunityTeamMemberExtension(ApexPages.StandardController stdController){
        this.oppId = ApexPages.CurrentPage().getParameters().get('oppId');
        System.debug('Opp ID: ' + oppId);
    }
    public Id getoppId(){
        return oppId;
    }
}