global class UpdateSFDCIdForNotesLink_Batch implements Database.Batchable<sObject>,Schedulable{

    //public static String query = 'SELECT Id, GUID__c, Note_ID__c, Object_ID__c, Object_Type__c FROM Content_Note_Link__c LIMIT 5000000';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = System.Label.Content_Note_Link_SOQL;
        return Database.getQueryLocator(soql);
    }
    
    global static void execute(Database.BatchableContext BC, List<Content_Note_Link__c> scope){
        Set<String> accGUID = new Set<String>();
        Set<String> contGUID = new Set<String>();
        Set<String> projGUID = new Set<String>();
        Set<String> actGUID = new Set<String>();
        Map<String, Id> guidSfdcIdMap = new Map<String, Id>();

        for(Content_Note_Link__c cnl: scope){
            if(cnl.Object_Type__c == 'Account')
                accGUID.add(cnl.GUID__c);
            
            if(cnl.Object_Type__c == 'Project')
                projGUID.add(cnl.GUID__c);
            
            if(cnl.Object_Type__c == 'Contact')
                contGUID.add(cnl.GUID__c);
            
            if(cnl.Object_Type__c == 'Activity')
                actGUID.add(cnl.GUID__c);
        }
        
        if(accGUID.size()>0){
            for(List<Account> accList: [SELECT Id, SAP_Account_GUID__c from Account where SAP_Account_GUID__c IN :accGUID]){
                for(Account acc: accList){
                    guidSfdcIdMap.put(acc.SAP_Account_GUID__c, acc.Id);
                }
            }
        }
        
        if(contGUID.size()>0){
            for(List<Contact> contList: [SELECT Id, SAP_Contact_GUID__c from Contact where SAP_Contact_GUID__c IN :contGUID]){
                for(Contact cont: contList){
                    guidSfdcIdMap.put(cont.SAP_Contact_GUID__c, cont.Id);
                }
            }
        }
        
        if(projGUID.size()>0){
            for(List<Opportunity> oppList: [SELECT Id, SAP_Opportunity_GUID__c from Opportunity where SAP_Opportunity_GUID__c IN :projGUID]){
                for(Opportunity opp: oppList){
                    guidSfdcIdMap.put(opp.SAP_Opportunity_GUID__c, opp.Id);
                }
            }
        }
        
        if(actGUID.size()>0){
            for(List<Event> apptList: [SELECT Id, SAP_Appointment_GUID__c from Event where SAP_Appointment_GUID__c IN :actGUID]){
                for(Event appt: apptList){
                    guidSfdcIdMap.put(appt.SAP_Appointment_GUID__c, appt.Id);
                }
            }
            
            for(List<Task> taskList: [SELECT Id, SAP_Appointment_GUID__c from Task where SAP_Appointment_GUID__c IN :actGUID]){
                for(Task task: taskList){
                    guidSfdcIdMap.put(task.SAP_Appointment_GUID__c, task.Id);
                }
            }
        }
        
        //Map SFDC Object Id for GUID
        List<Content_Note_Link__c> contNoteLinkList = new List<Content_Note_Link__c>();
        for(Content_Note_Link__c cnl: Scope){
            String sfdcId = '';
            Content_Note_Link__c contNoteLink = new Content_Note_Link__c();
            sfdcId = guidSfdcIdMap.get(cnl.GUID__c);
            IF(sfdcId != null){
                contNoteLink = cnl;
                contNoteLink.Object_ID__c = sfdcId;
                contNoteLinkList.add(contNoteLink);
            }
        }
        
        //Update with SFDC ID
        if(contNoteLinkList.size()>0){
            Try{
                System.debug('ContentNoteLink : ' + contNoteLinkList); 
                List<Database.SaveResult> contentResults = Database.Update(contNoteLinkList);
                for (Database.SaveResult sr : contentResults) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully updated Content_Note_Link: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('Error while updating Content_Note_Link: ' + err.getFields());
                        }
                    }
                }
            }
            Catch(Exception dmle){
                System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
        Database.executeBatch(new UpdateSFDCIdForNotesLink_Batch());
    }
}