@isTest
public class MATtoTerritoryAccountSync_BatchTest {
static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static List<User> userList = new List<User>();
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static Id resTerrUserRecTypeId = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
    static List<Territory2> terr2List = new List<Territory2>();
    static List<Territory_User__c>  terrUserList;
    
    
    static List<Territory_User__c>  terrUser1;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    @testSetup
    public static void init(){   
      //  Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
       
    }
   static testmethod void testTerritoryAccountsSync() {
        MAT_Batch_Setting__c setting = new MAT_Batch_Setting__c();
            setting.Last_Run_At__c = SYstem.today().adddays(-2);
            setting.MinutesDelay__c = 5;
            setting.Batch_Size__c = 200;
            setting.Name='MATtoTerritoryAccountSync_Batch';
            setting.Batch_Query__c = 'Select Id,Account__c,Global_Account_Number__c from Mohawk_Account_Team__c where RecordType.Name = \'' + 'Residential Invoicing' +' \'';
            insert setting;
       System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            
           
            
            User u=Utility_Test.getTestUser('TestUser',null);
            insert u;
            Territory2Model terr2Model = [SELECT Id FROM Territory2Model LIMIT 1];
            Territory2Type terr2Type = [SELECT Id FROM Territory2Type LIMIT 1];
            
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 TM', 'D991_TM'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 DM', 'D991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'R991 - South Soft RVP ', 'R991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'S991 - S/S East SVP ', 'S991'));
            insert terr2List;
           
            terrList =  new List<Territory__c>();
            Territory__c terr = new Territory__c();
            terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
            terr.Territory_Code__c = '991';
            terr.Region_Code__c = '991';
            terr.Sector_Code__c = '991';
            terr.Name = 'District 991';
            terrList.add(terr);
            insert terrList;
           userList = Utility_Test.createTestUsers(false, 5, 'Residential Sales User');
           insert userList;
             terrUserList = new List<Territory_User__c>();
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-J8L', 'Territory Manager', userList[0].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-K8L', 'District Manager', userList[1].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-H8L', 'Business Development Manager', userList[2].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-M8L', 'Regional Vice President', userList[3].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-N8L', 'Senior Vice President', userList[4].Id));
            insert terrUserList;
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c=aplist[0].id;
            }
            insert resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
           
            /***END Creating Account Profile Setting and Account Profile****/
           
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                mat.User__c=u.id;
                mat.Territory_User_Number__c = '05-J8L';
            }
            insert mohAccTeam;
            

            test.setCreatedDate(mohAccTeam[0].id, System.today());
            test.startTest();
               
                 Id batchId=   Database.executeBatch(new MATtoTerritoryAccountSync_Batch());
                 String jobIdSch = System.schedule('Test my class', '0 0 0 ? * * *', new MATtoTerritoryAccountSync_Batch());
                 System.assert(batchId!=null);
                 System.assert(jobIdSch!=null);
         
            test.stopTest();
            
        }
     }
    static testmethod void testTerritoryAccountsSync1() {
        System.runAs(Utility_Test.ADMIN_USER) {           
            init();
            User u=Utility_Test.getTestUser('TestUser',null);
            insert u;
            Territory2Model terr2Model = [SELECT Id FROM Territory2Model LIMIT 1];
            Territory2Type terr2Type = [SELECT Id FROM Territory2Type LIMIT 1];
            
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 TM', 'D991_TM'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 DM', 'D991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'R991 - South Soft RVP ', 'R991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'S991 - S/S East SVP ', 'S991'));
            insert terr2List;
           
            terrList =  new List<Territory__c>();
            Territory__c terr = new Territory__c();
            terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
            terr.Territory_Code__c = '991';
            terr.Region_Code__c = '991';
            terr.Sector_Code__c = '991';
            terr.Name = 'District 991';
            terrList.add(terr);
            insert terrList;
           userList = Utility_Test.createTestUsers(false, 5, 'Residential Sales User');
           insert userList;
             terrUserList = new List<Territory_User__c>();
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-J8L', 'Territory Manager', userList[0].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-K8L', 'District Manager', userList[1].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-H8L', 'Business Development Manager', userList[2].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-M8L', 'Regional Vice President', userList[3].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-N8L', 'Senior Vice President', userList[4].Id));
            insert terrUserList;
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c=aplist[0].id;
            }
            insert resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
           
            /***END Creating Account Profile Setting and Account Profile****/
           
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                mat.User__c=u.id;
                mat.Territory_User_Number__c = '05-J8L';
            }
            insert mohAccTeam;
            
          // UserTerritory2Association usrTerr2Association = new UserTerritory2Association(Territory2Id = terr2List[0].Id, UserId = u.Id);
          // insert usrTerr2Association;
            
            test.setCreatedDate(mohAccTeam[0].id, System.today());
            test.startTest();
                Delete [Select id from ObjectTerritory2Association];
                     Id batchId=   Database.executeBatch(new MATtoTerritoryAccountSync_Batch());
                        String jobIdSch = System.schedule('Test my class', '0 0 0 ? * * *', new MATtoTerritoryAccountSync_Batch());
                  System.assert(batchId!=null);
                 System.assert(jobIdSch!=null);
         
            test.stopTest();
            
        }
    }
    
   
     public static Territory_User__c setTerritoryUserFields(Id terrId, Id recTypeId, String name, String role, String userId){
        Territory_User__c tUser = new Territory_User__c();
        tUser.RecordTypeId = recTypeId;
        tUser.Territory__c = terrId;
        tUser.Name = name;
        tUser.Role__c = role;
        tUser.User__c = userId;
        return tUser;
    }
    public static Territory2 setTerritory2Fields(Id terr2ModelId, Id terr2TypeId, String name, String devName){
        Territory2 terr2 = new Territory2();
        terr2.Territory2ModelId = terr2ModelId;
        terr2.Territory2TypeId = terr2TypeId;
        terr2.Name = name;
        terr2.DeveloperName = devName;
        return terr2;
    }
}