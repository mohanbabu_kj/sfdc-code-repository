public class OpportunityTeamMemberTriggerHandler {
    
    public static List<Id> delOppShareIds = new List<Id>(); // Added by MB - 06/13/17
    public static List<OpportunityShare> updOppShareList = new List<OpportunityShare>(); //Start of code - MB - 08/04/2017
    
    //Start of code - MB - 08/04/2017
    public static void AddOppShareAccess(List<OpportunityTeamMember> oppTeamMemberList){
        Set<Id> oppIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        //Set<Id> grpIds = new Set<Id>();
        Set<String> terrNames = new Set<String>();
        Map<Id, Set<String>> oppTerrMap = new Map<Id, Set<String>>();
        Map<Id, Set<Id>> oppGrpMap = new Map<Id, Set<Id>>();
        Map<Id, Id> oppRecTypeMap = new Map<Id, Id>();
        Map<Id, Id> oppOwnerMap = new Map<Id, Id>();
        Map<Id, Id> oppTeamTerrMap = new Map<Id, Id>();
        Map<Id, Id> userTerrIdMap = new Map<Id, Id>();
        Map<Id, String> terrIdNameMap = new Map<Id, String>();
        Map<String, Id> grpNameIdMap = new Map<String, Id>();
        List<OpportunityShare> oppShareList = new List<OpportunityShare>(); 
        
        for(OpportunityTeamMember oppTeam: oppTeamMemberList){
            oppIds.add(oppTeam.OpportunityId);
            userIds.add(oppTeam.UserId);
        }
        if(oppIds.size()>0){
            for(Opportunity opp: [SELECT Id, RecordTypeId, OwnerId FROM Opportunity
                                  WHERE Id IN :oppIds]){
                                      oppRecTypeMap.put(opp.Id, opp.recordTypeId);
                                      oppOwnerMap.put(opp.Id, opp.OwnerId);
                                  }
        }
        if(userIds.size() > 0){
            for(Territory_User__c terrUser: [Select Territory__c, Territory__r.Name, Role__c, User__c FROM Territory_User__c 
                                             Where User__c IN :userIds and Role__c =: Utilitycls.AE]){
                                                 terrNames.add(terrUser.Territory__r.Name);
                                                 terrIdNameMap.put(terrUser.Territory__c, terrUser.Territory__r.Name);
                                                 userTerrIdMap.put(terrUser.User__c, terrUser.Territory__c);
                                             }
            if(terrNames.size() > 0){
                for(Group grp : [Select Id, Name from Group Where Name IN :terrNames
                                 and type = 'Regular']){
                                     grpNameIdMap.put(grp.Name, grp.Id);
                                 }
            }
            /*for(OpportunityTeamMember oppTeam: oppTeamMemberList){
//oppIds.add(oppTeam.OpportunityId);
string recordtypename = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(oppRecTypeMap.get(oppTeam.OpportunityId)).getname();
System.debug('Record Type Name: ' + recordtypename);
if((recordtypename == UtilityCls.ANNUITY || recordtypename == Utilitycls.COMMERCIAL_TRADITIONAL || recordtypename == Utilitycls.TRANSACTIONAL) &&
oppTeam.UserId != oppOwnerMap.get(oppTeam.OpportunityId)){
for(Territory_User__c terrUser: [Select Territory__c, Territory__r.Name, Role__c, User__c FROM Territory_User__c 
Where User__c =: oppTeam.UserId and Role__c =: Utilitycls.AE]){
//terrIds.add(terrUser.Territory__c);
terrNames.add(terrUser.Territory__r.Name);
if(oppTerrMap.containsKey(oppTeam.OpportunityId)){
oppTerrMap.get(oppTeam.OpportunityId).add(terrUser.Territory__r.Name);
}else{
Set<String> teamNames = new Set<String>();
teamNames.add(terrUser.Territory__r.Name);
oppTerrMap.put(oppTeam.OpportunityId, teamNames);
}
oppTeamTerrMap.put(oppTeam.Id, terrUser.Territory__c);
break;
}
System.debug('Territory Names: ' + terrNames);
System.debug('User Territory Map: ' + userTerrIdMap);
if(terrNames.size() > 0){
for(Group grp : [Select Id, Name from Group Where Name IN :terrNames
and type = 'Regular']){
if(oppGrpMap.containsKey(oppTeam.OpportunityId)){
oppGrpMap.get(oppTeam.OpportunityId).add(grp.Id);
}else{
Set<Id> groupIds = new Set<Id>();
groupIds.add(grp.Id);
oppGrpMap.put(oppTeam.OpportunityId, groupIds);
}
} 
System.debug('Opportunity Groups Map: ' + oppGrpMap);
}
}
}*/
            
            for(OpportunityTeamMember oppTeam: oppTeamMemberList){
                string recordtypename = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(oppRecTypeMap.get(oppTeam.OpportunityId)).getname();
                System.debug('Record Type Name: ' + recordtypename);
                if((recordtypename == UtilityCls.ANNUITY || recordtypename == Utilitycls.COMMERCIAL_TRADITIONAL || recordtypename == Utilitycls.TRANSACTIONAL) &&
                   oppTeam.UserId != oppOwnerMap.get(oppTeam.OpportunityId)){
                       Id terrId = userTerrIdMap.get(oppTeam.UserId);
                       String tName = terrIdNameMap.get(terrId);
                       Id grpId = grpNameIdMap.get(tName);
                       if(oppGrpMap.containsKey(oppTeam.OpportunityId)){
                           oppGrpMap.get(oppTeam.OpportunityId).add(grpId);
                       }else{
                           Set<Id> groupIds = new Set<Id>();
                           groupIds.add(grpId);
                           oppGrpMap.put(oppTeam.OpportunityId, groupIds);
                       }
                   }
            }
            if(oppGrpMap.size() > 0){
                for(Id oppId: oppIds){
                    Set<Id> groupListIds = oppGrpMap.get(oppId);
                    for(Id tmpid: groupListIds){
                        if(tmpid != null && String.valueOf(tmpid) != ''){
                            OpportunityShare oppShare =  new OpportunityShare();
                            oppShare.OpportunityId = oppId;
                            oppShare.OpportunityAccessLevel = 'Edit';
                            oppShare.UserOrGroupId = tmpid;
                            oppShareList.add(oppShare);
                        }
                    }
                }
            }
            
            System.debug('Opp. Share List: ' + oppShareList);
            if(oppShareList.size() > 0){
                if(updOppShareList.size()>0){
                    updOppShareList.clear();
                }
                updOppShareList.addAll(oppShareList);
                UpdateOpportunityShareAccess oppShareAccess = new UpdateOpportunityShareAccess();
                oppShareAccess.InsertOppShareAccess();
            } 
        }
    }
    
    public static void updateOppTeam(List<OpportunityTeamMember> oppTeamMemberList){
        Set<Id> oppIds = new Set<Id>();
        //Set<Id> userIds = new Set<Id>();
        Map<Id, Id> oppRecTypeMap = new Map<Id, Id>();
        Map<Id, Id> userTerrIdMap = new Map<Id, Id>();
        for(OpportunityTeamMember oppTeam: oppTeamMemberList){
            oppTeam.OpportunityAccessLevel = 'Edit';
            oppIds.add(oppTeam.OpportunityId);
            //userIds.add(oppTeam.UserId);
        }
        if(oppIds.size()>0){
            for(Opportunity opp: [SELECT Id, RecordTypeId FROM Opportunity
                                  WHERE Id IN :oppIds]){
                                      oppRecTypeMap.put(opp.Id, opp.recordTypeId);
                                  }
        }
        /*if(userIds.size() > 0){
for(Territory_User__c terrUser: [Select Territory__c, Territory__r.Name, Role__c, User__c FROM Territory_User__c 
Where User__c IN :userIds and Role__c =: Utilitycls.AE]){
userTerrIdMap.put(terrUser.User__c, terrUser.Territory__c);
}
}*/
        for(OpportunityTeamMember oppTeam: oppTeamMemberList){
            string recordtypename = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(oppRecTypeMap.get(oppTeam.OpportunityId)).getname();
            if(recordtypename == UtilityCls.ANNUITY || recordtypename == Utilitycls.COMMERCIAL_TRADITIONAL || recordtypename == Utilitycls.TRANSACTIONAL){
                oppTeam.Team_Role_Commercial__c = oppTeam.TeamMemberRole;
                /*if(userTerrIdMap.get(oppTeam.UserId) != null){
oppTeam.Territory__c = userTerrIdMap.get(oppTeam.UserId);
}*/
            }else{
                oppTeam.Team_Role_Resi__c = oppTeam.TeamMemberRole;
            }
        }
        System.debug('Opportunity Team Member: ' + oppTeamMemberList);
    }
    //End of code - MB - 08/04/2017
    
    public static void DeleteOppShareAccess(List<OpportunityTeamMember> oppTeamMemberList){
        Set<String> grpNameSet = new Set<String>();
        Set<Id> oppIds = new Set<Id>();
        Set<Id> delOppIds = new Set<Id>();
        Set<Id> delGrpIds = new Set<Id>();
        Set<Id> publicGroupIds = new Set<Id>();
        //Set<Id> terrIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        List<OpportunityShare> delOppShareList = new List<OpportunityShare>();
        Map<Id,List<Id>> oppPublicGroupMap = new Map<Id,List<Id>>();
        Map<Id, String> publicGroupNameMap = new Map<Id, String>();
        Map<Id, Id> delOppShareMap = new Map<Id, Id>();
        Map<Id, String> terrNameMap = new Map<Id, String>();
        Map<Id, Id> userTerrIdMap = new Map<Id, Id>();
        
        //Collect OppIds and TerritoryIds
        for(OpportunityTeamMember oppTeamMember: oppTeamMemberList){
            oppIds.add(oppTeamMember.OpportunityId);
            //terrIds.add(oppTeamMember.Territory__c);
            userIds.add(oppTeamMember.UserId);
        }
        
        //Get the Territory Names which will be Public Group Names
        if(userIds.size()>0){
            //for(Territory__c terr: [SELECT Id, Name FROM Territory__c 
            //                        WHERE Id IN (SELECT Territory__c from Territory_User__c WHERE User__c IN :userIds)]){
            for(Territory_User__c terrUser: [SELECT Id, Name, Territory__c, Territory__r.Name, User__c from Territory_User__c WHERE User__c IN :userIds]){
                grpNameSet.add(terrUser.Territory__r.Name);
                terrNameMap.put(terrUser.Territory__c, terrUser.Territory__r.Name);
                userTerrIdMap.put(terrUser.User__c, terrUser.Territory__c);
            }
        }
        System.debug('TerrNameMap: ' + terrNameMap);
        
        //Get existing opportunity share
        if(oppIds.size()>0){
            for(OpportunityShare oppShare : [SELECT Id, OpportunityId, UserOrGroupId FROM OpportunityShare
                                             WHERE OpportunityId IN :oppIds]){
                                                 String grpValue = String.valueOf(oppShare.UserOrGroupId);
                                                 if(grpValue.startsWith('00G')){
                                                     publicGroupIds.add(oppShare.UserOrGroupId);
                                                     if(oppPublicGroupMap.containsKey(oppShare.OpportunityId)){
                                                         oppPublicGroupMap.get(oppShare.OpportunityId).add(oppShare.UserOrGroupId);
                                                     }else{
                                                         List<Id> userOrGroupIds = new List<Id>();
                                                         userOrGroupIds.add(oppShare.UserOrGroupId);
                                                         oppPublicGroupMap.put(oppShare.OpportunityId, userOrGroupIds);
                                                     }
                                                 }
                                             }
        }
        
        //Get the Group Names
        if(grpNameSet.size()>0 || publicGroupIds.size()>0){
            for(Group grp: [SELECT Id, Name, Type from Group
                            Where ( Id IN :publicGroupIds 
                                   OR Name IN :grpNameSet ) 
                            AND type = 'Regular']){
                                publicGroupNameMap.put(grp.Id, grp.Name);
                            }
        }
        
        //Validate the deleted team members Territory's group matches in opportunity share
        //and collect oppIds and groupIds
        for(OpportunityTeamMember oppTeamMember: oppTeamMemberList){
            System.debug(oppPublicGroupMap);
            List<Id> grpIds = oppPublicGroupMap.get(oppTeamMember.OpportunityId);
            //String terrName = terrNameMap.get(oppTeamMember.Territory__c);
            String terrName = terrNameMap.get(userterrIdMap.get(oppTeamMember.UserId));
           // System.debug('Group Ids: ' + grpIds);
            if(grpIds!=null){
            for(Id grpId: grpIds){
                String grpName = publicGroupNameMap.get(grpId);
                if((terrName != '' && terrName != null) && terrName == grpName){
                    System.debug('terrName: ' + terrName + ' = grpName: ' + grpName );
                    delOppIds.add(oppTeamMember.OpportunityId);
                    delGrpIds.add(grpId);
                }
            }
            }
        }
        
        //Get the Id of Opportunity Share for deletion
        if(delOppIds.size()>0 && delGrpIds.size()>0){
            // Start of Code - MB - 06/13/17
            if(delOppShareIds.size()>0){
                delOppShareIds.clear();
            }
            // End of Code - MB - 06/13/17
            for(List<OpportunityShare> oppShareList : [SELECT Id, OpportunityId, UserOrGroupId FROM OpportunityShare
                                                       WHERE OpportunityId IN :delOppIds
                                                       AND UserOrGroupId IN :delGrpIds]){
                                                           for(OpportunityShare oppShare : oppShareList){
                                                               delOppShareIds.add(oppShare.Id);
                                                           }
                                                       }
            // Start of Code - MB - 06/13/17
            if(delOppShareIds.size()>0){
                UpdateOpportunityShareAccess updOppShareAccess = new UpdateOpportunityShareAccess();
                updOppShareAccess.DeleteOppShare();
            }
            // Removed Database.DeleteResult section and moved to private class
            // End of Code - MB - 06/13/17
        }
    }
    // Start of code SB - 9-26-17
    //  This is needed for SFDC to LDS interface, since can't add these fields using formula. 
    //  This code should be removed once SFDC->LDS interface is not in use . that is : Roll Out Phase is complete.
    public static void updateOppDetails(List<OpportunityTeamMember> oppTeamMemberList, Boolean updateflag){
        Set<Id> oppIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        Map<Id, Opportunity> oppIdMap 	= new Map<Id, Opportunity>();
        Map<Id, User> userIdGuidMap 	= new Map<Id, User>();
        User loggedInUsr = [Select CAMS_Employee_Number__c from User where Id  =: UserInfo.getUserId()];  // SB added 10-18-2017
        FOR(OpportunityTeamMember oppTeam: oppTeamMemberList){
            oppIds.add(oppTeam.OpportunityId);
            userIds.add(oppTeam.UserId);
        }
        
        FOR(Opportunity opp: [SELECT Id, SAP_Opportunity_GUID__c,RecordTypeId FROM Opportunity WHERE Id IN :oppIds]){
            oppIdMap.put(opp.Id, opp);
        }
        FOR(User usr: [SELECT Id, CAMS_Employee_Number__c FROM User WHERE Id IN :userIds]){
            userIdGuidMap.put(usr.Id, usr);
        }
        
        FOR(OpportunityTeamMember oppTeam: oppTeamMemberList){
            Opportunity opp = oppIdMap.get(oppTeam.OpportunityId);
            User usr		= userIdGuidMap.get(oppTeam.UserId);
            if(usr != null)//Added By LJ - 2/1/2018
            {
                if(updateflag && oppTeam.External_ID__c == null ) { 
                    oppTeam.SAP_Opportunity_Id__c 		= opp.SAP_Opportunity_GUID__c;
                    oppTeam.CAMS_Employee_ID__c   		= usr.CAMS_Employee_Number__c;//userIdGuidMap.get(oppTeam.UserId);
                    oppTeam.External_ID__c				= opp.Id + '_' + usr.Id;
                    oppTeam.Record_type_of_Oppt__c   	= Schema.SObjectType.Opportunity.getRecordTypeInfosById()
                        .get(opp.RecordTypeId).getname();
                    oppTeam.CAMS_Employee_ID_of_Logged_in_User__c = loggedInUsr.CAMS_Employee_Number__c;  // SB added 10-18-2017
                } else {
                    oppTeam.SAP_Opportunity_Id__c 		= opp.SAP_Opportunity_GUID__c;
                    oppTeam.CAMS_Employee_ID__c   		= usr.CAMS_Employee_Number__c;//userIdGuidMap.get(oppTeam.UserId);
                    oppTeam.External_ID__c				= opp.Id + '_' + usr.Id;
                    oppTeam.Record_type_of_Oppt__c   	= Schema.SObjectType.Opportunity.getRecordTypeInfosById()
                        .get(opp.RecordTypeId).getname();
                    oppTeam.CAMS_Employee_ID_of_Logged_in_User__c = loggedInUsr.CAMS_Employee_Number__c;    // SB added 10-18-2017 
                }
            }
        }
    }
    
    // End of code SB - - 9-26-17
    
    //Start of Code - MB - 2/26/18 - Store deleted opp. team members in custom object to send to HANA
    public static void insOppTeamMember(List<OpportunityTeamMember> delOppTeamMemberList){
        List<Opportunity_Team_Member__c> addOppTeamMemList = new List<Opportunity_Team_Member__c>();
        for(OpportunityTeamMember oppTeamMem: delOppTeamMemberList){
            Opportunity_Team_Member__c addOppTeamMem = new Opportunity_Team_Member__c();
            addOppTeamMem.Opportunity__c = oppTeamMem.OpportunityId;
            addOppTeamMem.Opportunity_Team_Member_Id__c = oppTeamMem.Id;
            addOppTeamMem.User__c = oppTeamMem.UserId;
            addOppTeamMemList.add(addOppTeamMem);
        }
        if(!addOppTeamMemList.isEmpty()){
            try{
                List<Database.SaveResult> results = Database.insert(addOppTeamMemList, false);
                for (Database.SaveResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Opp Team Member: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while inserting Opp Team Member: ' + err.getMessage());
                        }
                    }
                } 
            }Catch(Exception dmle){
                System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
            }
        }
    }
    //End of Code - MB - 2/26/18
    
    // Start of Code - MB - 06/13/17
    // Private Inner Class without sharing for users to update AccountShare access
    private without sharing class UpdateOpportunityShareAccess{
        //Start of code - MB - 08/04/2017
        public void InsertOppShareAccess(){
            try{
                List<Database.SaveResult> results = Database.insert(updOppShareList, false);
                for (Database.SaveResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Opportunity Share: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while inserting Opportunity Share ' + err.getMessage());
                        }
                    }
                } 
            }Catch(Exception dmle){
                System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
            }
        }//End of code - MB - 08/04/2017
        public void DeleteOppShare(){
            try{
                Database.DeleteResult[] results = Database.delete(delOppShareIds, false);
                for (Database.DeleteResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully deleted Opportunity Share: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while deleting Opportunity Share ' + err.getMessage());
                        }
                    }
                } 
            }
            catch (System.DMLException dmle){
                for(Integer i=0; i<dmle.getNumDml(); i++){
                    system.debug(dmle.getDmlMessage(i));
                }
            }
        }
    }
    // End of Code - MB - 06/13/17
    
    
}