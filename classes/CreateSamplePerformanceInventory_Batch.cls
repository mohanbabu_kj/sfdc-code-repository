/**************************************************************************

Name : createSamplePerformanceInventory_Batch

===========================================================================
Purpose : 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit         20/Nov/2018    Created          

***************************************************************************/
global class CreateSamplePerformanceInventory_Batch implements Database.Batchable<sObject>, Schedulable {
    public String query;
    public static Boolean error = false;
    
    global Database.QueryLocator start( Database.BatchableContext bc ) {
        Batch_Query__mdt[] queryList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'CreateSamplePerformanceInventory_Batch']; //default query 
        If(queryList.size() > 0) {
            query = queryList[0].Query_String__c;                
        } else {
            query = 'SELECT Id,Customer_Group_Number__c,Business_Type__c,RecordTypeId,Global_Account_Number__c FROM Account ' + 
                'WHERE SFDC_Display__c = true AND (LastModifiedDate = TODAY OR LastModifiedDate = LAST_N_DAYS:1) AND Business_Type__c = \'Residential\' AND Type = \'Invoicing\' ' ;
        }
        return Database.getQueryLocator( query );   
    }
    
    global void execute( Database.BatchableContext bc, List<Account> scope ) {
        List<Product_Customer_Group_Relationship__c>proCustomerList = new List<Product_Customer_Group_Relationship__c>();
        List<Sample_Display_Inventory__c> listsdi = new List<Sample_Display_Inventory__c>();
        List<Sample_Display_Inventory__c> listSampleProfInve = new List<Sample_Display_Inventory__c>();
        List<Sample_Display_Inventory__c> deleteSampleList = new List<Sample_Display_Inventory__c>();
        Set<String> accountids = new Set<String>();
        List<Id> dspiIds = new List<Id>();
        Set<String> cusGroupSet = new Set<String>();
        Map<String, String> prodBrandTypeMap = new Map<String, String>();
        Id invRTId = UtilityCls.getRecordTypeInfo('Account', 'Invoicing');
        for( Product_SFDC_Mapping__c prodBrandType : [SELECT Brand__c, Product_Type__c, Sample_Inventory_Category__c FROM Product_SFDC_Mapping__c] ){
            String brandType = prodBrandType.Brand__c + '_' + prodBrandType.Product_Type__c;
            prodBrandTypeMap.put( brandType, prodBrandType.Sample_Inventory_Category__c );
        }
        
        for ( Account ac : scope ) {
            if( ac.Customer_Group_Number__c != null && ac.Business_Type__c == UtilityCls.RESIDENTIAL && ac.RecordTypeId == invRTId ){
                accountids.add( ac.Id );   
                List<String> cusGroup = ac.Customer_Group_Number__c.split(';');
                cusGroupSet.addAll(cusGroup);
            }
        }
        
        if( !cusGroupSet.contains( '000000' ) ){
            cusGroupSet.add( '000000' );
        }
        
        Map<String,string>buyingGroupMap = new Map<String,string>();
        for( Buying_Group__c bg : [ SELECT Name,Buying_Group_Number__c From Buying_Group__c ] ){
            buyingGroupMap.put( bg.Buying_Group_Number__c,BG.Name );
        }
        system.debug( ':::::cusGroupSet::::' + cusGroupSet );
        if( !accountids.isEmpty() ){
            string recordTypeId18 = [ Select Id From RecordType  Where SobjectType =: 'PRODUCT2' and DeveloperName =:  UtilityCls.REDIDENTALDISPLAY ].id;
            string recotdTypeId = recordTypeId18.substring(0,15);            
            proCustomerList = [ SELECT id,
                               Product__c,
                               Customer_Group_Number__c,
                               Product__r.Residential_Product_Unique_Key__c,
                               Product__r.Brand_Code__c,
                               Product__r.ERP_Product_Type__c,
                               Product__r.Brand_Description__c,
                               Product__r.Name
                               FROM Product_Customer_Group_Relationship__c
                               WHERE Is_Product_Active__c = true AND
                               Product_Record_Type__c =: recotdTypeId AND
                               Product__r.Trackable_Display_asset__C = true AND  
                               Customer_Group_Number__c IN : cusGroupSet                                     
                              ];
            
            listSampleProfInve = [ Select Id,
                                  Customer_Group_Number__c
                                  from Sample_Display_Inventory__c
                                  Where Placement_Status__c != 'In Store' AND
                                  Customer_Group_Number__c != '000000' AND // Added by MB - Bug 70817 - 2/15/19
                                  Account__c IN : accountids Limit 50000
                                 ]; 
            
            system.debug( ':::::proCustomerList::::' + proCustomerList );
            for( Account ac : scope ){
                for( Product_Customer_Group_Relationship__c p : proCustomerList ){
                    if(( ac.Customer_Group_Number__c != null && ac.Customer_Group_Number__c.contains( p.Customer_Group_Number__c ))||  
                       (!(ac.Customer_Group_Number__c.contains('000000')) && p.Customer_Group_Number__c =='000000')){
                           Sample_Display_Inventory__c sdi = new Sample_Display_Inventory__c();
                           String brandType = '';
                           sdi.Account__c = ac.Id;
                           sdi.Inventory__c = p.Product__c;
                           sdi.RecordTypeId = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get(UtilityCls.SAMPLEREDIDENTALDISPLAY).getRecordTypeId();
                           sdi.Customer_Group__c = buyingGroupMap.get( p.Customer_Group_Number__c );
                           sdi.Customer_Group_Number__c = p.Customer_Group_Number__c;
                           //sdi.Placement_Status__c = UtilityCls.Not_In_Store; - Commented by MB - Bug 71599 - 3/14/19
                           sdi.External_Id__c = ac.Global_Account_Number__c +'_'+ p.Product__r.Residential_Product_Unique_Key__c;
                           sdi.Name = p.Product__r.Name; 
                           brandType = p.Product__r.Brand_Code__c + '_' + p.Product__r.ERP_Product_Type__c ;
                           sdi.Sample_Inventory_Category__c = ( prodBrandTypeMap != null && prodBrandTypeMap.containsKey(brandType) ? prodBrandTypeMap.get(brandType) : '' );
                           listsdi.add( sdi );
                       }
                }
            }
        }
        if( listSampleProfInve.size() > 0 ) {
            for(Account acc: scope){
                for( Sample_Display_Inventory__c spi : listSampleProfInve ){
                    if(spi.Customer_Group_Number__c != '000000' && !acc.Customer_Group_Number__c.contains( spi.Customer_Group_Number__c )){
                        deleteSampleList.add(spi);
                        dspiIds.add( spi.Id );
                    }
                }
            }
        }
        
        if( listsdi.size()>0 ){
                List<Database.UpsertResult> results = Database.upsert(listsdi, Sample_Display_Inventory__c.Field.External_Id__c , false); //.upsert( listsdi, false ); - Modified upsert method to include external Id - MB - Bug 70817 - 2/15/19
                UtilityCls.createUpsertDMLexceptionlog(listsdi,results,'CreateSamplePerformanceInventory_Batch','createSamplePerformanceInventory');
                for( Database.UpsertResult result: results ){
                    if( !result.isSuccess() ){
                        error = true;
                        /*for( Account ac : scope ){
                            if( ac.Id == result.getId() ){
                                for( Database.Error err: result.getErrors() ){
                                    //ac.addError( err.getMessage() + ' : ' + err.getFields() );
                                }
                            }
                        }*/
                    }
                }
        }
        if(dspiIds.size()>0){
            //database.delete( dspiIds );
            /* Start of Code - MB - Bug 68526 - 12/11/18 */
            List<Database.DeleteResult> dResults = Database.delete( dspiIds, false );
            UtilityCls.createDeleteDMLexceptionlog(deleteSampleList, dspiIds,dResults,'CreateSamplePerformanceInventory_Batch','deleteSamplePerformanceInventory');
            for( Database.DeleteResult result: dResults ){
                if( !result.isSuccess() ){
                    error = true;
                }
            }
            /* End of Code - MB - Bug 68526 - 12/11/18 */
        }
    }
    
    
    global void finish(Database.BatchableContext BC) {
        /* Start of Code - MB - Bug 68526 - 12/11/18 */
        if(error){
            String body = '';
            List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';');  
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            body = 'Hello All, <br /><br />';
            body += 'Please check Application log for failed records from CreateSamplePerformanceInventory_Batch class.<br />';
            String subject = 'Error Occurred in CreateSamplePerformanceInventory_Batch';
            email.setSubject(subject);
            email.setToAddresses(emailAddressList);
            email.setHtmlBody(body);
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
        /* End of Code - MB - Bug 68526 - 12/11/18 */
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
        //Database.executeBatch(new CreateSamplePerformanceInventory_Batch());
        // Start of Code - MB - Bug 71599 - 3/14/19 
        Batch_Query__mdt[] queryList = [SELECT Batch_Size__c FROM Batch_Query__mdt where Class_Name__c = 'CreateSamplePerformanceInventory_Batch']; //default query 
        if(queryList != null && queryList.size()>0 && queryList[0].Batch_Size__c != null && queryList[0].Batch_Size__c != 0){
            Integer batchSize = (Integer)queryList[0].Batch_Size__c;
            System.debug('batchSize: ' + batchSize);
            Database.executeBatch(new CreateSamplePerformanceInventory_Batch(), batchSize);
        }else{
            Database.executeBatch(new CreateSamplePerformanceInventory_Batch(), 50);
        }
        // End of Code - MB - Bug 71599 - 3/14/19 
    }
}