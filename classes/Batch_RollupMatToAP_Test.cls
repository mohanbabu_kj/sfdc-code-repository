@isTest
private class Batch_RollupMatToAP_Test {
    static List<Account_Profile__c> accProfile;
    static List<Territory__c> terrList;
    static List<Territory_User__c>  terrUser;
    static List<Account> resAccListForInvoicing;
    
    private static void init(){   
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        MAT_Batch_Setting__c setting = new MAT_Batch_Setting__c();
            setting.Last_Run_At__c = SYstem.today().adddays(-5);
            setting.MinutesDelay__c = 5;
            setting.Name='Batch_RollupMatToAP';
            insert setting;
    }
    
    static testMethod void testMethod1() {
        test.startTest();
            init();
            //Inserting Account Profile
            accProfile = Utility_Test.createAccountProfile(false, 1, 'R.900124', false);
            accProfile[0].multi_channel__c=true;
            accProfile[0].retail__c=100;
            accProfile[0].builder__c=100;
            accProfile[0].Multi_Family__c=100;
            insert accProfile;
            
            //Inserting Territory
            terrList = Utility_Test.createTerritories(false, 1);
            for(Integer i=0;i<terrList.size();i++){
                terrList[i].Name = 'Testterr'+i;
                terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
            }            
            insert terrList;
            
            //Inserting Territory User
            terrUser = Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
            
            //Inserting Account
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for( integer i = 0; i < resAccListForInvoicing.size(); i++ ){
                resAccListForInvoicing[i].Chain_Name__c = 'R.900124';
                resAccListForInvoicing[i].Business_Type__c = 'Commercial';
                resAccListForInvoicing[i].Territory__c = terrList[i].Id;
            }
            insert resAccListForInvoicing;
            
            //Inserting MAT
            Mohawk_Account_Team__c moht = new Mohawk_Account_Team__c(
                                            Account__c = resAccListForInvoicing[0].Id,
                                            User__c = Utility_Test.ADMIN_USER.id,
                                            Brands__c = 'Karastan;Aladdin;Horizon',
                                            Product_Types__c='Carpet',
                                            Territory__c = terrList[0].Id,
                                            Actuals_Sent_from_BI__c=true,
                                            Account_Profile__c=accProfile[0].id,
                                            RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId()
                                        );
            insert moht;
            user intregrationUser = [ SELECT Id,Name,Email, ProfileId FROM User WHERE Profile.Name = 'Integration User' AND IsActive=true  and Name = 'Integration User' LIMIT 1 ];
            System.runAs( intregrationUser ) {
                moht.Brands__c = 'Aladdin;Horizon';
                update moht;
            }
             
            Batch_RollupMatToAP  cls = new Batch_RollupMatToAP(true,false);            
            string jobId=database.executeBatch(cls,10);
            system.assert( jobId != null );
            String sch = '0 0 23 * * ?';
            system.schedule('Test SCH Check', sch, cls); 
        test.stopTest();
    }
    
    static testMethod void testMethod2() {
        test.startTest();
            init();
            //Inserting Account Profile
            accProfile = Utility_Test.createAccountProfile(false, 1, 'R.900124', false);
            accProfile[0].multi_channel__c=true;
            accProfile[0].retail__c=100;
            accProfile[0].builder__c=100;
            accProfile[0].Multi_Family__c=100;
            insert accProfile;
            
            //Inserting Territory
            terrList = Utility_Test.createTerritories(false, 1);
            for(Integer i=0;i<terrList.size();i++){
                terrList[i].Name = 'Testterr'+i;
                terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
            }            
            insert terrList;
            
            //Inserting Territory User
            terrUser = Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
            
            //Inserting Account
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for( integer i = 0; i < resAccListForInvoicing.size(); i++ ){
                resAccListForInvoicing[i].Chain_Name__c = 'R.900124';
                resAccListForInvoicing[i].Business_Type__c = 'Commercial';
                resAccListForInvoicing[i].Territory__c = terrList[i].Id;
            }
            insert resAccListForInvoicing;
            
            //Inserting MAT
            Mohawk_Account_Team__c moht = new Mohawk_Account_Team__c(
                                            Account__c = resAccListForInvoicing[0].Id,
                                            User__c = Utility_Test.ADMIN_USER.id,
                                            Brands__c = 'Karastan;Aladdin;Horizon',
                                            Product_Types__c='Carpet',
                                            Territory__c = terrList[0].Id,
                                            Actuals_Sent_from_BI__c=true,
                                            Account_Profile__c=accProfile[0].id,
                                            RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId()
                                        );
            insert moht;
            user intregrationUser = [ SELECT Id,Name,Email, ProfileId FROM User WHERE Profile.Name = 'Integration User' AND IsActive=true  and Name = 'Integration User' LIMIT 1 ];
            System.runAs( intregrationUser ) {
                moht.Brands__c = 'Aladdin;Horizon';
                update moht;
            }
             
            Batch_RollupMatToAP  cls = new Batch_RollupMatToAP(false,true);           
            string jobId=database.executeBatch(cls,10);
            system.assert( jobId != null );
        test.stopTest();
    }
}