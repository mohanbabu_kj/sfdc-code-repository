/**************************************************************************

Name : AccountProfileShare_Batch_Test 
===========================================================================
Purpose : Uitlity class to AccountProfileShare_Batch test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik     3/May/2017      Create  
***************************************************************************/
@isTest
private class AccountProfileShare_Batch_Test {
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    static List<Territory_User__c>  terrUser;
    static List<Territory_User__c>  terrUser1;
    static List<Account_Profile__c> accprofile;
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    @TestSetup
    public static void init(){   
        Utility_Test.getTeamCreationCusSetting();
        Utility_Test.getDefaultConfCusSetting();
        //  Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        AP_Trigger_Settings__c APSC1 = new AP_Trigger_Settings__c();
        APSC1.Name = 'Chain Number Exclude';
        APSC1.Exclude_Chain_Numbers_1__c = '10';
        APSC1.Exclude_Chain_Numbers_2__c = '13';
        insert APSC1;
    }
    
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
        //    init();
            
            terrList = Utility_Test.createTerritories(false, 2);
            for(Territory__c ter : terrList){
                ter.type__c='Builder / Multi-Family';
            }
            insert terrList;
            User u=Utility_Test.getTestUser('TestUser',null);
            insert u;
            List<Territory_User__c> terUsers=Utility_Test.createTerritoryWithUsers(false,terrList,u,2);
            for(Territory_User__c tu :terUsers){
                tu.role__c='Sales Vice President';
            }
            insert terUsers;
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Retail');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c=aplist[0].id;
            }
            insert resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
            /***END Creating Account Profile Setting and Account Profile****/
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                mat.User__c=u.id;
            }
            insert mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Aladdin;Karastan';
                mohAccTeam[i].Product_Types__c = 'Carpet;Cushion';
            }
            update mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Horizon;Hard Surface;Portico';
                mohAccTeam[i].Product_Types__c = 'Hardwood; Laminate; Tile; Resilient';
            }
            update mohAccTeam;
            
            system.assert(mohAccTeam!=null);
            
            
            AccountProfileShare_Batch  cls = new AccountProfileShare_Batch ();
           // cls.query='SELECT Id, territory__c, Account__r.Account_Profile__c, User__c, Account__r.Account_Profile__r.ownerId FROM Mohawk_Account_Team__c WHERE Account__r.Account_Profile__c != null AND User__c != null';
            String jobIdShare=Database.executeBatch(cls);
            
            
            System.assert(jobIdShare != null);
            
            test.stopTest();
            
        }
        
    }
}