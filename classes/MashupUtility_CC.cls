public without sharing class MashupUtility_CC {

	/*public static boolean validateProfileName(String str){
		String profileName = [SELECT Name from Profile where id =: UserInfo.getprofileId()].Name;
		if(profileName.contains(str)){
			return true;
		}
		return false;
	}*/

	public static boolean isRecordTypeAvailOnObject(String qryObject) {
		if(qryObject == 'Account' || qryObject == 'Contact' || qryObject == 'Product2' || qryObject == 'Opportunity' || qryObject == 'Order')
			return true;
		else
			return false;
	}

	public static String buildQueryStringForMashupUrl(Mashup_Management__c data, String finalUrl, SObject objectData, Map<String,String> paramList) {
      String newUrl = finalUrl;
      String queryString = data.Query_String__c;
      String newValue = '';

      if(UtilityCls.isStrNotNull(finalUrl) == false)
          return '';

      if(UtilityCls.isStrNotNull(queryString)){
          if(data.No_Query_String__c){
              System.debug('queryString initial ' + queryString);
              String urlTemp = newUrl;
              List<String> qryStrArr = queryString.split(';;');
              for(String str : qryStrArr) {
                  if(UtilityCls.isStrNotNull(str)){
                      List<String> valueArr = str.split('=');
                      if(UtilityCls.isStrNotNull(valueArr[0]) && urlTemp.Contains(valueArr[0])){
                        System.debug('object' + objectData);
                        System.debug('valueArr' + valueArr);
                          newValue = (valueArr[1]!='RECORD_ID' && valueArr[1]!='recordId' ? (String)objectData.get(valueArr[1]) : paramList.get(valueArr[1])); 
                          newValue = (UtilityCls.isStrNotNull(newValue) ? newValue : 'NO_'+valueArr[0]);
                          urlTemp = urlTemp.replace(valueArr[0], newValue);
                      }
                  }
              }
              newUrl = urlTemp;
          } else {
              List<String> qryStrArr = queryString.split('&');
              for(String str : qryStrArr) {
                  if(UtilityCls.isStrNotNull(str)) {
                      List<String> valueArr = str.split('=');
                      if(UtilityCls.isStrNotNull(valueArr[0]) && UtilityCls.isStrNotNull(valueArr[1])){
                          if(UtilityCls.hasSObjectField(valueArr[1], objectData)){
                              queryString = queryString.replace(str, valueArr[0] + '=' + (String)objectData.get(valueArr[1]));
                          }
                      }
                  }
              }
              if(newUrl.contains(queryString)==false){
                  if(newUrl.Contains('?'))
                      newUrl += '&' + queryString;
                  else
                      newUrl += '?' + queryString;
              }
          }
      }
      System.debug('build url:' + data.Display_Name__c + ' = ' + newUrl);

      return newUrl;

  }

	public static String getFinalUrl(SObject objectData, Mashup_Management__c data, Boolean isResidential, Boolean isCommercial) {
        String finalUrl = '';

        if(data.Profile__c == 'All')
            finalUrl = data.Target_URL__c;
        else if(isCommercial && data.Profile__c == UtilityCls.COMMERCIAL)
            finalUrl = data.Target_URL__c;
        else if(isResidential && data.Profile__c == UtilityCls.RESIDENTIAL)
            finalUrl = data.Target_URL__c;

        // to handle null value on non commercial/residential profiles and display commercial record 04/18/2017
        if(UtilityCls.isStrNotNull(finalUrl) == false && !isResidential && !isCommercial)// && data.Profile__c == UtilityCls.COMMERCIAL
             finalUrl = data.Target_URL__c;

        if(UtilityCls.isStrNotNull(finalUrl)){
            finalUrl = MashupUtility_CC.buildQueryStringForMashupUrl(data, finalUrl, objectData, new Map<String,String>());
        }

        return finalUrl;
    }

	public static Map<String, Map<String,list<Mashup_Management__c>>> getRecordsByMashupType(SObject objectData, String mashupType, String objectName, String currentRecordType, Boolean isResidential, Boolean isCommercial) {
        Integer recordsCount = 0;

        String groupName = '';
        String typeName = '';
        String finalUrl = '';        

        if(objectName =='Product2')
            objectName =  'Product';
        else if(objectName =='Opportunity')
            objectName =  'Project';
        else if(objectName.contains('Order'))
            objectName =  'Order';

        Map<String,String> groupCleanStr = new Map<String,String>();

        Map<String, Map<String,list<Mashup_Management__c>>> groupMap = new Map<String, Map<String,list<Mashup_Management__c>>>();

        System.debug('mashups' + Mashup_Management__c.getAll().values());
        
        for(Mashup_Management__c data :Mashup_Management__c.getAll().values()){
            // System.debug('data-mashup' + data);

            typeName = data.Type__c;
            finalUrl = '';

            Set<String> objectList = new Set<String>();
            objectList.add(data.Object__c);

            if(typeName == mashupType && objectList.contains(objectName) && data.Is_Active__c == true && data.Is_Publisher_Action__c == false
                && (data.Object_Record_Type__c == 'All' || data.Object_Record_Type__c == currentRecordType)
                ){
                // System.debug(data);

                finalUrl = getFinalUrl(objectData, data, isResidential, isCommercial);
                // System.debug('finalUrl' + finalUrl);
                if(UtilityCls.isStrNotNull(finalUrl)){
                    // System.debug('finalUrl-in' + finalUrl);
                    data.Target_URL__c = finalUrl;
                    recordsCount++;

                    groupName = data.Group__c;
                    if(UtilityCls.isStrNotNull(groupName)==false)
                        groupName = 'General';
                    groupCleanStr.put(groupName, groupName.replaceAll('[0-9]{1,}|\\.', '').trim());// 05/03/2017

                    if(!groupMap.containsKey(typeName))
                        groupMap.put(typeName, new Map<String,list<Mashup_Management__c>>());

                    if(groupMap.get(typeName).containsKey(groupName)){
                        groupMap.get(typeName).get(groupName).add(data);
                    }else{
                        list<Mashup_Management__c> mmList = new list<Mashup_Management__c>();
                        mmList.add(data);
                        groupMap.get(typeName).put(groupName, mmList);
                    }
                }

            }
        }

        return groupMap;

    }
}