/**************************************************************************

Name : MyAccountPlanTriggerHandler_Test

===========================================================================
Purpose : This class is used for MyAccountPlanTriggerHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE              DETAIL          DESCRIPTION
1.0        Lester         28/April/2017     Created 
1.1        Susmitha       07/Nov/2017       Modified
***************************************************************************/
@isTest
private class MyAccountPlanTriggerHandler_Test { 
    static List<Account> accForInvoicingList; 
    static List<My_Account_Plan__c> myAccPlanList;
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        Default_Configuration__c df = new Default_Configuration__c(Account_Team_Access__c = 'Read',
                                                                   Opportunity_Team_Access__c = 'Read',
                                                                   Residential_Invoicing_Roles__c ='Regional Vice President,Sales Vice President, District Manager',
                                                                   Residential_Non_Invoicing_Roles__c = 'Regional Vice President,Sales Vice President',
                                                                   SAL_Residential_Roles__c = 'District Manager,Regional Vice President,Senior Vice President',
                                                                   Groups__c = 'Strategic_Account_Managers;Admin;Commercial;Residential', // Added by MB - 07/10/17
                                                                   Fire_Account_Trigger__c = true,
                                                                   Fire_Lead_Trigger__c = true,
                                                                   Fire_MohawkAccountTeam_Trigger__c = true,
                                                                   Fire_Opportunity_Trigger__c = true,
                                                                   Residential_Sales_Ops__c = 'test',
                                                                   Commercial_Sales_Ops__c = 'test',
                                                                   Fire_RelatedAccount_Trigger__c = true,
                                                                   Fire_TerritoryUser_Trigger__c = true,
                                                                   Fire_Event_Trigger__c = true, // Added by MB - 07/10/17
                                                                   Opportunity_Team_Member_Trigger__c = true,
                                                                   Healthcare_Sr_Living__c = 'Healthcare;Hospitality;Mixed Use;Public Spaces;Senior Living',
                                                                   Workplace_Retail__c ='Corporate;Faith Base;CRE/TI;Retail',
                                                                   Education_Govt__c = 'Government;Higher Education;K12;Higher Ed');
        
        insert df;
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            accForInvoicingList = Utility_Test.createResidentialAccountsForInvoicing(true, 2);
            
            myAccPlanList =  Utility_Test.createMyAccountPlan(false, 2);
            for(Integer i=0;i<myAccPlanList.size();i++) {
                myAccPlanList[i].Account__c = accForInvoicingList.get(0).Id;
                myAccPlanList[i].Status__c = 'Approved';
                myAccPlanList[i].Segments__c='Healthcare';
            }
            
            insert myAccPlanList;
            system.assert(myAccPlanList.size()>0);
            
            My_Account_Plan__c myAccPlan1 = myAccPlanList.get(0);
            myAccPlan1.Goal_Opportunity__c = 200;
            myAccPlan1.Segments__c='Senior Living';
            myAccPlan1.Account__c = accForInvoicingList.get(0).Id;
            //update myAccPlan1;
            
            My_Account_Plan__c myAccPlan2 = myAccPlanList.get(0);
            myAccPlan2.Segments__c='Government';
            myAccPlan2.Account__c = accForInvoicingList.get(0).Id;
            
            update myAccPlanList;
            
            myAccPlan1.Goal_Opportunity__c = 200;
            myAccPlan1.Segments__c='Faith Base';
            myAccPlan2.Segments__c='Mixed Use';
            
            update myAccPlanList;
            
            delete myAccPlanList;
            undelete myAccPlanList;
            
            System.assert( myAccPlanList != null );
            test.stopTest();    
        }
    } static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            accForInvoicingList = Utility_Test.createResidentialAccountsForInvoicing(true, 2);
            
            myAccPlanList =  Utility_Test.createMyAccountPlan(false, 2);
            for(Integer i=0;i<myAccPlanList.size();i++) {
                myAccPlanList[i].Account__c = accForInvoicingList.get(0).Id;
                myAccPlanList[i].Status__c = 'Not Submitted';
                myAccPlanList[i].Unlock__c=True;
                myAccPlanList[i].Segments__c='Healthcare';
                myAccPlanList[i].Goal_Opportunity__c=100;
            }
            
            insert myAccPlanList;
            system.assert(myAccPlanList.size()>0);
            
            My_Account_Plan__c myAccPlan1 = myAccPlanList.get(0);
            myAccPlan1.Goal_Opportunity__c = 200;
            myAccPlan1.segments__c='Government';
            update myAccPlan1;
            
            My_Account_Plan__c myAccPlan2 = myAccPlanList.get(1);
            //myAccPlan2.Account__c = accForInvoicingList.get(1).Id;
            update myAccPlan2;
            
            
            MyAccountPlanTriggerHandler.updateAccountPlan(myAccPlanList);
            delete myAccPlanList;
            // undelete myAccPlanList;
            
            System.assert( myAccPlanList != null );
            test.stopTest();    
        }
    }
    static testMethod void testMethod3() {
        
        test.startTest();
        init();
        
        
        User mgr = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()),
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US',
            LocaleSidKey    = 'en_US',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id,
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert mgr;
        User ur =Utility_Test.ADMIN_USER;
        ur.managerId=mgr.id;
        update ur;
        System.runAs(ur) {
            
            accForInvoicingList = Utility_Test.createResidentialAccountsForInvoicing(true, 2);
            for(account a: accForInvoicingList){
                a.Total_Account_Plan_Goal__c=-1;
                a.Hc_Acc_Plan_Goal__c=10;
            }
            update accForInvoicingList;
            myAccPlanList =  Utility_Test.createMyAccountPlan(false, 2);
            for(Integer i=0;i<myAccPlanList.size();i++) {
                myAccPlanList[i].Account__c = accForInvoicingList.get(0).Id;
                myAccPlanList[i].Status__c = 'Approved';
                if(i==1)
                    myAccPlanList[i].Status__c = 'Sent for Approval';
                
                myAccPlanList[i].Unlock__c=True;
                myAccPlanList[i].Segments__c='Healthcare';
                myAccPlanList[i].Fiscal_Year__c=String.valueOf(Date.Today().Year());
                //yAccPlanList[i].Approver__c=ur.id;
                
                
            }
            
            insert myAccPlanList;
            system.assert(myAccPlanList.size()>0);
            
            system.debug('approver'+myAccPlanList[0].approver__c);
            
            My_Account_Plan__c myAccPlan1 = myAccPlanList.get(0);
            myAccPlan1.Goal_Opportunity__c = 200;
            update myAccPlan1;
            
            My_Account_Plan__c myAccPlan2 = myAccPlanList.get(1);
            // myAccPlan2.Account__c = accForInvoicingList.get(1).Id;
            update myAccPlan2;
            
             My_Account_Plan__c myAccPlan3 =new My_Account_Plan__c();
            myAccPlan3.Status__c = 'Not Submitted';
            myAccPlan3.Unlock__c=true;
            myAccPlan3.OwnerId=userinfo.getUserId();
            insert myAccPlan3;
            myAccPlanList.add(myAccPlan3);
            
            My_Account_Plan__c myAccPlan4 =new My_Account_Plan__c();
            myAccPlan4.Status__c = 'Sent for Approval';
            myAccPlan4.Unlock__c=true;
                myAccPlan4.Fiscal_Year__c=String.valueOf(Date.Today().Year());

           /// myAccPlan4.OwnerId=userinfo.getUserId();
            insert myAccPlan4;
            myAccPlanList.add(myAccPlan4);
            
 MyAccountPlanTriggerHandler.updateAccountPlan(myAccPlanList);
            System.assert( myAccPlan2 != null );
            
        }
        System.runAs(mgr) {
            MyAccountPlanTriggerHandler.updateAccountPlan(myAccPlanList);}
        delete myAccPlanList;
        undelete myAccPlanList;
        test.stopTest();    
        
    }static testMethod void testMethod4() {
        
        test.startTest();
        init();
        
        
        User mgr = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()),
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US',
            LocaleSidKey    = 'en_US',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id,
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert mgr;
        User ur =Utility_Test.ADMIN_USER;
        ur.managerId=mgr.id;
        update ur;
        System.runAs(ur) {
            
            accForInvoicingList = Utility_Test.createResidentialAccountsForInvoicing(true, 2);
            for(account a: accForInvoicingList){
                a.Total_Account_Plan_Goal__c=-1;
            }
            update accForInvoicingList;
            myAccPlanList =  Utility_Test.createMyAccountPlan(false, 2);
            for(Integer i=0;i<myAccPlanList.size();i++) {
                myAccPlanList[i].Account__c = accForInvoicingList.get(0).Id;
                myAccPlanList[i].Status__c = 'Sent for Approval';
                myAccPlanList[i].Unlock__c=True;
                myAccPlanList[i].Segments__c='Healthcare';
                
            }
            
            insert myAccPlanList;
            system.assert(myAccPlanList.size()>0);
            
            system.debug('approver'+myAccPlanList[0].approver__c);
            
            My_Account_Plan__c myAccPlan1 = myAccPlanList.get(0);
            myAccPlan1.Goal_Opportunity__c = 200;
            update myAccPlan1;
            
            My_Account_Plan__c myAccPlan2 = myAccPlanList.get(1);
            // myAccPlan2.Account__c = accForInvoicingList.get(1).Id;
            update myAccPlan2;
            
            System.assert( myAccPlan2 != null );
            
        }
        System.runAs(mgr) {
            MyAccountPlanTriggerHandler.updateAccountPlan(myAccPlanList);}
        
        delete myAccPlanList;
        undelete myAccPlanList;
        test.stopTest();    
        
    }
    static testMethod void testMethod5() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            accForInvoicingList = Utility_Test.createResidentialAccountsForInvoicing(true, 2);
            
            myAccPlanList =  Utility_Test.createMyAccountPlan(false, 2);
            for(Integer i=0;i<myAccPlanList.size();i++) {
                myAccPlanList[i].Account__c = accForInvoicingList.get(0).Id;
                myAccPlanList[i].Status__c = 'Approved';
                myAccPlanList[i].Segments__c='Mixed Use';
            }
            
            insert myAccPlanList;
            system.assert(myAccPlanList.size()>0);
            
            My_Account_Plan__c myAccPlan1 = myAccPlanList.get(0);
            myAccPlan1.Goal_Opportunity__c = 200;
            myAccPlan1.Segments__c='Retail';
            myAccPlan1.Account__c = accForInvoicingList.get(0).Id;
            //update myAccPlan1;
            
            My_Account_Plan__c myAccPlan2 = myAccPlanList.get(0);
            myAccPlan2.Segments__c='Senior Living';
            myAccPlan2.Account__c = accForInvoicingList.get(0).Id;
            
            update myAccPlanList;
            
            myAccPlan1.Goal_Opportunity__c = 200;
            myAccPlan1.Segments__c='Mixed Use';
            myAccPlan2.Segments__c='Mixed Use';
            
            update myAccPlanList;
            System.assert( myAccPlanList != null );
            
            test.stopTest();    
        }
    }
}