/**************************************************************************

Name : DiaplsySAL_CC

===========================================================================
Purpose : This tess class is used for DisplaySAL_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         3/March/2017     Created 
2.0        Lester         4/May/2017       Updated
***************************************************************************/
@isTest
private class DisplaySAL_CC_Test { 
    static List<Account> resAccListForInvoicing;
    static List<Account> resAccListForNonInvoicing;
    static List<Territory__c> terrList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Territory_User__c> terrUsers;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    
    static List<Account> comsAccListForInvoicing;
    static List<Action_List__c> actListForCommercialFL;
    static List<Action_List__c> actListForCommercialSAL;
    static List<Opportunity> oppList;
    static List<Event_Action__c> eventActionList;
    static List<Event> eventList;
    static List<My_Appointment_List__c> myAppList;
    static User resUser = Utility_Test.RESIDENTIAL_USER;
    static User comUser = Utility_Test.COMMERCAIL_USER;
    
    public static void init(){
        
        Utility_Test.getTeamCreationCusSetting();
    }
    
    static testMethod void testResidentialAccount() {
        
        
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            //2017/5/8 only add one method for this to get DefaultConfCusSetting. 
            // System.DmlException: Insert failed. First exception on row 0; first error: UNABLE_TO_LOCK_ROW, unable to obtain exclusive access to this record: []
            Utility_Test.getDefaultConfCusSetting();
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            terrList = Utility_Test.createTerritories(true, 2);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
            actListForResidentialFL = Utility_Test.createActionListForResidentialFL(true, 1);
            actListForResidentialSAL = Utility_Test.createActionListForResidentialSAL(true, 1);
            actListForResidential.add(actListForResidentialFL.get(0));
            actListForResidential.add(actListForResidentialSAL.get(0));
            
            salTerr = Utility_Test.createSALTerritory(true, 2, actListForResidential, terrList);
            List<String> brandlist = new List<String>{'201 - Southeast', '204 - Central Plains'};
                List<String> seglist = new List<String>{'Soft Surface', '   Hard Surface'};
                    String id = resAccListForInvoicing.get(0).id;
            
            DisplaySAL_CC.getObjectNameById(id);
            DisplaySAL_CC.retrieveSALs(id, brandlist, seglist);
            DisplaySAL_CC.retrieveSALs(Utility_Test.ADMIN_USER.Id, brandlist, seglist);
            DisplaySAL_CC.retrieveSALs(id, null, seglist);
            DisplaySAL_CC.retrieveSALs(id, brandlist, null);
            system.assert(brandlist!=null);
            
            
            DisplaySAL_CC.getObjectRecord('Account', id, 'Id');
            test.stopTest();
        }
        
    }
    
    static testMethod void testNonMHKTeamCase() {
        
        
        test.startTest();
        
        init();
        List<String> brandlist = new List<String>{'201 - Southeast', '204 - Central Plains'};
            List<String> seglist = new List<String>{'Soft Surface', '   Hard Surface'};
                Utility_Test.getDefaultConfCusSetting();
        
        System.runAs(Utility_Test.RESIDENTIAL_USER) {
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        }
        
        System.runAs(Utility_Test.ADMIN_USER) {
            
            List<Mohawk_Account_Team__c> mohAccTeamList = [Select  Id, Brands__c, User__c From Mohawk_Account_Team__c Where Account__c =: resAccListForInvoicing.get(0).Id];
            //mohAccTeamList[0].Brands__c = 'Aladdin';
            List<Mohawk_Account_Team__c> mTeamUpdate = new List<Mohawk_Account_Team__c>();
            for(Mohawk_Account_Team__c mUser : mohAccTeamList) {
                mUser.Brands__c = 'Aladdin';
                mTeamUpdate.add(mUser);
            }
            upsert mTeamUpdate;
            
            // resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            terrList = Utility_Test.createTerritories(true, 2);
            
            mohAccTeam = new List<Mohawk_Account_Team__c> ();
            mohAccTeam.add(new Mohawk_Account_Team__c(Account__c = resAccListForInvoicing[0].Id, User__c = Utility_Test.COMMERCAIL_USER.Id, Brands__c = 'Aladdin;Horizon',
                                                      Territory__c = terrList[0].Id));
            /*mohAccTeam.add(new Mohawk_Account_Team__c(Account__c =  resAccListForInvoicing[0].Id, User__c = Utility_Test.RESIDENTIAL_USER.Id, Brands__c = 'Aladdin;Horizon',
Territory__c = terrList[1].Id));*/
            
            insert mohAccTeam;
            system.assert(mohAccTeam!=null);
            
            terrUsers = new List<Territory_User__c>();
            
            terrUsers.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Business Development Manager', User__c = Utility_Test.ADMIN_USER.Id));
            terrUsers.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Business Development Manager', User__c = Utility_Test.RESIDENTIAL_USER.Id));
            terrUsers.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Business Development Manager', User__c = Utility_Test.COMMERCAIL_USER.Id));
            insert terrUsers;
            
            actListForResidentialFL = Utility_Test.createActionListForResidentialFL(true, 2);
            actListForResidentialSAL = Utility_Test.createActionListForResidentialSAL(true, 1);
            actListForResidential.add(actListForResidentialFL.get(0));
            actListForResidential.add(actListForResidentialSAL.get(0));
            
            salTerr = new List<SAL_Territory__c>();
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialFL[0].Id, Territory__c = terrList[0].Id)); 
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialFL[1].Id, Territory__c = terrList[0].Id));   
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialSAL[0].Id, Territory__c = terrList[0].Id));   
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialSAL[0].Id, Territory__c = terrList[1].Id));   
            insert salTerr;
            
            String id = resAccListForInvoicing.get(0).id;
            system.assert(resAccListForInvoicing!=null);
            
            DisplaySAL_CC.retrieveSALs(id, brandlist, seglist);
        }
        test.stopTest();
        
    }
    
    static testMethod void testResidentialAccount2() {
        
        
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            
            Utility_Test.getDefaultConfCusSetting();
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            terrList = Utility_Test.createTerritories(true, 2);
            String id = resAccListForInvoicing.get(0).id;
            DisplaySAL_CC.retrieveSALs(id, null, null);
            
            mohAccTeam = new List<Mohawk_Account_Team__c> ();
            mohAccTeam.add(new Mohawk_Account_Team__c(Account__c = resAccListForInvoicing[0].Id, User__c = Utility_Test.ADMIN_USER.Id, Brands__c = 'Aladdin;Horizon',
                                                      Territory__c = terrList[0].Id));
            mohAccTeam.add(new Mohawk_Account_Team__c(Account__c =  resAccListForInvoicing[0].Id, User__c = Utility_Test.ADMIN_USER.Id, Brands__c = 'Aladdin;Horizon',
                                                      Territory__c = terrList[1].Id));
            
            insert mohAccTeam;
            
            terrUsers = new List<Territory_User__c>();
            terrUsers.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Business Development Manager', User__c = Utility_Test.ADMIN_USER.Id));
            terrUsers.add(new Territory_User__c(Territory__c = terrList[1].Id, Role__c = 'Business Development Manager', User__c = Utility_Test.ADMIN_USER.Id));
            insert terrUsers;
            
            actListForResidentialFL = Utility_Test.createActionListForResidentialFL(true, 1);
            actListForResidentialSAL = Utility_Test.createActionListForResidentialSAL(true, 1);
            actListForResidential.add(actListForResidentialFL.get(0));
            actListForResidential.add(actListForResidentialSAL.get(0));
            
            salTerr = new List<SAL_Territory__c>();
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialFL[0].Id, Territory__c = terrList[0].Id)); 
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialFL[0].Id, Territory__c = terrList[1].Id));   
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialSAL[0].Id, Territory__c = terrList[0].Id));   
            salTerr.add(new SAL_Territory__c( Action_List__c = actListForResidentialSAL[0].Id, Territory__c = terrList[1].Id));   
            insert salTerr;
            Map<id, Set<String>> idStrMap = new Map<id,Set<String>>();
            Map<id,id> idMap = new Map<id, id>();
            idStrMap.put(salTerr[0].Id, new Set<String>{'abc'});
            idMap.put(actListForResidentialFL[0].Id,salTerr[0].Id);
            DisplaySAL_CC.checkActionListWithBrandCollection(actListForResidentialFL, idStrMap, idMap);
            
            //String id = resAccListForInvoicing.get(0).id;
            system.assert(resAccListForInvoicing!=null);
            
            DisplaySAL_CC.retrieveSALs(id, null, null);
            test.stopTest();
        }
        
    }
    
    static testMethod void testCommercialAccount() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            comsAccListForInvoicing = Utility_Test.createCommercialAccountsForInvoicing(true, 1);
            actListForCommercialFL = Utility_Test.createActionListForCommercialFL(true, 1);
            actListForCommercialSAL = Utility_Test.createActionListForCommercialSAL(false, 1);
            for(Action_List__c actList : actListForCommercialSAL) {
                actList.End_Date__c =  system.today() + 7;
            }
            insert actListForCommercialSAL;
            
            oppList = Utility_Test.createOpportunities(true, 1, comsAccListForInvoicing);
            
            String id = comsAccListForInvoicing.get(0).id;
            system.assert(id!=null);
            
            DisplaySAL_CC.retrieveSALs(id, null, null);
            test.stopTest();
        }
    }
    
    static testMethod void testCommercialAccount2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            List<Account> tempAccs = Utility_Test.createCommercialAccountsForInvoicing(true, 2);
            Account parentAccount = tempAccs.get(0);
            Account comsAccForInvoicing = tempAccs.get(1);
            comsAccForInvoicing.ParentId = parentAccount.Id;
            update comsAccForInvoicing;
            
            comsAccListForInvoicing = new List<Account>();
            comsAccListForInvoicing.add(comsAccForInvoicing);
            actListForCommercialFL = Utility_Test.createActionListForCommercialFL(true, 1);
            actListForCommercialSAL = Utility_Test.createActionListForCommercialSAL(true, 1);
            
            oppList = Utility_Test.createOpportunities(true, 1, comsAccListForInvoicing);
            
            String id = comsAccForInvoicing.id;
            system.assert(id!=null);
            
            DisplaySAL_CC.retrieveSALs(id, null, null);
            test.stopTest();
        }
    }
    
    static testMethod void testCommercialAccount3() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            List<Account> tempAccs = Utility_Test.createCommercialAccountsForInvoicing(true, 2);
            Account parentAccount = tempAccs.get(0);
            parentAccount.Strategic_Account__c = True;
            Account comsAccForInvoicing = tempAccs.get(1);
            comsAccForInvoicing.ParentId = parentAccount.Id;
            update comsAccForInvoicing;
            
            comsAccListForInvoicing = new List<Account>();
            comsAccListForInvoicing.add(comsAccForInvoicing);
            actListForCommercialFL = Utility_Test.createActionListForCommercialFL(true, 1);
            actListForCommercialSAL = Utility_Test.createActionListForCommercialSAL(true, 1);
            
            oppList = Utility_Test.createOpportunities(true, 1, comsAccListForInvoicing);
            
            String id = comsAccForInvoicing.id;
            system.assert(id!=null);
            
            DisplaySAL_CC.retrieveSALs(id, null, null);
            test.stopTest();
        }
    }
    
    static testMethod void testCommercialAccountFromEvent() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            comsAccListForInvoicing = Utility_Test.createCommercialAccountsForInvoicing(true, 1);
            actListForCommercialFL = Utility_Test.createActionListForCommercialFL(false, 1);
            for(Action_List__c actList : actListForCommercialFL) {
                actList.Start_Date__c = system.today() - 7;
                actList.End_Date__c =  system.today() + 7;
            }
            insert actListForCommercialFL;
            
            actListForCommercialSAL = Utility_Test.createActionListForCommercialSAL(false, 1);
            for(Action_List__c actList : actListForCommercialSAL){
                actList.Start_Date__c = system.today() - 7;
            }
            insert actListForCommercialSAL;
            
            oppList = Utility_Test.createOpportunities(true, 1, comsAccListForInvoicing);
            eventActionList = Utility_Test.createEventActions(true, 1);
            eventList = Utility_Test.createEventsWithEventAction(true, 1, comsAccListForInvoicing, eventActionList);
            myAppList = Utility_Test.createMyAppointmentList(true, 1, actListForCommercialFL, eventActionList.get(0));
            String id = eventList.get(0).id;
            system.assert(id!=null);
            DisplaySAL_CC.retrieveSALs(id, null, null);
            test.stopTest();
        }
    }
    
    static testMethod void testCreateMyAppointmentList() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            comsAccListForInvoicing = Utility_Test.createCommercialAccountsForInvoicing(true, 1);
            actListForCommercialFL = Utility_Test.createActionListForCommercialFL(true, 1);
            actListForCommercialSAL = Utility_Test.createActionListForCommercialSAL(true, 1);
            oppList = Utility_Test.createOpportunities(true, 1, comsAccListForInvoicing);
            eventActionList = Utility_Test.createEventActions(true, 1);
            eventList = Utility_Test.createEventsWithEventAction(true, 1, comsAccListForInvoicing, eventActionList);
            myAppList = Utility_Test.createMyAppointmentList(true, 1, actListForCommercialFL, eventActionList.get(0));
            String id = eventList.get(0).id;
            
            List<String> actionListIds = new List<String>();
            String actListForCommercialFLId = actListForCommercialFL.get(0).id;
            String actListForCommercialSALId = actListForCommercialSAL.get(0).id;
            
            actionListIds.add(actListForCommercialFLId);
            actionListIds.add(actListForCommercialSALId);
            
            String eventActionListId = eventActionList.get(0).id;
            String myAppListId = myAppList.get(0).id;
            
            
            String insertMyAppListJSON = '[{"Action_List__c":"' + actListForCommercialSALId +'","Event_Action__c":"'+ eventActionListId +
                '","Include_Attachments_In_Email__c":true}]';
            String updateMyAppListJSON = '[{"Action_List__c":"'+ actListForCommercialFLId +' ","Include_Attachments_In_Email__c":false,"Event_Action__c":"'+ 
                eventActionListId  +'","Id":"'+ myAppListId +'"}]';
            
            String insertMyAppListJSONException = '[{"Action_Lst":"' + actListForCommercialSALId +'","Event_Acion__c":"'+ eventActionListId +
                '","Include_Attachments_In_Email__c":true}';
            String updateMyAppListJSONException = '[{"Action_Lst__c":"'+ actListForCommercialFLId +' ","Inclde_Attachments_In_Email__c":false,"Event_Action__c":"'+ 
                eventActionListId  +'","d":"'+ myAppListId +'"}]';
            
            DisplaySAL_CC.createMyAppointmentList(id, actionListIds, insertMyAppListJSON, updateMyAppListJSON);
            DisplaySAL_CC.createMyAppointmentList(id, actionListIds, insertMyAppListJSONException, updateMyAppListJSONException);
            test.stopTest();
        }
        
    }
    
    static testMethod void testgetPicklist(){
        test.startTest();
        Map<String, List<String>> pickMap =  DisplaySAL_CC.getPicklistValue();
        test.stopTest();
    }
    
    
}