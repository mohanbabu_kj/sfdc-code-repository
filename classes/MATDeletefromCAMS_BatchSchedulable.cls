global class MATDeletefromCAMS_BatchSchedulable implements Schedulable{
    global void execute (SchedulableContext ctx) {
        Integer batchSize=200;
         MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('MATDeletefromCAMS_Batch');
          if (custoSettingVal != null) {
            batchSize = custoSettingVal.Batch_Size__c != null ? Integer.valueOf(custoSettingVal.Batch_Size__c) : 200;    
          }
        MATDeletefromCAMS_Batch b = new MATDeletefromCAMS_Batch();
        database.executebatch(b,batchSize);
    }

}