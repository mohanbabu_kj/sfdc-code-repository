@isTest
private class AccountProfileTrigger_Test {
    
    static List<Account_Profile__c> accProfile ;
    
    @TestSetup
    public static void init(){
        Default_Configuration__c dc = Utility_Test.getDefaultConfCusSetting();
        Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        AP_Trigger_Settings__c APSC1 = new AP_Trigger_Settings__c();
        APSC1.Name = 'Chain Number Exclude';
        APSC1.Exclude_Chain_Numbers_1__c = '10';
        APSC1.Exclude_Chain_Numbers_2__c = '13';
        insert APSC1;    
    }
    
    static testmethod void testmethod1(){
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
        apSetting[0].Red_Threshold_Carpet__c = 10;
        apSetting[0].Yellow_Threshold_Cushion__c = 10;
        apSetting[0].Red_Threshold_Cushion__c = 10;
        apSetting[0].Yellow_Threshold_Hardwood__c = 10;
        apSetting[0].Red_Threshold_Hardwood__c = 10; 
        apSetting[0].Yellow_Threshold_Laminate__c = 10; 
        apSetting[0].Red_Threshold_Laminate__c = 10;
        apSetting[0].Yellow_Threshold_Tile__c = 10;
        apSetting[0].Red_Threshold_Tile__c = 10;
        apSetting[0].Yellow_Threshold_Resilient__c = 10;
        apSetting[0].Red_Threshold_Resilient__c = 10;
        apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
        apSetting[0].Red_Threshold_Mainstreet__c = 10;
        apSetting[0].Multi_Family_Business_Split_2Team__c =10;
        apSetting[0].Retail_Business_Split_2Team__c = 10;
        apSetting[0].Retail_Business_Split_1Team__c = 10;
        apSetting[0].Builder_Business_Split_2_Team__c = 10;
        apSetting[0].Builder_Business_Split_1Team__c = 10;
        apSetting[0].Multi_Family_Business_Split_1team__c = 10;
        apSetting[0].Retail_Multi_channel_split__c =100;
        apSetting[0].Builder_Multi_Family_Multi_Channel_Split__c  =100;
        
        update apSetting[0];
        
        List<Account_Profile__c> apList = new List<Account_Profile__c>();
        Account_Profile__c ap = new Account_Profile__c();
        ap.Name = 'TEST ACCOUNT PROFILE TEST CLASS';
        ap.Primary_Business__c = apSetting[0].Id;
        ap.Annual_Retail_Sales__c = 300000;
        ap.Annual_Retail_Sales_Non_Flooring__c = 10;
        
        ap.EWP_Total__c=10;
        ap.AMP_Total__c=10;
        ap.EWP_Total__c=10;
        apList.add(ap);
        insert apList;
        apList[0].Annual_Retail_Sales__c = 100000;
        apList[0].Multi_Channel__c = false;
        apList[0].R_ERS_RC__c = 10;
        apList[0].R_ERS_AC__c = 10;
        apList[0].B_ERS_RC__c = 10;
        apList[0].B_ERS_AC__c = 10;
        
        apList[0].R_ERS_Car__c = 10;
        apList[0].B_ERS_Car__c = 10;
        apList[0].retail__c = 2; 
        apList[0].builder__c = 1;
        apList[0].Multi_Family__c = 1;
        update apList;
    }
    
    
}