/**************************************************************************

Name : UtilityCls_Test

===========================================================================
Purpose : This tess class is used for UtilityCls
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         22/Feb/2017     Created 
1.1        Lester         26/April/2017   Modified
1.2        Susmitha       27/Feb/2018     Modified
***************************************************************************/
@isTest
private class UtilityCls_Test {
    
    static testMethod void testUtilityCls(){
        List<String> strList = new List<String>();
        strList.add('A');
        strList.add('B');
        Set<String> setStr = new Set<String>();
        setStr.add('A');
        setStr.add('B');
        
        Set<Id> ids = new Set<Id>();
        Utility_Test.getTeamCreationCusSetting();
        List<Account> accList = Utility_Test.createAccountsForInvoicing(true, 2);
        ids.add((accList.get(0)).Id);
        ids.add((accList.get(1)).Id);
        UtilityCls.toString4ListMembers(accList);
        UtilityCls.stringFormatWithSemicolon(null);         
        UtilityCls.stringFormatWithSemicolon(strList);
        UtilityCls.getStringFormatForSOQL(ids);
        UtilityCls.getSetFormatForIncludesSOQL(setStr);
        UtilityCls.getStringFormatForIncludesSOQL('A;B;C');
        UtilityCls.getStringFormatForListdsIncludesSOQL('A,B,C');
        UtilityCls.getStringFormatForSetIdsIncludesSOQL(ids);
        UtilityCls.sessionId();
        UtilityCls.validateProfileByName('Admin');
        UtilityCls.getObjectNameById((accList.get(0)).Id);
        UtilityCls.checkUserPermissionSet(Utility_Test.ADMIN_USER.Id, 'System Administrator' );
        UtilityCls.getObjectFieldsForSql('Account');
        Datetime myDateTime = datetime.now();
        UtilityCls.getLocalDateTime(myDateTime);
        UtilityCls.hasSObjectField('Id',accList.get(0));
        UtilityCls.getFieldSetAllFields('Account', 'Strategic_Account_Field_Set');
        UtilityCls.getFieldSetAllFieldPaths('Account', 'Strategic_Account_Field_Set');
        // methods
        Boolean isNUll = UtilityCls.isStrNotNull('Test');
        system.assert(isNUll);
        UtilityCls.getRecord('Account', null, 'Id');
        
        List<Mashup_Management__c>  mashupManList = Utility_Test.getMashupManagementCusSetting();
        Mashup_Management__c data = mashupManList.get(0);
        Account_Profile__c accPro = new Account_Profile__c();
        Map<String,String> paramList = new  Map<String,String>();
        paramList.put('Id', accPro.Id );
        // UtilityCls.buildQueryStringForMashupUrl(data, 'https://myanalyticsqa.mohawkind.com/t/Flooring/views/AEandTeamCommissionableSales_0/SalesInfomation?:embed=y',
        //                                         accPro, paramList);
        //Id salesActionListRecordType =   
        
        test.startTest();
        Try {
            UtilityCls.getRecordTypeInfo('Action_List__c', UtilityCls.COMMERCIAL_FUNDAMENT_LIST);
            UtilityCls.getPicklistValues('Account', 'Industry');
            UtilityCls.resiInvoicingRoles_CS();
            UtilityCls.salResidentialRoles_CS();
            UtilityCls.getPicklistValuesFromCustomObject('Product_SFDC_Mapping__c', 'Sample_Inventory_Category__c');
            // insert new Account(); is used for UtilityCls.createExceptionLog
            insert new Account();
            List<Contact> conLst=new list<contact>();
            Contact c=new Contact(lastname='Test',Phone='7065434563',email='test@con.com');
            conLst.add(c);
            List < Database.Saveresult > results=database.insert(conLst,false);
            UtilityCls.createDMLexceptionlog(conLst,results,'testClass','TestMethod');
            List < Database.Upsertresult > results1=database.upsert(conLst,false);
            UtilityCls.createUpsertDMLexceptionlog(conLst,results1,'testClass','TestMethod');
            List < Database.Deleteresult > results2=database.delete(conLst,false);
            UtilityCls.createDeleteDMLexceptionlog(conLst,null,results2,'testClass','TestMethod');
            
        } Catch (Exception ex){
            UtilityCls.createExceptionLog(ex, 'Test Class name', 'Test sourceFunction', 'Test logCode', 'Test referenceId', 'Test referenceInfo');    
        }
        
        test.stopTest();
    }
    static testMethod void testUtilityMethod1(){
        test.startTest();
        
        List<Contact> conLst=new list<contact>();
        List<Contact> conLst1=new list<contact>();

        Contact c=new Contact(Phone='7065434563',email='test@con.com');
        Contact c1=new Contact(lastname='test',Phone='7065434563',email='test@con.com');

        conLst.add(c);
        conLst1.add(c1);

        List < Database.Saveresult > results=database.insert(conLst,false);
        List < Database.Saveresult > results0=database.insert(conLst1,false);

        UtilityCls.createDMLexceptionlog(conLst,results,'testClass','TestMethod');
        conLst1[0].lastname=null;
        List < Database.Upsertresult > results1=database.upsert(conLst1,false);
        UtilityCls.createUpsertDMLexceptionlog(conLst1,results1,'testClass','TestMethod');
        List < Database.Deleteresult > results2=database.delete(conLst1,false);
        UtilityCls.createDeleteDMLexceptionlog(conLst1,null,results2,'testClass','TestMethod');
        
        test.stopTest();
        
    }
    static testMethod void testUtilityCls2(){
        
        test.startTest();
        Try {
            // to create an exception from UtilityCls.getRecord method
            UtilityCls.getRecord('ABC', null, 'Id');
            UtilityCls.createDataErrorLog('Test Class name', ' Test Message', 'Test sourceFunction', 'Test logCode', 'Test referenceId', 'Test referenceInfo');
            //System.assert(false, 'Exception expected');
        } Catch (Exception ex){
            UtilityCls.createExceptionLog(ex, 'Test Class name', 'Test sourceFunction', 'Test logCode', 'Test referenceId', 'Test referenceInfo');    
        }
        
        test.stopTest();
    }
    
    static testMethod void testUtilityCls3(){
        System.runAs(Utility_Test.ADMIN_USER) {
            
            test.startTest();
            id ProfileId   = [SELECT Id FROM Profile WHERE Name='Commercial Sales Manager'].Id;
            system.assert(ProfileId!=null);
            Utility_Test.getDefaultConfCusSetting() ;
            UtilityCls.resiInvoicingRoles_CS();
            UtilityCls.resiNonInvoicingRoles_CS();
            UtilityCls.salResidentialRoles_CS();
            UtilityCls.stringtoList('hello;world');
            
            UtilityCls.toStringReturnEmptyIfNull('test');
            UtilityCls.addSuperScriptToDate('1111-1101-2018');
            UtilityCls.getSelectAllQuery('Account');
            UtilityCls.Determine_Segment('Account');
            UtilityCls.addPadding(12,'test','tes');
            UtilityCls.getDependentOptions('Account','Name','Id','Name');
            new UtilityCls.Bitset().LoadCharCodes();
            new UtilityCls.Bitset().testBit('testss',10);
            new UtilityCls.TPicklistEntry().active='test';
            new UtilityCls.TPicklistEntry().defaultValue='ssss';
            new UtilityCls.TPicklistEntry().label='addd';
            new UtilityCls.TPicklistEntry().value='weee';
            new UtilityCls.TPicklistEntry().validFor='hhhh';
            UtilityCls.getProfileName(ProfileId);
            test.stopTest();
            
        }
    }
    static testMethod void testUtilityCls4(){
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            Default_Configuration__c df = new Default_Configuration__c(Account_Team_Access__c = 'Read',
                                                                       Opportunity_Team_Access__c = 'Read',
                                                                       Residential_Invoicing_Roles__c ='Regional Vice President,Sales Vice President, District Manager',
                                                                       Residential_Non_Invoicing_Roles__c = 'Regional Vice President,Sales Vice President',
                                                                       SAL_Residential_Roles__c = 'District Manager,Regional Vice President,Senior Vice President',
                                                                       Groups__c = 'Strategic_Account_Managers;Admin;Commercial;Residential', // Added by MB - 07/10/17
                                                                       Fire_Account_Trigger__c = true,
                                                                       Fire_Lead_Trigger__c = true,
                                                                       Fire_MohawkAccountTeam_Trigger__c = true,
                                                                       Fire_Opportunity_Trigger__c = true,
                                                                       Residential_Sales_Ops__c = 'test',
                                                                       Commercial_Sales_Ops__c = 'test',
                                                                       Fire_RelatedAccount_Trigger__c = true,
                                                                       Fire_TerritoryUser_Trigger__c = true,
                                                                       Fire_Event_Trigger__c = true, // Added by MB - 07/10/17
                                                                       Opportunity_Team_Member_Trigger__c = true,
                                                                       Healthcare_Sr_Living__c = 'Healthcare;Hospitality;Mixed Use;Public Spaces;Senior Living',
                                                                       Workplace_Retail__c ='Corporate;Faith Base;CRE/TI;Retail',
                                                                       Education_Govt__c =  'Government;Higher Education;K12;Higher Ed');
            
            insert df;
            
            system.assert(df!=null);
            
            Contact c=new Contact(lastname='Test',Phone='7065434563',email='test@con.com');
            insert c;
            String s= UtilityCls.getObjectLabelById(c.id);
            UtilityCls.getRecordTypeInfo('Action_List__c', null);
            UtilityCls.Determine_Segment('Healthcare');
            UtilityCls.Determine_Segment('Government');
            UtilityCls.Determine_Segment('Corporate');
            UtilityCls.addSuperScriptToDate('2222-1101-2018');
            UtilityCls.addSuperScriptToDate('3333-1101-2018');
            Boolean b;
            b=UtilityCls.isTriggerExecute('Lead');
            b=UtilityCls.isTriggerExecute('Account');
            b=UtilityCls.isTriggerExecute('Opportunity');
            b=UtilityCls.isTriggerExecute('Mohawk_Account_Team__c');
            b=UtilityCls.isTriggerExecute('Related_Account__c');
            b=UtilityCls.isTriggerExecute('Territory_User__c');
            b=UtilityCls.isTriggerExecute('OpportunityTeamMember');
            b=UtilityCls.isTriggerExecute('Contact');
            b=UtilityCls.isTriggerExecute('CPL_Commercial_Broadloom__c');
            b=UtilityCls.isTriggerExecute('CPL_Residential_Broadloom__c');
            b=UtilityCls.isTriggerExecute('CPL_Resilient_Sheet__c');
            b=UtilityCls.isTriggerExecute('CPL_Carpet_Tile__c');
            b=UtilityCls.isTriggerExecute('CPL_Cushion__c');
            b=UtilityCls.isTriggerExecute('CPL_Solid_Wood__c');
            b=UtilityCls.isTriggerExecute('CPL_Tec_Wood__c');
            b=UtilityCls.isTriggerExecute('CPL_Rev_Wood__c');
            b=UtilityCls.isTriggerExecute('CPL_Tile__c');
            b=UtilityCls.isTriggerExecute('CPL_Resilient_Tile__c');
            b=UtilityCls.isTriggerExecute('CPL_Installation_Accessories__c');
            b=UtilityCls.isTriggerExecute('CPL_Care_and_Maintenance__c');
            b=UtilityCls.isTriggerExecute('null');
            
            
            test.stopTest();
        }
    }
    
}