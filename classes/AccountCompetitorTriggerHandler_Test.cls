/**************************************************************************

Name : AccountCompetitorTriggerHandler_Test

===========================================================================
Purpose : This tess class is used for AccountCompetitorTriggerHandler and AccountCompetitorTrigger
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha      6/Nov/2017     Created             

***************************************************************************/
@isTest
private class AccountCompetitorTriggerHandler_Test {
    
    static testMethod void testMethod1() {
        Id competitorRT=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Competitors').getRecordTypeId();  
        
        Account acc=new Account(Name='Test Account');
        insert acc;
        Account com=new Account(Name='Test Competitor',recordTypeId=competitorRT);
        insert com;
        Account_Competitor__c acCom=new Account_Competitor__c(Account__c=acc.id,Competitor__c=com.id);
        insert acCom;
        system.assert(acCom.Account__c==acc.id && acCom.Competitor__c==com.id);

    }
    
}