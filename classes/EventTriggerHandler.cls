public without sharing class EventTriggerHandler {
    
    public static Boolean runUpdateTrigger = true; //Added by MB - Bug 60273 - 08/09/18
    
    public static void validateContact(List<Event> eventList){
        for(Event event: eventList){
            Boolean error = true;
            //Added by MB - 2/23/18
            String objName = '';
            if(event.WhatId != null){
                objName = UtilityCls.getObjectNameById(event.WhatId);
                if(objName != null && objName == 'Account' && event.WhoId != null){
                    for(Contact contact: [SELECT Id, Name FROM Contact WHERE AccountId =: event.WhatId]){
                        system.debug(event.WhoId + ':' + contact.Id);
                        if(event.WhoId == contact.Id){
                            error = false;
                            break;
                        }else {
                            error = true;
                        }
                    }
                    if(error){
                        event.addError('Please select Contacts associated to Account');
                    }
                }
            }
        }
    }
    
    public static void updateAttendeeToName(Set<Id> eventIds){
        //Set<Id> eventIds = new Set<Id>();
        Map<Id, Set<Id>> inviteeNameMap = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> contactNameMap = new Map<Id, Set<Id>>();
        Map<Id, EventRelation> contactIdEventIdMap = new Map<Id, EventRelation>();
        
        /*for(Event event: eventList){
eventIds.add(event.Id);
}*/
        
        if(!eventIds.isEmpty()){
            for(EventRelation eveRel: [SELECT Id, IsInvitee, EventId, RelationId FROM EventRelation
                                       WHERE EventId IN: eventIds]){
                                           if(test.isRunningTest()){
                                               eveRel.IsInvitee=true;
                                           }
                                           String relValue = String.valueOf(eveRel.RelationId);
                                           if(relValue.startsWith('003') && eveRel.IsInvitee){
                                               if(inviteeNameMap.containsKey(eveRel.EventId)){
                                                   inviteeNameMap.get(eveRel.EventId).add(eveRel.RelationId);
                                               }else{
                                                   Set<Id> relationIds = new Set<Id>();
                                                   relationIds.add(eveRel.RelationId);
                                                   inviteeNameMap.put(eveRel.EventId, relationIds);
                                               }
                                               contactIdEventIdMap.put(eveRel.RelationId, eveRel);
                                           }else if(relValue.startsWith('003') && !eveRel.IsInvitee){
                                               if(contactNameMap.containsKey(eveRel.EventId)){
                                                   contactNameMap.get(eveRel.EventId).add(eveRel.RelationId);
                                               }else{
                                                   Set<Id> relationIds = new Set<Id>();
                                                   relationIds.add(eveRel.RelationId);
                                                   contactNameMap.put(eveRel.EventId, relationIds);
                                               }
                                           }
                                       }
        }
        System.debug('InviteeNameMap: ' + inviteeNameMap);
        System.debug('ContactNameMap: ' + contactNameMap);
        
        Set<EventRelation> eventRelationSet = new Set<EventRelation>();
        Set<EventRelation> eventRelationUpdSet = new Set<EventRelation>();
        for(Id eventId: eventIds){
            Set<Id> inviteeIds = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
            inviteeIds = inviteeNameMap.get(eventId);
            contactIds = contactNameMap.get(eventId);
            if(inviteeIds != null){
                for(Id invitee: inviteeIds){
                    if(contactIds != null && contactIds.contains(invitee)){
                        continue;
                    }else{
                        EventRelation eventRel = new EventRelation();
                        if(contactIdEventIdMap != null && contactIdEventIdMap.containsKey(invitee)){
                            eventRel = contactIdEventIdMap.get(invitee);
                            eventRel.IsParent = true;
                            eventRelationUpdSet.add(eventRel);
                        }else{
                            eventRel.IsInvitee = false;
                            eventRel.EventId = eventId;
                            eventRel.RelationId = invitee;
                            eventRel.IsParent = true;
                            eventRelationSet.add(eventRel);
                        }
                    }
                }
            }
        }
        System.debug('Event Relation: ' + eventRelationSet);
        System.debug('Event Relation Upd: ' + eventRelationUpdSet);
        List<EventRelation> eventRelationList = new List<EventRelation>();
        if(!eventRelationSet.isEmpty()){
            eventRelationList.addAll(eventRelationSet);
            System.debug('Event Relation List: ' + eventRelationList);
            List < Database.Saveresult > results=Database.insert(eventRelationList,true);
            //utilityCls.createDMLexceptionlog(eventRelationList, results, 'EventTriggerHandler','updateAttendeeToName');
        }
        eventRelationList.clear();
        if(!eventRelationUpdSet.isEmpty()){
            eventRelationList.addAll(eventRelationUpdSet);
            System.debug('Event Relation List: ' + eventRelationList);
            List < Database.Saveresult > results= Database.update(eventRelationList,true);
            //utilityCls.createDMLexceptionlog(eventRelationList, results, 'EventTriggerHandler','updateAttendeeToName');
            
        }
    }
    
    public static void updateAppt(List<Event> newList, Boolean iflag, Boolean uflag){
        Set<Id> accIds = new Set<Id>();
        Set<Id> mhkatIds = new Set<Id>();
        //Map<Id, Mohawk_Account_Team__c> mhkAccTeamMap = new Map<Id, Mohawk_Account_Team__c>();
        Map<Id,String> accNamemap = new Map<Id,String>();
        
        if(iflag){
            for(Event event: newList){
                /* Start of Code - MB - Bug 64410 - 08/20/18 */
                /* Fill Account__c for the use in Batch class AccountAppointmentsRollup_Batch */
                Id whatId = event.WhatId;
                String sObjName = '';
                if(whatId != null) {sObjName = whatId.getSObjectType().getDescribe().getName();}
                if(sObjName == 'Account'){ event.Account__c = whatId; }
                /* End of Code - MB - Bug 64410 - 08/20/18 */
                //Start of Code - MB - Bug 71554 - 3/13/19
                if(sObjName == 'Mohawk_Account_Team__c'){ mhkatIds.add(event.WhatId); }
                //End of Code - MB - Bug 71554 - 3/13/19
                if(event.mamd__WA_Multiday_Waypoint__c != null){
                    mhkatIds.add(event.WhatId);
                }
            }
			
            Map<Id, Mohawk_Account_Team__c> mhkAccTeamMap = new Map<Id, Mohawk_Account_Team__c>([Select Id,Account__C,Account__r.Name, Account__r.address__c from Mohawk_Account_Team__C where id in :mhkatIds]);
            
            for(Event event: newList){
                Id whatId = event.WhatId;
                String sObjName = '';
                if(whatId != null){sObjName = whatId.getSObjectType().getDescribe().getName();}
                event.Start_Date__c = Date.valueOf(event.StartDateTime); //Added by MB - 08/08/18 - Bug 60008
                
                if(event.mamd__WA_Multiday_Waypoint__c != null || sObjName == 'Mohawk_Account_Team__c'){ //Added condition for sObjName - MB - Bug 71554 - 3/13/19
                    Mohawk_Account_Team__c mat = mhkAccTeamMap.get(event.WhatId);
                    if(mat != null){
                        event.Subject = 'Sales Call - ' +  mat.Account__r.Name;
                        event.Account__c = mat.Account__c; //Added by MB - 08/08/18 - Bug 60008
                        event.location = mat.Account__r.address__c; // - SS - 3/20/19 - 71906
                        //accNameMap.get(mat.id) acMap.get(mat.Account__c).Name ;           
                    } 
                }
            }
        }else if(uflag){//Added by MB - 08/08/18 - Bug 60008
            for(Event event: newList){
                event.Start_Date__c = Date.valueOf(event.StartDateTime); 
            }
        }
    }
    
    /* Start of Code - MB - 08/09/18 - Bug 60273 */
    public static void createEventAction(Map<Id, Event> eventMap){
        List<Event_Action__c> eveAcList = new List<Event_Action__c>();
        List<Event> evList = new List<Event>();
        for(Event ev: eventMap.values()){
            Event_Action__c eveAc = new Event_Action__c();
            eveAc.Event_Id__c = ev.Id;
            eveAcList.add(eveAc);
        }
        if(eveAcList.size() > 0){
            try{
                List<Database.SaveResult> results = Database.insert(eveAcList, false);
                for(Database.SaveResult result : results) {
                    if(result.isSuccess()) {               
                        for(Event_Action__c eveAc: eveAcList){ 
                            if(eveAc.Id == result.getId()){
                                Event ev = new Event(Id = eventMap.get(eveAc.Event_Id__c).Id, Event_Action__c = eveAc.Id);
                                evList.add(ev);
                            }
                        }
                    }
                } 
                UtilityCls.createDMLexceptionlog(eveAcList,results,'EventTriggerHandler','createEventAction');
                if(evList.size() > 0){
                    runUpdateTrigger = false;
                    List<Database.SaveResult> evResults = Database.update(evList, false);
                    UtilityCls.createDMLexceptionlog(evList,evResults,'EventTriggerHandler','createEventAction');
                }
                
            }catch(Exception ex){
                UtilityCls.createDataErrorLog('EventTriggerHandler', ex.getMessage(), 'createEventAction', '', '', '');
            }
        }
    }
    /* End of Code - MB - 08/09/18 - Bug 60273 */
    
    /* Start of Code - MS - 02/11/2018 - Bug 70109 */
    public static void updateCustomActivity( List<Event> eventList,Map<id,Event>eventMap ){
        Set<Id> eventIds = new  Set<Id>();
        if( eventList != null && eventList.size() > 0 ){
            for( Event evt : eventList ){
                if(evt.Account__c != null){
                    eventIds.add( evt.id );
                }
            }
        }

        if( eventIds != null && eventIds.size() > 0 ){
            List<Custom_Activities__c>listToUpdate = new List<Custom_Activities__c>();
            for( Custom_Activities__c CA : [ SELECT id,Account__c,Activity_ID__c,All_Day_Appointment__c,Assigned_To__c,Confirmation1__c,Confirmation2__c,Confirmation3__c,Confirmation4__c,Confirmation5__c,Flexible__c,Follow_Up_Date__c,Location__c,Objectives__c,Start_Date__c,Status__c,Subject__c,Topic__c,WhatId__c FROM Custom_Activities__c WHERE Activity_ID__c IN : eventIds ] ){
                //CA.Account__c = eventMap.get( CA.Activity_ID__c ).Account__c;
                CA.Activity_ID__c = eventMap.get( CA.Activity_ID__c ).Id;
                CA.All_Day_Appointment__c = eventMap.get( CA.Activity_ID__c ).isAllDayEvent;
                CA.Assigned_To__c = eventMap.get( CA.Activity_ID__c ).OwnerId;
                CA.Confirmation1__c = eventMap.get( CA.Activity_ID__c ).Confirmation1__c;
                CA.Confirmation2__c = eventMap.get( CA.Activity_ID__c ).Confirmation2__c;
                CA.Confirmation3__c = eventMap.get( CA.Activity_ID__c ).Confirmation3__c;
                CA.Confirmation4__c = eventMap.get( CA.Activity_ID__c ).Confirmation4__c;
                CA.Confirmation5__c = eventMap.get( CA.Activity_ID__c ).Confirmation5__c;
                CA.Flexible__c = eventMap.get( CA.Activity_ID__c ).Flexible__c;
                CA.Follow_Up_Date__c = date.valueof( eventMap.get( CA.Activity_ID__c ).Follow_Up_Date__c );
                CA.Location__c = eventMap.get( CA.Activity_ID__c ).Location;
                CA.Objectives__c = eventMap.get( CA.Activity_ID__c ).Objectives__c;
                CA.Start_Date__c = date.valueof( eventMap.get( CA.Activity_ID__c ).StartDateTime );
                CA.Status__c = eventMap.get( CA.Activity_ID__c ).Status__c;
                CA.Subject__c = eventMap.get( CA.Activity_ID__c ).subject;
                CA.Topic__c = eventMap.get( CA.Activity_ID__c ).Topic__c;
                CA.WhatId__c = eventMap.get( CA.Activity_ID__c ).WhatId;
                listToUpdate.add( CA );
            }
            
            if( !listToUpdate.isEmpty() )
                update listToUpdate;
        }
    }
    public static void deleteCustomActivity( Set<Id> eventIds ){
        if( eventIds != null && eventIds.size() > 0 )
            delete [ SELECT id FROM Custom_Activities__c WHERE Activity_ID__c IN : eventIds ];
    
    }
    /* End of Code - MS - 02/11/2018 - Bug 70109 */
}