public class DeletedOpptLineItemTriggerHandler {
    
    public static void deleteAfterInsert(Map<Id, Opportunity_Line_Item__c> newListMap){
        List<Id> delIds = new List<Id>();
        
        for(Id delId: newListMap.keySet()){
            delIds.add(delId);
        }
        
        if(!delIds.isEmpty()){
            try{
                Database.DeleteResult[] results = Database.delete(delIds, false);
                for(Database.DeleteResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully deleted Opp Line Item: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while deleting Opp Line Item: ' + err.getMessage());
                        }
                    }
                } 
            }
            catch (System.DMLException dmle){
                for(Integer i=0; i<dmle.getNumDml(); i++){
                    system.debug(dmle.getDmlMessage(i));
                }
            }
        }
    }
}