/***************************************************************************

Name : HybrisPostHandlerCalloutMock_Test 

===========================================================================
Purpose :  Create XML response for HybrisPostHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand        30/March/2017     Created 
***************************************************************************/
@isTest
global class HybrisPostHandlerCalloutMock_Test implements HttpCalloutMock  {

     global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
 
                    +'<intFlooringNaB2BQuoteResponse>'
                     
                        +'<b2bUnitID>0000412354</b2bUnitID>'
                     
                        +'<quoteID>Q10000033I</quoteID>'
                        +'<ResponseStatus>Success</ResponseStatus>'
                     
                        +'<quoteStatus>Requested</quoteStatus>'
                     
                    +'</intFlooringNaB2BQuoteResponse>');

           return res;
      }//end of Method
}//End of Class