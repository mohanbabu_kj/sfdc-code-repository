/**************************************************************************

Name : RelatedContactsOnAppointment_CC

===========================================================================
Purpose : Controller class to search contacts information from appointment
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         20/March/2017    Created         CSR:

***************************************************************************/
public without sharing class RelatedContactsOnAppointment_CC{  

	public class ContactViewModel {
		@AuraEnabled
      	public Boolean isSelect {get; set;} // current checkbox is select or not. It does not care about this value store in DB or not
      	@AuraEnabled
      	public Boolean isExistingDataInDB {get; set;} // to decide whether this record store in DB or not
      	@AuraEnabled
      	public Id id {get; set;}
      	@AuraEnabled
      	public String name {get; set;}
      	@AuraEnabled
      	public String eMail {get; set;}
      	@AuraEnabled
      	public String mobilePhone {get; set;}
      	@AuraEnabled
      	public String primaryRole{get;set;}
        @AuraEnabled
        public Boolean error = false;
        @AuraEnabled
        public String message = '';
      

      	public ContactViewModel(Boolean isSelect, Boolean isExistingDataInDB, Id id, String name, String  eMail, String mobilePhone, String primaryRole) {
      		this.isSelect = isSelect;
      		this.isExistingDataInDB = isExistingDataInDB;
      		this.id = id;
      		this.name = name;
      		this.eMail = eMail;
      		this.mobilePhone = mobilePhone;
      		this.primaryRole = primaryRole;
      		
      	}
        
        public ContactViewModel(){
            
        }

	}

	// display Contact information 
	/*Modified By Lillian Chen , query primary role from contact  */
	@AuraEnabled
	public static List<ContactViewModel> getContactFieldsInfo(String eventId) {
		System.debug('getContactFieldsInfo : eventId : ' + eventId);
		List<ContactViewModel> viewModel = new List<ContactViewModel>();

		if(!UtilityCls.isStrNotNull(eventId)) {
			return viewModel;
		}else {
            eventId = Appointments_CC.validateRecordId(eventId);
			Event event = [SELECT Id, Accountid, WhatId FROM Event WHERE Id =: eventId]; // 71609 - MS
			System.debug(LoggingLevel.INFO, '*** event: ' + event);
			// to get whoId from this event
			List<EventWhoRelation> eventWhoRelList = [Select RelationId from EventWhoRelation 
			                                         WHERE eventId =: event.Id AND Type = 'Contact'];

			System.debug('getContactFieldsInfo : eventWhoRelList : '+ eventWhoRelList);
			Map<Id, EventWhoRelation> conIdAndEvtMap = new Map<Id, EventWhoRelation>();
			Set<Id> existingEventWhoIds = new Set<Id>();
			for(EventWhoRelation eventWhoRel : eventWhoRelList){
				existingEventWhoIds.add(eventWhoRel.RelationId);
				conIdAndEvtMap.put(eventWhoRel.RelationId, eventWhoRel);
			}

			if(UtilityCls.isStrNotNull(event.WhatId)){ 
				List<Contact> contactOnAppoinetList = new List<Contact> ();
				
				
				contactOnAppoinetList = [SELECT Id, Name, Email, Primary_Role__c, MobilePhone From Contact WHERE AccountId =: event.AccountId];  // 71609 - MS
				System.debug(LoggingLevel.INFO, '*** contactOnAppoinetList: ' + contactOnAppoinetList);
				// check contact Id is added to EventWhoIds or not.
				if(existingEventWhoIds.isEmpty()){
					for(Contact contact : contactOnAppoinetList){
						ContactViewModel viewModelObj = new ContactViewModel(False, False, contact.Id, contact.Name, contact.Email, contact.MobilePhone, contact.Primary_Role__c);
						viewModel.add(viewModelObj);
					}
				}else {
					for(Contact contact : contactOnAppoinetList){
						ContactViewModel viewModelObj;
						// contact id exists in EventWhoIds fields
						if(existingEventWhoIds.contains(contact.Id)) {
							viewModelObj = new ContactViewModel(TRUE, TRUE, contact.Id, contact.Name, contact.Email, contact.MobilePhone, contact.Primary_Role__c);
						}else {
							viewModelObj = new ContactViewModel(False, False, contact.Id, contact.Name, contact.Email, contact.MobilePhone, contact.Primary_Role__c);
						} // end of if(existingEventWhoIds.contains(contact.Id)) 
						viewModel.add(viewModelObj);			}
				} // end of if(existingEventWhoIds.isEmpty())
			} // end of if(UtilityCls.isStrNotNull(event.WhatId))

			System.debug('viewModel :' + viewModel);
			return viewModel;
		}
	}
    
    @AuraEnabled
	public static ContactViewModel updateEventRelations(String eventId, String viewModel) {

		Set<id> deleteSet =  new Set<id>();
		Set<id> insertSet =  new Set<id>();
        
        eventId = Appointments_CC.validateRecordId(eventId); //Added by MB - 08/07/18 - For MA Checkout - Bug 58798
		System.debug(LoggingLevel.INFO, '*** eventId: ' + eventId);
		System.debug(LoggingLevel.INFO, '*** viewModel: ' + viewModel);
	
		List<ContactViewModel> viewModelList = (List<ContactViewModel>)JSON.deserialize(viewModel, List<ContactViewModel>.class);
		ContactViewModel contRelInfo = new ContactViewModel();
        
	    for(ContactViewModel cvm : viewModelList){
			System.debug(LoggingLevel.INFO, '*** cvm: ' + cvm);
			if(cvm.isExistingDataInDB == true && cvm.isSelect == false){
				deleteSet.add(cvm.id);		
			}
			if(cvm.isExistingDataInDB == false && cvm.isSelect == true){
				insertSet.add(cvm.id);
		    }
		}
		
        System.debug('Delete List: ' + deleteSet);
        if(!deleteSet.isEmpty()){
            List<EventRelation> eventRelList = [select id from eventRelation where eventId =:eventId AND RelationId in: deleteSet]; 
            System.debug('Delete Event RelList: ' + eventRelList);
            if(!eventRelList.isEmpty()){
                try{
                    List<Database.DeleteResult> results = Database.Delete(eventRelList, true);
                    System.debug('Delete Result: ' + results);
                    for(database.DeleteResult result: results){
                        if(!result.isSuccess()){
                            for(Database.Error err : result.getErrors()){
                                contRelInfo.error = true;
                                contRelInfo.message = err.getMessage();
                                return contRelInfo;
                            }
                        }
                        else{
                            contRelInfo.error = false;
                            contRelInfo.message = 'Relationship updated in Appointment';
                            return contRelInfo;
                        }
                    }
                }
                catch(Exception ex){
                    contRelInfo.error = true;
                    contRelInfo.message = ex.getMessage();
                    return contRelInfo;
                }
                //UtilityCls.createDeleteDMLexceptionlog(eventRelList,null,results,'RelatedContactsOnAppointMent_CC','updateEventRelations');           	
            }
        }
		
		//Insert Selected EventRelations
		List<EventRelation> eventRelList = new List<EventRelation>();
        
		for(Id  conId : insertSet){
            EventRelation eventRel = new EventRelation(Isparent = true, IsInvitee = false,
   										RelationId = conId, Eventid = eventId);
			eventRelList.add(eventRel);
		}
        
        if(!insertSet.isEmpty()){
            try{
                List<Database.SaveResult> results = Database.insert(eventRelList,true);
                for(database.SaveResult result: results){
                    if(!result.isSuccess()){
                        for(Database.Error err : result.getErrors()){
                            contRelInfo.error = true;
                            contRelInfo.message = err.getMessage();
                            return contRelInfo;
                        }
                    }
                    else{
                        contRelInfo.error = false;
                        contRelInfo.message = 'Relationship updated in Appointment';
                        return contRelInfo;
                    }
                }
            }
            catch(Exception ex){
                contRelInfo.error = true;
                contRelInfo.message = ex.getMessage();
                return contRelInfo;
            }
            //UtilityCls.createDMLexceptionlog(eventRelList,results,'RelatedContactsOnAppointMent_CC','updateEventRelations');
           
         	 // insert eventRelList;
         	
		}
        return contRelInfo;
	}	
}