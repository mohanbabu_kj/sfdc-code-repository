/***************************************************************************

Name : cloneQuickActionControllerTest 

===========================================================================
Purpose :  Test class for Controller for cloneQuickAction lightning component
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha      1/March/2018    Created 
***************************************************************************/

@isTest
private class cloneQuickActionControllerTEST{
    
    static testMethod void testMethod1() {
        Test.startTest();
        
        List<Account> acList = Utility_Test.createAccountsForNonInvoicing(true, 1); 
        List<Opportunity> Projlist = Utility_Test.createOpportunities(True,1,acList);
     
        cloneQuickActionController.accountAndQuoteWrapper wrpr = new cloneQuickActionController.accountAndQuoteWrapper();
       
        wrpr = cloneQuickActionController.getDealerAndQuotes( Projlist[0].id,'' );
        System.assertEquals(wrpr.isError, false);
        System.assertEquals(wrpr.message, null);
        
        wrpr = cloneQuickActionController.getDealerAndQuotes('','');
        System.assertEquals(wrpr.isError, true);
        System.assertEquals(wrpr.message, 'Please give valid ids');
        
        
        Quote q = new Quote();
        q.name = 'Quote-' + Projlist[0].name;
        q.opportunityId = Projlist[0].id;
        insert q;
        
        cloneQuickActionController.getQuote(q.id);
        System.assert( cloneQuickActionController.getQuote(q.id) != null );
        
        wrpr = cloneQuickActionController.getDealerAndQuotes('',q.id);
        cloneQuickActionController.getQuote(null);
        System.assert( cloneQuickActionController.getQuote(null) == null );
        Test.stopTest();       
        
    }
    
}