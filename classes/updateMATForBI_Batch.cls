/**************************************************************************

Name : updateMATForBI_Batch

===========================================================================
Purpose : To Send unique MAT records to BI out of all updated MAT records 
		  at Chain + Employee number combination
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit         19/Nov/2018    Created          

***************************************************************************/
global class updateMATForBI_Batch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    
    global MAT_Batch_Setting__c QueryList;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        String RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId();
        //QueryList = [ SELECT id,Batch_Query__c,Last_Run_At__c,Batch_Size__c FROM MAT_Batch_Setting__c WHERE NAME = 'updateMATForBI_Batch' LIMIT 1 ];        
        List<MAT_Batch_Setting__c>BatchSettingList = new List<MAT_Batch_Setting__c>( [ SELECT id,Batch_Query__c,Last_Run_At__c,Batch_Size__c FROM MAT_Batch_Setting__c WHERE NAME = 'updateMATForBI_Batch' LIMIT 1 ] );
        if( BatchSettingList != null && BatchSettingList.size() > 0){
            QueryList = BatchSettingList[0];
        }
               
        if( QueryList != null && QueryList.Last_Run_At__c != null ){
            //query = QueryList.Batch_Query__c  + ' AND LastModifiedDate >= '+QueryList.Last_Run_At__c;stom 
            DateTime dateTimeFormat = QueryList.Last_Run_At__c.addminutes(1);
            System.debug('Last Rune' +dateTimeFormat);
            query = QueryList.Batch_Query__c  + ' AND LastModifiedDate > : dateTimeFormat  ORDER BY Chain_Number_UserId__c ASC ';
            
        }else{
            query = 'SELECT Id,Chain_Number_UserId__c, Main_Profile__c, LastModifiedDate FROM Mohawk_Account_Team__c WHERE RecordTypeId = \''+ RecordTypeId + '\' AND LastModifiedDate = TODAY ORDER BY Chain_Number_UserId__c ASC';
        }
        system.debug( '::::Query::::' + Query );
        return Database.getQueryLocator( Query );   
    }
    
    global void execute( Database.BatchableContext BC, List<Mohawk_Account_Team__c> scope ) {
        Map<string,Mohawk_Account_Team__c>uniqueMATMap = new Map<string,Mohawk_Account_Team__c>();
        set<string> mainMAT = new set<string>();
        //List<Mohawk_Account_Team__c>MATToUpdate = new List<Mohawk_Account_Team__c>();
        for( Mohawk_Account_Team__c MAT : scope ){
            System.debug('Last modified Date '+ MAT.LastModifiedDate);
            mainMAT.add(MAT.Chain_Number_UserId__c);
            if(MAT.Main_Profile__c){
                uniqueMATMap.put( MAT.Chain_Number_UserId__c,MAT ); 
            } else {
                mainMAT.add(MAT.Chain_Number_UserId__c);
            }            
        }
        
        for(  Mohawk_Account_Team__c MAT : [select id, Chain_Number_UserId__c from mohawk_account_team__c where Chain_Number_UserId__c in : mainMAT and Main_Profile__c = true]){
             uniqueMATMap.put( MAT.Chain_Number_UserId__c,MAT ); 
        }
        
        /*
        Map<id,List<Mohawk_Account_Team__c>>MATMap = new Map<id,List<Mohawk_Account_Team__c>>();
        for( string str : uniqueMATMap.keyset() ){
            if( MATMap.ContainsKey( uniqueMATMap.get( str ).Account_Profile__c ) ){
                MATMap.get( uniqueMATMap.get( str ).Account_Profile__c ).add( uniqueMATMap.get( str ) );
            }else{
                MATMap.put( uniqueMATMap.get( str ).Account_Profile__c,new List<Mohawk_Account_Team__c>{ uniqueMATMap.get( str ) } );
            }
        }
        
        for( id str : MATMap.keySet() ){
            if( MATMap.get( str ) != null && MATMap.get( str ).size() > 0 ){
                MATToUpdate.add( MATMap.get( str )[0] );
            }
        }
        
        if( MATToUpdate != null && MATToUpdate.size() > 0 )
            updateMAT( MATToUpdate );
        */
        if( uniqueMATMap != null && uniqueMATMap.size() > 0 ){
            updateMAT( uniqueMATMap.values() );
        }
    }
    
    global void updateMAT( List<Mohawk_Account_Team__c> matToUpdate ){
        if( matToUpdate != null && matToUpdate.size() > 0 ){
            MohawkAccountTeamService.updateFromEvent = true;  // Used for not firing the MAT Trigger
            update matToUpdate;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if( QueryList != null ){
            QueryList.Last_Run_At__c = system.now();
            update QueryList;
        }else{
            MAT_Batch_Setting__c MBS = new MAT_Batch_Setting__c(
                name = 'updateMATForBI_Batch',
                Last_Run_At__c = system.now(),
                Batch_Size__c = 200
            );
            insert MBS;
        }
    }
    
    global void execute(SchedulableContext SC){
        String jobId = Database.executeBatch( new updateMATForBI_Batch(),Integer.valueOf(QueryList.Batch_Size__c) );
    }
}