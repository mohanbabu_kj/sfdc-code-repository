@isTest
private class Grid_Test {

    @testSetup
    private static void setData() {
        //insert users
        User[] users = new User[]{}; 
        User tmUser = ResidentialPricingGridUtilityTest.createUserRecord('Residential Sales User');
        tmUser.username += '.tm';
        users.add(tmUser);
        
        User salesopsUser = ResidentialPricingGridUtilityTest.createUserRecord('Residential Sales Operation');
        salesopsUser.username += '.salesops';
        users.add(salesopsUser);
        
        insert users;
        
        // insert an account
        Id invoicingRT=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();
        Account[] accts = new Account[]{};
        Account Acct=new Account(Name='Test Account', RecordTypeId = invoicingRT, CAMS_Account_Number__c = 'R.123456.0000', Chain_Number__c ='R.123456', OwnerId = tmUser.Id);
        accts.add(acct);
        Account AcctSalesOps=new Account(Name='Test Account', RecordTypeId = invoicingRT, CAMS_Account_Number__c = 'R.123457.0000', Chain_Number__c ='R.123457', OwnerId = salesOpsUser.Id);
        accts.add(AcctSalesOps);
        insert accts;
        
        //insert products
        List<Product2> listProducts = New List<Product2>();
        listProducts.addall(createProductRecords(2,'FLOORINGNACARPETPRODUCT','Commercial Broadloom','Commercial Broadloom'));
        listProducts.addall(createProductRecords(2,'FLOORINGNACARPETPRODUCT','Residential Broadloom','Residential Broadloom'));
        listProducts.addall(createProductRecords(2,'FLOORINGNARESILIENTPRODUCT','Sheet','Resilient Sheet'));
        listProducts.addall(createProductRecords(2,'FLOORINGNACARPETPRODUCT','Carpet Tile','Carpet Tile'));
        listProducts.addall(createProductRecords(2,'FLOORINGNACUSHIONPRODUCT','Rebond','Cushion'));
        listProducts.addall(createProductRecords(2,'FLOORINGNAHARDWOODPRODUCT','Solid','SolidWood'));
        listProducts.addall(createProductRecords(2,'FLOORINGNAHARDWOODPRODUCT','Engineered','TecWood'));
        listProducts.addall(createProductRecords(2,'FLOORINGNALAMINATEPRODUCT','Laminate','RevWood'));
        listProducts.addall(createProductRecords(2,'FLOORINGNATILEPRODUCT','Ceramic','Tile'));
        listProducts.addall(createProductRecords(2,'FLOORINGNARESILIENTPRODUCT','Rigid Plank','Resilient Tile'));
        listProducts.addall(createProductRecords(2,'FLOORINGNAINSTALLATIONPRODUCT','Adhesives','Installation Accessories'));
        listProducts.addall(createProductRecords(2,'FLOORINGNACAREMAINTENANCEPRODUCT','Cleaners','Care and Maintenance'));
        insert listProducts;
        
        Product_Relationship__c[] prodrels = new Product_Relationship__c[]{};
        for (integer i=0; i<listProducts.size(); i++){
            integer indexOfOtherProd;
            if (Math.mod(i,2) == 0)
                indexOfOtherProd = i+1;
            else
                indexOfOtherProd = i-1;
            prodrels.add(new Product_Relationship__c(Product__c = listProducts[i].Id, Relations_Type__c = 'DISPLAY', Related_Product__c = listProducts[indexOfOtherProd].Id, Source_Product_Unique_Id__c = 'abc' + i, Target_Product_Unique_Id__c = 'def' + i));
            if (i > 1)
                prodrels.add(new Product_Relationship__c(Product__c = listProducts[0].Id, Relations_Type__c = 'DISPLAY', Related_Product__c = listProducts[i].Id, Source_Product_Unique_Id__c = 'ab' + i, Target_Product_Unique_Id__c = 'de' + i));
        }
        insert prodrels;
        
        //insert CPL records
        List<CPL_Commercial_Broadloom__c> listCB = New List<CPL_Commercial_Broadloom__c>();
        List<CPL_Residential_Broadloom__c> listRB = New List<CPL_Residential_Broadloom__c>();
        List<CPL_Resilient_Sheet__c> listRS = New List<CPL_Resilient_Sheet__c>();
        List<CPL_Carpet_Tile__c> listCT = New List<CPL_Carpet_Tile__c>();
        List<CPL_Cushion__c> listC = New List<CPL_Cushion__c>();
        List<CPL_Solid_Wood__c> listSW = New List<CPL_Solid_Wood__c>();
        List<CPL_Tec_Wood__c> listTW = New List<CPL_Tec_Wood__c>();
        List<CPL_Rev_Wood__c> listRW = New List<CPL_Rev_Wood__c>();
        List<CPL_Tile__c> listT = New List<CPL_Tile__c>();
        List<CPL_Resilient_Tile__c> listRT = New List<CPL_Resilient_Tile__c>();
        List<CPL_Installation_Accessories__c> listIA = New List<CPL_Installation_Accessories__c>();
        List<CPL_Care_and_Maintenance__c> listCM = New List<CPL_Care_and_Maintenance__c>();
        
        listCB.add((CPL_Commercial_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Commercial_Broadloom__c(),listProducts[0],Acct.Id,true));
        listCB.add((CPL_Commercial_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Commercial_Broadloom__c(),listProducts[1],Acct.Id,true));
        listCB.add((CPL_Commercial_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Commercial_Broadloom__c(),listProducts[0],AcctSalesOps.Id,true));
        listCB.add((CPL_Commercial_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Commercial_Broadloom__c(),listProducts[1],AcctSalesOps.Id,true));
        insert listCB;
       
        listRB.add((CPL_Residential_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Residential_Broadloom__c(),listProducts[2],Acct.Id,true));
        listRB.add((CPL_Residential_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Residential_Broadloom__c(),listProducts[3],Acct.Id,true));
        listRB.add((CPL_Residential_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Residential_Broadloom__c(),listProducts[2],AcctSalesOps.Id,true));
        listRB.add((CPL_Residential_Broadloom__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Residential_Broadloom__c(),listProducts[3],AcctSalesOps.Id,true));
        insert listRB;
        
        listRS.add((CPL_Resilient_Sheet__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Sheet__c(),listProducts[4],Acct.Id,true));
        listRS.add((CPL_Resilient_Sheet__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Sheet__c(),listProducts[5],Acct.Id,true));
        listRS.add((CPL_Resilient_Sheet__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Sheet__c(),listProducts[4],AcctSalesOps.Id,true));
        listRS.add((CPL_Resilient_Sheet__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Sheet__c(),listProducts[5],AcctSalesOps.Id,true));
        insert listRS;
        
        listCT.add((CPL_Carpet_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Carpet_Tile__c(),listProducts[6],Acct.Id,false));
        listCT.add((CPL_Carpet_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Carpet_Tile__c(),listProducts[7],Acct.Id,false));
        listCT.add((CPL_Carpet_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Carpet_Tile__c(),listProducts[6],AcctSalesOps.Id,false));
        listCT.add((CPL_Carpet_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Carpet_Tile__c(),listProducts[7],AcctSalesOps.Id,false));
        insert listCT;
       
        listC.add((CPL_Cushion__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Cushion__c(),listProducts[8],Acct.Id,false));
        listC.add((CPL_Cushion__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Cushion__c(),listProducts[9],Acct.Id,false));
        listC.add((CPL_Cushion__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Cushion__c(),listProducts[8],AcctSalesOps.Id,false));
        listC.add((CPL_Cushion__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Cushion__c(),listProducts[9],AcctSalesOps.Id,false));
        insert listC;
        
        listSW.add((CPL_Solid_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Solid_Wood__c(),listProducts[10],Acct.Id,false));
        listSW.add((CPL_Solid_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Solid_Wood__c(),listProducts[11],Acct.Id,false));
        listSW.add((CPL_Solid_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Solid_Wood__c(),listProducts[10],AcctSalesOps.Id,false));
        listSW.add((CPL_Solid_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Solid_Wood__c(),listProducts[11],AcctSalesOps.Id,false));
        insert listSW;
        
        listTW.add((CPL_Tec_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tec_Wood__c(),listProducts[12],Acct.Id,false));
        listTW.add((CPL_Tec_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tec_Wood__c(),listProducts[13],Acct.Id,false));
        listTW.add((CPL_Tec_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tec_Wood__c(),listProducts[12],AcctSalesOps.Id,false));
        listTW.add((CPL_Tec_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tec_Wood__c(),listProducts[13],AcctSalesOps.Id,false));
        insert listTW;
        
        listRW.add((CPL_Rev_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Rev_Wood__c(),listProducts[14],Acct.Id,false));
        listRW.add((CPL_Rev_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Rev_Wood__c(),listProducts[15],Acct.Id,false));
        listRW.add((CPL_Rev_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Rev_Wood__c(),listProducts[14],AcctSalesOps.Id,false));
        listRW.add((CPL_Rev_Wood__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Rev_Wood__c(),listProducts[15],AcctSalesOps.Id,false));
        insert listRW;
       
        listT.add((CPL_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tile__c(),listProducts[16],Acct.Id,false));
        listT.add((CPL_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tile__c(),listProducts[17],Acct.Id,false));
        listT.add((CPL_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tile__c(),listProducts[16],AcctSalesOps.Id,false));
        listT.add((CPL_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Tile__c(),listProducts[17],AcctSalesOps.Id,false));
        insert listT;
       
        listRT.add((CPL_Resilient_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Tile__c(),listProducts[18],Acct.Id,false));
        listRT.add((CPL_Resilient_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Tile__c(),listProducts[19],Acct.Id,false));
        listRT.add((CPL_Resilient_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Tile__c(),listProducts[18],AcctSalesOps.Id,false));
        listRT.add((CPL_Resilient_Tile__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Resilient_Tile__c(),listProducts[19],AcctSalesOps.Id,false));
        insert listRT;
        
        listIA.add((CPL_Installation_Accessories__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Installation_Accessories__c(),listProducts[20],Acct.Id,false));
        listIA.add((CPL_Installation_Accessories__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Installation_Accessories__c(),listProducts[21],Acct.Id,false));
        listIA.add((CPL_Installation_Accessories__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Installation_Accessories__c(),listProducts[20],AcctSalesOps.Id,false));
        listIA.add((CPL_Installation_Accessories__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Installation_Accessories__c(),listProducts[21],AcctSalesOps.Id,false));
        insert listIA;
        
        listCM.add((CPL_Care_and_Maintenance__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Care_and_Maintenance__c(),listProducts[22],Acct.Id,false));
        listCM.add((CPL_Care_and_Maintenance__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Care_and_Maintenance__c(),listProducts[23],Acct.Id,false));
        listCM.add((CPL_Care_and_Maintenance__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Care_and_Maintenance__c(),listProducts[22],AcctSalesOps.Id,false));
        listCM.add((CPL_Care_and_Maintenance__c)ResidentialPricingGridUtilityTest.setCPLFields(New CPL_Care_and_Maintenance__c(),listProducts[23],AcctSalesOps.Id,false));
        insert listCM;
        
        List<Zone__c> listZones = ResidentialPricingGridUtilityTest.createZoneRecords(5);
        insert listZones;
        
        List<Territory__c> listTerritory = ResidentialPricingGridUtilityTest.createTerritoryRecords(5);
        listTerritory[0].Zone__c = listZones[0].Id;
        listTerritory[0].Region__c = '202 - Southeast Soft';
        listTerritory[0].Region_Code__c = 'R202';
        listTerritory[1].Zone__c = listZones[0].Id;
        listTerritory[1].Region__c = '203 - North Central Soft';
        listTerritory[1].Region_Code__c = 'R203';
        listTerritory[2].Zone__c = listZones[0].Id;
        listTerritory[2].Region__c = '204 - Central Soft';
        listTerritory[2].Region_Code__c = 'R204';
        listTerritory[3].Zone__c = listZones[0].Id;
        listTerritory[3].Region__c = '205 - Southwest Coast Soft';
        listTerritory[3].Region_Code__c = 'R205';
        listTerritory[4].Zone__c = listZones[0].Id;
        listTerritory[4].Region__c = '206 - Northeast Soft';
        listTerritory[4].Region_Code__c = 'R206';
        insert listTerritory;
        
        List<Territory_User__c> listTerritoryUser = ResidentialPricingGridUtilityTest.createTerritoryUserRecords(3);
        listTerritoryUser[0].User__c = tmUser.Id;
        listTerritoryUser[0].Territory__c = listTerritory[0].id;
        listTerritoryUser[1].User__c = tmUser.Id;
        listTerritoryUser[1].Territory__c = listTerritory[1].id;
        listTerritoryUser[2].User__c = tmUser.Id;
        listTerritoryUser[2].Territory__c = listTerritory[2].id;
        insert listTerritoryUser;
        
        Account_Product_Territory_Relationship__c[] acctProdTerrRel = new Account_Product_Territory_Relationship__c[]{};
        for (Product2 prod :listProducts){
            Account_Product_Territory_Relationship__c aptr = new Account_Product_Territory_Relationship__c();
            aptr.company_code__c = prod.Company__c;
            aptr.Brand_Code__c = prod.Brand_Code__c;
            aptr.Product_Type__c = prod.ERP_Product_Type__c;
            aptr.Sales_Group_TM__c = 'TM0';
            aptr.Account__c = acct.Id;
            aptr.District_Code__c = '1';
            aptr.District_Description__c = 'A';
            aptr.Region_Code__c = 'R202';
            aptr.Region_Description__c = 'B';
            aptr.Zone_Code__c = '0';
            aptr.Zone_Description__c = 'C';
            acctProdTerrRel.add(aptr);
        }
        insert acctProdTerrRel;
        
        List<Warehouse__c> listWarehouses = ResidentialPricingGridUtilityTest.createWarehouseRecords(5);
        insert listWarehouses;
        
        List<Warehouse_District_Relationship__c> listWDR = New List<Warehouse_District_Relationship__c>();
        listWDR.add(New Warehouse_District_Relationship__c(Warehouse__c = listWarehouses[0].Id, District__c = listTerritory[0].Id, Is_Active__c = true));
        listWDR.add(New Warehouse_District_Relationship__c(Warehouse__c = listWarehouses[1].Id, District__c = listTerritory[1].Id, Is_Active__c = true));
        listWDR.add(New Warehouse_District_Relationship__c(Warehouse__c = listWarehouses[2].Id, District__c = listTerritory[2].Id, Is_Active__c = true));
        listWDR.add(New Warehouse_District_Relationship__c(Warehouse__c = listWarehouses[3].Id, District__c = listTerritory[3].Id, Is_Active__c = false));
        listWDR.add(New Warehouse_District_Relationship__c(Warehouse__c = listWarehouses[4].Id, District__c = listTerritory[4].Id, Is_Active__c = false));
        insert listWDR;
        
        List<Buying_Group__c> listBuyingGroup = ResidentialPricingGridUtilityTest.createBuyingGroupRecords(5);
        insert listBuyingGroup;
        
        List<Price_Grid__c> listPriceGrids = New List<Price_Grid__c>();
        listPriceGrids.add(createPriceGridRecords(listProducts[0],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[1],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[2],'MBP',listTerritory[1].Region_Code__c,listZones[1].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[3],'MBP',listTerritory[2].Region_Code__c,listZones[2].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[4],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[5],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[6],'MBP',listTerritory[1].Region_Code__c,listZones[1].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[7],'MBP',listTerritory[2].Region_Code__c,listZones[2].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[8],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[9],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[10],'MBP',listTerritory[1].Region_Code__c,listZones[1].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[11],'MBP',listTerritory[2].Region_Code__c,listZones[2].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[12],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[13],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[14],'MBP',listTerritory[1].Region_Code__c,listZones[1].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[15],'MBP',listTerritory[2].Region_Code__c,listZones[2].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[16],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[17],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[18],'MBP',listTerritory[1].Region_Code__c,listZones[1].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[19],'MBP',listTerritory[2].Region_Code__c,listZones[2].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[20],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[21],'MBP',listTerritory[0].Region_Code__c,listZones[0].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[22],'MBP',listTerritory[1].Region_Code__c,listZones[1].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[23],'MBP',listTerritory[2].Region_Code__c,listZones[2].Zone_Code__c,1,null)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[0],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[1],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[2],'BGP',listTerritory[1].Region_Code__c,listTerritory[1].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[3],'BGP',listTerritory[2].Region_Code__c,listTerritory[2].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[4],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[5],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[6],'BGP',listTerritory[1].Region_Code__c,listTerritory[1].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[7],'BGP',listTerritory[2].Region_Code__c,listTerritory[2].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[8],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[9],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[10],'BGP',listTerritory[1].Region_Code__c,listTerritory[1].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[11],'BGP',listTerritory[2].Region_Code__c,listTerritory[2].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[12],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[13],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[14],'BGP',listTerritory[1].Region_Code__c,listTerritory[1].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[15],'BGP',listTerritory[2].Region_Code__c,listTerritory[2].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[16],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[17],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[18],'BGP',listTerritory[1].Region_Code__c,listTerritory[1].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[19],'BGP',listTerritory[2].Region_Code__c,listTerritory[2].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[20],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[21],'BGP',listTerritory[0].Region_Code__c,listTerritory[0].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[22],'BGP',listTerritory[1].Region_Code__c,listTerritory[1].Region_Code__c,1,listBuyingGroup)[0]);
        listPriceGrids.add(createPriceGridRecords(listProducts[23],'BGP',listTerritory[2].Region_Code__c,listTerritory[2].Region_Code__c,1,listBuyingGroup)[0]);
        insert listPriceGrids;
    }
    
    

    public static List<Product2> createProductRecords(Integer Quantity, String Category, String SubCategory, String prodCat){
        String strRecordType = [Select Id From RecordType Where Name='Residential Product' and sobjectType = 'Product2' Limit 1].Id;
        List<Product2> listProducts = New List<Product2>();
         for (Integer j=0; j< Quantity;j++){
             Product2 P = New Product2();
             P.RecordTypeId = strRecordType;
             P.Name = 'Product' + j;
             P.Category__c = Category;
             P.Sub_Category__c = SubCategory;
             P.Company__c = 'R';
             P.Inventory_Style_Num__c = 'abc' + SubCategory.substring(0, 1) + j;
             P.Product_Style_Number__c = 'abc' + SubCategory.substring(0, 1) + j;
             P.Backing__c = 'BK';
             P.Size__c = String.valueOf(1000+j);
             P.Residential_Product_Unique_Key__c = 'R.' + SubCategory.substring(0, 4) + j + '.' + 'BK.' + String.valueOf(1000 + j);  
             p.Brand_Code__c = SubCategory.substring(1, 2) + j;
             p.ERP_Product_Type__c = SubCategory.substring(0, 1); 
             p.Salesforce_Product_Category__c = prodCat;          
             listProducts.add(P);
         }
        return listProducts;
    }
    
    public static List<Price_Grid__c> createPriceGridRecords(Product2 Product, String GridType, String RegionCode, String ZoneCode, Integer Quantity, Buying_Group__c[] buyingGroups){
        List<Price_Grid__c> listPriceGrid = New List<Price_Grid__c>();
        for (Integer j=0; j< Quantity;j++){
            Price_Grid__c PG = New Price_Grid__c();
            PG.Grid_Type__c = GridType;
            PG.Import_External_Id__c = 'PriceGrid' + GridType + j + DateTime.now();
            PG.Product__c = Product.Id;
            PG.Region__c = RegionCode;
            PG.Zone__c = ZoneCode;
            PG.Company__c = Product.Company__c;
            PG.Inventory_Style_Num__c = Product.Inventory_Style_Num__c;
            PG.Selling_Style_Num__c = Product.Inventory_Style_Num__c;
            PG.Backing__c = Product.Backing__c;
            PG.Size__c = Product.Size__c;
            PG.Effective_Date__c = Date.Today().AddDays(-1);
            PG.Product_Unique_Key__c = Product.Residential_Product_Unique_Key__c;
            PG.Price_Grid_Unique_Key__c = RegionCode + '.' + ZoneCode + '.' + Product.Residential_Product_Unique_Key__c;
            //PG.Buying_Group_Number__c = j;
            if (buyingGroups != null){
                PG.Buying_Group__c = buyingGroups[Math.mod(j,buyingGroups.size())].Id;
            }
            
            listPriceGrid.add(PG);
        }
        return listPriceGrid;
    }

    @isTest
    static void testPriceGridAsTM(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.tm'),'Price_Grid',null,null,false);
    }

    @isTest
    static void testPriceGridAsSalesOps(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.salesops'),'Price_Grid',null,null,true);
    }

    @isTest
    static void testBuyingGroupGridAsTM(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.tm'),'Buying_Group_Grid',null,null,true);
    }

    @isTest
    static void testBuyingGroupGridAsSalesOps(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.salesops'),'Buying_Group_Grid',null,null,false);
    }

    @isTest
    static void testCPLAsTM(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.tm'),'Customer_Price_List',getAccountId('R.123456.0000'),null,false);
    }

    @isTest
    static void testCPLAsSalesOps(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.salesops'),'Customer_Price_List',getAccountId('R.123457.0000'),null,true);
    }

    @isTest
    static void testMVPLAsTM(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.tm'),'Merchandizing_Vehicle_Price_List',getAccountId('R.123456.0000'),getProductId(),true);
    }

    @isTest
    static void testMVPLAsSalesOps(){
        testBaseGridAsUser(getUserByUsername('testuser@mohawkind.com.salesops'),'Merchandizing_Vehicle_Price_List',getAccountId('R.123457.0000'),getProductId(),false);
    }
    
    static User getUserByUsername(String username){
        return [SELECT Id FROM User WHERE username = :username];
    }
    
    static void testBaseGridAsUser(User u, String gridType, String accountId, String productId, boolean isMobile){
        system.runAs(u){
            testGridByType(gridType,accountId,productId,isMobile);
        }
    }
    
    static Id getAccountId(String AccountNumber){
        Id accId;
        try{
            accId = [SELECT Id From Account WHERE CAMS_Account_Number__c = :AccountNumber].Id;
        }catch(Exception ex){
            
        }
        return accId;
    }
    
    static Id getProductId(){
        return [SELECT Id FROM Product2 WHERE Category__c = 'FLOORINGNACARPETPRODUCT' LIMIT 1].Id;
    }
    
    static void testGridByType(String gridType, String accountId, String productId, boolean isMobile){
        Test.startTest();
        if(accountId == null){
            accountId = getAccountId('R.123456.0000');
        }
        object[] baseGridResponse = Grid_BaseController.fetchProductCategories(gridType,accountId,productId);
        Grid_Product_Category__mdt[] categories = (Grid_Product_Category__mdt[]) baseGridResponse[1];
        String sObjectName = (String) baseGridResponse[0];
        String categoriesJSON = JSON.serialize(categories);
        
        if (UserInfo.getUserName() == 'testuser@mohawkind.com.salesops')
            system.assert(categories.size() == 12,categories.size() + ' product categories available.  Expected 12');
        
        for(integer i=0; i<categories.size(); i++)
        {
            // check every other product category.  Second time through will test the remaining ones.  Too many queries to test all 12 categories in a single test method.
            integer modCounter = i;
            if (isMobile)
                modCounter++;
            if (Math.mod(i,2) == 0)
                continue;
            String productCategoryId = categories[i].Id;
        
            Grid_BaseHelper.ViewData vdata = Grid_BaseController.fetchFields(productCategoryId,sObjectName,categoriesJSON,accountId);
            system.assert(vdata.listOfPrimaryFields.size() > 0,'No fields returned');
            system.assert(vdata.listOfSecondaryFilters.size() > 0,'No secondary filters returned.');
            if (gridType == 'Price_Grid' || gridType == 'Buying_Group_Grid')
                system.assert(vdata.listOfPrimaryFilters.size() > 0,'No primary filters returned.');
            else
                //system.assert(vdata.listOfPrimaryFilters.size() == 0,vdata.listOfPrimaryFilters.size() + ' primary filters returned.  Expected 0.');
                
            System.debug('isMobile: ' + isMobile);
            vdata.isMobile = isMobile;
            if (isMobile) //Removed vdata. - MB - 08/27/18
                vdata.searchKeyword = 'abc';
            
            if(accountId != null)
            	vdata.account = (Account) baseGridResponse[3];
            
            vdata.productId = productId;
            
            for (Grid_BaseHelper.FilterWrapper filter :vdata.listOfPrimaryFilters){
                if (filter.listOfFilterOptions.size() > 0)
                    filter.stringValue = filter.listOfFilterOptions[0].value;
                system.debug('FILTER: ' + filter.stringValue);
            }
            /*Start of Code - MB - 08/27/18 */
            if(vdata.sObjectName == 'Price_Grid__c'){
                vdata.account = null;
            }
            /*End of Code - MB - 08/27/18 */
            vdata = Grid_BaseController.fetchListOfData(JSON.serialize(vdata));
            
            system.assert(vdata.listOfData.size() > 0,'No data returned from fetch call');
            
            String warehouseValue;
            if (gridType == 'Price_Grid' && vdata.listOfPrimaryFilters.size() > 1)
                warehouseValue = vdata.listOfPrimaryFilters[1].stringValue;
            else if (gridType == 'Buying_Group_Grid' && vdata.listOfPrimaryFilters.size() > 2)
                warehouseValue = vdata.listOfPrimaryFilters[2].stringValue;
            
            Grid_BaseHelper.ViewData secondarydata = Grid_BaseController.fetchSecondaryDisplayData(((sObject[]) vdata.listOfData)[0].Id,vdata.sObjectName,productCategoryId,warehouseValue,accountId,vdata.productId,'Merch Vehicle Price List')[0];
            system.assert(secondarydata.listOfData.size() == 1,secondarydata.listOfData.size() + ' secondary records returned.  Expected exactly 1.');
            system.assert(secondarydata.listOfSecondaryFields.size() > 0,'No secondary fields returned.');
        }
        Test.stopTest();
    }
    
    @istest
    static void testGridTypePicklist(){
        Grid_GridTypePicklist gtp = new Grid_GridTypePicklist();
        VisualEditor.DataRow dr = gtp.getDefaultValue();
        String val = ((String) dr.getValue());
        system.assert(val == 'Price_Grid','Default value is not correct.  Expected "Price_Grid", received "' + val + '"');
        
        VisualEditor.DynamicPicklistRows dplr = gtp.getValues();
        Integer size = dplr.size();
        System.assert(size == 4,'Incorrect number of picklist values.  Expected 4, received ' + size);
    }
}