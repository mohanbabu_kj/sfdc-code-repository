/**************************************************************************

Name : FreightRateCalloutMock_Test 

===========================================================================
Purpose :  Create JSON for Freight Rate
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        14/Feb/2017     Created 
***************************************************************************/
@isTest
global class FreightRateCalloutMock_Test implements HttpCalloutMock  {

     global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody(+'{'
                          +'"MAIN": {'
                                +'"IPACCOUNT": "R.102318.0000",'
                                +'"OACCOUNTNAME": "Hockstein Inc.",'
                                +'"ODATETIME": "*February 14 - 2017, 04:45 AM EST*",'
                                +'"OMESSAGE": null,'
                                +'"OFREIGHTDATA": ['
                                    +'{'
                                        +'"TACTICALCAT": "Soft Surface",'
                                        +'"PRODTYPE": "Residential",'
                                        +'"FREIGHTRATE": ".30",'
                                        +'"MINFREIGHT": "30.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Soft Surface",'
                                        +'"PRODTYPE": "Mainstreetcommercial",'
                                        +'"FREIGHTRATE": ".30",'
                                        +'"MINFREIGHT": "30.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Soft Surface",'
                                        +'"PRODTYPE": "Carpet Tile",'
                                        +'"FREIGHTRATE": ".91",'
                                        +'"MINFREIGHT": "46.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Cushion",'
                                        +'"PRODTYPE": "Padding",'
                                        +'"FREIGHTRATE": ".00",'
                                        +'"MINFREIGHT": "47.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Hard Surface",'
                                        +'"PRODTYPE": "Engineered Wood",'
                                        +'"FREIGHTRATE": null,'
                                        +'"MINFREIGHT": "54.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Hard Surface",'
                                        +'"PRODTYPE": "Solid Wood",'
                                        +'"FREIGHTRATE": null,'
                                        +'"MINFREIGHT": "54.00"' 
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Hard Surface",'
                                        +'"PRODTYPE": "Ceramic Tile",'
                                        +'"FREIGHTRATE": null,'
                                        +'"MINFREIGHT": "54.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Hard Surface",'
                                        +'"PRODTYPE": "Laminate",'
                                        +'"FREIGHTRATE": null,'
                                        +'"MINFREIGHT": "54.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Hard Surface",'
                                        +'"PRODTYPE": "Vinyl Broadloom",'
                                        +'"FREIGHTRATE": null,'
                                        +'"MINFREIGHT": "54.00"'
                                    +'},'
                                    +'{'
                                        +'"TACTICALCAT": "Hard Surface",'
                                        +'"PRODTYPE": "Vinyl Tile",'
                                        +'"FREIGHTRATE": null,'
                                        +'"MINFREIGHT": "54.00"'
                                    +'}'
                                +']'
                            +'}'
                        +'}');

           return res;
      }//end of Method
}//End of Class