/**************************************************************************

Name : relatedAccountDetailController_Test

===========================================================================
Purpose : Test Class for relatedAccountDetailController
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha       12/June/2018     Created 
***************************************************************************/
@isTest
private class relatedAccountDetailController_Test { 
  
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            List<String> getPickListValuesIntoList=relatedAccountDetailController.getPickListValuesIntoList();
            List<Account> accList=Utility_Test.createCommercialAccountsForInvoicing(true,1);
            List<Opportunity> optyList=Utility_Test.createOpportunities(true,1);
            List<Contact> contactList=Utility_Test.createContacts(true,1,accList);
            List<Related_Account__c> relAccList=Utility_Test.createRelatedAccountList(false,1,accList,optyList);
            relatedAccountDetailController.createRARecord(relAccList[0],accList[0].id,optyList[0].id,contactList);
            relatedAccountDetailController.getOpptyFromIdId(optyList[0].id);

            test.stopTest();  
        }
    }
        static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            List<String> getPickListValuesIntoList=relatedAccountDetailController.getPickListValuesIntoList();
            List<Account> accList=Utility_Test.createCommercialAccountsForInvoicing(false,1);
            accList[0].Strategic_Account__c=true;
            insert accList;
            accList.add(accList[0]);
            List<Opportunity> optyList=Utility_Test.createOpportunities(true,1);
            List<Contact> contactList=Utility_Test.createContacts(true,2,accList);
            List<Related_Account__c> relAccList=Utility_Test.createRelatedAccountList(false,1,accList,optyList);
            relatedAccountDetailController.createRARecord(relAccList[0],accList[0].id,optyList[0].id,contactList);
            relatedAccountDetailController.getOpptyFromIdId(optyList[0].id);
			relatedAccountDetailController.checkAccountisStrategic(accList[0].id);
            test.stopTest();  
        }
    }
    
}