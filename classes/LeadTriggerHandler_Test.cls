/**************************************************************************

Name : LeadTriggerHandler_Test

===========================================================================
Purpose : Uitlity class to LeadTriggerHandler  test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand          6/Jan/2016     Created         
2.0        Lester         10/Jan/2016   Updated                 
***************************************************************************/
@isTest
private class LeadTriggerHandler_Test {
    
    static List<Lead> leadList;
    
    static List<Territory_Geo_Code__c> territoryGeoCodeList;
    static List<Account> accList;
    static List<Territory__c> terrList;
    static List<Territory_User__c>  terrUser;
    static List<Territory_Geo_Code__c> terrgeocode;
    static Set<id> idList;
    static List<Account> resAccListForInvoicing;
    static List<Account> accountList;
    static List<Territory_Geo_Code__c> tGeoCodeList;
    static List<Territory_User__c> tUserList;
    static List<Product2> invoivingproducts;
    static Territory__c terr;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        Utility_Test.getDefaultConfCusSetting(); // Added by MB - 07/10/17
        String TerrRecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();   
        
        terr =  new Territory__c(Name = 'Test Territory',recordTypeId=TerrRecordTypeId);
        
        insert terr ;
        system.assert(terr.id!=null);
        tGeoCodeList = Utility_Test.createTerritoryGeoCodes(false, 2);
        
        for(Territory_Geo_Code__c tgeo :tGeoCodeList){
            tgeo.Name='12345678';
            tgeo.territory__c=terr.Id;
            // tgeo.Business_Type__c=UtilityCls.COMMERCIAL;
            
        }
        insert tGeoCodeList;
        tUserList = Utility_Test.createTerritoryUsers(false, 1);
        for(Territory_User__c tusr: tUserList){
            tusr.Role__c = UtilityCls.AE;
            tusr.territory__c=terr.Id;
        }
        invoivingproducts = Utility_Test.createProduct2(true,1);
        String RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(UtilityCls.LABELREDIDENTALDISPLAY).getRecordTypeId();   
        for(Integer i=0;i<invoivingproducts.size();i++){
            invoivingproducts[i].Buying_Group__c = 'Abbey (IA)';
            invoivingproducts[i].RecordTypeId = RecordTypeId;
            invoivingproducts[i].IsActive = true;
            invoivingproducts[i].External_Id__c = 'ext id';
            
        }
        update invoivingproducts;
    }
    
    
    static testMethod void testMethod0() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            init();
            UtilityCls.isTriggerFired = false;
            
            User user = utility_test.getTestUser('Test1', null);
            user.segment__c='Corporate';
            INSERT USER;
            
            User user1 = utility_test.getTestUser('Test2', null);
            user1.segment__c='Corporate';
            INSERT USER1;
            
            List<Territory_User__c> territoryUserList = new List<Territory_User__c>();
            
            List<Territory_Geo_Code__c> terrgeo1=Utility_Test.createTerritoryGeoCodes(false,3);
            for(Territory_Geo_Code__c tgo : terrgeo1){
                tgo.Name='12345test678';
                tgo.territory__c=terr.id;
            }
            insert terrgeo1;
            system.assert(terrgeo1.size()>0);
            // Name is Auto Number (Data Type). 
            
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = UtilityCls.AE, User__c = user.Id)); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = UtilityCls.AE, User__c = user1.Id)); 
            
            insert territoryUserList;
            accList = Utility_Test.createAccounts(true,3);
            
            id acc= accList.get(0).id;
            leadList  = Utility_Test.createLeads(true, 3);
            idList = new Set<id>();
            id newResRTId = UtilityCls.getRecordTypeInfo('Lead','New Residential');
            
            for(Lead lead:leadList){
                lead.Account__c = null; 
                lead.postalcode='12345test678';
                lead.RecordTypeId=  UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial');
                lead.Architect_Zip_Code__c='12345test678';
                lead.From_Other_System__c=false;
                lead.Territory__c=terr.id;
                lead.Market_Segments__c='Corporate';
                idList.add(lead.id);
                
                //  LastName = 'Test Lead'+i, Company='Test Account'+i ,PostalCode='30047',Architect_Zip_Code__c='30047',Email='praveen_sub@mohawkkind.com', RecordTypeId = UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial')  ));                 
                
            }
            update leadList;
            LeadTriggerHandler.zipCodeMap(leadList);
            LeadTriggerHandler.leadAssignMentForSF1((set<id>)idList);
            List<Territory_User__c> terruser=Utility_Test.createTerritoryUsers(true,3);
            List<Territory_Geo_Code__c> terrgeo=Utility_Test.createTerritoryGeoCodes(true,3);
            //List<lead> leadList=Utility_Test.createLeads(true,3);
            LeadTriggerHandler.initTerritoryUser(terruser.get(0).id);
            // LeadTriggerHandler.handleMultipleTerritory(leadList,ds);
            LeadTriggerHandler.UpdateTerritoryFromZipCode(leadList, true, true);
            LeadTriggerHandler.changeRTtoMasterOpportunity(leadList);
            LeadTriggerHandler.territoryUsers(leadList);
            LeadTriggerHandler.roundRobinAssignment(leadList);
            
            test.stopTest();
        }
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            init();
            // Boolean b = Boolean.valueOf('true');
            //System.assertEquals(true,b);
            accList = Utility_Test.createAccounts(true,3);
            
            id acc= accList.get(0).id;
            leadList  = Utility_Test.createLeads(true, 3);
            idList = new Set<id>();
            id newResRTId = UtilityCls.getRecordTypeInfo('Lead','New Residential');
            
            for(Lead lead:leadList){
                lead.Account__c = acc; 
                lead.RecordTypeId=  newResRTId;
                
                idList.add(lead.id);
                
                //  LastName = 'Test Lead'+i, Company='Test Account'+i ,PostalCode='30047',Architect_Zip_Code__c='30047',Email='praveen_sub@mohawkkind.com', RecordTypeId = UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial')  ));                 
                
            }
            update leadList;
            system.assert(leadList.size()>0);
            
            LeadTriggerHandler.zipCodeMap(leadList);
            LeadTriggerHandler.leadAssignMentForSF1((set<id>)idList);
            List<Territory_User__c> terruser=Utility_Test.createTerritoryUsers(true,3);
            List<Territory_Geo_Code__c> terrgeo=Utility_Test.createTerritoryGeoCodes(true,3);
            //List<lead> leadList=Utility_Test.createLeads(true,3);
            LeadTriggerHandler.initTerritoryUser(terruser.get(0).id);
            // LeadTriggerHandler.handleMultipleTerritory(leadList,ds);
            LeadTriggerHandler.UpdateTerritoryFromZipCode(leadList, true, true);
            LeadTriggerHandler.changeRTtoMasterOpportunity(leadList);
            
            test.stopTest();
        }
    }
    
    //Written By susmitha
    static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            init();
            accList = Utility_Test.createAccounts(false,3);
            for(account a : accList){
                a.Account_Focus__c = UtilityCls.TARGET;
                a.ABC_Classification__c = 'A';
            }
            insert accList;
            system.assert(accList.size()>0);
            
            list<Territory_Geo_Code__c> tGeoCodeList1 = Utility_Test.createTerritoryGeoCodes(false, 2);
            
            for(Territory_Geo_Code__c tgeo :tGeoCodeList1){
                tgeo.Name='123456780';
                tgeo.territory__c=terr.Id;
                // tgeo.Business_Type__c=UtilityCls.COMMERCIAL;
                
            }
            insert tGeoCodeList1;
            id acc= accList.get(0).id;
            leadList  = Utility_Test.createLeads(false, 3);
            idList = new Set<id>();
            for(Lead lead:leadList){
                lead.Account__c = acc; 
                lead.RecordTypeId=  UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial');
                lead.PostalCode='123456780';
                lead.Architect_Zip_Code__c='123456780';
                lead.From_Other_System__c=false;
                lead.street='test street';
                lead.city='test city';
                lead.country='togo';
                
                
                idList.add(lead.id);
            }
            insert leadList[0];
            
            
            update leadList[0];
            
            // LeadTriggerHandler.zipCodeMap(leadList);
            // LeadTriggerHandler.leadAssignMentForSF1((set<id>)idList);
            // List<Territory_User__c> terruser=Utility_Test.createTerritoryUsers(true,3);
            // List<Territory_Geo_Code__c> terrgeo=Utility_Test.createTerritoryGeoCodes(true,3);
            // List<lead> leadList=Utility_Test.createLeads(true,3);
            //LeadTriggerHandler.initTerritoryUser(terruser.get(0).id);
            // LeadTriggerHandler.handleMultipleTerritory(leadList,ds);
            // LeadTriggerHandler.UpdateTerritoryFromZipCode(leadList, true, true);
            // LeadTriggerHandler.changeRTtoMasterOpportunity(leadList);
            
            test.stopTest();
        }
    }
    //Written By susmitha
    static testMethod void testMethod3() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            init();
            accList = Utility_Test.createAccounts(true,3);
            system.assert(accList.size()>0);
            
            id acc= accList.get(0).id;
            leadList  = Utility_Test.createLeads(false, 3);
            idList = new Set<id>();
            for(Lead lead:leadList){
                lead.Account__c = null; 
                lead.RecordTypeId=  UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial');
                lead.PostalCode='12345678';
                lead.Architect_Zip_Code__c='12345678';
                lead.From_Other_System__c=false;
                
                idList.add(lead.id);
            }
            insert leadList;
            
            
            update leadList;
            
            // LeadTriggerHandler.zipCodeMap(leadList);
            // LeadTriggerHandler.leadAssignMentForSF1((set<id>)idList);
            // List<Territory_User__c> terruser=Utility_Test.createTerritoryUsers(true,3);
            // List<Territory_Geo_Code__c> terrgeo=Utility_Test.createTerritoryGeoCodes(true,3);
            // List<lead> leadList=Utility_Test.createLeads(true,3);
            //LeadTriggerHandler.initTerritoryUser(terruser.get(0).id);
            // LeadTriggerHandler.handleMultipleTerritory(leadList,ds);
            // LeadTriggerHandler.UpdateTerritoryFromZipCode(leadList, true, true);
            // LeadTriggerHandler.changeRTtoMasterOpportunity(leadList);
            
            test.stopTest();
        }
    }
    Static testMethod void LeadBeforeupdate(){
        accList=Utility_Test.createAccounts(true, 3);
        
        Test.StartTest(); 
        init();
        Contact TestCon= new Contact();
        TestCon.FirstName='TestFIrst';
        TestCon.LastName='TestLastname';
        TestCon.Email='test@gmail.com';
        insert TestCon;
        
        Lead TestLead  = new Lead();
        TestLead.IsConverted=true;
        //  TestLead.ConvertedContactId=TestCon.Id;
        // TestLead.ConvertedAccountId=accList.get(0).id;
        TestLead.Email='test@gmail.com';  
        TestLead.LastName='dddd';
        TestLead.Company='test';
        TestLead.PostalCode='12345678';
        TestLead.Street='DF';
        TestLead.city='DFD';
        TestLead.country='Togo';
        
        TestLead.Architect_Zip_Code__c='12345678';
        TestLead.RecordTypeId=  UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial');
        TestLead.From_Other_System__c=false;
        try{
            insert TestLead;
            system.assert(TestLead.id!=null);
            
            update TestLead;      
        }
        catch(exception e){
            system.debug('error e'+e);
        }
        Test.StopTest();
        
        
    }
    //Written By susmitha
    static testMethod void testMethod4() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            init();
            accList = Utility_Test.createAccounts(true,3);
            system.assert(accList.size()>0);
            id acc= accList.get(0).id;
            leadList  = Utility_Test.createLeads(false, 3);
            idList = new Set<id>();
            for(Lead lead:leadList){
                // lead.Account__c = acc; 
                lead.RecordTypeId=  UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial');
                lead.PostalCode='12345test678';
                lead.Architect_Zip_Code__c='12345test678';
                lead.From_Other_System__c=false;
                lead.Territory__c=terr.id;
                
                idList.add(lead.id);
            }
            insert leadList;
            
            
            update leadList;
            
            // LeadTriggerHandler.zipCodeMap(leadList);
            // LeadTriggerHandler.leadAssignMentForSF1((set<id>)idList);
            // List<Territory_User__c> terruser=Utility_Test.createTerritoryUsers(true,3);
            // List<Territory_Geo_Code__c> terrgeo=Utility_Test.createTerritoryGeoCodes(true,3);
            // List<lead> leadList=Utility_Test.createLeads(true,3);
            //LeadTriggerHandler.initTerritoryUser(terruser.get(0).id);
            // LeadTriggerHandler.handleMultipleTerritory(leadList,ds);
            // LeadTriggerHandler.UpdateTerritoryFromZipCode(leadList, true, true);
            // LeadTriggerHandler.changeRTtoMasterOpportunity(leadList);
            
            test.stopTest();
        }
    }
    static testMethod void testMethod5() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            init();
            UtilityCls.isTriggerFired = false;
            User user = utility_test.getTestUser('Test1', null);
            user.segment__c='Corporate';
            INSERT USER;
            User user1 = utility_test.getTestUser('Test2', null);
            user1.segment__c='Corporate';
            INSERT USER1;
            List<Territory_User__c> territoryUserList = new List<Territory_User__c>();
            
            List<Territory_Geo_Code__c> terrgeo1=Utility_Test.createTerritoryGeoCodes(false,3);
            for(Territory_Geo_Code__c tgo : terrgeo1){
                tgo.Name='12345test678';
                tgo.territory__c=terr.id;
            }
            insert terrgeo1;
            system.assert(terrgeo1.size()>0);
            
            // Name is Auto Number (Data Type). 
            
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = UtilityCls.AE, User__c = user.Id)); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = UtilityCls.AE, User__c = user1.Id)); 
            
            insert territoryUserList;
            
            
            system.debug('territoryUserList <<<<<<<'+territoryUserList);
            accList = Utility_Test.createAccounts(true,3);
            
            id acc= accList.get(0).id;
            leadList  = Utility_Test.createLeads(false, 3);
            idList = new Set<id>();
            for(Lead lead:leadList){
                lead.RecordTypeId=  UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial');
                lead.PostalCode='12345test678';
                lead.Architect_Zip_Code__c='12345test678';
                lead.From_Other_System__c=false;
                lead.Territory__c=terr.id;
                lead.Market_Segments__c='Corporate';
                
                idList.add(lead.id);
            }
            insert leadList;
            system.assert(leadList.size()>0);
            
            
            update leadList;
            
            accList = Utility_Test.createAccounts(true,3);
            
            // id acc= accList.get(0).id;
            //leadList  = Utility_Test.createLeads(true, 3);
            idList = new Set<id>();
            id newResRTId = UtilityCls.getRecordTypeInfo('Lead','New Residential');
            
            for(Lead lead:leadList){
                lead.Account__c = acc; 
                lead.RecordTypeId=  newResRTId;
                
                idList.add(lead.id);
                
                //  LastName = 'Test Lead'+i, Company='Test Account'+i ,PostalCode='30047',Architect_Zip_Code__c='30047',Email='praveen_sub@mohawkkind.com', RecordTypeId = UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial')  ));                 
                
            }
            update leadList;
            LeadTriggerHandler.zipCodeMap(leadList);
            LeadTriggerHandler.leadAssignMentForSF1((set<id>)idList);
            List<Territory_User__c> terruser=Utility_Test.createTerritoryUsers(true,3);
            List<Territory_Geo_Code__c> terrgeo=Utility_Test.createTerritoryGeoCodes(true,3);
            //List<lead> leadList=Utility_Test.createLeads(true,3);
            LeadTriggerHandler.initTerritoryUser(terruser.get(0).id);
            // LeadTriggerHandler.handleMultipleTerritory(leadList,ds);
            LeadTriggerHandler.UpdateTerritoryFromZipCode(leadList, true, true);
            LeadTriggerHandler.changeRTtoMasterOpportunity(leadList);
            
            test.stopTest();
        }
    }
}