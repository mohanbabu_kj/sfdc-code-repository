/**************************************************************************

Name : AssignSalesActionTerritory_CC_Test

===========================================================================
Purpose : This tess class is used for AssignSalesActionTerritory_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         3/March/2017    Add
***************************************************************************/
@isTest
private class AssignSalesActionTerritory_CC_Test {
    static List<Account> resAccListForInvoicing;
    static List<Account> comAccListForInvoicing;
    static List<Account> tempAccsList;
    static List<Territory__c> terrList;
    static List<Territory__c> terrListWithTypeAndRegion  = new  List<Territory__c>();
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<Action_List__c> actListForCommericalFL;
    static List<Action_List__c> actListForCommericalSAL;
    static List<Action_List__c> actListForCommerical = new List<Action_List__c>();
    
    static List<SAL_Territory__c>  salTerr;
    static List<SAL_Territory__c>  comSalTerr;
    static List<SAL_Territory__c>  salTerrWithTypeAndRegion = new List<SAL_Territory__c>();
    static List<Id> terrsIdsList = new List<Id>();
    static List<Id> uncheckedIdList = new List<Id>();
    
    static testMethod void testMethod1() {
        test.startTest();
        System.runAs(Utility_Test.ADMIN_USER){
            Utility_Test.getTeamCreationCusSetting();
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList!=null);
            Id terr1 = terrList.get(0).Id;
            Id terr2 = terrList.get(1).Id;
            
            terrsIdsList.add(terr1);
            terrsIdsList.add(terr2);
            
            actListForResidentialFL = Utility_Test.createActionListForResidentialFL(true, 1);
            actListForResidentialSAL = Utility_Test.createActionListForResidentialSAL(true, 1);
            actListForResidential.add(actListForResidentialFL.get(0));
            actListForResidential.add(actListForResidentialSAL.get(0));
            salTerr = Utility_Test.createSALTerritory(true, 2, actListForResidential, terrList);
            for(SAL_Territory__c sal : salTerr){
                uncheckedIdList.add(sal.Id);
            }
            
            String actListForResidentialFLId = String.valueOf(actListForResidentialFL.get(0).Id);
            String actListForResidentialSALId = String.valueOf(actListForResidentialSAL.get(0).Id);
            AssignSalesActionTerritory_CC.TerritoryModel terrModel = new AssignSalesActionTerritory_CC.TerritoryModel(terrList.get(0));
            
            AssignSalesActionTerritory_CC.tempTerritory(actListForResidentialFLId);
            AssignSalesActionTerritory_CC.existingTerritoies(actListForResidentialFLId);
            AssignSalesActionTerritory_CC.getPicklistFieldValues('Territory__c', 'Type__c');
            AssignSalesActionTerritory_CC.createSalTerritories(actListForResidentialSALId, terrsIdsList,uncheckedIdList);
        }
        test.stopTest();
    }
    
    static testMethod void testMethod2() {
        test.startTest();
        Utility_Test.getTeamCreationCusSetting();
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        system.assert(resAccListForInvoicing!=null);
        
        //  String resiRegRecorType = UtilityCls.getInfo('Territory__c','Residential Regions');
        String resiRegRecorType = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get('Residential Regions').getRecordTypeId();   
        terrListWithTypeAndRegion.add(new Territory__c(Name = 'Test Territory With Type and Region 1', RecordTypeId=resiRegRecorType, Type__c = 'Soft Surface', Region__c = '212 - Canada')); 
        terrListWithTypeAndRegion.add(new Territory__c(Name = 'Test Territory With Type and Region 2', RecordTypeId=resiRegRecorType, Type__c = 'Soft Surface', Region__c = '212 - Canada')); 
        insert terrListWithTypeAndRegion;
        actListForResidentialFL = Utility_Test.createActionListForResidentialFL(true, 1);
        String actListForResidentialFLId = String.valueOf(actListForResidentialFL.get(0).Id);
        actListForResidentialSAL = Utility_Test.createActionListForResidentialSAL(true, 1);
        
        actListForResidential.add(actListForResidentialFL.get(0));
        actListForResidential.add(actListForResidentialSAL.get(0));
        
        salTerrWithTypeAndRegion = Utility_Test.createSALTerritory(true, 2, actListForResidential, terrListWithTypeAndRegion);
        
        AssignSalesActionTerritory_CC.retrieveTerritories(actListForResidentialFLId,new List<String>{'Soft Surface'}, new List<String>{'212 - Canada'} );
        
        test.stopTest();
    }
    
    static testMethod void testMethod3() {
        test.startTest();
        Utility_Test.getTeamCreationCusSetting();
        comAccListForInvoicing = Utility_Test.createCommercialAccountsForInvoicing(true, 1);
        system.assert(comAccListForInvoicing!=null);
        
        terrList = Utility_Test.createTerritories(true, 2);
        
        Id terr1 = terrList.get(0).Id;
        Id terr2 = terrList.get(1).Id;
        
        terrsIdsList.add(terr1);
        terrsIdsList.add(terr2);
        
        actListForCommericalFL = Utility_Test.createActionListForCommercialFL(true, 1);
        actListForCommericalSAL = Utility_Test.createActionListForCommercialSAL(true, 1);
        actListForCommerical.add(actListForCommericalFL.get(0));
        actListForCommerical.add(actListForCommericalSAL.get(0));
        comSalTerr = Utility_Test.createSALTerritory(true, 2, actListForCommerical, terrList);
        for(SAL_Territory__c sal : comSalTerr){
            uncheckedIdList.add(sal.Id);
        }
        System.assert(comSalTerr != null);
        
        String actListForCommericalFLId = String.valueOf(actListForCommericalFL.get(0).Id);
        String actListForCommericalSALId = String.valueOf(actListForCommericalSAL.get(0).Id);
        AssignSalesActionTerritory_CC.TerritoryModel terrModel = new AssignSalesActionTerritory_CC.TerritoryModel(terrList.get(0));
        
        AssignSalesActionTerritory_CC.tempTerritory(actListForCommericalFLId);
        AssignSalesActionTerritory_CC.existingTerritoies(actListForCommericalFLId);
        AssignSalesActionTerritory_CC.getPicklistFieldValues('Account', 'Industry');
        AssignSalesActionTerritory_CC.createSalTerritories(actListForCommericalSALId, terrsIdsList, uncheckedIdList);
        test.stopTest();
    }
    /*
static testMethod void testGetDependent(){
test.startTest();
Map<String, List<String>> dependentMap =  AssignSalesActionTerritory_CC.getDependentPicklistOptions('Territory__c', 'Type__c', 'Region__c' , 'Soft Surface');
test.stopTest();
}*/
}