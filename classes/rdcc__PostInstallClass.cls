/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PostInstallClass implements System.InstallHandler {
    global PostInstallClass() {

    }
    global void addInsightAPIDetails() {

    }
    global void createConfigSetting() {

    }
    global void createDataImportSetting() {

    }
    global void installInsightRoles() {

    }
    global void installLuTradeCodes() {

    }
    global void installStateCodes() {

    }
    global void onInstall(System.InstallContext context) {

    }
    global void scheduleBuyerInfoBatch() {

    }
    global void scheduleErrorLogBatch() {

    }
}
