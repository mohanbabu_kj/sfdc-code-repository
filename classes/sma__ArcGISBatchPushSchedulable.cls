/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ArcGISBatchPushSchedulable implements System.Schedulable, sma.MASchedulable {
    global ArcGISBatchPushSchedulable() {

    }
    global void execute(System.SchedulableContext sc) {

    }
}
