public class UserTriggerHandler {
    
    public static void updateUserInfo(List<User> newList, Map<Id, User> oldMap, Boolean iflag, Boolean uflag){
        Set<String> mgrCAMSId = new Set<String>();        
        Map<String, Id> mgrCAMSIdMap = new Map<String, Id>();
        for(User usr: newList){
            User oldUser = oldMap != null ? oldMap.get(usr.Id) : null;
            String oldMgrId = oldUser != null ? oldUser.Manager_CAMS_Id__c : null;
            if((usr.Manager_CAMS_Id__c != null && iflag) || 
               (usr.Manager_CAMS_Id__c != null && oldMgrId != null && usr.Manager_CAMS_Id__c != oldMgrId && uflag)){
                   mgrCAMSId.add(usr.Manager_CAMS_Id__c);
               }else if(usr.Manager_CAMS_Id__c == null && oldMgrId != null && uflag){
                   usr.ManagerId = null;
               }else if( oldMgrId == null && usr.Manager_CAMS_Id__c != null && uflag ){
                    mgrCAMSId.add(usr.Manager_CAMS_Id__c);
               }
               
            if(usr.NickName__c != null && usr.Business_Type__c == 'Residential'){
                usr.FirstName = usr.NickName__c;
            }
        }
        
        if(mgrCAMSId.size() > 0){
            for(List<User> userList: [SELECT Id, CAMS_Employee_Number__c FROM User WHERE CAMS_Employee_Number__c IN: mgrCAMSId]){
                for(User usr: userList){
                    mgrCAMSIdMap.put(usr.CAMS_Employee_Number__c, usr.Id);
                }
            }
            for(User usr: newList){
                Id managerId = mgrCAMSIdMap.containsKey(usr.Manager_CAMS_Id__c) ? mgrCAMSIdMap.get(usr.Manager_CAMS_Id__c) : null;
                if(managerId != null) usr.ManagerId = managerId;
            }
        }
    }
}