/**************************************************************************

Name : TerritoryUserTriggerHandler 

===========================================================================
Purpose : This class is used for Territory User Trigger.
===========================================================================
History:
--------
VERSION    	AUTHOR         	DATE           	DETAIL          DESCRIPTION
1.0        	Sasi Naik     	10/APR/2017    	Created
1.1			Mohan Babu		11/19/2018		Modified		Commented old code and added new logic to delete duplicate TM entries.

***************************************************************************/

public with sharing class TerritoryUserTriggerHandler {

    public static Id resRecordTypeId = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
    // Validate duplicate TM Number if there is any change in district
    public static void validateDuplicate(Map<Id, Territory_User__c> newListMap) {
        
        Set<String> tmNumSet = new Set<String>();
        Set<String> externalIDSet = new Set<String>();
        List<Id> tUserIdToDelete = new List<Id>();
        
        for(Territory_User__c tUser: newListMap.values()){
            tmNumSet.add(tUser.Name);
            externalIDSet.add(tUser.External_ID__c);
        }
        if(tmNumSet.size() > 0 && externalIDSet.size() > 0){
            for(List<Territory_User__c> tUserList : [SELECT Id, Name FROM Territory_User__c 
                                                     WHERE Name IN: tmNumSet 
                                                     AND External_ID__c NOT IN: externalIDSet
                                                     AND Role__c = 'Territory Manager']){
                for(Territory_User__c tUser: tUserList){
                    tUserIdToDelete.add(tUser.Id);
                }
            }
            System.debug('Delete List: ' + tUserIdToDelete);
            if(tUserIdToDelete.size() > 0){
                List<Database.DeleteResult> deleteResults = Database.delete(tUserIdToDelete, false);
            }
        }
    }
    
    /* Start of Code - MB - Bug 70120 - 1/28/19 */
    public static void createUserAssociationInTerritoryModel(List<Territory_User__c> newTerrUserList){
        Set<String> roleCodeSet = new Set<String>();
        List<Territory2> terr2List = new List<Territory2>();
        Map<String, Id> roleCodeIdMap = new Map<String, Id>();
        Set<Id> userIdSet = new Set<Id>();
        Set<Id> terr2IdSet = new Set<Id>();
        Set<String> terr2IdUserIdSet = new Set<String>();
        List<UserTerritory2Association> usrTerr2AssociationList = new List<UserTerritory2Association>();
        
        for(Territory_User__c tUser: newTerrUserList){
            if(tUser.Role__c != null && resRecordTypeId == tUser.RecordTypeId && tUser.User__c != null){
                String roleCode = getRoleCode(tUser);
                if(roleCode != null){ roleCodeSet.add(roleCode); }
                userIdSet.add(tUser.User__c);
            }
        }

        if(roleCodeSet.size()>0){
            for(List<Territory2> tList: [SELECT Id, DeveloperName FROM Territory2 WHERE DeveloperName IN: roleCodeSet]){
                for(Territory2 terr: tList){
                    terr2IdSet.add(terr.Id);
                    roleCodeIdMap.put(terr.DeveloperName, terr.Id);
                }
            } 
        }
        
        if(userIdSet.size() > 0 && terr2IdSet.size() > 0){
            for(List<UserTerritory2Association> usrTerr2List : [SELECT Id, Territory2Id, UserId from UserTerritory2Association WHERE UserId IN: userIdSet AND Territory2Id IN: terr2IdSet]){
                for(UserTerritory2Association usrTerr2: usrTerr2List){
                    terr2IdUserIdSet.add(usrTerr2.Territory2Id + '_' + usrTerr2.UserId);
                }
            }
        }
        if(roleCodeIdMap.size()>0){
            for(Territory_User__c tUser: newTerrUserList){
                if(tUser.Role__c != null && resRecordTypeId == tUser.RecordTypeId && tUser.User__c != null){
                    UserTerritory2Association usrTerr2Association = new UserTerritory2Association();
                    String roleCode;
                    Id terr2Id;
                    roleCode = getRoleCode(tUser);
                    terr2Id = (roleCode != null && roleCodeIdMap.containsKey(roleCode)) ? roleCodeIdMap.get(roleCode) : null;
                    if(terr2Id != null){
                        String terr2IdUserId = terr2Id + '_' + tUser.User__c;
                        if(!terr2IdUserIdSet.contains(terr2IdUserId)){
                            usrTerr2Association.Territory2Id = terr2Id;
                            usrTerr2Association.UserId = tUser.User__c;
                            usrTerr2AssociationList.add(usrTerr2Association);
                        }
                    }
                }
            }
        }
        
        if(usrTerr2AssociationList.size()>0){
            List<Database.UpsertResult> results = Database.upsert(usrTerr2AssociationList);
            UtilityCls.createUpsertDMLexceptionlog(usrTerr2AssociationList,results,'TerrtitoryTriggerHandler','createUserAssociationInTerritoryModel');
        }
    }
    
    public static String getRoleCode(Territory_User__c tUser){
        String roleCode;
        switch on tUser.Role__c {
            when 'Territory Manager' {
                roleCode = 'D' + tUser.Territory_Code__c + '_TM';
            }
            when 'District Manager' {
                roleCode = 'D' + tUser.Territory_Code__c;
            }
            when 'Business Development Manager' {
                String regionCode = tUser.Region_Code__c;
                roleCode = regionCode.startsWith('R') ? tUser.Region_Code__c : 'R' + tUser.Region_Code__c;
            }
            when 'Regional Vice President' {
                String regionCode = tUser.Region_Code__c;
                roleCode = regionCode.startsWith('R') ? tUser.Region_Code__c : 'R' + tUser.Region_Code__c;
            }
            when 'Senior Vice President' {
                String sectorCode = tUser.Sector_Code__c;
                roleCode = sectorCode.startsWith('S') ? tUser.Sector_Code__c : 'S' + tUser.Sector_Code__c;
            }
        }
        return roleCode;
    }
    
    public static void updateUserAssociationInTerritoryModel(Map<Id, Territory_User__c> newTerrUserMap, Map<Id, Territory_User__c> oldTerrUserMap){
        List<Territory_User__c> createUserTerr2AssociationList = new List<Territory_User__c>();
        List<Territory_User__c> deleteUserTerr2AssociationList = new List<Territory_User__c>();
        for(Territory_User__c tUserNew: newTerrUserMap.values()){
            Territory_User__c tUserOld = oldTerrUserMap.containsKey(tUserNew.Id) ? oldTerrUserMap.get(tUserNew.Id) : null;
            if(tUserOld != null && resRecordTypeId == tUserOld.RecordTypeId){
                if(tUserOld.Role__c != null && tUserNew.Role__c != null && 
                   tUserOld.User__c != null && tUserNew.User__c != null &&
                  ((tUserOld.Role__c != tUserNew.Role__c && tUserOld.User__c == tUserNew.User__c) ||
                   (tUserOld.Role__c == tUserNew.Role__c && tUserOld.User__c != tUserNew.User__c) ||
                   (tUserOld.Role__c != tUserNew.Role__c && tUserOld.User__c != tUserNew.User__c))) {
                    createUserTerr2AssociationList.add(tUserNew);
                    deleteUserTerr2AssociationList.add(tUserOld);
                }else if(tUserOld.User__c != null && tUserNew.User__c == null){
                    deleteUserTerr2AssociationList.add(tUserOld);
                }else if(tUserOld.User__c == null && tUserNew.User__c != null){
                    createUserTerr2AssociationList.add(tUserNew);
                }
            }
        }
        if(deleteUserTerr2AssociationList.size()>0){
            System.debug('deleteUserTerr2AssociationList: ' + deleteUserTerr2AssociationList);
            List<Id> toBeDeletedIdList = deleteUserAssociationInTerritoryModel(deleteUserTerr2AssociationList, true);
            if(toBeDeletedIdList.size()>0){
                deleteUserTerr2AssociationUsingId(toBeDeletedIdList);
            }
        }
        if(createUserTerr2AssociationList.size()>0){
            System.debug('createUserTerr2AssociationList: ' + createUserTerr2AssociationList);
            collectNewIds(createUserTerr2AssociationList);
        }
    }
    
    public static List<Id> deleteUserAssociationInTerritoryModel(List<Territory_User__c> oldTerrUserList, Boolean fetchIds){
        Set<String> roleCodeSet = new Set<String>();
        List<Territory2> terr2List = new List<Territory2>();
        Set<Id> userIdSet = new Set<Id>();
        Set<Id> toBeDeletedUserIdSet = new Set<Id>();
        Set<Id> terr2IdSet = new Set<Id>();
        Set<Id> terrIdSet = new Set<Id>();
        List<Id> usrTerr2IdList = new List<Id>();
        Map<String, Integer> delUserIdCountMap = new Map<String, Integer>();
        Map<String, Integer> userIdCountMap = new Map<String, Integer>();
        List<UserTerritory2Association> usrTerr2AssociationList = new List<UserTerritory2Association>();
        
        for(Territory_User__c tUser: oldTerrUserList){
            System.debug('tUser: ' + tUser);
            if(tUser.Role__c != null && resRecordTypeId == tUser.RecordTypeId && tUser.User__c != null){
                String roleCode = getRoleCode(tUser);
                String userIdRoleCode;
                if(roleCode != null){ 
                    roleCodeSet.add(roleCode); 
                    userIdSet.add(tUser.User__c);
                    terrIdSet.add(tUser.Territory__c);
                    userIdRoleCode = (string)tUser.Territory__c + '_' + (string)tUser.User__c + '_' + roleCode;
                    if(delUserIdCountMap.containsKey(userIdRoleCode)){
                        Integer count = delUserIdCountMap.get(userIdRoleCode);
                        delUserIdCountMap.put(userIdRoleCode, count + 1);
                    }else{
                        Integer count = 1;
                        delUserIdCountMap.put(userIdRoleCode, count);
                    }
                }
            }
        }
        
        if(userIdSet.size()>0){
            for(List<Territory_User__c> terrUserList: [SELECT Id, RecordTypeId, Territory__c, Territory_Code__c, Role__c, User__c, Region_Code__c, Sector_Code__c
                                                       FROM Territory_User__c WHERE Territory__c IN: terrIdSet AND User__c IN: userIdSet]){
                for(Territory_User__c tUser: terrUserList){
                    String roleCode = getRoleCode(tUser);
                    String userIdRoleCode;
                    if(roleCode != null){ 
                        roleCodeSet.add(roleCode); 
                        userIdRoleCode = (string)tUser.Territory__c + '_' + (string)tUser.User__c + '_' + roleCode;
                        if(userIdCountMap.containsKey(userIdRoleCode)){
                            Integer count = userIdCountMap.get(userIdRoleCode);
                            userIdCountMap.put(userIdRoleCode, count + 1);
                        }else{
                            Integer count = 1;
                            userIdCountMap.put(userIdRoleCode, count);
                        }
                    }
                }
            }
        }
        
        for(Territory_User__c tUser: oldTerrUserList){
            if(tUser.Role__c != null && resRecordTypeId == tUser.RecordTypeId && tUser.User__c != null){
                String roleCode = getRoleCode(tUser);
                String userIdRoleCode;
                Integer delCount = 0; Integer actCount = 0;
                if(roleCode != null){ 
                    roleCodeSet.add(roleCode); 
                    userIdRoleCode = (string)tUser.Territory__c + '_' + (string)tUser.User__c + '_' + roleCode;
                    if(userIdCountMap.containsKey(userIdRoleCode)){ actCount = userIdCountMap.get(userIdRoleCode); }
                    if(delUserIdCountMap.containsKey(userIdRoleCode)){ delCount = delUserIdCountMap.get(userIdRoleCode); }
                    System.debug('actCount: ' + actCount);
                    System.debug('delCount: ' + delCount);
                    if(delCount > actCount){ toBeDeletedUserIdSet.add(tUser.User__c); }
                }
            }
        }

        if(roleCodeSet.size()>0){
            System.debug('roleCodeSet: ' + roleCodeSet);
            for(List<Territory2> tList: [SELECT Id, DeveloperName FROM Territory2 WHERE DeveloperName IN: roleCodeSet]){
                for(Territory2 terr2: tList){
                    terr2IdSet.add(terr2.Id);
                }
            } 
        }
        
        if(userIdSet.size()>0 && terr2IdSet.size()>0){
            System.debug('userIdSet: ' + userIdSet);
            System.debug('toBeDeletedUserIdSet: ' + toBeDeletedUserIdSet);
            System.debug('terr2IdSet: ' + terr2IdSet);
            for(List<UserTerritory2Association> usrTerr2List : [SELECT Id, Territory2Id, UserId from UserTerritory2Association WHERE UserId IN: toBeDeletedUserIdSet AND Territory2Id IN: terr2IdSet]){
                for(UserTerritory2Association usrTerr2: usrTerr2List){
                    usrTerr2IdList.add(usrTerr2.Id);
                    usrTerr2AssociationList.add(usrTerr2);
                }
            }
            if(usrTerr2IdList.size()>0 && !fetchIds){
                List<Database.DeleteResult> results = Database.delete(usrTerr2IdList);
        		UtilityCls.createDeleteDMLexceptionlog(usrTerr2AssociationList, usrTerr2IdList, results,'TerrtitoryTriggerHandler','deleteUserAssociationInTerritoryModel');
            }
        }
        return usrTerr2IdList;
    }
    
    @future
    public static void deleteUserTerr2Association(Set<Id> delIdSet){
        if(delIdSet.size()>0){
            List<Territory_User__c> tUserList = [SELECT Id, RecordTypeId, Territory__c, Territory_Code__c, Role__c, User__c, Region_Code__c, Sector_Code__c
                                                FROM Territory_User__c WHERE Id IN: delIdSet ALL ROWS];
            List<Id> deletedIdList = deleteUserAssociationInTerritoryModel(tUserList, false);
        }
    }
    
    @future
    public static void deleteUserTerr2AssociationUsingId(List<Id> deleteIdList){
        List<Database.DeleteResult> results = Database.delete(deleteIdList);
    }
    
    @future
    public static void createUserTerr2Association(Set<Id> newSet){
        if(newSet.size()>0){
            List<Territory_User__c> tUserList = [SELECT Id, RecordTypeId, Territory__c, Territory_Code__c, Role__c, User__c, Region_Code__c, Sector_Code__c
                                                FROM Territory_User__c WHERE Id IN: newSet];
            createUserAssociationInTerritoryModel(tUserList);
        }
    }
    
    public static void collectNewIds(List<Territory_User__c> newList){
        Set<Id> terrIdSet = new Set<Id>();
        for(Territory_User__c tUser: newList){
            terrIdSet.add(tUser.Id);
        }
        if(terrIdSet.size()>0){
            createUserTerr2Association(terrIdSet);
        }
    }
    
    public static void collectDeletedIds(List<Territory_User__c> oldList){
        Set<Id> terrIdSet = new Set<Id>();
        for(Territory_User__c tUser: oldList){
            terrIdSet.add(tUser.Id);
        }
        if(terrIdSet.size()>0){
            deleteUserTerr2Association(terrIdSet);
        }
    }

    /* End of Code - MB - Bug 70120 - 1/28/19 */
    
    /*public static void updateUserGeography(List<Territory_User__c> terrUserList){
        Set<Id> terrIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        Map<Id, Territory__c> terrMap = new Map<Id, Territory__c>();
        Map<Id, String> userGeoMap = new Map<Id, String>();
        Id recordTypeId = UtilityCls.getRecordTypeInfo('Territory_User__c', UtilityCls.COMMERCIAL);
        List<User> updUserList = new List<User>();
        System.debug('RecordTypeId: ' + recordTypeId);
        for(Territory_User__c terrUser: terrUserList){
            if(terrUser.RecordTypeId == recordTypeId){
                terrIds.add(terrUser.Territory__c);
                if(terrUser.User__c != null){
                    userIds.add(terrUser.User__c);
                }
            }
        }
        if(userIds.size()>0){
            for(User usr: [SELECT Id, Geography__c FROM User WHERE Id IN :userIds and Business_Type__c =: UtilityCls.COMMERCIAL]){
                userGeoMap.put(usr.Id, usr.Geography__c);
            }
        }
        System.debug('UserGeoMap: ' + userGeoMap);
        if(terrIds.size()>0){
            for(Territory__c terr: [SELECT Id, Name, Region__c, Sector__c
                                 FROM Territory__c WHERE Id IN :terrIds]){
                                     terrMap.put(terr.Id, terr);
                                 }
            System.debug('terrMap: ' + terrMap);
            for(Territory_User__c terrUser: terrUserList){
                User usr = new User();
                if(terrUser.User__c != null && userGeoMap.containsKey(terrUser.User__c) && terrMap.containsKey(terrUser.Territory__c)){
                    Territory__c terr = terrMap.get(terrUser.Territory__c);
                    System.debug('terr: ' + terr);
                    if(terrUser.Role__c == UtilityCls.AE && usr.Geography__c != terr.Name){
                        usr.Id = terrUser.User__c;
                        usr.Geography__c = terr.Name;
                        updUserList.add(usr);
                    }else if(terrUser.Role__c == UtilityCls.RVP && usr.Geography__c != terr.Region__c){
                        usr.Id = terrUser.User__c;
                        usr.Geography__c = terr.Region__c;
                        updUserList.add(usr);
                    }else if(terrUser.Role__c == UtilityCls.SVP && usr.Geography__c != terr.Sector__c){
                        usr.Id = terrUser.User__c;
                        usr.Geography__c = terr.Sector__c;
                        updUserList.add(usr);
                    } 
                }
            }
            if(updUserList.size()>0){
                System.debug('Users to be Updated: ' + updUserList);
              
                   List < Database.Saveresult > results= Database.update(updUserList, false);
                   utilityCls.createDMLexceptionlog(updUserList, results, 'TerritoryUserTriggerHandler', 'updateUserGeography');
                
            }
        }
    }*/
}