/**************************************************************************

Name : RetrievePaymentTerms_CC 

===========================================================================
Purpose :   class to send HTTP request along with global accountNumber number
            Parse the response as specified format
            Iterate over the wrapper class list to hold response
            Return the formatted response
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        10/Feb/2017     Created 
2.0        Lester        14/Feb/2017     Updated          Using Custom labels to store infomration
***************************************************************************/
global class RetrievePaymentTerms_CC {
    @AuraEnabled
    public static PaymentTermsViewModel handlePaymentTerms (String accountId) {
        try {
            //a042C000000vgaN
            System.debug('RetrievePaymentTerms_CC : accountId : '+ accountId);
            
            PaymentTermsViewModel viewModel = new PaymentTermsViewModel(); 
            
            // We have to use  Account Id to get the globalAccountNumber field. 
            // Then we will send globalAccountNumber as a para 
            if (UtilityCls.isStrNotNull(accountId)) {
                Account acc = [Select Id, Global_Account_Number__c, CAMS_Account_number__c From Account 
                                                where Id =: accountId][0];
            
                String globalAccountNumber = acc == null ? '' : acc.Global_Account_Number__c;
                System.debug('RetrievePaymentTerms_CC : globalAccountNumber : ' + globalAccountNumber);

                if (UtilityCls.isStrNotNull(globalAccountNumber)){
                    String strResBody = SendHttpRequest.sendRequest(globalAccountNumber,null,'Terms_Service');                    
                   // String strResBody = '{"ErrorCode":"0000","message":"success","AccountName":"Becklers Cpt Otlt Inc","DateTimeSFDC":"*April 26 - 2018, 01:01pm EST*","InfoMessage":"If you have a question regarding Terms, please contact Mohawk Financial Services at (800) 427-4900.","TermsDataArray":[{"TacticalCat":"Soft Surface","prodTypeDesc":"Residential","termsDesc":"5/30 Net 31"},{"TacticalCat":"Soft Surface","prodTypeDesc":"Mainstreet Commercial","termsDesc":"Net 30 Days"},{"TacticalCat":"Soft Surface","prodTypeDesc":"Carpet Tile","termsDesc":"Net 30 Days"},{"TacticalCat":"Soft Surface","prodTypeDesc":"Image","termsDesc":"Net 60 Days"},{"TacticalCat":"Soft Surface","prodTypeDesc":"Karastan Woven","termsDesc":""},{"TacticalCat":"Soft Surface","prodTypeDesc":"Karastan Tufted","termsDesc":""},{"TacticalCat":"Soft Surface","prodTypeDesc":"Helios","termsDesc":""},{"TacticalCat":"Cushion","prodTypeDesc":"Padding","termsDesc":"Net 30 Days"},{"TacticalCat":"Cushion","prodTypeDesc":"Padding - Karastan","termsDesc":""},{"TacticalCat":"Hard Surface","prodTypeDesc":"Engineered Wood","termsDesc":"Net 30 Days"},{"TacticalCat":"Hard Surface","prodTypeDesc":"Solid Wood","termsDesc":"Net 30 Days"},{"TacticalCat":"Hard Surface","prodTypeDesc":"Ceramic Tile","termsDesc":"Net 30 Days"},{"TacticalCat":"Hard Surface","prodTypeDesc":"Laminate","termsDesc":"Net 30 Days"},{"TacticalCat":"Hard Surface","prodTypeDesc":"Vinyl Broadloom","termsDesc":"Net 30 Days"},{"TacticalCat":"Hard Surface","prodTypeDesc":"Vinyl Tile","termsDesc":"Net 30 Days"}]}';
                    PaymentTermsWrapper paymentTermsWrapper = new PaymentTermsWrapper();    
                    paymentTermsWrapper = parse(strResBody);
            
                    return handleResponse(paymentTermsWrapper);
                }else {
                    viewModel.status = UtilityCls.GLOBAL_ACCOUNT_NUMBER_REQUIRED;
                    viewModel.exceptionMessage = Label.Payment_Terms_Global_Account_Number_Required;
                    System.debug('RetrievePaymentTerms_CC : viewModel : ' + viewModel);

                    return viewModel;
                } // end of if (UtilityCls.isStrNotNull(globalAccountNumber))                 
            } else {
                viewModel.status = UtilityCls.ID_REQUIRED;
                viewModel.exceptionMessage = Label.Payment_Terms_Id_Required;
                System.debug('RetrievePaymentTerms_CC : viewModel : ' + viewModel);

                return viewModel;
            } // end of if (UtilityCls.isStrNotNull(myAccountProfileId))
                      
        } catch (Exception ex) {
            System.debug('RetrievePaymentTerms_CC occurs an exception : ' + ex.getMessage());

            // try to record the error message
            String sourceFunction = Label.Payment_Terms_Exception_SourceFunction;
            String logCode = Label.Payment_Terms_Exception_LogCode ;
            String referenceInfo = Label.Payment_Terms_Exception_ReferenceInfo;
            UtilityCls.createExceptionLog(ex,  'RetrievePaymentTerms_CC', sourceFunction, logCode, accountId, referenceInfo);
            
            PaymentTermsViewModel viewModel = new PaymentTermsViewModel();
            viewModel.status = UtilityCls.EXCEPTION_OCCUR;
            viewModel.exceptionMessage = Label.Payment_Terms_Exception_ExceptionMessage+ex.getMessage();
            System.debug('RetrievePaymentTerms_CC : viewModel : ' + viewModel);
            
            return viewModel;
        }
    }

    private static PaymentTermsViewModel handleResponse(PaymentTermsWrapper main){ 
        System.debug('RetrieveFreightRate_CC : PaymentTermsWrapper : ' + main);

        PaymentTermsViewModel viewModel = new PaymentTermsViewModel();
		
        if( main != null ){
          //  viewModel.iPAccount = main.iPAccount == null ? '' : main.Main.iPAccount ;
            viewModel.oAccountName = main.AccountName == null ? '' : main.AccountName;
            viewModel.oDateTime = main.DateTimeSFDC  == null ? '' : UtilityCls.addSuperScriptToDate(main.DateTimeSFDC);
          // viewModel.oMessage = main.Main.oMessage  == null ? '' : main.Main.oMessage;
			viewModel.oInformessage = main.InfoMessage  == null ? '' : main.InfoMessage;

            if(main.TermsDataArray != null && main.TermsDataArray.size() > 0){
                List<OtermsDataList> softSurfaceList = new List<OtermsDataList>();
                List<OtermsDataList> cushionList = new List<OtermsDataList>();
                List<OtermsDataList> hardsurfaceList = new List<OtermsDataList>();

                for(OtermsData data : main.TermsDataArray){
                    OtermsDataList otermsDataListObj;

                    // we group OtermsData based on tacticalCat. 
                    // In addition, we only have three groups: Soft Surface, Cushion, Hard Surface.
                    // Therefore, if tacticalCat is not belong to three groups, we will not show this oFreightData record.
                    if (data.tacticalCat != null && data.tacticalCat.equalsIgnoreCase(UtilityCls.SOFT_SURFACE)){
                        otermsDataListObj = new otermsDataList(data.tacticalCat, data.prodTypeDesc, data.termsDesc, data.nonStdind);
                        softSurfaceList.add(otermsDataListObj);
                    }else if (data.tacticalCat != null && data.tacticalCat.equalsIgnoreCase(UtilityCls.CUSHION)){
                        otermsDataListObj = new otermsDataList(data.tacticalCat, data.prodTypeDesc, data.termsDesc, data.nonStdind);
                        cushionList.add(otermsDataListObj);
                    }else if (data.tacticalCat != null && data.tacticalCat.equalsIgnoreCase(UtilityCls.HARD_SURFACE)) {
                        otermsDataListObj = new otermsDataList(data.tacticalCat, data.prodTypeDesc, data.termsDesc, data.nonStdind);
                        hardsurfaceList.add(otermsDataListObj);
                    }else {
                        //System.debug('RetrievePaymentTerms_CC : tacticalCat : null : ' + data.tacticalCat);
                    }
                } // end of for(OtermsData data : main.Main.otermsData)

                 //System.debug('RetrievePaymentTerms_CC : softSurfaceList : ' + softSurfaceList);
                 //System.debug('RetrievePaymentTerms_CC : cushionList : ' + cushionList);
                 //System.debug('RetrievePaymentTerms_CC : hardsurfaceList : ' + hardsurfaceList);
                
                // check the data is empty
                Integer dataSum = 0;
                dataSum = softSurfaceList.size() + cushionList.size() + hardsurfaceList.size();
                if (dataSum > 0) {
                    viewModel.status = UtilityCls.OK;
                }else {
                    // Now viewModel.oMessage should not be empty and show some information.
                    viewModel.status = UtilityCls.GLOBAL_ACCOUNT_NUMBER_INCORRECT;
                    viewModel.exceptionMessage = main.Message + Label.Payment_Terms_Global_Account_Number_Incorrect;
                }

                viewModel.softSurfaceList = softSurfaceList;
                viewModel.cushionList = cushionList;
                viewModel.hardsurfaceList = hardsurfaceList;

            }else {
                // main.Main.oFreightData  may be null or main.Main.oFreightData.size() is empty
                // Now viewModel.oMessage should not be empty and show some information.
                viewModel.status = UtilityCls.DATA_IS_EMPTY;
                viewModel.exceptionMessage = Label.Payment_Terms_Data_Empty; 
            } // end of if(main.Main.otermsData != null && main.Main.otermsData.size() > 0)
        }else {
            // It may parse JSON format issue
            viewModel.status = UtilityCls.PASRE_JSON_ERROR;
            viewModel.exceptionMessage = Label.Payment_Terms_Parse_JSON_Error;
        } // end of if(main != null && main.Main != null)

        System.debug('RetrievePaymentTerms_CC : viewModel : ' + viewModel);
        return viewModel;
    }

/**************************************************************************
Name : PaymentTermsWrapper 
===========================================================================
Purpose :  Wrapper class to parse the Payment Terms http response in order to display in lightning component.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         10/Feb/2017     Created 
***************************************************************************/
    
    private class PaymentTermsWrapper {
        public String ErrorCode;
		public String Message;
		public String AccountName;
		public String DateTimeSFDC;
		public String InfoMessage;
		public List<OtermsData> TermsDataArray;
    }
    
     private static PaymentTermsWrapper parse(String jsonStr) {
        system.debug('Json parsing - ' + JSON.deserialize(jsonStr, PaymentTermsWrapper.class) );
        return (PaymentTermsWrapper) JSON.deserialize(jsonStr, PaymentTermsWrapper.class);
    }
    
   /* private class Main {
        private String iPAccount;
        private String oAccountName;
        private String oDateTime;
        private String oMessage;
        private String oInformessage;
        private List<OtermsData> otermsData;
    }

    private class OtermsData {
        // tacticalCat only has three values : Soft Surface, Cushion and Surface
        // other fields will be depent on the tacticalCat field.
        private String tacticalCat;
        private String prodTypeDesc;
        private String termsDesc;
        private String nonStdind;
    } */ 
	
	    
    private class Main {
		public String ErrorCode;
		public String Message;
		public String AccountName;
		public String DateTimeSFDC;
		public String InfoMessage;
		public List<OtermsData> TermsDataArray;
    }

    private class OtermsData {
        // tacticalCat only has three values : Soft Surface, Cushion and Surface
        // other fields will be depent on the tacticalCat field.
        public String TacticalCat;
		public String ProdtypeDesc;
		public String TermsDesc;
		public String NonstdInd;
    } 
    
    public class PaymentTermsViewModel {
        @AuraEnabled
        public String iPAccount {get; set;}
        @AuraEnabled
        public String oAccountName {get; set;}
        @AuraEnabled
        public String oDateTime {get; set;}
        @AuraEnabled
        public String oMessage {get; set;}
        @AuraEnabled
        public String oInformessage {get; set;}
        @AuraEnabled
        public String status {get; set;} // HTTP requst status
        @AuraEnabled
        public String exceptionMessage {get; set;} // recod exception message
        @AuraEnabled
        public List<OtermsDataList> softSurfaceList {get; set;}
        @AuraEnabled
        public List<OtermsDataList> cushionList {get; set;}
        @AuraEnabled
        public List<OtermsDataList> hardsurfaceList {get; set;}
    }
    
     public class OtermsDataList {
        @AuraEnabled
        public String tacticalCat {get; set;}
        @AuraEnabled
        public String prodTypeDesc {get; set;}
        @AuraEnabled
        public String termsDesc {get; set;}
        @AuraEnabled
        public String nonStdind {get; set;}

        public OtermsDataList(String tacticalCat, String prodTypeDesc, String termsDesc, String nonStdind){
            this.tacticalCat = tacticalCat == null ? '' : tacticalCat;
            this.prodTypeDesc = prodTypeDesc == null ? '' : prodTypeDesc;
            this.termsDesc = termsDesc == null ? '' : termsDesc;
            this.nonStdind = nonStdind == null ? '' : nonStdind;
        }
    }
}