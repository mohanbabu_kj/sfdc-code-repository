@isTest
private class CustomLookup_CC_Test{
    
    public static List<Competitor_Products__c> comProductsList;
    public static List<Account> accList;
    private static void init(){
        comProductsList = Utility_Test.createCompetitorProducts(true,1); 
        accList = Utility_Test.createaccounts(true, 1);
        system.assert(accList != null);
        
    }
    
    private static testMethod void testfetchLookupValues() {
        init();
        test.startTest();
        List<SObject> f1List = CustomLookup_CC.fetchLookupValues('samp', 'Competitor_Products__c');
        List<SObject> f2List = CustomLookup_CC.fetchLookupValues('abc', 'Account');
        system.assert(f1List != null);
        test.stopTest();       
    }
}