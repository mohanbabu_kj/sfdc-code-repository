global class MATMultiteamCalc_Batch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    
    global final MAT_Batch_Setting__c setting;
    global DateTime batchStartTime;
    
    //New Variable - Added By Mudit on 12-12-2018
    global MAT_Batch_Setting__c QueryList;
    global DateTime fetchRecordsUpto;
    
    global MATMultiteamCalc_Batch(){
        this.batchStartTime = System.now();
        Map<String,MAT_Batch_Setting__c> batchSettingMap = MAT_Batch_Setting__c.getAll();
        setting = batchSettingMap.get('MATMultiteamCalc_Batch');
    }
    
    //Start Method
    //Added By Mudit on 12-12-2018
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        Set<string>chainNumbers = new set<string>( chainNumberSet() );
        List<MAT_Batch_Setting__c> batchSettingList = new List<MAT_Batch_Setting__c>( [ SELECT id,Batch_Query__c,Last_Run_At__c,Batch_Size__c,MinutesDelay__c FROM MAT_Batch_Setting__c WHERE NAME = 'MATMultiteamCalc_Batch' LIMIT 1 ] );
        if( batchSettingList != null && BatchSettingList.size() > 0){
            QueryList = batchSettingList[0];
        }
        if( QueryList != null && QueryList.Last_Run_At__c != null && QueryList.Batch_Query__c != null ){
            DateTime lastRunTime = QueryList.Last_Run_At__c;
            fetchRecordsUpto = (batchSettingList[0].MinutesDelay__c !=null) ? System.now().addMinutes(Integer.valueof(batchSettingList[0].MinutesDelay__c )) : System.now().addminutes(-10) ;    
            query = QueryList.Batch_Query__c  + ' AND LastModifiedDate > : lastRunTime AND LastModifiedDate < : fetchRecordsUpto AND Chain_Number__c  IN : chainNumbers Order by Chain_Number__c';
        }else{
            List<string>whereClause = new List<string>{ 'Account_Profile__r.Two_Sales_Teams__c = true','Account_Profile__r.Primary_Business__c != null'};
            
            /*if( QueryList.Last_Run_At__c != null ) {
                whereClause.add( 'LastModifiedDate >= '+setting.Last_Run_At__c.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') );
            }*/
            if( chainNumbers != null && chainNumbers.size() > 0 ){
                whereClause.add( 'Chain_Number__c IN : chainNumbers');
            }
            query = generateSOQLQueryString( 'Mohawk_Account_Team__c',new List<string>{ 'Id','Account_Profile__c','Chain_Number__c','Accounts_Account_Profile_Id__c','RecordTypeId' },whereClause ) + ' Order by Chain_Number__c ALL ROWS';
        }
        system.debug( ':::::>' + query );
        return Database.getQueryLocator(query);
    }
    /*** PREVIOUS START METHOD
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if( setting != null ) {
            List<string>whereClause = new List<string>{ 'Account_Profile__r.Two_Sales_Teams__c = true' };
                if( setting.Last_Run_At__c != null ) {
                    whereClause.add( 'LastModifiedDate >= '+setting.Last_Run_At__c.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') );
                }
            string queryStr = generateSOQLQueryString( 'Mohawk_Account_Team__c',new List<string>{ 'Id','Account_Profile__c','Accounts_Account_Profile_Id__c','RecordTypeId' },whereClause ) + ' ALL ROWS';
            return Database.getQueryLocator( queryStr );
        }
        else{
            return null;
        }
    }
    ***/
    //Execute Method
    global void execute(Database.BatchableContext BC, List<Mohawk_Account_Team__c> scope) {
        set<string>fetchedAccountProfileIds = fetchAccountProfileId( scope,'Accounts_Account_Profile_Id__c' );
        
        map<string,set<string> > check2Team = new map<string,set<string> >();
        map<id,set<string> > matProdBrand = new map<id,set<string> >();
        map<string,double> tmPercentage = new map<string, double>();
        map<string,set<string> > prodTypebrand = new map<string,set<string> >();
        
        Map<id,Account_Profile__c> acProfileMap = MohawkAccountTeamTriggerHandler.getRelatedObjValuesNewFields(scope);
        system.debug( '::::acProfileMap::::' + acProfileMap );
        list<Mohawk_Account_Team__c> updateMatList = new list<Mohawk_Account_Team__c>();
        
        for( Prodtype_and_Brand__mdt prodTypeBrandmt : [SELECT Product_Category__c, Brands__c FROM Prodtype_and_Brand__mdt] ) {
            ProdTypeBrand.put( prodTypeBrandmt.Product_Category__c, new set<string>( prodTypeBrandmt.Brands__c.split( ';' ) ) );
        }
        system.debug( '::::ProdTypeBrand::::' + ProdTypeBrand );
        map<id,Mohawk_Account_Team__c> twoTeamMAT = new map<id, Mohawk_Account_Team__c>([select id,Chain_Number__c,Chain_Number_UserId__c, BMF_Team_Member__c, Product_Types__c, Act_MHK_Purch_CU_HOR__c,
                                                                                         Act_MHK_Purch_HW_RT__c, Act_MHK_Purch_CU_KAR__c, Act_MHK_Purch_CU_ALA__c,
                                                                                         Act_MHK_Purch_HW_PP__c, Act_MHK_Purch_Lam_RT__c, Act_MHK_Purch_Lam_PP__c,
                                                                                         Act_MHK_Purch_Tile_RT__c, Act_MHK_Purch_Tile_PP__c, Act_MHK_Purch_Resi_PP__c,
                                                                                         Act_MHK_Purch_CU_ALA_Comm__c, Act_MHK_Purch_CU_PP__c, Act_MHK_Purch_Car_PP__c,
                                                                                         Brands__c, Accounts_Account_Profile_Id__c,
                                                                                         Account__c, Account__r.Total_Account_Chain_Sales__c from Mohawk_Account_Team__c where Account_Profile__c in: fetchedAccountProfileIds ]);
        
        system.debug( '::::twoTeamMAT::::' + twoTeamMAT );
        for (id matID : twoTeamMAT.keySet()) {
            //  Mohawk_Account_Team__c matRecord = twoTeamMAT.get(matID) != null ? twoTeamMAT.get(matID) : null;
            String chainNum =  twoTeamMAT.get(matID) != null && twoTeamMAT.get(matID).Chain_Number__c != null ? twoTeamMAT.get(matID).Chain_Number__c : '';
            String chainTMnum = twoTeamMAT.get(matID) != null && twoTeamMAT.get(matID).Chain_Number_UserId__c != null ? twoTeamMAT.get(matID).Chain_Number_UserId__c : '';
            Double accChainSales = twoTeamMAT.get(matID) != null && twoTeamMAT.get(matID).Account__r.Total_Account_Chain_Sales__c != null ? twoTeamMAT.get(matID).Account__r.Total_Account_Chain_Sales__c : 0;
            String prodTypes = twoTeamMAT.get(matID) != null && twoTeamMAT.get(matID).Product_Types__c != null ? twoTeamMAT.get(matID).Product_Types__c : '';
            String brands = twoTeamMAT.get(matID) != null && twoTeamMAT.get(matID).brands__c != null ? twoTeamMAT.get(matID).brands__c : '';
            
            if(prodTypes != null && brands != null) {       
                for(String prodType : prodTypes.split(';')) {
                    if (prodType != null && prodType != '') {
                        for(String brand : brands.split(';')) {
                            if (brand != null && brand != '') {  
                                if(prodTypebrand.get(prodType).contains(brand)) {
                                    if (tmPercentage.containsKey(chainTMnum + prodtype + brand)) {
                                        Double distper = TMpercentage.get(chainTMnum + prodtype + brand);
                                        TMpercentage.put(chainTMnum + prodtype + brand, distper + accChainSales);
                                    } else {
                                        TMpercentage.put(chainTMnum + prodtype + brand, accChainSales);
                                    }
                                    
                                    if (matProdBrand.containsKey(matID)) {
                                        matProdBrand.get(matID).add('[' + prodType + ']' + '(' + brand + ')');
                                    } else {
                                        matProdBrand.put(matID, new Set<string>{'[' + prodType + ']' + '(' + brand + ')'});
                                    }
                                    
                                    string chainprodbrand = chainNum + prodType + brand;
                                    if (check2Team.containsKey(chainprodbrand)) {
                                        check2Team.get(chainprodbrand).add(chainTMnum);
                                    } else {
                                        check2Team.put(chainprodbrand, new Set<string>{chainTMnum});
                                    }
                                    system.debug('check2Team' + check2Team);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        for(id matid : matProdBrand.keySet() ) {
            set<string> prodTypeSet = new set<string>();
            set<string> brandSet = new set<string>();
            map<string, Double> disPerProdtype = new map<string, Double>();
            Mohawk_Account_Team__c mac = twoTeamMAT.get(matid);
            
            for(string prodbrand : matProdBrand.get(matid) ) {
                
                String prodType = prodbrand.substring(prodbrand.indexOf('[') + 1, prodbrand.indexOf(']'));
                String brand = prodbrand.substring(prodbrand.indexOf('(') + 1, prodbrand.indexOf(')'));
                
                // Two team account
                if (check2Team.containsKey(mac.Chain_Number__c + prodtype + brand) && check2Team.get(mac.Chain_Number__c + prodtype + brand).size() > 1) {
                    If (TMpercentage.get(mac.Chain_Number_UserId__c + prodtype + brand) > 0){
                        double distper = TMpercentage.get(mac.Chain_Number_UserId__c + prodType + brand );
                        system.debug( ':::distper:::' + distper );
                        disPerProdtype.put(prodType + brand, distper );
                    }
                }
            }
            system.debug( '::::disPerProdtype::::' + disPerProdtype );
            mac = MohawkAccountTeamService.clearMATcalculation(mac);
            Account_Profile__c acProfile = new Account_Profile__c();
            acProfile = acProfileMap.get(mac.Accounts_Account_Profile_Id__c);
            mac = MohawkAccountTeamService.CalculatetwoSalesTeamSplit(mac, acProfile, disPerProdtype );
            updateMatList.add(mac);
        }
        
        check2Team = null;  matProdBrand = null;
        
        if (updateMatList.size() > 0) {
            // Set Static variable to avoid recurssion flag.
            MohawkAccountTeamService.twoteamsplitupdate = true;
            List<Database.SaveResult> results = Database.update(updateMatList, false);
            
        }
        
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC) {
        setting.Last_Run_At__c = batchStartTime;
        update setting;
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
        Database.executeBatch(new MATMultiteamCalc_Batch());
    }
    
    public set<string>fetchAccountProfileId( List<sobject> fetchedMAT,string fieldToMakeSet ){
        set<string>returnSet = new set<string>();
        if( fetchedMAT != null && fetchedMAT.size() > 0 ) {
            for( sobject obj : fetchedMAT ) {
                returnSet.add( (STRING)obj.get( fieldToMakeSet ) );
            }
        }
        return returnSet;
    }
    
    public string generateSOQLQueryString( string objectName,List<string>fields,List<string>whereClause ){
        string query = 'SELECT ';
        query += string.JOIN( fields,',' );
        query += ' FROM '+objectName;
        if( whereClause != null && whereClause.size() > 0 )
            query += ' WHERE '+string.JOIN( whereClause,' AND ' );
        return query;
    }
    
    public static Set<string> chainNumberSet( ){
        Set<string>chainSet1 = new Set<string>();
        Set<string>chainSet2 = new Set<string>();
        Set<string>returnChainSet = new Set<string>();
        for( AP_Trigger_Settings__c ATS : [ SELECT id,Exclude_Chain_Numbers_1__c,Exclude_Chain_Numbers_2__c FROM AP_Trigger_Settings__c WHERE Name =: 'Two Team Account' ] ){
            chainSet1.addall( ATS.Exclude_Chain_Numbers_1__c.split(';') );
            chainSet2.addall( ATS.Exclude_Chain_Numbers_2__c.split(';') );            
        }
        returnChainSet.addAll( chainSet1 );
        returnChainSet.addAll( chainSet2 );
        return returnChainSet;
    }
}