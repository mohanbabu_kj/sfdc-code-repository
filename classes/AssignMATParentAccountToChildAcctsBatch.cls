/**************************************************************************

Name : AssignMATParentAccountToChildAcctsBatch

===========================================================================
Purpose : Batch Class for Parent-Child Access
->If the account is associated to Parent and it has multiple child accounts, then who ever are there in Mohawk Account Team of Parent account needs to get access to all child accounts associated to that Parent Account.
->Pull the Mohawk account team members from Parent account and put it in standard Account Team object of all child accounts.
->If a user is removed from Mohawk Account Team, then remove that user from child Account’s Account team.
->This will be a daily batch and if there are any addition or removal of Mohawk Account team in Parent account, the same will get changed in all child accounts.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha    7/March/2018    Created          CSR:

***************************************************************************/
global class AssignMATParentAccountToChildAcctsBatch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {//Added Database.Stateful by Susmitha 
    global static string ErrorRecords;
   // global static string errorRecs;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        
       /* //Need to add condition to retrieve recent modified users last 1 DAY
        String query = System.Label.Account_SOQL_for_Parent_Child_Association; 
        System.debug(LoggingLevel.INFO, '*** query ***' +query);
        System.debug(LoggingLevel.INFO, '*** query 12: ' +Database.getQueryLocator(Query));
        */
        /*--Modified codes by Susmitha on April 2 for Notifying error--*/
        String Query;
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'AssignMATParentAccountToChildAcctsBatch'];
        If(QureyList.size() > 0)
            query = QureyList[0].Query_String__c ;
        else
            query = System.Label.Account_SOQL_for_Parent_Child_Association; 
        /*--End Modified codes by Susmitha on April 2 for Notifying error--*/
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope) {
        Set<Id> ParentIdSet=new set<Id>();
        Set<Id> ChildAccIdSet=new set<Id>();
        Set<Id> accIdSet=new set<Id>();
        List<Mohawk_Account_Team__c> MATList=new List<Mohawk_Account_Team__c>();
        Map<Id, Set<Id>> parentChildIdsMap = new Map<Id, Set<Id>>();
        Map<Id,List<Mohawk_Account_Team__c>> MATMap=new Map<Id,List<Mohawk_Account_Team__c>>();
        Map<Id,List<Mohawk_Account_Team__c>> childMATMap=new Map<Id,List<Mohawk_Account_Team__c>>();
        List<Id> delMATIds = new List<Id>();
        List<Mohawk_Account_Team__c> addMATList = new List<Mohawk_Account_Team__c>();
        
        //System.debug('Scope : ' + scope);
        for(Account acc : scope){
            if(acc.ParentId != null){
                ParentIdSet.add(acc.ParentId);
            }else if(acc.ParentId == null && !ParentIdSet.contains(acc.Id)){
                accIdSet.add(acc.Id);
            }
        }
        System.debug('Account ID Set: ' + accIdSet);
        //Populate ParentIdSet only the accounts having child and has been modified.
        if(!accIdSet.isEmpty()){
            for(List<Account> accList: [SELECT Id, ParentId from Account where ParentId IN :accIdSet]){
                for(Account acc: accList){
                    ParentIdSet.add(acc.ParentId);
                }
            }
        }
        if(!ParentIdSet.isEmpty()){
            //Query all ParentAccounts to Construct Map of AccountId and List of Mohawk Account Teams
            for(List<Mohawk_Account_Team__c> mhkTeamList: [select Id, IsDeleted,Account__c,User__c,Role__c From Mohawk_Account_Team__c where Account__c in : ParentIdSet and user__c!=null ALL ROWS]){
                for(Mohawk_Account_Team__c m  : mhkTeamList){
                    if(MATMap.containsKey(m.Account__c)){
                        MATMap.get(m.Account__c).add(m);
                    }
                    else{
                        list<Mohawk_Account_Team__c> mtList=new List<Mohawk_Account_Team__c>();
                        mtList.add(m);
                        MATMap.put(m.Account__c, mtList);
                    }
                }
            }
            //System.debug('Parent MAT : ' + MATMap);
            
            //Query all child Accounts 
            for(List<Account> childAccList: [SELECT ID,Name, ParentId,(SELECT ID, Account__c, User__c, Parent_AT_Member__c FROM Mohawk_Account_Teams__r Where User__c != null) FROM ACCOUNT WHERE parentId IN: ParentIdSet]){
                for(Account childAcc: childAccList){
                    childMATMap.put(childAcc.id,childAcc.Mohawk_Account_Teams__r);
                    if(parentChildIdsMap.containsKey(childAcc.ParentId)){
                        parentChildIdsMap.get(childAcc.ParentId).add(childAcc.Id);
                    }else{
                        Set<Id> accIds = new Set<Id>();
                        accIds.add(childAcc.Id);
                        parentChildIdsMap.put(childAcc.ParentId, accIds);
                    }
                }
            }
            //System.debug('Child MAT : ' + childMATMap);
            //System.debug('Parent Child : ' + parentChildIdsMap);
            
            for(Id acc: ParentIdSet){
                //System.debug('Scope : ' + acc);
                if(MATMap.containsKey(acc)){
                    for(Mohawk_Account_Team__c parentmhkAT: MATMap.get(acc)){
                        //System.debug('Parent MAT : ' + parentmhkAT);
                        if(!parentmhkAT.IsDeleted){
                            if(parentChildIdsMap.containsKey(acc)){
                                for(Id childId: parentChildIdsMap.get(acc)){
                                    Set<Id> userIds = new Set<Id>();
                                    if(childMATMap.containsKey(childId)){
                                        for(Mohawk_Account_Team__c atm: childMATMap.get(childId)){
                                            userIds.add(atm.User__c);
                                        }
                                    }
                                    // System.debug('Child MATs : ' + userIds);
                                    if(!userIds.contains(parentmhkAT.User__c)){
                                        Mohawk_Account_Team__c atm = new Mohawk_Account_Team__c();
                                        atm.Account__c = childId;
                                        atm.User__c = parentmhkAT.User__c;
                                        atm.Role__c = parentmhkAT.Role__c;// mat.Role__c; //Clarify with Mohan what could be the role?
                                        atm.Account_Access__c = 'Edit';
                                        atm.Parent_AT_Member__c = true;
                                        //atm.ContactAccessLevel = defaultConfig.Contact_Access__c;
                                        //atm.OpportunityAccessLevel = defaultConfig.Opportunity_Team_Access__c;                            
                                        addMATList.add(atm);  //Team Members that has to be created on child ACCOUNT
                                        ChildAccIdSet.add(childId);
                                    }
                                }
                            }
                        }else{
                            //system.debug(' deleted ::::::::');
                            for(Id childId: parentChildIdsMap.get(acc)){
                                for(Mohawk_Account_Team__c childMAT: childMATMap.get(childId)){
                                    if(parentmhkAT.User__c == childMAT.User__c && childMAT.Parent_AT_Member__c == true){
                                        delMATIds.add(childMAT.Id);
                                        ChildAccIdSet.add(childId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            System.debug('Add List: ' + addMATList);
            //Insert Account Team Member if the team member is added in Parent Mohawk Account Team
            if(!addMATList.isEmpty()){
                
                    List<Database.SaveResult> updateResult = Database.insert(addMATList, false);
                     for ( Integer i = 0; i < updateResult.size(); i++ ) {
                    if ( !updateResult.get(i).isSuccess() ) {
                        if ( ErrorRecords == null ) {
                            ErrorRecords = 'Operation, Object Name, Record Id, Name, Status Code, Error Message \n';
                        }
                        for ( Database.Error theError : updateResult.get(i).getErrors() ) {
                            string recordString = '"'+'Insert'+'","'+'Mohawk Account Team'+ '","' +addMATList.get(i).Id+'","' + '","' +theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                            ErrorRecords = ErrorRecords + recordString;                         
                        }
                    }
                } 
                //
                
            }
            //Delete Account Team Members if the team member is removed from Parent Mohawk Account Team
            if(!delMATIds.isEmpty()){
              
                    system.debug('###delATMIds'+delMATIds);
                   Database.DeleteResult [] delResult = Database.delete(delMATIds, false);
            
            for ( Integer i = 0; i < delResult.size(); i++ ) {
                if ( !delResult.get(i).isSuccess() ) {
                    if ( ErrorRecords == null ) {
                        ErrorRecords = 'Operation, Object Name, Record Id, Name, Status Code, Error Message \n';
                    }
                    for ( Database.Error theError : delResult.get(i).getErrors() ) {
                            string recordString = '"'+'Delete'+'","'+'Mohawk Account Team'+ '","'+delMATIds.get(i)+'","' + '","'+theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                            ErrorRecords = ErrorRecords + recordString;                         
                    }
                }
            }
               
            }
            
            if(!ChildAccIdSet.isEmpty()){
                updateMHKTeamMembers(ChildAccIdSet);
            }
        }
    }
    
    private static void updateMHKTeamMembers(Set<Id> accIds){
        Map<Id,Set<Id>> AccUserIdsMap=new Map<Id,Set<id>>();
        List<Account> accUpdList = new List<Account>();
        for(List<Account> AccountListToUpdate: [SELECT ID,Mohawk_Team_Members__c,(SELECT ID,Account__c,User__c FROM Mohawk_Account_Teams__r) FROM Account WHERE ID IN:accIds]){
            for(Account acc : AccountListToUpdate){
                for(Mohawk_Account_Team__c each: acc.Mohawk_Account_Teams__r){
                    if(AccUserIdsMap.containsKey(each.Account__c)){
                        AccUserIdsMap.get(each.Account__c).add(each.User__c);
                    }else{
                        set<id> acTeamList = new set<id>();
                        acTeamList.add(each.User__c);
                        AccUserIdsMap.put(each.Account__c, acTeamList);
                    }
                }
            }
        }
        for(Id accId :accIds){
            Account acc = new Account();
            String str='';
            if(AccUserIdsMap.containsKey(accId)){
                for(id usId:AccUserIdsMap.get(accId)){
                    str+=usId+';';
                }
                str = str.removeEnd(';');   //remove ; from  end if present  
                str = str.removeStart(';');   //remove ; from  Start if present            
            }
            if(str != ''){
                acc.Id = accId;
                acc.Mohawk_Team_Members__c=str;
                accUpdList.add(acc);   
            }
        }
        if(accUpdList.size()>0){
            UtilityCls.AsycupdateFromMAT = true; // To prevent recurssion
            //Database.update(accUpdList,false);
            Database.SaveResult [] updateResult;
            updateResult= Database.update(accUpdList, false);
            if(Test.isRunningTest()){
                accUpdList[0].Role_C__c='Deactivated';

                updateResult = Database.update(accUpdList, false);
                
            }


                 for ( Integer i = 0; i < updateResult.size(); i++ ) {
                    if ( !updateResult.get(i).isSuccess() ) {
                        if ( ErrorRecords == null ) {
                            ErrorRecords = 'Operation, Object Name, Record Id, Name , Status Code, Error Message \n';
                        }
                        for ( Database.Error theError : updateResult.get(i).getErrors() ) {
                            string recordString = '"'+'Update'+'","'+'Account'+ '","'+accUpdList.get(i).Id+'","'+accUpdList.get(i).Name+'","'+theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                            ErrorRecords = ErrorRecords + recordString;                         
                        }
                    }
                }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
       //ErrorRecords+=errorRecs;
        if (ErrorRecords != null)
            system.debug('Error Records :::::::'+ErrorRecords);
            NotificationUtility.sendBatchExceptionNotification(bc.getJobId(),ErrorRecords,'MAT Error Records');
            
    }
    
    //Schedulable 
    global void execute(SchedulableContext SC){
        String jobId = Database.executeBatch(new AssignMATParentAccountToChildAcctsBatch(),200); 
        System.debug('*******Job id***'+jobId);
        
    }
    
}