global with sharing class TrackMohawkAccountTeam_Batch implements Database.Batchable<sObject>{
    public static Boolean error = false;
    public static List<ApplicationLog__c> appLogList = new List<ApplicationLog__c>();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String Query;
        Batch_Query__mdt[] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'TrackMohawkAccountTeam_Batch'];
        If(QureyList.size() > 0) {
            Query = QureyList[0].Query_String__c;        
        }else{
            Query= 'SELECT Id,Account__c,isDeleted ,Role__c,Account_Access__c,User__c FROM Mohawk_Account_Team__c WHERE User__c != null AND (CreatedDate = TODAY OR CreatedDate = YESTERDAY) ALL ROWS';
        }
        System.debug(Query);
        return Database.getQueryLocator(Query) ;
    }
    
    global void execute(Database.BatchableContext bc,List<Mohawk_Account_Team__c> scope){
        set<Id> accountIds =  new set<Id>();
        set<Id> deletedMohawkATAccId =  new set<Id>();
        map<id,id> mpMohawkATIdAccountId  = new map<id,id>();
        map<id,set<id>> mpAccountIdToUserIds  = new map<id,set<id>>();
        set<id> mohawkUserIds  = new set<id>();
        List<AccountTeamMember> createAccountTeam = new List<AccountTeamMember>();
        List<AccountTeamMember> deleteAccountTeam = new List<AccountTeamMember>();
        for( Mohawk_Account_Team__c mat : scope ){
            if(!mat.isDeleted){
                accountIds.add( mat.Account__c );
                System.debug('MAT AccountIds: ' + mat.Account__c);
            }
            else{
                deletedMohawkATAccId.add( mat.Account__c );
                mohawkUserIds.add(mat.User__c);
            }
        }
        //Get account team member for created Mohawk AT
        System.debug('AccountIds: ' + accountIds);
        for (AccountTeamMember atm : [Select  Id,UserId, AccountId From AccountTeamMember where AccountId IN: accountIds]){
            Set<id> userIds = new Set<id>();
            if(mpAccountIdToUserIds.containsKey(atm.AccountId)){
                mpAccountIdToUserIds.get(atm.AccountId).add(atm.UserId);
                //userIds.add(atm.UserId);
                //mpAccountIdToUserIds.put(atm.AccountId,userIds);
            }else{
                userIds.add(atm.UserId);
                mpAccountIdToUserIds.put(atm.AccountId,userIds);
            }	
        }
        //create account team member if Account not found in Account  Team Member or Account and User does not exist
        for( Mohawk_Account_Team__c mat : scope ){
            if( mat.Account__c != null && !mat.isDeleted ){
                if(mpAccountIdToUserIds.containskey(mat.Account__c)){
                    if(!mpAccountIdToUserIds.get(mat.Account__c).contains(mat.User__c)){
                        createAccountTeam.add(processAccountTM(mat)); 
                    }  
                }else{
                    createAccountTeam.add(processAccountTM(mat));
                }
            }
        }
        CreateAccountTM(createAccountTeam);
        
        //Below logic to get Account Team Member for Deleted Mohawk AT
        
        //Get account team member for deleted Mohawk AT
        for(AccountTeamMember atm : [Select  Id, AccountId From AccountTeamMember where UserId in :mohawkUserIds And  AccountID in :deletedMohawkATAccId]){
            deleteAccountTeam.add(atm);
        }  
        deleteAccountTM(deleteAccountTeam);
    }
    
    global static AccountTeamMember processAccountTM(Mohawk_Account_Team__c mat){
        AccountTeamMember accTeamMember = new AccountTeamMember();
        accTeamMember.AccountId      = mat.Account__c;
        accTeamMember.TeamMemberRole = mat.Role__c;
        accTeamMember.UserId         = mat.User__c;
        if(mat.Account_Access__c != null && mat.Account_Access__c != ''){
            accTeamMember.AccountAccessLevel = mat.Account_Access__c;
            accTeamMember.ContactAccessLevel = mat.Account_Access__c;
            accTeamMember.OpportunityAccessLevel = mat.Account_Access__c;
        }else{
            accTeamMember.AccountAccessLevel = 'Edit';
            accTeamMember.ContactAccessLevel = 'Edit';
            accTeamMember.OpportunityAccessLevel = 'Edit';
        }
        return accTeamMember;
    }
    global static void CreateAccountTM(list<AccountTeamMember> lstAccTeamMember){
        Integer count = 0;
        if(lstAccTeamMember != null && lstAccTeamMember.size()>0){
            if(Test.isRunningTest()){
                AccountTeamMember atm = new AccountTeamMember(AccountId = '0012C000008DVW2QAO', UserId = '0052C000000DqtuQAC',TeamMemberRole='Territory Manager');
                lstAccTeamMember.add(atm);
            }
            //Integer count = 0;
            Database.SaveResult[] results = Database.insert(lstAccTeamMember, false);
            for (Database.SaveResult result : results) {
                if(!result.isSuccess()){
                    
                    // Operation failed, so get all errors                
                    for(Database.Error err : result.getErrors()) {
                        AccountTeamMember atm = lstAccTeamMember.get(count);
                        System.debug('Error while creating AccountTM: ' + err.getMessage());
                        ApplicationLog__c appLog = new ApplicationLog__c();
                        appLog.Message__c = err.getMessage();
                        appLog.Source__c = 'TrackMohawkAccountTeam_Batch';
                        appLog.SourceFunction__c = 'CreateAccountTM';
                        appLog.DebugLevel__c = 'Error'; 
                        appLog.ReferenceInfo__c = atm.AccountId + '_' + atm.UserId;
                        appLogList.add(appLog);
                        error = true;
                    }
                }
                count += 1;
            } 
        }
    }
    global static void deleteAccountTM(list<AccountTeamMember> lstAccTeamMember){
        Integer count = 0;
        if(lstAccTeamMember != null && lstAccTeamMember.size()>0){
            if(Test.isRunningTest()){
                AccountTeamMember atm = new AccountTeamMember(Id = '01M4C000001DLnTUAW');
                lstAccTeamMember.add(atm);
            }
            Database.DeleteResult[] results = Database.delete(lstAccTeamMember, false);
            for (Database.DeleteResult result : results) {
                if(!result.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : result.getErrors()) {
                        AccountTeamMember atm = lstAccTeamMember.get(count);
                        System.debug('Error while Deleting AccountTM: ' + err.getMessage());
                        ApplicationLog__c appLog = new ApplicationLog__c();
                        appLog.Message__c = err.getMessage();
                        appLog.Source__c = 'TrackMohawkAccountTeam_Batch';
                        appLog.SourceFunction__c = 'deleteAccountTM';
                        appLog.DebugLevel__c = 'Error'; 
                        appLog.ReferenceInfo__c = atm.AccountId + '_' + atm.UserId;
                        appLogList.add(appLog);
                        error = true;
                    }
                }
                count += 1;
            } 
        }
    }
    global void finish(Database.BatchableContext bc){
        if(appLogList.size()>0){
            insert appLogList;
        }
        if(error || Test.isRunningTest()){
            String body = '';
            List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';');  
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            body = 'Hello All, <br /><br />';
            body += 'Please check Application log for failed records from TrackMohawkAccountTeam_Batch class.<br />';
            String subject = 'Failed to create record in TrackMohawkAccountTeam_Batch';
            email.setSubject(subject);
            email.setToAddresses(emailAddressList);
            email.setHtmlBody(body);
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }
    
}