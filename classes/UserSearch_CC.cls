/**************************************************************************

Name : UserSearch_CC

===========================================================================
Purpose : This class is used for the lightning components which will search the production and assign it to opportunityLineIteams
===========================================================================
History:
--------
VERSION      AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik       4/APR/2017    Created  
2.0        sUSMITHA        3/MAY/2018   MODIFIED
***************************************************************************/
public with sharing class UserSearch_CC {
    
    // Start of Code - MB - 06/13/17
    public static List<OpportunityShare> updOppShareList = new List<OpportunityShare>();
    public static List<OpportunityTeamMember> ProjectTeamMemList = new List<OpportunityTeamMember>();
    public static Boolean SaveResult = false;
    // End of Code - MB - 06/13/17
    
    //This Method will be used to search the results from the terriotry User object and returive the list to the UI to show the user lists
    //Changed return parameter of method from Territory_User__c to User - MB - 06/20/17
    @AuraEnabled
    public static List<User> getTerrUserOwnerlist(string Name,  string Territory,string AreacodeValue,string StateValue,string Majormetrosvalue,string Territoryvalue) {
        //List<Territory_User__c> userList = new List<Territory_User__c>(); //Commented by MB - 06/20/17
        List<Territory_User__c> terrUserList = new List<Territory_User__c>(); //Added by MB - 06/20/17
        List<Territory_User__c> UserTerritory;
        String RecordTypeId = null;
        Set<String> terruserids = new Set<String>();
        String soql = '';
        // Start of Code - MB - 06/20/17
        Boolean terrUserObj = false; 
        List<User> retUserList = new List<User>();
        Set<Id> userIds = new Set<Id>();
        // End of Code - MB - 06/20/17
        //Basic dynamic SOQL as string to initiate the Query
        //To check the parameters are passed to get the search results
        if(Territoryvalue == ''){
            if(Territory== UtilityCls.ALLUSERSWITHINMYTERRITORY){
                UserTerritory = [SELECT User__c,User__r.Name, User__r.UserRole.Name,Id, OwnerId, Name, Territory__r.Name FROM Territory_User__c Where user__c=:UserInfo.getUserId() Limit 1000];
                for(Territory_User__c tu:UserTerritory){
                    terruserids.add(tu.Territory__c);
                }
                // Start of Code - MB - 06/20/17
                soql ='SELECT User__c,User__r.Name,User__r.MHKUserRole__c, User__r.Title, User__r.Geography__c, User__r.Segment__c, Segments__c, RecordTypeId,User__r.SmallPhotoUrl,User__r.Area_Code__c,User__r.State, User__r.State__c, User__r.Major_Metros__c,User__r.UserRole.Name,Id, OwnerId, Name, Territory__r.Name FROM Territory_User__c WHERE ';
                // End of Code - MB - 06/20/17
                soql += 'Territory__c IN';
                soql +=':terruserids';
                terrUserObj = true;
                
            } else if(Territory==UtilityCls.ALLUSERS){
                string OuserBusinessType = [Select Id,Name,Business_Type__c from user where Id=:UserInfo.getUserId()].Business_Type__c;
                // Start of Code - MB - 06/20/17
                soql ='SELECT Id, Name, Title, Geography__c, Segment__c, SmallPhotoUrl, Area_Code__c, State, State__c, Major_Metros__c, UserRole.Name, MHKUserRole__c FROM User WHERE IsActive = true AND'; //Added condition for selecting active users - MB - 2/28/18
                //soql += ' Id != \''+UserInfo.getUserId()+'\'';
                soql += ' Business_Type__c =\''+OuserBusinessType+'\'';
                // End of Code - MB - 06/20/17
            }
            System.debug('terrUserObj: ' + terrUserObj);
            // Added condition to fetch either from territory_user__c object or user object - MB - 06/20/17
            if(Name!=''){
                if(terrUserObj){
                    soql +=' AND User__r.Name Like \'%'+String.escapeSingleQuotes(Name)+ '%\'';
                }else{
                    soql +=' AND Name Like \'%'+String.escapeSingleQuotes(Name)+ '%\'';
                }
            }
            
            // Added condition to fetch either from territory_user__c object or user object - MB - 06/20/17
            if(AreacodeValue !=''){
                if(terrUserObj){    
                    soql +=' AND User__r.Area_Code__c Like \'%'+String.escapeSingleQuotes(AreacodeValue)+ '%\'';
                }else{
                    soql +=' AND Area_Code__c Like \'%'+String.escapeSingleQuotes(AreacodeValue)+ '%\'';
                }
            }
            
            // Added condition to fetch either from territory_user__c object or user object - MB - 06/20/17
            if(StateValue!=UtilityCls.NONE){
                if(terrUserObj){
                    soql +=' AND User__r.State__c includes( \''+StateValue+ '\')';
                }else{
                    soql +=' AND State__c includes( \''+StateValue+ '\')';
                }
            }
            
            // Added condition to fetch either from territory_user__c object or user object - MB - 06/20/17
            if(Majormetrosvalue !=UtilityCls.NONE){
                if(terrUserObj){
                    soql +=' AND User__r.Major_Metros__c includes( \''+Majormetrosvalue+ '\')';
                }else{
                    soql +=' AND Major_Metros__c includes( \''+Majormetrosvalue+ '\')';
                }
            }
            
        } else {
            // Start of Code - MB - 06/20/17
            soql ='SELECT User__c,User__r.Name,User__r.MHKUserRole__c, User__r.Title, User__r.Geography__c, User__r.Segment__c, Segments__c, RecordTypeId,User__r.SmallPhotoUrl,User__r.Area_Code__c,User__r.State, User__r.State__c, User__r.Major_Metros__c, User__r.UserRole.Name,Id, OwnerId, Name, Territory__r.Name FROM Territory_User__c WHERE ';
            // End of Code - MB - 06/20/17
            soql += ' Territory__c=:Territoryvalue';
            terrUserObj = true;
        }
        
        soql=soql + ' limit 100';
        //userList = Database.Query(soql); - Commented by MB - 06/20/17
        // Start of Code - MB - 06/20/17
        if(terrUserObj){
            terrUserList = Database.Query(soql);
        }else{
            retUserList = Database.Query(soql);
        }
        if(terrUserObj && terrUserList != null){
            for(Territory_User__c terrUser: terrUserList){
                userIds.add(terrUser.User__c);
            }
            if(userIds.size()>0){
                retUserList = [SELECT Id, Name, Title, Geography__c, Segment__c, SmallPhotoUrl, Area_Code__c, State, State__c, Major_Metros__c, UserRole.Name, MHKUserRole__c 
                               FROM User 
                               WHERE Id IN: userIds and IsActive = true];
            }
        }
        // End of Code - MB - 06/20/17
        //return userList; Commented by MB - 06/20/17
        System.debug('Return List: '+ retUserList);
        return retUserList; //Added by MB - 06/20/17
    }
    
    // to featch the State picklist values from User Object and pass it to the UI 
    @AuraEnabled
    public static String ChangeOwner(String recordId){
        String Territoryvalue = '';
        //Get prefix from record ID
        //This assumes that you have passed at least 3 characters
        String myIdPrefix = String.valueOf(recordId).substring(0,3);
        user provalue = [Select Id,Profile.Name from User where Id =:UserInfo.getUserId() Limit 1];
        if(myIdPrefix!=null){
            if(myIdPrefix.equals('001')){
                Account accountObject = [Select Id,Territory__c,Owner.ProfileId from Account where Id=:recordId Limit 1];
                if(provalue.Profile.Name == UtilityCls.COMM_SALES_USER || provalue.Profile.Name == UtilityCls.RES_SALES_USER){
                    Territoryvalue = accountObject.Territory__c;
                } 
                
            } else if(myIdPrefix.equals('00Q')){
                Lead leadObject = [Select Id,Territory__c,Owner.ProfileId from Lead where Id=:recordId Limit 1];
                if(provalue.Profile.Name == UtilityCls.COMM_SALES_USER || provalue.Profile.Name == UtilityCls.RES_SALES_USER){
                    Territoryvalue = leadObject.Territory__c;
                }
            } 
        }  
        return Territoryvalue;
    }
    
    // to featch the Major Metros picklist values from User Object and pass it to the UI 
    @AuraEnabled
    public static List<String> getMajorMetorsPickListvalues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.USER,UtilityCls.MAJORMETROS);
        return Cpickvalues;
    }
    
    // to featch the State picklist values from User Object and pass it to the UI - Added by MB - 07/05/17
    @AuraEnabled
    public static List<String> getStatePickListvalues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.USER, 'State__c');
        System.debug('State List: ' + Cpickvalues);
        return Cpickvalues;
    } // end of code - MB - 07-05/17
    
    @AuraEnabled
    // objectName, such as Project
    public static List<String> getProjectNameInfo(String ProjectId,String ProjectTeamId) {        // SSB 08-13-17 Added  ProjectTeamId parameter and logic to pass it
        List<String> projectinfo = new List<String>();
        if (ProjectTeamId != '' && ProjectTeamId.substring(0,3) == '00q') {                 // SSB 08-13-17 Added    
            
            OpportunityTeamMember tm =  [Select Id,Name,Opportunity.Name from               // SSB 08-13-17 Added
                                         OpportunityTeamMember where Id=:ProjectTeamId ];    // SSB 08-13-17 Added
            System.debug(tm.Opportunity.Name);                                              // SSB 08-13-17 Added   
            System.debug(tm.Opportunity.Id);
            projectinfo.add(tm.Opportunity.Id);
            projectinfo.add(tm.Opportunity.Name);
            // return tm.Opportunity.Name;                                                      // SSB 08-13-17 Added
            return projectinfo;
        } else {
            // return [Select Id,Name from Opportunity where Id=:ProjectId Limit 1].Name;
            Opportunity opp = [Select Id,Name from Opportunity where Id=:ProjectId Limit 1];
            projectinfo.add(opp.Id);   // SSB 08-27-17 Added
            projectinfo.add(opp.Name);  // SSB 08-27-17 Added
            return projectinfo;
            
        }
    } 
    // SSB 08-13-17 Start of code change - method to padd USer for Edit Team component
    @AuraEnabled
    public static List<String> getProjectTeamMember(String ProjectTeamId) {        
        System.debug('Project Team Member ID : ' + ProjectTeamId);
        OpportunityTeamMember tm =  [Select Id,Name,TeamMemberRole,User.Name from               
                                     OpportunityTeamMember where Id=:ProjectTeamId ];   
        System.debug('Team Member Name : '+ tm.User.Name);  
        System.debug('Team Member Role '+ tm.TeamMemberRole);   
        List<String> projectinfo = new List<String>();
        projectinfo.add(tm.User.Name);
        projectinfo.add(tm.TeamMemberRole);
        //return tm.User.Name;
        System.debug(projectinfo);  
        return projectinfo;
    }
    
    // to fetch the Major Metros picklist values from User Object and pass it to the UI 
    @AuraEnabled
    public static List<String> getProjectTeammemberRolevalues(){
        //List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.OPPORTUNITYTEAMMEMBER,UtilityCls.TEAMMEMBERROLE); - Commented by Mb - 07/13/17
        // Start of Code - Mb - 07/13/17
        String profName = UtilityCls.getProfileName(UserInfo.getProfileId());
        String role = '';
        if(profName.contains(UtilityCls.COMMERCIAL)){
            role = 'Team_Role_Commercial__c';
        }else if(profName.contains(UtilityCls.RESIDENTIAL)){
            role = 'Team_Role_Resi__c';
        }else{
            role = 'TeamMemberRole';
        } // End of Code - MB - 07/13/17
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.OPPORTUNITYTEAMMEMBER, role);
        return Cpickvalues;
    }
    
    //To change the Account owner Value by getting the RecordId and the selected userId values in the parameters.
    @AuraEnabled
    public static String GetCurrentOwnerName(String recordId) {
        String Ownername = '';
        //Get prefix from record ID
        //This assumes that you have passed at least 3 characters
        String myIdPrefix = String.valueOf(recordId).substring(0,3);
        if(myIdPrefix!=null){
            if(myIdPrefix.equals(UtilityCls.ACCOUNT_ID_START_NUMBER)){
                Ownername = [Select Id,Owner.Name from Account Where Id=:recordId].Owner.Name;
            } else if(myIdPrefix.equals(UtilityCls.LEAD_ID_START_NUMBER)){
                Ownername = [Select Id,Owner.Name from Lead Where Id=:recordId].Owner.Name;
            }
        }  
        return Ownername;
    }
    
    //To change the Account owner Value by getting the RecordId and the selected userId values in the parameters.
    @AuraEnabled
    public static Boolean ChangeAccountOwner( string RecordId,  string UserId) {
        List<Account> ownerAcclist = new List<Account>();
        Boolean SaveResult = false;
        for(Account acc:[Select Id,OwnerId from Account where Id=:RecordId Limit 1]){
            Account ac = new Account();
            ac.Id= acc.Id;
            ac.OwnerId = UserId;
            ownerAcclist.add(ac);
        }
        if(ownerAcclist.size()>0){
            List<Database.SaveResult> results=Database.update(ownerAcclist,false);
            UtilityCls.createDMLexceptionlog(ownerAcclist,results,'UserSearch_CC','ChangeAccountOwner');
            SaveResult = true;
        }   
        return SaveResult;
    }
    
    //To change the Lead owner Value by getting the RecordId and the selected userId values in the parameters.
    @AuraEnabled
    public static Boolean ChangeLeadOwner( string RecordId,  string UserId) {
        List<Lead> ownerLeadlist = new List<Lead>();
        Boolean SaveResult = false;
        for(Lead lea:[Select Id,OwnerId from Lead where Id=:RecordId Limit 1]){
            Lead le = new Lead();
            le.Id= lea.Id;
            le.OwnerId = UserId;
            ownerLeadlist.add(le);
        }
        if(ownerLeadlist.size()>0){
            List<Database.SaveResult> results=Database.update(ownerLeadlist,false);
            UtilityCls.createDMLexceptionlog(ownerLeadlist,results,'UserSearch_CC','ChangeLeadOwner');
            SaveResult = true;
        }   
        return SaveResult;
    }
    
    // SSB 08-13-17  - update Project team  start
    @AuraEnabled
    public static Boolean editProjectTeammember( string ProjectTeamId,string TeamMemberRole) {
        System.debug('editProjectTeammember : ProjectTeamId : ' + ProjectTeamId);
        System.debug('editProjectTeammember : TeamMemberRole : ' + TeamMemberRole);     
        If(String.isNotBlank(ProjectTeamId) && String.isNotBlank(TeamMemberRole)) {
            OpportunityTeamMember opptm = new OpportunityTeamMember();
            opptm.Id = ProjectTeamId;
            opptm.TeamMemberRole = TeamMemberRole;
            if(ProjectTeamMemList.size()>0){
                ProjectTeamMemList.clear();   
            }
            ProjectTeamMemList.add(opptm);
            if(ProjectTeamMemList.size()>0){
                // Call the inner class without sharing so that non-owners can add team members
                SaveResult = false;
                UpdateOpportunityTeamAccess oppShareAccess = new UpdateOpportunityTeamAccess();
                oppShareAccess.editOppTeamMember();
            }
        }
        System.debug(' Save Result : ' + SaveResult);
        return SaveResult;
    }  // end of method editProjectTeammember
    
    // SSB 08-13-17  update Project team end
    //To change the Account owner Value by getting the RecordId and the selected userId values in the parameters.
    @AuraEnabled
    public static Boolean AddProjectProductTeammembers( string OpportunityId,  string UserId,string TeamMemberRole, string territoryId) {
        If(String.isNotBlank(OpportunityId) && String.isNotBlank(UserId) && String.isNotBlank(TeamMemberRole)){
            //Start of Code - MB - 08/04/2017
            //List<Opportunity> opp = [Select Id  From Opportunity Where Id =: OpportunityId AND RecordType.DeveloperName IN ('Annuity', 'Commercial_Traditional', 'Transactional') ];
            if(ProjectTeamMemList.size()>0){
                ProjectTeamMemList.clear();   
            }
            OpportunityTeamMember opptm = new OpportunityTeamMember();
            opptm.OpportunityId = OpportunityId;
            opptm.UserId = UserId;
            opptm.TeamMemberRole = TeamMemberRole;
            ProjectTeamMemList.add(opptm);
            if(ProjectTeamMemList.size()>0){
                // Call the inner class without sharing so that non-owners can add team members
                SaveResult = false;
                UpdateOpportunityTeamAccess oppShareAccess = new UpdateOpportunityTeamAccess();
                oppShareAccess.InsertOppTeamMember();
            }//End of Code - MB - 08/04/2017
        }      
        return SaveResult;
    }
    
    //Start of Code - MB - 07/21/17
    @AuraEnabled
    public static List<User> getUserInfo(String Name, String AreacodeValue, String StateValue, String Majormetrosvalue, Boolean CorporateUser){
        List<User> retUserList = new List<User>();
        String userBusinessType = [Select Id,Name,Business_Type__c from user where Id=:UserInfo.getUserId()].Business_Type__c;
        String soql ='SELECT Id, Name, Email, Phone, Title, Geography__c, Segment__c, Segment_Group__c, SmallPhotoUrl, Area_Code__c, State, Major_Metros__c, UserRole.Name, MHKUserRole__c FROM User WHERE IsActive = true AND Business_Type__c =: userBusinessType'; //Added condition to get only Active users - MB - 2/28/18
        List<String> lstAlpha=new list<String>();
        //soql += ' Business_Type__c =\''+OuserBusinessType+'\'';
        if(Name!=''){
            soql +=' AND Name Like \'%'+String.escapeSingleQuotes(Name)+ '%\'';
        }
        if(AreacodeValue !=''){
            soql +=' AND Area_Code__c Like \'%'+String.escapeSingleQuotes(AreacodeValue)+ '%\'';
        }
        if(StateValue!=''){
            system.debug('StateValue :::::::'+StateValue);
            soql +=' AND State__c includes( \''+StateValue+ '\')';
            /*Code Added By Susmitha on May 21st for bug 60035
String alpha = stateValue;
lstAlpha = alpha.split(';');
System.debug('lstAlpha::::'+lstAlpha.size());
if(!StateValue.contains(';')){
soql +=' AND State__c = \''+StateValue+ '\'';
}
else if(StateValue.contains(';') && lstAlpha.size()>0){
soql +=' AND State__c in: lstAlpha';
system.debug('Entry else if');
} - Commented by MB - 05/29/18 - Bug 60892 */
        }
        if(Majormetrosvalue !=''){
            soql +=' AND Major_Metros__c includes( \''+Majormetrosvalue+ '\')';
        }
        //Start of Code - MB - 08/07/2017
        if(CorporateUser)
            soql += ' AND Corporate_User__c = :CorporateUser';
        //End of Code - MB - 08/07/2017
        System.debug('SOQL: ' + soql);
        retUserList = Database.Query(soql);
        System.debug('Return List: '+ retUserList);
        return retUserList; 
    }
    // End of Code - MB - 07/21/17
    
    // Start of Code - MB - 06/13/17
    // Private Inner Class without sharing for users to update Opportunity Team and Share access
    private without sharing class UpdateOpportunityTeamAccess{ 
        public void InsertOppTeamMember(){
            List<Database.SaveResult> results = Database.insert(ProjectTeamMemList, false);
            UtilityCls.createDMLexceptionlog(ProjectTeamMemList,results,'UserSearch_CC','InsertOppTeamMember');
            for (Database.SaveResult result : results) {
                if (result.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    SaveResult = true;
                    System.debug('Successfully inserted Opportunity Team Member: ' + result.getId());
                }
                else {
                    // Operation failed, so get all errors
                    SaveResult = false;                
                    
                }
            } 
        }
        // Added by SSB 08-13-17 Start
        public void editOppTeamMember(){
            List<Database.SaveResult> results = Database.update(ProjectTeamMemList, true);
            UtilityCls.createDMLexceptionlog(ProjectTeamMemList,results,'UserSearch_CC','editOppTeamMember');
            for (Database.SaveResult result : results) {
                if (result.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    SaveResult = true;
                    System.debug('Successfully updated Opportunity Team Member: ' + result.getId());
                }
                else {
                    // Operation failed, so get all errors
                    SaveResult = false;                
                    for(Database.Error err : result.getErrors()) {
                        System.debug('Error while inserting Opportunity Team Member: ' + err.getMessage());
                    }
                }
            } 
        } // end of method editOppTeamMember
        // Added by SSB 08-13-17 end
    }
    // End of Code - MB - 06/13/17
}