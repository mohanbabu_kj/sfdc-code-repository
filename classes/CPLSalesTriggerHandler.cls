/**************************************************************************

Name : CPLSalesTriggerHandler

===========================================================================
Purpose : This class is used for CPL_Sales__c Trigger
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Nick W         2/Apr/2018     Created
***************************************************************************/

public with sharing class CPLSalesTriggerHandler{
    // Class Variables
    
    // Static Class Variables
    static{}
    

    // For each CPL Sales record, we need to CPL Sales Relationship for each CPL Object records matching ExternalID
    public static void createCPLObjectRelationships(Map<Id,CPL_Sales__c> newMap){
		List<CPL_Sales__c> newList = [Select Id, External_ID__c, Product__c, Product__r.Salesforce_Product_Category__c
                                     		From CPL_Sales__c Where Id in :newMap.keySet() ORDER BY Product__r.Salesforce_Product_Category__c];
        
        //List of objects to update
        //DML Update operation only support 10 different types of SObjects, se we'll use 2 list to handle the 12 objects
        List<SObject> objectsToUpdate1 = new List<SObject>();
        List<SObject> objectsToUpdate2 = new List<SObject>();

        //Build the Set of ExternalID used to find the matching CPL Objects
        Set<String> externalIdSet = new Set<String>();
        Set<String> productCatSet = new Set<String>();

        for (CPL_Sales__c cs : newList) {

            if (String.isNotEmpty(cs.External_ID__c))
                externalIdSet.add(cs.External_ID__c);

            if (String.isNotEmpty(cs.Product__r.Salesforce_Product_Category__c))
                productCatSet.add(cs.Product__r.Salesforce_Product_Category__c);
        }

		System.debug('### externalIdSet: ' + externalIdSet);
		System.debug('### productCatSet: ' + productCatSet);
        
        //Build a Map with all CPL Objects matching the External Id with not CPL Sales relationship yet, indexed by Product category
        Map<String, List<SObject>> cplObjectsByCat = new Map<String, List<SObject>>();

        cplObjectsByCat.put('Commercial Broadloom',     [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Commercial_Broadloom__c      WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Residential Broadloom',    [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Residential_Broadloom__c     WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Resilient Sheet',          [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Resilient_Sheet__c           WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Carpet Tile',              [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Carpet_Tile__c               WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Cushion',                  [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Cushion__c                   WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('SolidWood',               	[SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Solid_Wood__c                WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('TecWood',                 	[SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Tec_Wood__c                  WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('RevWood',                  [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Rev_Wood__c                  WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Tile',                     [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Tile__c                      WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Resilient Tile',           [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Resilient_Tile__c            WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Installation Accessories', [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Installation_Accessories__c  WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);
        cplObjectsByCat.put('Care and Maintenance',     [SELECT Id, CPL_Sales__c, CPLSales_External_Id__c FROM CPL_Care_and_Maintenance__c      WHERE CPL_Sales__c = null AND CPLSales_External_Id__c IN: externalIdSet ]);


        for (CPL_Sales__c cs : newList) {

            //Get the matching values from CPL Sales record
            String externalID = cs.External_ID__c;
            String productCat = cs.Product__r.Salesforce_Product_Category__c;
            System.debug('### ');
            System.debug('### productCat: ' + productCat + ' - externalID: ' + externalID);
            

            //Process only if both matching values are not null
            if (String.isNotEmpty(externalID) && String.isNotEmpty(productCat)) {

                //The the CPL Objects based on the Product Category of CPL Sales
                List<SObject> objList = (List<SObject>) cplObjectsByCat.get(productCat);
                for (SObject obj : objList) {

                    String objExternalID =  (String) obj.get('CPLSales_External_Id__c');
                    System.debug('### productCat: ' + productCat + ' - externalID: ' + externalID + ' - objExternalID: ' +objExternalID);

                    //Update CPL Sales Relationship if external ID matches
                    if (externalID ==  objExternalID) {
                        obj.put('CPL_Sales__c', cs.Id);

                        //Add the object to one of the list to update
                        if (productCat == 'Commercial Broadloom' || productCat == 'Residential Broadloom' || productCat == 'Resilient Sheet')
                            objectsToUpdate1.add(obj);
                        else 
                            objectsToUpdate2.add(obj);
                    }
                }
            }
        }
        
        //Commit changes to DB
        if (objectsToUpdate1.size() > 0) {
            List<Database.SaveResult> results = Database.update(objectsToUpdate1);
            UtilityCls.createDMLexceptionlog(objectsToUpdate1,results,'CPLSalesTriggerHandler','createCPLObjectRelationships');
        }

        if (objectsToUpdate2.size() > 0){
            List<Database.SaveResult> results = Database.update(objectsToUpdate2);
            UtilityCls.createDMLexceptionlog(objectsToUpdate2,results,'CPLSalesTriggerHandler','createCPLObjectRelationships');
        }
    }

    
    
    // For each CPL Sales record, we need to insert a Account CPL Sales Relationship for each Account with the same Account Number
    public static void createAccountCPLSalesRelationships(List<CPL_Sales__c> newList){
        Set<String> setChainNum = new Set<String>();
        Map<String,List<CPL_Sales__c>> mapChainAccountToCPLSales = New Map<String,List<CPL_Sales__c>>();
        
        List<Account_CPL_Sales_Relationship__c> lstNewAcctCPLSRelationships = new List<Account_CPL_Sales_Relationship__c>();
        
        for(CPL_Sales__c cs : newList){
            setChainNum.add(cs.Division_Customer_No_SFX_Id__c);
            
            if (!mapChainAccountToCPLSales.containsKey(cs.Division_Customer_No_SFX_Id__c)){
                mapChainAccountToCPLSales.put(cs.Division_Customer_No_SFX_Id__c, New List<CPL_Sales__c>());
            }
            mapChainAccountToCPLSales.get(cs.Division_Customer_No_SFX_Id__c).add(cs);
        }
        
        for(Account a : [SELECT Id, CAMS_Account_number__c, Chain_Number__c 
                        FROM Account 
                        WHERE Chain_Number__c IN :setChainNum 
                        AND Business_Type__c = :UtilityCls.RESIDENTIAL]){
                            
        	for(CPL_Sales__c cs : mapChainAccountToCPLSales.get(a.Chain_Number__c)){      
                lstNewAcctCPLSRelationships.add(new Account_CPL_Sales_Relationship__c(Account__c = a.Id, 
                                                                                      CPL_Sales__c = cs.Id,
                                                                                      External_Id__c = a.CAMS_Account_number__c + '.' + cs.External_ID__c,
                                                                                      CPL_Sales_External_Id__c = cs.External_ID__c,
                                                                                      Account_External_Id__c = a.CAMS_Account_number__c));
            }    
       
        }
        
        if(!lstNewAcctCPLSRelationships.isEmpty()){
            Schema.SObjectField f = Account_CPL_Sales_Relationship__c.fields.External_Id__c;
            Database.UpsertResult[] results = Database.upsert(lstNewAcctCPLSRelationships, f, false);
        }
        
        /*Set<String> setChainNum = new Set<String>();
        Set<String> setProductId = new Set<String>();
        
        Set<String> setAcctCPLSRelationships = new Set<String>();
        List<Account_CPL_Sales_Relationship__c> lstNewAcctCPLSRelationships = new List<Account_CPL_Sales_Relationship__c>();
        
        for(CPL_Sales__c cs : newList){
            setChainNum.add(cs.Division_Customer_No_SFX_Id__c);
            setProductId.add(cs.Product_Unique_Id__c);
        }
        
        for(Account_CPL_Sales_Relationship__c a : [SELECT Id, Account_and_CPL_Sales_Ids__c
                FROM Account_CPL_Sales_Relationship__c
                WHERE Account__r.Chain_Number__c IN :setChainNum AND CPL_Sales_Product_Unique_Id__c IN : setProductId]){
            setAcctCPLSRelationships.add(a.Account_and_CPL_Sales_Ids__c);
        }
        
        for(Account a : [SELECT Id, CAMS_Account_number__c 
                        FROM Account 
                        WHERE Chain_Number__c IN :setChainNum 
                        AND Business_Type__c = :UtilityCls.RESIDENTIAL]){
            for(CPL_Sales__c cs : newList){      
                if(!setAcctCPLSRelationships.contains(a.CAMS_Account_number__c + '-' + cs.Product_Unique_Id__c)){
                    lstNewAcctCPLSRelationships.add(new Account_CPL_Sales_Relationship__c(Account__c = a.Id, CPL_Sales__c = cs.Id));
                }
            }
        }
        
        if(!lstNewAcctCPLSRelationships.isEmpty()){
            Database.SaveResult[] results = Database.insert(lstNewAcctCPLSRelationships, false);
            UtilityCls.createDMLexceptionlog(lstNewAcctCPLSRelationships, 
                results, 'CPLSalesTriggerHandler', 'createAccountCPLSalesRelationships');
        }
*/
    }
}