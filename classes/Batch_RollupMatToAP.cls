/************************
Batch class is used to rollup Account Profile Using MAT.
************************/
global class Batch_RollupMatToAP implements Database.Batchable<sobject>, Database.Stateful,Schedulable  {
    public String query;
    public dateTime batchRunDate;
   //Start -logic to run batch in DefaultQuery mode or LastBatchRun mode - Nagendra Changes 20-11-18
    global Batch_RollupMatToAP (Boolean runDefaultQuery, Boolean LastBatchRunDate) {
         
        string RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId();
        string lastModifiedUserName = 'Integration User';
       //  DateTime batchRunDate;
         
        MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('Batch_RollupMatToAP');
        if (custoSettingVal != null) {
            batchRunDate = custoSettingVal.Last_Run_At__c != null ? custoSettingVal.Last_Run_At__c : system.now();
            batchRunDate = (custoSettingVal.MinutesDelay__c !=null && batchRunDate !=null ) ? batchRunDate.addMinutes(Integer.valueof(custoSettingVal.MinutesDelay__c )) : batchRunDate ;
        }
        
        if (runDefaultQuery) {  
            Batch_Query__mdt[] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'RollupActualsFromMatToAP_Batch']; //default query 
            If(QureyList.size() > 0) {
                query = QureyList[0].Query_String__c;                
            } else {
              this.query = 'SELECT Id,Account_Profile__c FROM Mohawk_Account_Team__c WHERE lastmodifiedby.name = \''+ lastModifiedUserName +'\' AND RecordTypeId = \''+ RecordTypeId +'\' ORDER BY Account_Profile__c ASC';
            }
        } else if (LastBatchRunDate) {
            this.query = 'SELECT Id,Account_Profile__c FROM Mohawk_Account_Team__c WHERE LastModifiedDate >= :batchRunDate AND lastmodifiedby.name = \''+ lastModifiedUserName +'\' AND RecordTypeId = \''+ RecordTypeId +'\' ORDER BY Account_Profile__c ASC';   
        }
           
    }
    //End - Nagendra Changes

    
    global Database.QueryLocator start(Database.BatchableContext info) {
        system.debug('####query'+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext info, List<Mohawk_Account_Team__c> scope) {
        set<string>apIDS = new set<string>();
        List<Account_Profile__c>apToUpdate =  new LIst<Account_Profile__c>();
        for( Mohawk_Account_Team__c mat : scope ){
            if( mat.Account_Profile__c != null ){
                apIDS.add( mat.Account_Profile__c );
            }
        }
        if( apIDS != null && apIDS.size() > 0 ){
            for( Account_Profile__c AP : getAPforRollUp( apIDS ) ){

                ap.R_GOpp_A__c = 0;
                ap.R_Yellow_Opportunity_Aladdin__c = 0;
                ap.R_ROpp_A__c = 0;
                ap.B_GOpp_A__c = 0;
                ap.B_Yellow_Opportunity_Aladdin__c = 0;
                ap.B_ROpp_A__c = 0;
                ap.R_GOpp_Cus_A__c = 0;
                ap.R_YOpp_Cus_Ala__c = 0;
                ap.R_ROpp_Cus_A__c = 0;
                ap.B_GOpp_Cus_A__c = 0;
                ap.B_YOpp_Cus_Ala__c = 0;
                ap.B_ROpp_Cus_A__c = 0;
                ap.R_GOpp_Hor__c = 0;
                ap.R_Yellow_Opportunity_Horizon__c = 0;
                ap.R_ROpp_Hor__c = 0;
                ap.B_GOpp_Hor__c = 0;
                ap.B_Yellow_Opportunity_Horizon__c = 0;
                ap.B_ROpp_Hor__c = 0;
                ap.R_GOpp_Cus_Hor__c = 0;
                ap.R_YOpp_Cus_Hor__c = 0;
                ap.R_ROpp_Cus_Hor__c = 0;
                ap.B_GOpp_Cus_Hor__c = 0;
                ap.B_YOpp_Cus_Hor__c = 0;
                ap.B_ROpp_Cus_HoB__c = 0;
                ap.R_GOpp_Kar__c = 0;
                ap.R_Yellow_Opportunity_Karastan__c = 0;
                ap.R_ROpp_K__c = 0;
                ap.B_GOpp_Kar__c = 0;
                ap.B_Yellow_Opportunity_Karastan__c = 0;
                ap.B_ROpp_K__c = 0;
                ap.R_GOpp_Cus_Kar__c = 0;
                ap.R_YOpp_Cus_Kar__c = 0;
                ap.R_ROpp_Cus_K__c = 0;
                ap.B_GOpp_Cus_Kar__c = 0;
                ap.B_YOpp_Cus_Kar__c = 0;
                ap.B_ROpp_Cus_K__c = 0;
                ap.R_GOpp_AC__c = 0;
                ap.R_Yellow_Opportunity_Aladdin_Comm__c = 0;
                ap.R_ROpp_AC__c = 0;
                ap.B_GOpp_AC__c = 0;
                ap.B_Yellow_Opportunity_Aladdin_Comm__c = 0;
                ap.B_ROpp_AC__c = 0;
                ap.R_GOpp_Cus_AC__c = 0;
                ap.R_YOpp_Cus_Ala_Comm__c = 0;
                ap.R_ROpp_Cus_AC__c = 0;
                ap.B_GOpp_Cus_AC__c = 0;
                ap.B_YOpp_Cus_Ala_Comm__c = 0;
                ap.B_ROpp_Cus_AC__c = 0;
                ap.R_GOpp_RC__c = 0;
                ap.R_Yellow_Opportunity_Resilient_Comm__c = 0;
                ap.R_Red_Opportunity_Resilient_Comm__c = 0;
                ap.B_GOpp_RC__c = 0;
                ap.B_Yellow_Opportunity_Resilient_Comm__c = 0;
                ap.B_Red_Opportunity_Resilient_Comm__c = 0;
                ap.R_GOpp_Car_PP__c = 0;
                ap.R_YOpp_Car_PP__c = 0;
                ap.R_ROpp_Car_PP__c = 0;
                ap.B_GOpp_Car_PP__c = 0;
                ap.B_YOpp_Car_PP__c = 0;
                ap.B_ROpp_Car_PP__c = 0;
                ap.R_GOpp_Cus_PP__c = 0;
                ap.R_YOpp_Cus_PP__c = 0;
                ap.R_ROpp_Cus_PP__c = 0;
                ap.B_GOpp_Cus_PP__c = 0;
                ap.B_YOpp_Cus_PP__c = 0;
                ap.B_ROpp_Cus_PP__c = 0;
                ap.R_GOpp_Har_PP__c = 0;
                ap.R_YOpp_Har_PP__c = 0;
                ap.R_ROpp_Har_PP__c = 0;
                ap.B_GOpp_Har_PP__c = 0;
                ap.B_YOpp_Har_PP__c = 0;
                ap.B_ROpp_Har_PP__c = 0;
                ap.R_GOpp_Lam_PP__c = 0;
                ap.R_YOpp_Lam_PP__c = 0;
                ap.R_ROpp_Lam_PP__c = 0;
                ap.B_GOpp_Lam_PP__c = 0;
                ap.B_YOpp_Lam_PP__c = 0;
                ap.B_ROpp_Lam_PP__c = 0;
                ap.R_GOpp_Tile_PP__c = 0;
                ap.R_YOpp_Tile_PP__c = 0;
                ap.R_ROpp_Tile_PP__c = 0;
                ap.B_GOpp_Tile_PP__c = 0;
                ap.B_YOpp_Tile_PP__c = 0;
                ap.B_ROpp_Tile_PP__c = 0;
                ap.R_GOpp_Res_PP__c = 0;
                ap.R_YOpp_Res_PP__c = 0;
                ap.R_ROpp_Res_PP__c = 0;
                ap.B_GOpp_Res_PP__c = 0;
                ap.B_YOpp_Res_PP__c = 0;
                ap.B_ROpp_Res_PP__c = 0;
                ap.R_GOpp_Har_RW__c = 0;
                ap.R_YOpp_Har_RW__c = 0;
                ap.R_ROpp_Har_RW__c = 0;
                ap.B_GOpp_Har_RW__c = 0;
                ap.B_YOpp_Har_RW__c = 0;
                ap.B_ROpp_Har_RW__c = 0;
                ap.R_GOpp_Lam_RL__c = 0;
                ap.R_YOpp_Lam_RL__c = 0;
                ap.R_ROpp_Lam_RL__c = 0;
                ap.B_GOpp_Lam_RL__c = 0;
                ap.B_YOpp_Lam_RL__c = 0;
                ap.B_ROpp_Lam_RL__c = 0;
                ap.R_GOpp_Tile_RT__c = 0;
                ap.R_YOpp_Tile_RT__c = 0;
                ap.R_ROpp_Tile_RT__c = 0;
                ap.B_GOpp_Tile_RT__c = 0;
                ap.B_YOpp_Tile_RT__c = 0;
                ap.B_ROpp_Tile_RT__c = 0;
                ap.R_GOpp_RR__c = 0;
                ap.R_Yellow_Opportunity_Resilient_Retail__c = 0;
                ap.R_Red_Opportunity_Resilient_Retail__c = 0;
                ap.B_GOpp_RR__c = 0;
                ap.B_Yellow_Opportunity_Resilient_Retail__c = 0;
                ap.B_Red_Opportunity_Resilient_Retail__c = 0;
                ap.R_GOpp_Car__c = 0;
                ap.R_Yellow_Opportunity_Carpet__c = 0;
                ap.R_ROpp_Car__c = 0;
                ap.B_GOpp_Car__c = 0;
                ap.B_Yellow_Opportunity_Carpet__c = 0;
                ap.B_ROpp_Car__c = 0;
                ap.R_GOpp_Cus__c = 0;
                ap.R_Yellow_Opportunity_Cushion__c = 0;
                ap.R_ROpp_Cus__c = 0;
                ap.B_GOpp_Cus__c = 0;
                ap.B_Yellow_Opportunity_Cushion__c = 0;
                ap.B_ROpp_Cus__c = 0;
                ap.R_GOpp_Har__c = 0;
                ap.R_Yellow_Opportunity_Hardwood__c = 0;
                ap.R_ROpp_Har__c = 0;
                ap.B_GOpp_Har__c = 0;
                ap.B_Yellow_Opportunity_Hardwood__c = 0;
                ap.B_ROpp_Har__c = 0;
                ap.R_GOpp_L__c = 0;
                ap.R_Yellow_Opportunity_Laminate__c = 0;
                ap.R_ROpp_L__c = 0;
                ap.B_GOpp_L__c = 0;
                ap.B_Yellow_Opportunity_Laminate__c = 0;
                ap.B_ROpp_L__c = 0;
                ap.R_GOpp_T__c = 0;
                ap.R_Yellow_Opportunity_Tile__c = 0;
                ap.R_Red_Opportunity_Tile__c = 0;
                ap.B_GOpp_T__c = 0;
                ap.B_Yellow_Opportunity_Tile__c = 0;
                ap.B_Red_Opportunity_Tile__c = 0;
                ap.R_GOpp_R__c = 0;
                ap.R_Yellow_Opportunity_Resilient__c = 0;
                ap.R_ROpp_R__c = 0;
                ap.B_GOpp_R__c = 0;
                ap.B_Yellow_Opportunity_Resilient__c = 0;
                ap.B_ROpp_R__c = 0;

                apToUpdate.add( MohawkAccountTeamService.calculateRollupfromMAT( AP ) );
            }
            if( apToUpdate != null && apToUpdate.size() > 0 ){
                MohawkAccountTeamService.updateFromEvent = true;
                update apToUpdate;
            }
               
        }
    }
    
    global void finish(Database.BatchableContext info) {
        //Start-logic to run batch in DefaultQuery mode or LastBatchRun mode NAgendra changes 20-11-18
        AsyncApexJob a = [SELECT Id, Status, CompletedDate FROM AsyncApexJob WHERE Id =: info.getJobId()];
        MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('Batch_RollupMatToAP');
        if (custoSettingVal != null) {
            custoSettingVal.Last_Run_At__c = a.CompletedDate;
            update custoSettingVal;
        }else{
            custoSettingVal = new MAT_Batch_Setting__c(name = 'Batch_RollupMatToAP', Last_Run_At__c = a.CompletedDate);
            insert custoSettingVal;
        }
        //End- NAgendra changes
    }
    
    private static List<Account_Profile__c> getAPforRollUp( set<string>apids ){
        return [ SELECT id
                ,Product_Category_Allocation_Carpet__c
                ,Product_Category_Allocation_Cushion__c
                ,Product_Category_Allocation_Wood__c
                ,Product_Category_Allocation_Laminate__c
                ,Product_Category_Allocation_Tile__c
                ,Product_Category_Allocation_Resilient__c              
                FROM Account_Profile__c WHERE id IN : apids ];
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
      database.executeBatch(new Batch_RollupMatToAP(false, true), 200);
    }
}