/**************************************************************************

Name : TaskSalesCoachingExt_Test 

===========================================================================
Purpose :   This class is used for TaskSalesCoachingExt
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        3/May/2017     Created 
***************************************************************************/
@isTest
private class TaskSalesCoachingExt_Test {  
    static List<Account> resAccForInvList;
    static List<Coaching_Conversation__c> ccList;
    static Coaching_Conversation__c coachingcon;

    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        resAccForInvList = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        ccList =  Utility_Test.createCoachingConverstions(true, 1);
        if(!ccList.isEmpty()){
            
          coachingcon = ccList.get(0);
        }        
    }

    static testMethod void testMethod1() {
       System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
                init();
           system.assert(userInfo.getUserId()==Utility_Test.ADMIN_USER.Id);
                ApexPages.StandardController sc = new ApexPages.StandardController(coachingcon);
                TaskSalesCoachingExt taskSalesCE = new TaskSalesCoachingExt(sc);
                taskSalesCE.getReportIds();
                Boolean isLeadAccess = taskSalesCE.validateObjectAccess('Lead');
                system.assert(isLeadAccess != null);
            Test.stopTest();
        }    
    }
}