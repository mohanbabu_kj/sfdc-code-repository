public without sharing class CPLObjectsTriggerHandler {

    public static void updateCPLSalesRelationship(List<SObject> cplObjects){ 

        System.Debug('### ');
        System.Debug('### --------------------------------------------------------------');


        //Build Chain Number and Product SETs to be used in query for CPL_Sales 
        Set<String> salesExtIDSet =     new Set<String>();

        for (SObject obj : cplObjects) {
            String salesExtID = (String) obj.get('CPLSales_External_ID__c');

            if (String.isNotEmpty(salesExtID))
                salesExtIDSet.add(salesExtID);
        }

        System.Debug('### CPLObjectsTriggerHandler - salesExtIDSet: ' + salesExtIDSet);

        //Query CPL_Sales records and create a Map indexed by External ID
        Map<String, String> cplMap = new Map<String, String>();

        for (CPL_Sales__c cplSalesObj : [SELECT Id, External_ID__c
                                        FROM CPL_Sales__c 
                                        WHERE External_ID__c IN : salesExtIDSet]) {

            String key = cplSalesObj.External_ID__c;

            System.Debug('### CPLObjectsTriggerHandler - creating key: ' + key + ' for id: ' + cplSalesObj.Id);
            cplMap.put(key, cplSalesObj.Id);
        }


        //Update CPL Object with CPL_Sales ID for matching record by Product and Account Chain number
        for (SObject obj : cplObjects) {

            //Get product and chain nunmber from CPL Objet
            String externalId =     (String) obj.get('CPLSales_External_ID__c');

            //Retrieve the CPL_Sales id from the Map
            String cplSalesId = cplMap.get(externalId); 

            System.Debug('### CPLObjectsTriggerHandler - retrieving key: ' + externalId + ' =  ' + cplSalesId);

            //If the cplSalesId is not null, then a match exist and CPL_Sales id needs to be updated in CPL_Object      
            if (String.isNotEmpty(cplSalesId)) {
                obj.put('CPL_Sales__c', cplSalesId);
            }
            
        }

    
    }
    
}