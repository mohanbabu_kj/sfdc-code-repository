/**************************************************************************
 
Name : SampleDisplayPerformance_CC_Test 
===========================================================================
Purpose : Uitlity class to SampleDisplayPerformance_CC test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        26/APR/2017    Create  
2.0        Sasi Naik    2/May/2017      Updated
***************************************************************************/

@isTest
private class SampleDisplayPerformance_CC_Test {
  static List<Sample_Display_Inventory__c> sampleDisInvList = new List<Sample_Display_Inventory__c>();
  static List<Account> accList;
  static Account acc;
  static List<Mashup_Management__c> mashupManList; 
  static Mashup_Management__c data;

    public static void init(){   
        Utility_Test.getTeamCreationCusSetting();
        mashupManList  = Utility_Test.getMashupManagementCusSetting();
        data = mashupManList.get(0);
        accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
        acc = (Account)accList.get(0);
        acc.Customer_Group__c = 'Open Line(OL);Abbey(IA)';
        update acc;
        
        System.assert(acc != null);
        
        Sample_Display_Inventory__c samDisInv1 = new Sample_Display_Inventory__c(
        Account__c = acc.Id,
        //Architech_Folder__c ='Antiek', 
        Brand__c = 'Horizon',
        Customer_Group__c = 'Open Line (OL)',
        RecordTypeId = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get(UtilityCls.SAMPLECOMMERCIALDISPLAY).getRecordTypeId());
        sampleDisInvList.add(samDisInv1);
        insert samDisInv1;
        
        System.assert(samDisInv1 != null);
        
        mashupManList  = Utility_Test.getMashupManagementCusSetting();
        data = mashupManList.get(0);
    }

    static testMethod void testMethod1() {
      init();
      test.startTest();
        SampleDisplayPerformance_CC.getAccountDisplaySamples(acc.Id, 'Horizon', 'Open Line' );
        SampleDisplayPerformance_CC.saveSamplesData(sampleDisInvList);
        SampleDisplayPerformance_CC.getPlacementStatusPicklistValues();
        SampleDisplayPerformance_CC.getBrandCollectionPicklistValues();
        SampleDisplayPerformance_CC.getCustomerGroupPicklistValues(acc.Id);
        SampleDisplayPerformance_CC.getMashupUrl(data.Name, data.Id);
      test.stopTest();
    }

}