/**************************************************************************

Name : AccNonInvoicingNogenerator 

===========================================================================
Purpose :   provide the reuse function for http request
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0       Sasi Naik      11/04/2017     Created 
***************************************************************************/
global with sharing class AccNonInvoicingNogenerator {
    // Called the method form the batch class by passing the Set of Account ids 
    public static void SendHttpsreq(Set<Id> newacclistids){
        
        List<Account> lupdateaccnoninvoice = new List<Account>();
        //SOQL for loop to get the Account list and do call the API services
        for(Account acc:[Select Id,Name,RecordTypeId,Business_Type__c From Account Where Id IN:newacclistids Limit 100]){
            try {
                //initiate the HttpRequest
                HttpRequest req = new HttpRequest();
                
                //req.setEndpoint('https://pubtst.virtualservices.mohawkind.com/SOAT305/vPUB.MHK.eMPower.UniqueIDGeneratorREST.svc');
                //pass the callout form the naming credentials
                req.setEndpoint('callout:Non_Invoice_Number_Generator');
                req.setMethod('POST');
                //req.setHeader('Content-Type', 'application/json');
                req.setHeader('Content-Type', 'application/json;charset=UTF-8');
                String strReqBody = '{"buID":"1","erpID":"1","erpKey":"2142","objectType":"SFDC_ACCOUNT"}';
                //req.setHeader('Authorization', 'Basic ZU1wb3dlcl9oeWJyaXM6R3IzQHRKbzgh');
                
                //System.debug('SendHttpRequest : request Body : '+ strReqBody);
                req.setBody(strReqBody);
                
                Http http = new Http();
                String strResBody = '';
                HttpResponse res = new HttpResponse();
                
                if(!test.isRunningTest()){
                    res = http.send(req);
                    strResBody = res.getBody();           
                }else{
                    strResBody =  ('{"UniqueId" : "000000123"}' );
                    res.setStatusCode(200); // Added by MB - 07/10/17
                }
                
                ResultHandler result = (ResultHandler)JSON.deserialize(strResBody, ResultHandler.class);
                
                
                if(res.getStatusCode() == 200){
                    Account a = new Account(Id = acc.Id);
                    if(acc.Business_Type__c == UtilityCls.COMMERCIAL){
                        a.Global_Account_Number__c = result.uniqueId;
                    } else{
                        a.Global_Account_Number__c = result.uniqueId;
                    }                       
                    lupdateaccnoninvoice.add(a);  
                    if(Test.isRunningTest()){
                        throw new DMLException('AccNonInvoicingNogenerator EXCEPTION'); 
                    }
                }    
            } catch (Exception ex) {
                // try to record the error message
                String sourceFunction ='class to post the reuest and get the responsone as the auto generator number which will store in the Account Non-Invoicing RcordType.';
                String logCode = 'the method is AccNonInvoicingNogenerator.Sendreq' ;
                String refreenceInfo = 'We use the Global_Account_Number__c field to save the response value';
                UtilityCls.createExceptionLog(ex,  'AccNonInvoicingNogenerator.Sendreq', sourceFunction, logCode, acc.Id, 'Gloabl Account Number');
                
            }
        }
        //Check the list is having the records then only process in the if condition
        if(lupdateaccnoninvoice.size()>0){
            Set<Id> myMethodToaddToErrorObjectList = new Set<Id>();
            //if(!Test.isRunningTest()){
            Database.SaveResult[] lsr = Database.update(lupdateaccnoninvoice, false);
            for(Database.SaveResult sr : lsr){
              if (!sr.isSuccess()) {
                  myMethodToaddToErrorObjectList.add(sr.getId());
                 }
              }             
              
             //}
          
                // try to record the DML error message
                for (Id accountid : myMethodToaddToErrorObjectList) {
                    String sourceFunction ='class to post the reuest and get the responsone as the auto generator number which will store in the Account Non-Invoicing RcordType.';
                    String logCode = 'DML Exception - Failuer to update Account Record' ;
                    String refreenceInfo = 'We use the Global_Account_Number__c field to save the response value';
                    UtilityCls.createExceptionLog(new DMLException(),  'AccNonInvoicingNogenerator.Sendreq', sourceFunction, logCode, accountid, 'Gloabl Account Number');
                }
            }
            
        
    }
    
    //Wrapper Class to collect the response value
    Public Class ResultHandler{
        //field which will collect the response body value intot he uniqueId
        public String uniqueId; // ex : "UniqueId" : "000000123"
    }
}