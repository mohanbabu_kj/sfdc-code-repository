/**************************************************************************

Name : UpdateAccMarketSegTotal_Test
===========================================================================
Purpose : Uitlity class to UpdateAccMarketSegTotal class
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha        27/Feb/2018  Create  
***************************************************************************/
@isTest
private class UpdateAccMarketSegTotal_Test {
    static testMethod void testMethod1() {
        Test.startTest();
        
        List<Account> accList=   Utility_Test.createAccounts(true,2);
        List<Opportunity> oppList=   Utility_Test.createOpportunities(true,2,accList);
        List<Related_Account__c> relAccList=Utility_Test.createRelatedAccountList(true,2,accList,oppList);
        
        String query='SELECT ID,NAME FROM Account LIMIT 1000';
        UpdateAccMarketSegTotal uamst= new UpdateAccMarketSegTotal(query);
        String jobId = Database.executeBatch(uamst);
        System.assert(jobId != null);
        Test.stopTest();
    }
}