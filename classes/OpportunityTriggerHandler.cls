/**************************************************************************

Name : OpportunityTriggerHandler

===========================================================================
Purpose : This class is used for Opportunity Trigger actions
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand         2/Feb/2017     Created  

***************************************************************************/
public with sharing class OpportunityTriggerHandler {
    
    /*
* Purpose : Create Related Accounts on opportunity based on partner accounts of opportunity account
*/
    public static Id commTraRtId = UtilityCls.getRecordTypeInfo('Opportunity', 'Commercial Traditional');
    public static Id resPartRTId = UtilityCls.getRecordTypeInfo('Partner_Account__c', 'Residential'); //Added by MB - 3/8/18 - Bug 57347
    public static Id resRelAccRTId = UtilityCls.getRecordTypeInfo('Related_Account__c', 'Residential'); //Added by MB - 3/29/18 - Bug 57183
    public static Id commRelAccRTId = UtilityCls.getRecordTypeInfo('Related_Account__c', 'Commercial'); //Added by MB - 3/29/18 - Bug 57183
    public static Default_Configuration__c defConfig = Default_Configuration__c.getOrgDefaults();
    
    public static void createRelatedAccounts(List<Opportunity> oppList, Map<Id, Opportunity> oppMap) {
        User usr = [Select CAMS_Employee_Number__c from User where Id  =: UserInfo.getUserId()]; //  Added by sb 10-18-17
        
        Set<Id> acIds = new Set<Id>();
        for(Opportunity opp : oppList){
            acIds.add(opp.AccountId);
        }
        
        //Map initlize the each accountid with list of partner accounts.
        Map<Id, List<Partner_Account__c>> partnerMap =  new Map<Id, List<Partner_Account__c>>();
        
        for(Partner_Account__c each : [Select Non_Invoicing_Account__c,Invoicing_Account__c,Role__c FROM Partner_Account__c 
                                       WHERE Non_Invoicing_Account__c in: acIds and RecordTypeId = :resPartRTId]){ //Added by MB - 3/7/18 - Bug 57347 - Added condition only for residential
                                           if(partnerMap.containsKey(each.Non_Invoicing_Account__c)){
                                               partnerMap.get(each.Non_Invoicing_Account__c).add(each);
                                           }else{
                                               List<Partner_Account__c> partnerAcs = new List<Partner_Account__c>();
                                               partnerAcs.add(each);
                                               partnerMap.put(each.Non_Invoicing_Account__c,partnerAcs);
                                           }
                                       }
        //intialize related Accounts with partner accounts to insert
        List<Related_Account__c> relatedAcList = new List<Related_Account__c>();
        for(Opportunity opp : oppList){
            if(!partnerMap.isEmpty() && partnerMap.containsKey(opp.AccountId)){
                for(Partner_Account__c each : partnerMap.get(opp.AccountId)){
                    //relatedAcList.add(initRelAccount(opp.Id, each.Invoicing_Account__c, each.Role__c, false));
                    Related_Account__c relAcc = initRelAccount(opp.Id, each.Invoicing_Account__c, each.Role__c, false);
                    relAcc.RecordTypeId = resRelAccRTId; //Included RecordTypeId as part of bug 57183 - MB - 3/29/18
                    relatedAcList.add(relAcc);
                }//end of inner for loop
            }
            //Create relete account for opportunity's End Primary user 
            //Start of Comment - MB - 3/29/18 - 
            //Commented by Mb - As we are not using Primary End User and Dealer in Opp. Header details. Manually will be added in Related Account
            /*if(opp.Primary_End_User__c != null){
relatedAcList.add(initRelAccount(opp.id, opp.Primary_End_User__c, 'End User', true));
}

//Create relete account for opportunity's End Primary user
if(opp.Primary_Dealer__c != null){
relatedAcList.add(initRelAccount(opp.id, opp.Primary_Dealer__c, 'Dealer', true));
}*/ 
            //End of Comment - MB - 3/29/18
            //Create relate account for account as specifier
            if(opp.RecordTypeId == commTraRtId){ //Added by MB - Bug 55834 - 3/28/18 - Check only for Commercial
                if(opp.AccountId != null){
                    //relatedAcList.add(initRelAccount(opp.id, opp.AccountId, 'Specifier', true)); - Commented by MB - 08/14/17
                    //Start of Code -MB- 08/14/17
                    Related_Account__c relAcc = initRelAccount(opp.id, opp.AccountId, 'Specifier', true);
                    relAcc.RecordTypeId = commRelAccRTId; //Included RecordTypeId as part of bug 57183 - MB - 3/29/18
                    if(opp.Contact__c != null){
                        relAcc.Contact__c = opp.Contact__c;
                        relAcc.External_ID__c = opp.id + '_' + opp.AccountID + '_' + relAcc.Contact__c;
                        relAcc.CAMS_Employee_ID_of_logged_in_User__c = usr.CAMS_Employee_Number__c;  //  Added by sb 10-18-17
                    }else{
                        relAcc.External_ID__c = opp.id + '_' + opp.AccountId;
                        relAcc.CAMS_Employee_ID_of_logged_in_User__c = usr.CAMS_Employee_Number__c; //  Added by sb 10-18-17
                    }
                    relatedAcList.add(relAcc);
                    //End of Code -MB- 08/14/17
                }
            }
            
        }//End of for loo;
        
        //Insert related accounts 
        /*Commented by MB - 08/02/2017
try{
database.INSERT(relatedAcList);
}Catch(Exception e){
system.debug('Error Occured::'+e);
} - End of Comment by MB - 08/02/2017 */
        // Start of Code - MB - 08/02/2017
        if(!relatedAcList.isEmpty()){ //Added by MB - Bug 55834 - 3/28/18
            List<Database.SaveResult> srList = Database.insert(relatedAcList, false);
            UtilityCls.createDMLexceptionlog(relatedAcList,srList,'OpportunityTriggerHandler','createRelatedAccounts');
            
            Map<Id, String> errorMap = new Map<Id, String>();
            String errorMsg = 'Error Occured while saving';
            // Iterate through each returned result
            for(Integer i =0; i < srList.size() ; i ++ ){
                Database.SaveResult sr = srList[i];
                if(!sr.isSuccess()){
                    Related_Account__c relAcc = relatedAcList[i];
                    System.debug(LoggingLevel.INFO, '*** srs: ' + sr);
                    //portunity opp = oppMap.get(relAcc.Opportunity__c);
                    for(Database.Error err : sr.getErrors()) {
                        errorMsg = errorMsg + ': ' + err.getMessage();
                    }
                    errorMap.put(relAcc.Opportunity__c, errorMsg);
                    for(Opportunity opp: oppList){
                        String errMsg = errorMap.get(opp.Id);
                        if(errMsg != null){
                            opp.addError(errMsg);
                        }
                    }
                }
            }
        }
        // End of Code - MB - 08/02/2017
    }
    
    //Initialize Related Account 
    private static Related_Account__c initRelAccount(Id oppId, Id acId, String role, boolean isPrimary){
        
        return new Related_Account__c(Opportunity__c  = oppId,
                                      Account__c      = acId,
                                      Project_Role__c = role,
                                      Primary__c      = isPrimary);                             
        
    }
    
    // Start of of Code - SSB- 9-28-17 - 
    // this code is needed for SFDC to LDS interface 
    // and should be deactivated once rollout is complete
    public static void ldsFlagAndLostToCompetitor(List<Opportunity> newList,Boolean update_flag,Boolean insert_flag ){
        User usr = [Select CAMS_Employee_Number__c from User where Id  =: UserInfo.getUserId()];
        for(Opportunity  opp : newList){
            opp.CAMS_Employee_ID_of_Logged_n_User__c = usr.CAMS_Employee_Number__c; 
            if(update_flag && opp.RecordTypeId == commTraRtId){
                /*      if(opp.LDS_Create__c){        // All updates will be handlled in batch mode. 10-16-17
opp.LDS_Create__c = false; 
}   
if(!opp.LDS_Update__c){
opp.LDS_Update__c = true; 
} */                            // All updates will be handlled in batch mode. 10-16-17
            } else if(insert_flag && opp.RecordTypeId == commTraRtId) {
                
                opp.LDS_Create__c = true;
                opp.LDS_Update__c = false;
            }
        }
    }
    // End of Code - SSB- 9-28-17
    
    
    //Start of Code - 10/16/17
    public static void updateOppOnBefore(List<Opportunity> newList, Boolean before_insert, Boolean before_update){
        for(Opportunity opp: newList){
            if(opp.Status__c == Null && before_insert == true){
                if( opp.StageName != UtilityCls.PROJECT_STAGE_CLOSED_WON && opp.StageName != UtilityCls.PROJECT_STAGE_CLOSED_LOST){
                    opp.Status__c = UtilityCls.INPROCESS;
                }else{
                    opp.Status__c = UtilityCls.PROJECT_STATUS_CLOSED;
                }
            }else if(before_update == true && 
                     ( opp.StageName == UtilityCls.PROJECT_STAGE_CLOSED_WON || opp.StageName == UtilityCls.PROJECT_STAGE_CLOSED_LOST)){
                         opp.Status__c = UtilityCls.PROJECT_STATUS_CLOSED;
                         opp.CloseDate = date.today(); //Added by MB - 06/05/18 - Bug 60753
                     }
        }
    }
    //End of Code - 10/16/17
    
    //Start of Code - MB - 10/25/17
    public static void updateAccountPipeline(Map<Id, Opportunity> newListMap, Map<Id, Opportunity> oldListMap){
        Set<Id> oppIds = new Set<Id>();
        //Set<Id> oppReOpenedIds = new Set<Id>();
        Set<Id> oppOldIds = new Set<Id>();
        Set<Id> accIds = new Set<Id>();
        Map<Id, String> oppStatusMap = new Map<Id, String>();
        Map<Id, List<Id>> oppAccMap = new Map<Id, List<Id>>();
        Map<Id, Set<Id>> accOppMap = new Map<Id, Set<Id>>();
        Map<Id, Account> accMap = new Map<Id, Account>();
        
        
        for(Opportunity opp: newListMap.values()){
            Opportunity oppOld = oldListMap.get(opp.Id);
            if(opp.RecordTypeId == commTraRtId){
                if( oppOld.Status__c != UtilityCls.PROJECT_STATUS_CLOSED && opp.Status__c == UtilityCls.PROJECT_STATUS_CLOSED) {
                    oppIds.add(opp.Id);
                }else if( oppOld.Status__c == UtilityCls.PROJECT_STATUS_CLOSED && opp.Status__c != UtilityCls.PROJECT_STATUS_CLOSED) {
                    oppIds.add(opp.Id);
                }
            }
        }
        
        if(oppIds.size()>0){
            for(Related_Account__c relAcc: [SELECT Id, Opportunity__c, Account__c FROM Related_Account__c WHERE Opportunity__c IN :oppIds]){
                if(relAcc.Account__c != null){
                    /*if(oppAccMap.containsKey(relAcc.Opportunity__c)){
oppAccMap.get(relAcc.Opportunity__c).add(relAcc.Account__c);
}else{
List<Id> accIdList = new List<Id>();
accIdList.add(relAcc.Account__c);
oppAccMap.put(relAcc.Opportunity__c, accIdList);
}*/
                    accIds.add(relAcc.Account__c);
                    
                    if(accOppMap.containsKey(relAcc.Account__c)){
                        accOppMap.get(relAcc.Account__c).add(relAcc.Opportunity__c);
                    }else{
                        Set<Id> oppIdSet = new Set<Id>();
                        oppIdSet.add(relAcc.Opportunity__c);
                        accOppMap.put(relAcc.Account__c, oppIdSet);
                    }
                    //accIds.add(relAcc.Account__c);
                }
            }
            System.debug('OppAccMapp: ' + oppAccMap);
            /*for(Account acc: [SELECT Id, Ed_Pipeline__c , Hc_Pipeline__c , Wp_Pipeline__c //Total_Pipeline__c
FROM Account
WHERE Id IN :accIds]){
accMap.put(acc.Id, acc);
}*/
            /*if(accIds.size()>0){
accMap = new Map<Id, Account>([SELECT Id, Ed_Pipeline__c , Hc_Pipeline__c , Wp_Pipeline__c //Total_Pipeline__c
FROM Account
WHERE Id IN :accIds]);
}*/
            if(accIds.size()>0){
                for(List<Account> accList: [SELECT Id, Ed_Pipeline__c , Hc_Pipeline__c , Wp_Pipeline__c //Total_Pipeline__c
                                            FROM Account
                                            WHERE Id IN :accIds]){
                                                List<Account> updAccountList = new List<Account>();
                                                for(Account acc: accList){
                                                    for(Id oppId: accOppMap.get(acc.Id)){
                                                        if(newListMap.get(oppId).Status__c != oldListMap.get(oppId).Status__c && 
                                                           newListMap.get(oppId).Status__c == UtilityCls.PROJECT_STATUS_CLOSED){
                                                               if(newListMap.get(oppId).Calculated_Revenue__c != null && oldListMap.get(oppId).Probability != null){
                                                                   Decimal appRev = newListMap.get(oppId).Calculated_Revenue__c * ( oldListMap.get(oppId).Probability / 100 );
                                                                   acc.Ed_Pipeline__c = (newListMap.get(oppId).Market_Segment_Group__c == UtilityCls.Edu_Govt && acc.Ed_Pipeline__c != null && acc.Ed_Pipeline__c>0) ? acc.Ed_Pipeline__c - appRev : acc.Ed_Pipeline__c;
                                                                   acc.Hc_Pipeline__c = (newListMap.get(oppId).Market_Segment_Group__c == UtilityCls.Healthcare && acc.Hc_Pipeline__c != null && acc.Hc_Pipeline__c>0) ? acc.Hc_Pipeline__c - appRev : acc.Hc_Pipeline__c;
                                                                   acc.Wp_Pipeline__c = (newListMap.get(oppId).Market_Segment_Group__c == UtilityCls.Work_Retail && acc.Wp_Pipeline__c != null && acc.Wp_Pipeline__c>0) ? acc.Wp_Pipeline__c - appRev : acc.Wp_Pipeline__c;
                                                               }
                                                           }/*else if(newListMap.get(oppId).Status__c != oldListMap.get(oppId).Status__c && 
                                                                    oldListMap.get(oppId).Status__c == UtilityCls.PROJECT_STATUS_CLOSED &&
                                                                    newListMap.get(oppId).Status__c != UtilityCls.PROJECT_STATUS_CLOSED){
                                                                        acc.Ed_Pipeline__c = (newListMap.get(oppId).Market_Segment_Group__c == UtilityCls.Edu_Govt && acc.Ed_Pipeline__c != null && acc.Ed_Pipeline__c>0) ? acc.Ed_Pipeline__c + newListMap.get(oppId).Applied_Revenue__c : acc.Ed_Pipeline__c;
                                                                        acc.Hc_Pipeline__c = (newListMap.get(oppId).Market_Segment_Group__c == UtilityCls.Healthcare && acc.Hc_Pipeline__c != null && acc.Hc_Pipeline__c>0) ? acc.Hc_Pipeline__c + newListMap.get(oppId).Applied_Revenue__c : acc.Hc_Pipeline__c;
                                                                        acc.Wp_Pipeline__c = (newListMap.get(oppId).Market_Segment_Group__c == UtilityCls.Work_Retail && acc.Wp_Pipeline__c != null && acc.Wp_Pipeline__c>0) ? acc.Wp_Pipeline__c + newListMap.get(oppId).Applied_Revenue__c : acc.Wp_Pipeline__c;
                                                                    } - Reopen will handle in RelatedAccountTriggerHandler*/
                                                    }
                                                    updAccountList.add(acc);
                                                }
                                                if(updAccountList.size()>0){
                                                    OppWrapperClass oppWrap= new OppWrapperClass();
                                                    oppWrap.updateAccPipeline(updAccountList);
                                                }
                                            }
            }
        }
        /* for(Opportunity opp: newList){
Account acc = new Account();
Opportunity oppOld = oldListMap.get(opp.Id);
if(opp.RecordTypeId == commTraRtId){
// If the Project is closed, deduct revenue of project in Account Total Pipeline
if(oppOld.Status__c != UtilityCls.PROJECT_STATUS_CLOSED && opp.Status__c == UtilityCls.PROJECT_STATUS_CLOSED){
List<Id> oppAccIds = oppAccMap.get(opp.Id);
if(oppAccIds != null && !oppAccIds.isEmpty()){
for(Id accId: oppAccIds){
if(accId != null){
acc = accMap.containsKey(accId) ? accMap.get(accId) : null;
if(acc != null){
if(opp.Market_Segment_Group__c == UtilityCls.Edu_Govt && acc.Ed_Pipeline__c != null && acc.Ed_Pipeline__c>0){
acc.Ed_Pipeline__c = acc.Ed_Pipeline__c - opp.Applied_Revenue__c;
}else if(opp.Market_Segment_Group__c == UtilityCls.Healthcare && acc.Ed_Pipeline__c != null && acc.Ed_Pipeline__c>0){
acc.Hc_Pipeline__c = acc.Hc_Pipeline__c - opp.Applied_Revenue__c;
}if(opp.Market_Segment_Group__c == UtilityCls.Edu_Govt && acc.Ed_Pipeline__c != null && acc.Ed_Pipeline__c>0){
acc.Ed_Pipeline__c = acc.Ed_Pipeline__c - opp.Applied_Revenue__c;
}
acc.Total_Pipeline__c = acc.Total_Pipeline__c - opp.Calculated_Revenue__c;
updAccountList.add(acc);
}
}
}
}
// If the Project is open again, add revenue of project in Account Total Pipeline
}else if(oppOld.Status__c == UtilityCls.PROJECT_STATUS_CLOSED && opp.Status__c != UtilityCls.PROJECT_STATUS_CLOSED){
for(List<Id> oppAccIds: oppAccMap.values()){
for(Id accId: oppAccIds){
if(accId != null){
acc = accMap.containsKey(accId) ? accMap.get(accId) : null;
if(acc != null){
acc.Total_Pipeline__c = acc.Total_Pipeline__c + opp.Calculated_Revenue__c;
updAccountList.add(acc);
}
}
}
}
}
}
}
System.debug('updAccountList: ' + updAccountList);
if(updAccountList.size()>0){
OppWrapperClass oppWrap= new OppWrapperClass();
oppWrap.updateAccPipeline(updAccountList);
/*try{
List<Database.SaveResult> results = Database.update(updAccountList, false);
for (Database.SaveResult result : results) {
if (result.isSuccess()) {
// Operation was successful, so get the ID of the record that was processed
System.debug('Successfully updated Account with Total Pipeline: ' + result.getId());
}
else {
// Operation failed, so get all errors                
for(Database.Error err : result.getErrors()) {
System.debug('Error while updating Account ' + err.getMessage());
}
}
} 
}
catch(Exception dmle){
System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
}*/
        // }
        // }
    }
    
    //End of Code - MB - 10/25/17
    //
    //Start of Code - MB - 10/27/17
    public without sharing class OppWrapperClass{
        public void updateAccPipeline(List<Account> acc){
            
            List<Database.SaveResult> results = Database.update(acc, false);
            UtilityCls.createDMLexceptionlog(acc,results,'OpportunityTriggerHandler.OppWrapperClass','OppWrapperClass.updateAccPipeline');
            
        }
    }
    //End of Code - MB - 10/27/17
}