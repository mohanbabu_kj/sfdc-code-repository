/**************************************************************************

Name : RelatedAccountTriggerHandler_Test

===========================================================================
Purpose : This class is used for Related Account Trigger actions
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         6/Feb/2017     Created 
2.0       Sasi Naik       3/mar/2017     Updated
***************************************************************************/
@isTest
private class RelatedAccountTriggerHandler_Test { 
    static List<Opportunity> oppList;
    static List<Account> accForNonInvoicingList; 
    static List<Account> accForNonInvoicingList2; 
    static List<Related_Account__c> relAccList; 
    static List<Related_Account__c> relAccList1; 
    static List<AccountTeamMember> accTeamMem;
    static user comluser;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        //Utility_Test.getDefaultConfCusSetting();
        Default_Configuration__c df = new Default_Configuration__c(Account_Team_Access__c = 'Read',
                                                                   Opportunity_Team_Access__c = 'Read',
                                                                   Residential_Invoicing_Roles__c ='Regional Vice President,Sales Vice President, District Manager',
                                                                   Residential_Non_Invoicing_Roles__c = 'Regional Vice President,Sales Vice President',
                                                                   SAL_Residential_Roles__c = 'District Manager,Regional Vice President,Senior Vice President',
                                                                   Groups__c = 'Strategic_Account_Managers;Admin;Commercial;Residential', // Added by MB - 07/10/17
                                                                   Fire_Account_Trigger__c = true,
                                                                   Fire_Lead_Trigger__c = true,
                                                                   Fire_MohawkAccountTeam_Trigger__c = true,
                                                                   Fire_Opportunity_Trigger__c = true,
                                                                   Residential_Sales_Ops__c = 'test',
                                                                   Commercial_Sales_Ops__c = 'test',
                                                                   Fire_RelatedAccount_Trigger__c = true,
                                                                   Fire_TerritoryUser_Trigger__c = true,
                                                                   Fire_Event_Trigger__c = true, // Added by MB - 07/10/17
                                                                   Opportunity_Team_Member_Trigger__c = true,
                                                                   Healthcare_Sr_Living__c = 'Healthcare;Hospitality;Mixed Use;Public Spaces;Senior Living',
                                                                   Workplace_Retail__c ='Workplace;Corporate;Faith Base;CRE/TI;Retail;CRE;TI',
                                                                   Education_Govt__c = 'Government;Higher Education;K12;Higher Ed');
        
        insert df;
        
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 1);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList[i].Business_Type__c = 'Commercial';
            }
            
            insert accForNonInvoicingList; 
            system.assert(accForNonInvoicingList.size()>0);
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            // accForNonInvoicingList2=Utility_Test.createMohawkAccountTeam();
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Business_Type__c = 'Commercial';
            }
            
            insert accForNonInvoicingList2;
            system.assert(accForNonInvoicingList2.size()>0);
            
            oppList = Utility_Test.createOpportunities(true, 1,accForNonInvoicingList);
            relAccList = Utility_Test.createRelatedAccountList(true, 1, accForNonInvoicingList2, oppList);
            try{
                List<contact> conList = Utility_Test.createContacts(true,1,accForNonInvoicingList2);
                system.debug('==contact'+ conList.get(0).Id);
                for(Related_Account__c ac : relAccList){
                    ac.Contact__c = conList.get(0).Id;
                }
                update relAccList;
                for(Related_Account__c ac : relAccList){
                    ac.Contact__c = null;  
                }
                update relAccList;
                
                
                
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            // delete relAccList;
            test.stopTest();  
        }
    }
    
    static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 1);
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList[i].Business_Type__c = 'Commercial';
            }
            insert accForNonInvoicingList;
            system.assert(accForNonInvoicingList.size()>0);
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Business_Type__c = 'Commercial';
            }
            
            insert accForNonInvoicingList2;
            system.assert(accForNonInvoicingList2.size()>0);
            
            oppList = Utility_Test.createOpportunities(true, 1,accForNonInvoicingList);
            relAccList = Utility_Test.createRelatedAccountList(true, 1, accForNonInvoicingList2, oppList);
            
            
            try {
                relAccList1 = Utility_Test.createRelatedAccountList(true, 1, accForNonInvoicingList2, oppList);
            } catch (Exception e) {
                
            }
            test.stopTest();  
        }
    }
    
    static testMethod void testMethod3() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 1);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                accForNonInvoicingList[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            
            insert accForNonInvoicingList; 
            system.assert(accForNonInvoicingList.size()>0);
            
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            List<Account> accList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            insert accList2;
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList2[i].Business_Type__c = UtilityCls.COMMERCIAL;
                
            }
            
            insert accForNonInvoicingList2;
            oppList = Utility_Test.createOpportunities(true, 1,accForNonInvoicingList);
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Senior Living';
            }
            update oppList;
            test.startTest();
            
            relAccList = Utility_Test.createRelatedAccountList(true, 1, accForNonInvoicingList2, oppList);
            system.assert(relAccList.size()>0);
            
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            List<contact> conList = Utility_Test.createContacts(true,1,accForNonInvoicingList2);
            system.debug('==contact'+ conList.get(0).Id);
            for(Related_Account__c ac : relAccList){
                ac.Contact__c = conList.get(0).Id;
            }
            update relAccList;
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Retail';
            }
            
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            
            delete relAccList;
            test.stopTest();  
        }
    } 
    static testMethod void newTestMethod() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 1);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                accForNonInvoicingList[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            
            insert accForNonInvoicingList; 
            system.assert(accForNonInvoicingList.size()>0);
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            List<Account> accList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            insert accList2;
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList2[i].Business_Type__c = UtilityCls.COMMERCIAL;
                
            }
            
            insert accForNonInvoicingList2;
            system.assert(accForNonInvoicingList2.size()>0);
            
            oppList = Utility_Test.createOpportunities(true, 1,accForNonInvoicingList);
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Senior Living';
                op.Amount  =110;
                op.Probability=2;
                op.Units__c =1;
            }
            update oppList;
            test.startTest();
            
            relAccList = Utility_Test.createRelatedAccountList(false, 1, accForNonInvoicingList2, oppList);
            relAccList[0].Project_Old_Market_Segment__c='Senior Living';
            relAccList[0].Project_Old_Calculated_Revenue__c=566;
            relAccList[0].RecordtypeID = UtilityCls.getRecordTypeInfo( 'Related_Account__c' , 'Commercial');
            insert relAccList;
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            List<contact> conList = Utility_Test.createContacts(true,1,accForNonInvoicingList2);
            system.debug('==contact'+ conList.get(0).Id);
            for(Related_Account__c ac : relAccList){
                ac.Contact__c = conList.get(0).Id;
            }
            update relAccList;
            system.assert(relAccList.size()>0);
            
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Retail';
            }
            
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            
            delete relAccList;
            test.stopTest();  
        }
    } 
    
    static testMethod void newTestMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 1);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                accForNonInvoicingList[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            
            insert accForNonInvoicingList; 
            system.assert(accForNonInvoicingList.size()>0);
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            List<Account> accList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            insert accList2;
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList2[i].Business_Type__c = UtilityCls.COMMERCIAL;
                
            }
            
            insert accForNonInvoicingList2;
            oppList = Utility_Test.createOpportunities(true, 1,accForNonInvoicingList);
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Workplace';
                op.Amount  =110;
                op.Probability=2;
                op.Units__c =1;
            }
            update oppList;
            test.startTest();
            
            relAccList = Utility_Test.createRelatedAccountList(false, 1, accForNonInvoicingList2, oppList);
            relAccList[0].Project_Old_Market_Segment__c='Workplace';
            relAccList[0].Project_Old_Calculated_Revenue__c=566;
            relAccList[0].RecordtypeID = UtilityCls.getRecordTypeInfo( 'Related_Account__c' , 'Commercial');
            insert relAccList;
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            List<contact> conList = Utility_Test.createContacts(true,1,accForNonInvoicingList2);
            system.debug('==contact'+ conList.get(0).Id);
            for(Related_Account__c ac : relAccList){
                ac.Contact__c = conList.get(0).Id;
            }
            update relAccList;
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Retail';
            }
            
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            system.assert(relAccList.size()>0);
            
            delete relAccList;
            test.stopTest();  
        }
    } 
    static testMethod void newTestMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 1);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                accForNonInvoicingList[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            
            insert accForNonInvoicingList; 
            system.assert(accForNonInvoicingList.size()>0);
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            List<Account> accList2 = Utility_Test.createAccountsForNonInvoicing(false, 1);
            insert accList2;
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList2[i].Business_Type__c = UtilityCls.COMMERCIAL;
                
            }
            
            insert accForNonInvoicingList2;
            system.assert(accForNonInvoicingList2.size()>0);
            
            oppList = Utility_Test.createOpportunities(true, 1,accForNonInvoicingList);
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Higher Ed';
                op.Amount  =110;
                op.Probability=2;
                op.Units__c =1;
            }
            update oppList;
            test.startTest();
            
            relAccList = Utility_Test.createRelatedAccountList(false, 1, accForNonInvoicingList2, oppList);
            relAccList[0].Project_Old_Market_Segment__c='Higher Education';
            relAccList[0].Project_Old_Calculated_Revenue__c=566;
            relAccList[0].RecordtypeID = UtilityCls.getRecordTypeInfo( 'Related_Account__c' , 'Commercial');
            
            insert relAccList;
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            List<contact> conList = Utility_Test.createContacts(true,1,accForNonInvoicingList2);
            system.debug('==contact'+ conList.get(0).Id);
            for(Related_Account__c ac : relAccList){
                ac.Contact__c = conList.get(0).Id;
            }
            update relAccList;
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Retail';
            }
            
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            
            delete relAccList;
            test.stopTest();  
        }
    } 
    
    static testMethod void testMethod5() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            Utility_Test.ADMIN_USER.Business_Type__c = 'Commercial';
            update Utility_Test.ADMIN_USER;
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 2);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList[i].Strategic_Account__c =  true;
                accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                
                
            }
            
            insert accForNonInvoicingList; 
            system.assert(accForNonInvoicingList.size()>0);
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 2);
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                //accForNonInvoicingList2[i].Project_Role__c =  'Strategic Account';
                // accForNonInvoicingList2[i].ParentId =  accForNonInvoicingList[0].Id;
                accForNonInvoicingList2[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            
            insert accForNonInvoicingList2;
            oppList = Utility_Test.createOpportunities(true, 2,accForNonInvoicingList);
            List<OpportunityTeamMember>  oppTeamMemList =Utility_Test.createOppTeamMember(false,2,oppList,Utility_test.ADMIN_USER);
            oppTeamMemList[0].Team_Role_Commercial__c = 'Account Executive';
            oppTeamMemList[1].Team_Role_Commercial__c = 'Account Executive';
            
            oppTeamMemList[0].TeamMemberRole = 'Account Executive';
            oppTeamMemList[1].TeamMemberRole = 'Account Executive';
            insert oppTeamMemList;
            
            try{
                
                List<contact> conList = Utility_Test.createContacts(true,2,accForNonInvoicingList2);
                
                OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId=oppList[0].Id, ContactId=conList.get(0).Id);
                insert ocr;              
                
                accForNonInvoicingList2[1].Strategic_Account__c = true;
                
                
                accForNonInvoicingList2[0].parentId=accForNonInvoicingList2[1].Id;
                update accForNonInvoicingList2;
                Related_Account__c rl = new Related_Account__c(Account__c = accForNonInvoicingList2[0].Id,
                                                               Opportunity__c =  oppList[0].Id,
                                                               Project_Role__c = 'End User' , Contact__c = conList.get(0).Id);
                Related_Account__c rl1 = new Related_Account__c(Account__c = accForNonInvoicingList2[1].Id,
                                                                Opportunity__c =  oppList[0].Id,
                                                                Project_Role__c = 'End User' , Contact__c = conList.get(0).Id);
                List<Related_Account__c> rlList = new List<Related_Account__c>();
                rlList.add(rl);
                rlList.add(rl1);
                insert rlList; 
                
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            // delete relAccList;
            test.stopTest();  
        }
    }
    static testMethod void testMethod6(){
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            List<Account> acList = Utility_Test.createAccounts(true, 3);
            
            
        }
        
        
        
        //  insert accForNonInvoicingList2;
        
        // List<Account> acList = createAccounts(true, 3);
        
        
        
        
        
        
        
        // static testMethod void test() {
        // public class OppTeamMember {
        
        
        // public static testMethod void test() {
        
        
        // RelatedAccountTriggerHandler_Test.OppTeamMember C = new RelatedAccountTriggerHandler_Test.OppTeamMember();
        
    }
    
    
    static testMethod void testMethod7() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            list<Account> parentAccs0 = Utility_Test.createAccountsForNonInvoicing(true, 2);
            
            list<Account> parentAccs = Utility_Test.createAccountsForNonInvoicing(false, 2);
            for(Integer i=0;i<parentAccs.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                parentAccs[i].Strategic_Account__c = true;
                parentAccs[i].parentId = parentAccs0[i].id;
                
            }
            
            insert parentAccs; 
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 2);
            system.assert(accForNonInvoicingList.size()>0);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                accForNonInvoicingList[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            
            insert accForNonInvoicingList; 
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 2);
            List<Account> accList2 = Utility_Test.createAccountsForNonInvoicing(false, 2);
            insert accList2;
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList2[i].Business_Type__c = UtilityCls.COMMERCIAL;
                accForNonInvoicingList2[i].parentId = parentAccs[i].id;
                
                
            }
            
            insert accForNonInvoicingList2;
            oppList = Utility_Test.createOpportunities(true, 2,accForNonInvoicingList);
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Senior Living';
            }
            update oppList;
            test.startTest();
            
            relAccList = Utility_Test.createRelatedAccountList(false, 2, accForNonInvoicingList2, oppList);
            List<contact> conList = Utility_Test.createContacts(true,2,accForNonInvoicingList2);
            system.debug('==contact'+ conList.get(0).Id);
            integer i=0;
            for(Related_Account__c ac : relAccList){
                ac.Contact__c = conList[i].Id;
                ac.Project_Role__c='Strategic Account';
                i++;
            }
            
            try{
                
                insert relAccList;
            }
            catch(exception e){
                system.debug('exception is >>>>>>'+e);
            }
            
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Retail';
            }
            
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            
            List<Territory__c> terrList= Utility_Test.createTerritories(true,2);
            
            User u=new User(Id=userinfo.getuserid());
            try{
                List<Mohawk_Account_Team__c> mohawkAccTeam=Utility_Test.createMohawkAccountTeam(true,2,accForNonInvoicingList2,terrList,u);
            }
            catch(exception e){
                system.debug('error is'+e);
            }
            
            //  delete relAccList;
            test.stopTest();  
        }
    } 
    static testMethod void testMethod8() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            list<Account> parentAccs0 = Utility_Test.createAccountsForNonInvoicing(true, 2);
            
            list<Account> parentAccs = Utility_Test.createAccountsForNonInvoicing(false, 2);
            for(Integer i=0;i<parentAccs.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                parentAccs[i].Strategic_Account__c = true;
                parentAccs[i].parentId = parentAccs0[i].id;
                
            }
            
            insert parentAccs; 
            comluser = Utility_Test.getTestUser('Test Name','Commercial Sales User');
            accForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 2);
            system.assert(accForNonInvoicingList.size()>0);
            
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                //accForNonInvoicingList[i].Business_Type__c = 'Commercial';
                accForNonInvoicingList[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            
            insert accForNonInvoicingList; 
            
            accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 2);
            List<Account> accList2 = Utility_Test.createAccountsForNonInvoicing(false, 2);
            insert accList2;
            for(Integer i=0;i<accForNonInvoicingList.size();i++){
                accForNonInvoicingList2[i].Name = 'Teat comm RelAc1';
                accForNonInvoicingList2[i].Role_C__c = UtilityCls.ENDUSER;
                accForNonInvoicingList2[i].Business_Type__c = UtilityCls.COMMERCIAL;
                accForNonInvoicingList2[i].parentId = parentAccs[i].id;
                
                
            }
            
            insert accForNonInvoicingList2;
            oppList = Utility_Test.createOpportunities(true, 2,accForNonInvoicingList);
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Faith Based';
            }
            update oppList;
            test.startTest();
            
            relAccList = Utility_Test.createRelatedAccountList(false, 2, accForNonInvoicingList2, oppList);
            List<contact> conList = Utility_Test.createContacts(true,2,accForNonInvoicingList2);
            system.debug('==contact'+ conList.get(0).Id);
            integer i=0;
            for(Related_Account__c ac : relAccList){
                ac.Contact__c = conList[i].Id;
                ac.Project_Role__c='Strategic Account';
                i++;
            }
            
            try{
                
                insert relAccList;
            }
            catch(exception e){
                system.debug('exception is >>>>>>'+e);
            }
            
            
            
            try{
                
                update relAccList;
            }
            catch(exception e){
                system.debug('exception is'+e);
            }
            
            List<Territory__c> terrList= Utility_Test.createTerritories(true,2);
            
            User u=new User(Id=userinfo.getuserid());
            try{
                List<Mohawk_Account_Team__c> mohawkAccTeam=Utility_Test.createMohawkAccountTeam(true,2,accForNonInvoicingList2,terrList,u);
            }
            catch(exception e){
                system.debug('error is'+e);
            }
            
            for(Opportunity op : oppList){
                op.Market_Segment__c = 'Government';
            }
            update oppList;
            
            
            //  delete relAccList;
            test.stopTest();  
        }
    } 
    
}