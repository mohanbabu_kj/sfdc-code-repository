public class genericRelatedListHandler {

    private static final String Icon_EXT = '.svg';
    
    @AuraEnabled
    public static List<relNameWrapper> getRelatedListNames( Id recordId ){
        List<relNameWrapper> contentList = new List<relNameWrapper>();
        List<relNameWrapper> relatedListRecs = new List<relNameWrapper>();
        String svgIconPath ='';
        Schema.SObjectType objType = recordId.getSobjectType();
        String objName = objType.getDescribe().getName();
        system.debug('objectname:::'+ objName);
        Map<String,Set<String>> mapCMD = new Map<String, Set<String>>();
        for(Generic_Related_List__mdt cmdRec : 
            [SELECT DeveloperName,MasterLabel, Related_List_Names__c FROM Generic_Related_List__mdt]) {
            	Set<String> relSet = new Set<String>();
                if(String.IsNotBlank(cmdRec.Related_List_Names__c))
                {
                    list<String> tempList = cmdRec.Related_List_Names__c.split(',');
                    relSet.addAll(tempList);
                }
                mapCMD.put(cmdRec.MasterLabel, relSet);
           
        }

        system.debug('mapkey$$$'+mapCMD.keyset());
        system.debug('mapvalue***'+mapCMD.values());
        DescribeSObjectResult describe = objType.getDescribe();
		for (ChildRelationship relation : describe.getChildRelationships())
		{
		    system.debug(relation.getRelationshipName());
		    String chilObjPluralName = relation.getChildSObject().getDescribe().getName();
            system.debug(chilObjPluralName);
			if(mapCMD!=null && mapCMD.get(objName)!= null && 
               mapCMD.get(objName).contains(chilObjPluralName))
                
				{
                    String substring='';
                    String sprite1 = '';
                    String sprite2Full = '';
                    String sprite2 = '';
                    String chilObjName = relation.getChildSObject().getDescribe().getName();
                    svgIconPath =  getIconName(chilObjName,objName);
                    if(svgIconPath!=null)
                    {
                        substring = svgIconPath.substringAfterLast('t4v35/');
                        system.debug('substring'+substring);
                        sprite1=substring.split('/')[0];
                        system.debug('sprite1'+sprite1);
                        sprite2Full=substring.split('/')[1];
                        system.debug('sprite2Full'+sprite2Full);
                            Pattern pat = Pattern.compile('([0-9]+)');
                            Matcher matcher = pat.matcher(sprite2Full);
                            Boolean matches = matcher.find();
                            sprite2 = matcher.group(1);
                            system.debug(logginglevel.error,matches);
                            system.debug(logginglevel.error,matcher.group(1));
                    }
					system.debug('$$$'+relation.getChildSObject().getDescribe().getName()+'666'
                         +relation.getChildSObject().getDescribe().getLabelPlural());
                    system.debug('svgIconPath'+svgIconPath);
		    		contentList.add(new relNameWrapper(objName,chilObjName,
                                               relation.getChildSObject().getDescribe().getLabelPlural(),
                                               relation.getRelationshipName(),
                                               relation.getField().getDescribe().getName(),0,svgIconPath,sprite1,sprite2));
		    }
		}
		relatedListRecs = getRecordCount(contentList, recordId);
        
        return relatedListRecs;
    }
    
    public static List<relNameWrapper> getRecordCount(List<relNameWrapper> dataList,Id recordId){
    	List<relNameWrapper> completeDataList = new List<relNameWrapper>();
        if(dataList!=null && dataList.size()>0)
        {
            for(integer i=0;i<dataList.size();i++)
            {
            	integer  count;
                if(dataList[i].relationshipName=='AttachedContentNotes')
                {
                    List<ContentDocumentLink> notsList = [SELECT ContentDocumentId FROM ContentDocumentLink
                                                          WHERE LinkedEntityId =  :recordId ];
                    if(notsList!=null && notsList.size()>0)
                        count = notsList.size();
                }
                else
                { 
                    count= database.countQuery('select count() from '+dataList[i].childObjName+' where '+
                                                dataList[i].fieldName+' = \''+ recordId +'\'' );
                }
                completeDataList.add(new relNameWrapper(dataList[i].parentObjName,dataList[i].childObjName,
                                            dataList[i].relatedListLabel,dataList[i].relationshipName,
                                                        dataList[i].fieldName,count,dataList[i].iconPath,
                                                       dataList[i].iconSprite1,dataList[i].iconSprite2));
            }
        }
        return completeDataList;
    
    }
    
    @AuraEnabled
    public static String getIconName(String sObjectName, String parentObjName){
        String u;
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();

        for(Schema.DescribeTabSetResult tsr : tabSetDesc) 
        { 
            tabDesc.addAll(tsr.getTabs()); 
        }

        for(Schema.DescribeTabResult tr : tabDesc) {
            system.debug('getSobjectName###'+tr.getSobjectName());
            system.debug('sObjectName###'+sObjectName);
            Boolean check = false;
            if( sObjectName == tr.getSobjectName() ) {
                for(Schema.DescribeIconResult iconResult: tr.getIcons()){
                    String iconUrl = iconResult.getUrl();
                    if(iconUrl.endsWith(Icon_EXT)){
                        u=iconUrl;
                        check=true;
                        return u;
                    }
                }
            }
            else if(!check && parentObjName == tr.getSobjectName() )
            {
                system.debug('parentObjName###'+tr.isCustom());
                for(Schema.DescribeIconResult iconResult: tr.getIcons()){
                    String iconUrl = iconResult.getUrl();
                    if(iconUrl.endsWith(Icon_EXT)){
                        u=iconUrl;
                        return u;
                    }
                }
            }
            /*if( sObjectName == tr.getSobjectName() ) {
                system.debug('tr.isCustom()###'+tr.isCustom());
                if( tr.isCustom() == true ) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    u = 'standard:' + sObjectName.toLowerCase();
                }
            }
            else if(parentObjName == tr.getSobjectName() )
            {
                system.debug('parentObjName###'+tr.isCustom());
                if( tr.isCustom() == true ) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    u = 'standard:' + sObjectName.toLowerCase();
                }
            }*/
        }
        /*for (Schema.DescribeIconResult ir : iconDesc) {
            if (ir.getContentType() == 'image/svg+xml'){
                u = 'custom:' + ir.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                break;
            }
        }*/
        system.debug('iconpath###'+u);
        return u;
    }


    public class relNameWrapper
    {
    	
        @AuraEnabled
    	public  String parentObjName;
        @AuraEnabled
    	public  String childObjName;
        @AuraEnabled
    	public  String relatedListLabel;
    	@AuraEnabled
    	public  String relationshipName;
        @AuraEnabled
    	public  String fieldName;
        @AuraEnabled
    	public  Integer recordCount;
        @AuraEnabled
    	public  String iconPath;
        @AuraEnabled
    	public  String iconSprite1;
        @AuraEnabled
    	public  String iconSprite2;

    	public relNameWrapper(String pObjName, String cObjName,String relLabel , String relAPIName,
                              String fName, Integer count,String svgIcon, String iconS1, String iconS2)
    	{
    		this.parentObjName = pObjName;
    		this.childObjName = cObjName;
            this.relationshipName = relAPIName;
    		this.relatedListLabel = relLabel;
            this.fieldName = fName;
            this.recordCount = count;
            this.iconPath = svgIcon;
            this.iconSprite1 = iconS1;
            this.iconSprite2 = iconS2;
    	}

    }
}