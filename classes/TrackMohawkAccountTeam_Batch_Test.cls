@isTest
public class TrackMohawkAccountTeam_Batch_Test {
	
    static List<Account> resAccList = new List<Account>();
    static List<Mohawk_Account_Team__c> mhkTeamList = new List<Mohawk_Account_Team__c>();
    static List<User> userList = new List<User>();
    static Id recordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId();
    
    @isTest
    public static void init(){
        AP_Trigger_Settings__c aPSettings =  new AP_Trigger_Settings__c(Name = 'Two Team Account', Exclude_Chain_Numbers_1__c = '389829', 
                                                                        Exclude_Chain_Numbers_2__c = '488493');
        insert aPSettings;
        
        userList = Utility_Test.createTestUsers(true, 2, 'Residential Sales User');
        
        resAccList = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
        for( Integer i=0; i < resAccList.size(); i++ ){
            resAccList[i].Business_Type__c = 'Residential';
            resAccList[i].Customer_Group__c = 'Open Line (OL)';
        }
        insert resAccList;
        
        Mohawk_Account_Team__c mhkTeam = new Mohawk_Account_Team__c(Account__c = resAccList[0].Id, 
                                                                    User__c = userList[0].Id, 
                                                                    RecordTypeId = recordTypeId,
                                                                    Role__c = 'Territory Manager',
                                                                    Source_CAMS__c = true,
                                                                    Brands__c = 'Aladdin',
                                                                    Product_Types__c = 'Carpet');
        
		mhkTeamList.add(mhkTeam);
        mhkTeam = new Mohawk_Account_Team__c(Account__c = resAccList[0].Id, 
                                             User__c = userList[1].Id, 
                                             RecordTypeId = recordTypeId,
                                             Account_Access__c = 'Edit',
                                             Role__c = 'Territory Manager',
                                             Source_CAMS__c = true,
                                             Brands__c = 'Karastan',
                                             Product_Types__c = 'Cushion');
        mhkTeamList.add(mhkTeam);
        insert mhkTeamList;
        
    }
    
    @isTest
    static void methodForBatchRun() {
        System.runAs( Utility_Test.ADMIN_USER ) {            
            init();
            Delete [SELECT Id from AccountTeamMember];
            TrackMohawkAccountTeam_Batch  cls = new TrackMohawkAccountTeam_Batch();
            database.executeBatch(cls,200);
            
            String sch = '0 0 23 * * ?';
            String jobId = System.schedule('test TrackMohawkAccountTeam_Batch',sch, new TrackMohawkAccountTeam_Schedule());
            system.assert( jobId != null );
        }
    }
    
    @isTest
    static void methodForBatchRunDelete() {
        System.runAs( Utility_Test.ADMIN_USER ) {            
            init();
            Delete [SELECT Id from Mohawk_Account_Team__c];
            List<AccountTeamMember> accTeamList = new List<AccountTeamMember>();
            AccountTeamMember accTeam = new AccountTeamMember(AccountId = resAccList[0].Id, 
                                                              UserId = userList[0].Id, 
                                                              TeamMemberRole = 'Territory Manager');
            
            accTeamList.add(accTeam);
            Insert accTeamList;
            TrackMohawkAccountTeam_Batch  cls = new TrackMohawkAccountTeam_Batch();
            database.executeBatch(cls,200);
            
            String sch = '0 0 23 * * ?';
            String jobId = System.schedule('test TrackMohawkAccountTeam_Batch',sch, new TrackMohawkAccountTeam_Schedule());
            system.assert( jobId != null );
        }
    }
}