@isTest 
public class OpportunityTeamMemberTrigger_Test {
      static testMethod void testforDeleteOppShareAccess() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            Utility_Test.getDefaultConfCusSetting();
            List<Account> accList = Utility_Test.createAccountsForNonInvoicing(true, 1);
            
            
            List<Opportunity> oppList = Utility_Test.createOpportunities(true, 1, accList);
            List<User> commUser = Utility_Test.createTestUsers(true, 1, 'Commercial Sales User');
            System.debug('User: ' + commUser);
            List<Territory__c> terrList = Utility_Test.createTerritories(true, 1);
            List<Territory_User__c> terrUserList = Utility_Test.createTerritoryWithUsers(true, terrList, commUser[0], 1);
           Territory_User__c terUr=new Territory_User__c(user__c= commUser[0].id,Role__c = Utilitycls.AE,Territory__c=terrList[0].id);
            insert terUr;
            List<Group> grpList = Utility_Test.createGroup(true, 1, 'Test Territory');
            Boolean result = UserSearch_CC.AddProjectProductTeammembers(oppList[0].Id, commUser[0].Id, 'Account Executive', terrList[0].Id);
            List<OpportunityTeamMember> oppTeamMemList = [SELECT Id, OpportunityId, Territory__c, UserId from OpportunityTeamMember Where OpportunityId =: oppList[0].Id
                                                            AND UserId != :Utility_Test.ADMIN_USER.Id];
            OpportunityTeamMemberTriggerHandler.DeleteOppShareAccess(oppTeamMemList);
            OpportunityTeamMemberTriggerHandler.updateOppTeam(oppTeamMemList);
            for(OpportunityTeamMember otm:oppTeamMemList){
                otm.External_ID__c=null;
            }
            OpportunityTeamMemberTriggerHandler.updateOppDetails(oppTeamMemList,True);
           Update oppTeamMemList;
           Delete oppTeamMemList;
            test.stopTest();
         }
    }
    
    
}