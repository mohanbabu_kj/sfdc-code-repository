/**************************************************************************

Name : NonInvoiceGLoablAccNumber_Batch_Test

===========================================================================
Purpose : This tess class is used for NonInvoiceGLoablAccNumber_Batch
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        krishnapriya   7/Nov/2017     Modified       Added testforAutoNumberGenerator4    
***************************************************************************/
@isTest
private class NonInvoiceGLoablAccNumber_Batch_Test {
    static testMethod void testforAutoNumberGenerator1() {
       System.runAs(Utility_Test.ADMIN_USER) {
                 

        test.startTest();
           List<Account> accListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
           for (Account acc : accListForInvoicing){
            acc.Business_Type__c = 'Commercial';
            acc.RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Non-Invoicing');
           }
           insert accListForInvoicing;    
            
            Test.setMock(HttpCalloutMock.class, new NonInvoiceGLoablAccNumberMock_Test());     
            NonInvoiceGLoablAccNumber_Batch cls = new NonInvoiceGLoablAccNumber_Batch();
            string jobId = database.executeBatch(cls,10);
           System.assert(jobId != null);

            AccountTriggerHandler.CollecttheAccountids(accListForInvoicing);
        test.stopTest();
       }
       
    }

    static testMethod void testforAutoNumberGenerator2() {
        System.runAs(Utility_Test.ADMIN_USER) {     

        test.startTest();
           Test.setMock(HttpCalloutMock.class, new NonInvoiceGLoablAccNumberMock_Test()); 
           List<Account> accListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
           for(Account acc : accListForInvoicing){
               acc.Business_Type__c = 'Residential';
             acc.RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Non-Invoicing');
             acc.Global_Account_Number__c = null;
           }
           insert accListForInvoicing; 

           Set<Id> acIds = new Set<Id>();
            for(Account acc : accListForInvoicing){
                acIds.add(acc.Id);
            }
           //accListForInvoicing[0].Global_Account_Number__c = null;   

           
           AccNonInvoicingNogenerator.SendHttpsreq(acIds);  

           NonInvoiceGLoablAccNumber_Batch cls = new NonInvoiceGLoablAccNumber_Batch();
           string jobId = database.executeBatch(cls,10);
           System.assert(jobId != null);
           AccountTriggerHandler.CollecttheAccountids(accListForInvoicing);
        test.stopTest();
        }      
    }
    static testMethod void testforAutoNumberGenerator3() {
        System.runAs(Utility_Test.ADMIN_USER) {    
           Utility_Test.getTeamCreationCusSetting();
            List<Account> accListForNonInvoicing = Utility_Test.createAccountsForNonInvoicing(false, 1);
            for(Account acc : accListForNonInvoicing) {
                acc.Business_Type__c = 'Commercial';
               acc.Global_Account_Number__c = null;
                acc.ShippingCity = '';
            }
            insert accListForNonInvoicing;

            Set<Id> acIds = new Set<Id>();
            for(Account acc : accListForNonInvoicing){
              acIds.add(acc.Id);
            }
            Test.setMock(HttpCalloutMock.class, new NonInvoiceGLoablAccNumberMock_Test());   
          
            test.startTest();
            AccNonInvoicingNogenerator.SendHttpsreq(acIds);              
            NonInvoiceGLoablAccNumber_Batch cls = new NonInvoiceGLoablAccNumber_Batch();
            string jobId = database.executeBatch(cls);
            System.assert(jobId != null);
            test.stopTest();
      }
    }
    
    static testMethod void testforAutoNumberGenerator4() {
        test.startTest();
        System.runAs(Utility_Test.ADMIN_USER) { 
            Id NonInvoiceAccRT=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-Invoicing').getRecordTypeId();
            Account a=new Account(name='test',RecordTypeId=NonInvoiceAccRT);
            insert a;
            
            NonInvoiceGLoablAccNumber_Batch cls = new NonInvoiceGLoablAccNumber_Batch();
            string jobId = database.executeBatch(cls);
            System.assert(jobId != null);
            NonInvoiceGLoablAccNumber_Batch sched = new NonInvoiceGLoablAccNumber_Batch();
            string sch ='0 0 23 * * ?';
            system.schedule('Teststatuscheck',sch,sched); 
         test.stopTest();
      }
    }
    static testMethod void testforAutoNumberGeneratorAccountNonInvoice(){
         test.startTest();
        System.runAs(Utility_Test.ADMIN_USER) { 
            Test.setMock(HttpCalloutMock.class, new NonInvoiceGLoablAccNumberMock_Test()); 
            List<Account> accListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account acc : accListForInvoicing){
                acc.Business_Type__c = 'Residential';
                acc.RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Non-Invoicing');
                acc.Global_Account_Number__c = null;
           }
          insert accListForInvoicing; 

           Set<Id> acIds = new Set<Id>();
           for(Account acc : accListForInvoicing){
                acIds.add(acc.Id);
           }
           //accListForInvoicing[0].Global_Account_Number__c = null;   

           
           AccNonInvoicingNogenerator.SendHttpsreq(acIds);  

           NonInvoiceGLoablAccNumber_Batch cls = new NonInvoiceGLoablAccNumber_Batch();
           string jobId = database.executeBatch(cls,10);
           System.assert(jobId != null);
           AccountTriggerHandler.CollecttheAccountids(accListForInvoicing);
        }
         test.stopTest();
    }
}