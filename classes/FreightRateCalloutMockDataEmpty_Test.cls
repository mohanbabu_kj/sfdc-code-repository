/**************************************************************************

Name : FreightRateCalloutMockDataEmpty_Test 

===========================================================================
Purpose :  the JSON format for OFREIGHTDATA is empty
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        14/Feb/2017     Created 
***************************************************************************/
@isTest
global class FreightRateCalloutMockDataEmpty_Test implements HttpCalloutMock  {

     global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody(+'{'
                          +'"MAIN": {'
                                +'"IPACCOUNT": "R.102318.0000",'
                                +'"OACCOUNTNAME": "Hockstein Inc.",'
                                +'"ODATETIME": "*February 14 - 2017, 04:45 AM EST*",'
                                +'"OMESSAGE": null,'
                                +'"OFREIGHTDATA": ['
                                +']'
                            +'}'
                        +'}');

           return res;
      }//end of Method
}//End of Class