/**************************************************************************

Name : AccountProfileShare_Batch_Test 
===========================================================================
Purpose : Uitlity class to AccountProfileShare_Batch test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit Saxena   31/OCT/2018    Create  
***************************************************************************/
@isTest
private class AccountProfileShare_BatchV1_Test {
    
    static List<Account> resAccListForInvoicing;
    
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();            
            
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Retail');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c = aplist[0].id;
                a.Business_Type__c = 'Residential';
                a.Type = 'Invoicing';
            }
            insert resAccListForInvoicing;
            
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
            
            Group grp = new Group();
            grp.name = 'Test Group1';
            grp.Type = 'Regular'; 
            Insert grp; 
            
            AccountShare ash = new AccountShare();
            ash.AccountId = resAccListForInvoicing[0].id;
            ash.UserOrGroupId = grp.Id;
            ash.RowCause = 'Manual';
            ash.AccountAccessLevel = 'Edit';
            ash.ContactAccessLevel = 'Edit';
            ash.OpportunityAccessLevel = 'Read';
            insert ash;
            
            AccountProfileShare_BatchV1  cls = new AccountProfileShare_BatchV1 ();
            String jobIdShare = Database.executeBatch(cls);
            
            String sch = '0 0 23 * * ?';
            system.schedule('Test SCH Check', sch, cls); 
            
            test.stopTest();
            
        }
        
    }
}