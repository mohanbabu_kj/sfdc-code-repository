/**************************************************************************************

Name : InsertClaimApproversResponse 

=======================================================================================
Purpose :Class to generate Response JSON for Rest Service class- InsertClaimApprovers
=======================================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          
 1.0           Susmitha       16/Nov/2017    Created        

***************************************************************************************/

global  class InsertClaimApproversResponse {

global class SuccessResponse{
        public Integer ClaimApprovalsInserted;
        public Integer ExistingClaims;
        public Integer ClaimsInserted;
        public Integer ExistingClaimApprovers;
        public Integer DeletedClaimApprovers;

        public SuccessResponse(Integer ClaimsInserted, Integer ClaimApprovalsInserted, Integer ExistingClaims, Integer ExistingClaimApprovers,Integer DeletedClaimApprovers) {
            this.ClaimApprovalsInserted = ClaimApprovalsInserted;
            this.ClaimsInserted = ClaimsInserted;
            this.ExistingClaims=ExistingClaims;
            this.ExistingClaimApprovers=ExistingClaimApprovers;
            this.DeletedClaimApprovers=DeletedClaimApprovers;

            
        }

}

global class FailResponse{
        public String ApproverInsertFailReason;
        public String ClaimInsertFailReason;
        public Integer ClaimApprovalsFailed;

        public Integer ClaimsFailed;

        public FailResponse(Integer ClaimsFailed,Integer ClaimApprovalsFailed,String ClaimInsertFailReason,String ApproverInsertFailReason){
        this.ApproverInsertFailReason=ApproverInsertFailReason;
        this.ClaimInsertFailReason=ClaimInsertFailReason;
        this.ClaimApprovalsFailed=ClaimApprovalsFailed;
        this.ClaimsFailed=ClaimsFailed;
        }

}
        
        
        
        
    
}