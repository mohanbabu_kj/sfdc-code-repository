/**************************************************************************

Name : InsertClaimApprovers_Rest

===========================================================================
Purpose :Rest Service to insert Claims and related Claim Approvers
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          SAMPLE JSON
      1.0       Susmitha       10/Nov/2017    Created         {
                                                            "claims": [{
                                                            "shipToGlobalID" : "0000193180",
                                                            "claimNumber" : "3.8",
                                                            "claimTypeCode" : "FREIGHT",
                                                            "claimSubmitDate" : "02/02/2017",
                                                            "claimAmount" : "100",
                                                            "claimStatus" : "RESOLVED",
                                                            "Company"     : "c",
                                                            "salesUsers": [
                                                            {
                                                            "emailID": "test6@gmail.com",
                                                            "employeeID": "23604"
                                                            }
                                                            ]
                                                            
                                                            }]
                                                            }

***************************************************************************/
@RestResource(urlMapping = '/ClaimApprover/*')
global with sharing class InsertClaimApprovers_Rest {

    global class SalesUsers {
        global String emailID;
        global String employeeID;
    }

    global class Claims {
        global String shipToGlobalID;
        global String claimNumber;
        global String claimTypeCode;
        global String claimSubmitDate;
        global String claimAmount;
        global String claimStatus;
        global String Company;
        global SalesUsers[] salesUsers;
    }


    @HttpPost
    global static String postClaimApprover(Claims[] claims) {

    
    /*------------------------Variables used for Displaying Response ----------------------------------*/
        String ExceptionMsg = '';
        Integer ExistingClaims = 0, ExistingClaimApprovers = 0;
        String claimFailReason = '', ApproverFailReason = '';
        Integer ClaimSuccess = 0, ClaimFail = 0, ApproverSuccess = 0, ApproverFail = 0;
        List < Database.Saveresult > ClaimsToInsert = new List < Database.Saveresult > ();
        List < Database.Saveresult > ClaimApproversToInsert = new List < Database.Saveresult > ();
        String JSONresponse = '';
        Integer DeletedClaimApprovers=0;

    


   /*--------------Record Type Ids used for Mapping based on the letter on Company -----------------------------------------*/
        Id CommercialRT = Schema.SObjectType.Claim__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();
        Id ResidentialRT = Schema.SObjectType.Claim__c.getRecordTypeInfosByName().get('Residential').getRecordTypeId();
        
    /*--------------Other Variables ---------------------------------------------------------------------------------------*/
        Map < string, Claim_Approvers__c > existingApproversinTheListMap = new Map < string, Claim_Approvers__c > ();
        List < claim__c > ExistingClaimsList = new List < claim__c > ();
        List < Claim_Approvers__c > ExistingApproversList = new List < Claim_Approvers__c > ();
        
        List < claim__c > insertClaimList = new List < claim__c > ();
        List < Claim_Approvers__c > InsertApproversList = new List < Claim_Approvers__c > ();
        Map < String, ID > claimMap = new Map < String, ID > ();  
        
        
        Map < string, User > userMap = new Map < string, User > ();
        Map < string, claims > duplicateCheckMap = new Map < string, claims > (); 
        set < string > employeeIdSet = new set < string > ();

        Set < String > GlobalAccountNum = new set < String > ();
        Map < String, Account > AccountMap = new Map < String, Account > (); //AccountMap to Hold Global Account Number
        List < Account > AccountList = new list < Account > ();
        
        
        system.debug('claims : '+claims);
        if(claims == null)
        {
            return 'There are no Claims to process';
        }
        for (Claims i: claims) {
            duplicateCheckMap.put(i.claimNumber, i);
            GlobalAccountNum.add(i.shipToGlobalID);
            if(i.salesUsers != null)
            {
                
                for (SalesUsers salesUsr: i.salesUsers) {
                    employeeIdSet.add(salesUsr.employeeID);
                }
                
            }
        }


        if (GlobalAccountNum.size() > 0) {
            AccountList = [SELECT ID, Global_Account_Number__c from Account where Global_Account_Number__c In: GlobalAccountNum];

        }
        For(Account a: AccountList) {
            AccountMap.put(a.Global_Account_Number__c, a);
        }
        
        if (duplicateCheckMap.size() > 0) {
            ExistingClaimsList = [SELECT ID, Name, Account_Global_Id__c, Claim_Type__c, Claim_Submit_Date__c, Claim_Amount__c, RecordTypeId, Status__c FROM Claim__c where name IN: duplicateCheckMap.keySet()];
            
            //Check whether Claim already present, if present add Claim number and Id to claimMap which is used to insert Claim Approvers
            if (ExistingClaimsList.size() > 0) {
                for (Claim__c c: ExistingClaimsList) {
                    ExistingClaims++;
                    claimMap.put(c.name, c.id);
                }
            }

            //If there are no existing claims present insert the new ones from the request.
            for (string str: duplicateCheckMap.keySet()) {
                if (!claimMap.containsKey(duplicateCheckMap.get(str).claimNumber)) { //Insert only new Claims and not the existing ones
                    try {
                        Claim__c c = new claim__c();
                        c.Name = duplicateCheckMap.get(str).claimNumber;
                        c.Account_Global_Id__c = duplicateCheckMap.get(str).shipToGlobalID;
                        c.Claim_Type__c = duplicateCheckMap.get(str).claimTypeCode;
                        system.debug('Date Test' + Date.parse(duplicateCheckMap.get(str).claimSubmitDate) + 'without parsing  ' + duplicateCheckMap.get(str).claimSubmitDate);
                        c.Claim_Submit_Date__c = Date.parse(duplicateCheckMap.get(str).claimSubmitDate);
                        c.Claim_Amount__c = decimal.valueof(duplicateCheckMap.get(str).claimAmount);
                        c.Status__c = duplicateCheckMap.get(str).claimStatus;
                        //Condition to add recordtype based on Company code sent from the ESB, if "C"-> Commercial, "R"->Residential
                        if (duplicateCheckMap.get(str).Company == 'c') {
                            c.RecordTypeId = CommercialRT;
                        } else if (duplicateCheckMap.get(str).Company == 'r') {
                            c.RecordTypeId = ResidentialRT;
                        }

                        if (AccountMap.containsKey(duplicateCheckMap.get(str).shipToGlobalID))
                            c.Account__c = AccountMap.get(duplicateCheckMap.get(str).shipToGlobalID).id;

                        insertClaimList.add(c); //List to hold new Claims
                    }
                    Catch(exception e) {
                        system.debug('Exception e' + e);
                        ExceptionMsg = string.valueOf(e);
                    }
                }
            }

            if (insertClaimList.size() > 0) {
            
            
            /*----Set of code to Insert the claims and track Number of success and failures -----------*/
                ClaimsToInsert = Database.insert(insertClaimList, FALSE);
                // Iterate through each returned result
                for (Database.SaveResult sr: ClaimsToInsert) {
                    if (sr.isSuccess()) {
                        ClaimSuccess++;
                    } else {
                        system.debug('entry fail 1');
                        // Operation failed, so get all errors                
                        for (Database.Error err: sr.getErrors()) {
                            ClaimFail++;
                            claimFailReason += ' The following error has occurred while creating claims. ';
                            claimFailReason += err.getStatusCode() + ' : ' + err.getMessage();
                        }
                    }
                }
            
            
            
            

           /*--After Inserting claims add Claim number and Id to claimMap which is used to insert Claim Approvers----*/
                for (Claim__c claim1: insertClaimList) {
                    claimMap.put(claim1.name, claim1.id); 
                }
            }
            //List to hold existing Claim approvers which were not present in the new request.
            ExistingApproversList = [SELECT ID FROM Claim_Approvers__c WHERE Employee_ID__c NOT IN: employeeIdSet and claim__r.name in: duplicateCheckMap.keySet()];

system.debug('ExistingApproversList size'+ExistingApproversList.size());
            if (ExistingApproversList.size() > 0) {
            DeletedClaimApprovers=ExistingApproversList.size();
                delete ExistingApproversList; //Delete all the existing claim approvers which were not in the ESB list
            }

            for (Claim_Approvers__c cl: [SELECT ID, Employee_ID__c FROM Claim_Approvers__c WHERE Employee_ID__c IN: employeeIdSet and claim__r.name in: duplicateCheckMap.keySet()]) {
                existingApproversinTheListMap.put(cl.Employee_ID__c, cl); //Map to hold existing claim approvers which are also present in new list.
            }

            for (User u: [SELECT id, CAMS_Employee_Number__c, name FROM USER WHERE CAMS_Employee_Number__c IN: employeeIdSet]) {
                userMap.put(u.CAMS_Employee_Number__c, u); //User Map to hold Employee Number and User which is used in Mapping Name and Approver for creating Claim Approver
            }
            ExistingClaimApprovers = existingApproversinTheListMap.keyset().size();
            
            /*-----For creating claim approvers---------- */
            for (Claims i: claims) {
                if(i.salesUsers != null)
                {
                    for (SalesUsers salesUsr: i.salesUsers) {
                        try {
                            Claim_Approvers__c clmApp = new Claim_Approvers__c();
                            if (userMap.containsKey(salesUsr.employeeID) && claimMap.containsKey(i.claimNumber)) {
                                clmApp.Name = userMap.get(salesUsr.employeeID).Name;
                                clmApp.Approver__c = userMap.get(salesUsr.employeeID).ID;
                                clmApp.Employee_Email__c = salesUsr.emailID;
                                clmApp.Employee_Id__c = salesUsr.employeeID;
                                
                                clmApp.claim__c = claimMap.get(i.claimNumber);
                                
                                if (!existingApproversinTheListMap.containsKey(salesUsr.employeeID)) //Insert only those Claim Approvers which are not in existing list.
                                    InsertApproversList.add(clmApp); // List to hold Claim Approvers
                            }
                        } catch (Exception e) {
                            ExceptionMsg += string.valueOf(e);
                        }
                    }
                }
            }

        }
        if (InsertApproversList.size() > 0) {
            ClaimApproversToInsert = Database.insert(InsertApproversList, FALSE);
        }
        // Iterate through each returned result
        for (Database.SaveResult sr: ClaimApproversToInsert) {
            if (sr.isSuccess()) {
                ApproverSuccess++;
            } else {
                // Operation failed, so get all errors                
                for (Database.Error err: sr.getErrors()) {
                    ApproverFail++;
                    ApproverFailReason += ' The following error has occurred while creating claim approvers ';
                    ApproverFailReason += err.getStatusCode() + ' : ' + err.getMessage();
                }
            }
        }



        InsertClaimApproversResponse.SuccessResponse sr = new InsertClaimApproversResponse.SuccessResponse(ClaimSuccess, ApproverSuccess, ExistingClaims, ExistingClaimApprovers,DeletedClaimApprovers);
        InsertClaimApproversResponse.FailResponse fr = new InsertClaimApproversResponse.FailResponse(ClaimFail, ApproverFail, ClaimFailReason, ApproverFailReason);

        if (ClaimSuccess > 0 || ApproverSuccess > 0 || ExistingClaimApprovers > 0 || ExistingClaims > 0)
            JSONresponse += string.valueof(sr);
        if (ClaimFail > 0 || ApproverFail > 0)
            JSONresponse += string.valueof(fr);



        if (ExceptionMsg != '') {
            JSONresponse += ExceptionMsg;
        }
        System.debug('JSONresponse :'+JSONresponse);
        return JSONresponse;
    }

}