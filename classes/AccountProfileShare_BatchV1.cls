/**************************************************************************

Name : AccountProfileShare_Batch 

===========================================================================
Purpose : Batch class to Share account profile record with it's accounts mohawk account team members
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Anand       26/April/2017        Created          CSR:

***************************************************************************/
global class AccountProfileShare_BatchV1 implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    global static string ErrorRecords;
     
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        id invMATRTId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId();
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'AccountProfileShare_BatchV1'];
        If(QureyList.size() > 0)
            query = QureyList[0].Query_String__c ;
        else
            query = 'SELECT id,AccountId,UserOrGroupId,Account.Account_Profile__c,isDeleted FROM AccountShare WHERE Account.Business_Type__c = \''+'Residential'+'\'  AND Account.Type = \''+'Invoicing'+'\'  AND Account.Account_Profile__c != null AND (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND ( RowCause = \''+'Manual'+'\' or RowCause = \''+'Team'+'\' ) Order By Account.Account_Profile__c ASC';
        
        return Database.getQueryLocator(query);

    }
    
    global void execute(Database.BatchableContext BC, list<AccountShare> scope) {
        List<Account_Profile__Share> acProShareList = new List<Account_Profile__Share>();
        //Inserting Account Profile Share
        for( AccountShare accShare : scope ){
        
            Account_Profile__Share accProfShare = new Account_Profile__Share();
            accProfShare.ParentId = accShare.Account.Account_Profile__c;
            accProfShare.AccessLevel = 'Edit';
            accProfShare.UserOrGroupId = accShare.UserOrGroupId;
            acProShareList.add(accProfShare);
            System.debug('accProfShare' + accShare);

        }

        if(acProShareList.size() > 0){
            List<Database.SaveResult> updateResult = Database.Insert(acProShareList, false);
            for ( Integer i = 0; i < updateResult.size(); i++ ) {
                if ( !updateResult.get(i).isSuccess() ) {
                    if ( ErrorRecords == null ) {
                        ErrorRecords = 'Operation, Object Name, Record Id, Name, Status Code, Error Message \n';
                    }
                    for ( Database.Error theError : updateResult.get(i).getErrors() ) {
                        string recordString = '"'+'Upsert'+'","'+'Account Profile Share'+ '","' +acProShareList.get(i).Id+'","' + '","' +theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                        ErrorRecords = ErrorRecords + recordString;                         
                    }
                }
            } 
        }

       
        
}
    
    global void finish(Database.BatchableContext BC) {
        if (ErrorRecords != null){
            NotificationUtility.sendBatchExceptionNotification(bc.getJobId(),ErrorRecords,'Account Profile share Error Records');
        }

        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
        {
           APshare_Delete_Batch apDeleteBatch = new APshare_Delete_Batch();
           Database.executeBatch(apDeleteBatch,200);          
        }
    }
    
    global void execute(SchedulableContext SC){
        database.executeBatch(new AccountProfileShare_BatchV1());
    }
}