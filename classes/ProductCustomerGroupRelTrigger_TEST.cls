@isTest
private class ProductCustomerGroupRelTrigger_TEST {

    // Class Variables
    

    static testMethod void updateIndependentAlignedBuyingGroup() {

    	//Create Buying Groups records
    	List<Buying_Group__c> bgList = new List<Buying_Group__c>();
    	bgList.add(createBuyingGroup('BG00', true));
    	bgList.add(createBuyingGroup('BG01', false));
    	bgList.add(createBuyingGroup('BG02', true));
    	bgList.add(createBuyingGroup('BG03', false));

    	insert bgList;
    	System.Debug('updateIndependentAlignedBuyingGroup - bgList: ' + bgList);

    	//Create Product records
    	List<Product2> productList = new List<Product2>();
    	productList.add(createProduct('PROD00', 'Commercial Broadloom', 'FLOORINGNACARPETPRODUCT', 0));
    	productList.add(createProduct('PROD01', 'Commercial Broadloom', 'FLOORINGNACARPETPRODUCT', 1));
    	productList.add(createProduct('PROD02', 'Commercial Broadloom', 'FLOORINGNACARPETPRODUCT', 2));
    	productList.add(createProduct('PROD03', 'Commercial Broadloom', 'FLOORINGNACARPETPRODUCT', 3));

    	insert productList;
    	System.Debug('updateIndependentAlignedBuyingGroup - productList: ' + productList);

    	//Create Product Group Relationship records
    	List<Product_Customer_Group_Relationship__c> pcgrList = new List<Product_Customer_Group_Relationship__c>();
		pcgrList.add(createCustomerGroupRelationship(productList[0], bgList[0], 0)); //PROD00 - BG00
		pcgrList.add(createCustomerGroupRelationship(productList[1], bgList[1], 1)); //PROD01 - BG01
		pcgrList.add(createCustomerGroupRelationship(productList[2], bgList[2], 2)); //PROD02 - BG02
		pcgrList.add(createCustomerGroupRelationship(productList[3], bgList[3], 3)); //PROD03 - BG03

		Test.startTest();

		insert pcgrList;
    	System.Debug('updateIndependentAlignedBuyingGroup - pcgrList: ' + pcgrList);

    	//Test assertions:
    	//After the trigger, PROD00 and PROD02 should have Independent_Aligned_Buying_Group__c related to Buying Group
    	//while PROD01 and PROD03 should have not Buying Group related

    	List<Product2> updatedProducts = [
    		SELECT Id, Name, Independent_Aligned_Buying_Group__c
    		FROM Product2
    		WHERE Id IN : productList];


    	for (Product2 obj : updatedProducts)  {
	    	System.Debug('updateIndependentAlignedBuyingGroup - evaluate asserts for: ' + obj);
    		if (obj.Name == 'PROD00') System.assertNotEquals(null, obj.get('Independent_Aligned_Buying_Group__c'));
    		if (obj.Name == 'PROD01') System.assertEquals(null, obj.get('Independent_Aligned_Buying_Group__c'));
    		if (obj.Name == 'PROD02') System.assertNotEquals(null, obj.get('Independent_Aligned_Buying_Group__c'));
    		if (obj.Name == 'PROD03') System.assertEquals(null, obj.get('Independent_Aligned_Buying_Group__c'));
    	}

    	Test.stopTest();

    }

    public static Buying_Group__c createBuyingGroup(String buyingGroupNumber, Boolean isAlignedGroup) {
    	Buying_Group__c bg = new Buying_Group__c();
    	bg.Buying_Group_Number__c = buyingGroupNumber;
    	bg.Independent_Aligned_Group__c = isAlignedGroup;

    	return bg;
    }

    public static Product_Customer_Group_Relationship__c createCustomerGroupRelationship(Product2 product, Buying_Group__c buyingGroup, Integer index) {

		Product_Customer_Group_Relationship__c pcgr = new Product_Customer_Group_Relationship__c();

		pcgr.Product__c = product.Id;
		pcgr.Customer_Group_Number__c = buyingGroup.Buying_Group_Number__c;
		pcgr.External_Id__c = 'EXTID' + index;

		return pcgr;
	
	}

    public static Product2 createProduct(String productName, String category, String subCategory, Integer index) {

    	String strRecordType = [Select Id From RecordType Where Name='Residential Product' and sobjectType = 'Product2' Limit 1].Id;

    	Product2 p = new Product2();
		p.RecordTypeId = strRecordType;
		p.Name = productName;
		p.Category__c = category;
		p.Sub_Category__c = subCategory;
		p.Salesforce_Product_Category__c = category;
		p.Company__c = 'R';
		p.Inventory_Style_Num__c = 'sl' + index;
		p.Backing__c = 'BK';
		p.Size__c = String.valueOf(1000+index);
		p.Residential_Product_Unique_Key__c = 'R.' + SubCategory.substring(0, 4) + index + '.' + 'BK.' + String.valueOf(1000 + index);             

    	return p;
    }



}