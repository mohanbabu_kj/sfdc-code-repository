public class ContentDocumentTriggerHandler {
    
    public static void preventDeletion(Map<Id, ContentDocument> triggerOldMap){
        set<id> contentDocIds=new set<Id>();
        list<String> profList=new list<String>();
        list<String> objList=new list<String>();
        
        set<String> objset=new set<String>();
        Map<Id,ContentDocument> contentDocMap=new map<Id,ContentDocument>();
        Map<ID,String> contDocLinkMap=new Map<ID,String>();
        String profileNames=system.Label.Profile_Names;
        String ObjNames=system.Label.Object_Names;
        
        if(trigger.isDelete){
            for(Id contId: triggerOldMap.keySet()){
                contentDocIds.add(contId);
            }
        }
        List<ContentDocumentLink> contDocLink = [SELECT ContentDocumentId,LinkedEntity.Name,LinkedEntityId 
                                                 FROM ContentDocumentLink WHERE ContentDocumentId in:contentDocIds];
        
        profList = profileNames.split(',');
        objList  =ObjNames.split(',');
        objSet.addAll(objList);
        String profName = UtilityCls.getProfileName(UserInfo.getProfileId());
        system.debug('User Profile Name::::'+profName);
        for(ContentDocument contDoc: triggerOldMap.values()){
            for(ContentDocumentLink cdl: contDocLink){
                if(contDoc.Id != cdl.ContentDocumentId)
                    continue;

                Id contentId = cdl.LinkedEntityId;
                String objName = contentId.getSObjectType().getDescribe().getName();
                String contId = cdl.LinkedEntityId;
                
                if(contId.substring(0,3) == '005')
                    continue;
                
                if(objSet.contains(objName) && profList.contains(profName)){
                    contDoc.addError(System.Label.Note_Delete_Error_Message);
                }
            }
        }
    }
    
    // Start of Code - MB - Bug 70374 - 2/7/19
    public static void updateProject(List<ContentDocument> newList){
        Set<Id> noteIdSet = new Set<Id>();
        Set<Id> oppIdSet = new Set<Id>();
        
        for(ContentDocument content: newList){
            noteIdSet.add(content.Id);
        }
        
        if(noteIdSet.size()>0){
            for(List<ContentDocumentLink> contentDocLinkList: [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN: noteIdSet]){
                for(ContentDocumentLink contentLink: contentDocLinkList){
                    String entityId = contentLink.LinkedEntityId;
                    if(entityId.subString(0,3) == '006'){
                        oppIdSet.add(entityId);
                    }
                }
            }
        }
        if(oppIdSet.size()>0){
            List<Opportunity> oppIdList = new List<Opportunity>([SELECT Id FROM Opportunity WHERE Id IN: oppIdSet]);
            update oppIdList;
        }
    }
    // End of Code - MB - Bug 70374 - 2/7/19
}