/*
 * // Created by: Dharmesh Patel (Salesforce.com)
 * // Class Description: This class is a wrapper class for the Lightning Component
 * // Change History:
 * // Created Date: 4/16/2018 by Dharmesh Patel
 * */

public class ResidentialPriceGridProductCategory {

    @AuraEnabled public string ProductCategoryName {get; set;}
    @AuraEnabled public Map<String,String> AvailableZones {get; set;}
    
    public ResidentialPriceGridProductCategory(String ProductCategoryName, Map<String,String>AvailableZones){
        this.AvailableZones = AvailableZones;
        this.ProductCategoryName = ProductCategoryName;
    }
    
    public ResidentialPriceGridProductCategory(){}
}