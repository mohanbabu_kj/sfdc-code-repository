/**************************************************************************

Name : NonInvoiceGLoablAccNumber_Batch 

===========================================================================
Purpose :   This batch class will collect all the Global_Account_Number__c 
blank values of Non-invoice Accounts and process it to update by integration.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0       Sasi Naik      12/04/2017     Created 
***************************************************************************/
global class NonInvoiceGLoablAccNumber_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts, Schedulable {

    //Start method
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //return the Account Query to the execute method
        return Database.getQueryLocator([SELECT Id, Name, Ownerid, Global_Account_Number__c, Business_Type__c,RecordtypeId FROM Account WHERE Global_Account_Number__c = null AND Recordtype.DeveloperName = 'Non_Invoicing' LIMIT 24000]);
    }
    // Execute Methos
    global void execute(Database.BatchableContext BC, List<Account> scope) {
            Set<Id> AccountIds = new Set<Id>();
            for(Account ac:scope){
                AccountIds.add(ac.Id);
            }
            //check the AccoountId's are present in the set
            if(AccountIds.size()>0){
                //Call the method from the AccNonInvoicingNogenerator class
                AccNonInvoicingNogenerator.SendHttpsreq(AccountIds);
            }
    }
    //Finish Method
    global void finish(Database.BatchableContext BC) {
            

    }
     // Schedulable bath
     global void execute(SchedulableContext sc){
            Database.executeBatch(new NonInvoiceGLoablAccNumber_Batch());
      }

}