@istest
public class Batch_RollupMatToAPSchedulable_Test {
    private static void init(){   
       
        
        MAT_Batch_Setting__c setting = new MAT_Batch_Setting__c();
            setting.Last_Run_At__c = SYstem.today().adddays(-5);
            setting.MinutesDelay__c = 5;
            setting.Batch_Size__c = 200;
            setting.Name='Batch_RollupMatToAP';
            insert setting;
    }
    static testmethod void test(){
         init();
        
        String sch = '0 0 23 * * ?';
        String jobId = System.schedule('testBatch_RollupMatToAP',sch, new Batch_RollupMatToAPSchedulable());
        system.assert( jobId != null );
    }

}