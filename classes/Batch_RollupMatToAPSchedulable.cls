global class Batch_RollupMatToAPSchedulable implements Schedulable {
    global void execute (SchedulableContext ctx) {
        Integer batchSize=200;
         MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('Batch_RollupMatToAP');
          if (custoSettingVal != null) {
            batchSize = custoSettingVal.Batch_Size__c != null ? Integer.valueOf(custoSettingVal.Batch_Size__c) : 200;    
          }
        Batch_RollupMatToAP b = new Batch_RollupMatToAP(false, true); // Updated the flag
        database.executebatch(b,batchSize);
    }

}