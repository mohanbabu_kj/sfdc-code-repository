/**************************************************************************

Name : ProductSearch_CC

===========================================================================
Purpose : This class is used for the lightning components which will search the production and assign it to opportunityLineIteams
===========================================================================
History
--------
VERSION      AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik       14/MAR/2017    Created                             
***************************************************************************/
public without sharing class ProductSearch_CC {
    
    public class ProductInfo{
        @AuraEnabled public List<wrapproduct> prodList;
        @AuraEnabled public Map<String, Boolean> fieldBooleanMap;
        @AuraEnabled public Map<String, String> fieldValuesMap;
        @AuraEnabled public List<String> collectionList;
        @AuraEnabled public List<String> subCategoryList;
        @AuraEnabled public List<String> promotionList;
        @AuraEnabled public List<String> fiberTypeList;
        @AuraEnabled public List<String> backingDescList;
        @AuraEnabled public List<String> faceWeightList; //Added BY Mudit - BUG 67456 - 28-11-2018
        @AuraEnabled public List<String> selectedFaceWeightList; //Added BY Mudit - BUG 67456 - 28-11-2018
        @AuraEnabled public User userInfo; //Added BY MB - BUG 68960 - 1/8/19
        
        public ProductInfo(){
            prodList = new List<wrapproduct>();
            collectionList = new List<String>();
            subCategoryList = new List<String>();
            promotionList = new List<String>();
            fibertypeList = new List<String>();
            backingDescList = new List<String>();
            faceWeightList = new List<String>();//Added BY Mudit - BUG 67456 - 28-11-2018
            selectedFaceWeightList = new List<String>();//Added BY Mudit - BUG 67456 - 28-11-2018
            fieldValuesMap = new Map<String, String>();
            fieldBooleanMap = new Map<String, Boolean>();
            userInfo = new User();
        }
    }
    
    
    // to featch the State picklist values from User Object and pass it to the UI - Added by Susmitha - May 22,2018
    @AuraEnabled
    public static List<String> getSubCategoryPickListvalues(){
        /*List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2, 'Sub_Category_C__c');
        system.debug('Entry method:::::::Cpickvalues::::'+ Cpickvalues);
        return Cpickvalues;*/
        List<String> lstPickvals = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Product2');//From the Object Api name retrieving the SObject
        Sobject ObjectName = targetType.newSObject();
        Schema.sObjectType sobjType = ObjectName.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobjDesc = sobjType.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> mapField = sobjDesc.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> listPickListVals = mapField.get('Sub_Category_C__c').getDescribe().getPickListValues(); 
        
        for (Schema.PicklistEntry entry : listPickListVals) {
            lstPickvals.add((string)entry.getLabel());
        }
        /* BDS added 1/14/19 */
        System.debug('List of picklist values:' + 'lstPickvals');
        return lstPickvals;

    } // end of code - Susmitha
    
    @AuraEnabled
    public static List<wrapproduct> getproductlist( string StyleNames,  string StyleNumber, string Collection,Boolean GenericProduct,string Brand,string ProductType, Boolean QuickShip,String recId,String SubCategory) {
        System.debug('**getproductlist is executed ');
        List<Product2> productList = new List<Product2>(); 
        List<wrapproduct> wrapproductList = new List<wrapproduct>();
      /*  String RecordTypeId = null;
        
        string OuserBusinessType = [Select Id,Name,Business_Type__c from user where Id=:UserInfo.getUserId()].Business_Type__c;
        
        if(OuserBusinessType!=''){
            if(OuserBusinessType == UtilityCls.COMMERCIAL){
                OuserBusinessType = UtilityCls.COMMERCIALPRODUCT;
            } else if(OuserBusinessType == UtilityCls.RESIDENTIAL){
                OuserBusinessType = UtilityCls.RESIDENTIALPRODUCT;
            }
            try{
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(OuserBusinessType).getRecordTypeId();  
            }
            catch(exception e){
                UtilityCls.createExceptionLog(e,  'ProductSearch_CC', 'getproductlist', '', '', 'referenceInfo');
                
                system.debug('exception e'+e);
            }
        }
        String soql ='SELECT Id, Name,IsActive,Mid_Market_CAN__c,Mid_Market_USD__c,Floor_CAN__c,Floor_USD__c,Stretch_CAN__c,Stretch_USD__c,Price__c,RecordType.Name,Quantity__c,ProductCode,Mohawk_Public_Group_URL__c,MHK_Group_URL__c, View_Product__c,Collection_Name__c,Product_Style_Number__c,Category__c,Sub_Category__c,Buying_Group__c,Brand_Code__c,Brand_Description__c,UOM__c FROM Product2 WHERE RecordTypeId =\''+RecordTypeId+'\''; //Modified by susmitha for Bug 42115 on Jan 11/2018
        
        if(GenericProduct){ // Added by MB -07/13/17
            soql += ' and Generic_Product__c =: GenericProduct';
        }
        
        if( (QuickShip !=null) && QuickShip){ // Added by MB -07/13/17
            soql += ' and QuickShip_Product__c =: QuickShip';
        }
        if(StyleNames!=''){
            soql += ' and Name Like \'%'+String.escapeSingleQuotes(StyleNames)+'%\'';
        }
        
        if(StyleNumber!=''){
            soql += ' and Product_Style_Number__c Like \'%'+String.escapeSingleQuotes(StyleNumber)+'%\'';
        }
        
        if(Collection!='none'){
            soql += ' and Collection_Name__c = \''+Collection+'\'';
        }
        
        if(Brand!='none'){
            soql += ' and Collection_Brand__c = \''+Brand+'\'';
        }
        
        if(ProductType!='none'){
            soql += ' and Product_Type__c = \''+ProductType+'\'';
        }
        if(SubCategory!='none'){
            soql += ' and Sub_Category__c = \''+SubCategory+'\'';
        }
        
        soql=soql + ' and IsActive = true ORDER BY Name ASC limit 1000';
        //Start Modified By LJ Jan-31-2018(product.isExists to check if product already added to the project)
        Set<id> proPodList;
        Map<Id, List<OpportunityLineItem>> projProdMap = new Map<Id, List<OpportunityLineItem>>();
        system.debug('recId :'+recId);
        if(UtilityCls.isStrNotNull(recId)){
            // commented by Mohan
            //proPodList = getProjectProducts(recId);
            projProdMap = getProjectProductsMap(recId);            
        }
        System.debug('**projProdMap: ' + projProdMap);                	
        System.debug('**ProductList: ' + Database.Query(soql));
        for(Product2 p:Database.Query(soql)){
            Boolean prodExists = false;
            System.debug('**Product p: ' + p);
            if(!projProdMap.isEmpty() && projProdMap.containsKey(p.id)){ 
                List<OpportunityLineItem> oliList = projProdMap.get(p.Id);
                for(OpportunityLineItem oli: oliList){
                    prodExists = true;
                    wrapproduct wp = new wrapproduct(p,prodExists);
                    wp.price = oli.UnitPrice;
                    wp.quantity = oli.Quantity;
                    wrapproductList.add(wp);
                    System.debug('**wp ' + wp);
                }
            } else {
                wrapproductList.add(new wrapproduct(p,prodExists));
            }            
        }//end for 
        system.debug('wrapproductList :'+wrapproductList);
        //End By Modified LJ Jan-31-2018(product.isExists to check if product already added to the project)
        //*/
        return wrapproductList;
        
    }
    
    //get the product ids Map based on project Id
    //Created By Mohan
    public static Map<Id, List<OpportunityLineItem>> getProjectProductsMap(String projectId){
        Map<Id, List<OpportunityLineItem>> projProdMap = new Map<Id, List<OpportunityLineItem>>();
        for(OpportunityLineItem oli: [SELECT Product2Id, Quantity, UnitPrice, PricebookEntryId, Color__c FROM OpportunityLineItem where OpportunityId =: projectId]){
            if(projProdMap.containsKey(oli.Product2Id)){
                projProdMap.get(oli.Product2Id).add(oli);
            }else{
                List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
                oliList.add(oli);
                projProdMap.put(oli.Product2Id, oliList);
            }
        }
        return projProdMap;
    }    
    
    //Method to get the Collection Name Picklist vlaues to UI
    @AuraEnabled
    public static List<String> getProductCollectionNamePicklistValues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2,UtilityCls.COLLECTIONNAME);
        return Cpickvalues;
    }
    
    //Method to get the Collection Brand Picklist vlaues to UI
    @AuraEnabled
    public static List<String> getProductCollectionBrandPicklistValues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2,UtilityCls.COLLECTIONBRAND);
        return Cpickvalues;
    }
    
    //Method to get the Product Type Picklist vlaues to UI
    @AuraEnabled
    public static List<String> getProductProductTypePicklistValues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2,UtilityCls.PRODUCTTYPE);
        return Cpickvalues;
    }
    
    //Method to get the Promotion Picklist vlaues to UI
    public static List<String> getPromotionPicklistValues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2,'Promotion__c');
        return Cpickvalues;
    }
    
    //Method to get the fiber type Picklist vlaues to UI
    public static List<String> getFiberTypePicklistValues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2,'Fiber_Type__c');
        return Cpickvalues;
    }
    //Method to get the  backing desc picklist vlaues to UI
    public static List<String> getBackingDescPicklistValues(){
        List<String> Cpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2,'Backing_Desc__c');
        return Cpickvalues;
    }
    
    //Method to get the Face Weight Picklist vlaues to UI //Added BY Mudit - BUG 67456 - 28-11-2018
    @AuraEnabled
    public static List<String> getFaceWeightPicklistValues(){
        List<String> FWpickvalues = UtilityCls.getPicklistValues(UtilityCls.PRODUCT2,'Face_Weight__c');
        return FWpickvalues;
    }
    
    //Method to get the Logged in User Business Type values to UI
    @AuraEnabled
    public static String getLoggedinuserBusinessTypeInfo() {
        return [Select Id,Business_Type__c from User where Id=:UserInfo.getUserId() Limit 1].Business_Type__c;
    } 
    
    //This method will add the selected products form the add product function to projectlineitem
    //Modified by Mohan - 02/11/2018 - to upsert the Opportunity Line items
    @AuraEnabled
    public static Boolean AddProductstoProject(String ProjectId,String SelectedProducts){
        Boolean SuccessValue = false;
        Set<Id> prodIdSet = new Set<Id>();
        Map<Id, Id> prodPriceBookIdMap = new Map<Id, Id>();
        //List<Product2> productsToUpdate = new List<Product2>();
        try{
            System.debug(LoggingLevel.INFO, '*** SelectedProducts: ' + SelectedProducts);        
            Map<Id, List<OpportunityLineItem>> oppLineItemMap = getProjectProductsMap(ProjectId);        
            List<opportunitylineitem> newlistopplisitem = new List<opportunitylineitem>();
            List<wrapproduct>  deserializedOppLine = (List<wrapproduct>)JSON.deserialize(SelectedProducts, List<wrapproduct>.class);
            System.debug('**deserializedOppLine: ' + deserializedOppLine + '\n Size of deserializedOppLine: ' + deserializedOppLine.size());
            for(wrapproduct pr:deserializedOppLine){
                prodIdSet.add(pr.pr.Id);
            }
            User usr = [SELECT DefaultCurrencyIsoCode  FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1]; //Added by MB - Bug 69678 - 1/21/19
            for(PricebookEntry record:[select id,unitprice,CurrencyIsoCode,product2id,product2.name,product2.Color_Cast__c from pricebookentry 
                                       where product2id IN: prodIdSet and isActive = true and CurrencyIsoCode =: usr.DefaultCurrencyIsoCode ]){ //Added by MB - 2/27/18
                                           prodPriceBookIdMap.put(record.Product2Id, record.Id);
                                       }
            System.debug('priceBookEntryMap: ' + prodPriceBookIdMap);
            for(wrapproduct pr:deserializedOppLine){
                Id priceBookEntryId = prodPriceBookIdMap.containsKey(pr.pr.Id) ? prodPriceBookIdMap.get(pr.pr.Id) : null;
                if(priceBookEntryId !=null &&pr.isSelected !=null && pr.isSelected){
                    //if(pr.oppLineItemId != null){ //!oppLineItemMap.isEmpty() && oppLineItemMap.containsKey(pr.pr.Id)){
                    //List<OpportunityLineItem> oliList = oppLineItemMap.get(pr.pr.Id);
                    /*if(oliList.size()>0){
                        for(OpportunityLineItem oli: oliList){
                            opportunitylineitem OppLineItem = new opportunitylineitem();
                            //if(pr.oppLineItemId == oli.Id &&oli.PricebookEntryId == priceBookEntryId){ 
                            if(pr.color == oli.Color__c && oli.PricebookEntryId == priceBookEntryId){ //Added additional condition for color - MB - Bug 69678 - 1/21/19
                                OppLineItem.Id = oli.Id;
                            } else {
                                OppLineItem.opportunityid = ProjectId;
                                OppLineItem.pricebookentryid = priceBookEntryId;
                                OppLineItem.Color__c = pr.color; //Added by MB - Bug 69678 - 1/21/19
                            }
                            OppLineItem.unitprice = pr.Price;
                            OppLineItem.quantity = pr.Quantity;  
                            
                            newlistopplisitem.add(OppLineItem);
                            System.debug('**OppLineItem: ' + OppLineItem);
                        }
                    }else{*/
                        OpportunityLineItem oppLineItem = new OpportunityLineItem();
                        oppLineItem.opportunityid = ProjectId;
                        oppLineItem.pricebookentryid = priceBookEntryId;
                        oppLineItem.unitprice = pr.Price;
                        oppLineItem.quantity = pr.Quantity;  
                        oppLineItem.Color__c = pr.color; //Added by MB - Bug 69678 - 1/21/19
                        newlistopplisitem.add(oppLineItem);
                        System.debug('**OppLineItem: ' + oppLineItem);
                    //} 
                }
                //}
                System.debug('**newlistopplisitem: ' + newlistopplisitem + '\n Size of newlistopplisitem: ' + newlistopplisitem.size());
                if(newlistopplisitem.size()>0){
                    Database.UpsertResult[] upsertResultList = Database.upsert(newlistopplisitem, false);
                    for(Database.UpsertResult ur: upsertResultList){
                        if(!ur.isSuccess()){
                            System.debug('**error message: ' + ur.getErrors());
                        }
                    }
                    SuccessValue = true;
                }
            }
        }
        catch(Exception ex){
            SuccessValue = false;
        }
        System.debug('**SuccessValue: ' + SuccessValue);
        return SuccessValue;
    }
    
    @AuraEnabled
    public static ProductInfo initialize(){
        ProductInfo prodInfo = new ProductInfo();
        prodInfo.collectionList = getProductCollectionNamePicklistValues();
        prodInfo.subCategoryList = getSubCategoryPickListvalues();
        prodInfo.promotionList = getPromotionPickListvalues();
        prodInfo.fiberTypeList = getFiberTypePickListvalues();
        prodInfo.backingDescList = getBackingDescPickListvalues();
        prodInfo.faceWeightList = getFaceWeightPicklistValues();//Added BY Mudit - BUG 67456 - 28-11-2018
        prodInfo.selectedFaceWeightList = new List<string>();//Added BY Mudit - BUG 67456 - 28-11-2018
        
        prodInfo.fieldValuesMap.put('styleName', '');
        prodInfo.fieldValuesMap.put('styleNumber', '');
        prodInfo.fieldValuesMap.put('collection', '');
        prodInfo.fieldValuesMap.put('promotion', '');
        prodInfo.fieldValuesMap.put('subCategory', '');
        
        //Newly added on Sept 26,2018 for bug 65443
        prodInfo.fieldValuesMap.put('fiberType', '');
        prodInfo.fieldValuesMap.put('backing', '');
        //End Newly added on Sept 26,2018 for bug 65443
        
        /* Start of Code - MB - Bug 68960 - 1/8/19 */
        User usr = [SELECT Id, Business_Type__c, CountryCode, International_User__c from User Where Id =: UserInfo.getUserId() LIMIT 1];
        prodInfo.userInfo = usr;
        /* End of Code - MB - Bug 68960 - 1/8/19 */
        
        prodInfo.fieldBooleanMap.put('genericProduct', false);
        prodInfo.fieldBooleanMap.put('quickShip', false);
        prodInfo.fieldBooleanMap.put('customProduct', false);
        prodInfo.fieldValuesMap.put('minPrice', System.Label.Min_Base_Price);
        prodInfo.fieldValuesMap.put('maxPrice', System.Label.Max_Base_Price);
        prodInfo.fieldValuesMap.put('minFaceweight', System.Label.Min_Base_Price);
        prodInfo.fieldValuesMap.put('maxFaceweight', System.Label.Max_Base_Price);
        
        //String userCountry = [SELECT CountryCode from USER WHERE Id =: userInfo.getUserId()].CountryCode;
        //prodInfo.fieldValuesMap.put('userCountry', userCountry);
        
        return prodInfo;
    }
    
    @AuraEnabled
    public static ProductInfo getProductListNew(String productInfoStr, String recId) {
        ProductInfo productInfo = (ProductInfo)JSON.deserialize(productInfoStr, ProductInfo.class);
        List<Product2> productList = new List<Product2>(); 
        List<wrapproduct> wrapproductList = new List<wrapproduct>();
        String styleName = productInfo.fieldValuesMap.get('styleName');
        String RecordTypeId = null;
        Decimal minPrice = Decimal.valueOf(productInfo.fieldValuesMap.get('minPrice'));
        Decimal maxPrice = Decimal.valueOf(productInfo.fieldValuesMap.get('maxPrice'));
        Decimal minFaceweight = Decimal.valueOf(productInfo.fieldValuesMap.get('minFaceweight'));
        Decimal maxFaceweight = Decimal.valueOf(productInfo.fieldValuesMap.get('maxFaceweight'));
        
        System.debug('MinPrice: ' + minPrice + '; Max Price: ' + maxPrice);
        system.debug( ':::::>' + productInfo.selectedFaceWeightList);
        try{
            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(UtilityCls.COMMERCIALPRODUCT).getRecordTypeId();  
        }
        catch(exception e){
            UtilityCls.createExceptionLog(e,  'ProductSearch_CC', 'getproductlist', '', '', 'referenceInfo');
            system.debug('exception e'+e);
        }
        Date today=System.today(); 
        String soql = 'SELECT Id, Name,IsActive,Mid_Market_CAN__c,Mid_Market_USD__c,Floor_CAN__c,Floor_USD__c,Stretch_CAN__c,' +
            ' Stretch_USD__c,Price__c,RecordType.Name,Quantity__c,ProductCode,Mohawk_Public_Group_URL__c,MHK_Group_URL__c,' +
            ' View_Product__c,Collection_Name__c,Product_Style_Number__c,Category__c,Sub_Category__c,Buying_Group__c, Generic_Product__c,' +
            ' SVP_Roll_Price_USD__c, Bonus_Price_USD__c, SVP_Roll_Price_CAN__c, Bonus_Price_CAN__c,' +
            ' X1_0_Pricing_CAD__c, X1_0_Pricing_USD__c, X1_5_Pricing_CAD__c,' +
            ' X1_5_Pricing_USD__c, X2_0_Pricing_CAD__c, X2_0_Pricing_USD__c, X2_5_Pricing_USD__c, X2_5_Pricing_CAD__c, Promotion__c,' +
            ' Includes_Freight__c, Brand_Code__c,Brand_Description__c,UOM__c,Drop_Date__c,Custom_Product__c,Color_Cast__c,Wear_Layer__c FROM Product2'+
            //' WHERE IsActive = true AND (Drop_Date__c >= :today OR Drop_Date__c = null) AND RecordTypeId =\''+RecordTypeId+'\'';
        	' WHERE IsActive = true AND RecordTypeId =\''+RecordTypeId+'\''; // Removed Drop Date condition - Bug 70851 - MB - 2/18/19
        
        if(productInfo.fieldBooleanMap.get('genericProduct')){
            soql += ' and Generic_Product__c = true';
        }
        
        if(productInfo.fieldBooleanMap.get('quickShip')){
            soql += ' and QuickShip_Product__c = true';
        }
        /* Start of Code - Nagen - Bug 69678 - 1/21/19 */
        if(productInfo.fieldBooleanMap.get('customProduct')){
            soql += ' and Custom_Product__c = true';
        }
        /* End of Code - Nagen - Bug 69678 - 1/21/19 */
        System.debug('FieldValuesMap: ' + productInfo.fieldValuesMap);
        if(styleName != ''){
            System.debug('Style Name: ' + styleName);
            soql += ' and Name Like \'%'+String.escapeSingleQuotes(styleName)+'%\'';
        }
        
        if(productInfo.fieldValuesMap.get('styleNumber') != ''){
            soql += ' and Product_Style_Number__c Like \'%'+String.escapeSingleQuotes(productInfo.fieldValuesMap.get('styleNumber'))+'%\'';
        }
        
        if(productInfo.fieldValuesMap.get('collection') != 'none'){
            soql += ' and Collection_Name__c = \''+productInfo.fieldValuesMap.get('collection')+'\'';
        }
        
        if(productInfo.fieldValuesMap.get('promotion') != 'none'){
            soql += ' and Promotion__c = \''+productInfo.fieldValuesMap.get('promotion')+'\'';
        }
        
        if(productInfo.fieldValuesMap.get('subCategory') != 'none'){
            soql += ' and Sub_Category__c = \''+productInfo.fieldValuesMap.get('subCategory')+'\'';
        }
        
        if(productInfo.fieldValuesMap.get('minPrice') != '0'){
            if(productInfo.userInfo.CountryCode == 'US'){ // Updated field condition as userCountry is no longer used - Bug 70815 - 2/15/19
                soql += ' and Mid_Market_USD__c >= :minPrice';
            }else if(productInfo.userInfo.CountryCode == 'CA'){ // Updated field condition as userCountry is no longer used - Bug 70815 - 2/15/19
                soql += ' and Mid_Market_CAN__c >= :minPrice';
            }
        }
        
        System.debug('maxPrice: ' + maxPrice);
        if(productInfo.fieldValuesMap.get('maxPrice') != '0' && productInfo.fieldValuesMap.get('maxPrice') != '50'){
            System.debug('userCountry: ' + productInfo.userInfo.CountryCode);
            if(productInfo.userInfo.CountryCode == 'US'){ // Updated field condition as userCountry is no longer used - Bug 70815 - 2/15/19
                soql += ' and Mid_Market_USD__c <= :maxPrice';
            }else if(productInfo.userInfo.CountryCode == 'CA'){ // Updated field condition as userCountry is no longer used - Bug 70815 - 2/15/19
                soql += ' and Mid_Market_CAN__c <= :maxPrice';
            }
        }
        
        //Newly added on Sept 26,2018 for bug 65443
        if(productInfo.fieldValuesMap.get('fiberType') != null && productInfo.fieldValuesMap.get('fiberType') != ''&& productInfo.fieldValuesMap.get('fiberType') != 'none'){
            soql += ' and Fiber_Type__c = \''+productInfo.fieldValuesMap.get('fiberType')+'\'';
        }
        if(productInfo.fieldValuesMap.get('backing') != null && productInfo.fieldValuesMap.get('backing') != 'none'){
            soql += ' and Backing_Description__c = \''+productInfo.fieldValuesMap.get('backing')+'\'';
        }
        /*Commented BY Mudit - BUG 67456 - 28-11-2018
if(productInfo.fieldValuesMap.get('minFaceweight') != '0'){
soql += ' and Face_Weigh__c >= :minFaceweight';
}
if(productInfo.fieldValuesMap.get('maxFaceweight') != '0' && productInfo.fieldValuesMap.get('maxFaceweight') != '50'){
soql += ' and Face_Weigh__c <= :maxFaceweight';
}
Commented BY Mudit - BUG 67456 - 28-11-2018*/
        if( productInfo.selectedFaceWeightList != null  && productInfo.selectedFaceWeightList.size() > 0){
            soql += ' AND '+generateSoqlForFaceWeight( productInfo.selectedFaceWeightList );
        }
        //End Newly added on Sept 26,2018 for bug 65443
        soql=soql + ' ORDER BY Name ASC limit 1000';
        Set<id> proPodList;
        Map<Id, List<OpportunityLineItem>> projProdMap = new Map<Id, List<OpportunityLineItem>>();
        system.debug('recId :'+recId);
        System.debug('SOQL: ' + soql);
        if(UtilityCls.isStrNotNull(recId)){
            projProdMap = getProjectProductsMap(recId);            
        }
        System.debug('**projProdMap: ' + projProdMap); 
        System.debug('**ProductList: ' + Database.Query(soql));
        
        for(Product2 p:Database.Query(soql)){
            Boolean prodExists = false;
            //Boolean moreThan1Product = false;
            //Integer count = 0;
            //System.debug('**Product p: ' + p);
            // Start of Code - MB - Bug 68960 - 1/15/19 //
            if(productInfo.userInfo.International_User__c){
                p.UOM__c = System.Label.Sq_Meter;
            }
            // End of Code - MB - Bug 68960 - 1/15/19 //
            /* Start of Comment - MB - Bug 69678 - 2/4/19 
            if(!projProdMap.isEmpty() && projProdMap.containsKey(p.id)){  
                List<OpportunityLineItem> oliList = projProdMap.get(p.Id);
                if(oliList.size() > 1){
                    moreThan1Product = true;
                }
                for(OpportunityLineItem oli: oliList){
                    prodExists = true;
                    wrapproduct wp = new wrapproduct(p,prodExists);
                    wp.price = oli.UnitPrice;
                    wp.quantity = oli.Quantity;
                    wp.oppLineItemId = oli.Id;
                    if(p.Promotion__c != null && p.Promotion__c != ''){
                        wp.isPromotion = true;
                    }
                    // Start of Code - MB - Bug 69678 - 1/21/19 //
                    if(p.Custom_Product__c){ wp.color = oli.Color__c; }
                    if(moreThan1Product && count > 0){ wp.duplicateEntry = true; }
                    count += 1;
                    // End of Code - MB - Bug 69678 - 1/21/19 //
                    wrapproductList.add(wp);
                }
            } else {
                wrapproduct prod = new wrapproduct(p,prodExists);
                if(p.Promotion__c != null && p.Promotion__c != ''){
                    prod.isPromotion = true;
                }
                prod.quantity = prod.price = 0;
                wrapproductList.add(prod);
            } - End of Comment - MB - Bug 69678 - 2/4/19 */            
            // Start of Code - MB - Bug 69678 - 2/4/19 //
            wrapproduct prod = new wrapproduct(p,prodExists);
            if(p.Promotion__c != null && p.Promotion__c != ''){
                prod.isPromotion = true;
            }
            prod.quantity = prod.price = 0;
            wrapproductList.add(prod);
            // End of Code - MB - Bug 69678 - 2/4/19 //
        }
        system.debug('wrapproductList :'+wrapproductList);
        if(wrapproductList.size()>0){
            productInfo.prodList = wrapproductList;
        }
        return productInfo;
    }
    
    public static string generateSoqlForFaceWeight( List<string>selVal ){
        string returnstr;
        if( selVal != null && selVal.size() > 0 ){
            List<string>beforeJoin = new List<string>();
            string startBracket = '(';
            string endBracket = ')';
            string joined;
            for (integer i = 0; i < selVal.size(); i++) {
                if( selVal.size() == 1 ){
                    beforeJoin.add( 'Face_Weigh__c >= '+selVal[i]+' AND Face_Weigh__c < '+ string.valueof( integer.valueof( selVal[i] ) +1 ) );     
                }else{
                    beforeJoin.add( startBracket+' Face_Weigh__c >= '+selVal[i]+' AND Face_Weigh__c < '+ string.valueof( integer.valueof( selVal[i] ) +1 ) + endBracket); 
                }
            }
            if( selVal.size() == 1 ){
                joined = string.join(beforeJoin, ' OR ');    
            }else{
                joined = startBracket + string.join(beforeJoin, ' OR ') + endBracket;    
            }
            returnstr = joined;
        }
        return returnstr;
    }
    
    /****************
wrapproduct
****************/
    public class wrapproduct{
        @AuraEnabled public Product2 pr;
        @AuraEnabled public Id productId;
        @AuraEnabled public Id oppLineItemId;
        @AuraEnabled public String color;
        @AuraEnabled public Decimal price;
        @AuraEnabled public Decimal quantity;
        @AuraEnabled public boolean isSelected;
        @AuraEnabled public boolean isPromotion;
        @AuraEnabled public boolean isFutureDropDate;
        @AuraEnabled public boolean duplicateEntry;
        
        public wrapproduct(Product2 p,Boolean isExists){
            if(p.Drop_Date__c >= system.today() ){
                this.isFutureDropDate =true;
            }else{
                this.isFutureDropDate =false;
            }
            this.pr=p;
            this.isselected=false;
            this.isPromotion = false;
            this.duplicateEntry = false;
        }
    }
}