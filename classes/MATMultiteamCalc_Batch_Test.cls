/**************************************************************************

Name : MATMultiteamCalc_Batch_Test
===========================================================================
Purpose : Test class to cover MATMultiteamCalc_Batch class 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha       Aug/29/2018        Create  
***************************************************************************/
@isTest()
private class MATMultiteamCalc_Batch_Test {
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    static List<Territory_User__c>  terrUser;
    static List<Territory_User__c>  terrUser1;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    
    
   
     static testMethod void testMethod0() {        
        MAT_Batch_Setting__c cs = new MAT_Batch_Setting__c();
        cs.Name='MATMultiteamCalc_Batch';
        cs.Last_Run_At__c=system.today().addDays(-1);
        cs.Batch_Query__c = 'SELECT id,Account_Profile__c,Accounts_Account_Profile_Id__c,RecordTypeId FROM Mohawk_Account_Team__c WHERE Account_Profile__r.Two_Sales_Teams__c = true AND Account_Profile__r.Primary_Business__c != null';
        cs.MinutesDelay__c = 5;
        insert cs;
         
        Utility_Test.getTeamCreationCusSetting();
        Utility_Test.getDefaultConfCusSetting();
        //Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = 'R.197101';
        APSC.Exclude_Chain_Numbers_2__c = 'R.197106';
        insert APSC;
        
        AP_Trigger_Settings__c APSC1 = new AP_Trigger_Settings__c();
        APSC1.Name = 'Chain Number Exclude';
        APSC1.Exclude_Chain_Numbers_1__c = '10';
        APSC1.Exclude_Chain_Numbers_2__c = '13';
        insert APSC1;
        
         
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
        apSetting[0].Red_Threshold_Carpet__c = 10;
        apSetting[0].Yellow_Threshold_Cushion__c = 10;
        apSetting[0].Red_Threshold_Cushion__c = 10;
        apSetting[0].Yellow_Threshold_Hardwood__c = 10;
        apSetting[0].Red_Threshold_Hardwood__c = 10; 
        apSetting[0].Yellow_Threshold_Laminate__c = 10; 
        apSetting[0].Red_Threshold_Laminate__c = 10;
        apSetting[0].Yellow_Threshold_Tile__c = 10;
        apSetting[0].Red_Threshold_Tile__c = 10;
        apSetting[0].Yellow_Threshold_Resilient__c = 10;
        apSetting[0].Red_Threshold_Resilient__c = 10;
        apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
        apSetting[0].Red_Threshold_Mainstreet__c = 10;
        update apSetting[0];
      
        List<Account_Profile__c> apList = new List<Account_Profile__c>();
        Account_Profile__c ap = new Account_Profile__c();
        ap.Primary_Business__c = apSetting[0].Id;
        ap.Annual_Retail_Sales__c = 300000;
        ap.Annual_Retail_Sales_Non_Flooring__c = 10;
        ap.Chain_Number__c = 'R.197101';
        ap.Two_Sales_Teams__c=true;
        apList.add(ap);        
        insert apList;
        
        List<Territory__c> terrList = Utility_Test.createTerritories(false, 1);
        for(Integer i=0;i<terrList.size();i++){
            terrList[i].Name = 'Testterr'+i;
            terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
        }

        //List<Account> acList = Utility_Test.createAccounts(true, 1);
        List<Account> resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
        for(Integer i=0;i<resAccListForInvoicing.size();i++){
            resAccListForInvoicing[i].Business_Type__c = 'Residential';
            resAccListForInvoicing[i].Chain_Number__c = 'R.197106';
            resAccListForInvoicing[i].Account_Profile__c = ap.Id;
        }
        insert resAccListForInvoicing;

        List<Mohawk_Account_Team__c> mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER);
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].Product_Types__c = 'Carpet';
            mohAccTeam[i].Brands__c = 'Aladdin';
            mohAccTeam[i].BMF_Team_Member__c = false;
            mohAccTeam[i].account_profile__c = ap.id;
        }
        
        insert mohAccTeam;
        system.assert( mohAccTeam.size() > 0 );
        test.startTest();    
            MATMultiteamCalc_Batch  cls = new MATMultiteamCalc_Batch ();
            String jobId = database.executeBatch(cls);
            System.assert(jobId != null);
            
            String sch = '0 0 23 * * ?';
            system.schedule('MATMultiteamCalc_Batch_Test', sch, cls);
        test.stopTest();

    }
    
    static testMethod void testMethod1() {        
        MAT_Batch_Setting__c cs = new MAT_Batch_Setting__c();
        cs.Name='MATMultiteamCalc_Batch';
        //cs.Last_Run_At__c=system.today().addDays(-1);
        //cs.Batch_Query__c = 'SELECT id,Account_Profile__c,Accounts_Account_Profile_Id__c,RecordTypeId FROM Mohawk_Account_Team__c WHERE Account_Profile__r.Two_Sales_Teams__c = true AND Account_Profile__r.Primary_Business__c != null';
        //cs.MinutesDelay__c = 5;
        insert cs;
         
        Utility_Test.getTeamCreationCusSetting();
        Utility_Test.getDefaultConfCusSetting();
        //Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = 'R.197101';
        APSC.Exclude_Chain_Numbers_2__c = 'R.197106';
        insert APSC;
        
        AP_Trigger_Settings__c APSC1 = new AP_Trigger_Settings__c();
        APSC1.Name = 'Chain Number Exclude';
        APSC1.Exclude_Chain_Numbers_1__c = '10';
        APSC1.Exclude_Chain_Numbers_2__c = '13';
        insert APSC1;
        
         
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
        apSetting[0].Red_Threshold_Carpet__c = 10;
        apSetting[0].Yellow_Threshold_Cushion__c = 10;
        apSetting[0].Red_Threshold_Cushion__c = 10;
        apSetting[0].Yellow_Threshold_Hardwood__c = 10;
        apSetting[0].Red_Threshold_Hardwood__c = 10; 
        apSetting[0].Yellow_Threshold_Laminate__c = 10; 
        apSetting[0].Red_Threshold_Laminate__c = 10;
        apSetting[0].Yellow_Threshold_Tile__c = 10;
        apSetting[0].Red_Threshold_Tile__c = 10;
        apSetting[0].Yellow_Threshold_Resilient__c = 10;
        apSetting[0].Red_Threshold_Resilient__c = 10;
        apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
        apSetting[0].Red_Threshold_Mainstreet__c = 10;
        update apSetting[0];
      
        List<Account_Profile__c> apList = new List<Account_Profile__c>();
        Account_Profile__c ap = new Account_Profile__c();
        ap.Primary_Business__c = apSetting[0].Id;
        ap.Annual_Retail_Sales__c = 300000;
        ap.Annual_Retail_Sales_Non_Flooring__c = 10;
        ap.Chain_Number__c = 'R.197101';
        ap.Two_Sales_Teams__c=true;
        apList.add(ap);        
        insert apList;
        
        List<Territory__c> terrList = Utility_Test.createTerritories(false, 1);
        for(Integer i=0;i<terrList.size();i++){
            terrList[i].Name = 'Testterr'+i;
            terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
        }

        //List<Account> acList = Utility_Test.createAccounts(true, 1);
        List<Account> resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
        for(Integer i=0;i<resAccListForInvoicing.size();i++){
            resAccListForInvoicing[i].Business_Type__c = 'Residential';
            resAccListForInvoicing[i].Chain_Number__c = 'R.197106';
            resAccListForInvoicing[i].Account_Profile__c = ap.Id;
        }
        insert resAccListForInvoicing;

        List<Mohawk_Account_Team__c> mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER);
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].Product_Types__c = 'Carpet';
            mohAccTeam[i].Brands__c = 'Aladdin';
            mohAccTeam[i].BMF_Team_Member__c = false;
            mohAccTeam[i].account_profile__c = ap.id;
        }
        
        insert mohAccTeam;
        system.assert( mohAccTeam.size() > 0 );
        test.startTest();    
            MATMultiteamCalc_Batch  cls = new MATMultiteamCalc_Batch ();
            String jobId = database.executeBatch(cls);
            System.assert(jobId != null);
            
            String sch = '0 0 23 * * ?';
            system.schedule('MATMultiteamCalc_Batch_Test', sch, cls);
        test.stopTest();

    }
    
}