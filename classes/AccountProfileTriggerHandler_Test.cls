@isTest
private class AccountProfileTriggerHandler_Test {
    
    @testSetup static void setup() {
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
        apSetting[0].Red_Threshold_Carpet__c = 10;
        apSetting[0].Yellow_Threshold_Cushion__c = 10;
        apSetting[0].Red_Threshold_Cushion__c = 10;
        apSetting[0].Yellow_Threshold_Hardwood__c = 10;
        apSetting[0].Red_Threshold_Hardwood__c = 10; 
        apSetting[0].Yellow_Threshold_Laminate__c = 10; 
        apSetting[0].Red_Threshold_Laminate__c = 10;
        apSetting[0].Yellow_Threshold_Tile__c = 10;
        apSetting[0].Red_Threshold_Tile__c = 10;
        apSetting[0].Yellow_Threshold_Resilient__c = 10;
        apSetting[0].Red_Threshold_Resilient__c = 10;
        apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
        apSetting[0].Red_Threshold_Mainstreet__c = 10;
        apSetting[0].Multi_Family_Business_Split_2Team__c =10;
        apSetting[0].Retail_Business_Split_2Team__c = 10;
        apSetting[0].Retail_Business_Split_1Team__c = 10;
        apSetting[0].Builder_Business_Split_2_Team__c = 10;
        apSetting[0].Builder_Business_Split_1Team__c = 10;
        apSetting[0].Multi_Family_Business_Split_1team__c = 10;
        apSetting[0].Retail_Multi_channel_split__c =100;
        apSetting[0].Builder_Multi_Family_Multi_Channel_Split__c  =100;

        update apSetting[0];
        
        List<Account_Profile__c> apList = new List<Account_Profile__c>();
        Account_Profile__c ap = new Account_Profile__c();
        ap.Name = 'TEST ACCOUNT PROFILE TEST CLASS';
        ap.Primary_Business__c = apSetting[0].Id;
        ap.Annual_Retail_Sales__c = 300000;
        ap.Annual_Retail_Sales_Non_Flooring__c = 10;
        
        ap.EWP_Total__c=10;
        ap.AMP_Total__c=10;
        ap.EWP_Total__c=10;
        apList.add(ap);
        insert apList;
        system.assert(apList.size()>0);
    }
    
    @isTest
    private static void testupdateAccountProfileSingle() {
        Account_Profile__c ap = [ SELECT R_ERS_RC__c,R_ERS_AC__c,B_ERS_RC__c,B_ERS_AC__c FROM Account_Profile__c WHERE Name =: 'TEST ACCOUNT PROFILE TEST CLASS' LIMIT 1 ];
        ap.Multi_Channel__c = false;
        ap.R_ERS_RC__c = 10;
        ap.R_ERS_AC__c = 10;
        ap.B_ERS_RC__c = 10;
        ap.B_ERS_AC__c = 10;
        
        ap.R_ERS_Car__c = 10;
        ap.B_ERS_Car__c = 10;
        ap.retail__c = 2; 
        ap.builder__c = 1;
        ap.Multi_Family__c = 1;
        update ap;
        system.assert(ap!=null);

    }
    
    @isTest
    private static void testupdateAccountProfileMulti() {
        Account_Profile__c ap = [ SELECT R_ERS_RC__c,R_ERS_AC__c,B_ERS_RC__c,B_ERS_AC__c FROM Account_Profile__c WHERE Name =: 'TEST ACCOUNT PROFILE TEST CLASS' LIMIT 1 ];
        ap.Multi_Channel__c = true;
        ap.R_ERS_RC__c = 10;
        ap.R_ERS_AC__c = 10;
        ap.B_ERS_RC__c = 10;
        ap.B_ERS_AC__c = 10;
        
        ap.R_ERS_Car__c = 10;
        ap.B_ERS_Car__c = 10;
        ap.retail__c = 2; 
        ap.builder__c = 1;
        ap.Multi_Family__c = 1;
        update ap;
        system.assert(ap!=null);

        AccountProfileTriggerHandler.BIDataCalculation(ap);
    }
    
    
}