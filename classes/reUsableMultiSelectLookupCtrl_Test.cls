/**************************************************************************

Name : reUsableMultiSelectLookupCtrl_Test

===========================================================================
Purpose : Test Class for reUsableMultiSelectLookupCtrl
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha       12/June/2018     Created 
***************************************************************************/
@isTest
private class reUsableMultiSelectLookupCtrl_Test { 
  
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            List<Account> accList=Utility_Test.createCommercialAccountsForInvoicing(true,1);

            List<Contact> contactList=Utility_Test.createContacts(true,1,accList);
            reUsableMultiSelectLookupCtrl.fetchLookUpValues('Test','Contact',contactList,'AccountId',string.valueOf(accList[0].id));
            test.stopTest();  
        }
    }
    
}