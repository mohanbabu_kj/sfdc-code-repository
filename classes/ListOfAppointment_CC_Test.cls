/**************************************************************************

Name : ListOfAppointment_CC_Test 

===========================================================================
Purpose : This tess class is used for ListOfAppointments_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         22/Feb/2017     Created 
2.0        Lester         23/March/2017   Modified      ListOfAppointments_CC are rewritten 
***************************************************************************/
@isTest
private class ListOfAppointment_CC_Test {
    static List<AccountTeamMember> accTeamMember;
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        
    }
    
    static testMethod void testMethod1() {
        init();
        test.startTest();
        System.runAs(Utility_Test.ADMIN_USER){
            List<Account> accList  =  Utility_Test.createAccounts(true, 1);
            accList[0].Market_Segments__c = ' Hospitality';
            update accList[0];
            Contact con = new Contact(LastName = 'Test Contact 1', AccountId = accList[0].id
                                      , Email='test@test.com', MobilePhone='1234567890', RecordTypeId= Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Residential').getRecordTypeId());
            
            List<AccountTeamMember> atlist = Utility_Test.createAccountTeamMember(true, 1,  accList,Utility_Test.ADMIN_USER );
           user u=Utility_Test.getTestUser('TestUs','Residential Sales User');
            u.ManagerId=Utility_Test.RESIDENTIAL_SALES_MANAGER_USER.Id;
            insert u;
            List<Task> tasks = new List<Task>();
            
           Event E = new Event();
            E.Type = 'Email';
            E.DurationInMinutes = 56;
            E.Description = '';
            E.OwnerId = UserInfo.getUserId(); 
            E.WhatId = accList[0].id;
            E.ActivityDateTime = System.today();
             insert E;
            
          
            ListOfAppointments_CC.subordinatesUsers(u.ManagerId);
            insert con;
            system.assert(con!=null);
            
            Id contactId = con.Id;
            ListOfAppointments_CC.getAccountId(contactId);
            
            accTeamMember =  Utility_Test.createAccountTeamMember(true, 1, accList, Utility_Test.ADMIN_USER);
            ListOfAppointments_CC.getAccountTeam(contactId);
            ListOfAppointments_CC.getAccountAppointmentUsers(accList[0].id);
            ListOfAppointments_CC.subUsersWithInAcTeam(contactId);
            ListOfAppointments_CC.getAccountAppointmentByUsers(accList[0].id, '','');
            ListOfAppointments_CC.getAccountAppointmentByUsers(accList[0].id, UtilityCls.MYTEAMACTIVITIES,'');
            ListOfAppointments_CC.allActivity(accList[0].id);
            ListOfAppointments_CC.getAppointmentType();
            System.assert(accTeamMember != null);
        }
        test.stopTest();
    }
    
}