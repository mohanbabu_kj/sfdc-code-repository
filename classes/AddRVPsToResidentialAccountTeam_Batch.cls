/**************************************************************************

Name : AddRVPsToResidentialAccountTeam_Batch

===========================================================================
Purpose : Batch class to Add all RVP, SVP, DM role users to Account Team based on User's territories on each Mohawk Account Team.
          This is for Record types Residential-Invoicing, Residential Non-Invoicing
===========================================================================
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Anand           16/May/2017        Created          CSR:


***************************************************************************/
public class AddRVPsToResidentialAccountTeam_Batch{
/* global class AddRVPsToResidentialAccountTeam_Batch implements Database.Batchable<sObject> {
    public String query = 'SELECT Id, User__c,Account__c, Role__c,RecordTypeId, Recordtype.Name FROM Mohawk_Account_Team__c '+
                           'WHERE createddate = TODAY AND (Recordtype.DeveloperName = \'Residential_Invoicing\' OR Recordtype.DeveloperName = \'Residential_Non_Invoicing\')';   
   global AddRVPsToResidentialAccountTeam_Batch() {
        this.query = query;

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('*****query****'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Mohawk_Account_Team__c> scope) {
       
       Map<Id,Set<Id>>  userTerMap = new Map<Id,Set<Id>>();
       Set<Id>  userIdset = new Set<Id>();
       Set<Id>  terIdset = new Set<Id>();
       for(Mohawk_Account_Team__c mhk : scope){
          userIdSet.add(mhk.User__c);
       }   
       
       //one user may exist in multiple territories
       for(Territory_User__c tUser : [Select Id, User__c, Territory__c FROM Territory_User__c WHERE User__c in : userIdSet]){
           if(userTerMap.containsKey(tUser.User__c)){
               userTerMap.get(tUser.User__c).add(tUser.Territory__c);
           
           }else{
            Set<Id> terIds = new Set<Id>();
            terIds.add(tUser.Territory__c);
            userTerMap.put(tUser.User__c, terIds);
           }
           // Add Territory Ids to List
           terIdset.add(tUser.Territory__c);
       }
         system.debug('****userTerMap**'+userTerMap);
         system.debug('****terIdset**'+terIdset);
  
  
        
    Set<String> resiRolesSet = UtilityCls.resiInvoicingRoles_CS();
        system.debug('resiRolesSet>>>>>>>'+resiRolesSet);
    Map<Id, List<Territory_User__c>> superiorTerUsrMap = new Map<Id, List<Territory_User__c>>();   
     //Adding all RVP, SVP, DM role users to Account Team based on territory on each Mohawk Account Team.
    //This query retrieves only users with roles RVP, SVP, DM roles and user should not be part of mohawk Ac team(inorder to avoid duplicate users adding in account team)

    for(Territory__c ter : [SELECT Id, Name, 
                              (SELECT Id, User__c ,Role__c FROM Territory_Users__r WHERE Role__c in: resiRolesSet AND user__c NOT IN: userIdSet)
                              FROM Territory__c where Id in: terIdset]){   
                                           
          System.debug('@@@@@TERRR INNNER ter@@'+ter);
         if(ter.Territory_Users__r.size()>0){
           // system.debug('Entry superiorTerUsrMap ter.Territory_Users__r>>>>>>>>'+ter.Territory_Users__r);

             superiorTerUsrMap.put(ter.Id, ter.Territory_Users__r);
          }
    }

    Map<Id, list<Territory_User__c>> UsersMap = new  Map<Id, list<Territory_User__c>>();
    //Intialize all territory superiors to Map along with current user Id
    for(Id uId : userIdSet){
        if(userTerMap.containsKey(uId) && userTerMap.get(uId) != null){
            
            List<Territory_User__c> terUserList = new List<Territory_User__c>();
            for(Id terId : userTerMap.get(uId)){
                if(superiorTerUsrMap.containsKey(terId)){
                terUserList.addAll(superiorTerUsrMap.get(terId));
            }
            }
            system.debug('terUserList>>>>>>>>'+teruserList);
            UsersMap.put(uid, teruserList);
        }
    }
    System.debug('****superiorTerUsrMap**'+superiorTerUsrMap);

    /*********intialize account team for each mohawk account team user**************/
    /*
    List<Mohawk_Account_Team__c> mhkList = new List<Mohawk_Account_Team__c>();
    List<AccountTeamMember> acTeamMemList = new List<AccountTeamMember>();
    Default_Configuration__c  defaultConfig =  Default_Configuration__c.getOrgDefaults(); 
    for(Mohawk_Account_Team__c mhk : scope){

        if(UsersMap.containsKey(mhk.User__c) && usersMap.get(mhk.User__c) != null){
             for(Territory_User__c tu : usersMap.get(mhk.User__c)){
                if(mhk.Role__c == UtilityCls.DISTRICT_MANAGER){
                    Mohawk_Account_Team__c ca = new Mohawk_Account_Team__c();
                    ca.Account__c        = mhk.Account__c;
                    ca.Role__c           = mhk.Role__c;
                    ca.User__c           = tu.User__c;
                    ca.Account_Access__c = defaultConfig.Contact_Access__c;  
                    ca.RecordTypeId      = mhk.RecordTypeId;
                    mhkList.add(ca);
                }else{
                    AccountTeamMember atm = new AccountTeamMember();
                    atm.AccountId      = mhk.Account__c;
                    atm.TeamMemberRole = mhk.Role__c;
                    atm.UserId         = tu.User__c;  
                    atm.AccountAccessLevel = defaultConfig.Account_Team_Access__c;
                    atm.ContactAccessLevel = defaultConfig.Contact_Access__c;
                    atm.OpportunityAccessLevel = defaultConfig.Opportunity_Team_Access__c;        
                    acTeamMemList.add(atm);    
                         
                }
            }
        }
    }//End of for

    try{
       Insert mhkList;   
       Insert acTeamMemList; 
       System.debug('****mhkList**'+mhkList);
       System.debug('****acTeamMemList**'+acTeamMemList);    
    }Catch(Exception e){
        System.debug('ERROR In Inserting Mohawk Account Team records !!!' +e);
    }

}//End of Execute method */

    /*global void execute(Database.SchedulableContext SC) { 
        id batchId = database.executeBatch(new AddRVPsToResidentialAccountTeam_Batch());
    }*/
    
    /*

    global void finish(Database.BatchableContext BC) { 

    }  */
}