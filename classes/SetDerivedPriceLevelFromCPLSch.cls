/*

    for sobject a:
    SetDerivedPriceLevelFromCPLSch lsu = new SetDerivedPriceLevelFromCPLSch('a');
    String cron30 = '0 20 * * * ?'; // for every 20 minutes
    String jobID = system.schedule('A object update Job', cron30, lsu);

*/
global with sharing class SetDerivedPriceLevelFromCPLSch implements Schedulable{
    global final String sObjectType;
    
    global SetDerivedPriceLevelFromCPLSch(String s){
        sObjectType = s;
    }
    
    global void execute(SchedulableContext sc) {        
        Map<String,CPL_Update_Batch_Setting__c> cplUpdateSettingMap = CPL_Update_Batch_Setting__c.getAll();
        final Integer batchSize = Integer.valueOf(cplUpdateSettingMap.get(sObjectType).Batch_size__c);
        final String sObjectType = String.valueOf(cplUpdateSettingMap.get(sObjectType).sObject_Type__c);
        SetDerivedPriceLevelFromCPL cplBatch = new SetDerivedPriceLevelFromCPL(sObjectType); 
        database.executebatch(cplBatch, batchSize);
    }
}