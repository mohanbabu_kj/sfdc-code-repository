/**************************************************************************
Name : MATDeletefromCAMS_Batch 
===========================================================================
Purpose : Batch class to Delete the Mohawk Account Teams without TM numbers
.
		  Fetches records from Last Run time and upto now minus Delay time set in settings
		  To delete all Open MAT Records- run wihtout Custom Settings.     		
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Suresh       28/11/2018          Created          CSR:

***************************************************************************/
global class MATDeletefromCAMS_Batch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    global static string ErrorRecords;
    global MAT_Batch_Setting__c QueryList;
    global DateTime fetchRecordsUpto;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        String RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId();
        List<MAT_Batch_Setting__c> batchSettingList = new List<MAT_Batch_Setting__c>( [ SELECT id,Batch_Query__c,Last_Run_At__c,Batch_Size__c,MinutesDelay__c FROM MAT_Batch_Setting__c WHERE NAME = 'MATDeletefromCAMS_Batch' LIMIT 1 ] );
        System.debug('$$$$$ Custom Setting' + batchSettingList);
        if( batchSettingList != null && BatchSettingList.size() > 0){
            QueryList = batchSettingList[0];
        }
            if( QueryList != null && QueryList.Last_Run_At__c != null ){
            DateTime lastRunTime = QueryList.Last_Run_At__c;
           // fetchRecordsUpto = System.now().addminutes(-10);     
            fetchRecordsUpto = (batchSettingList[0].MinutesDelay__c !=null) ? System.now().addMinutes(Integer.valueof(batchSettingList[0].MinutesDelay__c )) : System.now().addminutes(-10) ;    
            System.debug('Last Run ####' +lastRunTime);
            System.debug('Run upto @@@@@' +fetchRecordsUpto);    
            query = QueryList.Batch_Query__c  + ' AND LastModifiedDate > : lastRunTime AND LastModifiedDate < : fetchRecordsUpto ORDER BY Chain_Number_UserId__c ASC ';
            
        }else{
           query = 'SELECT id,name, Territory_User_Number__c from Mohawk_Account_Team__c where User__c != null and recordtype.name = \''+ 'Residential Invoicing' + '\'  and  ( Territory_User_Number__c = null OR Territory_User_Number__c = \'' + '\')';
                    }
        
    /*    Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'MATDeletefromCAMS_Batch'];
        If(QureyList.size() > 0)
            query = QureyList[0].Query_String__c ;
        else         
            query = 'SELECT id,name, Territory_User_Number__c from Mohawk_Account_Team__c where User__c != null and recordtype.name = \''+ 'Residential Invoicing' + '\'  and  ( Territory_User_Number__c = null OR Territory_User_Number__c = \'' + '\')';
      */  
        system.debug('$$$ Query -' + query );
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<Mohawk_Account_Team__c> scope) {
            list<Mohawk_Account_Team__c>  delMATList = scope;
            Database.DeleteResult[] results = Database.delete(delMATList, false);
            for ( Integer i = 0; i < results.size(); i++ ) {
                if ( !results.get(i).isSuccess() || test.isrunningtest() ) {
                    if ( ErrorRecords == null ) {
                        ErrorRecords = 'Operation, Object Name, Record Id, Name, Status Code, Error Message \n';
                    }
                    for ( Database.Error theError : results.get(i).getErrors() ) {
                        string recordString = '"'+'Delete'+'","'+'MAT Delete'+ '","' +delMATList.get(i).Id+'","' + '","' +theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                        ErrorRecords = ErrorRecords + recordString;                         
                    }
                }
            } 
        
    }
    
    global void finish(Database.BatchableContext BC) {
        if (ErrorRecords != null) {
            NotificationUtility.sendBatchExceptionNotification(bc.getJobId(),ErrorRecords,'MAT Delete Error Records');
        	}
        if( QueryList != null ){
            QueryList.Last_Run_At__c = fetchRecordsUpto;
            update QueryList;
        	}
        }
    
    global void execute(SchedulableContext SC){
        database.executeBatch(new MATDeletefromCAMS_Batch());
    }
}