/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MAMD_ReoptimizationBatch implements Database.Batchable<SObject> {
    global MAMD_ReoptimizationBatch() {

    }
    global void execute(Database.BatchableContext BC, List<mamd__Multiday_Route_Template_User__c> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
