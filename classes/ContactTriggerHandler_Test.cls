/**************************************************************************

Name : ContactTriggerHandler_Test
===========================================================================
Purpose : Test class for ContactTriggerHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
1.0        Susmitha      07/nov/2017       Create  
***************************************************************************/
@isTest
public class ContactTriggerHandler_Test {
    
    static testMethod void  testMethod1(){
        System.runAs(Utility_Test.ADMIN_USER){
            Utility_Test.getDefaultConfCusSetting();
            List<Account> accList = Utility_Test.createAccountsForNonInvoicing(true, 1);
            List<Contact> contList = Utility_Test.createContacts(true, 1, accList);
            List<Contact> contList1 = Utility_Test.createContacts(true, 1);
            update contList1;
            system.assert(contList1.size()>0);
            ContactTriggerHandler.updateContact(contList,contList,FALSE,TRUE);
            
           
        }
    }
}