/**************************************************************************

Name : OpportunityLineItemTriggerHandler_Test
===========================================================================
Purpose : Test class for OpportunityLineItemTriggerHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
1.0        Susmitha      07/nov/2017       Create  
***************************************************************************/
@isTest
public class OpportunityLineItemTriggerHandler_Test {
    
    static testMethod void  testMethod1(){
        // Set up some local variables
        String opportunityName = 'My Opportunity';
        String standardPriceBookId = '';

        // Create a custom price book
        List<Pricebook2> pb2Standard = Utility_Test.createPricebook2(false,1);
        pb2Standard[0].Name = UtilityCls.Std_Price_Book;
        insert pb2Standard;
        system.assert(pb2Standard!=null);
        standardPriceBookId = pb2Standard[0].Id;
        
        // set up opp and Verify that the results are as expected.
        List<Opportunity>oppList = Utility_Test.createOpportunities(false,1);
        oppList[0].AccountId = null;
        oppList[0].Name = opportunityName;
        oppList[0].StageName = 'Bidding';
        oppList[0].CloseDate = Date.today();
        insert oppList;
        system.assert(oppList.size()>0);
        Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :oppList[0].Id];
        System.assertEquals(opportunityName, opp.Name);

        // set up product2 and Verify that the results are as expected.
        List<Product2>prod2List = Utility_Test.createProduct2(false, 1);
        prod2List[0].Name = 'Test Product';
        prod2List[0].isActive = true;
        prod2List[0].CAMS_Product_Number__c = 'test';
        prod2List[0].CAMS_Alt_Product_Number_2__c = 'test';
        prod2List[0].CAMS_Alt_Product_Number_3__c = 'test';
        prod2List[0].CAMS_Alt_Product_Number_4__c = 'test';     
        insert prod2List;
        
        Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :prod2List[0].Id];
        System.assertEquals('Test Product', p2ex.Name);

        // set up PricebookEntry and Verify that the results are as expected.
        List<PricebookEntry>pbeList = Utility_Test.createPricebookEntry(false,1,prod2List,standardPriceBookId);
        pbeList[0].CurrencyIsoCode = 'USD';
        pbeList[0].UnitPrice = 99;
        insert pbeList;
        
        PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbeList[0].Id];
        System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);
        
        // set up OpportunityLineItem and Verify that the results are as expected.
        OpportunityLineItem oli = new OpportunityLineItem(
            PriceBookEntryId=pbeList[0].Id,
            OpportunityId=oppList[0].Id,
            Quantity=1,
            TotalPrice=99,
            Updated_By_ESB__c = true,
            LDS_PB_Mapping_ID__c = 'test'
        );
        insert oli;
        system.assert(oli.id!=null);
        
        OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
        System.assertEquals(pbeList[0].Id, oliex.PriceBookEntryId);
        
        delete oli;
        
        
    }
}