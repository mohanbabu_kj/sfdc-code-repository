/**************************************************************************

Name : PaymentTermsCalloutMockDataEmpty 

===========================================================================
Purpose :  the JSON format for OFREIGHTDATA is empty
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        14/Feb/2017     Created 
***************************************************************************/
@isTest
global class PaymentTermsCalloutMockDataEmpty_Test implements HttpCalloutMock  {

     global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody(+'{'
                          +'"MAIN": {'
                                +'"IPACCOUNT": "R.018129.0000",'
                                +'"OACCOUNTNAME": "J A Lotourneau & Fils Inc",'
                                +'"ODATETIME": "*February 10 - 2017, 01:47pm EST*",'
                                +'"OMESSAGE": null,'
                                +'"OMESSAGE": "If you have a question regarding Terms, please contact Mohawk Financial Services at (800) 427-4900.",'
                                +'"OFREIGHTDATA": ['
                                +']'
                            +'}'
                        +'}');

           return res;
      }//end of Method
}//End of Class