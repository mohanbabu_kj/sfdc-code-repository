/**************************************************************************

Name : LightningController

===========================================================================
Purpose : This class is used for the  List of Appoinements lightning components (Story 30720) 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        22/Feb/2017    Created    
2.0        Anand         23/Feb/2017    Modified                       
***************************************************************************/
public without sharing class ListOfAppointments_CC 
{
    
    public static String currentUserId;
    
    static{
        currentUserId = (String)Userinfo.getuserId();
    }
    
    // 03/22/2017 get accountId from Contact if recordId is Contact record
    public static Id getAccountId(Id recordId) {
        String objectName = UtilityCls.getObjectNameById(recordId);
        if(objectName == 'Contact'){
            List<Contact> result = [SELECT Id, AccountId FROM Contact WHERE Id =: recordId];
            return result[0].AccountId;
        } else
            return recordId;
    }
    
    public static List<Mohawk_Account_Team__c> getAccountTeam(String recordId){
        
        Id accountId = getAccountId(recordId);
        
        /*return [SELECT Id, UserId, User.name, TeamMemberRole FROM AccountTeamMember
Where AccountId =: accountId];*/
        //Added by MB - 4/2/18 - To get from Mohawk Account team - Also updated in all places that the method is accessed - Bug 58008
        
        return [SELECT Id, User__c, User__r.Name, Role__c FROM Mohawk_Account_Team__c
                WHERE Account__c =: accountId ];
    }
    
    /*Modified BY Lillian 7/3/2017  Get Log In User TeamMemberRole*/
    //Modified by MB - 4/2/18 - To get from Mohawk Account team - Also updated in all places that the method is accessed - Bug 58008
    public static Mohawk_Account_Team__c getCurrentMember(String recordId){
        Id accountId = getAccountId(recordId);
        Mohawk_Account_Team__c accTeamMem = new Mohawk_Account_Team__c();
        for(Mohawk_Account_Team__c atm : getAccountTeam(accountId)){
            
            if(atm.User__c == currentUserId){
                accTeamMem = atm;
            }
        }
        return accTeamMem;
    }
    
    /*
* Method to get the list of Users of current account 
*/
    @AuraEnabled
    public static List<AccountAppointmentUsers> getAccountAppointmentUsers(String recordId){
        Id accountId = getAccountId(recordId);
        
        System.debug('getAccountAppointmentUsers : accountId :' + accountId);
        
        /*Modified BY Lillian 7/3/2017   Add Team Role Condition*/
        Mohawk_Account_Team__c currentMember = getCurrentMember(recordId);
        System.debug(LoggingLevel.INFO, '*** currentMember: ' + currentMember);
        User loggerInUser=[Select ID,profileid,userRole.Name from User Where Id=:userInfo.getUserId() limit 1];
        
        String loggedInUserRole='';
        loggedInUserRole=loggerInUser.userRole.Name;
        system.debug('User role ::::::'+loggedInUserRole);
        //String profileName = ''; 
        String profileName = currentMember.Role__c;
        System.debug(LoggingLevel.INFO, '*** profileName: ' + profileName);
        boolean allteam = false;
        boolean myteam = false;
        boolean myact = false;
        system.debug(profileName);
        //if(profileName.contains(UtilityCls.RESIDENTIAL)){
        if(profileName == UtilityCls.ROLE_NAME_TM || UtilityCls.getProfileName(loggerInUser.profileid).contains(UtilityCls.RESIDENTIAL)){
            myact = true; allteam = false; myteam = false;
        }else {
            myact = false; allteam = false; myteam = true;
        }
        // }
        /*Modified BY Lillian 7/3/2017  */
        
        List<AccountAppointmentUsers> options = new List<AccountAppointmentUsers>();
        options.add(new AccountAppointmentUsers(currentUserId, 'My Activities', myact)); //Added By Nagendra - changed from UtilityCls.MYACTIVITIES to My Activities
        //if(loggedInUserRole!=null && loggedInUserRole!='' &&(loggedInUserRole.endsWith('DM') || loggedInUserRole.endsWith('RVP') ||loggedInUserRole.equals(utilityCls.RVP))){
        if(loggedInUserRole!=null && loggedInUserRole!='' && !loggedInUserRole.endsWith('TM')){ //Added by MB - Bug 60008 - 08/13/18
            options.add(new AccountAppointmentUsers(UtilityCls.MYTEAMACTIVITIES, UtilityCls.MYTEAMACTIVITIES, myteam)); 
            
        }
        
        options.add(new AccountAppointmentUsers(UtilityCls.ALLTEAMACTIVITIES, 'All Activities', allteam)); //Added By Nagendra - changed from UtilityCls.ALLTEAMACTIVITIES to All Activities   
        
        List<AccountAppointmentUsers> optionsUsers = new List<AccountAppointmentUsers>();
        
        for(Mohawk_Account_Team__c accTeamMember : getAccountTeam(accountId)){
            //Avoid displaying logged in user name in dropdown list
            //if(accTeamMember.User__c != currentUserId){
            //  options.add(new AccountAppointmentUsers((String)accTeamMember.User__c, accTeamMember.User__r.name, false));
            optionsUsers.add(new AccountAppointmentUsers((String)accTeamMember.User__c, accTeamMember.User__r.name, false));
            
            // }
        }
        optionsUsers.sort();
        options.addAll(optionsUsers);
        System.debug('getAccountAppointmentUsers : options : ' + optionsUsers );
        
        return options;
    }
    
    /*
* Method to get the list of Appointments based on given account Id and userId 
*/
    @AuraEnabled
    public static List<Event> getAccountAppointmentByUsers(String recordId, String userId,string type) {
        System.debug(LoggingLevel.INFO, '*** recordId: ' + recordId);
        System.debug(LoggingLevel.INFO, '*** userId: ' + userId);
        
        Id accountId = getAccountId(recordId);
        
        System.debug('getAccountAppointmentByUsers : accountId : ' + accountId + ', userId : ' + userId );
        
        List<Event> eventList = new List<Event>();
        
        // new dynamic soql query 03/22/2017
        String soql = 'SELECT Id, Subject, Status__c, Description, OwnerId, Owner.Name, WhoId, StartDateTime, EndDateTime, IsAllDayEvent FROM Event'
            + ' WHERE ((Account__c = \''+accountId+'\' OR AccountId = \''+accountId+'\' ) AND (StartDateTime = LAST_N_DAYS:365 OR StartDateTime > today))' ;
        /*Modifiec BY Lillian 7/3/2017  Initial table by login user teammemberrole  */
        if(userId == '' || userId == null){
            
            Mohawk_Account_Team__c currentMember = getCurrentMember(recordId);
            System.debug(LoggingLevel.INFO, '*** currentMember: ' + currentMember);
            String profileName = currentMember.Role__c;
            System.debug(LoggingLevel.INFO, '*** userId: ' + userId);
            
            if(profileName == UtilityCls.ROLE_NAME_TM){
                userId = currentUserId; //UtilityCls.MYACTIVITIES;
            }else{
                userId = UtilityCls.MYTEAMACTIVITIES;
            }
            System.debug(LoggingLevel.INFO, '*** userId: ' + userId);
            
        }
        
        if(userId == UtilityCls.ALLTEAMACTIVITIES) {
            soql = soql;
            
        } else if(userId == UtilityCls.MYTEAMACTIVITIES) {
            Set<Id> filterUserIds = subUsersWithInAcTeam(accountId);
            filterUserIds.add(currentUserId); //Add current logged in user Id
            String idStr = UtilityCls.getStringFormatForSetIdsIncludesSOQL(filterUserIds);
            soql += ' AND OwnerId in ' + idStr;
            
        } else {
            
            soql += ' AND OwnerId = \''+userId+'\'';
        }
        
        /*Modified BY Mudit 22/11/2018*/
        if( type != 'All' ){
            soql += ' AND Type = \''+type+'\'';
        }
        /*Modified BY Mudit 22/11/2018*/        
        
        System.debug(LoggingLevel.INFO, '*** soql: ' + soql);
        /*  if(sortOrder !='')
		soql += ' ORDER BY Owner.Name ' + sortOrder;*/
        soql += ' ORDER BY StartDateTime DESC';
        
        /*Modified BY Lillian 7/3/2017*/
        
        
        try{
            eventList = Database.query(soql);
        }Catch(Exception e){
            UtilityCls.createExceptionLog(e,  'ListOfAppointments_CC', 'getAccountAppointmentByUsers', 'logCode', '', 'referenceInfo');
            System.debug('Errot***'+e.getMessage());
        }
        // System.debug('result***'+eventList);
        
        return eventList; 
        // new dynamic soql query 03/22/2017
        
        /*if(userId == UtilityCls.ALLTEAMACTIVITIES || userId == '' || userId == null) {


eventList = [SELECT Id, Subject, Description, OwnerId, Owner.Name, WhoId, StartDateTime, EndDateTime, IsAllDayEvent FROM Event
WHERE AccountId =: accountId
ORDER BY Owner.Name ASC];

System.debug('getAccountAppointmentByUsers : eventList : ' + eventList );                                  

}else if(userId == UtilityCls.MYTEAMACTIVITIES || userId == '' || userId == null) {

Set<Id> filterUserIds = subUsersWithInAcTeam(accountId);
eventList = [SELECT Id, Subject, Description, OwnerId, Owner.Name, WhoId, StartDateTime, EndDateTime, IsAllDayEvent FROM Event
Where AccountId =: accountId AND 
OwnerId in : filterUserIds
ORDER BY Owner.Name ASC];

}
else {
eventList = [SELECT Id, Subject, Description, OwnerId, Owner.Name, WhoId, StartDateTime, EndDateTime, IsAllDayEvent FROM Event
Where AccountId =: accountId AND 
OwnerId =: userId];

System.debug('getAccountAppointmentByUsers : eventList : ' + eventList );

}  
return eventList;  */
    }//End Method
    
    //Recursive Method
    //Method to get the subordinate userIds if current User is Manager
    public static Set<Id> subordinatesUsers(String userId){
        
        Set<Id> userIds = new Set<Id>();
        // Manager filter lavel 1
        for(User  u : [SELECT Id FROM USER WHERE ManagerId =: userId]) { 
            
            userIds.add(u.Id);           
        }
        
        // Manager filter lavel 2 (Managers Manager)
        for(User  u : [SELECT Id FROM USER WHERE ManagerId in:userIds]) { 
            
            userIds.add(u.Id);           
        }
        
        return userIds;
    }//End of Method
    
    
    //Method to get the subordinate userIds with in Account Team
    public static Set<Id> subUsersWithInAcTeam(String recordId){
        
        Id accountId = getAccountId(recordId);
        
        Set<Id> subUserIds = new Set<Id>(); //subordinatesUsers(currentUserId);
        Set<Id> acTeamUserIds = new Set<Id>();
        Set<Id> terrIds = new Set<Id>();
        for(List<Territory_User__c> terrUserList : [SELECT Id, Territory__c FROM Territory_User__c
                                                    WHERE User__c =: UserInfo.getUserId()]){
                                                        for(Territory_User__c terrUser: terrUserList){
                                                            terrIds.add(terrUser.Territory__c);
                                                        }
                                                    }
        
        if(terrIds.size() > 0){
            for(List<Territory_User__c> terrUserList : [SELECT Id, User__c FROM Territory_User__c
                                                        WHERE Territory__c IN: terrIds and Role__c =: UtilityCls.ROLE_NAME_TM]){
                                                            for(Territory_User__c terrUser: terrUserList){
                                                                subUserIds.add(terrUser.User__c);
                                                            }
                                                        }
        }
        for(Mohawk_Account_Team__c uId : getAccountTeam(accountId)){
            
            if(subUserIds.contains(uid.User__c)){
                acTeamUserIds.add(uid.User__c);
            }
        }
        return acTeamUserIds;
    }
    
	@AuraEnabled
	public static List<activityWrapper>allActivity( String recordId ){
		List<activityWrapper>returnList = new List<activityWrapper>();
		
		Id accountId = getAccountId(recordId);
		String soql = 'SELECT Id,ownerId, Subject, StartDateTime,Status__c, Owner.Name FROM Event'+ ' WHERE  ((Account__c = \''+accountId+'\' OR AccountId = \''+accountId+'\' ) AND (StartDateTime = LAST_N_DAYS:365 OR StartDateTime > today) ) ORDER BY StartDateTime DESC';	//Added ORDER BY StartDateTime DESC -Nagendra 17 Jan 2019 	
		string soqlTsk = 'SELECT id,ownerId,Subject,ActivityDate,Status,Owner.Name,TaskSubtype FROM Task'+' WHERE AccountId = \''+accountId+'\' AND ( ActivityDate = LAST_N_DAYS:365 OR ActivityDate > today )';
		system.debug('::soql::' + soql);
        system.debug('::soqlTsk::' + soqlTsk);
        /*if( userId == '' || userId == null ){
            Mohawk_Account_Team__c currentMember = getCurrentMember( recordId );
            String profileName = currentMember.Role__c;
            if( profileName == UtilityCls.ROLE_NAME_TM ){
                userId = currentUserId; //UtilityCls.MYACTIVITIES;
            }else{
                userId = UtilityCls.MYTEAMACTIVITIES;
            }            
        }
		if(userId == UtilityCls.ALLTEAMACTIVITIES) {
            soql = soql;          
			soqlTsk = soqlTsk;	
        } else if(userId == UtilityCls.MYTEAMACTIVITIES) {
            Set<Id> filterUserIds = subUsersWithInAcTeam(accountId);
            filterUserIds.add(currentUserId); //Add current logged in user Id
            String idStr = UtilityCls.getStringFormatForSetIdsIncludesSOQL(filterUserIds);
            soql += ' AND OwnerId in ' + idStr;
			soqlTsk += ' AND OwnerId in ' + idStr;
            
        } else {            
            soql += ' AND OwnerId = \''+userId+'\'';
			soqlTsk += ' AND OwnerId = \''+userId+'\'';
		}*/
		
		for( Event Event : Database.query(soql) ){
            Datetime sdt = event.StartDateTime;
            Time t = sdt.time();
            String s = string.valueOf(t);
			returnList.add( new activityWrapper( Event.id,Event.Subject,date.valueof(Event.StartDateTime), s, Event.Status__c,Event.Owner.Name,'Appointment',Event.ownerId,String.valueOf(Event.StartDateTime))  );			
        }
		for( Task tsk : Database.query( soqlTsk ) ){
			returnList.add( new activityWrapper( tsk.id,tsk.Subject,tsk.ActivityDate,null, tsk.status,tsk.Owner.Name,tsk.TaskSubtype,tsk.ownerId,String.valueOf(tsk.ActivityDate) ) );
		}
		
		system.debug( '::::returnList:::::' + returnList );
		return returnList;
	}
	
    /** METHOD FOR Get Appointment Type - Mudit **/
    @AuraEnabled
    public static List<string>getAppointmentType(){
        List<string>returnList = new List<string>();
        Schema.DescribeFieldResult fieldResult = Event.Type.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            returnList.add( pickListVal.getLabel() );
        }
        return returnList;
    }
    
    // Wrapper class to store user id and its name for picklist selection
    public class AccountAppointmentUsers implements Comparable {
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public Boolean selected {get; set;}
        
        public AccountAppointmentUsers(String id ,  String name, boolean selected) { 
            this.id = id;
            this.name = name;    
            this.selected = selected;
        }
        
        public Integer compareTo(Object objToCompare) {
            //Sort by name Alphabetically
            
            return name.compareTo(((AccountAppointmentUsers)objToCompare).name);
            
            
        }
    }
    
    public class activityWrapper{
        @AuraEnabled
        public id id {get; set;}
        @AuraEnabled
        public String subject {get; set;}
        @AuraEnabled
		public date startDate {get; set;}
        @AuraEnabled
		public String startTime {get; set;}
		@AuraEnabled
		public string status {get; set;}
		@AuraEnabled
		public string ownerName {get; set;}
        @AuraEnabled
		public string type {get; set;}
        @AuraEnabled
		public string ownerId {get; set;}
        
        @AuraEnabled
		public String startDateTime {get; set;}
		
        public activityWrapper(id i,String s,date sd, String st, string stat, string o, string t,string ownerId,string startDateTime) { 
            this.id = i;
            this.subject = s;    
            this.startDate = sd;
            this.startTime = st;
            this.status = stat;    
            this.ownerName = o;
            this.type = t;
            this.ownerId = ownerId;
            this.startDateTime= startDateTime;
        }
    }
    
    @AuraEnabled
    public static boolean isCurrentUserResidentialUser (){
        
       if(UtilityCls.getProfileName(UserInfo.getProfileId()).contains(UtilityCls.RESIDENTIAL) ) 
         return true;
        else 
          return false;
        
        return false;
    }
    
}//End of Cls