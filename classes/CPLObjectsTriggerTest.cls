@isTest
public class CPLObjectsTriggerTest {
    
    public static String COMMERCIAL_BROADLOOM =     'Commercial Broadloom';
    public static String RESIDENTIAL_BROADLOOM =    'Residential Broadloom';
    public static String RESILIENT_SHEET =          'Resilient Sheet';
    public static String CARPET_TILE =              'Carpet Tile';
    public static String CUSHION =                  'Cushion';
    public static String SOLID_WOOD =               'Solid Wood';
    public static String TEC_WOOD =                 'Tec Wood';
    public static String REV_WOOD =                 'Rev Wood';
    public static String TILE =                     'Tile';
    public static String RESILIENT_TILE =           'Resilient Tile';
    public static String INSTALLATION_ACCESSORIES = 'Installation Accessories';
    public static String CARE_AND_MAINTENANCE =     'Care and Maintenance';
    

    
    
    @isTest 
    static void triggerTest() {
        

        List<User> listUser = New List<User>();
        listUser.add(createUserRecord('Residential Sales User'));
        System.Debug('### listUser: ' + listUser);
        insert listUser;
        
        List<Zone__c> listZones = createZoneRecords(1);
        System.Debug('### listZones: ' + listZones);
        insert listZones;
        
        List<Territory__c> listTerritory = createTerritoryRecords(1);
        listTerritory[0].Zone__c = listZones[0].id;
        System.Debug('### listTerritory: ' + listTerritory);
        insert listTerritory;
        
        List<Territory_User__c> listTerritoryUser = createTerritoryUserRecords(1);
        listTerritoryUser[0].User__c = listUser[0].Id;
        listTerritoryUser[0].Territory__c = listTerritory[0].id;
        System.Debug('### listTerritoryUser: ' + listTerritoryUser);
        insert listTerritoryUser;        
        
        List<Product2> listProducts = New List<Product2>();
        listProducts.add(createProductRecords(1,0,'FLOORINGNACARPETPRODUCT',COMMERCIAL_BROADLOOM)[0]);
        listProducts.add(createProductRecords(1,1,'FLOORINGNACARPETPRODUCT',RESIDENTIAL_BROADLOOM)[0]);
        listProducts.add(createProductRecords(1,8,'FLOORINGNARESILIENTPRODUCT',RESILIENT_SHEET)[0]);
        listProducts.add(createProductRecords(1,2,'FLOORINGNACARPETPRODUCT',CARPET_TILE)[0]);
        listProducts.add(createProductRecords(1,3,'FLOORINGNACUSHIONPRODUCT',CUSHION)[0]);
        listProducts.add(createProductRecords(1,4,'FLOORINGNAHARDWOODPRODUCT',SOLID_WOOD)[0]);
        listProducts.add(createProductRecords(1,5,'FLOORINGNAHARDWOODPRODUCT',TEC_WOOD)[0]);
        listProducts.add(createProductRecords(1,6,'FLOORINGNAHARDWOODPRODUCT',REV_WOOD)[0]);
        listProducts.add(createProductRecords(1,7,'FLOORINGNATILEPRODUCT',TILE)[0]);
        listProducts.add(createProductRecords(1,9,'FLOORINGNARESILIENTPRODUCT',RESILIENT_TILE)[0]);
        listProducts.add(createProductRecords(1,10,'FLOORINGNAINSTALLATIONPRODUCT',INSTALLATION_ACCESSORIES)[0]);
        listProducts.add(createProductRecords(1,11,'FLOORINGNACAREMAINTENANCEPRODUCT',CARE_AND_MAINTENANCE)[0]);
        System.Debug('### listProducts: ' + listProducts);
        insert listProducts;
        
        Id invoicingRT=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();
        Account account =new Account(Name='Test Account', RecordTypeId = invoicingRT, CAMS_Account_Number__c = 'R.123456.0000', Chain_Number__c ='R.123456', Customer_Group_Number__c='900011');
        insert account;
            
        Account_Product_Territory_Relationship__c accPTR = createAccountRelationship(account, '0', 'R0', 'D0', 'TM0');
        System.Debug('### accPTR: ' + accPTR);

        
        List<CPL_Sales__c> cplSales = new List<CPL_Sales__c>();
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID00', COMMERCIAL_BROADLOOM, listProducts[0], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID01', RESIDENTIAL_BROADLOOM, listProducts[1], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID02', RESILIENT_SHEET, listProducts[2], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID03', CARPET_TILE, listProducts[3], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID04', CUSHION, listProducts[4], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID05', SOLID_WOOD, listProducts[5], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID06', TEC_WOOD, listProducts[6], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID07', REV_WOOD, listProducts[7], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID08', TILE, listProducts[8], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID09', RESILIENT_TILE, listProducts[9], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID10', INSTALLATION_ACCESSORIES, listProducts[10], account));
        cplSales.add((CPL_Sales__c)setSalesFields(New CPL_Sales__c(), 'EXTID11', CARE_AND_MAINTENANCE, listProducts[11], account));
        insert cplSales;
        
        System.Debug('inserted sales records: ' + cplSales);
                
        Test.startTest();
        
        List<CPL_Commercial_Broadloom__c>       listCB = New List<CPL_Commercial_Broadloom__c>();
        List<CPL_Residential_Broadloom__c>      listRB = New List<CPL_Residential_Broadloom__c>();
        List<CPL_Resilient_Sheet__c>            listRS = New List<CPL_Resilient_Sheet__c>();
        List<CPL_Carpet_Tile__c>                listCT = New List<CPL_Carpet_Tile__c>();
        List<CPL_Cushion__c>                    listC = New List<CPL_Cushion__c>();
        List<CPL_Solid_Wood__c>                 listSW = New List<CPL_Solid_Wood__c>();
        List<CPL_Tec_Wood__c>                   listTW = New List<CPL_Tec_Wood__c>();
        List<CPL_Rev_Wood__c>                   listRW = New List<CPL_Rev_Wood__c>();
        List<CPL_Tile__c>                       listT = New List<CPL_Tile__c>();
        List<CPL_Resilient_Tile__c>             listRT = New List<CPL_Resilient_Tile__c>();
        List<CPL_Installation_Accessories__c>   listIA = New List<CPL_Installation_Accessories__c>();
        List<CPL_Care_and_Maintenance__c>       listCM = New List<CPL_Care_and_Maintenance__c>();
        
        listCB.add((CPL_Commercial_Broadloom__c)setCPLFields(New CPL_Commercial_Broadloom__c(),'EXTID00', listProducts[0],account.Id,true, accPTR.Id, 'Greater_than_TM1'));
        listCB.add((CPL_Commercial_Broadloom__c)setCPLFields(New CPL_Commercial_Broadloom__c(),'EXTID00', listProducts[0],account.Id,true, accPTR.Id, 'TM1'));
        listCB.add((CPL_Commercial_Broadloom__c)setCPLFields(New CPL_Commercial_Broadloom__c(),'EXTID00', listProducts[0],account.Id,true, accPTR.Id, 'TM2'));
        listCB.add((CPL_Commercial_Broadloom__c)setCPLFields(New CPL_Commercial_Broadloom__c(),'EXTID00', listProducts[0],account.Id,true, accPTR.Id, 'Less_than_RVP'));
       
        
        listRB.add((CPL_Residential_Broadloom__c)setCPLFields(New CPL_Residential_Broadloom__c(),'EXTID01', listProducts[1],account.Id,true, accPTR.Id, 'Greater_than_TM1'));
        listRB.add((CPL_Residential_Broadloom__c)setCPLFields(New CPL_Residential_Broadloom__c(),'EXTID01', listProducts[1],account.Id,true, accPTR.Id, 'TM1'));
        listRB.add((CPL_Residential_Broadloom__c)setCPLFields(New CPL_Residential_Broadloom__c(),'EXTID01', listProducts[1],account.Id,true, accPTR.Id, 'TM2'));
        listRB.add((CPL_Residential_Broadloom__c)setCPLFields(New CPL_Residential_Broadloom__c(),'EXTID01', listProducts[1],account.Id,true, accPTR.Id, 'Less_than_RVP'));
        
        listRS.add((CPL_Resilient_Sheet__c)setCPLFields(New CPL_Resilient_Sheet__c(),'EXTID02', listProducts[2],account.Id,true, accPTR.Id, 'Greater_than_TM1'));
        listRS.add((CPL_Resilient_Sheet__c)setCPLFields(New CPL_Resilient_Sheet__c(),'EXTID02', listProducts[2],account.Id,true, accPTR.Id, 'TM1'));
        listRS.add((CPL_Resilient_Sheet__c)setCPLFields(New CPL_Resilient_Sheet__c(),'EXTID02', listProducts[2],account.Id,true, accPTR.Id, 'TM2'));
        listRS.add((CPL_Resilient_Sheet__c)setCPLFields(New CPL_Resilient_Sheet__c(),'EXTID02', listProducts[2],account.Id,true, accPTR.Id, 'Less_than_RVP'));
        
        listCT.add((CPL_Carpet_Tile__c)setCPLFields(New CPL_Carpet_Tile__c(),'EXTID03', listProducts[3],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listCT.add((CPL_Carpet_Tile__c)setCPLFields(New CPL_Carpet_Tile__c(),'EXTID03', listProducts[3],account.Id,false, accPTR.Id, 'TM1'));
        listCT.add((CPL_Carpet_Tile__c)setCPLFields(New CPL_Carpet_Tile__c(),'EXTID03', listProducts[3],account.Id,false, accPTR.Id, 'TM2'));
        listCT.add((CPL_Carpet_Tile__c)setCPLFields(New CPL_Carpet_Tile__c(),'EXTID03', listProducts[3],account.Id,false, accPTR.Id, 'Less_than_RVP'));
       
        listC.add((CPL_Cushion__c)setCPLFields(New CPL_Cushion__c(),'EXTID04', listProducts[4],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listC.add((CPL_Cushion__c)setCPLFields(New CPL_Cushion__c(),'EXTID04', listProducts[4],account.Id,false, accPTR.Id, 'TM1'));
        listC.add((CPL_Cushion__c)setCPLFields(New CPL_Cushion__c(),'EXTID04', listProducts[4],account.Id,false, accPTR.Id, 'TM2'));
        listC.add((CPL_Cushion__c)setCPLFields(New CPL_Cushion__c(),'EXTID04', listProducts[4],account.Id,false, accPTR.Id, 'Less_than_RVP'));
       
        listSW.add((CPL_Solid_Wood__c)setCPLFields(New CPL_Solid_Wood__c(),'EXTID05', listProducts[5],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listSW.add((CPL_Solid_Wood__c)setCPLFields(New CPL_Solid_Wood__c(),'EXTID05', listProducts[5],account.Id,false, accPTR.Id, 'TM1'));
        listSW.add((CPL_Solid_Wood__c)setCPLFields(New CPL_Solid_Wood__c(),'EXTID05', listProducts[5],account.Id,false, accPTR.Id, 'TM2'));
        listSW.add((CPL_Solid_Wood__c)setCPLFields(New CPL_Solid_Wood__c(),'EXTID05', listProducts[5],account.Id,false, accPTR.Id, 'Less_than_RVP'));
        
        listTW.add((CPL_Tec_Wood__c)setCPLFields(New CPL_Tec_Wood__c(),'EXTID06', listProducts[6],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listTW.add((CPL_Tec_Wood__c)setCPLFields(New CPL_Tec_Wood__c(),'EXTID06', listProducts[6],account.Id,false, accPTR.Id, 'TM1'));
        listTW.add((CPL_Tec_Wood__c)setCPLFields(New CPL_Tec_Wood__c(),'EXTID06', listProducts[6],account.Id,false, accPTR.Id, 'TM2'));
        listTW.add((CPL_Tec_Wood__c)setCPLFields(New CPL_Tec_Wood__c(),'EXTID06', listProducts[6],account.Id,false, accPTR.Id, 'Less_than_RVP'));
        
        listRW.add((CPL_Rev_Wood__c)setCPLFields(New CPL_Rev_Wood__c(),'EXTID07', listProducts[7],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listRW.add((CPL_Rev_Wood__c)setCPLFields(New CPL_Rev_Wood__c(),'EXTID07', listProducts[7],account.Id,false, accPTR.Id, 'TM1'));
        listRW.add((CPL_Rev_Wood__c)setCPLFields(New CPL_Rev_Wood__c(),'EXTID07', listProducts[7],account.Id,false, accPTR.Id, 'TM2'));
        listRW.add((CPL_Rev_Wood__c)setCPLFields(New CPL_Rev_Wood__c(),'EXTID07', listProducts[7],account.Id,false, accPTR.Id, 'Less_than_RVP'));

        listT.add((CPL_Tile__c)setCPLFields(New CPL_Tile__c(),'EXTID08', listProducts[8],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listT.add((CPL_Tile__c)setCPLFields(New CPL_Tile__c(),'EXTID08', listProducts[8],account.Id,false, accPTR.Id, 'TM1'));
        listT.add((CPL_Tile__c)setCPLFields(New CPL_Tile__c(),'EXTID08', listProducts[8],account.Id,false, accPTR.Id, 'TM2'));
        listT.add((CPL_Tile__c)setCPLFields(New CPL_Tile__c(),'EXTID08', listProducts[8],account.Id,false, accPTR.Id, 'Less_than_RVP'));
       
        listRT.add((CPL_Resilient_Tile__c)setCPLFields(New CPL_Resilient_Tile__c(),'EXTID09', listProducts[9],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listRT.add((CPL_Resilient_Tile__c)setCPLFields(New CPL_Resilient_Tile__c(),'EXTID09', listProducts[9],account.Id,false, accPTR.Id, 'TM1'));
        listRT.add((CPL_Resilient_Tile__c)setCPLFields(New CPL_Resilient_Tile__c(),'EXTID09', listProducts[9],account.Id,false, accPTR.Id, 'TM2'));
        listRT.add((CPL_Resilient_Tile__c)setCPLFields(New CPL_Resilient_Tile__c(),'EXTID09', listProducts[9],account.Id,false, accPTR.Id, 'Less_than_RVP'));
        
        listIA.add((CPL_Installation_Accessories__c)setCPLFields(New CPL_Installation_Accessories__c(),'EXTID10', listProducts[10],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listIA.add((CPL_Installation_Accessories__c)setCPLFields(New CPL_Installation_Accessories__c(),'EXTID10', listProducts[10],account.Id,false, accPTR.Id, 'TM1'));
        listIA.add((CPL_Installation_Accessories__c)setCPLFields(New CPL_Installation_Accessories__c(),'EXTID10', listProducts[10],account.Id,false, accPTR.Id, 'TM2'));
        listIA.add((CPL_Installation_Accessories__c)setCPLFields(New CPL_Installation_Accessories__c(),'EXTID10', listProducts[10],account.Id,false, accPTR.Id, 'Less_than_RVP'));
        
        listCM.add((CPL_Care_and_Maintenance__c)setCPLFields(New CPL_Care_and_Maintenance__c(),'EXTID11', listProducts[11],account.Id,false, accPTR.Id, 'Greater_than_TM1'));
        listCM.add((CPL_Care_and_Maintenance__c)setCPLFields(New CPL_Care_and_Maintenance__c(),'EXTID11', listProducts[11],account.Id,false, accPTR.Id, 'TM1'));
        listCM.add((CPL_Care_and_Maintenance__c)setCPLFields(New CPL_Care_and_Maintenance__c(),'EXTID11', listProducts[11],account.Id,false, accPTR.Id, 'TM2'));
        listCM.add((CPL_Care_and_Maintenance__c)setCPLFields(New CPL_Care_and_Maintenance__c(),'EXTID11', listProducts[11],account.Id,false, accPTR.Id, 'Less_than_RVP'));
        
        
        insert listCB;
        insert listRB;
        insert listRS;
        insert listCT;
        insert listC;
        insert listSW;
        insert listTW;
        insert listRW;
        insert listT;
        insert listRT;
        insert listIA;
        insert listCM;
        
        System.Debug('inserted records: ' + listCB);
        System.Debug('inserted records: ' + listRB);
        System.Debug('inserted records: ' + listRS);
        System.Debug('inserted records: ' + listCT);
        System.Debug('inserted records: ' + listC);
        System.Debug('inserted records: ' + listSW);
        System.Debug('inserted records: ' + listTW);
        System.Debug('inserted records: ' + listRW);
        System.Debug('inserted records: ' + listT);
        System.Debug('inserted records: ' + listRT);
        System.Debug('inserted records: ' + listIA);
        System.Debug('inserted records: ' + listCM);
        
        
        
        List<SObject> resultListCB = [SELECT Id, CPL_Sales__c FROM CPL_Commercial_Broadloom__c      WHERE Id IN: listCB];
        List<SObject> resultListRB = [SELECT Id, CPL_Sales__c FROM CPL_Residential_Broadloom__c     WHERE Id IN: listRB];
        List<SObject> resultListRS = [SELECT Id, CPL_Sales__c FROM CPL_Resilient_Sheet__c           WHERE Id IN: listRS];
        List<SObject> resultListCT = [SELECT Id, CPL_Sales__c FROM CPL_Carpet_Tile__c               WHERE Id IN: listCT];
        List<SObject> resultListC =  [SELECT Id, CPL_Sales__c FROM CPL_Cushion__c                   WHERE Id IN: listC];
        List<SObject> resultListSW = [SELECT Id, CPL_Sales__c FROM CPL_Solid_Wood__c                WHERE Id IN: listSW];
        List<SObject> resultListTW = [SELECT Id, CPL_Sales__c FROM CPL_Tec_Wood__c                  WHERE Id IN: listTW];
        List<SObject> resultListRW = [SELECT Id, CPL_Sales__c FROM CPL_Rev_Wood__c                  WHERE Id IN: listRW];
        List<SObject> resultListT =  [SELECT Id, CPL_Sales__c FROM CPL_Tile__c                      WHERE Id IN: listT];
        List<SObject> resultListRT = [SELECT Id, CPL_Sales__c FROM CPL_Resilient_Tile__c            WHERE Id IN: listRT];
        List<SObject> resultListIA = [SELECT Id, CPL_Sales__c FROM CPL_Installation_Accessories__c  WHERE Id IN: listIA];
        List<SObject> resultListCM = [SELECT Id, CPL_Sales__c FROM CPL_Care_and_Maintenance__c      WHERE Id IN: listCM];
        

        System.Debug('query records: ' + resultListCB);
        System.Debug('query records: ' + resultListRB);
        System.Debug('query records: ' + resultListRS);
        System.Debug('query records: ' + resultListCT);
        System.Debug('query records: ' + resultListC);
        System.Debug('query records: ' + resultListSW);
        System.Debug('query records: ' + resultListTW);
        System.Debug('query records: ' + resultListRW);
        System.Debug('query records: ' + resultListT);
        System.Debug('query records: ' + resultListRT);
        System.Debug('query records: ' + resultListIA);
        System.Debug('query records: ' + resultListCM);


        for (SObject obj : resultListCB) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListRB) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListRS) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListCT) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListC)  System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListSW) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListTW) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListRW) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListT)  System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListRT) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListIA) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        for (SObject obj : resultListCM) System.assertNotEquals(null, obj.get('CPL_Sales__c'));
        
        
        Test.stopTest();
        
    }


    
    public static Account_Product_Territory_Relationship__c createAccountRelationship(Account account, String zone, String region, String district, String Territory) {
        
        Account_Product_Territory_Relationship__c accPTR = new Account_Product_Territory_Relationship__c();
        accPTR.Account__c = account.Id;
        accPTR.Zone_Code__c = zone;
        accPTR.Zone_Description__c = zone;
        accPTR.Region_Code__c = region;
        accPTR.Region_Description__c = region;
        accPTR.District_Code__c = district;
        accPTR.District_Description__c = district;
        accPTR.Sales_Group_TM__c = Territory;
        accPTR.Company_Code__c = 'R';
        accPTR.Brand_Code__c = 'R';
        accPTR.Product_Type__c = 'R';
        
        insert accPTR;
        return accPTR;
    }
    
    public static User createUserRecord(String ProfileName){
        String strProfileId = [Select Id From Profile Where Name = :ProfileName Limit 1].Id;
        
        User u = New User();
        u.ProfileId = strProfileId;
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@mohawkind.com';
        u.Username = 'testuser@mohawkind.com';
        u.Alias = 'tuser';
        u.TimeZoneSidKey = 'America/New_York';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.CurrencyIsoCode = 'USD';
        u.EmailEncodingKey = 'ISO-8859-1';
        
        return u;
        
    }    

  
    
    public static List<Zone__c> createZoneRecords(integer Quantity){
        List<Zone__c> listZones = New List<Zone__c>();
        for (Integer j=0; j< Quantity;j++){
           listZones.add(New Zone__c(Name = 'Zone' + j, Zone_Code__c = string.valueof(j)));   
        }

        return listZones;
    }
        
    
    public static List<Territory__c> createTerritoryRecords(integer Quantity){
        String strRecordType = [Select Id From RecordType Where Name='Residential' and sobjectType = 'Territory__c' Limit 1].Id;
        List<Territory__c> listTerritory = New List<Territory__c>();
        for (Integer j=0; j< Quantity;j++){
           listTerritory.add(New Territory__c(Name = 'Territory' + j, Region_Code__c = 'R' + string.valueof(j), RecordTypeId = strRecordType));   
        }

        return listTerritory;
    }    
    
    public static List<Territory_User__c> createTerritoryUserRecords(integer Quantity){
        
        Id strRecordType = ResidentialPricingGridUtility.getRecordTypesByObject('Territory_User__c').get(ResidentialPricingGridUtility.TERRITORY_USER_RESIDENTIAL_RECORD_TYPE_NAME);        
        //String strRecordType = [Select Id From RecordType Where Name='Residential' and sobjectType = 'Territory_User__c' Limit 1].Id;
        List<Territory_User__c> listTerritoryUser = New List<Territory_User__c>();
        for (Integer j=0; j< Quantity;j++){
           listTerritoryUser.add(New Territory_User__c(Name = 'TM' + j, RecordTypeId = strRecordType, Role__c ='Territory Manager'));   
        }

        return listTerritoryUser;
    } 
    
    
    public static List<Product2> createProductRecords(Integer Quantity, Integer Index, String Category, String SubCategory){
        String strRecordType = [Select Id From RecordType Where Name='Residential Product' and sobjectType = 'Product2' Limit 1].Id;
        List<Product2> listProducts = New List<Product2>();
         for (Integer j=0; j< Quantity;j++){
             Product2 P = New Product2();
             P.RecordTypeId = strRecordType;
             P.Name = 'Product' + Index;
             P.Category__c = Category;
             P.Sub_Category__c = SubCategory;
             P.Company__c = 'R';
             P.Inventory_Style_Num__c = 'sl' + Index;
             P.Backing__c = 'BK';
             P.Size__c = String.valueOf(1000+Index);
             P.Residential_Product_Unique_Key__c = 'R.' + SubCategory.substring(0, 4) + j + '.' + 'BK.' + String.valueOf(1000 + Index);             
             listProducts.add(P);
         }
        return listProducts;
    }
    
    public static sObject setSalesFields(sObject so, String externalId, String category, Product2 prod, Account account){
        
        so.put('External_ID__c', externalId);
        so.put('Chain_Account__c', account.Id);
        so.put('Division_Customer_No_SFX_Id__c', account.Chain_Number__c);
        so.put('Product__c', prod.Id);
        //so.put('Product_Category__c', category);
        so.put('Selling_Style_Num__c', prod.Inventory_Style_Num__c);
        so.put('Product_Unique_Id__c', prod.Residential_Product_Unique_Key__c);
        so.put('MTD_Price_Level__c', '');
        so.put('YTD_Price_Level__c', '');
        so.put('R12_Price_Level__c', '');
        so.put('Net_Sales_MTD__c', 323.46);
        so.put('Net_Sales_YTD__c', 2686.16);
        so.put('Net_Sales_R12__c', 673.65);

        return so;
    }     
    
    
    public static sObject setSalesItemFields(sObject so, CPL_Sales__c cplSales, String category, Product2 prod, Account account, String timePeriod, String region, String District, String territory, String priceLevel){
        
        so.put('CPL_Sales__c', cplSales.Id);
        so.put('Product__c', prod.Id);
        so.put('Division_Customer_No_SFX_Id__c', account.Chain_Number__c);
        so.put('Product_Categoy__c', category);
        so.put('Selling_Style_Num__c', prod.Inventory_Style_Num__c);
        so.put('Product_Unique_Id__c', prod.Residential_Product_Unique_Key__c);
        so.put('External_Id__c', prod.Residential_Product_Unique_Key__c + '.' + account.Chain_Number__c);
        so.put(timePeriod + '_Price_Level__c', priceLevel);
        so.put('Net_Sales_'  + timePeriod + '__c', 5.00);
        so.put('Region_Code__c', region);
        so.put('District_Code__c', District);
        so.put('Territory_Manager_Id__c', territory);
        so.put('Territory_Manager_User_Name__c', 'Test User');
        so.put('Net_Sales_MTD__c', 323.46);
        so.put('Net_Sales_YTD__c', 2686.16);
        so.put('Net_Sales_R12__c', 673.65);
        so.put('Gross_Avg_Price_MTD__c', 4.0);
        so.put('Gross_Avg_Price_YTD__c', 5.0);
        so.put('Gross_Avg_Price_R12__c', 6.0);

        return so;
    }     
    
    public static sObject setCPLFields(sObject so, String salesExternalId, Product2 prod, String AccountId, Boolean isCarpet, String accPTRId, String priceLevel){
        
        so.put('CPLSales_External_ID__c', salesExternalId);
        so.put('CPL_Product_Territory__c', accPTRId);
        so.put('Account__c', AccountId);
        so.put('Product__c',prod.Id);
        so.put('Product_Unique_Key__c',prod.Residential_Product_Unique_Key__c);
        so.put('Company__c','R');
        so.put('Selling_Style_Num__c',prod.Inventory_Style_Num__c);
        so.put('Backing__c','');
        so.put('Size__c','');
        so.put('Buying_Group_Price__c',true);
        so.put('Min_Order_Qty__c',true);
        so.put('Currency__c','USD');
        so.put('Start_Date__c',Date.today()-1);
        so.put('End_Date__c', date.ValueOf('4000-12-31'));
        so.put('Price_Level__c', priceLevel);
        if (isCarpet){
            so.put('Net_Price_Roll__c',2.00);
            so.put('Net_Price_Cut__c',3.00);
            so.put('Cuts_at_Roll__c',20);
            so.put('Net_Price_Roll__c',1.5);
        }
        else{
            so.put('Billing_Price__c',5.00);
            so.put('Net_Price__c',4.00);
        }
        return so;
    }    

    
}