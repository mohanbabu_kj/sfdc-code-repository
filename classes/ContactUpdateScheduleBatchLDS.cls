public class ContactUpdateScheduleBatchLDS {
    
}
/*
global class ContactUpdateScheduleBatchLDS implements Schedulable
{
    
    global void execute(SchedulableContext ctx) 
    {
        string bType = 'Commercial';
        String RecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-Invoicing').getRecordTypeId();
        CronTrigger ct;
        
        try
        {
          ct = [SELECT ID, CronExpression, CronJobDetail.Name, CronJobDetailId, EndTime, 
                               NextFireTime, PreviousFireTime, StartTime, State, TimesTriggered 
                               FROM CronTrigger where CronJobDetail.Name = 'ContactUpdateScheduleBatchLDSJob' limit 1];
        }catch(Exception e)
        {
            system.debug('Exception ');
             
        }
        

        DateTime sdt;
        //for the first time batch secduled it does't have any job ran, So we will take start time as one hour prior.
        if(ct == null)
        {
            sdt = System.Now().addHours(-1);
        }
        else
            sdt = ct.PreviousFireTime;
        
        DateTime endDateTime = System.Now().addMinutes(-2);
        DateTime startDateTime = sdt.addMinutes(-4);
        String q ='Select id , LDS_Create__c , LDS_Update__c ,LastModifiedDate from contact where LDS_Create__c = '+ true+ ' and LDS_Update__c = '+ false+ ' and ';
        q += ' LastModifiedDate >=: start and LastModifiedDate <= :endDate1';
        
        
        String upda= 'LDS_Update__c';
        String cre = 'LDS_Create__c';
        Boolean v1=true;
        Boolean v2 = false;

        AccConOppUpdateBatchLDS p = new AccConOppUpdateBatchLDS(q,upda,cre,v1,v2,bType,RecTypeId,startDateTime,endDateTime);
        Database.executeBatch(p,200);
        
    }
    
} */