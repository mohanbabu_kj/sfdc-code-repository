/**************************************************************************

Name : SampleDisplayInventoryInsert_Batch 

===========================================================================
Purpose : Batch class to Insert Sample Display Inventory based on Account and Product conditions.
Account : 
(1) Customer Group field(Picklist (Multi-Select))
(2) Business_Type__c must be Residential
(3) Record type must be Invoicing
Product : 
(1) Buying Group field(Picklist)
(2) Generic_Product__c is true
If Customer Group in Account include Buying Group in Product, to create one Sample Display Inventory reocrd
Insert Sample Display Inventory field required :
(1) Account__c 
(2) Inventory__c (Lookup(Product))
(3) Placement_Status__c sets to to 'Not In Store'
(4) External_Id__c sets to the value which Global_Account_Number__c in Account combine External_Id__c in Product
===========================================================================
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0       Sasi Naik       28/April/2017    Created          CSR:

***************************************************************************/
global class SampleDisplayInventoryInsert_Batch implements Database.Batchable<sObject>,Schedulable {
    
    Map<String, String> prodBrandTypeMap = new Map<String, String>();
    global final String query;
    public static Boolean error = false;
    
    //Batch Start Method     
    global Database.QueryLocator start(Database.BatchableContext bc) {

        system.debug( '::::soql::::' + query );
        return Database.getQueryLocator(query);
    }  
    
    global SampleDisplayInventoryInsert_Batch(String q){
        String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId(); 
        query = q != null ? q : 'Select Id,Customer_Group__c,Customer_Group_Number__c,Global_Account_Number__c from Account where SFDC_Display__c = true AND RecordTypeId = \''+recordTypeId+'\' and Business_Type__c =\''+UtilityCls.RESIDENTIAL+'\'';
    }
    
    global SampleDisplayInventoryInsert_Batch(){
        
        String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();
        //query = 'Select Id,Customer_Group__c,Customer_Group_Number__c,Global_Account_Number__c from Account where SFDC_Display__c = true AND RecordTypeId = \''+recordTypeId+'\' and Business_Type__c =\''+UtilityCls.RESIDENTIAL+'\'';
        // Start of Code - MB - Bug 71599 - 3/14/19 
        Batch_Query__mdt[] queryList = [SELECT Query_String__c FROM Batch_Query__mdt where QualifiedApiName = 'SampleDisplayInventory_Query']; //default query 
        If(queryList.size() > 0) {
            query = queryList[0].Query_String__c;                
        } else {
            query = 'Select Id,Customer_Group__c,Customer_Group_Number__c,Global_Account_Number__c from Account where SFDC_Display__c = true AND RecordTypeId = \''+recordTypeId+'\' and Business_Type__c =\''+UtilityCls.RESIDENTIAL+'\'' ;
        }
        // End of Code - MB - Bug 71599 - 3/14/19 
    }
    //Batch Execute Method by passing the Account SOQL List
    global void execute(Database.BatchableContext BC, List<Account> scope) {
/********************************PREVIOUS CODE ************
Commented code removed as part of bug 68526
**********************************************************/
        List<Sample_Display_Inventory__c> listsdi = new List<Sample_Display_Inventory__c>();
        Batch_Query__mdt [] queryList = [SELECT Query_String__c FROM Batch_Query__mdt where QualifiedApiName = 'SampleDisplayInventoryInsert_Batch'];
        String recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Residential Display').getRecordTypeId();    
        
        String SOQL = 'SELECT id,Product__c,Customer_Group_Number__c,Product__r.Residential_Product_Unique_Key__c,Product__r.Brand_Code__c,Product__r.Brand_Description__c, Product__r.ERP_Product_Type__c , Product__r.Name FROM Product_Customer_Group_Relationship__c WHERE Is_Product_Active__c = true AND Product__r.Trackable_Display_asset__C = true AND Product__r.RecordtypeId =: recordTypeId '; 
        If(queryList.size() > 0){
            SOQL = queryList[0].Query_String__c ;
        }
        System.debug(LoggingLevel.DEBUG, '*** : ProdCustomerGrp SOQL**' + SOQL );     
        //  List<Account> Accountlist = database.query(SOQL);
        List<Product_Customer_Group_Relationship__c> ProcCustGroupList = database.query(SOQL);
        
        Map<String, String> prodBrandTypeMap = new Map<String, String>();
        if (ProcCustGroupList.size() > 0){
            for(Product_SFDC_Mapping__c prodBrandType : [SELECT Brand__c, Product_Type__c, Sample_Inventory_Category__c 
                                                         FROM Product_SFDC_Mapping__c]){
                                                             String brandType = prodBrandType.Brand__c + '_' + prodBrandType.Product_Type__c;
                                                             prodBrandTypeMap.put(brandType, prodBrandType.Sample_Inventory_Category__c);
                                                         }        
            
            Map<String,string>buyingGroupMap = new Map<String,string>();
            
            for( Buying_Group__c BG : [ SELECT Name,Buying_Group_Number__c From Buying_Group__c ] ){
                buyingGroupMap.put( BG.Buying_Group_Number__c,BG.Name );
            }
            
            for( Account ac:scope ){
                Set<String> cusGroupSet = new Set<String>();
                List<String> cusGroup = ac.Customer_Group_Number__c != null ? ac.Customer_Group_Number__c.split(';'): new list<string>();
                
                if (cusGroup.size() > 0){
                    cusGroupSet.addAll(cusGroup);
                }
                for( Product_Customer_Group_Relationship__c p: ProcCustGroupList ){
                    system.debug('ProdCustGrp' + p.Customer_Group_Number__c );
                    if(cusGroupSet.contains(p.Customer_Group_Number__c) || p.Customer_Group_Number__c == '000000'){
                        Sample_Display_Inventory__c sdi = new Sample_Display_Inventory__c();
                        String brandType = '';
                        sdi.Account__c = ac.Id;
                        sdi.Inventory__c = p.Product__c;
                        sdi.External_Id__c = ac.Global_Account_Number__c +'_'+ p.Product__r.Residential_Product_Unique_Key__c;
                        //sdi.Placement_Status__c = UtilityCls.Not_In_Store; - Commented by MB - Bug 71599 - 3/14/19
                        sdi.Customer_Group__c = buyingGroupMap.get( p.Customer_Group_Number__c );
                        sdi.Customer_Group_Number__c = p.Customer_Group_Number__c; // Added by MB - Bug 68526 - 12/11/18
                        brandType = p.Product__r.Brand_Code__c + '_' + p.Product__r.ERP_Product_Type__c;
                        sdi.Sample_Inventory_Category__c = (prodBrandTypeMap != null && prodBrandTypeMap.containsKey(brandType) ? prodBrandTypeMap.get(brandType) : '');
                        sdi.RecordTypeId = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get(UtilityCls.SAMPLEREDIDENTALDISPLAY).getRecordTypeId();
                        sdi.Name = p.Product__r.Name;                       
                        listsdi.add(sdi);
                    }
                }
            }
            If(listsdi.size()>0){
                System.debug(LoggingLevel.INFO, '*** listsdi: ' +listsdi.size() );
                //database.insert(listsdi,false); - Commented by MB - Bug 68753 - 12/11/18
                /* Start of Code - MB - Bug 68753 - 12/11/18 */
                List<Database.UpsertResult> results = Database.upsert(listsdi, Sample_Display_Inventory__c.Field.External_Id__c, false);
                UtilityCls.createUpsertDMLexceptionlog(listsdi,results,'SampleDisplayInventoryInsert_Batch','createSamplePerformanceInventory');
                for( Database.UpsertResult result: results ){
                    if( !result.isSuccess() ){
                        error = true;
                    }
                }
                /* End of Code - MB - Bug 68753 - 12/11/18 */
            }        
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC) {
        /* Start of Code - MB - Bug 68753 - 12/11/18 */
        if(error || Test.isRunningTest()){
            String body = '';
            List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';');  
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            body = 'Hello All, <br /><br />';
            body += 'Please check Application log for failed records from SampleDisplayInventoryInsert_Batch class.<br />';
            String subject = 'Failed to create record in SampleDisplayInventoryInsert_Batch';
            email.setSubject(subject);
            email.setToAddresses(emailAddressList);
            email.setHtmlBody(body);
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
        /* End of Code - MB - Bug 68753 - 12/11/18 */
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
        //Database.executeBatch(new SampleDisplayInventoryInsert_Batch(null); - Commented by MB - Bug 71599 - 3/14/19
        // Start of Code - MB - Bug 71599 - 3/14/19 
        List<Batch_Query__mdt> queryList = [SELECT Batch_Size__c FROM Batch_Query__mdt where QualifiedApiName = 'SampleDisplayInventory_Query']; //default query 
        if(queryList != null && queryList.size()>0 && queryList[0].Batch_Size__c != null && queryList[0].Batch_Size__c != 0){
            Integer batchSize = (Integer)queryList[0].Batch_Size__c;
            System.debug('batchSize: ' + batchSize);
        	Database.executeBatch(new SampleDisplayInventoryInsert_Batch(), batchSize);
        }else{
            Database.executeBatch(new SampleDisplayInventoryInsert_Batch(), 50);
        }
        // Start of Code - MB - Bug 71599 - 3/14/19 
    }
}