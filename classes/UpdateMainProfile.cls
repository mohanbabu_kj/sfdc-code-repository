/**************************************************************************
Name : UpdateMainProfile
===========================================================================  
Purpose : This class is used to check Main Profile field  
=========================================================================== 
History:    --------   
VERSION    AUTHOR         DATE               DETAIL          DESCRIPTION   
1.0        Nagendra Singh 28/01/2019                         Created      
***************************************************************************/
public without sharing class UpdateMainProfile {
    public static List < Mohawk_Account_Team__c > updateExistingMATs;
    public static void checkMainProfileCheckBox(List < Mohawk_Account_Team__c > newList) {
        Set < String > chainNumbersUserIds = new Set < String > ();
        set < String > matCheckedFound = new set < String > ();
        map < String, Mohawk_Account_Team__c > mpNoneMATNotChecked = new map < String, Mohawk_Account_Team__c > ();
        updateExistingMATs = new List < Mohawk_Account_Team__c > ();
        
        for (Mohawk_Account_Team__c mat: newList) {
            if (mat.Chain_Number_UserId__c != null) {
                chainNumbersUserIds.add(mat.Chain_Number_UserId__c);
            }
        }
        List < Mohawk_Account_Team__c > existingMats = [Select Id, CreatedDate, Main_Profile__c, Chain_Number_UserId__c from Mohawk_Account_Team__c where(recordtype.name = 'Residential Non-Invoicing'
                                                                                                                                                          or recordtype.name = 'Residential Invoicing') And 
                                                        Chain_Number_UserId__c != null and Chain_Number_UserId__c in: chainNumbersUserIds order by CreatedDate ASC];
        for (Mohawk_Account_Team__c existingMAT: existingMats) { 
            //if Main Profile found checked on any MAT                    
            if(existingMAT.Main_Profile__c){                           
                matCheckedFound.add(existingMAT.Chain_Number_UserId__c);                                                 
            }else{                     
                //Main profile not checked then find oldest craetedate MAT          
                if(!mpNoneMATNotChecked.containsKey(existingMAT.Chain_Number_UserId__c) && !matCheckedFound.contains(existingMAT.Chain_Number_UserId__c)) {                       
                    // mpNoneMATNotChecked.put(existingMAT.Chain_Number_UserId__c,existingMAT.CreatedDate.Date());        
                    mpNoneMATNotChecked.put(existingMAT.Chain_Number_UserId__c,existingMAT );           
                }                 
            }     
        }   
        chainNumbersUserIds = new set<string>();       
        for(Mohawk_Account_Team__c mat:newList){           
            if(!matCheckedFound.contains(mat.Chain_Number_UserId__c) && !mpNoneMATNotChecked.containsKey(mat.Chain_Number_UserId__c)){ 
                mat.Main_Profile__c = true; 
                matCheckedFound.add(mat.Chain_Number_UserId__c); 
                if(!Trigger.isExecuting){ 
                    updateExistingMATs.add(mat);
                }
            } else if (!matCheckedFound.contains(mat.Chain_Number_UserId__c) && mpNoneMATNotChecked.containskey(mat.Chain_Number_UserId__c)) {   
                updateExistingMATs.add((new Mohawk_Account_Team__c(id = mpNoneMATNotChecked.get(mat.Chain_Number_UserId__c).id, Main_Profile__c = true)));
                matCheckedFound.add(mat.Chain_Number_UserId__c);
            }
        }
        if (updateExistingMATs.size() > 0) {
            doUpdate(updateExistingMATs);
        }
    }
    public static void doUpdate(List < Mohawk_Account_Team__c > updateExistingMATs) {
        List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
        MohawkAccountTeamService.updateFromEvent = true;  
        Database.SaveResult[] results = Database.update(updateExistingMATs, false);  
        UtilityCls.createDMLexceptionlog(updateExistingMATs,results,'UpdateMainProfile','doUpdate');        
    } 
    public static void check_MainProfileCheckBox(List < Mohawk_Account_Team__c > oldList) {
        Set < String > chainNumbersUserIds = new Set < String > ();
        updateExistingMATs = new list < Mohawk_Account_Team__c > ();
        
        map < String, Mohawk_Account_Team__c > mpNoneMATNotChecked = new map < string, Mohawk_Account_Team__c > ();
        for (Mohawk_Account_Team__c mat: oldList) {
            if (mat.Main_Profile__c) {
                chainNumbersUserIds.add(mat.Chain_Number_UserId__c);
            }
        }
        List < Mohawk_Account_Team__c > existingMats = [Select id, CreatedDate, Main_Profile__c, Chain_Number_UserId__c from Mohawk_Account_Team__c where(recordtype.name = 'Residential Non-Invoicing'
                                                                                                                                                          or recordtype.name = 'Residential Invoicing') And
                                                        Chain_Number_UserId__c != null and Chain_Number_UserId__c in: chainNumbersUserIds order by CreatedDate ASC];
        for (Mohawk_Account_Team__c existingMAT: existingMats) {
            if (!mpNoneMATNotChecked.containskey(existingMAT.Chain_Number_UserId__c)) {
                mpNoneMATNotChecked.put(existingMAT.Chain_Number_UserId__c, existingMAT);
            }
        }
        
        chainNumbersUserIds = new Set < String > ();
        for (Mohawk_Account_Team__c mat: oldList) {
            if (mpNoneMATNotChecked.containskey(mat.Chain_Number_UserId__c) && !chainNumbersUserIds.contains(mat.Chain_Number_UserId__c)) { // Bug 72186 - SS - 3/25
                updateExistingMATs.add((new Mohawk_Account_Team__c(id = mpNoneMATNotChecked.get(mat.Chain_Number_UserId__c).id, Main_Profile__c = true)));
                chainNumbersUserIds.add(mat.Chain_Number_UserId__c);
            }
        }      
        
        if (updateExistingMATs.size() > 0) {
            doUpdate(updateExistingMATs);
        }
    }
}