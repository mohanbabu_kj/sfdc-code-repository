/**************************************************************************

Name : TerritoryCheckPublicGroup_Batch

===========================================================================
Purpose : Batch class to Check the fact that all Terrirory name should be match the same with public group.
If Terrirory has a new  territory user reocrd, you have to create this recod in public group.
If Terrirory removes a territory user reocrd, you have to remove this recod in public group.
If Terrirory changes existing territory user record , firstly you have to remove this recod in public group. 
Then, have to create this recod in public group.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand         28/Feb/2017    Created          CSR:

***************************************************************************/
global class TerritoryCheckPublicGroup_Batch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {//Added Database.Stateful by Susmitha
    global static string ErrorRecords;
    Static String resRole;
    Static String commRole;
    static string TerritoryRegionRTId ; // Variable is used to get the id of Territory Record Type - Added By Mudit on 7 Feb - US30128
    static string TerritoryRTId; 
    static string TeriUsersRecoType;
    static set<string> ResiRoleSet = new set<string>();
    Static {
        
        resRole = UtilityCls.CS_RESROLE.Role__c;
        commRole = UtilityCls.CS_COMMROLE.Role__c; 
        TerritoryRegionRTId = UtilityCls.getRecordTypeInfo('Territory__c','Residential Regions');
        TerritoryRTId = UtilityCls.getRecordTypeInfo('Territory__c','Residential');
        TeriUsersRecoType = UtilityCls.getRecordTypeInfo('Territory_User__c','Residential');
        ResiRoleSet = UtilityCls.resPublicGroupRoles_CS();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //Need to add condition to retrieve recent modified users last 1 DAY
        //Query only Territory User records which are modifed in last 1 day.
        /* String query = 'SELECT Id, User__c, Territory__c,RecordTypeId, IsDeleted, Role__c,Region__c, Territory__r.Name,Territory__r.RecordTypeId '+ 
'FROM Territory_User__c WHERE (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND User__c != NULL ALL ROWS'; //Commented ALL ROWS before WHERE by SUSMITHA ON NOV 6 // Region__c and Territory__r.RecordTypeId Added by - Mudit on 7 Feb - US30128 
System.debug(LoggingLevel.INFO, '*** query ***' +query);
System.debug(LoggingLevel.INFO, '*** query 12: ' +Database.getQueryLocator(Query));*/
        
        /*--Modified codes by Susmitha on April 2 for Notifying error--*/
        String Query;
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'TerritoryCheckPublicGroup_Batch'];
        If(QureyList.size() > 0)
            query = QureyList[0].Query_String__c ;
        else
            query = 'SELECT Id, User__c, Territory__c,RecordTypeId, IsDeleted, Role__c,Region__c, Territory__r.Name,Territory__r.RecordTypeId '+ 
            'FROM Territory_User__c WHERE (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND User__c != NULL ALL ROWS';
        /*--End Modified codes by Susmitha on Apr 2--*/
        return Database.getQueryLocator(Query);
        
    }
    
    
    global void execute(Database.BatchableContext BC, List<Territory_User__c> scope) {
        
        //Map with territory name with set of user ids
        Map<String, List<Territory_User__c>> terUserIdsMap = new Map<String, List<Territory_User__c>>();  // for public group update
        Map<String, Set<Id>> MATterUserIdsMap = new Map<String, Set<Id>>();  // for public group update
        Map<Id, Map<Id,String>> terUsersMap = new  Map<Id,  Map<Id,String>>(); //for Mohawk Account team update
        //Map<String, Set<Id>> deletedRecordids = new  Map<String, Set<Id>>();
        
        system.debug( ':::MS TEST::::' + scope.size() );
        system.debug( ':::MS TEST::::' + scope );
        for(Territory_User__c tu : scope){     

           // Not needed        
            system.debug( ':::DATE::::' + tu );
            if(tu.IsDeleted== true){
                system.debug('****'+tu);
            }else
            {
                system.debug('Un Del****'+tu); 
            }
            
           // 
            

            // for public group update (Key: Territory Name, Value: Terrritory User ids)
            if(terUserIdsMap.containsKey(tu.Territory__r.Name) ){ 
            //  Added by - Suresh - 06/07/18         
               if ( tu.Territory__r.RecordTypeId !=  TerritoryRTId || (tu.Territory__r.RecordTypeId ==  TerritoryRTId && ResiRoleSet.contains(tu.Role__c) ))
                 terUserIdsMap.get(tu.Territory__r.Name).add(tu);
               //MATterUserIdsMap.get(tu.Territory__c).add(tu.User__c);
                
            }else{
              //  Added by - Suresh - 06/07/18  
               if ( tu.Territory__r.RecordTypeId !=  TerritoryRTId || (tu.Territory__r.RecordTypeId ==  TerritoryRTId && ResiRoleSet.contains(tu.Role__c) )){
                    List<Territory_User__c> tuList = new List<Territory_User__c>();  
                    tuList.add(tu);
                    terUserIdsMap.put(tu.Territory__r.Name, tuList);
               }
                //MATterUserIdsMap.put(tu.Territory__c, userids);
            }
            
            //  Checking for Residential Regions Territory's user only  Added by - Mudit on 7 Feb - US30128 
            //  Added by - Suresh - 06/07/18  
            if( tu.RecordTypeId == TeriUsersRecoType && ResiRoleSet.contains(tu.Role__c) ){
                system.debug('terUserIdsMap.get( tu.Region__c ) :'+terUserIdsMap.get( tu.Region__c ));
                if(terUserIdsMap.containsKey( tu.Region__c ) ){
                    system.debug('terUserIdsMap.get( tu.Region__c ) :'+terUserIdsMap.get( tu.Region__c ));
                    terUserIdsMap.get( tu.Region__c ).add( tu );
                }else{
                    terUserIdsMap.put( tu.Region__c, new List<Territory_User__c>{ tu } );
                }
            }
            
            //for Mohawk Account team update (Key: Territory Id, Value: Territory User Ids)
            /*  if(terUsersMap.containsKey(tu.Territory__c) && (tu.Role__c == resRole ||  tu.Role__c == commRole)){

terUsersMap.get(tu.Territory__c).put(tu.user__c, tu.Role__c);

}else if(tu.Role__c == resRole ||  tu.Role__c == commRole){

Map<Id,String> userMap = new Map<Id,String>(); 
userMap.put(tu.user__c, tu.Role__c);
terUsersMap.put(tu.Territory__c, userMap);
} */
            
        }//End of for
        
        
        //Map with Public group name with set of group mamber ids
        Map<String, Set<Id>> pubGrpUserIdsMap= new Map<String, Set<Id>>();
        //Map with public group Name with group Id
        Map<String,Id> publicGrpMap  = new Map<String,Id>();
        
        for(Group  g : [SELECT Id, Name, (SELECT Group.Name, GroupId, UserOrGroupId FROM GroupMembers) 
                        FROM Group WHERE type = 'Regular' AND  Name IN: terUserIdsMap.keySet() AND Name != null]){ 
                            
                            if(pubGrpUserIdsMap.containsKey(g.Name)){
                                
                                //Considering only user as group members for that using filter on user id starts with '005' 
                                for(GroupMember gm : g.GroupMembers){
                                    if( String.valueOf(gm.UserOrGroupId).startsWith('005')){
                                        pubGrpUserIdsMap.get(g.Name).add(gm.UserOrGroupId);
                                    }
                                }
                                
                            }else{
                                Set<Id> userIds = new Set<Id>(); 
                                for(GroupMember gm : g.GroupMembers){
                                    if( String.valueOf(gm.UserOrGroupId).startsWith('005')){
                                        userIds.add(gm.UserOrGroupId);
                                    }
                                }
                                pubGrpUserIdsMap.put(g.Name, userIds);
                            }
                            
                            //Public group Map to store GroupName with GroupId;
                            publicGrpMap.put(g.Name, g.Id);
                            
                        }//End of For 
        
        system.debug('@@@@terUserIdsMap@@@'+terUserIdsMap);
        system.debug('@@@@pubGrpUserIdsMap@@@'+pubGrpUserIdsMap);
        system.debug('@@@@publicGrpMap@@@'+publicGrpMap);
        userCompare(terUserIdsMap, pubGrpUserIdsMap, publicGrpMap); 
        
        //deleteMHKAccountTeam(deletedRecordids,dMatIdsMap,dmatMap);
        /*for(GroupMember gm : [SELECT Group.Name, GroupId, UserOrGroupId FROM GroupMember
WHERE Group.Name IN: terUserIdsMap.keySet()]){
}*/
        
    }
    
    
    //Method to compare the both territory users and public group users and found any differene then udpate the public group members
    global void userCompare( Map<String, List<Territory_User__c>>  terMap,  Map<String, Set<Id>> pubGrpUserIdsMap,  Map<String, Id> publicGrpMap){
        
        list<GroupMember> insertGrpMemList = new list<GroupMember>();
        List<Id> deleteGrpIdList = new List<Id>();
        Set<Id> deleteGrpUserIdList = new Set<Id>();
        Set<Id> existingDelUseList = new Set<Id>();
        
        system.debug('@@@@termap.keySet()@@@'+termap.keySet());
        for(String terName : termap.keySet()){
            //Compare both the territory & public group user sets
            /*if(terMap.get(terName).equals(pubGrpUserIdsMap.get(terName))){
return;
}  */
            System.debug('##Territory name'+terName);  
            System.debug('##Territory pubGrpUserIdsMap.get(terName)'+pubGrpUserIdsMap.get(terName)); 
            
            If(pubGrpUserIdsMap.containsKey(terName)){
                
                
                system.debug('terMap.get(terName) != null<<<'+terMap.get(terName) != null+'<<<<pubGrpUserIdsMap.get(terName) == null<<<<'+(pubGrpUserIdsMap.get(terName) == null));
                //Condition : If both the territory having users and public group not having any users
                if(terMap.get(terName) != null && pubGrpUserIdsMap.get(terName) == null){
                    // add new team members to public group
                    
                    for(Territory_User__c tUser : terMap.get(terName)){
                        if(tUser.IsDeleted == false){
                            GroupMember grpMember = new GroupMember(GroupId =  publicGrpMap.get(terName),
                                                                    UserOrGroupId = tUser.User__c);
                            system.debug('##Insidet first Insert ::'+grpMember);
                            if(!checkUser(grpMember , insertGrpMemList)) //Add By LJ Jan 30
                                insertGrpMemList.add(grpMember); 
                        }
                    }
                    
                }//End of inner if
                
                
                //Condition : If both the territory and public group not null
                else if(terMap.get(terName) != null && pubGrpUserIdsMap.get(terName) != null){
                    system.debug('******terMap.get(terName)***'+terMap.get(terName));
                    
                    for(Territory_User__c tUser : terMap.get(terName)){
                        //add new team members to public group
                        system.debug('!. tUser :: '+tUser);
                        if(!pubGrpUserIdsMap.get(terName).contains(tUser.User__c) && tUser.IsDeleted == false){
                            system.debug('1. if :: ');
                            GroupMember grpMember = new GroupMember(GroupId = publicGrpMap.get(terName),
                                                                    UserOrGroupId = tUser.User__c);
                            system.debug('##Insidet Second Insert ::'+grpMember);
                            if(!checkUser(grpMember , insertGrpMemList))//Add By LJ Jan 30
                                insertGrpMemList.add(grpMember); 
                        } else //Add By LJ Jan 30
                            if(pubGrpUserIdsMap.get(terName).contains(tUser.User__c) && tUser.IsDeleted == false)
                        {
                            system.debug('1:--- if :: '+tUser.id+'---user---'+tuser.User__c);
                            existingDelUseList.add(tUser.User__c);
                        }
                        //Remove existing team members from public group
                        //
                        system.debug('2:---tUser.id---'+tUser.id+'----tUser.IsDeleted----'+tUser.IsDeleted+'---user---'+tuser.User__c+'----pubGrpUserIdsMap.get(terName).contains(tUser.User__c)----' +pubGrpUserIdsMap.get(terName).contains(tUser.User__c)+'----------(existingDelUseList.contains(tUser.User__c))'+(existingDelUseList.contains(tUser.User__c)));
                        if(tUser.IsDeleted == true && pubGrpUserIdsMap.get(terName).contains(tUser.User__c) && !(existingDelUseList.contains(tUser.User__c)) ){ //Add By LJ Jan 30 !existingDelUseList.contains(tUser.User__c)
                            system.debug('1. if :: ');
                            deleteGrpIdList.add(publicGrpMap.get(terName)); //Add group id to list to query group member records to delete
                            deleteGrpUserIdList.add(tUser.User__c); //Add userid to list to query group member records to delete
                        }
                    } 
                    
                    /* 
if(terMap.get(terName).size() <= pubGrpUserIdsMap.get(terName).size()){ 

for(id pGUserId : pubGrpUserIdsMap.get(terName)){

if(!terMap.get(terName).contains(pGUserId)){

deleteGrpIdList.add(publicGrpMap.get(terName)); //Add group id to list to query group member records to delete
deleteGrpUserIdList.add(pGUserId); //Add userid to list to query group member records to delete

}//End of If 
}//end of For

}//End of main If */
                }//IF
            }
            
        }//End of for(String terName : termap.keySet())
        
        List<GroupMember> delGrpMemList;
        system.debug('@@@@deleteGrpIdList@@@'+deleteGrpIdList); 
        system.debug('@@@@deleteGrpUserIdList@@@'+deleteGrpUserIdList); 
        if(deleteGrpIdList != null && !deleteGrpIdList.isEmpty())
            delGrpMemList = [SELECT Id FROM GroupMember WHERE GroupId in: deleteGrpIdList AND UserOrGroupId in: deleteGrpUserIdList];
        
        if(delGrpMemList != null && !delGrpMemList.isEmpty()){
            system.debug('@@@@delGrpMemList@@@'+delGrpMemList); 
            
            Database.DeleteResult [] delResult = Database.delete(delGrpMemList, false);
            
            for ( Integer i = 0; i < delResult.size(); i++ ) {
                if ( !delResult.get(i).isSuccess() ) {
                    if ( ErrorRecords == null ) {
                        ErrorRecords = 'Operation, Record Id, Status Code, Error Message \n';
                    }
                    for ( Database.Error theError : delResult.get(i).getErrors() ) {
                        string recordString =  '"'+'Delete'+'","'+delGrpMemList.get(i).Id+'","'+theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                        ErrorRecords = ErrorRecords + recordString;                         
                    }
                }
            }
        }
        // database.delete(delGrpMemList, false);
        
        
        if(insertGrpMemList != null && !insertGrpMemList.isEmpty()){
            system.debug('@@@@insertGrpMemList@@@'+insertGrpMemList);
            
            Database.SaveResult [] updateResult;
            updateResult= Database.insert(insertGrpMemList, false);
          
            
            
            
            /*--Only for test class to cause dml exception-Added by Susmitha on Apr 3,2018---*/
            if(Test.isRunningTest()){
                GroupMember grpMember = new GroupMember(GroupId =  publicGrpMap.values()[0]);
                insertGrpMemList.add(grpMember); 
                updateResult= Database.insert(insertGrpMemList, false);
            }
            /*--End Only for test class to cause dml exception-Added by Susmitha on Apr 3,2018 ----*/
            
            
            for ( Integer i = 0; i < updateResult.size(); i++ ) {
                if ( !updateResult.get(i).isSuccess() ) {
                    if ( ErrorRecords == null ) {
                        ErrorRecords = 'Operation, Record Id, Status Code, Error Message \n';
                    }
                    for ( Database.Error theError : updateResult.get(i).getErrors() ) {
                      string  recordString = '"'+'Insert'+'","'+insertGrpMemList.get(i).Id+'","'+theError.getStatusCode()+'","'+ theError.getMessage() +'"\n';
                        ErrorRecords = ErrorRecords + recordString;  
                    }
                }
            }
        }
                        system.debug('Error Records 1::::::::::::'+ErrorRecords);

        // database.insert(insertGrpMemList, false);
        
        
        
        
        
    }//End of Method
    //Method to check the user already existes in the public group
    public boolean checkUser(GroupMember grpObj , list<GroupMember> grpMembersList)
    {
        boolean isTrue = false;
        for(GroupMember grp: grpMembersList)
        {
            if(grp.UserOrGroupId == grpObj.UserOrGroupId && grp.GroupId ==grpObj.GroupId)
            {
                isTrue = true;
            }
        }
        return isTrue;
    }
    
    
    global void finish(Database.BatchableContext BC) {
        if (ErrorRecords != null)
            system.debug('Error Records:::::'+ErrorRecords);
        NotificationUtility.sendBatchExceptionNotification(bc.getJobId(),ErrorRecords, 'Territory Group Errors');
    }
    
    //Schedulable 
    global void execute(SchedulableContext SC){
        Integer batchSize = 10;
        Batch_Query__mdt [] QureyList = [SELECT Batch_Size__c FROM Batch_Query__mdt where Class_Name__c = 'TerritoryCheckPublicGroup_Batch'];
        If(QureyList.size() > 0)
            batchSize = Integer.valueOf(QureyList[0].Batch_Size__c) ;            
        String jobId = Database.executeBatch(new TerritoryCheckPublicGroup_Batch(),batchSize); 
        System.debug('*******Job id***'+jobId);
    }
    
}