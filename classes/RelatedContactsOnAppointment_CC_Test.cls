/**************************************************************************

Name : RelatedContactsOnAppointment_CC_Test 

===========================================================================
Purpose :   This class is used for RelatedContactsOnAppointment_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        21/March/2017     Created 
***************************************************************************/
// see the EventWhoRelation table therefore, we need to use (SeeAllData=true)
@isTest
private class RelatedContactsOnAppointment_CC_Test { 
    
    static List<Account> resAccListForInvoicing;
    static List<Account> accountList;
    
    
    
    @isTest(SeeAllData=true)
    static void testMethod2(){     
        test.startTest();
        try{
            RelatedContactsOnAppointment_CC.ContactViewModel contRelInfo = new RelatedContactsOnAppointment_CC.ContactViewModel();
            RelatedContactsOnAppointment_CC.getContactFieldsInfo(null);
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Business_Type__c = 'Residential';
                resAccListForInvoicing[i].Customer_Group__c = 'Open Line';
            }
            
            update resAccListForInvoicing;
            system.assert(resAccListForInvoicing!=null);
            
            System.debug(LoggingLevel.INFO, '*** :resAccListForInvoicing ' +resAccListForInvoicing );
            Contact contact = new Contact(LastName = 'Test Contact 2', AccountId = resAccListForInvoicing[0].Id, Email='test2@test.com', MobilePhone='1234567899');
            insert contact;
            System.debug(LoggingLevel.INFO, '*** contact2: ' + contact);
            contact = [SELECT AccountId FROM Contact WHERE Id =:contact.Id Limit 1];
            System.debug(LoggingLevel.INFO, '*** contact.AccountId: ' + contact.AccountId);
            List<Event> events = Utility_Test.createEvents(false, 1, resAccListForInvoicing);
            Event evt = events[0];
            evt.WhoId = contact.Id;
            evt.WhatId = resAccListForInvoicing[0].Id;
            insert evt;
            system.assert(evt!=null);
           
            System.debug(LoggingLevel.INFO, '*** evt.WhatId: ' + evt.WhatId);
            
            List<RelatedContactsOnAppointment_CC.ContactViewModel> contactview = RelatedContactsOnAppointment_CC.getContactFieldsInfo(evt.Id);
            contactview[0].isSelect = false;
            contactview[0].isExistingDataInDB = true;
            String jStr = JSON.serialize(contactview);
            contRelInfo = RelatedContactsOnAppointment_CC.updateEventRelations(evt.Id, jStr);
            
            Contact con = new Contact(LastName = 'Test Contact 3', AccountId = resAccListForInvoicing[0].Id, Email='test3@test.com', MobilePhone='1234567899');
            insert con;
            evt.WhoId = con.Id;
            update evt;
            
            List<RelatedContactsOnAppointment_CC.ContactViewModel> contactview1 = RelatedContactsOnAppointment_CC.getContactFieldsInfo(evt.Id);
            contactview1[0].isSelect = true;
            contactview[0].isExistingDataInDB = false;
            String jStr1 = JSON.serialize(contactview1);
            contRelInfo = RelatedContactsOnAppointment_CC.updateEventRelations(evt.Id, jStr1);
            
            
            system.assertEquals(evt!= null, true);
            
            
        }
        catch(exception e){
            system.debug('error e'+e);
        }
        test.stopTest();
    }
    @isTest
    static void testMethod3(){     
        test.startTest();
        try{
            RelatedContactsOnAppointment_CC.ContactViewModel contRelInfo = new RelatedContactsOnAppointment_CC.ContactViewModel();
            RelatedContactsOnAppointment_CC.getContactFieldsInfo(null);
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Business_Type__c = 'Residential';
                resAccListForInvoicing[i].Customer_Group__c = 'Open Line';
            }
            
            update resAccListForInvoicing;
            system.assert(resAccListForInvoicing!=null);
            
            System.debug(LoggingLevel.INFO, '*** :resAccListForInvoicing ' +resAccListForInvoicing );
            Contact contact = new Contact(LastName = 'Test Contact 2', AccountId = resAccListForInvoicing[0].Id, Email='test2@test.com', MobilePhone='1234567899');
            insert contact;
            System.debug(LoggingLevel.INFO, '*** contact2: ' + contact);
            contact = [SELECT AccountId FROM Contact WHERE Id =:contact.Id Limit 1];
            System.debug(LoggingLevel.INFO, '*** contact.AccountId: ' + contact.AccountId);
            List<Event> events = Utility_Test.createEvents(false, 1, resAccListForInvoicing);
            Event evt = events[0];
            evt.WhatId = resAccListForInvoicing[0].Id;
            insert evt;
            System.debug(LoggingLevel.INFO, '*** evt.WhatId: ' + evt.WhatId);
            
            List<RelatedContactsOnAppointment_CC.ContactViewModel> contactview = RelatedContactsOnAppointment_CC.getContactFieldsInfo(evt.Id);
            contactview[0].isSelect = false;
            contactview[0].isExistingDataInDB = true;
            String jStr = JSON.serialize(contactview);
            contRelInfo = RelatedContactsOnAppointment_CC.updateEventRelations(evt.Id, jStr);
            
            Contact con = new Contact(LastName = 'Test Contact 3', AccountId = resAccListForInvoicing[0].Id, Email='test3@test.com', MobilePhone='1234567899');
            insert con;
            evt.WhoId = con.Id;
            update evt;
            system.assert(evt!=null);
            
            List<RelatedContactsOnAppointment_CC.ContactViewModel> contactview1 = RelatedContactsOnAppointment_CC.getContactFieldsInfo(evt.Id);
            contactview1[0].isSelect = true;
            contactview[0].isExistingDataInDB = false;
            String jStr1 = JSON.serialize(contactview1);
            contRelInfo = RelatedContactsOnAppointment_CC.updateEventRelations(evt.Id, jStr1);
            
            system.assertEquals(evt!= null, true);
        }
        catch(exception e){
            system.debug('error e'+e);
        }
        test.stopTest();
    }
    
    
}