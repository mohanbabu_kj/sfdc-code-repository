@isTest
public class EventTriggerHandler_Test {
    
    static testMethod void  testForvalidateContact(){
        System.runAs(Utility_Test.ADMIN_USER){
            Utility_Test.getDefaultConfCusSetting();
            List<Account> accList = Utility_Test.createAccountsForNonInvoicing(true, 1);
            List<Contact> contList = Utility_Test.createContacts(true, 1, accList);
            List<Contact> contList1 = Utility_Test.createContacts(true, 1);
            
            // Contact from Account
            List<Event>  eveList = Utility_Test.createEvents(false, 1, accList);
            for(Integer i=0;i<eveList.size();i++){
                eveList[i].WhoId = contList[i].Id;
            }
            insert eveList;
            
            eveList.clear();
            system.assert(eveList.size()==0);
            eveList = Utility_Test.createEvents(false, 1, accList);
            //Contact not belong to Account
            for(Integer i=0;i<eveList.size();i++){
                eveList[i].WhoId = contList1[i].Id;
            }
            try{
                insert eveList;
            }
            catch (DmlException e){
                
            }
            system.assert(eveList[0].WhoId == contList1[0].Id);
            //EventTriggerHandler.updateAppt( eveList, false, true );
        }
    }
     static testMethod void  testForvalidateContact1(){
        System.runAs(Utility_Test.ADMIN_USER){
            Utility_Test.getDefaultConfCusSetting();
            List<Account> accList = Utility_Test.createAccountsForNonInvoicing(true, 1);
            List<Contact> contList = Utility_Test.createContacts(true, 1, accList);
            List<Contact> contList1 = Utility_Test.createContacts(true, 1);
            set<Id> eventIds=new set<Id>();
            List<Territory__c> terrList = Utility_Test.createTerritories(true, 2);

            Mohawk_Account_Team__c mt=new Mohawk_Account_Team__c();
            mamd__Multiday_Waypoint__c mm=new mamd__Multiday_Waypoint__c();
            insert mm;
            
            List<Mohawk_Account_Team__c> mohawkAccTeam = new List<Mohawk_Account_Team__c>();
            
            for(Integer i = 0; i < 1; i++){
                mohawkAccTeam.add(new Mohawk_Account_Team__c(Account__c = accList[i].Id, User__c = Utility_Test.ADMIN_USER.ID, Brands__c = 'Karastan;Aladdin;Horizon',
                                                             Territory__c = terrList[i].Id));                 
            }
            
                insert mohawkAccTeam;
             
            
            List<eventRelation> evetRelList=new List<eventrelation>();
            // Contact from Account
            List<Event>  eveList = Utility_Test.createEvents(false, 1, accList);
            for(Integer i=0;i<eveList.size();i++){
                eveList[i].WhoId = contList[i].Id;
                eveList[i].whatId=mohawkAccTeam[i].id;
                eveList[i].mamd__WA_Multiday_Waypoint__c=mm.Id;

                //eveList[0].whatId=null;//Commented by mudit - 21-08-18 Hitting null pointer exception. 
            }
            insert eveList;
            for(event e :eveList){
                e.mamd__WA_Multiday_Waypoint__c=mm.Id;
                eventIds.add(e.id);
            }
            //eveList.clear();
           // system.assert(eveList.size()==0);
           
            for(EventRelation eveRel: [SELECT Id, IsInvitee, EventId, RelationId FROM EventRelation
                                       WHERE EventId IN: eventIds]){
                                           eveRel.IsInvitee=false; // Changed By MUDIT - 21-08-18 - make it false. Beacause System.DmlException: Update failed. First exception on row 1 with id 0RE4C000000FM9gWAG; first error: FIELD_INTEGRITY_EXCEPTION, A What relation requires isInvitee to be false.: [IsInvitee].
                                           //eveRel.RelationId=
                                           evetRelList.add(eveRel);
                                           String relValue = String.valueOf(eveRel.RelationId);
                                           if(relValue.startsWith('003') && eveRel.IsInvitee){
                                           }
                                       }
            EventTriggerHandler.runUpdateTrigger=true;
            update evetRelList;            
            update eveList;
            //EventTriggerHandler.updateAppt( eveList, false, true );
        }
    }
    
    @isTest
    static void  testForUpdateCustomActivity(){
        System.runAs( Utility_Test.ADMIN_USER ){
            List<Event> eveList = createEvent();
            EventTriggerHandler.runUpdateTrigger = true;
            eveList[0].Subject = 'Test 342';
            update eveList;
        }
    }
    
    static List<Event> createEvent(){
        Utility_Test.getDefaultConfCusSetting();
        List<Account> accList = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
        for(Integer i=0;i<accList.size();i++){
            accList[0].Name = 'TEST EVENT TRIGGER HANDLER ACC 1';
        }
        insert accList;
        
        List<Contact> contList = Utility_Test.createContacts(true, 1, accList);
        List<Event>eveList = Utility_Test.createEvents(false, 1, accList);
        
        for(Integer i=0;i<eveList.size();i++){
            eveList[i].WhoId = contList[i].Id;
            eveList[i].WhatId = eveList[i].Account__c = accList[0].id;
        }
        insert eveList;
        return eveList;
    }
    
    @isTest
    static void  testForDeleteCustomActivity(){
        System.runAs( Utility_Test.ADMIN_USER ){
            List<Event> eveList = createEvent();
            delete eveList;
        }
    }
}