@isTest
public class Grid_AccessoriesHelperTest {
    static testMethod void testShowViewAccessoriesMethod(){
        list<Grid_Accessory_Field__mdt> gridData = [select Grid_Product_Category__c from Grid_Accessory_Field__mdt];
        //Creating Product record
        Product2 testProduct1 = new Product2();
        testProduct1.Brand_Code__c = 'b';
        testProduct1.Company__c = 'c';
        testProduct1.ERP_Product_Type__c = 'p';        
        testProduct1.Salesforce_Product_Category__c = 'Cushion';
        testProduct1.Name = 'testProduct';
        testProduct1.Accessory_Identifier_Code__c = '';
        insert testProduct1;

        Product2 testProduct2 = new Product2();
        testProduct2.Brand_Code__c = 'b';
        testProduct2.Company__c = 'c';
        testProduct2.ERP_Product_Type__c = 'p';        
        testProduct2.Salesforce_Product_Category__c = 'Cushion';
        testProduct2.Name = 'testProduct';
        testProduct2.Category__c = 'FLOORINGNACUSHIONPRODUCT';
        testProduct2.Sub_Category__c = 'Rubber';
        testProduct2.Accessory_Identifier_Code__c = 'Test';
        insert testProduct2;
        
        //Create Price Grid Records
        Price_Grid__c priceGridValue = new Price_Grid__c();
        priceGridValue.TM_Price__c = 12.0;
        priceGridValue.TM2_Price__c = 11.0;
        priceGridValue.TM3_Price__c = 10.0;
        priceGridValue.DM_Price__c = 9.0;
        priceGridValue.RVP_Price__c =8.0;
        priceGridValue.Price_Grid_Unique_Key__c = 'rc.zc.'+'testKey1';
        priceGridValue.Product__c = testProduct1.id;
        priceGridValue.Import_External_Id__c='testId';
        priceGridValue.Grid_Type__c = 'MBP';
        priceGridValue.Product_Unique_Key__c = 'testKey1';
        Date d =system.today();
        priceGridValue.Effective_Date__c = d;
        priceGridValue.Region__c = 'rc';
        priceGridValue.Zone__c = 'zc';
        priceGridValue.Company__c = 'c';
        priceGridValue.Selling_Style_Num__c = 'sel1';
        priceGridValue.Size__c = 'testS';
        priceGridValue.Backing__c = 'tb';
        insert priceGridValue;
        
        Price_Grid__c priceGridValue2 = new Price_Grid__c();
        priceGridValue2.TM_Price__c = 12.0;
        priceGridValue2.TM2_Price__c = 11.0;
        priceGridValue2.TM3_Price__c = 10.0;
        priceGridValue2.DM_Price__c = 9.0;
        priceGridValue2.RVP_Price__c =8.0;
        priceGridValue2.Price_Grid_Unique_Key__c = 'rc.zc.'+'testKey';
        priceGridValue2.Product__c = testProduct2.id;
        priceGridValue2.Import_External_Id__c='testId';
        priceGridValue2.Grid_Type__c = 'MBP';
        priceGridValue2.Product_Unique_Key__c = 'testKey';
        Date d2 =system.today();
        priceGridValue2.Effective_Date__c = d2;
        priceGridValue2.Region__c = 'rc';
        priceGridValue2.Zone__c = 'zc';
        priceGridValue2.Company__c = 'c';
        priceGridValue2.Selling_Style_Num__c = 'sel1';
        priceGridValue2.Size__c = 'testS';
        priceGridValue2.Backing__c = 'tb';
        insert priceGridValue2;
        
        boolean valueExists = Grid_AccessoriesHelper.showViewAccessories(String.valueOf(gridData[0].Grid_Product_Category__c),priceGridValue.id,'Price_Grid__c');
        boolean valueNotExist = Grid_AccessoriesHelper.showViewAccessories(String.valueOf(gridData[0].Grid_Product_Category__c),priceGridValue2.id,'Price_Grid__c');
        system.assert(valueExists==true,'the value exists');
        system.assert(valueNotExist==false,'the value doesnt exists');
    }
    static testMethod void testGetAccessoriesScenario(){
        
        //ACM: AccessoryCategoryMapping:[
        //Categories={FLOORINGNAHARDWOODPRODUCT, FLOORINGNALAMINATEPRODUCT, FLOORINGNARESILIENTPRODUCT}, 
        //Sub_Categories={Engineered, Laminate, Luxury Vinyl Tile and Plank, Rigid Plank, Solid, Vinyl Composite Tile}]
        
        //Creating Product record
        Product2 testProduct1 = new Product2();
        testProduct1.Brand_Code__c = 'b';
        testProduct1.Company__c = 'c';
        testProduct1.ERP_Product_Type__c = 'p';        
        testProduct1.Salesforce_Product_Category__c = 'Cushion';
        testProduct1.Name = 'testProduct';
        insert testProduct1;
        
        
        Product2 testProduct2 = new Product2();
        testProduct2.Brand_Code__c = 'b';
        testProduct2.Company__c = 'c';
        testProduct2.ERP_Product_Type__c = 'p';        
        testProduct2.Salesforce_Product_Category__c = 'Cushion';
        testProduct2.Name = 'testProduct';
        testProduct2.Category__c = 'FLOORINGNAHARDWOODPRODUCT';
        testProduct2.Sub_Category__c = 'Engineered';
        insert testProduct2;
        
        Product_Color__c pc = new Product_Color__c();
        pc.Product__c = testProduct1.id;
        pc.Color_Number__c = '01';
        pc.Selling_Color_Name__c = '01';
        insert pc;
        
        //Create Price Grid Records
        Price_Grid__c priceGridValue = new Price_Grid__c();
        priceGridValue.TM_Price__c = 12.0;
        priceGridValue.TM2_Price__c = 11.0;
        priceGridValue.TM3_Price__c = 10.0;
        priceGridValue.DM_Price__c = 9.0;
        priceGridValue.RVP_Price__c =8.0;
        priceGridValue.Price_Grid_Unique_Key__c = 'rc.zc.'+'testKey1';
        priceGridValue.Product__c = testProduct1.id;
        priceGridValue.Import_External_Id__c='testId';
        priceGridValue.Grid_Type__c = 'MBP';
        priceGridValue.Product_Unique_Key__c = 'testKey1';
        Date d =system.today();
        priceGridValue.Effective_Date__c = d;
        priceGridValue.Region__c = 'rc';
        priceGridValue.Zone__c = 'zc';
        priceGridValue.Company__c = 'c';
        priceGridValue.Selling_Style_Num__c = 'sel1';
        priceGridValue.Size__c = 'testS';
        priceGridValue.Backing__c = 'tb';
        insert priceGridValue;
        
        Price_Grid__c priceGridValue2 = new Price_Grid__c();
        priceGridValue2.TM_Price__c = 12.0;
        priceGridValue2.TM2_Price__c = 11.0;
        priceGridValue2.TM3_Price__c = 10.0;
        priceGridValue2.DM_Price__c = 9.0;
        priceGridValue2.RVP_Price__c =8.0;
        priceGridValue2.Price_Grid_Unique_Key__c = 'rc.zc.'+'testKey';
        priceGridValue2.Product__c = testProduct2.id;
        priceGridValue2.Import_External_Id__c='testId';
        priceGridValue2.Grid_Type__c = 'MBP';
        priceGridValue2.Product_Unique_Key__c = 'testKey';
        Date d2 =system.today();
        priceGridValue2.Effective_Date__c = d2;
        priceGridValue2.Region__c = 'rc';
        priceGridValue2.Zone__c = 'zc';
        priceGridValue2.Company__c = 'c';
        priceGridValue2.Selling_Style_Num__c = 'sel1';
        priceGridValue2.Size__c = 'testS';
        priceGridValue2.Backing__c = 'tb';
        insert priceGridValue2;
        
        //Create Product Relationship Records
        Product_Relationship__c productRelationship = new Product_Relationship__c();
        productRelationship.product__c = testProduct1.id;
        productRelationship.Related_Product__c = testProduct2.id;
        productRelationship.Source_Customer_Group_Id__c = Label.Default_Customer_Group_Number;
        productRelationship.Relations_Type__c = Label.Residential_Accessory_Relationship_Type;
        productRelationship.Source_Product_Unique_Id__c = 'test1';
        productRelationship.Target_Product_Unique_Id__c = 'test2';
        insert productRelationship;
        
        list<Grid_Accessory_Field__mdt> gridDataTrims = [select Grid_Product_Category__c,DeveloperName  from Grid_Accessory_Field__mdt where Accessory_Grid__c = 'TRIMS_AND_MOLDING' and DeveloperName  LIKE 'PG%'];
        list<Grid_Accessory_Field__mdt> gridData = [select Grid_Product_Category__c from Grid_Accessory_Field__mdt where Accessory_Grid__c = 'CUSHION'];
        List<Grid_Accessory_Trim_Ma__mdt>  accessoryTrimMapping = [select Accessory_Code__c,Accessory_Type_Name__c,order__c,Grid_Product_Category__c  from Grid_Accessory_Trim_Ma__mdt where Grid_Product_Category__c =:gridDataTrims[0].Grid_Product_Category__c]; 
        
        //Calling Accesories Function
        test.startTest();
        List<Object> listOfAccessoriesPriceGridTrims = Grid_AccessoriesHelper.getAccessories(String.valueOf(priceGridValue.id),String.valueOf(accessoryTrimMapping[0].Grid_Product_Category__c),'Price_Grid__c');
        System.Debug('### test - listOfAccessoriesPriceGridTrims: ' + listOfAccessoriesPriceGridTrims);
        List<Object> listOfAccessoriesPriceGridCushion = Grid_AccessoriesHelper.getAccessories(String.valueOf(priceGridValue.id),String.valueOf(gridData[0].Grid_Product_Category__c),'Price_Grid__c');
        System.Debug('### test - listOfAccessoriesPriceGridCushion: ' + listOfAccessoriesPriceGridCushion);
        
        

        test.stopTest();
        
        
        

        //Assert Conditions
        //List<Grid_AccessoriesHelper.ViewData> listOfViewDataCushion = (List<Grid_AccessoriesHelper.ViewData>)JSON.deserialize(
        //    JSON.serialize(listOfAccessoriesPriceGridCushion), List<Grid_AccessoriesHelper.ViewData>.class);
        //List<Grid_AccessoriesHelper.ViewData> listOfViewDataTrims = (List<Grid_AccessoriesHelper.ViewData>)JSON.deserialize(
        //    JSON.serialize(listOfAccessoriesPriceGridTrims), List<Grid_AccessoriesHelper.ViewData>.class);
        //system.assert(listOfAccessoriesPriceGridCushion.size()>0,'listContainsData');
        system.assert(listOfAccessoriesPriceGridTrims.size()>0,'listContainsData');
    }
}