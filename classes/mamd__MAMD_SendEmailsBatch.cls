/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MAMD_SendEmailsBatch implements Database.Batchable<SObject>, Database.Stateful {
    global MAMD_SendEmailsBatch() {

    }
    global void execute(Database.BatchableContext bc, List<mamd__Multiday_Route_Template_User__c> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
