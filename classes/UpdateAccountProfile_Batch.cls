/**************************************************************************

Name : UpdateAccountProfile_Batch 

===========================================================================
Purpose : Batch class to update account profiles on nightly basis 
          And also update the Two_Sales_Teams__c based on Account's Mohawk Account teams BMF users 
===========================================================================
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Anand       26/April/2017        Created          CSR:

***************************************************************************/

global class UpdateAccountProfile_Batch implements Database.Batchable<sObject>,Schedulable {
   
   //public String query = 'SELECT Id, Two_Sales_Teams__c From Account_Profile__c where id = \'a042C0000015xhJQAQ\'';
     public String query = 'SELECT Id, Two_Sales_Teams__c,No_Karastan__c From Account_Profile__c';                                   
    global UpdateAccountProfile_Batch() {
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<sObject> scope) {

        List<Account_Profile__c> updateAcProfileList = new List<Account_Profile__c>();
        List<Account_Profile__c> updteAcProfList = (List<Account_Profile__c>)scope;
        Map<id,Account_Profile__c> acProfileMap =  new Map<id,Account_Profile__c>(updteAcProfList);
        List<Mohawk_Account_Team__c> updateMHKAcTeamList = new  List<Mohawk_Account_Team__c>();
        System.debug('Account Profile***'+scope);
        System.debug('acProfileMap***'+acProfileMap);
        
        Map<id,List<Mohawk_Account_Team__c>> mhkAcTeamMap  = new Map<id,List<Mohawk_Account_Team__c>>();
        for(Account ac : [Select Id, Account_Profile__c,
                         (SELECT Id,BMF_Team_Member__c FROM Mohawk_Account_Teams__r)
                         FROM Account WHERE Account_Profile__c in:acProfileMap.keySet()]){
            
            if(ac.Mohawk_Account_Teams__r != null){
                 updateMHKAcTeamList.addAll(ac.Mohawk_Account_Teams__r);
                 if(mhkAcTeamMap.containsKey(ac.Account_Profile__c)){
                    
                      mhkAcTeamMap.get(ac.Account_Profile__c).addAll(ac.Mohawk_Account_Teams__r);
                 }else{
                      //List<Mohawk_Account_Team__c> 
                      mhkAcTeamMap.put(ac.Account_Profile__c, ac.Mohawk_Account_Teams__r);
                 }
            }

        }//For Loop Account 

       
        // Consider all Mohawk Account Teams for each Account Profile(based on All accounts of Account Profile) and check BMF_Team_Member__c value 
        // based on that update account profile Two_Sales_Teams__c as true.
        for(Id acProfileId: mhkAcTeamMap.keySet()){ 

            Boolean BMFUSer = false;
            Boolean regularUser = false;
            Boolean twoSalesTeam = false;
            for(Mohawk_Account_Team__c mhk : mhkAcTeamMap.get(acProfileId)){
                
                if(mhk.BMF_Team_Member__c == true){
                      BMFUSer =  true;
                }else{

                     regularUser = true;
                }
                
            }//For MHK
            Account_Profile__c acProfile = new Account_Profile__c();
            //Scenario 1 : 1 Account, 1 Sales Team
            //Scenario 2  1 Account, 2 Sales Teams
            //Scenario 3  Multiple Accounts, 1 Sales Team
            //Scenario 4  Multiple Accounts, 2 Sales Teams
            if(BMFUSer == true && regularUser == true){  
                twoSalesTeam = true;
                System.debug('****BMFUSer == true && regularUser == true**');
            }else if(BMFUSer == true && regularUser == false){ //All BMF checkbox true
                twoSalesTeam = false;
                 System.debug('****(BMFUSer == true && regularUser == false)**'); 
            }else if(BMFUSer == false && regularUser == true){ //All BMF checkbox false
                twoSalesTeam = false;
                System.debug('****BMFUSer == false && regularUser == true**'); 
            }
            System.debug('****BMFUSer**'+BMFUSer);
            System.debug('****regularUser**'+regularUser);
            System.debug('****twoSalesTeam**'+twoSalesTeam);
            updateAcProfileList.add(new Account_Profile__c(Id = acProfileId, Two_Sales_Teams__c = twoSalesTeam ));

        }//End of for looop

        // Update all Account profiles and 
        //checking if the mhkAcTeamMap does not conatains the record to avoid duplicate records add to update list
        Set<Account_Profile__c> setaps = new Set<Account_Profile__c>();//added bhanu
        for(Account_Profile__c acPro: acProfileMap.values()){
            setaps.add(acPro);//added bhanu
            if(!mhkAcTeamMap.containsKey(acPro.Id)){
                 updateAcProfileList.add(acPro);
           }
               
        }
        List<Mohawk_Account_Team__c> listMat = [select id,Brands__c,Account_Profile__c,account_profile__r.No_Karastan__c from Mohawk_Account_Team__c where account_profile__c=:setaps];
        List<Account_Profile__c>  accprofList = new List<Account_Profile__c>();
        for(Mohawk_Account_Team__c mt : listMat){
        //added bhanu
        Account_Profile__c accpro = new Account_Profile__c();
        if(mt.account_profile__c != null){
            if(mt.Brands__c != null && (mt.Brands__c).contains('Karastan')){
                mt.account_profile__r.No_Karastan__c  = false;
                accpro.id = mt.account_profile__c;
            }
            else{
                mt.account_profile__r.No_Karastan__c  = true;
                accpro.id = mt.account_profile__c;
            }
          
            updateAcProfileList.add(accpro);
            }//end
        }
        System.debug('******'+updateAcProfileList);

          try{
                //Update Account Profiles
                /*if(!updateAcProfileList.isEmpty()){
                    update updateAcProfileList;
                }*/
                //Update Related Mohawk Account Teams to reflect Account Profile changes
                if(!updateMHKAcTeamList.isEmpty()){
                    update updateMHKAcTeamList;
                }
            }Catch(DMLException e){
                system.debug('!!!!!!!Error!!!'+e.getMessage());
           
        }

    }//Method Execute
        
    
    global void finish(Database.BatchableContext BC) {
        //Post commit Logic
    }

    global void execute(System.SchedulableContext SC){
         Id batchId = Database.executeBatch(new UpdateAccountProfile_Batch(),10);
    }
}