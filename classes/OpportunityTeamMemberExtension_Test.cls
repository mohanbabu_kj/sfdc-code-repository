/**************************************************************************

Name : OpportunityTeamMemberExtension_Test

===========================================================================
Purpose : This class is used for OpportunityTeamMemberExtension
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit         27/Sepetmber/2017     Created    1
2.0        Susmitha      26/March/2018        Modified
***************************************************************************/
@isTest
private class OpportunityTeamMemberExtension_Test {
    static testMethod void testMethod1() {
        
        List<Opportunity> OppList = Utility_Test.createOpportunities( false ,1 );
        for(Integer i=0; i < OppList.size(); i++){
            OppList[i].StageName = 'Bidding';
            OppList[i].Status__c = 'In Process';
        }
        insert OppList;
        system.assert(OppList[0].Status__c=='In Process');
        
        ApexPages.StandardController sc = new ApexPages.StandardController( OppList[0] );
        OpportunityTeamMemberExtension OTE = new OpportunityTeamMemberExtension( sc );
        OTE.getoppId();
    }
}