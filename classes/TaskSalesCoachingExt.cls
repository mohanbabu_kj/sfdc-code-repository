public with sharing class TaskSalesCoachingExt {

	private Coaching_Conversation__c coachingCon;

	public Map<String, String> recordApiList{get;set;}

	public List<String> apiList;

    public Boolean isBMFUser{get;set;}
    public Boolean hasLeadAccess{get;set;}
    public Boolean hadProjectAccess{get;set;}
 
    public TaskSalesCoachingExt(ApexPages.StandardController stdController) {
        coachingCon = new Coaching_Conversation__c();
        coachingCon = (Coaching_Conversation__c)stdController.getRecord();
        
        apiList = new List<String>();
        apiList.add('Activities_Tasks_completed_VF_Page');
        apiList.add('Appointment_Checkouts_VF');
        apiList.add('Appointment_pure_Appointment_Efficiency');
        apiList.add('Completed_Appointments_objectives_VF');
        apiList.add('Leads_builder_and_multi_family_only_VF');
        apiList.add('Projects_in_the_pipeline_VF');

        // to display Leads_builder_and_multi_family_only_VF report BMF user or user have access to Lead object
        // to display Projects_in_the_pipeline_VF report BMF user or user have access to Opportunity object
        isBMFUser = UtilityCls.checkUserPermissionSet(UserInfo.getUserId(), 'BMF_Permission');
        hasLeadAccess = validateObjectAccess('Lead');
        hadProjectAccess = validateObjectAccess('Opportunity');

    }

    public Boolean validateObjectAccess(String objectName) {
        Boolean isExists = false;

        list<ObjectPermissions> listData = [SELECT Id, SObjectType, PermissionsRead, PermissionsCreate FROM ObjectPermissions
                                    WHERE parentid in (
                                        select id from permissionset where PermissionSet.Profile.Id =: UserInfo.getProfileId()
                                        ) and PermissionsRead = true and SObjectType =: objectName];
        if(listData.size()>0){
            isExists = true;
        }

        return isExists;
    }

    public String getReportIds() {

    	recordApiList = new Map<String, String>();

        for(Report rt : [SELECT Id,DeveloperName FROM Report where DeveloperName in: apiList ] ){
        	recordApiList.put(rt.DeveloperName, rt.Id);
        }
        System.debug('recordApiList' + recordApiList);


    	return 'data generated';
    }
}