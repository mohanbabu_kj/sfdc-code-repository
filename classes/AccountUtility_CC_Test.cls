/**************************************************************************

Name : AccountUtility_CC_Test

===========================================================================
Purpose : This test class is used for AccountUtility_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Krishnapriya   6/Nov/2017     Created         CSR:    
2.0        Susmitha       12/March/2018  Modified
***************************************************************************/
@isTest
public class AccountUtility_CC_Test {

    static testMethod void testAccountInfo(){
        test.startTest();
        list<string> strlist=new list<string>();
        strlist.add('test1');
        strlist.add('test2');

        Account acc=new Account(name='Test');
        insert acc;
        system.assert(acc.Name=='Test');
        AccountUtility_CC.WrapArchitectFolder wrapA=AccountUtility_CC.getAccountInfo(acc.Id);
        AccountUtility_CC.WrapArchitectFolder b= AccountUtility_CC.updateArchitectFolder(acc,new Map<string,List<string>>{ 'archFolders' => strlist },'DESKTOP');
        AccountUtility_CC.checkDeleteAccess( acc.id,'Account' );
        AccountUtility_CC.deleteRecord( acc.id );
        
        List<contact> conList = Utility_Test.createContacts(true,1);
        AccountUtility_CC.checkDeleteAccess( conList[0].id,'Contact' );
        test.stopTest();
}
}