/**************************************************************************

Name : SelectItem_Test
  
===========================================================================
Purpose :   This class is used for SelectItem
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit        27/September/2017     Created 
***************************************************************************/
@isTest
private class SelectItem_Test{
    static testMethod void testMethod1() {
        SelectItem SI_1 = new SelectItem( 'TEST', 'TEST', true );
        SelectItem SI_2 = new SelectItem( 'TEST', 'TEST' );
        System.assert(SI_1 != null);
        System.assert(SI_2 != null);
    }
    
}