/*************************************************************************************

Name	:	customDetailUtility 

======================================================================================
Purpose	:	This class is utility class, used to create custom detail page for :
			1. Account Profile
			2. Mohawk Account Team
======================================================================================
History:
--------
VERSION		AUTHOR		DATE			DETAIL		DESCRIPTION
1.0        	Mudit		05/June/2018     			Created
*************************************************************************************/
public with sharing class customDetailUtility{
	
	public static final string AP_SECTION_OBJECT_NAME = 'AP_Section__mdt';						//Contains Custom MetaData API Name
    public static final string AP_SECTION_FIELD_OBJECT_NAME = 'AP_Section_Fields__mdt';			//Contains Custom MetaData API Name
    public static final List<string>AP_SECTION_OBJECT_FIELD_LIST = new List<string>{			//List of fields to fetch
        'MasterLabel','Always_Show__c','Brand_Type__c',
		'Business_Type__c','Display_As__c',
		'ExpandableDefaultON__c','Parent_Section__c',
		'Section_Object__c','Sequence__c','Section_Type__c','ExpandableDefaultON_TABLET__c','ExpandableDefaultON_PHONE__c'
	};
                
	public static final List<string>AP_SECTION_FIELD_FIELD_LIST = new List<string>{				//List of fields to fetch
		'MasterLabel','Alignment__c',
		'AP_Section__c	','Field_Name__c',
		'isShow__c','Label_L__c','Label_M__c','Label_S__c',
		'Length__c','Sequence__c','Type__c'
	};
                            
	public static final string retationShipName = 'AP_Section_Fields1__r';						//Relationship Name
	public static final Map<string,string>sectionObjectMap = new Map<string,string>{			//Map of API name and Detail Page Name
		'Account_Profile__c' => 'Account Profile',
		'Mohawk_Account_Team__c' => 'Mohawk Account Team'
	};
	
	/*************************************************************************************
	This method is use to get the picklist value in list of string.
	*************************************************************************************
	public static List<string> getPicklistValues( string fieldName,string objectName ){
		List<string>returnList = new List<string>();
		Map<String,Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult objectMetadata = sObjectTypeMap.get( objectName ).getDescribe();
		Map<String, Schema.SObjectField> fieldsMap = objectMetadata.fields.getMap();
		for ( Schema.PickListEntry entry : fieldsMap.get(fieldName).getDescribe().getPickListValues() ) {
			returnList.add(entry.getValue());
		}
		return returnList;
	}*/
	
	/*************************************************************************************
	This method is use to get query string.
	*************************************************************************************/
	public static string generateSOQLQuery( string objectName,List<string>fields,string whereClause,List<string>childFieldList,string relationShipName,string childWhereClause,string limitVar ){
        string soql = 'SELECT ' + string.join(fields, ',');
        if( childFieldList != null && childFieldList.size() > 0 && relationShipName != null && relationShipName != '' ){
            soql += ',( SELECT ' + 
                string.join(childFieldList, ',') + 
                ' FROM ' + relationShipName + ( childWhereClause != null && childWhereClause != '' ? ' WHERE ' + childWhereClause : '') + ')';
        }
        soql += ' FROM ' + objectName;
        soql += ( whereClause != null && whereClause != '' ? ' WHERE '+whereClause : '' );
        soql += ( limitVar != null && limitVar != '' ? ' LIMIT '+limitVar : '');
        return soql;
    }
	
	/*************************************************************************************
	This method is use to record name from ID.
	*************************************************************************************/
	public static string getObjectName( id recordId ){
        return recordId.getSObjectType().getDescribe().getName();
    }
}