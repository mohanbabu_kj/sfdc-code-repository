/**************************************************************************
Name : TerritoryUsersTriggerHandler_Test 
---------------------------------------------------------------------------
Purpose : Test class for TerritoryUserTriggerHandler 
---------------------------------------------------------------------------
History:
--------
VERSION    	AUTHOR         	DATE           	DETAIL DESCRIPTION
1.0        	Sasi Naik     	2/May/2017    	Create  
1.1		   	Mohan Babu		1/30/19			Re-structured the test class
***************************************************************************/
@isTest
private class TerritoryUsersTriggerHandler_Test {
    static List<User> userList = new List<User>();
    static List<Territory__c> terrList = new List<Territory__c>();
    static List<Territory_User__c> terrUserList = new List<Territory_User__c>();
    static Id resTerrUserRecTypeId = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
    static List<Territory2> terr2List = new List<Territory2>();
    
    static void init(){
        userList = Utility_Test.createTestUsers(false, 5, 'Residential Sales User');
        userList[0].Business_Type__c = userList[1].Business_Type__c = userList[2].Business_Type__c = userList[3].Business_Type__c = userList[4].Business_Type__c = 'Residential';
        User usr = Utility_Test.getTestUser('tt', 'Residential Sales Operation');
        userList.add(usr);
        usr = Utility_Test.getTestUser('tt', 'Residential Sales User');
        userList.add(usr);
        insert userList;
        //userList[5]
        System.runAs ( Utility_Test.ADMIN_USER ) {
            
            Territory2Model terr2Model = [SELECT Id FROM Territory2Model LIMIT 1];
            Territory2Type terr2Type = [SELECT Id FROM Territory2Type LIMIT 1];
            
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 TM', 'D991_TM'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 DM', 'D991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'R991 - South Soft RVP ', 'R991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'S991 - S/S East SVP ', 'S991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 992 TM', 'D992_TM'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 992 DM', 'D992'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'R992 - South Soft RVP ', 'R992'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'S992 - S/S East SVP ', 'S992'));
            insert terr2List;
            
            //Territory Creation
            Territory__c terr = new Territory__c();
            terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
            terr.Territory_Code__c = '991';
            terr.Region_Code__c = '991';
            terr.Sector_Code__c = '991';
            terr.Name = 'District 991';
            terrList.add(terr);
            terr = new Territory__c();
            terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
            terr.Territory_Code__c = '992';
            terr.Region_Code__c = '992';
            terr.Sector_Code__c = '992';
            terr.Name = 'District 992';
            terrList.add(terr);
            insert terrList;
        }
    }
    
    @isTest
    static void createTerrUser(){
        init();
        System.runAs(Utility_Test.ADMIN_USER){
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-J8L', 'Territory Manager', userList[0].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-K8L', 'District Manager', userList[1].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-H8L', 'Business Development Manager', userList[2].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-M8L', 'Regional Vice President', userList[3].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-N8L', 'Senior Vice President', userList[4].Id));
            insert terrUserList;
            
            Territory_User__c tUser = terrUserList[0];
            tUser.User__c = null;
            update tUser;
        }
    }
    
    @isTest
    static void deleteTerrUserList(){
        createTerrUser();
        delete terrUserList;
    }
    
    public static Territory_User__c setTerritoryUserFields(Id terrId, Id recTypeId, String name, String role, String userId){
        Territory_User__c tUser = new Territory_User__c();
        tUser.RecordTypeId = recTypeId;
        tUser.Territory__c = terrId;
        tUser.Name = name;
        tUser.Role__c = role;
        tUser.User__c = userId;
        return tUser;
    }
    
    public static Territory2 setTerritory2Fields(Id terr2ModelId, Id terr2TypeId, String name, String devName){
        Territory2 terr2 = new Territory2();
        terr2.Territory2ModelId = terr2ModelId;
        terr2.Territory2TypeId = terr2TypeId;
        terr2.Name = name;
        terr2.DeveloperName = devName;
        return terr2;
    }
}