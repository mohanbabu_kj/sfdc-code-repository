/**************************************************************************

Name : RequestToQuote_CC_Test 

===========================================================================
Purpose : This tess class is used for Mashups_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         22/Feb/2017     Created 
***************************************************************************/
@isTest
private class RequestToQuotes_CC_Test {
    
    static List<Request_A_Quote__c> reqQuoteList;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        reqQuoteList =  Utility_Test.createRequestAQuoteList(true, 1);
        
        
    }
    
    static testMethod void testMethod1() {
        init();
        Test.startTest();
        RequestToQuote_CC.generateProject(reqQuoteList[0].Id, 'Bidding');
        list<Opportunity> oppList = [Select id, name from opportunity where stageName ='Bidding'];
        system.debug( '::::SIZE::::' + oppList.size() );
        system.assert(oppList.size() > 0);
        
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        init();
        Test.startTest();
        
        system.assert(reqQuoteList[0].Id!=null);
        RequestToQuote_CC.generateProject(reqQuoteList[0].Id, 'Closed');
        
        
        Test.stopTest();
    }
    
    
}