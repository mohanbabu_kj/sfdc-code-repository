/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MAMD_RoutingBatch implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful {
    global MAMD_RoutingBatch(Id userId, Id templateId, mamd__Multiday_User_Working_Hours__c userWorkingHours) {

    }
    global void execute(Database.BatchableContext bc, List<mamd__Multiday_Object_Priority__c> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
