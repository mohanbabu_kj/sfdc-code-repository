/**************************************************************************

Name : customDetailUtilityTest 

===========================================================================
Purpose : This tess class is used for customDetailUtility
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Radhika       8/June/2018     Created         
***************************************************************************/
@isTest
public class customDetailUtilityTest
{
    public static Account_Profile__c apRec;
    public static Account accRec;
    public static Mohawk_Account_Team__c matRec;
    public static Account_Profile_Settings__c apsRec;
    
    static void dataSetUp()
    {
        apsRec = new Account_Profile_Settings__c(name='Retail');
        insert apsRec;
        apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        apRec.Primary_Business__c = apsRec.id;
        
        insert apRec;
        
        accRec = new Account();
        accRec.name = 'test Account';
        accRec.Account_Profile__c = apRec.id;
        insert accRec;
        
        matRec = new Mohawk_Account_Team__c();
        matRec.account__c=accRec.id;
        matRec.Account_Profile__c = apRec.id;
        insert matRec;
        
    }
    static testMethod void testmethod1()
    {
        dataSetUp();
        String objName  = customDetailUtility.getObjectName(apRec.id);
                
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
	    List<String> strList = new List<String>(objectFields.keySet());
        customDetailUtility.generateSOQLQuery(objName,strList,null,null,null,null,'5');


    }
}