/**************************************************************************
Name : TerritoryUserAssociation_BatchTest 
===========================================================================
Purpose :           
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Nagendra       29/01/2019         Created          

***************************************************************************/
@isTest
public class TerritoryUserAssociation_BatchTest {
    static List<User> userList = new List<User>();
    static List<Territory__c> terrList = new List<Territory__c>();
    static List<Territory_User__c> terrUserList = new List<Territory_User__c>();
    static Id resTerrUserRecTypeId = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
    static List<Territory2> terr2List = new List<Territory2>();
    
    @testSetup
    static void init(){
        userList = Utility_Test.createTestUsers(false, 5, 'Residential Sales User');
        userList[0].Business_Type__c = userList[1].Business_Type__c = userList[2].Business_Type__c = userList[3].Business_Type__c = userList[4].Business_Type__c = 'Residential';
        User usr = Utility_Test.getTestUser('tt', 'Residential Sales Operation');
        userList.add(usr);
        usr = Utility_Test.getTestUser('tt', 'Residential Sales User');
        userList.add(usr);
        insert userList;
        Set<Id> userIds = new Set<Id>();
        for(User usr1: userList){
            userIds.add(usr1.Id);
        }
        System.debug('userList: '+ userList);
        System.runAs ( Utility_Test.ADMIN_USER ) {
            
            Territory2Model terr2Model = [SELECT Id FROM Territory2Model LIMIT 1];
            Territory2Type terr2Type = [SELECT Id FROM Territory2Type LIMIT 1];
            
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 TM', 'D991_TM'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 DM', 'D991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'R991 - South Soft RVP ', 'R991'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'S991 - S/S East SVP ', 'S991'));
            insert terr2List;
		
            System.debug('terr2List'+terr2List);
            Set<Id> terr2Ids = new Set<Id>();
            for(Territory2 terr: terr2List){
                terr2Ids.add(terr.Id);
            }
            //Territory Creation
            Territory__c terr = new Territory__c();
            terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
            terr.Territory_Code__c = '991';
            terr.Region_Code__c = '991';
            terr.Sector_Code__c = '991';
            terr.Name = 'District 991';
            terrList.add(terr);
            insert terrList;
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-J8L', 'Territory Manager', userList[0].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-K8L', 'District Manager', userList[1].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-H8L', 'Business Development Manager', userList[2].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-M8L', 'Regional Vice President', userList[3].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-N8L', 'Senior Vice President', userList[4].Id));
            insert terrUserList;
            System.debug('terrUserList: '+terrUserList);
            
            UserTerritory2Association usrTerr2Association = new UserTerritory2Association(Territory2Id = terr2List[0].Id, UserId = userList[6].Id);
            insert usrTerr2Association;
            
            
        }
    }
    
    @isTest
    static void createTerrUserAssociation(){
        Set<Id> terr2IdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();
        System.runAs ( Utility_Test.ADMIN_USER ) {
            for(Territory2 terr2: [SELECT Id from Territory2 WHERE DeveloperName IN ('D991_TM', 'D991', 'R991', 'S991') LIMIT 4]){
                terr2IdSet.add(terr2.Id);
            }
            Integer count = 0;
            for(User usr: [SELECT Id FROM User WHERE Profile.Name = 'Residential Sales User' and username LIKE 'uniq%' and CreatedDate = TODAY ORDER BY Id]){
                count += 1;
                userIdSet.add(usr.Id);
                if(count==5){
                    break;
                }
            }
            System.debug('terr2IdSet'+terr2IdSet);
            System.debug('userIdSet'+userIdSet);
            System.debug('UserTerr2Association: ' + [SELECT Id, Territory2Id, UserId FROM UserTerritory2Association WHERE Territory2Id IN: terr2IdSet]);
            delete([SELECT Id, Territory2Id, UserId from UserTerritory2Association Where Territory2Id IN: terr2IdSet AND UserId IN: userIdSet]);
            Id BatchInstanceID = Database.executeBatch(new TerritoryUserAssociation_Batch());
             String jobIdSch = System.schedule('Test my class', '0 0 0 ? * * *', new TerritoryUserAssociation_Batch());
        }
    }

    public static Territory_User__c setTerritoryUserFields(Id terrId, Id recTypeId, String name, String role, String userId){
        Territory_User__c tUser = new Territory_User__c();
        tUser.RecordTypeId = recTypeId;
        tUser.Territory__c = terrId;
        tUser.Name = name;
        tUser.Role__c = role;
        tUser.User__c = userId;
        return tUser;
    }
    
    public static Territory2 setTerritory2Fields(Id terr2ModelId, Id terr2TypeId, String name, String devName){
        Territory2 terr2 = new Territory2();
        terr2.Territory2ModelId = terr2ModelId;
        terr2.Territory2TypeId = terr2TypeId;
        terr2.Name = name;
        terr2.DeveloperName = devName;
        return terr2;
    }
}