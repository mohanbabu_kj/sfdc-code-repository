/**************************************************************************

Name : Claim_ApproversTrigger_Handler

===========================================================================
Purpose : Trigger Handler for Claim_ApproversTrigger to post on chatter when a Claim approval is inserted
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0         Susmitha       10/Nov/2017    Created         

***************************************************************************/
public class Claim_ApproversTrigger_Handler {
    
    public static void postOnChatter(List < Claim_Approvers__c > tiggerNew) {
        
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        set<id> claimApproverSet=new set<id>();
        for (Claim_Approvers__c ca: tiggerNew) {
            claimApproverSet.add(ca.claim__c);
        }
        Map<id,claim__c> claimMap=new Map<Id,claim__c>([select id,name,Account__r.name,Claim_Type__c from Claim__c where Id in :claimApproverSet]);
        
        for (Claim_Approvers__c ca: tiggerNew) {
            // Modified by krishnapriya on Nov 14 2017 to add @Mention User name
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            ConnectApi.LinkCapabilityInput linkInput = new ConnectApi.LinkCapabilityInput();
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            mentionSegmentInput.id = ca.Approver__c; //Mention user 
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            
            if(claimMap.containsKey(ca.claim__c)){
                textSegmentInput.text = ' A '+claimMap.get(ca.claim__c).Claim_Type__c +' Claim for '+claimMap.get(ca.claim__c).Account__r.name + ' requires your Approval.';
                messageBodyInput.messageSegments.add(textSegmentInput);
                
               // linkInput.urlName = +'Claim Number '+claimMap.get(ca.claim__c).name;// Add the URL
               // linkInput.url = '/'+ca.claim__c;              
              //  ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
              //  feedElementCapabilitiesInput.link = linkInput;
               // feedItemInput.capabilities = feedElementCapabilitiesInput;
            }
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = claimMap.get(ca.claim__c).id; //Claim ID
            
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);       
        }        
        ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
    }
}