global class AssignTerritoryToAccounts_Batch implements Database.Batchable < sObject > , Database.Stateful, Schedulable{
    
    global static string ErrorRecords;
    global static string SuccessRecords;
    global string download_Or_Update = 'Download'; //Default
    global string tempErrorRecs='';
    global string tempSuccessRecs='';
    global string tempFinalRecords='';
    global static string finalstr = 'Id;Name;Territory__c;TerrName;Education_Govt_Territory__c;'+
        'EduTerrName;Healthcare_Sr_Living_Territory__c;HcTerrNam;Workplace_Retail_Territory__c;WpTerrName \r\n';
    
    
    // Batch Constructor
    global AssignTerritoryToAccounts_Batch(String downld_Or_Updte) {
        download_Or_Update = downld_Or_Updte;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String Query;
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'AccountWithoutTerritoryAssignment'];
        If(QureyList.size() > 0)
            query = QureyList[0].Query_String__c ;
        
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        
        Map<String, String> ctryCodeMap = new Map<String, String>();
        Set<String> accPostalCodes = new Set<String>();
        Map<String, String> postalCodeWithTerritoryMap = new Map<String, String>() ;
        Map<String, String> ctryTerrMap = new Map<String, String>();
        Map<Id, String> terrIdNameMap = new Map<Id, String>();
        Map<Id, Account> updAccTerrMap = new Map<Id, Account>();
        List<Account> updAccTerrList = new List<Account>();
        Id terrCommRTId;
        
        if(ctryCodeMap.size()==0){
            Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple){
                ctryCodeMap.put(f.getValue(), f.getLabel());
            }
            System.debug('CountryCodeMap: ' + ctryCodeMap);
        }
        terrCommRTId    = UtilityCls.getRecordTypeInfo('Territory__c', 'Commercial');
        for(Territory__c terr: [SELECT Id, Name, Country__c FROM Territory__c
                                WHERE RecordTypeId =: terrCommRTId]){
                                    List<String> countryList = new List<String>();
                                    if(terr.Country__c != null){
                                        String country = terr.Country__c;
                                        countryList = country.split(';');
                                        for(String ctry: countryList){
                                            ctryTerrMap.put(ctry, terr.Id);
                                            terrIdNameMap.put(terr.Id, terr.Name);
                                        }
                                    }
                                }
        System.debug('Country List Territory Map: ' + ctryTerrMap);
        
        for (Account tempAcc : scope) {
            if(tempAcc.Business_Type__c == UtilityCls.COMMERCIAL && tempAcc.Strategic_Account__c == false){
                String zipcode = tempAcc.ShippingPostalCode;
                if(tempAcc.ShippingCountryCode == 'CA'){
                    zipcode = zipcode.substring(0, 3);
                }
                accPostalCodes.add(zipCode);
            }
        }
        
        System.debug('updateTerritoryForUpdate : accPostalCodes : ' + accPostalCodes );
        // update Territory field based on Zip code.
        List<Territory_Geo_Code__c> tGeoCodeList = new List<Territory_Geo_Code__c>();
        if(!accPostalCodes.isEmpty()){
            tGeoCodeList = [SELECT  Id,
                            Name,
                            Territory__c, Territory__r.Name, Territory__r.Market_Segment_Group__c,
                            Business_Type__c 
                            FROM
                            Territory_Geo_Code__c
                            WHERE Name in : accPostalCodes];
        }
        
        // update record
        for (Account acc : scope){
            if(acc.Business_Type__c == UtilityCls.COMMERCIAL && acc.Strategic_Account__c == false){
                String zipcode = acc.ShippingPostalCode;
                if(acc.ShippingCountryCode == 'CA'){
                    zipcode = zipcode.substring(0, 3);
                }
                System.debug('ZipCode : '+ zipcode);
                System.debug('GeoCode: ' + tGeoCodeList);
                if(acc.ShippingCountryCode == UtilityCls.US || acc.ShippingCountryCode == UtilityCls.CA){  
                    if(tGeoCodeList.size() > 0){ 
                        for(Territory_Geo_Code__c tGeoCode : tGeoCodeList){
                            if(zipcode == tGeoCode.Name && tGeoCode.Business_Type__c == acc.Business_Type__c){   
                                if(tGeoCode.Territory__r != null){
                                    if(tGeoCode.Territory__r.Market_Segment_Group__c != null && (tGeoCode.Territory__r.Market_Segment_Group__c).contains('Healthcare')){
                                        acc.Healthcare_Sr_Living_Territory__c = tGeoCode.Territory__c;
                                        //acc.Healthcare_Sr_Living_Territory__r.Name = tGeoCode.Territory__r.Name;
                                        updAccTerrMap.put(acc.Id, acc);
                                    }else if(tGeoCode.Territory__r.Market_Segment_Group__c != null && (tGeoCode.Territory__r.Market_Segment_Group__c).contains('Education')){
                                        acc.Education_Govt_Territory__c = tGeoCode.Territory__c;
                                        //acc.Education_Govt_Territory__r.Name = tGeoCode.Territory__r.Name;
                                        updAccTerrMap.put(acc.Id, acc);
                                    }else if(tGeoCode.Territory__r.Market_Segment_Group__c != null && (tGeoCode.Territory__r.Market_Segment_Group__c).contains('Workplace')){
                                        acc.Workplace_Retail_Territory__c = tGeoCode.Territory__c;
                                        //acc.Workplace_Retail_Territory__r.Name = tGeoCode.Territory__r.Name;
                                        updAccTerrMap.put(acc.Id, acc);
                                    }else{
                                        acc.Territory__c = tGeoCode.Territory__c;
                                        //acc.Territory__r.Name = tGeoCode.Territory__r.Name;
                                        updAccTerrMap.put(acc.Id, acc);
                                    }
                                    terrIdNameMap.put(tGeoCode.Territory__c, tGeoCode.Territory__r.Name);
                                }
                            }
                        }
                    }
                }else if(acc.ShippingCountryCode != null){
                    String ctry = ctryCodeMap.get(acc.ShippingCountryCode);
                    if(ctryTerrMap.containsKey(ctry)){
                        acc.Territory__c = ctryTerrMap.get(ctry);
                        //acc.Territory__r.Name = terrIdNameMap.get(acc.Territory__c);
                        updAccTerrMap.put(acc.Id, acc);
                    }
                }   
            }
        }
        
        if(updAccTerrMap.size() > 0 && download_Or_Update == 'Update'){
            updAccTerrList = updAccTerrMap.values();
            try{
                List<Database.SaveResult> results = Database.update(updAccTerrList, false);
                for (Integer i = 0; i < results.size(); i++) {
                    if (!results.get(i).isSuccess()) {
                        if (ErrorRecords == null) {
                            ErrorRecords = 'Operation, Object Name, Record, Status Code, Error Message \r\n';
                        }
                        for (Database.Error theError: results.get(i).getErrors()) {
                            string recordString = 'Update' + ',' + 'Account Territory' + ',' + updAccTerrList.get(i) + ',' + theError.getStatusCode() + ',' + theError.getMessage() + '\r\n';
                            ErrorRecords = ErrorRecords + recordString;
                        }
                    }else if(results.get(i).isSuccess()){
                        if (SuccessRecords == null) {
                            SuccessRecords = 'Operation, Object Name, Record, Status Code, Message \r\n';
                        }
                        string recordString = 'Update' + ',' + 'Account Territory' + ',' + updAccTerrList.get(i) + ',' + 'Success' + ',' + 'Record Created' +  '\r\n';
                        SuccessRecords = SuccessRecords + recordString;
                    }
                    System.debug('Error Records' + ErrorRecords);
                    System.debug('Success Records' + SuccessRecords);   
                } 
            }Catch(Exception dmle){
                System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
            }
        }else if(updAccTerrMap.size() > 0 && download_Or_Update == 'Download'){
            for(Account acc: updAccTerrMap.values()){
                String terrName = (acc.Territory__c != null && terrIdNameMap.containsKey(acc.Territory__c)) ? terrIdNameMap.get(acc.Territory__c) : '';
                String terrEduName = (acc.Education_Govt_Territory__c != null && terrIdNameMap.containsKey(acc.Education_Govt_Territory__c)) ? terrIdNameMap.get(acc.Education_Govt_Territory__c) : '';
                String terrHcName = (acc.Healthcare_Sr_Living_Territory__c != null && terrIdNameMap.containsKey(acc.Healthcare_Sr_Living_Territory__c)) ? terrIdNameMap.get(acc.Healthcare_Sr_Living_Territory__c) : '';
                String terrWpName = (acc.Workplace_Retail_Territory__c != null && terrIdNameMap.containsKey(acc.Workplace_Retail_Territory__c)) ? terrIdNameMap.get(acc.Workplace_Retail_Territory__c) : '';
                string recordString = acc.id + ';' + acc.Name + ';' + acc.Territory__c + ';' + terrName + ';' + 
                    acc.Education_Govt_Territory__c + ';' + terrEduName + ';' +
                    acc.Healthcare_Sr_Living_Territory__c + ';' + terrHcName + ';' +
                    acc.Workplace_Retail_Territory__c + ';' + terrWpName + ' \r\n';
                finalstr = finalstr + recordString;
            }
        }
        
        
        tempErrorRecs+=ErrorRecords;
        tempSuccessRecs+=SuccessRecords;
        tempFinalRecords+=finalstr;
        
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('!!!!!!Error Records tempErrorRecs' + tempErrorRecs );
        System.debug('!!!!!!Success Records tempSuccessRecs' + tempSuccessRecs );
        System.debug('!!!!!!Batch Job Finished'+tempFinalRecords ); 
        // String[] emailAddressList  = new list<string> {'susmitha.josephine@ust-global.com'};
        if(download_Or_Update == 'Download' && tempFinalRecords!='' ){
            List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';');  
            
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(tempFinalRecords);
            string csvname = 'Account.csv';
            csvAttc.setFileName(csvname);
            csvAttc.setBody(csvBlob);
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String subject = 'Accounts Territory Assignment';
            email.setSubject(subject);
            email.setToAddresses(emailAddressList);
            email.setPlainTextBody('Accounts Territory Assignment ');
            email.setFileAttachments(new Messaging.EmailFileAttachment[] { csvAttc });
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
        if(download_Or_Update == 'Update'){
            List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';'); 
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String subject = 'Account Territory Assignment Log';
            email.setSubject(subject);
            email.setToAddresses(emailAddressList);
            email.setPlainTextBody('Account Territory Assignment Log');
            if (tempErrorRecs!= '' && tempErrorRecs!= null){
                Messaging.EmailFileAttachment csvAttcErr = new Messaging.EmailFileAttachment();
                blob csvBlob = Blob.valueOf(tempErrorRecs);
                string csvname = 'ErrorRecords.csv';
                csvAttcErr.setFileName(csvname);
                csvAttcErr.setBody(csvBlob);
                email.setFileAttachments(new Messaging.EmailFileAttachment[] { csvAttcErr });
            }
            if (tempSuccessRecs!= '' && tempSuccessRecs!= null){
                Messaging.EmailFileAttachment csvAttcSuccess = new Messaging.EmailFileAttachment();
                blob csvBlob = Blob.valueOf(tempSuccessRecs);
                string csvname = 'SuccessRecords.csv';
                csvAttcSuccess.setFileName(csvname);
                csvAttcSuccess.setBody(csvBlob);
                email.setFileAttachments(new Messaging.EmailFileAttachment[] {
                    csvAttcSuccess
                        });
            }
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
        //Database.executeBatch(new AssignPubGrpToAccountSharing_Batch());
    }
}