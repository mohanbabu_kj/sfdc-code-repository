/***************************************************************************

Name : IncludeSALInMail_CC_Test 

===========================================================================
Purpose :  Test class for IncludeSALInMail_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        28/April/2017   Created 
***************************************************************************/
@isTest
private class IncludeSALInMail_CC_Test {
    static List<Account> accList;
    static Account acc;
    static List<Event_Action__c> eventActionList;
    static List<Event> eventList;
    static Event event;
    static EmailTemplate validEmailTemplate;
    static List<Contact> contactList;
    static List<Action_List__c> actionListFL;
    static List<Action_List__c> actionListSAL;
    static List<My_Appointment_List__c> myAppList;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
            init();
            accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
            acc = (Account)accList.get(0);
            contactList = Utility_Test.createContacts(true, 1, accList);
            eventActionList = Utility_Test.createEventActions(true, 1);
            eventList = Utility_Test.createEventsWithEventAction(false, 1, accList, eventActionList);
            event = eventList.get(0);
            event.WhoId = contactList.get(0).Id;
            event.Include_SAL_In_Mail__c = true;
            insert event;
            
            actionListFL = Utility_Test.createActionListForResidentialFL(true, 1);
            actionListSAL = Utility_Test.createActionListForResidentialSAL(true, 1);
            
            myAppList = new List<My_Appointment_List__c>();
            myAppList.add(new My_Appointment_List__c(Action_List__c =  actionListFL.get(0).Id, Event_Action__c =  eventActionList.get(0).Id, Include_Attachments_In_Email__c = true)); 
            myAppList.add(new My_Appointment_List__c(Action_List__c =  actionListSAL.get(0).Id, Event_Action__c =  eventActionList.get(0).Id, Include_Attachments_In_Email__c = true)); 
            insert myAppList;
            system.assert(myAppList!=null);
            validEmailTemplate = new EmailTemplate();
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();
            validEmailTemplate.Subject = 'Your Subject Here';
            
            insert validEmailTemplate;
            
            IncludeSALInMail_CC.setSubject(event.Id);
            IncludeSALInMail_CC.setEmailBody(event.Id);
            IncludeSALInMail_CC.sendMail('abc@test.com', 'subject', 'main body','', 'cc@test.com', 'bcc@test.com');
            IncludeSALInMail_CC.sendMailMethod('abc@test.com', 'subject', 'main body', String.valueOf(event.Id) , 'test', 'cc@test.com', 'bcc@test.com');
            IncludeSALInMail_CC.dynamicTemplate(validEmailTemplate.Id);
            IncludeSALInMail_CC.appoinmentData(event.Id);
            IncludeSALInMail_CC.sALItems(event.Id);
            IncludeSALInMail_CC.fundametalItems(event.Id);
            IncludeSALInMail_CC.salAttachments(event.Id, 'test');
            IncludeSALInMail_CC.salAttachments(event.Id, 'id-name');
            IncludeSALInMail_CC.getLocalDateTime(Datetime.now());
            System.assert(validEmailTemplate != null);
            Test.stopTest();
        }     
    }
    
    static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
            init();
            accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
            acc = (Account)accList.get(0);
            contactList = Utility_Test.createContacts(true, 1, accList);
            eventActionList = Utility_Test.createEventActions(true, 1);
            eventList = Utility_Test.createEventsWithEventAction(false, 1, accList, eventActionList);
            event = eventList.get(0);
            event.WhoId = contactList.get(0).Id;
            event.Include_SAL_In_Mail__c = false;
            insert event;
            
            actionListFL = Utility_Test.createActionListForResidentialFL(true, 1);
            actionListSAL = Utility_Test.createActionListForResidentialSAL(true, 1);
            
            myAppList = new List<My_Appointment_List__c>();
            myAppList.add(new My_Appointment_List__c(Action_List__c =  actionListFL.get(0).Id, Event_Action__c =  eventActionList.get(0).Id, Include_Attachments_In_Email__c = false)); 
            myAppList.add(new My_Appointment_List__c(Action_List__c =  actionListSAL.get(0).Id, Event_Action__c =  eventActionList.get(0).Id, Include_Attachments_In_Email__c = false)); 
            insert myAppList;
            
            validEmailTemplate = new EmailTemplate();
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();
            validEmailTemplate.Subject = 'Your Subject Here';
            
            insert validEmailTemplate;
            
            IncludeSALInMail_CC.setSubject(event.Id);
            IncludeSALInMail_CC.setEmailBody(event.Id);
            IncludeSALInMail_CC.sendMail('abc@test.com', 'subject', 'main body','', 'cc@test.com', 'bcc@test.com');
            IncludeSALInMail_CC.sendMailMethod('abc@test.com', 'subject', 'main body', String.valueOf(event.Id) , 'test', 'cc@test.com', 'bcc@test.com');
            IncludeSALInMail_CC.dynamicTemplate(validEmailTemplate.Id);
            IncludeSALInMail_CC.appoinmentData(event.Id);
            IncludeSALInMail_CC.sALItems(event.Id);
            IncludeSALInMail_CC.fundametalItems(event.Id);
            System.assert(validEmailTemplate != null);
            Test.stopTest();
        }     
        
    }
    
    static testMethod void testMethod3() {
        System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
            init();
            accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
            acc = (Account)accList.get(0);
            contactList = Utility_Test.createContacts(true, 1, accList);
            
            eventList = Utility_Test.createEvents(false, 1, accList);
            event = eventList.get(0);
            event.WhoId = contactList.get(0).Id;
            event.Include_SAL_In_Mail__c = false;
            insert event;
            
            IncludeSALInMail_CC.getEventContactEmail(event.Id);
            
            
            Test.stopTest();
        }     
        
    }
}