/**************************************************************************

Name : AccountPlansController_CC_Test

===========================================================================
Purpose : This tess class is used for AccountPlansController_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha     19/Jan/2018     Created             

***************************************************************************/
@isTest
      private class AccountPlansController_CC_Test {
          static testmethod void test1() {
              
              List<Account> AccountList=Utility_Test.createAccounts(true,1);
              
             List<Territory__c> terrList= Utility_Test.createTerritories(true,1);
              List<My_Account_Plan__c> MyAccountPlanList=Utility_Test.createMyAccountPlan(false,1);
              for(My_Account_Plan__c macp: MyAccountPlanList){
                  macp.Account__c=AccountList[0].id;
                  macp.Fiscal_Year__c=String.valueOf(Date.Today().Year());
                  macp.Segments__c='Healthcare';
              }
              insert MyAccountPlanList;
              system.assert(MyAccountPlanList.size()>0);
              User u=new User(Id=userinfo.getuserid());
               
              List<Mohawk_Account_Team__c> mohawkAccTeam=Utility_Test.createMohawkAccountTeam(true,1,AccountList,terrList,u);
              List<My_Account_Plan__c> MyAccountPlan1=AccountPlansController_CC.getAccountPlans(AccountList[0].id,String.valueOf(Date.Today().Year()));
              List<String> getFiscalPicklistValues=AccountPlansController_CC.getFiscalPicklistValues();
              Account a=AccountPlansController_CC.getAccount(AccountList[0].id);
              Account ac=AccountPlansController_CC.getAccount('invalidAccountId');
              List<My_Account_Plan__c> MyAccountPlan2=AccountPlansController_CC.getAccountPlans('','invalid parameter');
              List<My_Account_Plan__c> MyAccountPlan3=AccountPlansController_CC.getAccountPlans('invalid id','invalid parameter');
               AccountPlansController_CC.AccountTeamMemberObj teamMemberObj=AccountPlansController_CC.getMohawkTeamMember(AccountList[0].id);
               AccountPlansController_CC.AccountTeamMemberObj teamMemberObj1=AccountPlansController_CC.getMohawkTeamMember('invalid id');
              system.assert(teamMemberObj!=teamMemberObj1);
          }
      }