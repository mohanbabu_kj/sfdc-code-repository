/**************************************************************************

Name : RetrieveFreightRate_CC 

===========================================================================
Purpose :   class to send HTTP request along with global accountNumber number
            Parse the response as specified format
            Iterate over the wrapper class list to hold response
            Return the formatted response
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        7/Feb/2017     Created 
2.0        Lester        14/Feb/2017    Updated          Using Custom labels to store infomration
***************************************************************************/
global class RetrieveFreightRate_CC {

    @AuraEnabled
    public static FreightRateViewModel handleFreightRate (String accountId) {
        try {
            System.debug('RetrieveFreightRate_CC : accountId : '+ accountId);
            
            FreightRateViewModel viewModel = new FreightRateViewModel(); 
            // We have to use Account Id to get the globalAccountNumber field. 
            // Then we will send globalAccountNumber as a para 
            if (UtilityCls.isStrNotNull(accountId)) {
                Account acc = [Select Id, Global_Account_Number__c,CAMS_Account_number__c,Name From Account 
                                                where Id =: accountId][0];
            
                String globalAccountNumber = acc == null ? '' : acc.Global_Account_Number__c;
                System.debug('RetrieveFreightRate_CC : globalAccountNumber : ' + globalAccountNumber);

                if (UtilityCls.isStrNotNull(globalAccountNumber)){ 
                    String strResBody = SendHttpRequest.sendRequest(globalAccountNumber,null,'Freight_Service');                    
                    //handle response
                    // we assume the response format. 
                    //String strResBody = '{"Response":[{"ErrorCode":"0000","Message":"success","PRIMEWHSE":"ILD","CARPETDLVDAYS":"TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY","PALLETDLVDAYS":"","DATETIMESFDC":"*April 24 - 2018, 08:06 AM EST*","FRIEGHTARRAY":[{"CATEGORY":"Soft Surface","PRODTYPE":"Residential","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Soft Surface","PRODTYPE":"Mainstreetcommercial","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Soft Surface","PRODTYPE":"Carpet Tile","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Cushion","PRODTYPE":"Padding","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Hard Surface","PRODTYPE":"Engineered Wood","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Hard Surface","PRODTYPE":"Solid Wood","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Hard Surface","PRODTYPE":"Ceramic Tile","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Hard Surface","PRODTYPE":"Laminate","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Hard Surface","PRODTYPE":"Vinyl Broadloom","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"},{"CATEGORY":"Hard Surface","PRODTYPE":"Vinyl Tile","WCFRTRTE":".00","WCMINFRT":".00","MDFRTRTE":".40","MDMINFRT":"30.00"}]}]}';
                    //String strResBody = '{"ErrorCode":"0000","Message":"success","primewhse":"ILD","carpetdlvdays":"TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY","palletdlvdays":"","DateTimeSFDC":"*April 24 - 2018, 08:06 AM EST*","FUELSCHGPCT":"000.00","FRIEGHTARRAY":[{"category":"Soft Surface","prodType":"Residential","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","MDMINFRT":"30.00"},{"category":"Soft Surface","prodType":"Mainstreetcommercial","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","MDMINFRT":"30.00"},{"category":"Soft Surface","prodType":"Carpet Tile","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"},{"category":"Cushion","prodType":"Padding","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"},{"category":"Hard Surface","prodType":"Engineered Wood","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"},{"category":"Hard Surface","prodType":"Solid Wood","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"},{"category":"Hard Surface","prodType":"Ceramic Tile","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"},{"category":"Hard Surface","prodType":"Laminate","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"},{"category":"Hard Surface","prodType":"Vinyl Broadloom","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"},{"category":"Hard Surface","prodType":"Vinyl Tile","WCFRTRTE":".00","wcminfrt":".00","mdfrtrte":".40","mdminfrt":"30.00"}]}';
                    FreightRateWrapper freightRateWrapper = new FreightRateWrapper();    
                    freightRateWrapper = parse(strResBody);
                    system.debug('freightRateWrapper' + freightRateWrapper);
                    return handleResponse(freightRateWrapper,acc);
                }else {
                    viewModel.status = UtilityCls.GLOBAL_ACCOUNT_NUMBER_REQUIRED;
                    viewModel.exceptionMessage = Label.Freight_Rate_Global_Account_Number_Required;
                    System.debug('RetrieveFreightRate_CC : viewModel : ' + viewModel);

                    return viewModel;
                } // end of if (UtilityCls.isStrNotNull(globalAccountNumber))               
            }else{
                viewModel.status = UtilityCls.ID_REQUIRED;
                viewModel.exceptionMessage = Label.Freight_Rate_Id_Required;
                System.debug('RetrieveFreightRate_CC : viewModel : ' + viewModel);

                return viewModel;
            } // end of if (UtilityCls.isStrNotNull(myAccountProfileId))
                      
        } catch (Exception ex) {
            System.debug('RetrieveFreightRate_CC occurs an exception : ' + ex.getMessage());

            // try to record the error message
            String sourceFunction = Label.Freight_Rate_Exception_SourceFunction;
            String logCode = Label.Freight_Rate_Exception_LogCode ;
            String referenceInfo = Label.Freight_Rate_Exception_ReferenceInfo;
            UtilityCls.createExceptionLog(ex,  'RetrieveFreightRate_CC', sourceFunction, logCode, accountId, referenceInfo);
            
            FreightRateViewModel viewModel = new FreightRateViewModel(); 
            viewModel.status = UtilityCls.EXCEPTION_OCCUR;
            viewModel.exceptionMessage = Label.Freight_Rate_Exception_ExceptionMessage+ex.getMessage();
            System.debug('RetrieveFreightRate_CC : viewModel : ' + viewModel);
            
            return viewModel;
        }
       
    }
    
    
    private static FreightRateViewModel handleResponse(FreightRateWrapper main,account a){ 
        System.debug('RetrieveFreightRate_CC : FreightRateWrapper : ' + main);

        FreightRateViewModel viewModel = new FreightRateViewModel();
        if( a != null ){
            viewModel.accName = a.Name == null ? '' : a.Name;
            viewModel.camsNum = a.CAMS_Account_number__c == null ? '' : a.CAMS_Account_number__c;
        }
        if(main != null){
    
            viewModel.ErrorCode = Main.ErrorCode == null ? '' : Main.ErrorCode ;
            viewModel.primewhse = Main.primewhse == null ? '' : Main.primewhse;
            viewModel.oDateTime = Main.DateTimeSFDC  == null ? '' : addSuperScriptToDate(Main.DateTimeSFDC);
            viewModel.Message = Main.Message  == null ? '' : Main.Message;
            viewModel.carpetdlvdays = Main.carpetdlvdays  == null ? '' : Main.carpetdlvdays;
            viewModel.palletdlvdays = Main.palletdlvdays  == null ? '' : Main.palletdlvdays;
            viewModel.FUELSCHGPCT = Main.FUELSCHGPCT  == null ? '' : Main.FUELSCHGPCT;
            
            System.debug(LoggingLevel.INFO, '*** viewModel.oDateTime: ' + viewModel.oDateTime);

            if(Main.frieghtarray != null && Main.frieghtarray.size() > 0){
                List<OFreightDataList> softSurfaceList = new List<OFreightDataList>();
                List<OFreightDataList> cushionList = new List<OFreightDataList>();
                List<OFreightDataList> hardsurfaceList = new List<OFreightDataList>();

                for(OFreightData data : Main.frieghtarray){
                    //System.debug('RetrieveFreightRate_CC : tacticalCat : ' + data.tacticalCat);
                    OFreightDataList oFreightDataListObj;

                    // we group oFreightData based on tacticalCat. 
                    // In addition, we only have three groups: Soft Surface, Cushion, Hard Surface.
                    // Therefore, if tacticalCat is not belong to three groups, we will not show this oFreightData record.
                    if (data.category != null && data.category.equalsIgnoreCase(UtilityCls.SOFT_SURFACE)){
                        //System.debug('RetrieveFreightRate_CC : tacticalCat : Soft Surface: ' + data.tacticalCat);
                        oFreightDataListObj = new OFreightDataList(data.category, data.prodType, data.WCFRTRTE, data.wcminfrt, data.mdfrtrte, data.mdminfrt);
                        softSurfaceList.add(oFreightDataListObj);
                    }else if (data.category != null && data.category.equalsIgnoreCase(UtilityCls.CUSHION)){
                        //System.debug('RetrieveFreightRate_CC : tacticalCat : Cushion : ' + data.tacticalCat);
                        oFreightDataListObj = new OFreightDataList(data.category, data.prodType, data.WCFRTRTE, data.wcminfrt, data.mdfrtrte, data.mdminfrt);
                        cushionList.add(oFreightDataListObj);
                    }else if (data.category != null && data.category.equalsIgnoreCase(UtilityCls.HARD_SURFACE)) {
                        //System.debug('RetrieveFreightRate_CC : tacticalCat : Hard Surface : ' + data.tacticalCat);
                        oFreightDataListObj = new OFreightDataList(data.category, data.prodType, data.WCFRTRTE, data.wcminfrt, data.mdfrtrte, data.mdminfrt);
                        hardsurfaceList.add(oFreightDataListObj);
                    }else {
                        //System.debug('RetrieveFreightRate_CC : tacticalCat : null : ' + data.tacticalCat);
                    }
                } // end of for(OFreightData data : main.Main.oFreightData)

                // System.debug('RetrieveFreightRate_CC : softSurfaceList : ' + softSurfaceList);
                // System.debug('RetrieveFreightRate_CC : cushionList : ' + cushionList);
                // System.debug('RetrieveFreightRate_CC : hardsurfaceList : ' + hardsurfaceList);
                
                // check the data is empty
                Integer dataSum = 0;
                dataSum = softSurfaceList.size() + cushionList.size() + hardsurfaceList.size();
                if (dataSum > 0) {
                    viewModel.status = UtilityCls.OK;
                }else {
                    // Now viewModel.oMessage should not be empty and show some information.
                    viewModel.status = UtilityCls.GLOBAL_ACCOUNT_NUMBER_INCORRECT;
                    viewModel.exceptionMessage = Main.Message + Label.Freight_Rate_Global_Account_Number_Incorrect;
                }

                viewModel.softSurfaceList = softSurfaceList;
                viewModel.cushionList = cushionList;
                viewModel.hardsurfaceList = hardsurfaceList;

            }else {
                // main.Main.oFreightData  may be null or main.Main.oFreightData.size() is empty
                // Now viewModel.oMessage should not be empty and show some information.
                viewModel.status = UtilityCls.DATA_IS_EMPTY;
                viewModel.exceptionMessage = Label.Freight_Rate_Data_Empty; 
            } // end of if(main.Main.oFreightData != null && main.Main.oFreightData.size() > 0)
        }else {
            // It may parse JSON format issue
            viewModel.status = UtilityCls.PASRE_JSON_ERROR;
            viewModel.exceptionMessage = Label.Freight_Rate_Parse_JSON_Error;
        } // end of if(main != null && main.Main != null)

        System.debug('RetrieveFreightRate_CC : viewModel : ' + viewModel);
        return viewModel;
    }


/**************************************************************************
Name : FreightRateWrapper 
===========================================================================
Purpose :  Wrapper class to parse the Freight rate http response in order to display in lightning component.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         7/Feb/2017     Created 
***************************************************************************/
    private class OFreightData {
        // tacticalCat only has three values : Soft Surface, Cushion and Surface
        // other fields will be depent on the tacticalCat field.
        private String category;
        private String prodType;
        private String WCFRTRTE;
        private String wcminfrt;
        private String mdfrtrte;
        private String mdminfrt;
    }

    private class FreightRateWrapper {
        private String ErrorCode;
        private String Message;
        private String primewhse;
        private String carpetdlvdays;
        private String palletdlvdays;
        private String DateTimeSFDC;   
        private string FUELSCHGPCT;
        private List<OFreightData> FRIEGHTARRAY;
     }

    private static FreightRateWrapper parse(String jsonStr) {
        
        system.debug('(FreightRateWrapper) JSON.deserialize(jsonStr, FreightRateWrapper.class)'+ JSON.deserialize(jsonStr, FreightRateWrapper.class));
        return (FreightRateWrapper) JSON.deserialize(jsonStr, FreightRateWrapper.class);
    }


    public class FreightRateViewModel {
       // @AuraEnabled
       // public String iPAccount {get; set;}
       // @AuraEnabled
       //  public String oAccountName {get; set;}
       	
        @AuraEnabled
        public string accName{get; set;}
        @AuraEnabled
        public string camsNum{get; set;}
        @AuraEnabled
        public String ErrorCode {get; set;}
        @AuraEnabled
        public String Message {get; set;}
        @AuraEnabled
        public String oDateTime {get; set;}
        @AuraEnabled
        public String primewhse {get; set;}
        @AuraEnabled
        public String carpetdlvdays {get; set;}
        @AuraEnabled
        public String palletdlvdays {get; set;}
        @AuraEnabled
        public String status {get; set;} // HTTP requst status
        @AuraEnabled
        public String exceptionMessage {get; set;} // recod exception message
        @AuraEnabled public string FUELSCHGPCT {get; set;} //Added by MS - Bug 69649 - 1/18/19
        @AuraEnabled
        public List<OFreightDataList> softSurfaceList {get; set;}
        @AuraEnabled
        public List<OFreightDataList> cushionList {get; set;}
        @AuraEnabled
        public List<OFreightDataList> hardsurfaceList {get; set;}

        public FreightRateViewModel() {
          softSurfaceList = new List<OFreightDataList>();
          cushionList = new List<OFreightDataList>();
          hardsurfaceList = new List<OFreightDataList>();
        }
    }

  public class OFreightDataList {
        
        @AuraEnabled
        public String category {get; set;}
        @AuraEnabled
        public String prodType {get; set;}
        @AuraEnabled    
        public String freightRate {get; set;}
        @AuraEnabled    
        public String minFreight {get; set;}
        @AuraEnabled
        public String delRate {get; set;}
        @AuraEnabled
        public String delMin {get; set;}

        public OFreightDataList(String icategory, String iprodType, String ifreightRate, String iminFreight, String idelRate, String idelMin){
            this.category = icategory == null ? '' : icategory;
            this.prodType = iprodType == null ? '' : iprodType;
            this.freightRate = ifreightRate == null ? '' : ifreightRate;
            this.minFreight = iminFreight == null ? '' : iminFreight;
            this.delRate = idelRate == null ? '' : idelRate;
            this.delMin = idelMin == null ? '' : idelMin;
        }
    }
    
    public static String addSuperScriptToDate(string dtStr){
        String dt = dtStr.removeStart('*');
                dt = dt.removeEnd('*');
		List<String> dtList = new List<String>();
		String htmlval = '';
		dtList = dt.split(',');
		if( dtList != null && dtList.size() > 0 ){
			List<String> innerDtList = new List<String>();				
			innerDtList = dtList[0].split( ' ' );
			if( !innerDtList.isEmpty() ){
				String digit = innerDtList[1].trim();
				if( digit.isNumeric() ){
					if( digit == '1' ){
						htmlval = '<sup>st</sup>';
					}else if( digit == '2' ){
						htmlval = '<sup>nd</sup>';
					}else{
						htmlval = '<sup>th</sup>';
					}			
					dt = innerDtList[0]+' '+innerDtList[1]+htmlval+', '+ dtList[1];			
				}
			}
		}
		return dt;
    }
}