@isTest
public class UserTriggerHandler_Test {
    
    @isTest public static void testMethodforUpdateUserInfo(){
        System.runAs(Utility_Test.ADMIN_USER){
            Default_Configuration__c dc = Utility_Test.getDefaultConfCusSetting();
            List<User> userList = Utility_Test.createTestUsers(false, 2, null);
            //userList[0].Manager_CAMS_Id__c = '12345';
            userList[0].NickName__c = 'MyNickName1';
            userList[0].Business_Type__c = 'Residential';
            userList[0].CAMS_Employee_Number__c = '12345';
            //userList[1].Manager_CAMS_Id__c = '12345';
            userList[1].NickName__c = 'MyNickName2';
            userList[1].Business_Type__c = 'Residential';
            userList[1].CAMS_Employee_Number__c = '67890';
            insert userList;
            userList.clear();
            userList = Utility_Test.createTestUsers(false, 1, null);
            userList[0].Manager_CAMS_Id__c = '12345';
            userList[0].NickName__c = 'MyName1';
            userList[0].Business_Type__c = 'Residential';
            userList[0].CAMS_Employee_Number__c = '10000';
            insert userList;
            
            userList[0].Manager_CAMS_Id__c = '12345';
            update userList;
            
            userList[0].Manager_CAMS_Id__c = null;
            update userList;
        }
    }
}