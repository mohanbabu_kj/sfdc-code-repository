public class SampleDisplayPerformance_CC {
    
    @AuraEnabled
    public static List < Sample_Display_Inventory__c > getAccountDisplaySamples(String recordId, String brandCollection, String customerGroup) {
        List < Sample_Display_Inventory__c > samples = new List < Sample_Display_Inventory__c > ();
        Id accountId = recordId;
        if (brandCollection == null) brandCollection = '';
        if (customerGroup == null) customerGroup = '';
        String soql = 'SELECT Id, External_Id__c, Display_External_ID__c, Name,Inventory__r.Display__c,Brand__c, Customer_Group__c, Display_Version__c, Placement_Status__c, Inventory__c,Fit__c,Display_Performance__c, MTD_Sales__c, YTD_Sales__c, R12_Sales__c FROM Sample_Display_Inventory__c WHERE Account__c =: accountId';
        if ( customerGroup != '' &&  customerGroup != 'All' && customerGroup != 'Select One'  ) {
            soql += ' AND Customer_Group__c =: customerGroup';
        }
        if ( brandCollection != '' ) {
            set<string>brandMap = new set<string>();
            set<string>prodTypeMap = new set<string>();
            Map<string,boolean>checkMap = new Map<string,boolean>();
            for( Product_SFDC_Mapping__c PSM : [ SELECT Brand__c,Product_Type__c FROM Product_SFDC_Mapping__c WHERE Sample_Inventory_Category__c =: brandCollection ] ){
                brandMap.add( PSM.Brand__c );
                prodTypeMap.add( PSM.Product_Type__c );
            }
            system.debug('::::brandMap::::' + brandMap);
            system.debug('::::prodTypeMap::::' + prodTypeMap);
            if( brandMap != null && prodTypeMap != null ){
                set<id>inventoryIDS = new set<id>();
                for( Sample_Display_Inventory__c SDI : Database.Query(soql) ){
                    inventoryIDS.add( SDI.Inventory__c );			
                }
                system.debug(':::inventoryIDS::::' + inventoryIDS);
                if( inventoryIDS != null && inventoryIDS.size() > 0 ){				
                    for( Product_Relationship__c PR : [ SELECT id, Product__c,Related_Product__c,Related_Product__r.Brand_Code__c,Related_Product__r.ERP_Product_Type__c FROM Product_Relationship__c WHERE Product__c IN : inventoryIDS ] ){
                        system.debug('::::::' + PR.Related_Product__r.Brand_Code__c );
                        system.debug('::::::' + PR.Related_Product__r.ERP_Product_Type__c );
                        if( brandMap.contains( PR.Related_Product__r.Brand_Code__c ) && prodTypeMap.contains( PR.Related_Product__r.ERP_Product_Type__c ) ){
                            system.debug(':::HERE IF:::');
                            checkMap.put( PR.Product__c,true );
                            continue;
                        }else{
                            system.debug(':::HERE ELSE:::');
                            checkMap.put( PR.Product__c,false );
                        }
                    }
                    system.debug('::::checkMap::::' + checkMap);
                }
            }
            for( Sample_Display_Inventory__c SDI : Database.Query(soql) ){
                if( checkMap.containsKey(SDI.Inventory__c) && checkMap.get( SDI.Inventory__c ) ){
                    samples.add( SDI );
                }
            }
        }else{
            soql += ' ORDER BY Placement_Status__c ASC';		
            samples = Database.Query(soql);        
        }
        return samples;
    }
    
    /*************************************************
@AuraEnabled
public static List < Sample_Display_Inventory__c > getAccountDisplaySamples(String recordId, String brandCollection, String customerGroup) {
Id accountId = recordId;
if (brandCollection == null) brandCollection = '';
if (customerGroup == null) customerGroup = '';
String soql = 'SELECT Id, External_Id__c, Display_External_ID__c, Name, Brand__c, Customer_Group__c, Display_Version__c, Placement_Status__c, Inventory__c, Fit__c,Display_Performance__c, MTD_Sales__c, YTD_Sales__c, R12_Sales__c FROM Sample_Display_Inventory__c WHERE Account__c =: accountId';
if (brandCollection != '') {
soql += ' AND Sample_Inventory_Category__c =: brandCollection'; // Bug 61926 : changed to Sample Inventory Cat 
}

if (customerGroup != '' &&  customerGroup!='All' && customerGroup!='Select One'  ) {
soql += ' AND Customer_Group__c =: customerGroup';
}
soql += ' ORDER BY Placement_Status__c ASC';
List < Sample_Display_Inventory__c > samples = new List < Sample_Display_Inventory__c > ();
samples = Database.Query(soql);        
return samples;
}
**********************************************/
    @AuraEnabled
    public static List < Sample_Display_Inventory__c > saveSamplesData(List < Sample_Display_Inventory__c > samples) {
        try{
            List < Database.SaveResult > sampleResults = Database.Update(samples, false);
            for (Database.SaveResult result: sampleResults) {
                if (result.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated sample displays: ' + result.getId());
                    // ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Success!'));
                } else {
                    // Operation failed, so get all errors                
                    for (Database.Error err: result.getErrors()) {
                        System.debug('Error while udpating sample displays ' + err.getFields());
                    }
                }
            }
        }
        catch(Exception e){
            system.debug('Exception e::'+e);
        }
        return samples;
    }
    
    @AuraEnabled
    public static List < String > getPlacementStatusPicklistValues() {
        
        List<String> PlStpickvalues = UtilityCls.getPicklistValues('Sample_Display_Inventory__c','Placement_Status__c');
        return PlStpickvalues;
        
        /*return new List < String > {
'In Store',
'Update Shipped',
'Update Ordered',
'Shipped',
'On Order',
'Not in Store',
'Removed'
};*/
    }
    
    @AuraEnabled
    public static List < String > getBrandCollectionPicklistValues() {
        Set<String> BrColpickvalues = UtilityCls.getPicklistValuesFromCustomObject('Product_SFDC_Mapping__c', 'Sample_Inventory_Category__c');
        List<String> brandCollPickValues = new List<String>();
        brandCollPickValues.addAll(BrColpickvalues);
        System.debug('Brand Collection: ' + brandCollPickValues);
        return brandCollPickValues;
    }
    //Commented out on Jul 20 for Bug 63315 and created new method with same name
    /* @AuraEnabled
public static List < String > getCustomerGroupPicklistValues(String recordId) {
String customerGroup = [SELECT Id, Customer_Group__c FROM Account WHERE Id =: recordId].Customer_Group__c;
List < String > CuGrppickvalues = UtilityCls.stringtoList(customerGroup);

//*******Added by SUSMITHA on NOV 2 for 41645 ***************************
Set < String > SortedCuGrppickvalues = new Set < String > ();
List < String > SortedCuGrppickvaluesList = new List < String > ();
set < String > SortedCuGrppickvaluesSet = new set < String > ();

if(Test.isRunningTest()){
SortedCuGrppickvalues.add('Color Center Select(MA)');
SortedCuGrppickvalues.add('Signature Series (OL)');
SortedCuGrppickvalues.add('Open Line (OL)');
}

for (string s: CuGrppickvalues) {
SortedCuGrppickvalues.add(s);
}

if (SortedCuGrppickvalues.contains('Color Center Select(MA)') || SortedCuGrppickvalues.contains('Color Center Elite (MA)') ||
SortedCuGrppickvalues.contains('Floorscapes - Canada (MA)') || SortedCuGrppickvalues.contains('Floorscapes (MA)')) {
for (string s: SortedCuGrppickvalues) {
if (s == 'Color Center Select(MA)' || s == 'Color Center Elite (MA)' || s == 'Floorscapes - Canada (MA)' || s == 'Floorscapes (MA)') {
SortedCuGrppickvaluesSet.add(s);
}
}
}
if (SortedCuGrppickvalues.contains('Signature Series (OL)')) {
for (string s: SortedCuGrppickvalues) {
if (s == 'Signature Series (OL)') {
SortedCuGrppickvaluesSet.add(s);
}
}
}
if (SortedCuGrppickvalues.contains('Open Line (OL)')) {

for (string s: SortedCuGrppickvalues) {
if (s != 'Open Line (OL)') {
SortedCuGrppickvaluesSet.add(s);
}

}
for (string s: SortedCuGrppickvalues) {
if (s == 'Open Line (OL)') {
SortedCuGrppickvaluesSet.add(s);
}
}
}
for (string s: SortedCuGrppickvaluesSet) {
SortedCuGrppickvaluesList.add(s);
}


//*********Added by SUSMITHA on NOV 2 for 41645 ***************



//List<String> CuGrppickvalues = UtilityCls.getPicklistValues('Product2','Buying_Group__c');
return SortedCuGrppickvaluesList;
}
*/
    @AuraEnabled
    public static List < String > getCustomerGroupPicklistValues(String recordId) {
        String customerGroup = [SELECT Id, Customer_Group__c FROM Account WHERE Id =: recordId].Customer_Group__c;
        List < String > CuGrppickvalues = UtilityCls.stringtoList(customerGroup);
        
        //*******Added by SUSMITHA on NOV 2 for 41645 ***************************
        Set < String > SortedCuGrppickvalues = new Set < String > ();
        List < String > SortedCuGrppickvaluesList = new List < String > ();
        set < String > SortedCuGrppickvaluesSet = new set < String > ();
        
        if(Test.isRunningTest()){
            SortedCuGrppickvalues.add('Color Center Select(MA)');
            SortedCuGrppickvalues.add('Signature Series (OL)');
            SortedCuGrppickvalues.add('Open Line (OL)');
        }
        
        for (string s: CuGrppickvalues) {
            SortedCuGrppickvalues.add(s);
        }
        SortedCuGrppickvaluesSet.add('Select One');
        
        for (string s: SortedCuGrppickvalues) {
            if (s != 'Open Line (OL)') {
                SortedCuGrppickvaluesSet.add(s);
            } 
        }
        SortedCuGrppickvaluesSet.add('All');
        SortedCuGrppickvaluesSet.add('Open Line (OL)');//Add Open Line for all no matter it's in Account or not
        for (string s: SortedCuGrppickvaluesSet) {
            SortedCuGrppickvaluesList.add(s);
        }
        /* if (SortedCuGrppickvalues.contains('Color Center Select(MA)') || SortedCuGrppickvalues.contains('Color Center Elite (MA)') ||
SortedCuGrppickvalues.contains('Floorscapes - Canada (MA)') || SortedCuGrppickvalues.contains('Floorscapes (MA)')) {
for (string s: SortedCuGrppickvalues) {
if (s == 'Color Center Select(MA)' || s == 'Color Center Elite (MA)' || s == 'Floorscapes - Canada (MA)' || s == 'Floorscapes (MA)') {
SortedCuGrppickvaluesSet.add(s);
}
}
}
if (SortedCuGrppickvalues.contains('Signature Series (OL)')) {
for (string s: SortedCuGrppickvalues) {
if (s == 'Signature Series (OL)') {
SortedCuGrppickvaluesSet.add(s);
}
}
}
if (SortedCuGrppickvalues.contains('Open Line (OL)')) {

for (string s: SortedCuGrppickvalues) {
if (s != 'Open Line (OL)') {
SortedCuGrppickvaluesSet.add(s);
}

}
for (string s: SortedCuGrppickvalues) {
if (s == 'Open Line (OL)') {
SortedCuGrppickvaluesSet.add(s);
}
}
}
for (string s: SortedCuGrppickvaluesSet) {
SortedCuGrppickvaluesList.add(s);
}*/
        
        
        //*********Added by SUSMITHA on NOV 2 for 41645 ***************
        
        
        
        //List<String> CuGrppickvalues = UtilityCls.getPicklistValues('Product2','Buying_Group__c');
        return SortedCuGrppickvaluesList;
    }
    @AuraEnabled
    public static String getMashupUrl(String mashupName, String recordId) {
        String mashupUrl = '';
        mashupUrl = Mashups_CC.getMashupRecordNew(mashupName, '', '');
        return mashupUrl;
    }
}