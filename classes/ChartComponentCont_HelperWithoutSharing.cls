public without sharing class ChartComponentCont_HelperWithoutSharing {
    
    public static ChartComponentController.ChartWrapper getData_chart2(String region, String district, String territory, String objName, String styleNum, String chartId, String timePeriod, String inventoryNum){
        ChartComponentController.ChartWrapper returndata = new ChartComponentController.ChartWrapper();

        String queryStr = ChartComponentController_Helper.getDataQueryStr_Chart2( region,  district,  territory,  objName,  styleNum, timePeriod, inventoryNum);
        
        //AggregateResult[] groupedResults = Database.query(queryStr); // Commented by MB - Bug 71514 - 3/12/19
        AggregateResult[] groupedResults = new List<AggregateResult>(); 
        if(queryStr != ''){ //Added condition if no CPL data, do not display Sales Item in the Chart - MB - Bug 71514 - 3/12/19
            groupedResults = Database.query(queryStr);
        }
        String chartColor = ChartComponentController_Helper.getColorForChart(chartId);
        returndata.dataLabels = ChartComponentController_Helper.retrievePriceLevelHeaders();
        
        Map<String, decimal> pLeveAmount = ChartComponentController_Helper.processResults(groupedResults, timePeriod + '_Price_Level__c', returndata.dataLabels);

        List<Double> amountList = new List<Double>();
        
        for (String label : returndata.dataLabels) {
                    
            double amount = pLeveAmount.get(label);
            amountList.add(amount.intValue());
            returndata.dataValues.add(string.valueof(amount.intValue()));
            returnData.colorValues.add(chartColor);
        }
        
        amountList.sort();
        if (amountList.size() > 0)
            returnData.maxValue = amountList.get(amountList.size()-1);
        else
            returnData.maxValue = 0;
        
        //Get Selling Style Num's full name
        
        if (String.isNotEmpty(styleNum)) {
            queryStr ='SELECT Name FROM Product2 WHERE  Product_style_number__c  = \'' + styleNum + '\' LIMIT 1';
            List<SObject> obj = Database.query(queryStr);

            if (obj != null && obj.size() > 0) {
                //Map<String,Object> fielMap = obj.get(0).getPopulatedFieldsAsMap();
                Product2 productMap = (Product2) obj.get(0);
                
                if (productMap != null) 
                    returndata.styleNumFullName = (String) productMap.get('Name') + ' (' + styleNum + ')';
                else 
                    returndata.styleNumFullName = ' (' + styleNum + ')';
            }
            else
                returndata.styleNumFullName = ' (' + styleNum + ')';
                
        }
        

        return returndata;
    }
    
    public static ChartComponentController.ChartWrapper getData_chart4(String territory, String category, String objName, String accountId, String chartId, String timePeriod){
        
        ChartComponentController.ChartWrapper returndata = new ChartComponentController.ChartWrapper();

        //Get Selling Style Num's full name
        String chainNumber = '';
        if (String.isNotEmpty(accountId)) {
            String queryStr ='SELECT Id, Name, CAMS_Account_Number__c, Chain_Number__c FROM Account WHERE Id = \'' + accountId + '\' LIMIT 1';
            List<SObject> obj = Database.query(queryStr);
            if (obj != null && obj.size() > 0) {
                chainNumber = (String) obj.get(0).get('Chain_Number__c');
                returndata.styleNumFullName = (String) obj.get(0).get('Name') + ' (' + (String) obj.get(0).get('CAMS_Account_Number__c') + ')';
            }
            else
                returndata.styleNumFullName = ' ';
        }


        System.debug('### chart 4 chainNumber (2): ' + chainNumber);
        
        String chartColor = ChartComponentController_Helper.getColorForChart(chartId);
        returndata.dataLabels = ChartComponentController_Helper.retrievePriceLevelHeaders();
        
        String queryStr = ChartComponentController_Helper.getDataQueryStr_Chart4(territory,  objName,  chainNumber, timePeriod, category);
        AggregateResult[] groupedResults = Database.query(queryStr);
        System.Debug('### chart 4 queryStr: ' + queryStr);
        System.Debug('### chart 4 groupedResults: ' + groupedResults);
        
        Map<String, decimal> pLeveAmount = new Map<String, decimal>();
        for (String label : returndata.dataLabels)
            pLeveAmount.put(label, 0);
        
        for (AggregateResult row : groupedResults) {
            String label = string.valueof(row.get(timePeriod + '_Price_Level__c'));
            label = ChartComponentController_Helper.shortLabel(label);
            decimal amount = (decimal)row.get('expr0');
            pLeveAmount.put(label, amount);
        }
            
        List<Double> amountList = new List<Double>();
        for (String label : returndata.dataLabels) {
            double amount = pLeveAmount.get(label);
            amountList.add(amount.intValue());
            returndata.dataValues.add(string.valueof(amount.intValue()));
            returnData.colorValues.add(chartColor);
        }
        
        amountList.sort();
        if (amountList.size() > 0)
            returnData.maxValue = amountList.get(amountList.size()-1);
        else
            returnData.maxValue = 0;

        return returndata;
    }
    
    public static list<sobject> ExecuteQueryDrillDownQuery(String sQuery, string ActChainNumbers){
        if (String.isNotBlank(ActChainNumbers)){
            sQuery += ' AND Division_Customer_No_SFX_Id__c IN ' + ActChainNumbers;
        }
        /*if (String.isNotBlank(LimitVar)){
            sQuery += ' LIMIT ' + LimitVar;
        }*/
        system.debug('::::sQuery::::' + sQuery);
        return database.query(sQuery);
    }
}