public without sharing class Grid_BaseHelper{
    public static final String ROLE_TM = 'TM';
    public static final String ROLE_DM = 'DM';
    public static final String ROLE_RVP = 'RVP';
    public static final String ROLE_SALESOPS = 'SalesOps';
    public static final String FIELD_STANDARDCOST = 'Product2.Standard_Cost__c';
    public static final String SPECIALFILTER_STANDARDCOST = 'Special (Standard Cost)';
    
    public static final String GRIDTYPE_PRICEGRID = 'Price Grid';
    public static final String GRIDTYPE_BUYINGGROUPGRID = 'Buying Group Grid';
    public static final String GRIDTYPE_CUSTOMERPRICELIST = 'Customer Price List';
    public static final String GRIDTYPE_MERCHANDIZINGVEHICLEPRICELIST = 'Merch Vehicle Price List';
    
    public static String usersRole{
        get{
            if (usersRole == null)
                usersRole = ResidentialPricingGridUtility.getRoleByLoggedUser();
            return usersRole;
        }
        set;
    }

    List<String> returnList;
    public static Map<String, SObjectType> globalDesc = Schema.getGlobalDescribe();

    public static List<String> fetchPickListValues(String objectName, String fieldName) {
        List<String> returnList = new List<String>();
        Schema.sObjectType sobjectType;
        List<String> fieldNameArray = fieldName.split('\\.');
        if(fieldNameArray.size() > 1){
            sobjectType = getRelationshipSObjectType(objectName, fieldNameArray[fieldNameArray.size() - 2]);
            fieldName = fieldNameArray[fieldNameArray.size() - 1];
        }
        else{
            sobjectType = globalDesc.get(objectName);
        }
        Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap();
        for (Schema.PicklistEntry pick : fieldMap.get(fieldName).getDescribe().getPickListValues()) {
            returnList.add(pick.getValue());
        }
        return returnList;
    }

    public static Schema.sObjectType getRelationshipSObjectType(String objectName, String relationshipName){
        Schema.sObjectType relSObjectType;
        Schema.sObjectType sobjectType = globalDesc.get(objectName);
        Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
        for(Schema.SobjectField field: sobjectDescribe.fields.getMap().Values()){
            if(field.getDescribe().getType() == Schema.DisplayType.REFERENCE
               && field.getDescribe().getRelationshipName() == relationshipName){
                   relSObjectType = field.getDescribe().getReferenceTo()[0];
               }
        }
        return relSObjectType;
    }

    public static Set<String> getAccessLevelFilter(){
        Set<String> metadataFilterList = new Set<String>();
        metadataFilterList.add('');
        if (usersRole == ROLE_DM || usersRole == ROLE_RVP || usersRole == ROLE_SALESOPS)
            metadataFilterList.add(ROLE_DM);
        if (usersRole == ROLE_RVP || usersRole == ROLE_SALESOPS)
            metadataFilterList.add(ROLE_RVP);
        if (Utility_Describe.isFieldAccessible(FIELD_STANDARDCOST))
            metadataFilterList.add(SPECIALFILTER_STANDARDCOST);
        return metadataFilterList;
    }

    public static String getGridType(String grid){
        return grid == 'Price Grid' ? ResidentialPricingGridUtility.GRID_TYPE_MBP : (grid == 'Buying Group Grid' ? ResidentialPricingGridUtility.GRID_TYPE_BGP : null);
    }

    public static  List<Map<String,Object>> getFieldValuesFromPriceGrid(Set<String> apiNamePrimaryList,Set<String> apiNameSecondaryList,Id recordId,String sObjectName){
        String queryValue = 'SELECT Id, Product_Unique_Key__c, Account__c, Product__c, Price_Grid_External_Id__c,' +
            'Product__r.Company__c, Product__r.Brand_Code__c, Product__r.ERP_Product_Type__c,Product__r.Salesforce_Product_Category__c ' +
            'FROM ' + sObjectName+ ' where id=:recordId' ;
         
 //       system.debug('getFieldValuesFromPriceGrid query :'+ queryValue);

        List<Sobject> scope = Database.query(queryValue);
        Set<String> setOfProductCategory = new Set<String>();
        Set<String> setOfPriceGridUniqueValues = new Set<String>();
        for (Sobject so : scope)    {
                
            if (so.get('Product__c') != null){
                //populate map
                if(so.getSObject('Product__r').get('Salesforce_Product_Category__c')!=NULL)
                    setOfProductCategory.add(so.getSObject('Product__r').get('Salesforce_Product_Category__c').toString());
                setOfPriceGridUniqueValues.add((String) so.get('Price_Grid_External_Id__c'));
            }
            System.debug('Price_Grid_External_Id__c : ' + so.get('Price_Grid_External_Id__c'));
        }
        Map<String,String> mapOfPriceGridValues = new Map<String,String>();
        String query = 'SELECT Id, ';
        Set<String> combinedAPIName = new  Set<String>(apiNamePrimaryList);
        combinedAPIName.addAll(apiNameSecondaryList);
        for(String field : combinedAPIName){
            query += field+', ';
        }
        query = query.removeEnd(', ');
        query += ' FROM Price_Grid__c ';
        query += ' WHERE Price_Grid_Unique_key__c IN: setOfPriceGridUniqueValues and Product__r.Salesforce_Product_Category__c in:setOfProductCategory';
        List<Sobject> listOfPriceGrid = Database.query(query);
        
    //    system.debug('getFieldValuesFromPriceGrid query 2 :'+ queryValue);
        
        Map <String, Object> mapOfPrimaryValues = new Map<String,Object>();
        Map <String, Object> mapOfSecondaryValues = new Map<String,Object>();
        List<Map<String,Object>> listOfValues = new List<Map<String,Object>>();
        for (Sobject so : listOfPriceGrid){
            for(String field : apiNamePrimaryList){
                if(field.contains('.')){
                    List<String> stringList = field.split('\\.');
                    if(so.getSObject(stringList[0])!=null){
                        if (so.getSObject(stringList[0]).get(stringList[1]) != null){
                            mapOfPrimaryValues.put(field,so.getSObject(stringList[0]).get(stringList[1]));
                        }
                    }
                }
                else{
                    if (so.get(field) != null){
                        mapOfPrimaryValues.put(field,so.get(field));
                    }
                }
            }
            for(String field : apiNameSecondaryList){
                if(field.contains('.')){
                    List<String> stringList = field.split('\\.');
                    if(so.getSObject(stringList[0])!=null){
                        if (so.getSObject(stringList[0]).get(stringList[1]) != null){
                            mapOfSecondaryValues.put(field,so.getSObject(stringList[0]).get(stringList[1]));
                        }
                    }
                }
                else{
                    if (so.get(field) != null){
                        mapOfSecondaryValues.put(field,so.get(field));
                    }
                }
            }
        }
        listOfValues.add(mapOfPrimaryValues);
        listOfValues.add(mapOfSecondaryValues);
        return listOfValues;
    }

    public class ViewData{
        @AuraEnabled public Account account;
        @AuraEnabled public String productId;
        @AuraEnabled public String searchKeyword;
        @AuraEnabled public String productCategoryId;
        @AuraEnabled public String sObjectName;
        @AuraEnabled public List<Object> listOfData;
        @AuraEnabled public List<FilterWrapper> listOfPrimaryFilters;
        @AuraEnabled public List<FilterWrapper> listOfSecondaryFilters;
        @AuraEnabled public List<FieldWrapper> listOfPrimaryFields;
        @AuraEnabled public List<FieldWrapper> listOfSecondaryFields;
        @AuraEnabled public List<FilterWrapper> listOfMobileFilters;
        @AuraEnabled public List<String> listOfFields;
        @AuraEnabled public List<String> listOfPrimaryAPINames;
        @AuraEnabled public String productFilter;
        @AuraEnabled public String gridType;
        @AuraEnabled public String numberOfRecords;
        @AuraEnabled public String rowHeight;
        @AuraEnabled public String rowHeightTablet;
        @AuraEnabled public Boolean isMobile;
        @AuraEnabled public Boolean hasUnselectedPrimaryFilters;
        @AuraEnabled public String headerFontSize;
        @AuraEnabled public String columnFontSize;
        @AuraEnabled public String headerFontSizeMobile;
        @AuraEnabled public String columnFontSizeMobile;
        @AuraEnabled public String fontFamily;
        @AuraEnabled public Boolean hasAccessories;
        @AuraEnabled public String accessoryCheckField;
        @AuraEnabled public Boolean hasIndicator;
        @AuraEnabled public String productCategory;
        @AuraEnabled public String grid;
        @AuraEnabled public List<Grid_Product_Category__mdt> productCategories;
        // Start of Code - MB - Bug - 70808 - 2/20/19
        @AuraEnabled public String globalSearchText;
        @AuraEnabled public Map<String, List<Id>> productCategoryIdMap;
        @AuraEnabled public List<Grid_Product_Category__mdt> filteredProductCategories;
        @AuraEnabled public List<Id> cplIdList;
        // End of Code - MB - Bug - 70808 - 2/20/19
        @AuraEnabled public List<ResidentialPriceGridProductCategory> productCategoryZonesMap; //Added by MB - Bug 71631 - 3/19/19
    }

    public class FilterWrapper {
        @AuraEnabled public String label;
        @AuraEnabled public String fieldName;
        @AuraEnabled public Decimal order;
        @AuraEnabled public String fieldType;
        @AuraEnabled public Boolean isAvailableOnMobile;
        @AuraEnabled public List<Map<String, String>> listOfPicklistValues;
        @AuraEnabled public String stringValue;
        @AuraEnabled public Date dateValue;
        @AuraEnabled public Grid_Category_Field__mdt field;
        @AuraEnabled public List<Grid_ActionHelper.FilterData> listOfFilterOptions;
        @AuraEnabled public Boolean hasFilterOptions = false;

        public FilterWrapper(Grid_Category_Field__mdt field, Decimal order) {
            this.field = field;
            this.label = field.Field_Label__c;
            this.fieldName = field.Field_API_Name__c;
            this.order = order;
            this.fieldType = field.Data_Type__c;
            this.stringValue = '';
            this.isAvailableOnMobile = field.Is_Available_On_Mobile__c;
        }

        public FilterWrapper(Grid_Category_Field__mdt field, Decimal order, String productCategory, String gridType) {
            this(field, order);
            if(String.isNotBlank(field.Filter_Values_Identifier__c)){
                this.listOfFilterOptions = Grid_ActionHelper.getFilterOptions(field.Filter_Values_Identifier__c, productCategory, gridType);
                this.hasFilterOptions = !this.listOfFilterOptions.isEmpty();
            }
        }
        
        public FilterWrapper(Grid_Category_Field__mdt field, Decimal order, String productCategory, String gridType, String accountId) {
            this(field, order);
            if(String.isNotBlank(field.Filter_Values_Identifier__c)){
                this.listOfFilterOptions = Grid_ActionHelper.getFilterOptions(field.Filter_Values_Identifier__c, productCategory, gridType, accountId);
                this.hasFilterOptions = !this.listOfFilterOptions.isEmpty();
            }
        }        
    }

    public class FieldWrapper {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public Decimal order;
        @AuraEnabled public Boolean isAvailableOnMobile;
        @AuraEnabled public String fieldType;
        @AuraEnabled public String columnWidth;
        @AuraEnabled public Boolean isSecondaryLink;
        @AuraEnabled public Grid_Category_Field__mdt field;
        @AuraEnabled public Boolean isIndicator;

        public FieldWrapper(Grid_Category_Field__mdt field, Decimal order) {
            this.field = field;
            this.fieldName = field.Field_API_Name__c;
            this.label = field.Field_Label__c;
            this.order = order;
            this.fieldType = field.Data_Type__c;
            this.columnWidth = field.Column_Width__c;
            this.isSecondaryLink = field.Is_Secondary_Link__c;
            this.isAvailableOnMobile = field.Is_Available_On_Mobile__c;
        }
    }
}