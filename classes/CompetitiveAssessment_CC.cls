/*********************************************************
Description : Controller For Competitive Assessment Page
CreatedBy : Lillian Chen
**********************************************************/
public with sharing class CompetitiveAssessment_CC {
    
    @AuraEnabled
    public static Map<String, Object> initalAssessments(String recordId){
        System.debug(LoggingLevel.INFO, '*** recordId: ' + recordId);
        
        Map<String, Object> initalMap = new Map<String, Object>();
        List<String> brandList = new List<String>();
        List<String> segList = new List<String>();
        List<Object> productList = getProductAssessment(recordId, '',UtilityCls.COMPETITIVE_DEFAULT);
        initalMap.put('Product', productList);
        List<Object> marketList = getMarketAssessment(recordId, '',UtilityCls.COMPETITIVE_DEFAULT);
        initalMap.put('Market', marketList);
        //List<Account> accList = [SELECT Id, Name FROM Account];
        List<Competitor_Products__c>  cpList = [SELECT Id, Name FROM Competitor_Products__c];
        
        //segList.add('None');
        segList.addAll(getSegementValues());
        
        initalMap.put('Brand', brandList);
        //initalMap.put('Account', accList);
        initalMap.put('CompetitorProduct', cpList);
        initalMap.put('Segement',segList);
        initalMap.put('Fiber', UtilityCls.getPicklistValues('Competitive_Assessment__c', 'Fiber__c'));
        initalMap.put('Construction', UtilityCls.getPicklistValues('Competitive_Assessment__c', 'Construction__c'));
        initalMap.put('Default', UtilityCls.COMPETITIVE_DEFAULT);
        initalMap.put('ProductType', getProductTypeId());
        initalMap.put('ShareType', getShareTypeId());
        
        System.debug(LoggingLevel.INFO, '*** initalMap: ' + initalMap);
        return initalMap;
    }
    
    public static String getProductTypeId(){
        return (String)UtilityCls.getRecordTypeInfo('Competitive_Assessment__c', UtilityCls.COMPETITIVE_PRODUCT);
    }
    
    public static String getShareTypeId(){
        return (String)UtilityCls.getRecordTypeInfo('Competitive_Assessment__c', UtilityCls.COMPETITIVE_SHARE);
    }
    
    @AuraEnabled
    public static String getCompetitorProductsRecordTypeId( string recordTypeName ){
        return (String)UtilityCls.getRecordTypeInfo('Competitor_Products__c', recordTypeName);
    }
    //Method to get current Account Competitors list of Products ids to filter competitive Assessment records
    /* public static Set<Id> accCompetitorIds(String recordId){

Set<Id>  competitorProdIds = new Set<Id>();
for(Competitor_Products__c cProd : [Select Id FROM Competitor_Products__c 
WHERE Competitor__c IN (SELECT Competitor__c FROM Account_Competitor__c WHERE Account__c =:recordId)])
{
competitorProdIds.add(cProd.Id);
}

return competitorProdIds;
}*/
    
    //Method to get accountProile id of current Account
    public static String accountProfileId(String recordId){
        try{
            Account ac =  new Account();
            ac = [SELECT Id, Account_Profile__c FROM Account WHERE Id =:recordId];
            
            if(ac.Account_Profile__c != null){
                return ac.Account_Profile__c ;
            }else{
                return null;
            }  
        }catch(Exception ex){
            if(recordId!=null){
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','accountProfileId','',recordId,'');
            }
            else{
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','accountProfileId','','recordId','');
            }

            return null;
        } 
    }
    
    @AuraEnabled
    public static Map<String, Object> getFilterList(String recordId, String brand, String segement){
        
        Map<String, Object> filterMap = new Map<String, Object>();
        filterMap.put('Product', getProductAssessment(recordId, '', segement));
        filterMap.put('Market', getMarketAssessment(recordId,'', segement));
        System.debug(LoggingLevel.INFO, '*** filterMap: ' + filterMap);
        return filterMap;
    }
    
    public static List<Object> getProductAssessment(String recordId, String brand, String segement){
        
        try{
            String accProfileId =  accountProfileId(recordId);
            String sql = 'SELECT Name,RecordType.Name,Competitor_Product__r.Competitor__r.Name,Competitor_Product__r.Competitor__c,Competitor_Product__r.Name,Competitor_Product__r.Style_Number__c,Competitor_Product__r.Fiber_Type__c,Competitor_Product__r.Construction__c, Competitor_Product__r.Category__c,Competitor_Product__r.Fiber__c,Competitor_Product__r.Weight__c,Competitor_Product__r.Face_Weight__c,Competitor_Product__r.Thickness__c,Competitor_Product__r.Density__c,Competitor_Product__r.Mohawk_Product_Type__c,Competitor_Product__r.Plank_Design__c,Competitor_Product__r.Application__c,Competitor_Product__r.Size__c,Competitor_Product__r.Locking_System__c,Competitor_Product__r.Wear_Layer__c,Competitor_Product__r.Width__c,Competitor_Product__r.Surface_Appearance__c,Competitor_Product__r.Moisture_Barrier__c,Competitor_Product__r.Antimicrobial__c,Competitor_Product__r.Species__c,Competitor_Product__r.Texture__c,Competitor_Product__r.Appearance__c,Competitor_Product__r.Gauge__c,RecordTypeId,Competitor__c,Product_Type__c,Competitor__r.Name,Competitor__r.Business_Unit__c, Velocity__c, Price__c FROM Competitive_Assessment__c ';
            String whereRecordType= 'WHERE RecordType.Name = \''+ UtilityCls.COMPETITIVE_PRODUCT +'\'';
            sql += whereRecordType;
            
            System.debug(LoggingLevel.INFO, '*** sql: ' + sql);
            
            //String compProdIds = UtilityCls.getStringFormatForSetIdsIncludesSOQL(accCompetitorIds(recordId));
            if(recordId!=null && recordId.length() >1){
                //String whereRecordId = ' AND Competitor__c = \''+recordId+'\'';
                String whereRecordId = ' AND Account_Profile__c =  \''+ accProfileId  +'\'';
                sql += whereRecordId;
            }
            
            System.debug(LoggingLevel.INFO, '*** sql: ' + sql);
            
            if(segement != null && segement.length() >1){
                //sql += ' AND Account_Profile__r.Product_Type__c = \''+ segement+'\'';
                sql += ' AND Product_Type__c = \''+segement+'\'';
            }
            string ORDERBY = ' ORDER BY createddate DESC';
            List<Competitive_Assessment__c> productList = Database.query(sql + ORDERBY);
            System.debug(LoggingLevel.INFO, '*** productList: ' + productList);
            return productList;
        }catch(Exception ex){
            if(recordId!=null){
                UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','getProductAssessment','',recordId,'');
            }
            else{
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','getProductAssessment','','recordId','');
            }
            return new List<Object>{ex.getMessage()};
                }
    }
    
    public static List<Object> getMarketAssessment(String recordId, String brand, String segement){
        
        try{
            String sql = 'SELECT Name, RecordType.Name, RecordTypeId,Competitor_Estimated_Sales__c, Competitor_Share__c,Account_Profile__r.Brand__c, Account_Profile__r.Product_Type__c,Competitor__r.Name FROM Competitive_Assessment__c';
            String whereRecordTYpe = ' WHERE RecordType.Name = \''+ UtilityCls.COMPETITIVE_SHARE +'\'';
            String accProfileId =  accountProfileId(recordId);
            sql += whereRecordType;
            if(recordId!=null && recordId.length() >1){
                String whereRecordId = ' AND Account_Profile__c = \'' +accProfileId+'\'';
                sql += whereRecordId;
            }
            
            if(segement != null && segement != 'None' && segement.length() >1){
                //sql += ' AND Account_Profile__r.Product_Type__c = \''+ segement+'\'';
                sql += ' AND Product_Type__c = \''+segement+'\'';
            }
            string ORDERBY = ' ORDER BY createddate DESC';
            List<Competitive_Assessment__c> shareList = Database.query(sql + ORDERBY);
            
            return shareList;
        }catch(Exception ex){
            if(recordId!=null){
                UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','getMarketAssessment','',recordId,'');
            }
            else{
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','getMarketAssessment','','recordId','');
            }          
            return new List<Object>{ex.getMessage()};
                }
    }
    
    /*public static List<String> getBrandValues(){
return UtilityCls.getPicklistValues('Account_Profile__c', 'Brand__c');
}*/
    
    public static List<String> getSegementValues(){
        return UtilityCls.getPicklistValues('Competitive_Assessment__c', 'Product_Type__c');
    }
    
    @AuraEnabled
    public static String updateAssessments(Competitive_Assessment__c modifiedCA){
        try{
            
            update modifiedCA;
            return 'true';
        }catch(DmlException ex){
            //UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','updateAssessments','',modifiedCA.id,'');
            if(modifiedCA.Id!=null){
                UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','updateAssessments','',modifiedCA.Id,'');
            }
            else{
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','updateAssessments','','recordId','');
            }          
            System.debug(LoggingLevel.INFO, '*** ex: ' + ex);
            return ex.getMessage();
        }   
    }
    
    @AuraEnabled
    public static String insertAssessment(Competitive_Assessment__c newAss, String recordId){
        try{
            
            String acProfileId = accountProfileId(recordId);
            if(acProfileId == null){ 
                return Label.Competitive_Assessment_Error1;
            }else{
                newAss.Account_Profile__c = acProfileId;
            }
            
            system.debug('newAss-$-' + newAss);
            insert newAss;
            return 'SUCCESS';
            //return null;
        }catch(DmlException ex){
             if(recordId!=null){
                UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','insertAssessment','',recordId,'');
            }
            else{
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','insertAssessment','','recordId','');
            } 
            return ex.getMessage();
        }
    }
    
    /*@AuraEnabled
public static Boolean updateAssessment(Competitive_Assessment__c curAss){
try{
update curAss;
return true;
}catch(DmlException ex){
return false;
}
} */
    
    @AuraEnabled
    public static String deleteAssessment(Competitive_Assessment__c curAss){
        try{
            delete curAss;
            return 'true';
            
        }catch(DmlException ex){
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','deleteAssessment','','Recordid','');
            return ex.getMessage();
            
        }
    }
    /*@AuraEnabled
public static Map<String,List<String>> getDependentPicklistOptions(String pObjName, String pControllingFieldName, String pDependentFieldName,String controllingFieldValue){
Map<String, List<String>> pickMap = new Map<String, List<String>>();
pickMap.put('All', new List<String>{'All'});
pickMap.putAll(UtilityCls.getDependentOptions(pObjName,pControllingFieldName,pDependentFieldName,controllingFieldValue));
return pickMap;
}*/
    
    //Method is use to insert  the Competitor_Products__c record - MUDIT
    @AuraEnabled
    public static returnWrapper createNewCompetitorProduct( string CP,string recordTypeName ){
        if( CP != null && CP.trim().length() > 0 ){
            Map<String, Object> parsedJsonMap = ( Map<String, Object> ) JSON.deserializeUntyped( CP );
            Competitor_Products__c newCompetitorProduct = new Competitor_Products__c(
               // Name = (STRING)parsedJsonMap.get( 'Name' )+' - '+(STRING)parsedJsonMap.get( 'Style_Number__c' ),
                Name=(STRING)parsedJsonMap.get( 'Name' ),
                Competitor__c = (ID)parsedJsonMap.get( 'Competitor__c' ),
                Competitor_Product__c = (ID)parsedJsonMap.get( 'Competitor_Product__c' ),
                Product_Description__c = (STRING)parsedJsonMap.get( 'Product_Description__c' ),
                Construction__c = (STRING)parsedJsonMap.get( 'Construction__c' ),
                Style_Number__c = (STRING)parsedJsonMap.get( 'Style_Number__c' ),
                Brand_Description__c = (STRING)parsedJsonMap.get( 'Brand_Description__c' ),
                Fiber_Type__c = (STRING)parsedJsonMap.get( 'Fiber_Type__c' ),
                Fiber_Description__c = (STRING)parsedJsonMap.get( 'Fiber_Description__c' ),
                Face_Weight__c = (STRING)parsedJsonMap.get( 'Face_Weight__c' ),
                Roll_Price__c = (DECIMAL)parsedJsonMap.get( 'Roll_Price__c' ),
                Cut_Price__c = (DECIMAL)parsedJsonMap.get( 'Cut_Price__c' ),
                Sales_Velocity__c = (STRING)parsedJsonMap.get( 'Sales_Velocity__c' ),
                Category__c = (STRING)parsedJsonMap.get( 'Category__c' ),
                Fiber__c = (STRING)parsedJsonMap.get( 'Fiber__c' ),
                Texture__c = (STRING)parsedJsonMap.get( 'Texture__c' ),
                Plank_Design__c = (STRING)parsedJsonMap.get( 'Plank_Design__c' ),
                Appearance__c = (STRING)parsedJsonMap.get( 'Appearance__c' ), 
                Gauge__c = (STRING)parsedJsonMap.get( 'Gauge__c' ),
                Thickness__c = (STRING)parsedJsonMap.get( 'Thickness__c' ),
                Density__c = (STRING)parsedJsonMap.get( 'Density__c' ),
                Moisture_Barrier__c = (STRING)parsedJsonMap.get( 'Moisture_Barrier__c' ),
                Antimicrobial__c = (STRING)parsedJsonMap.get( 'Antimicrobial__c' ),
                Width__c = (STRING)parsedJsonMap.get( 'Width__c' ),
                Species__c = (STRING)parsedJsonMap.get( 'Species__c' ),
                Application__c = (STRING)parsedJsonMap.get( 'Application__c' ),
                Size__c = (STRING)parsedJsonMap.get( 'Size__c' ),
                Wear_Layer__c = (STRING)parsedJsonMap.get( 'Wear_Layer__c' ),   
                Weight__c = decimal.valueof( (STRING)parsedJsonMap.get( 'Weight__c' ) != null && (STRING)parsedJsonMap.get( 'Weight__c' ) != '' ? (STRING)parsedJsonMap.get( 'Weight__c' ) : '0' ),
                RecordTypeId = UtilityCls.getRecordTypeInfo( 'Competitor_Products__c',recordTypeName )
            );
            System.debug('Moisture Barrier: ' + (STRING)parsedJsonMap.get( 'Moisture_Barrier__c' ));
            try{
                insert newCompetitorProduct;
                
                //return newCompetitorProduct.id;        
                return new returnWrapper( 'SUCCESS',newCompetitorProduct.id );
            }catch( System.DmlException ex ){ 
            UtilityCls.createExceptionLog(ex,'CompetitiveAssessment_CC','deleteAssessment','','','');
                system.debug(':::EXCEPTION::::' + ex.getMessage() );
                //system.debug(':::EXCEPTION::::' + ex.getDmlFieldNames(0));
                //system.debug(':::EXCEPTION::::' + ex.getDmlMessage(0) );
                //return null;
                return new returnWrapper( 'ERROR',ex.getMessage() );
            }
        }else{
            return null;
        }
    }
    @AuraEnabled
    public static String getrecordtypeIdforAll( string objectAPIName,string RecordTypeName ){
        return (String)UtilityCls.getRecordTypeInfo(objectAPIName, RecordTypeName);
    }
    
    public class returnWrapper{
        @AuraEnabled
        public string status{get;set;}
        @AuraEnabled
        public string value{get;set;}
        public returnWrapper( string s,string v ){
            this.status = s;
            this.value = v;
        }
    }
    
}