public class AccountCompetitorTriggerHandler {
    public static void updateAccountCompetitorOnCreate(List<Account_Competitor__c> accCompList){
        Set<Id> accIds = new Set<Id>();
        Map<Id, String> accNameMap = new Map<Id, String>();
        for(Account_Competitor__c accComp: accCompList){
            if(accComp.Account__c != null && accComp.Competitor__c != null){
                accIds.add(accComp.Account__c);
                accIds.add(accComp.Competitor__c);
            }
        }
        if(accIds.size()>0){
            for(Account acc: [SELECT Id, Name 
                              FROM Account
                              WHERE Id IN :accIds]){
                                  accNameMap.put(acc.Id, acc.Name);
                              }
        }
        for(Account_Competitor__c accComp: accCompList){
            if(accNameMap.containsKey(accComp.Account__c)){
                accComp.Account_Name__c = accNameMap.get(accComp.Account__c);
            }
            if(accNameMap.containsKey(accComp.Competitor__c)){
                accComp.Competitor_Name__c = accNameMap.get(accComp.Competitor__c);
            }
        }
    }
}