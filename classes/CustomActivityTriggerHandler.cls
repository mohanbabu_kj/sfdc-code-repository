public without sharing class CustomActivityTriggerHandler{
    
    public static void insertFields( List<Custom_Activities__c> newList ){
        
        CustomActivityTriggerHandler.insertCustomActivity(newList);//Added by Nagendra - 04-03-2019 - Bug 71058
        if( newList != null && newList.size() > 0 ){
            Set<id>userIds = new Set<id>();
            set<id>AccountIds = new Set<id>();
            for( Custom_Activities__c CA : newList ){
                if( CA.WhatId__c.substring( 0,3 ) == '001' && CA.Assigned_To__c != null ){
                    userIds.add( CA.Assigned_To__c );
                    AccountIds.add( CA.WhatId__c );
                }
            }
            
            //Map<id,id>MATIds = new Map<id,id>();
            Map<id,Map<id,id>>MATIds = new Map<id,Map<id,id>>();
            for( Mohawk_Account_Team__c MAT : [ SELECT id,User__c,Account__c FROM Mohawk_Account_Team__c WHERE User__c IN : userIds AND Account__c IN : AccountIds] ){
                //MATIds.put( MAT.User__c,MAT.id );
                MATIds.put( MAT.Account__c,new Map<id,id>{ MAT.User__c => MAT.id } );
            }
            for( Custom_Activities__c CA : newList ){
                System.debug('Custom Activity: ' + CA);
                Map<Id, Id> matIdUserIdMap = new Map<Id, Id>();
                if( CA.WhatId__c.substring( 0,3 ) == '001'){
                    CA.Activity_Related_To__c = 'Account';
                    matIdUserIdMap = MATIds.containsKey(CA.WhatId__c) ? MATIds.get( CA.WhatId__c ) : null;
                    CA.Mohawk_Account_Team__c =  matIdUserIdMap != null && matIdUserIdMap.containsKey(CA.Assigned_To__c) ? matIdUserIdMap.get( CA.Assigned_To__c ) : null;
                }else{
                    CA.Activity_Related_To__c = 'Mohawk Account Team';
                    CA.Mohawk_Account_Team__c = CA.WhatId__c;
                }
            }
        }
    }
    
    //Start of Code by Nagendra - 04-03-2019 - Bug 71058
    public static void insertCustomActivity( List<Custom_Activities__c> newList ){
        Set< Id> userIds = new Set<Id>();
        Map<Id,List<Territory_User__c>> terrIdToTerritoryUsersMap= new Map<Id,List<Territory_User__c>>();
        Set<Id> terrIds = new Set<Id>();
        Map<Id,set<Id>> userTerritoryIdMap= new Map<Id,set<Id>>();
        
        for(Custom_Activities__c ca:newList){
            userIds.add(ca.Assigned_To__c);      
        }
        
        for(List<Territory_User__c> tUserList:[SELECT Id, Territory__c, User__c FROM Territory_User__c
                                              WHERE User__c IN :userIds AND Role__c = 'Territory Manager']){
                                                  for(Territory_User__c tUser: tUserList){
                                                      if(userTerritoryIdMap.containskey(tUser.User__c)){
                                                          userTerritoryIdMap.get(tUser.User__c).add(tUser.Territory__c);
                                                      }else{ 
                                                          userTerritoryIdMap.put(tUser.User__c,new set<id>{tUser.Territory__c});
                                                      }
                                                      terrIds.add(tUser.Territory__c);  
                                                  }
                                              }
        if(terrIds.size()>0){
            for(List<Territory_User__c> tUserList: [SELECT Id, Territory__c, User__c, Role__c, Territory_Code__c FROM Territory_User__c
                                                   WHERE Territory__c in :terrIds 
                                                   AND Role__c IN ('District Manager', 'Regional Vice President', 'Business Development Manager') 
                                                   AND User__c != null]){
                                                       for(Territory_User__c tUser: tUserList){
                                                           if(terrIdToTerritoryUsersMap.containskey(tUser.Territory__c)){
                                                               terrIdToTerritoryUsersMap.get(tUser.Territory__c).add(tUser); 
                                                           }else{
                                                               terrIdToTerritoryUsersMap.put(tUser.Territory__c,new list<Territory_User__c>{tUser});
                                                           }
                                                       }
                                                   }
        }
        for(Custom_Activities__c ca: newList){
            String DM_Ids = '',RVP_Ids = '',BDM_Ids = '';
            //here pass the Assigned To to userTerritoryIdMap and get the territories ids and loop it.
            if(userTerritoryIdMap.containskey(ca.Assigned_To__c)){
                for(Id terrId: userTerritoryIdMap.get(ca.Assigned_To__c)){
                    if(terrIdToTerritoryUsersMap.containskey(terrId)){
                        for(Territory_User__c tUser:terrIdToTerritoryUsersMap.get(terrId)){
                            if(tUser.Role__c == 'District Manager'){
                               DM_Ids =  (DM_Ids !=null) ? (DM_Ids.contains(tUser.User__c) ?  DM_Ids : DM_Ids+';'+tUser.User__c) : tUser.User__c;
                            }else if(tUser.Role__c == 'Regional Vice President'){
                               RVP_Ids = (RVP_Ids !=null) ? (RVP_Ids.contains(tUser.User__c) ? RVP_Ids : RVP_Ids+';'+tUser.User__c) : tUser.User__c;
                            }else if(tUser.Role__c == 'Business Development Manager'){
                               BDM_Ids = (BDM_Ids !=null) ? (BDM_Ids.contains(tUser.User__c) ? BDM_Ids : BDM_Ids+';'+tUser.User__c) : tUser.User__c;
                            }
                        }
                    } 
                }
            }
            
            ca.BDM_Ids__c = BDM_Ids;
            ca.DM_Ids__c = DM_Ids;
            ca.RVP_Ids__c = RVP_Ids;
          
        }
    }
    //End of Code by Nagendra - 04-03-2019 - Bug 71058
}