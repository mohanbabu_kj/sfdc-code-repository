public class Grid_AccessoriesHelper{    
    
    public static final string PRODUCT_FIELD_NAME = 'product__c';
    public static final string PRICE_GRID = 'Price Grid';
    public static final string TRIMS_AND_MOLDING = 'TRIMS_AND_MOLDING';
    public static final string TEXT_FIELD_TYPE = 'text';
    public static final string COLORS = 'Colors';

    public static String zone;
    public static String region;
    public static String GridTypesObjectName;


    //Function used to calculate display of accessory tab based on defination in custom metadata
    //
    //@param
    //      String
    //          - product category Id.
    //@return
    //      Boolean - value retrived for showAccessory

    public static Boolean showViewAccessories(string productCategoryId, string recordId, string sObjectName){
        System.debug('productCategoryId::' + productCategoryId);
        List<Grid_Accessory_Field__mdt> accessoryFields = [SELECT Id
                                                           FROM Grid_Accessory_Field__mdt 
                                                           WHERE Grid_Product_Category__c =:productCategoryId];
        
        string query = 'Select Product__r.Accessory_Identifier_Code__c From ' + sObjectName + ' Where Id=:recordId';
        List<Sobject> listOfData = Database.query(query);
        if (listOfData.size() > 0){
            if (listOfData[0].getSObject('Product__r').get('Accessory_Identifier_Code__c') == null 
                && !accessoryFields.isEmpty()){
                    return true;
                }
        }

        return false;
    }
    
    public static String objectNameForAccessoryCategory(String accessoryCategory, String sObjectName) {
        
        if (accessoryCategory == 'CUSHION') 			return 'CPL_Installation_Accessories__c';
        if (accessoryCategory == 'UNDERLAYMENTS') 		return 'CPL_Installation_Accessories__c';
        if (accessoryCategory == 'ADHESIVES_SEALERS') 	return 'CPL_Installation_Accessories__c';
        if (accessoryCategory == 'INSTALLATION_ITEMS') 	return 'CPL_Installation_Accessories__c';
        if (accessoryCategory == 'TRIMS_AND_MOLDING') 	return sObjectName;
        if (accessoryCategory == 'CARE_MAINTENANCE') 	return 'CPL_Care_and_Maintenance__c';
        
        return '';
    }

    
    //Function used to fetch Accessory Data based on recordid,ProductCategoryId and sobject name passed
    //
    //@param
    //      String
    //          - product category Id.
    //      String
    //          - sobject name
    //      String
    //          - recordId
    //@return
    //      List<Object>
    //          - Accessory Grid Data
    public static List<Object> getAccessories(string recordId, string productCategoryId, string sObjectName){
        return Grid_AccessoriesHelper.getAccessories(recordId, productCategoryId, sObjectName, '', '');
    }
    
    public static List<Object> getAccessories(string recordId, string productCategoryId,string sObjectName, String accountId, String gridType){


        ID recordIdValue = Id.valueOf(recordId);

        String queryFields ='Id, product__c';

        if (sObjectName != null && sObjectName.toLowerCase() == 'price_grid__c') 
            queryFields += ', Region__c, Zone__c';
        

        string query = 'select ' + queryFields + ' from '+sObjectName+' where id = \'' + recordIdValue + '\'';
        System.debug('### getAccessories - query: ' + query);

        List<sObject> gridProduct = Database.query(query);        

        if (gridProduct != null && gridProduct.size() > 0 && sObjectName != null && sObjectName.toLowerCase() == 'price_grid__c') {
            sObject so = gridProduct.get(0);
            zone = so.get('Zone__c').toString();
            region = so.get('Region__c').toString();
        }

        System.debug('### getAccessories - Zone__c: ' + zone);
        System.debug('### getAccessories - Region__c: ' + region);

        List<Grid_Product_Category__mdt> gridCategory = [SELECT Product_Category__c FROM Grid_Product_Category__mdt WHERE id=:Id.valueOf(productCategoryId)];
        List<String> listOfGridName = new List<String>();
        Map<String,String> mapOfGridNameAndlabel = new Map<String,String>();
        Map<String,String> mapOfGridNameAndObjectName = new Map<String,String>();
        Map<String, List<Grid_Accessory_Field__mdt>> accessoryFieldsByGrid = new Map<String, List<Grid_Accessory_Field__mdt>>();
        System.debug('productCategoryId::' + productCategoryId);
        List<Grid_Accessory_Field__mdt> accessoryFields = [SELECT Accessory_Grid__c, Field_Label__c, Field_API_Name__c,Order__c,Data_Type__c,Grid_Display_Order__c, Accessory_Grid_Label__c, Grid_Product_Category_Object_Name__c 
                                                           FROM Grid_Accessory_Field__mdt 
                                                           WHERE Grid_Product_Category__c =:productCategoryId 
                                                           ORDER BY Grid_Display_Order__c, Order__c ];
        for(Grid_Accessory_Field__mdt field : accessoryFields){
            if(String.isNotBlank(field.Accessory_Grid__c)){
                if(accessoryFieldsByGrid.containsKey(field.Accessory_Grid__c)){
                    List<Grid_Accessory_Field__mdt> listOfMetadata =  accessoryFieldsByGrid.get(field.Accessory_Grid__c);
                    listOfMetadata.add(field);
                    accessoryFieldsByGrid.put(field.Accessory_Grid__c, listOfMetadata);
                }
                else{
                    List<Grid_Accessory_Field__mdt> listOfMetadata = new List<Grid_Accessory_Field__mdt>();
                    listOfMetadata.add(field);
                    accessoryFieldsByGrid.put(field.Accessory_Grid__c, listOfMetadata);
                    listOfGridName.add(field.Accessory_Grid__c);
                    mapOfGridNameAndlabel.put(field.Accessory_Grid__c,field.Accessory_Grid_Label__c);
                    mapOfGridNameAndObjectName.put(field.Accessory_Grid__c,field.Grid_Product_Category_Object_Name__c);
                    
                }
            }
        }
        
        
        List<ViewData> listOfViewData = new List<ViewData>();
        for(String s : listOfGridName){
            //LIST OF GRID TYPES
            //Price_Grid
            //Buying_Group_Grid
            //Merchandizing_Vehicle_Price_List
            //Customer_Price_List
            
            //Get all Lists with Product, No Account, and Accessory Grid Value from Grid Accessory Field Metadata
            //listOfViewData.add(getAccessoriesListWrapper((String)gridProduct[0].get(PRODUCT_FIELD_NAME), '', s, mapOfGridNameAndlabel.get(s), accessoryFieldsByGrid,mapOfGridNameAndObjectName.get(s),productCategoryId,sObjectName));
            
            if (accountId == null) accountId = '';
            system.debug('### ********* ' + s + ' **********');
            system.debug('### accessory object: ' + Grid_AccessoriesHelper.objectNameForAccessoryCategory(s, sObjectName));
            system.debug('### mapOfGridNameAndlabel.get(s): ' + mapOfGridNameAndlabel.get(s));
            system.debug('### accessoryFieldsByGrid: ' + accessoryFieldsByGrid);
            system.debug('### mapOfGridNameAndObjectName.get(s): ' + mapOfGridNameAndObjectName.get(s));
            
            String accessoryObjectName = Grid_AccessoriesHelper.objectNameForAccessoryCategory(s, sObjectName);
            
            
            listOfViewData.add(getAccessoriesListWrapper((String)gridProduct[0].get(PRODUCT_FIELD_NAME), accountId, s, mapOfGridNameAndlabel.get(s), accessoryFieldsByGrid,accessoryObjectName /*mapOfGridNameAndObjectName.get(s)*/,productCategoryId,sObjectName, gridType));
        }
        Grid_Product_Category__mdt prodCategory = [SELECT Id, Product_Category__c, 
                                                   Product_Category_Field_API_Name__c,
                                                   Grid_Type__r.Grid_Type__c,
                                                   Grid_Type__r.Number_of_Records_per_Page__c,
                                                   Grid_Type__r.Row_Height__c,
                                                   Grid_Type__r.Row_Height_Tablet__c,
                                                   Grid_Type__r.Header_Font_Size__c,
                                                   Grid_Type__r.Column_Font_Size__c,
                                                   Grid_Type__r.Header_Font_Size_Mobile__c,
                                                   Grid_Type__r.Column_Font_Size_Mobile__c,
                                                   Grid_Type__r.Font_Family__c
                                                   FROM Grid_Product_Category__mdt
                                                   WHERE Id =:productCategoryId];
        for(ViewData view : listOfViewData){
            view.gridType = prodCategory.Grid_Type__r.Grid_Type__c == PRICE_GRID ? Label.GRID_TYPE_MBP : Label.GRID_TYPE_BGP;
            view.headerFontSize = prodCategory.Grid_Type__r.Header_Font_Size__c;
            view.numberOfRecords = prodCategory.Grid_Type__r.Number_of_Records_per_Page__c;
            view.rowHeight = prodCategory.Grid_Type__r.Row_Height__c;
            view.rowHeightTablet = prodCategory.Grid_Type__r.Row_Height_Tablet__c;
            view.columnFontSize = prodCategory.Grid_Type__r.Column_Font_Size__c;
            view.headerFontSizeMobile = prodCategory.Grid_Type__r.Header_Font_Size_Mobile__c;
            view.columnFontSizeMobile = prodCategory.Grid_Type__r.Column_Font_Size_Mobile__c;
            view.fontFamily = prodCategory.Grid_Type__r.Font_Family__c;
        }
        return listOfViewData;        
    }
    
    
    //Function used to fetch Accessory List Data for a particular accessory Type
    //
    //@param
    //      Id
    //          - productId.
    //      String
    //          - accountId
    //      String
    //          - AccessoryType
    //      String
    //          - AccessoryLabel
    //      Map<String, List<Grid_Accessory_Field__mdt>>
    //          - accessoryFieldsByGrid
    //      String
    //          - accesoryObjectName
    //      String
    //          - product category Id.
    //      String
    //          - sobject name
    //@return
    //      ViewData
    //          - View Data Wrapper
    public static ViewData getAccessoriesListWrapper(Id productId, string accountId, string AccessoryType, string AccessoryLabel, Map<String, List<Grid_Accessory_Field__mdt>> accessoryFieldsByGrid,String accessoryObjectName,string productCategoryId,string sObjectName, String gridType){
        ViewData viewDataDetails = new ViewData();
        viewDataDetails.tab = AccessoryLabel;
        viewDataDetails.isEmpty = false;
        viewDataDetails.listOfPrimaryFields = new List<FieldWrapper>();
		GridTypesObjectName = sObjectName;
        System.debug('### getAccessoriesListWrapper - AccessoryType: ' + AccessoryType);
        System.debug('### getAccessoriesListWrapper - Zone__c: ' + zone);
        System.debug('### getAccessoriesListWrapper - Region__c: ' + region);
        System.debug('### getAccessoriesListWrapper - accesoryObjectName: ' + accessoryObjectName);
        System.debug('### getAccessoriesListWrapper - accountId: ' + accountId);
        System.debug('### getAccessoriesListWrapper - productId: ' + productId);
        System.debug('### getAccessoriesListWrapper - sObjectName: ' + sObjectName);
        System.debug('### getAccessoriesListWrapper - gridType: ' + gridType);
        
        String sObjectToUse = 'Price_Grid__c';
        if (sObjectName != null && sObjectName.toLowerCase() != 'price_grid__c' )
            sObjectToUse = accessoryObjectName;
        
        
        List<Grid_Accessory_Field__mdt> accessoryFields = accessoryFieldsByGrid.get(AccessoryType);

        // System.Debug('### getAccessoriesListWrapper - accessoryFields: ' + accessoryFields);

        if(!accessoryFields.isEmpty()){    
            List<Product_RelationShip__c> listOfAccessoryData = ResidentialPricingGridUtility.getAccessoryProductIds(productId, accountId, AccessoryType);
            if(AccessoryType == TRIMS_AND_MOLDING){
                System.debug('### getAccessoriesListWrapper - it is TRIMS_AND_MOLDING');
                return getAccessoriesTrimListWrapper(listOfAccessoryData, productId, accountId, AccessoryType,accessoryObjectName,AccessoryLabel,productCategoryId, gridType); 
            }
            else{
                System.debug('### getAccessoriesListWrapper - it is NOT TRIMS_AND_MOLDING');
                //Get All accessory field headers
                for(Grid_Accessory_Field__mdt fields : accessoryFields){
                    FieldWrapper data1 = new FieldWrapper();
                    data1.label = fields.Field_Label__c ;
                    data1.fieldName = fields.Field_API_Name__c ;
                    data1.order = fields.Order__c ;
                    data1.fieldType = fields.Data_Type__c;//TEXT_FIELD_TYPE;
                    viewDataDetails.listOfPrimaryFields.add(data1);
                }
                Set<Id> setOfRelatedProductId = new Set<Id>();
                Set<String> setOfRelatedColor = new Set<String>();
                System.debug('### getAccessoriesListWrapper - listOfAccessoryData: ' + listOfAccessoryData);    

                if(listOfAccessoryData!=null && !listOfAccessoryData.isEmpty()){
                    
                    for(Product_RelationShip__c productIterator : listOfAccessoryData){
                        setOfRelatedProductId.add(productIterator.Related_Product__c);
                        setOfRelatedColor.add(productIterator.Related_Product_Color_Number__c);
                    }
                    
                    
                    //Get Prudct Colors
                    String colorQuery = 'select id, name, Color_Number__c, Selling_Color_Name__c,product__c from product_color__c where product__c IN :setOfRelatedProductId and Color_Number__c in : setOfRelatedColor';
                    system.debug('### colorQuery: ' + colorQuery);
                    List<Product_Color__c> colorList = Database.query(colorQuery);
                    system.debug('### colorList: ' + colorList);
                    Map<String, String> sizeDescriptionMap = new Map<String, String>();
                    for (Product_Color__c pc : colorList) {
                        String key = pc.Color_Number__c + '.' + pc.product__c;
                        sizeDescriptionMap.put(key, pc.Selling_Color_Name__c );
                    }                    
                    
                    
                    //Build the query to get the Accesory records
                    String setOfRelatedProductIdJoinStr = '(\'' + String.join(new List<Id>(setOfRelatedProductId), '\',\'') + '\')';
                    String query = 'SELECT Id, ';        
                    Set<String> fields = new Set<String>();
                    System.debug('### list of field: ' + viewDataDetails.listOfPrimaryFields);
                    
                    for(FieldWrapper field : viewDataDetails.listOfPrimaryFields){
                        fields.add(field.fieldName);
                    }
                    for(String field : fields){
                        query += Grid_BaseController.getFieldForQuery(field, sObjectName) + ', ';
                    }
                    query = query.removeEnd(', ');        
                    query += ' FROM '+ sObjectToUse +' WHERE product__c in' + setOfRelatedProductIdJoinStr;  

                    if (sObjectName != null && sObjectName.toLowerCase() == 'price_grid__c') { 
                        if (region != null) query += ' AND Region__c = \'' + region + '\'';
                        if (zone != null)   query += ' AND Zone__c = \'' + zone + '\'';
                    }
                    else{
                        query += ' AND Account__c =:accountId';
                    }

                    List<SObject> queryResult = Database.query(query);
                    viewDataDetails.listOfFields = new List<String>();
                    viewDataDetails.listOfFields.addAll(fields);
                    viewDataDetails.listOfData = queryResult;

                    Set<String> prodList = new Set<String>();
                    Map<String, SObject> partNumMap = new Map<String, SObject>();
                    Map<String, Double> prodPriceMap = new Map<String, Double>();
                    
                    //Build a set of Product and a set of Part Number already included in the queryResult
                    for (SObject obj : queryResult) {
                        String prodId = (String) obj.get('Product__c');
                        SObject prodObj = obj.getSObject('Product__r');
                        String partNum = (String) prodObj.get('Part_Number__c');
                        if (prodId != null) prodList.add(prodId);
                        if (sObjectName != null && sObjectName.toLowerCase() == 'price_grid__c' ) {
                            if (prodId != null) prodPriceMap.put(prodId, (Double) obj.get('TM_Price__c'));
                        }
                        else {
                        	if (prodId != null) prodPriceMap.put(prodId, (Double) obj.get('Billing_Price__c'));
                        }
                        if (partNum != null) partNumMap.put(partNum, obj);
                    }

                    system.debug('### parNums: ' + partNumMap);
                    system.debug('### prodPriceMap: ' + prodPriceMap);
                    
					/***************************************************/
                    
					System.debug('### ');
					System.debug('### *********************************');
                    System.debug('### accessories query: ' + query);
                    System.debug('### accessories sObjectName: ' + sObjectName);
                    System.debug('### accessories listOfData: ' + viewDataDetails.listOfData);

		            System.debug('### listOfAccessoryData: ' + listOfAccessoryData);
                    Set<String> relatedProductId = new Set<String>();
                    for (Product_Relationship__c pr : listOfAccessoryData) {
                        if (pr.Related_Product__c  != null)
	                        relatedProductId.add(pr.Related_Product__c );
                    }
              
                    System.debug('### relatedProductId size: ' + relatedProductId.size());
                    system.debug('### sizeDescriptionMap: ' + sizeDescriptionMap);
                    
                    //Defines Product map variable to be filled below
                    Map<Id, Product2> productMap;

                    //Perfrm the query on Product2 table based on the grid type CPL, Price Grid
                    if (sObjectName != null && sObjectName.toLowerCase() == 'price_grid__c' ) { 
                        productMap = new Map<Id, Product2>([Select id,Name, Part_Number__c, Size_Description__c, Brand_Description__c, Spread_Rate__c, Master_Style_Num__c, Product_Style_Number__c,Selling_Style_Concat__c, (select id from price_grids__r where zone__c =: zone and region__c =: region) from product2 where id IN : relatedProductId]);
                        System.debug('### accessories productMap(' + productMap.size() + '): ' + productMap);
                    }
                    else {
                        String relationshipObject = sObjectToUse.replace('__c', '__r');
                        System.debug('### relationshipObject: ' + relationshipObject);
                        String productQuery = 'SELECT id,Name, Part_Number__c, Size_Description__c, Brand_Description__c, Spread_Rate__c, Master_Style_Num__c, Product_Style_Number__c,Selling_Style_Concat__c, (SELECT id FROM ' + relationshipObject + ' WHERE account__c =: accountId) from product2 WHERE id IN:relatedProductId';
                       	List<Product2> productList = Database.query(productQuery);     
                        productMap = new Map<Id, Product2>(productList);
                    }
                    
                    
                    //****************
                    //Match Product Relationship and Color/Size
                    for(Product_RelationShip__c pr : listOfAccessoryData){
                        Product2 pOriginal = (Product2) productMap.get(pr.Related_Product__c);
                        system.debug('### product(' + pr.Related_Product__c + '): ' + pOriginal);
                        
                        if (pOriginal != null) {
                            Product2 p = pOriginal.clone(true, true, true, true);
                            String color = pr.Related_Product_Color_Number__c;
                            String key = color + '.' + pr.Related_Product__c;
                            String sizeDescription = (String) sizeDescriptionMap.get(key);
                            system.debug('### key: ' + key + ' - description: ' +  sizeDescription + ' - price_Grids: ' + p.price_grids__r);
                            
                            if (p.price_grids__r == null || p.price_grids__r.size() == 0 || sObjectName != null && sObjectName.toLowerCase() == 'price_grid__c'){
                                SObject obj = partNumMap.get(pr.Part_Number__c);
                                system.debug('###');
                                system.debug('### part number: ' + pr.Part_Number__c + ' - ' + obj);
                                if (obj == null) {
                                    //There is no row yet created. Craete a new row based on data from the Product2 Table
                                    sObject cpl = Schema.getGlobalDescribe().get(sObjectToUse).newSObject() ;
                                    
                                    
                                    //Set Billing Price from prodPriceMap
                                    Double price = prodPriceMap.get(p.Id);
                                    system.debug('### price: ' + price);
                                    if (sObjectName != null && sObjectName.toLowerCase() == 'price_grid__c' ) 
                                        cpl.put('TM_Price__c', price);
                                    else
                                        cpl.put('Billing_Price__c',price);
                                    
                                    
                                    //Replace the Size column value for Installation Items
                                    if (AccessoryType == 'INSTALLATION_ITEMS' ) {
                                    	p.put('Size_Description__c', sizeDescription);
                                    }
                                    
                                    p.put('Part_Number__c', pr.Part_Number__c);
                                    
                                    cpl.putSObject('Product__r', p);
                                    cpl.put('Selling_Style_Num__c', p.Product_Style_Number__c);
                                    
                                    viewDataDetails.listOfData.add(cpl);
                                }
                                else {
                                    //There is row already there. Replace the Size column value for Installation Items
                                    if (AccessoryType == 'INSTALLATION_ITEMS' ) {
                                        SObject relatedProd = obj.getSObject('Product__r');
                                        relatedProd.put('Size_Description__c', sizeDescription);
                                    }
                                }
                            }     
                        }
                        
                    }
                    
                    
                    //****************
            
					System.debug('### *********************************');
					System.debug('### ');

                    
					/***************************************************/


                    
                    return viewDataDetails;                    
                }
                else{
                    return viewDataDetails;
                }
            }
        }
        else
            return viewDataDetails;
    }
    
    //Function used to fetch Accessory List for Trims and Molding Accessory Type
    //
    //@param
    //      List<Product_RelationShip__c>
    //          - prodRelationships.
    //      Id
    //          - productId
    //      String
    //          - accountId
    //      String
    //          - AccessoryType
    //      String
    //          - accesoryObjectName
    //      String
    //          - AccessoryLabel
    //      String
    //          - product category Id.
    //@return
    //      ViewData
    //          - View Data Wrapper
    public static ViewData getAccessoriesTrimListWrapper(List<Product_RelationShip__c> prodRelationships, Id productId, string accountId, string AccessoryType,String accesoryObjectName,String AccessoryLabel,string productCategoryId, String gridType){
        //accountId = '0010m00000AT8kgAAD';
        
        ViewData viewDataDetails = new ViewData();
        viewDataDetails.tab = AccessoryLabel;
        viewDataDetails.listOfPrimaryFields = new list<FieldWrapper>();
        // Map<ColorName, Map<ColumnName, ColumnValue>>
        Map<String, Map<String, String>> mapProductsByColor = new Map<String, Map<String, String>>();
        Map<String,String> mapOfColorNumberName = new Map<String,String>();
        Set<String> setOfColors = new Set<String>();
        Set<String> setOfcolumnName = new Set<String>();
        Map<String, String> setOfAccessoryCode = new Map<String, String>();
        List<String> listOfCode;
        for(Grid_Accessory_Trim_Ma__mdt  md : [select Accessory_Code__c,Accessory_Type_Name__c,order__c  from Grid_Accessory_Trim_Ma__mdt where Grid_Product_Category__c =:Id.valueOf(productCategoryId) order by order__c ]){
            System.debug('### accesory metadata: ' + md);
            String accesoryName = md.Accessory_Type_Name__c;
            setOfcolumnName.add(md.Accessory_Type_Name__c);
            if(md.Accessory_Code__c.contains(',')){
                listOfCode =  md.Accessory_Code__c.split(',');
                for(String value : listOfCode){
                    setOfAccessoryCode.put(value, accesoryName);
                }
            }
            else
                setOfAccessoryCode.put(md.Accessory_Code__c, accesoryName);
        }
        
        system.debug('### setOfAccessoryCode: ' + setOfAccessoryCode);
        
        //List<Product_Color__c> listOfAccessoryColorData = ResidentialPricingGridUtility.getProductColors('01tq00000026QjSAAU','', '');
        for(Product_color__c colorIterator : [select color_number__c,selling_color_name__c from Product_color__c where product__c=:productId]){
            mapOfColorNumberName.put(colorIterator.Color_Number__c, colorIterator.Selling_Color_Name__c);
            setOfColors.add(colorIterator.Color_Number__c);
        }
        System.debug('Trim_mapOfColorNumberName==>'+mapOfColorNumberName);
        System.debug('Trim_setOfColors==>'+setOfColors);
        Set<String> setOfUniqueProduct = new Set<String>();
        List<Product_Relationship__c> listOfProductRelationship = [SELECT Related_Product__c, Part_Number__c, Product_Color_Number__c,Related_Product__r.Residential_Product_Unique_Key__c, Related_Product_Color_Number__c, Related_Product__r.Name, Related_Product__r.Master_name__c, Related_Product__r.Accessory_Identifier_Code__c
                                                                   FROM Product_Relationship__c 
                                                                   WHERE Product__c =:productId and Product_Color_Number__c in:setOfColors AND Related_Product__r.Accessory_Identifier_Code__c IN:setOfAccessoryCode.keySet()
                                                                   ORDER BY Related_Product__r.Master_name__c];

        for(Product_Relationship__c iterator : listOfProductRelationship){
            setOfUniqueProduct.add(iterator.Related_Product__r.Residential_Product_Unique_Key__c);
            //setOfcolumnName.add(iterator.Related_Product__r.Name);
        }

        String setOfUniqueProductJoinStr = '(\'' + String.join(new List<String>(setOfUniqueProduct), '\',\'') + '\')';


        System.debug('Trim_setOfcolumnName==>'+setOfcolumnName);
        Map<String,Decimal> mapOfUniqueKeyAndPrice = new Map<String,Decimal>();

        string query;
        if (GridTypesObjectName != null && GridTypesObjectName.toLowerCase() == 'price_grid__c') { 
            query  = 'SELECT product_unique_key__c,product__r.name,Product__r.Accessory_Identifier_Code__c ';

            /*
             * if from price_grid use TM_Price__c field
             * if from buying_group_grid use Group_Price__c field
             */ 

            if (gridType == 'Price_Grid')
                query += ', TM_Price__c ';
            else //case gridType == Buying_Group_Grid
                query += ', Group_Price__c ';
                
            query += 'FROM Price_Grid__c ';
            query += 'WHERE product_unique_key__c IN ' + setOfUniqueProductJoinStr + 'AND Region__c =:region AND Zone__c =:zone';
            
        }
        else{
            query = 'select billing_price__c,product_unique_key__c,product__r.name,Product__r.Accessory_Identifier_Code__c from '+ 
                                accesoryObjectName +' where product_unique_key__C IN ' + setOfUniqueProductJoinStr +
                                'AND Account__c =:accountId';
        }

        system.debug('AccountID==>' + accountId);
        system.debug('AccessoryPriceQuery==>' + query);
        System.debug('accesoryObjectName==>'+accesoryObjectName);
        List<sObject> listOfData = Database.query(query);
        
        system.debug('### ');
        system.debug('### ****************');
        for (Sobject so : listOfData)    {
            system.debug('### so: ' + so);
            if (GridTypesObjectName != null && GridTypesObjectName.toLowerCase() == 'price_grid__c') { 
                if (gridType == 'Price_Grid' && so.get('TM_Price__c') != null && so.get('product_unique_key__C')!=null){
                    mapOfUniqueKeyAndPrice.put((string)so.get('product_unique_key__c'),(Decimal)so.get('TM_Price__c'));
                }
                if (gridType == 'Buying_Group_Grid' && so.get('Group_Price__c') != null && so.get('product_unique_key__C')!=null){
                    mapOfUniqueKeyAndPrice.put((string)so.get('product_unique_key__c'),(Decimal)so.get('Group_Price__c'));
                }
            }
            else{ 
                if (so.get('billing_price__c') != null && so.get('product_unique_key__C')!=null){
                    mapOfUniqueKeyAndPrice.put((string)so.get('product_unique_key__c'),(Decimal)so.get('billing_price__c'));
                }
            }
        }
        system.debug('### ****************');
        
        System.debug('Trim_mapOfUniqueKeyAndPrice==>'+mapOfUniqueKeyAndPrice);
        System.debug('Trim_mapProductsByColor==>'+mapProductsByColor);
        System.debug('Trim_mapOfColorNumberName==>'+mapOfColorNumberName);
        System.debug('Trim_mapOfUniqueKeyAndPrice==>'+mapOfUniqueKeyAndPrice);
        FieldWrapper data1 = new FieldWrapper();
        data1.label = COLORS;
        data1.fieldName = COLORS ;
        data1.order = 1 ;
        data1.fieldType = TEXT_FIELD_TYPE;
        viewDataDetails.listOfPrimaryFields.add(data1);  
        integer i =1;
        
        for(String columnIterator : setOfcolumnName){
            FieldWrapper data2 = new FieldWrapper();
            data2.label = columnIterator;
            data2.fieldName = columnIterator ;
            data2.order = i+1 ;
            data2.fieldType = TEXT_FIELD_TYPE;
            i++;
            viewDataDetails.listOfPrimaryFields.add(data2);
        }
        System.debug('Trim_viewDataDetails.listOfPrimaryFields==>'+viewDataDetails.listOfPrimaryFields);
        System.debug('### ');
        System.debug('### ******************************************');

        for(Product_Relationship__c iterator : listOfProductRelationship){

            String accesoryCode = iterator.Related_Product__r.Accessory_Identifier_Code__c;
            String accerotyTypeName = setOfAccessoryCode.get(accesoryCode);
            System.debug('### processing iterator                : ' + iterator);
            System.debug('### processing iterator related product: ' + iterator.Related_Product__r );
            System.debug('### accesoryCode: ' + accesoryCode);
            System.debug('### accerotyTypeName: ' + accerotyTypeName);
            System.debug('### ');

            String colorNameAndNumber = mapOfColorNumberName.get(iterator.Product_Color_Number__c)  + ' (' + iterator.Product_Color_Number__c + ')';
			
            if(mapProductsByColor.containsKey(colorNameAndNumber)){
                Map<string,string> tempMap = mapProductsByColor.get(colorNameAndNumber);
                //tempMap.put(iterator.Related_Product__r.Name,iterator.Part_Number__c+'-'+iterator.Related_Product_Color_Number__c+'\n'+(mapOfUniqueKeyAndPrice.containsKey(iterator.Related_Product__r.Residential_Product_Unique_Key__c)?(String.isBlank(String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c)))?'':'$'+String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c))):''));
                //Commented below line for Bug 63810 - MB - 08/06/18 - No need to display Related Product Colour Number
                //tempMap.put(accerotyTypeName,iterator.Part_Number__c+'-'+iterator.Related_Product_Color_Number__c+'<br/>'+(mapOfUniqueKeyAndPrice.containsKey(iterator.Related_Product__r.Residential_Product_Unique_Key__c)?(String.isBlank(String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c)))?'':'$'+String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c))):''));
                //Added below line for Bug 63810 - MB - 08/06/18 - Removed Related Product Colour Number
                tempMap.put(accerotyTypeName,iterator.Part_Number__c+'<br/>'+(mapOfUniqueKeyAndPrice.containsKey(iterator.Related_Product__r.Residential_Product_Unique_Key__c)?(String.isBlank(String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c)))?'':'$'+String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c))):''));
				tempMap.put('Product Color Number', iterator.Product_Color_Number__c); //Added by MB - Bug 63809 - 08/06/18
                mapProductsByColor.put(colorNameAndNumber, tempMap);
                //System.debug('### processing(1) ' + mapOfColorNumberName.get(iterator.Product_Color_Number__c) + ': ' + tempMap);
            }
            else{
                Map<string,string> tempMap = new Map<string,string>();                
                //tempMap.put(iterator.Related_Product__r.Name,iterator.Part_Number__c+'-'+iterator.Related_Product_Color_Number__c+'\n'+(mapOfUniqueKeyAndPrice.containsKey(iterator.Related_Product__r.Residential_Product_Unique_Key__c)?(String.isBlank(String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c)))?'':'$'+String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c))):''));
                //Commented below line for Bug 63810 - MB - 08/06/18 - No need to display Related Product Colour Number
                //tempMap.put(accerotyTypeName,iterator.Part_Number__c+'-'+iterator.Related_Product_Color_Number__c+'<br/>'+(mapOfUniqueKeyAndPrice.containsKey(iterator.Related_Product__r.Residential_Product_Unique_Key__c)?(String.isBlank(String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c)))?'':'$'+String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c))):'')); 
                //Added below line for Bug 63810 - MB - 08/06/18 - Removed Related Product Colour Number
                tempMap.put(accerotyTypeName,iterator.Part_Number__c+'<br/>'+(mapOfUniqueKeyAndPrice.containsKey(iterator.Related_Product__r.Residential_Product_Unique_Key__c)?(String.isBlank(String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c)))?'':'$'+String.valueOf(mapOfUniqueKeyAndPrice.get(iterator.Related_Product__r.Residential_Product_Unique_Key__c))):''));
				tempMap.put('Product Color Number', iterator.Product_Color_Number__c); //Added by MB - Bug 63809 - 08/06/18
                mapProductsByColor.put(colorNameAndNumber,tempMap);
                //System.debug('### processing(2) ' + mapOfColorNumberName.get(iterator.Product_Color_Number__c) + ': ' + tempMap);

            }
        }
        System.debug('### ******************************************');
        System.debug('### ');


        
        System.debug('Trim_mapProductsByColor==>'+mapProductsByColor);
        List<Map<String,string>> trimsAndMoldingData  = new List<Map<String,string>>();
        for(String stringIterator : mapProductsByColor.keyset()){
            //list<Map<String,string>> dataList = new  list<Map<String,string>>();
            Map<String,String> dataMap = new Map<String,String>();
            dataMap.put(COLORS,stringIterator);
            for(String valueIterator : mapProductsByColor.get(stringIterator).keyset()){
                dataMap.put(valueIterator,mapProductsByColor.get(stringIterator).get(valueIterator));
            }
            trimsAndMoldingData.add(dataMap);
        }
        System.debug('Trim_trimsAndMoldingData==>'+trimsAndMoldingData);
        viewDataDetails.listOfData = trimsAndMoldingData;
        
        return viewDataDetails;
    }
    
    
    public class ViewData{        
        @AuraEnabled public String productCategoryId;
        @AuraEnabled public String sObjectName;
        @AuraEnabled public List<Object> listOfData;        
        @AuraEnabled public List<FieldWrapper> listOfPrimaryFields;        
        @AuraEnabled public List<String> listOfFields;
        @AuraEnabled public String productFilter;
        @AuraEnabled public Boolean isEmpty;
        @AuraEnabled public String tab;
        @AuraEnabled public String gridType;
		@AuraEnabled public String numberOfRecords;
		@AuraEnabled public String rowHeight;
		@AuraEnabled public String rowHeightTablet;
        @AuraEnabled public Boolean isMobile;
        @AuraEnabled public Boolean hasUnselectedPrimaryFilters;
        @AuraEnabled public String headerFontSize;
        @AuraEnabled public String columnFontSize;
        @AuraEnabled public String headerFontSizeMobile;
        @AuraEnabled public String columnFontSizeMobile;
        @AuraEnabled public String fontFamily;
    }
    
    public class FieldWrapper {
        @AuraEnabled public Id id;
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public Decimal order;
        @AuraEnabled public Boolean isAvailableOnMobile;
        @AuraEnabled public String fieldType;
        @AuraEnabled public String align;
        @AuraEnabled public Boolean isSecondaryLink;
    }
    public class TrimsAndMoldingValues{        
    }    
}