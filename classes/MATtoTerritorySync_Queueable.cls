public without sharing class MATtoTerritorySync_Queueable implements Queueable { 

    private List<Mohawk_Account_Team__c> updatedMATs;
    
    public MATtoTerritorySync_Queueable( List<Mohawk_Account_Team__c> updMATs ) {
       this.updatedMATs = updMATs;
    }

    public void execute(QueueableContext context) {
        MATtoTerritoryAccountService.syncTerritoryAccounts(updatedMATs);  
    }

}