/**************************************************************************

Name : UpdateUserGeography_Batch_Test

===========================================================================
Purpose : This tess class is used for UpdateUserGeography_Batch
===========================================================================
History:

--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha      2/Nov/2017     Created             
***************************************************************************/
@isTest
private class UpdateUserGeography_Batch_Test {
    static testmethod void test1() {
        Territory__c ter=new Territory__c(name='test ter');
        insert ter;
        User ur = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()), 
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US', 
            LocaleSidKey    = 'en_US',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id, 
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert ur;
        system.assert(ur.Business_Type__c=='Commercial');
        String query = 'SELECT Id, Geography__c FROM User WHERE Business_Type__c = \'Commercial\'';
        // User ur=[SELECT Id, Geography__c FROM User WHERE Business_Type__c = 'Commercial' and Geography__c!=null limit 1];
        system.debug('geo'+ur.Geography__c);
        Territory_User__c terrUser =new Territory_User__c(User__c=ur.id,role__c=UtilityCls.AE,Territory__c=ter.id);
        insert terruser;
        
        Test.startTest();
        UpdateUserGeography_Batch em = new UpdateUserGeography_Batch();
        String jobId=Database.executeBatch(em,2000);
        system.assert(jobId!=null);
        Test.stopTest();
    }
    static testmethod void test2() {
        Territory__c ter=new Territory__c(name='test ter');
        insert ter;
        User ur = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()), 
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US', 
            LocaleSidKey    = 'en_US',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id, 
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert ur;
        String query = 'SELECT Id, Geography__c FROM User WHERE Business_Type__c = \'Commercial\'';
        // User ur=[SELECT Id, Geography__c FROM User WHERE Business_Type__c = 'Commercial' and Geography__c!=null limit 1];
        system.debug('geo'+ur.Geography__c);
        Territory_User__c terrUser =new Territory_User__c(User__c=ur.id,role__c=UtilityCls.SVP,Territory__c=ter.id);
        insert terruser;
        
        Test.startTest();
        UpdateUserGeography_Batch em = new UpdateUserGeography_Batch();
        String jobId=Database.executeBatch(em,2000);
        system.assert(jobId!=null);
        Test.stopTest();
    }
    
    static testmethod void test4() {
        Territory__c ter=new Territory__c(name='test ter');
        insert ter;
        User ur = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()), 
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US', 
            LocaleSidKey    = 'en_US',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id, 
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert ur;
        String query = 'SELECT Id, Geography__c FROM User WHERE Business_Type__c = \'Commercial\'';
        // User ur=[SELECT Id, Geography__c FROM User WHERE Business_Type__c = 'Commercial' and Geography__c!=null limit 1];
        system.debug('geo'+ur.Geography__c);
        Territory_User__c terrUser =new Territory_User__c(User__c=ur.id,role__c=UtilityCls.RVP,Territory__c=ter.id);
        insert terruser;
        
        Test.startTest();
        
        UpdateUserGeography_Batch em = new UpdateUserGeography_Batch();
        String jobId=Database.executeBatch(em,2000);
        system.assert(jobId!=null);
        Test.stopTest();
    }
}