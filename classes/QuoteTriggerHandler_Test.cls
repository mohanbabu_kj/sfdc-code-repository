@isTest
public class QuoteTriggerHandler_Test {
    
    @isTest
    public static void methodforValidateStatusChange(){
        List<Account> accList = new List<Account>();
        Default_Configuration__c defConfig = Utility_Test.getDefaultConfCusSetting();
        Quote quote = new Quote();
        String oppId = '';
        
        defConfig.Fire_Account_Trigger__c = false;
        defConfig.Fire_Opportunity_Trigger__c = false;       
        defConfig.Fire_RelatedAccount_Trigger__c = false;
        defConfig.Quote_Trigger__c = true;
        update defConfig;
        
        List<User> userList = Utility_Test.createTestUsers(false, 1, 'Commercial Sales User');
        userList[0].Business_Type__c = UtilityCls.COMMERCIAL;
        insert userList;
        
        User usr = userList[0];
        System.runAs(usr){
            accList = Utility_Test.createAccounts(false, 2);
            accList[0].Business_Type__c = UtilityCls.COMMERCIAL;
            accList[0].RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Non-Invoicing');
            accList[0].Role_C__c = 'End User';
            accList[0].Strategic_Account__c = true;
            accList[1].Business_Type__c = UtilityCls.COMMERCIAL;
            accList[1].Role_C__c = 'Dealer';
            accList[1].RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Invoicing');
            insert accList;
            
            List<Opportunity> oppList = Utility_Test.createOpportunities(true, 1, accList);
            for(Opportunity opp: oppList){
                opp.RecordTypeId = UtilityCls.getRecordTypeInfo('Opportunity', 'Commercial Traditional');
                if(opp.AccountId == accList[0].Id){
                    oppId = opp.Id;
                    opp.StageName = 'Discovery';
                    quote.Market_Segment__c = opp.Market_Segment__c = 'Healthcare';
                    quote.Job_Location__c = opp.Location__c = 'Atlanta';
                }
            }
            System.debug('OppId' + oppId);
            update oppList;
            
            quote.Dealer__c = accList[1].Id;
            quote.OpportunityId = oppId;
            quote.Strategic_Account__c = accList[0].Id;
            quote.Name = 'Test Quote for status change';
            quote.Status = 'Pending';
            insert quote;
            
            quote.Status = 'Sent';
            update quote;
        }
        userList = Utility_Test.createTestUsers(false, 2, 'Commercial Sales Operation');
        userList[0].Business_Type__c = UtilityCls.COMMERCIAL;
        userList[1].Business_Type__c = UtilityCls.COMMERCIAL;
        insert userList;
        
        User usrOp = userList[0];
        quote = new Quote();
        System.runAs(usrOp){
            AccountTriggerHandler.updatedFromRelAcc = true;
            accList = Utility_Test.createAccounts(false, 2);
            accList[0].Business_Type__c = UtilityCls.COMMERCIAL;
            accList[0].RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Non-Invoicing');
            accList[0].Role_C__c = 'End User';
            accList[0].Strategic_Account__c = true;
            accList[1].Business_Type__c = UtilityCls.COMMERCIAL;
            accList[1].Role_C__c = 'Dealer';
            accList[1].RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Invoicing');
            insert accList;
            
            Mohawk_Account_Team__c mhkteam = new Mohawk_Account_Team__c();
            mhkteam.Account__c = accList[0].Id;
            mhkteam.User__c = userList[1].Id;
            mhkteam.Role__c = 'Strategic Account Manager';
            insert mhkteam;

            List<Opportunity> oppList = Utility_Test.createOpportunities(true, 1, accList);
            for(Opportunity opp: oppList){
                opp.RecordTypeId = UtilityCls.getRecordTypeInfo('Opportunity', 'Commercial Traditional');
                if(opp.AccountId == accList[0].Id){
                    oppId = opp.Id;
                    opp.StageName = 'Discovery';
                    quote.Market_Segment__c = opp.Market_Segment__c = 'Healthcare';
                    quote.Job_Location__c = opp.Location__c = 'Atlanta';
                }
            }
            System.debug('OppId' + oppId);
            update oppList;
            
            quote.Dealer__c = accList[0].Id;
            quote.OpportunityId = oppId;
            quote.Strategic_Account__c = accList[0].Id;
            quote.Name = 'Test Quote for status change';
            quote.Status = 'Pending';
            insert quote;
            
            quote.Status = 'Sent';
            update quote;
        }
    }
}