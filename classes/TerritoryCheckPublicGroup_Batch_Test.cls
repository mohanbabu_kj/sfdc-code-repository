/**************************************************************************

Name : TerritoryCheckPublicGroup_Batch_Test
===========================================================================
Purpose : Uitlity class to TerritoryCheckPublicGroup_Batch  class 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         11/April/2017  Create  
1.1        Krishnapriya   7/Nov/2017     Modified        Added method test3
1.2		   Susmitha       26/March/2018  Modified
***************************************************************************/
@isTest
private class TerritoryCheckPublicGroup_Batch_Test {
    
    static List<Territory_User__c> territoryUserList;
    static String CRON_EXP = '0  00 1 3 * ?';
    static User resUser = Utility_Test.getTestUser('Residential', 'Residential Sales User');
    
    static void init(){
        Utility_Test.getTeamCreationCusSetting();
        Utility_Test.getDefaultConfCusSetting();
    }
    
    static testMethod void testMethod1() {
        Test.startTest();
        init();
        territoryUserList = Utility_Test.createTerritoryUsers(false, 1);
        
        for(Integer i=0;i<territoryUserList.size();i++){
            territoryUserList[i].Name = 'Testterr'+i;
            territoryUserList[i].RecordTypeId = Schema.SObjectType.Territory_User__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
        }
        insert territoryUserList;
        system.assert(territoryUserList.size()>0);
        String batchId = database.executeBatch(new TerritoryCheckPublicGroup_Batch());
        String jobId = System.schedule('Test my class', CRON_EXP, new TerritoryCheckPublicGroup_Batch());
        Test.stopTest();
    }
    
    static testMethod void testMethod2() {
        System.runAs(resUser) {        
            Test.startTest();
            init();
            Territory__c terr =  new Territory__c(Name = 'Test');
            insert terr;                
            system.assert(terr.id!=null);
            territoryUserList = new List<Territory_User__c>();
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = resUser.Id));
            insert territoryUserList;
          //  delete territoryUserList;

            String jobId = System.schedule('Test my class', CRON_EXP, new TerritoryCheckPublicGroup_Batch());
            
            Test.stopTest();
        }
    }
    
   static testMethod void testMethod3(){
        
        //Create Parent Group
        Group grp = new Group();
        grp.name = 'Test';
        grp.Type = 'Regular'; 
        Insert grp; 
        system.assert(grp.id!=null);
        
        Group grp0 = new Group();
        grp0.name = 'Test1';
        grp0.Type = 'Regular'; 
        Insert grp0; 
        system.assert(grp0.id!=null);
        
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        
        //Create Group Member
        GroupMember grpMem0 = new GroupMember();
        grpMem0.UserOrGroupId = UserInfo.getUserId();
        grpMem0.GroupId = grp0.Id;
        Insert grpMem0;
        //Create Parent Group
        Group grp1 = new Group();
        grp1.name = 'Test';
        grp1.Type = 'Regular'; 
        Insert grp1; 
        
        //Create Group Member
        GroupMember grpMem2 = new GroupMember();
        grpMem2.UserOrGroupId = UserInfo.getUserId();
        grpMem2.GroupId = grp1.Id;
        Insert grpMem2;
        System.runAs(resUser) {        
            Test.startTest();
            init();
            Territory__c terr =  new Territory__c(Name = 'Test');
            insert terr;
            system.assert(terr.id!=null);
            
            territoryUserList = new List<Territory_User__c>();
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = grpMem1.UserOrGroupId )); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = resUser.Id )); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = grpMem1.UserOrGroupId )); 

            insert territoryUserList;
            delete territoryUserList[0];

            String jobId = System.schedule('Test my class', CRON_EXP, new TerritoryCheckPublicGroup_Batch());
            
            Test.stopTest();
        }   
    }
    
    static testMethod void testMethod5(){
        
        //Create Parent Group
        Group grp = new Group();
        grp.name = 'Test';
        grp.Type = 'Regular'; 
        Insert grp; 
        system.assert(grp.id!=null);
        
        Group grp0 = new Group();
        grp0.name = 'Test1';
        grp0.Type = 'Regular'; 
        Insert grp0; 
        system.assert(grp0.id!=null);
        
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        
        //Create Group Member
        GroupMember grpMem0 = new GroupMember();
        grpMem0.UserOrGroupId = UserInfo.getUserId();
        grpMem0.GroupId = grp0.Id;
        Insert grpMem0;
        //Create Parent Group
        Group grp1 = new Group();
        grp1.name = 'Test';
        grp1.Type = 'Regular'; 
        Insert grp1; 
        
        //Create Group Member
        GroupMember grpMem2 = new GroupMember();
        grpMem2.UserOrGroupId = UserInfo.getUserId();
        grpMem2.GroupId = grp1.Id;
        Insert grpMem2;
        System.runAs(resUser) {        
            Test.startTest();
            init();
            Territory__c terr =  new Territory__c(Name = 'Test');
            insert terr;
            system.assert(terr.id!=null);
            
            territoryUserList = new List<Territory_User__c>();
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = grpMem1.UserOrGroupId )); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = resUser.Id )); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = grpMem0.UserOrGroupId )); 

            insert territoryUserList;
            delete territoryUserList[2];

            String jobId = System.schedule('Test my class', CRON_EXP, new TerritoryCheckPublicGroup_Batch());
            
            Test.stopTest();
        }   
    }
   static testMethod void testMethod6(){
        
        //Create Parent Group
        Group grp = new Group();
        grp.name = 'Test';
        grp.Type = 'Regular'; 
        Insert grp; 
        system.assert(grp.id!=null);
        
        Group grp0 = new Group();
        grp0.name = 'Test1';
        grp0.Type = 'Regular'; 
        Insert grp0; 
        system.assert(grp0.id!=null);
        
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        
        //Create Group Member
        GroupMember grpMem0 = new GroupMember();
        grpMem0.UserOrGroupId = UserInfo.getUserId();
        grpMem0.GroupId = grp0.Id;
        Insert grpMem0;
        //Create Parent Group
        Group grp1 = new Group();
        grp1.name = 'Test';
        grp1.Type = 'Regular'; 
        Insert grp1; 
        
        //Create Group Member
        GroupMember grpMem2 = new GroupMember();
        grpMem2.UserOrGroupId = UserInfo.getUserId();
        grpMem2.GroupId = grp1.Id;
        Insert grpMem2;
        System.runAs(resUser) {        
            Test.startTest();
            init();
            Territory__c terr =  new Territory__c(Name = 'Test');
            insert terr;
            system.assert(terr.id!=null);
            
            territoryUserList = new List<Territory_User__c>();
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = grpMem1.UserOrGroupId )); 
           // territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = resUser.Id )); 
           // territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = grpMem0.UserOrGroupId )); 

            insert territoryUserList;
            delete territoryUserList[0];

            String jobId = System.schedule('Test my class', CRON_EXP, new TerritoryCheckPublicGroup_Batch());
            
            Test.stopTest();
        }   
    }
    
    static testMethod void testMethod7(){
      
        //Create Parent Group
        Group grp = new Group();
        grp.name = 'Test';
        grp.Type = 'Regular'; 
        Insert grp; 
        system.assert(grp.id!=null);
        
        Group grp0 = new Group();
        grp0.name = 'Test1';
        grp0.Type = 'Regular'; 
        Insert grp0; 
        system.assert(grp0.id!=null);
        
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
       // Insert grpMem1;
        
        
        //Create Group Member
        GroupMember grpMem0 = new GroupMember();
        grpMem0.UserOrGroupId = UserInfo.getUserId();
        grpMem0.GroupId = grp0.Id;
       // Insert grpMem0;
        //Create Parent Group
        Group grp1 = new Group();
        grp1.name = 'Test';
        grp1.Type = 'Regular'; 
        Insert grp1; 
        
        //Create Group Member
        GroupMember grpMem2 = new GroupMember();
        grpMem2.UserOrGroupId = UserInfo.getUserId();
        grpMem2.GroupId = grp1.Id;
       // Insert grpMem2;
        System.runAs(resUser) {        
            Test.startTest();
            init();
            Territory__c terr =  new Territory__c(Name = 'Test',region__c='200 - Dalton SS House');
            insert terr;
            system.assert(terr.id!=null);
             
            territoryUserList = new List<Territory_User__c>();
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = resUser.Id)); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = resUser.Id )); 
            territoryUserList.add(new Territory_User__c(Territory__c = terr.Id, Role__c = 'Business Development Manager', User__c = resUser.Id)); 

            insert territoryUserList;
            delete territoryUserList[2];

            String jobId = System.schedule('Test my class', CRON_EXP, new TerritoryCheckPublicGroup_Batch());
            
            Test.stopTest();
        }   
    }
}