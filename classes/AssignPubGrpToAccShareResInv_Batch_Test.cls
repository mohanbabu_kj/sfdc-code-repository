/**************************************************************************

Name : AssignPubGrpToAccShareResInv_Batch_Test
===========================================================================
Purpose : Uitlity class to AssignPubGrpToAccShareResInv_Batch test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit          03/OCT/2017    Create  
2.0        Susmitha       02/March/2018  Modified

***************************************************************************/

@isTest
private class AssignPubGrpToAccShareResInv_Batch_Test { 
    
    @TestSetup
    public static void init(){
        Default_Configuration__c dc = Utility_Test.getDefaultConfCusSetting();
        Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        AP_Trigger_Settings__c APSC1 = new AP_Trigger_Settings__c();
        APSC1.Name = 'Chain Number Exclude';
        APSC1.Exclude_Chain_Numbers_1__c = '10';
        APSC1.Exclude_Chain_Numbers_2__c = '13';
        insert APSC1;    
    }
    
    static testMethod void testMethod1() {
        
        test.startTest(); 
      
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); // Added by MB - 07/10/17
            List<Account>resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            List<Territory__c>terrList = Utility_Test.createTerritories(true, 2);
            
            Id resiInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
            List<Mohawk_Account_Team__c>mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            mohAccTeam[0].Territory_User_Number__c = '05-J86'; 
            mohAccTeam[0].Account__c = resAccListForInvoicing[0].id;            
            insert mohAccTeam[0];
            
            AccountShare accShare = new AccountShare();
            accShare.AccountId = resAccListForInvoicing[0].Id;
            accShare.AccountAccessLevel = 'Edit';
            accShare.ContactAccessLevel = 'Edit';
            accShare.OpportunityAccessLevel = 'Edit';
            accShare.UserOrGroupId = Utility_Test.createGroup(true, 1, 'testGroup')[0].id;
            insert accShare;
            
            
            
            
            
            AssignPubGrpToAccShareResInv_Batch APG = new AssignPubGrpToAccShareResInv_Batch();
            string jobId = Database.executeBatch( APG,1 );
            System.assert(jobId != null);
        test.stopTest();
        
        
    }
}