@isTest
public class AssignTerritoryToAccounts_Batch_Test {
    static testMethod void testMethod1() {
        Utility_Test.getDefaultConfCusSetting();
        
        String com = 'Commercial';
        
        List<Account> parentAccs = Utility_Test.createAccounts(false, 3);
        //parentAccs[0].ShippingCountryCode = 'US';
        parentAccs[0].ShippingPostalCode = '30301';
        parentAccs[0].Business_Type__c=com;
        parentAccs[0].strategic_account__c=false;
        parentAccs[0].Type='Non-Invoicing';
        //parentAccs[1].ShippingCountryCode = 'CA';
        parentAccs[1].ShippingPostalCode = 'L5A 2X2';
        parentAccs[1].Business_Type__c=com;
        parentAccs[1].strategic_account__c=false;
        parentAccs[1].Type='Non-Invoicing';
        //parentAccs[2].ShippingCountryCode = parentAccs[2].BillingCountryCode = 'MX';
        parentAccs[2].Business_Type__c=com;
        parentAccs[2].strategic_account__c=false;
        parentAccs[2].Type='Non-Invoicing';
        Insert parentAccs;
        
        AssignTerritoryToAccounts_Batch rollupbatch = new AssignTerritoryToAccounts_Batch('Download');
        String jobId = Database.executeBatch(rollupbatch,200); 
        System.assert(jobId != null);
        
        List<Territory__c>  terrList = Utility_Test.createTerritories(false, 5);
        terrList[0].RecordTypeId = terrList[1].RecordTypeId = terrList[2].RecordTypeId = terrList[3].RecordTypeId = terrList[4].RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Commercial');
        terrList[0].Name ='Ed_Atlanta';
        terrList[0].Market_Segment_Group__c = 'Education/Govt';
        terrList[1].Name ='Hc_Atlanta';
        terrList[1].Market_Segment_Group__c = 'Healthcare/Sr Living';
        terrList[2].Name ='Wp_Atlanta';
        terrList[2].Market_Segment_Group__c = 'Workplace/Retail';
        terrList[3].Name ='Toronto1';
        terrList[3].Country__c ='Canada';
        terrList[4].Name = 'Mexico1';
        terrList[4].Country__c = 'Mexico';
        Insert terrList;
        
        List<Territory_Geo_Code__c> terrGeoCodeList = Utility_Test.createTerritoryGeoCodes(false, 4);
        terrGeoCodeList[0].Name = terrGeoCodeList[1].Name = terrGeoCodeList[2].Name = '30301';
        terrGeoCodeList[0].Territory__c = terrList[0].Id;
        terrGeoCodeList[1].Territory__c = terrList[1].Id;
        terrGeoCodeList[2].Territory__c = terrList[2].Id;
        terrGeoCodeList[3].Name = 'L5A';
        terrGeoCodeList[3].Territory__c = terrList[3].Id;
        
        List<Group> groupList = new List<Group>();
        Set<Group> groupSet = new Set<Group>();
        for(Territory__c terr: terrList){
            Group grp = new Group();
            grp.Name = grp.DeveloperName = terr.Name;
            groupSet.add(grp); 
        }
        groupList.addAll(groupSet);
        Insert groupList;
        
        System.assert(terrList!=null);
        System.assert(parentAccs!=null);
        
        parentAccs[0].ShippingCountryCode = 'US';
        parentAccs[1].ShippingCountryCode = 'CA';
        parentAccs[2].ShippingCountryCode = parentAccs[2].BillingCountryCode = 'MX';
        Update parentAccs;

        //test.startTEST();
        
        AssignTerritoryToAccounts_Batch rollupbatchUpd = new AssignTerritoryToAccounts_Batch('Update');
        String jobIdUpd = Database.executeBatch(rollupbatchUpd,200); 
        System.assert(jobIdUpd != null);

        //test.stoptest();
    }
}