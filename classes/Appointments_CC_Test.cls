/**************************************************************************

Name : Appointments_CC_Test

===========================================================================
Purpose : This tess class is used for Appointments_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         16/Feb/2017     Created 

***************************************************************************/
@isTest
private class Appointments_CC_Test { 
    static List<Event> events;
    static List<Account> accList;
    static List<Contact> contactList;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        accList =  Utility_Test.createAccounts(false, 1);
        
        for(Integer i=0;i<accList.size();i++){
            accList[i].Business_Type__c = 'Commercial';
            accList[i].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
        }
        insert accList;
        system.assert(accList!=null);
        contactList = Utility_Test.createContacts(false, 1, accList);
        
        
        //contactList = Utility_Test.createContacts(true, 1, accList);
        events = Utility_Test.createEvents(true, 1, accList);
    }
    
    static testMethod void testMethod1() {
        init();
        
        test.startTest();
        id eventId = events.get(0).Id;
        Appointments_CC.getContactFieldInfo();
        Appointments_CC.saveContactRecord((Contact)contactList.get(0),eventId);
        Appointments_CC.getObjectRecord('Event', eventId, 'Id' );
        Appointments_CC.updateAppoinment((Event)events.get(0), 'Test notes');
        List<ContentDocumentLink> notes = [SELECT id from ContentDocumentLink where LinkedEntityId =: eventId];
        system.assert(notes.size() != null);
        
        
        test.stopTest();
    }
    
    static testMethod void testMethod2() {
        init();
        
        test.startTest();
        // accList =  Utility_Test.createAccounts(false, 1);
        List<User> userList = Utility_Test.createTestUsers(true, 1, UtilityCls.COMM_SALES_USER);
        List<Territory__c> terrList = Utility_Test.createTerritories(true, 1);
        system.assert(terrList!=null);
        
        List<Mohawk_Account_Team__c> mhkTeam = Utility_Test.createMohawkAccountTeam(true, 1, accList, terrList,  userList[0]);
        Appointments_CC.validateRecordId(accList[0].Id);
        Appointments_CC.validateRecordId(events[0].Id);
        Appointments_CC.validateRecordId(mhkTeam[0].Id);
        
        // List<Event> eventList = Utility_Test.createEvents(true, 1,  accList)
        
        test.stopTest();
    }
    
    static testMethod void testMethod3() {
        test.startTest();
            init();
            Appointments_CC.getBusinessType();
            Appointments_CC.getAppointmentSAL( events[0].id );
        test.stopTest();
    }

    
}