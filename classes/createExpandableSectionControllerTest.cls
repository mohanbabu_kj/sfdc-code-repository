/**************************************************************************

Name : createExpandableSectionControllerTest

===========================================================================
Purpose : This tess class is used for createExpandableSectionController
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Radhika       7/June/2018     Created         
***************************************************************************/
@isTest
public class createExpandableSectionControllerTest
{
    public static Account_Profile__c apRec;
    public static Account accRec;
    public static Mohawk_Account_Team__c matRec;
    
    static void dataSetUp()
    {
        apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        insert apRec;
        
        accRec = new Account();
        accRec.name = 'test Account';
        accRec.Account_Profile__c = apRec.id;
        insert accRec;
        
        matRec = new Mohawk_Account_Team__c();
        matRec.account__c=accRec.id;
        matRec.Account_Profile__c = apRec.id;
        insert matRec;
        
    }
    static testMethod void getAccountProfileDetailTest()
    {
        dataSetUp();
        createExpandableSectionController.getAccountProfileDetail(apRec.id);
        createExpandableSectionController.showMatScreenOnAP( accRec.Id );
        createExpandableSectionController.showMatScreenOnAP( matRec.Id );
        
    }
    
    static testMethod void getMohawkAccountTeamTest()
    {
        dataSetUp();
        createExpandableSectionController.getMohawkAccountTeam(matRec.id);
        
    } 
    
    static testMethod void createExpandableSectionsTest()
    {
        dataSetUp();
        createExpandableSectionController.createExpandableSections('Retail','DESKTOP',apRec.id);
        
    }
    
}