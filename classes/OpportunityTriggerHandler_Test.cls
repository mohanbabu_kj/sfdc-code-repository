/**************************************************************************

Name : OpportunityTriggerHandler_Test 

===========================================================================
Purpose : This class is used for Opportunity Trigger actions
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         6/Feb/2017     Created 
2.0        Lester         2/May/2017     Modify 
3.0        Susmitha       9/Nov/2017     Modify
***************************************************************************/
@isTest
private class OpportunityTriggerHandler_Test { 
    static List<Opportunity> oppList;
    static List<Account> accForInvoicingList;
    static List<Account> accForNonInvoicingList; 
    static List<Partner_Account__c> parAccountList;
    
    @TestSetup
    public static void init(){
        Default_Configuration__c dc = Utility_Test.getDefaultConfCusSetting();
    }
    
    
    static testmethod void testMethod1() {
        test.startTest();
        List<Account> acForInvoicingList =  Utility_Test.createAccountsForInvoicing(false, 1);
        acForInvoicingList[0].Business_type__c='Commercial';
        acForInvoicingList[0].Total_Pipeline__c=20;
        insert acForInvoicingList;
        
        List<Account> acForNonInvoicingList = Utility_Test.createAccountsForNonInvoicing(false, 1);
        acForNonInvoicingList[0].Business_type__c='Commercial';
        acForNonInvoicingList[0].Total_Pipeline__c=20;
        insert acForNonInvoicingList;
        system.assert(acForNonInvoicingList.size()>0);     
        parAccountList = new List<Partner_Account__c>{
            new Partner_Account__c(
                Invoicing_Account__c = acForInvoicingList[0].Id ,
                Non_Invoicing_Account__c = acForNonInvoicingList[0].Id, 
                Role__c = 'AD',
                RecordTypeId = UtilityCls.getRecordTypeInfo('Partner_Account__c', 'Residential')
            )
                };
                    insert parAccountList ;
        system.assert(parAccountList.size()>0);     
        
        //Covering Before Insert
        oppList = Utility_Test.createOpportunitiesCommTrade(false, 2);
        oppList[0].AccountId = acForNonInvoicingList[0].id;
        oppList[0].Primary_End_User__c = acForNonInvoicingList[0].Id;
        oppList[0].General_Contractor__c = acForNonInvoicingList[0].Id;
        oppList[0].status__c = null;
        oppList[0].Amount = 2;
        
        oppList[1].AccountId = acForInvoicingList[0].id;
        oppList[1].Primary_End_User__c = acForNonInvoicingList[0].Id;
        oppList[1].General_Contractor__c = acForNonInvoicingList[0].Id;
        oppList[1].status__c = 'In Process';
        
        oppList[1].stageName = 'Setup' ;

        oppList[1].Amount = 2;
        insert oppList;
        
        oppList[0].Amount = 3;
        oppList[1].Amount = 4;
        oppList[0].stageName = UtilityCls.PROJECT_STAGE_CLOSED_WON ;
        
        oppList[0].status__c = UtilityCls.PROJECT_STATUS_CLOSED;
        oppList[1].stageName = UtilityCls.PROJECT_STAGE_CLOSED_WON ;

        oppList[1].status__c = UtilityCls.PROJECT_STATUS_CLOSED;

        update oppList;        
        
        test.stopTest();
    }
}