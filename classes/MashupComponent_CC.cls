/**************************************************************************

Name : MashupComponent_CC 

===========================================================================
Purpose : to fetch Hybris & Tableau URLs from "Custom Settings" - "Mashup_Management__c"
===========================================================================

METHODS IN USE:
- getRenderObjectData()
- validateProfileName(String str)
- getMashupRecord()
- getFinalUrl(Mashup_Management__c data)
- getRecordsByGroup()
- buildQueryStringForUrl(Mashup_Management__c data, String finalUrl)

***************************************************************************/
public with sharing class MashupComponent_CC {

    public String objectName {get; set;}
    // public SObject objectVar {get; set;}
    public String objectId {get; set;}
    public String recordType {get; set;}
    public Integer recordsCount {get; set;}
    // public Map<String,Mashup_Management__c> recordGroupMap {get; set;}

    public SObject objectData {get; set;}// 04/19/2017
    public String mashupName {get; set;}// 04/19/2017
    public String mashupUrl {get; set;}// 04/19/2017

    public Boolean isCommercial;
    public Boolean isResidential;
    public String userDeviceInfo {get; set;}
    public Boolean isDevice {get; set;}
    public Boolean isDesktop {get; set;}

    public Map<String,String> paramList = new Map<String,String>();

    public Map<String,String> groupCleanStr {get; set;}
    public Boolean displayList {get; set;}

    public String currentRecordType {get; set;}

    public MashupComponent_CC() {
        isCommercial = UtilityCls.validateProfileByName(UtilityCls.COMMERCIAL);
        isResidential = UtilityCls.validateProfileByName(UtilityCls.RESIDENTIAL);
        userDeviceInfo = UserInfo.getUiTheme();

        isDevice = (userDeviceInfo == 'Theme4t' ? true : false);
        isDesktop = (userDeviceInfo != 'Theme4t' ? true : false);

        recordsCount = 0;

        System.debug('objectId' + objectId);

        currentRecordType = 'All';

        // System.debug('paramList' + paramList);
    }

    // 04/19/2017 start
    public String getRenderObjectData() {
        System.debug('objectId--' + objectId);
        Boolean isRecordTypeAvail = false;
        if(UtilityCls.isStrNotNull(objectId)){
            String qryObject = objectName;
            if(objectName =='Product')
                qryObject =  'Product2';
            else if(objectName =='Project')
                qryObject =  'Opportunity';
            else if(objectName.contains('Order')) // fix 05/04/2017 (objectName == 'Sample Orders' || objectName == 'Product Orders')
                qryObject =  'Order';

            //Added by Anand to fix too many fields error in dynamic soql(8/20/2017)
            String fieldList;
            if(objectName == 'Account_Profile__c'){ //(8/20/2017)
                fieldList = 'id, Name, Chain_Number__c';//(8/20/2017)
            }else{//(8/20/2017)
                fieldList= UtilityCls.getObjectFieldsForSql(qryObject);
            }
            

            isRecordTypeAvail = MashupUtility_CC.isRecordTypeAvailOnObject(qryObject);
            if(isRecordTypeAvail){
            //if(qryObject == 'Account' || qryObject == 'Contact' || qryObject == 'Product2' || qryObject == 'Opportunity' || qryObject == 'Order'){
                // isRecordTypeAvail = true;
                fieldList += ', RecordType.DeveloperName';
            }

            String soql = 'SELECT ' + fieldList + ' FROM ' + qryObject + ' WHERE Id =: objectId';
            System.debug(soql);
            objectData = Database.query(soql);
        }

        paramList.put('recordId', objectId);
        
        // 05/03/2017
        displayList = true;
        if(objectData!=null){
            currentRecordType = (isRecordTypeAvail?(String)objectData.getSObject('RecordType').get('DeveloperName'):'All');
            if(objectName == 'Account') {
                // Bug: 40551 to hide hybris items on non-invoice, residential profile
                if(recordType == 'Hybris' && objectData.getSObject('RecordType').get('DeveloperName') == 'Non_Invoicing' && isResidential){
                    displayList = false;
                }
            }
        }

        return 'object data generated';
    }

    public Boolean getMashupRecord() {
        Boolean recFound = false;
        String finalUrl = '';

        Mashup_Management__c mashup = Mashup_Management__c.getInstance(mashupName);
        System.debug(mashup);
        if(mashup!=null ){// && UtilityCls.isStrNotNull(mashup.Name)
            finalUrl = getFinalUrl(mashup);
            if(UtilityCls.isStrNotNull(finalUrl)){
                mashup.Target_URL__c = finalUrl;
                mashupUrl = finalUrl;
                recFound = true;
            }
        }

        return recFound;
    }

    private String getFinalUrl(Mashup_Management__c data) {
        String finalUrl = '';

        if(data.Profile__c == 'All')
            finalUrl = data.Target_URL__c;
        else if(isCommercial && data.Profile__c == UtilityCls.COMMERCIAL)
            finalUrl = data.Target_URL__c;
        else if(isResidential && data.Profile__c == UtilityCls.RESIDENTIAL)
            finalUrl = data.Target_URL__c;

        // to handle null value on non commercial/residential profiles and display commercial record 04/18/2017
        if(UtilityCls.isStrNotNull(finalUrl) == false && !isResidential && !isCommercial)// && data.Profile__c == UtilityCls.COMMERCIAL
            finalUrl = data.Target_URL__c;

        if(UtilityCls.isStrNotNull(finalUrl)){
            finalUrl = MashupUtility_CC.buildQueryStringForMashupUrl(data, finalUrl, objectData, paramList);
        }

        return finalUrl;
    }

    // 04/19/2017 end

    public Map<String, Map<String,list<Mashup_Management__c>>> getRecordsByGroup() {
        recordsCount = 0;

        String groupName = '';
        String typeName = '';
        String finalUrl = '';        

        groupCleanStr = new Map<String,String>();

        Map<String, Map<String,list<Mashup_Management__c>>> groupMap = new Map<String, Map<String,list<Mashup_Management__c>>>();

        List<Mashup_Management__c> mashupData = [SELECT Id, Display_Name__c,Group__c,Icon__c,Icon_BG_Color__c,Is_Active__c,Is_Publisher_Action__c,
                                       No_Query_String__c,Object__c,Object_Record_Type__c,Profile__c,Query_String__c,Sort_Order__c,Target_URL__c,Type__c
                                       FROM Mashup_Management__c ORDER BY Sort_Order__c ASC NULLS Last];
        
       // for(Mashup_Management__c data :Mashup_Management__c.getAll().values()){
        for(Mashup_Management__c data : mashupData){
            System.debug('data-mashup' + data);
			System.debug('Mashup Data: ' + data);
            typeName = data.Type__c;
            finalUrl = '';

            Set<String> objectList = new Set<String>();
            /*if(data.Object__c.contains(','))
                objectList.addAll(data.Object__c.split(','));
            else*/
                objectList.add(data.Object__c);
            if(typeName == recordType && objectList.contains(objectName) && data.Is_Active__c == true && data.Is_Publisher_Action__c == false
                && (data.Object_Record_Type__c == 'All' || data.Object_Record_Type__c == currentRecordType)
                ){
                // System.debug(data);

                finalUrl = getFinalUrl(data);
                 System.debug('finalUrl' + finalUrl);
                if(UtilityCls.isStrNotNull(finalUrl)){
                    // System.debug('finalUrl-in' + finalUrl);
                    data.Target_URL__c = finalUrl;
                    recordsCount++;

                    groupName = data.Group__c;
                    if(UtilityCls.isStrNotNull(groupName)==false)
                        groupName = 'General';
                    groupCleanStr.put(groupName, groupName.replaceAll('[0-9]{1,}|\\.', '').trim());// 05/03/2017
                    // groupName = groupName.replaceAll('[0-9]{1,}|\\.', '');

                    if(!groupMap.containsKey(typeName))
                        groupMap.put(typeName, new Map<String,list<Mashup_Management__c>>());

                    if(groupMap.get(typeName).containsKey(groupName)){
                        groupMap.get(typeName).get(groupName).add(data);
                    }else{
                        list<Mashup_Management__c> mmList = new list<Mashup_Management__c>();
                        mmList.add(data);
                        groupMap.get(typeName).put(groupName, mmList);
                    }
                    
                    //added by mb 10-12-2017
                    //displayList = true;
                }

            }
        }

        System.debug(groupMap); // Added by SB 10-14-17
        return groupMap;

    }

    /* moved to UtilityCls
    private boolean validateProfileName(String str){
        String profileName = [SELECT Name from Profile where id =: UserInfo.getprofileId()].Name;
        if(profileName.contains(str)){
        	return true;
        }
        return false;
    }*/

    // remove to UtilityCls
    /*
    public String buildQueryStringForUrl(Mashup_Management__c data, String finalUrl) {
        String newUrl = finalUrl;
        String queryString = data.Query_String__c;
        String newValue = '';

        if(UtilityCls.isStrNotNull(finalUrl) == false)
            return '';

        if(UtilityCls.isStrNotNull(queryString)){
            if(data.No_Query_String__c == true){
                System.debug('queryString initial ' + queryString);
                String urlTemp = newUrl;
                List<String> qryStrArr = queryString.split(';;');
                for(String str : qryStrArr) {
                    // System.debug('qry str:' + str);
                    if(UtilityCls.isStrNotNull(str)){
                        List<String> valueArr = str.split('=');
                        // System.debug('valueArr:' + valueArr);
                        // System.debug('paramList:' + paramList);
                        if(UtilityCls.isStrNotNull(valueArr[0]) && urlTemp.Contains(valueArr[0])){
                            newValue = (valueArr[1]!='RECORD_ID' && valueArr[1]!='recordId' ? (String)objectData.get(valueArr[1]) : paramList.get(valueArr[1])); 
                            newValue = (UtilityCls.isStrNotNull(newValue) ? newValue : 'NO_'+valueArr[0]);
                            urlTemp = urlTemp.replace(valueArr[0], newValue);
                        }
                    }
                }
                newUrl = urlTemp;
            } else {
                List<String> qryStrArr = queryString.split('&');
                for(String str : qryStrArr) {
                    if(UtilityCls.isStrNotNull(str)) {
                        List<String> valueArr = str.split('=');
                        if(UtilityCls.isStrNotNull(valueArr[0]) && UtilityCls.isStrNotNull(valueArr[1])){
                            if(UtilityCls.hasSObjectField(valueArr[1], objectData)){
                                queryString = queryString.replace(str, valueArr[0] + '=' + (String)objectData.get(valueArr[1]));
                            }
                        }
                    }
                }
                if(newUrl.contains(queryString)==false){
                    if(newUrl.Contains('?'))
                        newUrl += '&' + queryString;
                    else
                        newUrl += '?' + queryString;
                }
            }
        }
        System.debug('build url:' + data.Display_Name__c + ' = ' + newUrl);

        return newUrl;

    }

    */
}