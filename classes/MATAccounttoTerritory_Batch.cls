/**************************************************************************
Name : MATAccounttoTerritory_Batch 
===========================================================================
Purpose :           
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Nagendra       29/01/2019         Created          

***************************************************************************/
global class MATAccounttoTerritory_Batch implements Database.Batchable<sObject>,Schedulable{
    global Database.QueryLocator start(Database.BatchableContext bc) {
         String query;
         List<MAT_Batch_Setting__c> batchSettingList = new List<MAT_Batch_Setting__c>( [ SELECT id,Batch_Query__c,Last_Run_At__c,Batch_Size__c,MinutesDelay__c FROM MAT_Batch_Setting__c WHERE NAME = 'MATAccounttoTerritory_batch' LIMIT 1 ] );
         DateTime MATCreatedDate =System.today();
        if(batchSettingList.size()>0){
            if(  batchSettingList[0].Last_Run_At__c != null ){
              MATCreatedDate = batchSettingList[0].Last_Run_At__c;
                system.debug('lastRunTime'+MATCreatedDate);
              query = batchSettingList[0].Batch_Query__c  +' where  CreatedDate < : MATCreatedDate';
            }
        }else{             
            query = 'Select Id,Territory_User_Number__c,Account__c,CreatedDate from Mohawk_Account_Team__c where CreatedDate < : MATCreatedDate ';
        }
        
        return Database.getQueryLocator(query);
        
    }
    global void execute(Database.BatchableContext BC, list<Mohawk_Account_Team__c> scope) {
       
        MATtoTerritoryAccountService.createTerritoryAccounts(scope);
    }
    global void finish(Database.BatchableContext BC){
        
    }
    global void execute(SchedulableContext SC){
        database.executeBatch(new MATAccounttoTerritory_batch());
    }
}