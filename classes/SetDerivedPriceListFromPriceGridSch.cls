/*

    for sobject a:
    SetDerivedPriceListFromPriceGridSch  lsu = new SetDerivedPriceListFromPriceGridSch('a');
    String cron30 = '0 20 * * * ?'; // for every 20 minutes
    String jobID = system.schedule('A object update Job', cron30, lsu);

*/
global class SetDerivedPriceListFromPriceGridSch implements Schedulable{
    global final String sObjectType;
    
    global SetDerivedPriceListFromPriceGridSch(String s){
        sObjectType = s;
    }
    
    global void execute(SchedulableContext sc) {
        Map<String,CPL_Update_Batch_Setting__c> cplUpdateSettingMap = CPL_Update_Batch_Setting__c.getAll();
        final Integer batchSize = Integer.valueOf(cplUpdateSettingMap.get(sObjectType).Price_Grid_Batch_Size__c );
        final String sObjectType = String.valueOf(cplUpdateSettingMap.get(sObjectType).sObject_Type__c);
        final String productCategory = String.valueOf(cplUpdateSettingMap.get(sObjectType).Product_Category__c); 
        SetDerivedPriceListFromPriceGrid cplBatch = new SetDerivedPriceListFromPriceGrid(sObjectType, productCategory); 
        database.executebatch(cplBatch, batchSize);
    }
}