@isTest(SeeAllData=true)
public class ApprovalRequest_CC_Test {
    static testmethod void testmethod1(){        
        System.runAs( Utility_Test.ADMIN_USER ){
        
            Test.startTest();       
            user testUser = utility_test.getTestUser('TestNewUserCreated','Commercial Sales Manager');
            insert testUser;
            
            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'Strategic_Account_Management'];
            insert new PermissionSetAssignment( AssigneeId = testUser.id, PermissionSetId = ps.Id );
            
            
            Claim__c claimObj = new Claim__c(Name='test', Status__c = 'INPROCESS',Claim_Submit_Date__c=system.today());
            insert claimObj;

            Claim_Approvers__c claimAppObj = new Claim_Approvers__c(Name= 'test', Approver__c=testUser.id, Claim__c = claimObj.Id);
            insert claimAppObj;
            
            
            system.runAs(testUser){
                List<Account>accList= Utility_Test.createAccountsForNonInvoicing(true, 1);           
                for(Integer i=0;i<accList.size();i++){
                    accList[i].Business_Type__c = 'Residential';
                    accList[i].Status__c = 'Active';
                    accList[i].Approval_Status__c = 'None';
                    accList[i].Strategic_Account__c = false;
                }
                update accList;
                
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setObjectId( accList[0].id );
                req1.setSubmitterId( testUser.id );
                req1.setComments('Submitting request for approval.');
                req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                //req1.setProcessDefinitionNameOrId('Non Invoicing Residential Acc Approval');
                //req1.setSkipEntryCriteria(false);
                Approval.ProcessResult result = Approval.process(req1);
                List<ApprovalRequest_CC.wrapApprovalRequest>wrapperList = ApprovalRequest_CC.getApprovalWorkitems();
                System.assert(wrapperList.size() > 0);
            }
            
            Test.stopTest();

        }
    }

}