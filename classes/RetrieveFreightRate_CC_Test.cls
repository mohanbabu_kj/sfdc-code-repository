/**************************************************************************

Name : RetrieveFreightRate_CC_Test 

===========================================================================
Purpose :   This class is used for RetrieveFreightRate_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        14/Feb/2017     Created 
***************************************************************************/
@isTest
private class RetrieveFreightRate_CC_Test { 
    static List<Account> accList;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        accList =  Utility_Test.createAccounts(FALSE, 1);
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.102318.0000';
        }
        
        INSERT accList;
    }
    
    static testMethod void testSendRequest() {
        init();	
        test.startTest();
        //update Global_Account_Number__c on account 
        
        
        
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new FreightRateCalloutMockNew_Test());
         RetrieveFreightRate_CC.handleFreightRate(accList[0].id);
        System.assertNotEquals(RetrieveFreightRate_CC.handleFreightRate(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithWrongGlobalAccountNumber() {
        init(); 
        //update Global_Account_Number__c on account 
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.1023';
        }
        
        UPDATE accList;
        
        test.startTest();
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new FreightRateCalloutMockNew_Test());
        System.assertNotEquals(RetrieveFreightRate_CC.handleFreightRate(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithException() {
        init();	
        //update Global_Account_Number__c on account 
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.1023';
        }
        
        UPDATE accList;
        
        test.startTest();
        system.assert(accList!=null);
        
        System.assertNotEquals(RetrieveFreightRate_CC.handleFreightRate(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithOutGlobalAccountNumber() {
        init();	        
        
        test.startTest();
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new FreightRateCalloutMockNew_Test());
        System.assertNotEquals(RetrieveFreightRate_CC.handleFreightRate(accList[0].id), null);
        RetrieveFreightRate_CC rfr = new  RetrieveFreightRate_CC();
        RetrieveFreightRate_CC.OFreightDataList childCls=new RetrieveFreightRate_CC.OFreightDataList('Test1','Test2','Test3','Test4','Test5','Test6');
        test.stopTest();
    }
    
    static testMethod void testWithJSONError() {
        init();         
        
        test.startTest();
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new FreightRateCalloutMockJSONError_Test());
        System.assertNotEquals(RetrieveFreightRate_CC.handleFreightRate(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithIdNull() {
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new FreightRateCalloutMock_Test());
        System.assertNotEquals(RetrieveFreightRate_CC.handleFreightRate(null), null);
        test.stopTest();
    }
    
    static testMethod void testWithDataEmpty() {
        init(); 
        //update Global_Account_Number__c on account 
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.102318.0000';
        }
        
        UPDATE accList;
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new FreightRateCalloutMockDataEmpty_Test());
        System.assertNotEquals(RetrieveFreightRate_CC.handleFreightRate(accList[0].id), null);
        test.stopTest();
    }
}