public class OpportunityLineItemTriggerHandler {
   	// Start of change SB 9-28-2017
    public static void updateLineItemNumber(List<OpportunityLineItem> oppList, Boolean updateFlag,Boolean insertFlag){
        User usr = [Select CAMS_Employee_Number__c from User where Id  =: UserInfo.getUserId()]; // Added by SB 10-18-2017
       
        //Set<Id> allOppIds = new Set<Id>();
        // Start of Code - MB - 10/03/17
        Set<String> LDSProdIds = new Set<String>(); 
        Set<Id> ProdIds = new Set<Id>(); 
        Map<String, Id> SFDCProdMap = new Map<String, Id>(); 
        List<PriceBookEntry> pbEntryList = new List<PriceBookEntry>();
        //End of Code - MB - 10/03/17
        //get all the Opportunity records for OpportunityLineItems oli are added
        for(OpportunityLineItem oli : oppList) {
            //allOppIds.add(oli.OpportunityId);
            if(oli.Updated_By_ESB__c && oli.LDS_PB_Mapping_ID__c != null)
            	LDSProdIds.add(oli.LDS_PB_Mapping_ID__c);// Added by MB - 10/03/17
        }
               
        //Start of Code - MB - 10/03/17
        Id pbId = [SELECT Id FROM PriceBook2 Where Name = :UtilityCls.Std_Price_Book].Id;
        IF(LDSProdIds.size()>0){
            for(Product2 prd: [SELECT Id, CAMS_Product_Number__c, CAMS_Alt_Product_Number_2__c, CAMS_Alt_Product_Number_3__c, CAMS_Alt_Product_Number_4__c
                               FROM Product2
                               WHERE CAMS_Product_Number__c    IN :LDSProdIds 
                               OR CAMS_Alt_Product_Number_2__c IN :LDSProdIds
                               OR CAMS_Alt_Product_Number_3__c IN :LDSProdIds
                               OR CAMS_Alt_Product_Number_4__c IN :LDSProdIds]){
                                   ProdIds.add(prd.Id);
                                   if(prd.CAMS_Product_Number__c != null){
                                       SFDCProdMap.put(prd.CAMS_Product_Number__c, prd.Id);
                                   }
                                   if(prd.CAMS_Alt_Product_Number_2__c != null){
                                       SFDCProdMap.put(prd.CAMS_Alt_Product_Number_2__c, prd.Id);
                                   }
                                   if(prd.CAMS_Alt_Product_Number_3__c != null){
                                       SFDCProdMap.put(prd.CAMS_Alt_Product_Number_3__c, prd.Id);
                                   }
                                   if(prd.CAMS_Alt_Product_Number_4__c != null){
                                       SFDCProdMap.put(prd.CAMS_Alt_Product_Number_4__c, prd.Id);
                                   }
                               }   
            if(pbId != null && ProdIds.size()>0){
                pbEntryList = [SELECT Id, Name, PriceBook2Id, Product2Id, CurrencyIsoCode
                                                    FROM PriceBookEntry
                                                    WHERE Product2Id IN :ProdIds
                                                    AND PriceBook2Id = :pbId];
            }
        }
        //End of Code - MB - 10/03/17
        for(OpportunityLineItem oli : oppList) {         
            oli.CAMS_Employee_ID_of_logged_in_User__c = usr.CAMS_Employee_Number__c; // Added by SB 10-18-2017
            //Start of Code - MB - 10/03/17
            if(oli.Updated_By_ESB__c && oli.LDS_PB_Mapping_ID__c != null){
                Id prdId = SFDCProdMap.get(oli.LDS_PB_Mapping_ID__c);
                if(prdId != null){
                    for(PriceBookEntry pbEntry: pbEntryList){
                        if(pbEntry.Product2Id == prdId && pbEntry.CurrencyIsoCode == oli.LDS_Currency_Code__c){
                            oli.PricebookEntryId = pbEntry.Id;
                            break;
                        }
                    }
                }
            }
            //End of Code - MB - 10/03/17
        }   
    }
    // End of change SB 9-28-2017
     
    
    //Start of Code - MB - 2/26/18 - Store deleted opp. line items in custom object to send to HANA
    public static void insertOppLineItem(List<OpportunityLineItem> delOppLineItemList){
        List<Opportunity_Line_Item__c> addOppLineItemList = new List<Opportunity_Line_Item__c>();
        for(OpportunityLineItem oppLineItem: delOppLineItemList){
            Opportunity_Line_Item__c addOppLineItem = new Opportunity_Line_Item__c();
            addOppLineItem.Opportunity__c = oppLineItem.OpportunityId;
            addOppLineItem.Opportunity_Line_Item_Id__c = oppLineItem.Id;
            addOppLineItem.Product__c = oppLineItem.Product2Id;
            addOppLineItem.Quantity__c = oppLineItem.Quantity;
            addOppLineItem.Price__c = oppLineItem.UnitPrice;
            addOppLineItemList.add(addOppLineItem);
        }
        if(!addOppLineItemList.isEmpty()){
            try{
                List<Database.SaveResult> results = Database.insert(addOppLineItemList, false);
                for (Database.SaveResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Opp Line Item: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while inserting Opp Line Item ' + err.getMessage());
                        }
                    }
                } 
            }Catch(Exception dmle){
                System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
            }
        }
    }
    
    //End of Code - MB - 2/26/18
    /*  public static void UpdateAccountTotalPipeline(List<OpportunityLineItem> newList,List<OpportunityLineItem> oldList,Map<Id,OpportunityLineItem> oldmapList,Boolean iflag,Boolean uflag,Boolean dflag) {
        List<OpportunityLineItem> oppLineItemslist = new List<OpportunityLineItem>();
        List<Related_Account__c> ralist = new  List<Related_Account__c>();
        List<Related_Account__c> updrelacc = new  List<Related_Account__c>();
        List<Account> laccount = new List<Account>();
        Set<Id> oppIds = new Set<Id>();
        
        //Get the list based on DML operation
        if(!dflag){
            oppLineItemslist = newList;
        } else{
            oppLineItemslist = oldList;
        }
        
        //Get all opportunity Ids
        for(OpportunityLineItem oppLineItem: oppLineItemslist){
            //oppId = oppLineItem.OpportunityId;
            if(!oppIds.contains(oppLineItem.OpportunityId))
                oppIds.add(oppLineItem.OpportunityId);
        }
        
        //Select all opportunities
        List<Opportunity> oppList = [SELECT Id, Name, Calculated_Revenue__c from Opportunity where Id IN :oppIds];
        //Get all related accounts of the opportunities and calulcate total pipeline.
        if(oppIds.size()>0){
            for(Opportunity opp: oppList){
                for(List<Related_Account__c> relAccList : [SELECT Id, Project_Old_Calculated_Revenue__c,Opportunity__c,Opportunity__r.Calculated_Revenue__c,Account__c,Account__r.Total_Pipeline__c 
                                                           FROM Related_Account__c 
                                                           where Opportunity__c = :opp.Id Limit 50000]){
                                                               for(Related_Account__c ra: relAccList){
                                                                   Decimal TotalPipeline = 0;
                                                                   Account acc = new Account();
                                                                   acc.Id = ra.Account__c;
                                                                   if(ra.Account__r.Total_Pipeline__c == null){
                                                                       TotalPipeline = 0;
                                                                   } else {
                                                                       TotalPipeline = ra.Account__r.Total_Pipeline__c;
                                                                   }
                                                                   acc.Total_Pipeline__c = TotalPipeline+ra.Opportunity__r.Calculated_Revenue__c-ra.Project_Old_Calculated_Revenue__c;
                                                                   laccount.add(acc);  
                                                                   system.debug('OppLineItemTrigger: ' + acc.Total_Pipeline__c);
                                                                   //ra.Project_Old_Calculated_Revenue__c = opp.Calculated_Revenue__c;
                                                                   //updrelacc.add(ra);
                                                               } 
                                                           }
            }
        }
        //update TotalPipeline in Accounts
        if(laccount.size()>0){
            try{
                Database.SaveResult[] results = Database.update(laccount, false);
                for(Integer index = 0; index < results.size(); index++) {
                    if(!results[index].isSuccess()){
                        laccount[index].addError('Failed to update: ' +results[index].getErrors()[0].getMessage());
                    }
                }
            }
            catch (System.DmlException e){
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    System.debug(e.getDmlMessage(i));
                    
                }
            }
        }
        //update Old Calculated Revenue
        system.debug(updrelacc);
        if(updrelacc.size()>0){
            try{
                Database.SaveResult[] results = Database.update(updrelacc, false);
                for(Integer index = 0; index < results.size(); index++) {
                    if(!results[index].isSuccess()){
                        updrelacc[index].addError('Failed to update: ' +results[index].getErrors()[0].getMessage());
                    }
                }
            }
            catch (System.DmlException e){
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    System.debug(e.getDmlMessage(i));
                    
                }
            }
        }
    } */
    
	

}