/**************************************************************************

Name : LeadTriggerHandler 

===========================================================================
Purpose : This class is used for Lead Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand          22/Dec/2016    Created
2.0        Anand          16/Feb/2017    Updated
Mudit          06/Oct/2017    Updated        Create new method name : leadAssignMentForSFI.
***************************************************************************/
public  class LeadTriggerHandler {
    
    public static Id newCommRTId; 
    public static Id existingComm;
    public static Id mhkGroupRTID;
    public static Boolean isTriggerFired;
    
    static{
        newCommRTId    = UtilityCls.getRecordTypeInfo('Lead','New Commercial');
        existingComm   = UtilityCls.getRecordTypeInfo('Lead','Existing Account Commercial');
        mhkGroupRTID   = UtilityCls.getRecordTypeInfo('Lead','MHK Group');
        isTriggerFired = false;
    } 
    
    //Method to identify whether the lead is commercial lead or residential by using record typeId
    @TestVisible
     public static Boolean isCommercialLead(Id leadRTId){      
        return (newCommRTId == leadRTID ||  existingComm == leadRTID || mhkGroupRTID == leadRTID);
    }
    
    /*
* Method to return Map with zip code and territoryid 
* key   : Lead zip code
* value : territoyid
*/
    //public static Map<String, String> zipCodeMap(list<Lead> leadList) { - Commented by MB - 06/20/17 - Bug 43495
    public static Map<String, List<Territory_Geo_Code__c>> zipCodeMap(list<Lead> leadList) { // Added by MB - 06/20/17 - Bug 43495
        Set<String> leadPostalCodeList = new Set<String>();
        //Map<String, String> postalCodeWithTerritoryMap = new Map<String, String>() ; - Commented by MB - 06/20/17 - Bug 43495
        Map<String, List<Territory_Geo_Code__c>> postalCodeWithTerritoryMap = new Map<String, List<Territory_Geo_Code__c>>(); // Added by MB - 06/20/17 - Bug 43495
        
        for(Lead tempLd : leadList){
            String zipcode = UtilityCls.isStrNotNull(tempLd.Architect_Zip_Code__c) ? tempLd.Architect_Zip_Code__c : tempLd.PostalCode;
            system.debug('***zipcode**'+zipcode);
            leadPostalCodeList.add(zipcode);
        }
        
        // Assumptions: Name is unique in Territory_Geo_Code__c
        List<Territory_Geo_Code__c> tGeoCodeList = [SELECT Id, 
                                                    Name,
                                                    Territory__c,
                                                    Business_Type__c // Added by MB - 06/20/17 - Bug 43495
                                                    FROM Territory_Geo_Code__c 
                                                    WHERE Name in : leadPostalCodeList];
        
        for(Territory_Geo_Code__c tGeoCode : tGeoCodeList) {
            //postalCodeWithTerritoryMap.put(tGeoCode.Name,tGeoCode.Territory__c); - Commented By MB - 06/20/17 - Bug 43495
            // Start of Code - MB - 06/20/17 - Bug 43495
            if(postalCodewithTerritoryMap.containsKey(tGeoCode.Name)){
                postalCodeWithTerritoryMap.get(tGeoCode.Name).add(tGeoCode);
            }else{
                List<Territory_Geo_Code__c> terrGeoCodeList = new List<Territory_Geo_Code__c>();
                terrGeoCodeList.add(tGeoCode);
                system.debug('testlistttt'+terrGeoCodeList.size());
                system.debug('testlistttt'+tGeoCode.Name);
                postalCodeWithTerritoryMap.put(tGeoCode.Name,terrGeoCodeList);
            }
            // End of Code - MB - 06/20/17 - Bug 43495
        }
        
        return postalCodeWithTerritoryMap;
    }
    
    /**
* Description :  When lead comes in then Lead will identify territory master by querying zipcode 
*                          with territory geo code object and get territory master  information.
*                Territory  master id will be update on lead territory lookup field.  
*/
    
    public static void UpdateTerritoryFromZipCode(list<Lead> leadList, Boolean isInsert, Boolean isUpdate) {
        
        list<Lead> LeadsWithTerIdList  = new list<Lead>();
        list<Lead> multipleLeadsWithTerId  = new  list<Lead>();
        
        //Map<String, String> zipMap = zipCodeMap(leadList); - Commented By MB - 06/20/17 - Bug 43495
        Map<String, List<Territory_Geo_Code__c>> zipMap = zipCodeMap(leadList); // Added by MB - 06/20/17 - Bug 43495
        // Ideally, ld.PostalCode is the same with tGeoCode.Name
        
        Set<string>multipleHandleTerritoryIds = new Set<string>();
        
        for(Lead ld : leadList) {
            //ld.Territory__c = zipMap.get(ld.PostalCode); - Commented By MB - 06/20/17 - Bug 43495
            // Start of Code - MB - 06/20/17 - Bug 43495
            List<Territory_Geo_Code__c> terrGeoCodeList = new List<Territory_Geo_Code__c>();
            String zipcode = UtilityCls.isStrNotNull(Ld.Architect_Zip_Code__c) ? Ld.Architect_Zip_Code__c : Ld.PostalCode;  // Added by MB - 06/22/17
            terrGeoCodeList = zipMap.get(ld.PostalCode);
           // System.debug('########test####'+terrGeoCodeList.size());
            if(terrGeoCodeList!=null && terrGeoCodeList.size() > 1 ){ //Added null check by Susmitha on 12/18/2017
                if( !ld.From_Other_System__c && ld.Account__c == null && isCommercialLead(ld.recordTypeId) ){
                    
                    for(Territory_Geo_Code__c terrGeoCode: terrGeoCodeList){
                        if( terrGeoCode.Business_Type__c == UtilityCls.COMMERCIAL ){
                            multipleHandleTerritoryIds.add( terrGeoCode.Territory__c );                         
                        }
                    }
                }
                multipleLeadsWithTerId.add( ld );
            }else{
            if(terrGeoCodeList!=null){
                for(Territory_Geo_Code__c terrGeoCode: terrGeoCodeList){
                    if(isCommercialLead(ld.recordTypeId) && terrGeoCode.Business_Type__c == UtilityCls.COMMERCIAL){
                        ld.Territory__c = terrGeoCode.Territory__c;                        
                    }else if(!isCommercialLead(ld.recordTypeId) && terrGeoCode.Business_Type__c == UtilityCls.RESIDENTIAL){
                        ld.Territory__c = terrGeoCode.Territory__c;
                    }
                }
                }
                LeadsWithTerIdList.add(ld);
            }
            // End of Code - MB - 06/20/17 - Bug 43495
            
        }
        //System.debug('***LeadsWithTerIdList'+LeadsWithTerIdList);
        //Perform Round robin before trigger in order to update 
        if(!LeadsWithTerIdList.isEmpty() && UtilityCls.isTriggerFired == false && isInsert == true){
            roundRobinAssignment(LeadsWithTerIdList);
            //Trigger will be fired second time after workflow rule exectuted 
            //Avoid(above scenario) Recursive execution for round robin
            //AvoidRecursiveTriggerClass.isTriggerFired = true;
            UtilityCls.isTriggerFired = true;
        }else if( !multipleLeadsWithTerId.isEmpty() && UtilityCls.isTriggerFired == false && isInsert == true ){
            handleMultipleTerritory( multipleLeadsWithTerId,zipMap,multipleHandleTerritoryIds );
            UtilityCls.isTriggerFired = true;
        }
    }
    
    /*
* Method to assign territory user as lead owner based on 
1.Architect_Zip_Code__c and 
2.Segment
*/
    public static Map<Id, List<Territory_User__c>> territoryUsers(list<Lead> leadList) {
        
        //Adding lead territory Ids to the set
        Set<Id> territoryIds = new Set<Id>();
        for(Lead led : leadList){
            territoryIds.add(led.Territory__c);
        }
        System.debug('***assignLeadOwner.territoryIds'+territoryIds);
        //Initializing Map with TerritoryId along with Territory Users.
        Map<Id, List<Territory_User__c>> TerritoryUsersMap =  new  Map<Id, List<Territory_User__c>>();
        System.debug('@@@territoryIds@@@'+territoryIds);
        for(Territory_User__c each : [SELECT Id, Segments__c,User__c,Territory__c,Last_Lead_Received_Time__c,Millisecond__c FROM Territory_User__c
                                      WHERE Territory__c in: territoryIds
                                      AND Role__c =: UtilityCls.AE // Added by MB - 09/08/17
                                      AND User__c != null // Added by MB - 09/08/17
                                      AND User__r.IsActive = true // Added by MB - 02/13/18
                                      order by Territory__c, Last_Lead_Received_Time__c, Millisecond__c]){
                                          System.debug('@@@Territory Users@@@'+each);
                                          if(TerritoryUsersMap.containsKey(each.Territory__c)){
                                              TerritoryUsersMap.get(each.Territory__c).add(each);
                                          }else{
                                              List<Territory_User__c> tUsersList = new List<Territory_User__c>();
                                              tUsersList.add(each);
                                              TerritoryUsersMap.put(each.Territory__c, tUsersList);
                                          }
                                      }
        system.debug('@@@Map with Ordered Users@@@'+TerritoryUsersMap.values()); 
        return TerritoryUsersMap;
    }//End of assignleadOwner 
    
    /*
* Method : RoundRobinAssignment
* Round Robin Lead Assignment
* Only for Commercials (Lead with Record type "New Commercial", "Existing Account Commercial")
*/
    public static void roundRobinAssignment(list<Lead> leadList) {
        Map<Id, List<Territory_User__c>> TerritoryUsersMap = territoryUsers(leadList);
        //List<Territory_User__c> terUserList =  new List<Territory_User__c>();  //Commented by MB - 07/20/17
        Map<Id, Territory_User__c> terUserMap =  new Map<Id, Territory_User__c>();  //Added by MB - 07/20/17
        
        for(Lead led : leadList){
            //if(TerritoryUsersMap.containsKey(led.Territory__c) && !UtilityCls.isStrNotNull(led.Account__c ) && isCommercialLead(led.recordTypeId) ){ - Commented by MB - 10/25/17 
            if(TerritoryUsersMap.containsKey(led.Territory__c) && isCommercialLead(led.recordTypeId) ){ // Added by MB - 10/25/17 - Removed Account condition as sitecore leads might come with new Accounts
                //Get Territory User based on last lead received time
                //Territory_User__c oldTerUser  = TerritoryUsersMap.get(led.Territory__c).get(0);
                Territory_User__c[] leadTerUserList = TerritoryUsersMap.get(led.Territory__c);
                if(leadTerUserList.size() > 0 ){
                    //market segment Test     
                    Boolean isUserFound = false;
                    Territory_User__c tUser = new Territory_User__c();
                    if(UtilityCls.isStrNotNull(led.Market_Segments__c)){
                        Set<String> leadSegments = new Set<String>();
                        leadSegments.addAll(led.Market_Segments__c.split(';'));
                        for(Territory_User__c oldTerUser : leadTerUserList){
                            Set<String> userSegments = new Set<String>();
                            if(UtilityCls.isStrNotNull(oldTerUser.Segments__c)){
                                userSegments.addAll(oldTerUser.Segments__c.split(';'));
                            }
                            if(userSegments.size() >= leadSegments.size()){ //Condition to check set method "ContainsAll"
                                // here userSegment size should be >= lead segment size means within the usersegments we are checking lead segments.
                                if(userSegments.containsAll(leadSegments)){
                                    //update Lead Owner with territory User in Round Robin Basis
                                    led.ownerId = oldTerUser.User__c;  
                                    //update current territory User Last Lead Received Time and Milliseconds
                                    tUser = initTerritoryUser(oldTerUser.id);
                                    isUserFound = true;
                                    // terUserList.add(tuser);
                                    break;
                                }
                            }
                        }//End of for 
                    }//End if 
                    if(isUserFound == false) { //Market segment == null OR (Market segment != null AND not avail in User List)
                        system.debug('leadTerUserList.User__c)' + leadTerUserList[0].User__c);
                        led.ownerId = leadTerUserList[0].User__c;
                        tUser = initTerritoryUser(leadTerUserList[0].id);                             
                    }
                    //terUserList.add(tuser); //Commented by MB - 07/20/17
                    terUserMap.put(tuser.Id, tuser); //Added by MB - 07/20/17
                }//End  if(terUserList.size() > 0 )
                /*for(Territory_User__c oldTerUser :  TerritoryUsersMap.get(led.Territory__c)){

Set<String> userSegments = new Set<String>();

//Round Robin 
//If Lead Market Segment == null
if(UtilityCls.isStrNotNull(led.Market_Segment__c)){

if(UtilityCls.isStrNotNull(oldTerUser.Segments__c)){
userSegments.addAll(oldTerUser.Segments__c.split(';'));
}

//If Lead Market Segment != null
if(userSegments.contains(led.Market_Segment__c)){
//update Lead Owner with territory User in Round Robin Basis
led.ownerId = oldTerUser.User__c;  
//update current territory User Last Lead Received Time and Milliseconds
Territory_User__c tUser = initTerritoryUser(oldTerUser.User__c);
terUserList.add(tuser);
break;
}else{

led.ownerId = oldTerUser.User__c;
Territory_User__c tUser = initTerritoryUser(oldTerUser.User__c);
terUserList.add(tuser);
break;
}

}else{

//update Lead Owner with territory User in Round Robin Basis
led.ownerId = oldTerUser.User__c;  
//update current territory User Last Lead Received Time and Milliseconds
Territory_User__c tUser = initTerritoryUser(oldTerUser.User__c);
terUserList.add(tuser);
break; 
}

} //End of Inner for Loop*/
                
            }//End of If
        }//End of lead for
        
        System.debug('***assignLeadOwner.leadList'+leadList);
        System.debug('***assignLeadOwner.terUserSetMap'+terUserMap);
        
        List<Territory_User__c> terUserList = terUserMap.values(); //Added by MB - 07/20/17
        if(terUserList.size()>0){ //Added by MB - 06/12/17
            try{
                Database.update(terUserList);
            }Catch(DMLException dmle){
                System.debug('Error!!!!' + dmle.getMessage());
            } 
        }
        
    }//End of the method
    
    public static Territory_User__c initTerritoryUser(Id terUserId){
        Territory_User__c tUser = new Territory_User__c(id = terUserId,
                                                        Last_Lead_Received_Time__c = System.now(),
                                                        Millisecond__c = System.currentTimeMillis());
        return tUser;
    }
    
    
    
    
    //Method to convert lead
    /*
* Method Name : ConvertLeads
* Description : Method to convert lead automatically on update lead based on criteria 
(If account != null && Account.Account_Focus__c == Target or Grow, account.ABC_Classification__c == A )
*/
    public static void ConvertLeads(Map<Id, Lead> newMap){
        
        
        Set<Id> acIdSet = new Set<Id>();
        for(Lead ld : newMap.values()){
            if(UtilityCls.isStrNotNull(ld.Account__c) && !ld.IsConverted && ld.RecordTypeId == existingComm ){
                acIdSet.add(ld.Account__c);
            }
        }//End  
        
        //Try{
        List<Database.LeadConvert> lcList = new List<Database.LeadConvert>();
        for(Lead ld : [SELECT id, Account__c, Account__r.Account_Focus__c,Account__r.ABC_Classification__c 
                       FROM Lead WHERE Account__c in: acIdSet 
                       AND Id In: newMap.keySet()]){
                           
                           if((ld.Account__r.Account_Focus__c == UtilityCls.TARGET || ld.Account__r.Account_Focus__c == UtilityCls.GROW)
                              &&(ld.Account__r.ABC_Classification__c == 'A')){
                                  Database.LeadConvert convertLd = new Database.LeadConvert();
                                  convertLd.setLeadId(ld.Id);
                                  convertLd.setAccountId(ld.Account__c);
                                  convertLd.setConvertedStatus('Converted');
                                  lcList.add(convertLd);
                              }
                       }
        
        
        List<Database.LeadConvertResult> lcrList;
        if ( ! lcList.isEmpty()){
            lcrList = Database.convertLead(lcList, False);
            System.debug('@@@newMap&&&'+newMap);
            for(Integer i=0; i<lcrList.size(); i++){
                
                if(!lcrList.get(i).isSuccess()){
                    
                    Id lId = lcrList.get(i).getLeadId();
                    System.debug('@@@lIdlId&&&'+lId);
                    
                    
                    Database.Error err = lcrList.get(i).getErrors().get(0); 
                    System.debug('@@@errerr&&&'+err);
                    newMap.get(lId).addError(err.getMessage());
                }
            }
        }  
        
        System.debug('@@@lcrList&&&'+lcrList);
        
        //}Catch(Exception e){
        //System.debug('!!!Error: in line number'+e.getLineNumber()+ ' Message: '+e.getMessage());
        //}        
    }
    
    /*
* Method Name : ChangeLeadOwner
* Description : If Account is exist on lead then assign Account's Mohawk Account Team member with Role as "Owner".
*/
    
    public static void ChangeLeadOwner(List<Lead> newList, Map<Id,Lead> oldMap, Boolean isInsert, Boolean isUpdate){
        
        Set<String> accountIdSet = new Set<String>();
        Map<String, List<Lead>> companyLeadsMap = new Map<String, List<Lead>>();
        
        for (Lead l : newList) {
            if(UtilityCls.isStrNotNull(l.Account__c)){
                accountIdSet.add(l.Account__c);
            }
        } 
        
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, (SELECT User__c FROM Mohawk_Account_Teams__r WHERE Role__c = 'Owner' LIMIT 1)
                                                          FROM Account WHERE id In: accountIdSet]);
        System.debug('***accountMap***'+accountMap);
        for(Lead l : newList){
            //only if account != null and Record type contains Commercial 
            if(UtilityCls.isStrNotNull(l.Account__c) && isCommercialLead(l.recordTypeId)){   
                Id userId;
                if(!accountMap.get(l.Account__c).Mohawk_Account_Teams__r.isEmpty()){
                    userId = accountMap.get(l.Account__c).Mohawk_Account_Teams__r[0].User__c;
                }
                System.debug('***UserId***'+userId);
                
                if(UtilityCls.isStrNotNull(userid)){ 
                    //When lead is updated Account changed on lead.
                    if(isUpdate){
                        if(l.Account__c != oldMap.get(l.Id).Account__c){
                            System.debug('***isUpdate***'+userId);
                            l.Ownerid = userId;
                        }
                        //When lead is inserted,  
                    }else if(isInsert){
                        System.debug('***isInsert***'+userId);
                        l.Ownerid = userId;
                    }
                }//End of If
            }//End of main if
            System.debug('***Lead***'+l);
        }//End of for
        
    }//End of Method
    
    
    /*
* Description : This Method is use by trigger to run assignment rule for for leads which is created by SF1.
*/
    @future
    public static void leadAssignMentForSF1( Set<id>LeadIds ){
        AssignmentRule AR = new AssignmentRule(); 
        AR = [select id from AssignmentRule where SobjectType = 'Lead' and Active =: true AND Name =: Label.Residential_Lead_Assignment_Rule limit 1];
        
        id newResRTId = UtilityCls.getRecordTypeInfo('Lead','New Residential');
        id existingRes = UtilityCls.getRecordTypeInfo('Lead','Existing Account Residential');
        
        List<lead>leadList = [SELECT Id FROM Lead WHERE Id IN : LeadIds AND ( recordTypeId =: newResRTId OR recordTypeId =: existingRes ) ];
        for ( Lead l : leadList ) {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.assignmentRuleId = AR.Id;
            l.setOptions(dmo);
        }
        try {
            update leadList;
        }catch (DmlException e) {
            system.debug( e.getMessage() );
        }
    }
    
 
    public static void changeRTtoMasterOpportunity(List<Lead> newLeads){
        
        // ID ResidentialMasterRT=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Residential Master').getRecordTypeId();
        set<ID> convertedOps=new set<ID>(); 
        // create the maps
        for(Lead lead : newLeads)
        {
            if(lead.IsConverted)
            {
                convertedOps.add(lead.ConvertedOpportunityId);   
            }
        }
        
        
        List<opportunity> OppList=[SELECT ID,RecordTypeId FROM Opportunity WHERE ID in:convertedOps];
        for(Opportunity op: OppList){
            //op.recordTypeId=ResidentialMasterRT;
            op.StageName='Discovery';   //modified by krishnapriya on  Nov 8,2017
            
        }
        User currentUser=[Select id,profile.Name from USER where id=:userInfo.getUserID()];
        if(currentUser!=null && currentUser.profile.Name =='Residential Sales User' && OppList.size()>0){
            Try{
                update OppList;
            }
            catch(exception e){
                system.debug('Exception e is'+e);
            }
        }
    }
    
    /*********************************************************************************
    * Author      : Mudit Saxena
    * Method Name : handleMultipleTerritory
    * Description : Assign commercial lead with account is equal to null and have multiple territory to appropriate user using round robin.
    * Date        : Dec 06,2017
    *********************************************************************************/
    public static void handleMultipleTerritory( list<Lead> leadList,Map<String, List<Territory_Geo_Code__c>>zipMap,Set<string>TerritoryIds ) {    
        
        Map<string,List<Territory_User__c>>TerritoryUserMap = new Map<string,List<Territory_User__c>>();
        if( TerritoryIds.size() > 0 ){
            for( Territory_User__c TU  : [ SELECT Id, Segments__c,User__c,User__r.Segment__c,Territory__c,Last_Lead_Received_Time__c,Millisecond__c FROM Territory_User__c WHERE Territory__c in: TerritoryIds AND Role__c =: UtilityCls.AE AND User__c != null AND User__r.Segment__c != null AND User__r.isActive = true order by Last_Lead_Received_Time__c, Millisecond__c ] ){
                if( TerritoryUserMap.ContainsKey( TU.Territory__c ) ){
                    TerritoryUserMap.get( TU.Territory__c ).add( TU );
                }else{
                    TerritoryUserMap.put( TU.Territory__c,new List<Territory_User__c>{ TU } );
                }
            }
        }
        
        Map<string,List<Territory_User__c>>TerritoryMap = new Map<string,List<Territory_User__c>>();
        for( string str : zipMap.keySet() ){            
            for( Territory_Geo_Code__c TGC : zipMap.get(str) ){
                if( TerritoryUserMap.ContainsKey( TGC.Territory__c ) ){
                    for( Territory_User__c terr : TerritoryUserMap.get( TGC.Territory__c ) ){
                        //if( TGC.Territory__c == terr.Territory__c ){
                            if( TerritoryMap.ContainsKey( str ) ){
                                TerritoryMap.get( str ).add( terr );
                            }else{
                                TerritoryMap.put( str, new List<Territory_User__c>{ terr } );
                            }
                        //}
                    }
                }
            }
        }
        system.debug( ':::::TerritoryMap::::' + TerritoryMap );
        Map<Id, Territory_User__c> terUserMap =  new Map<Id, Territory_User__c>(); 
        for( Lead led : leadList ){
            
            List<Territory_User__c>TerritoryUsersList = new List<Territory_User__c>();
                        TerritoryUsersList = TerritoryMap.get( UtilityCls.isStrNotNull(led.Architect_Zip_Code__c) ? led.Architect_Zip_Code__c : led.PostalCode );
            if( TerritoryUsersList != null && TerritoryUsersList .size() > 0 ){
            TerritoryUsersList.sort();
            
            if( isCommercialLead( led.recordTypeId ) ){
                if( TerritoryMap.size() > 0 ){
                    Boolean isUserFound = false;
                    Territory_User__c tUser = new Territory_User__c();
                    if( UtilityCls.isStrNotNull( led.Market_Segments__c ) ){
                        Set<String> leadSegments = new Set<String>();
                        leadSegments.addAll( led.Market_Segments__c.split(';') );
                        
                        
                        
                        for( Territory_User__c oldTerUser : TerritoryUsersList ){
                            Set<String> userSegments = new Set<String>();
                            if( UtilityCls.isStrNotNull(oldTerUser.User__r.Segment__c ) ){
                                userSegments.addAll( oldTerUser.User__r.Segment__c.split(';') );
                            }
                            if( userSegments.size() >= leadSegments.size() ){
                                if( userSegments.containsAll(leadSegments)){
                                    led.ownerId = oldTerUser.User__c;
                                    tUser = initTerritoryUser(oldTerUser.id);
                                    isUserFound = true;
                                    break;
                                }
                            }
                        }                           
                    }
                    if(isUserFound == false) { //Market segment == null OR (Market segment != null AND not avail in User List)                        
                        led.ownerId = TerritoryUsersList[0].User__c;
                        tUser = initTerritoryUser(TerritoryUsersList[0].id);                             
                    }
                    terUserMap.put(tuser.Id, tuser);
                    
                }
            }
            }
        }
        if( terUserMap!= null && terUserMap.values().size() > 0 ){
        List<Territory_User__c> terUserList = terUserMap.values();
        if(terUserList.size()>0){ //Added by MB - 06/12/17
            try{
                Database.update(terUserList);
            }Catch(DMLException dmle){
                System.debug('Error!!!!' + dmle.getMessage());
            } 
        }
        }
    }
    
    public static Map<decimal,Territory_User__c> sortMap ( Map<decimal,Territory_User__c> theMap ) {
        Map<decimal,Territory_User__c> returnMap = new Map<decimal,Territory_User__c>();
        Set<decimal> keySet = theMap.keySet();
        List<decimal> keyList = new List<decimal>();
        keyList.addAll(keySet);
        //sort the list ascending (predefined behaviour)
        keyList.sort();
        for (Integer i = 0; i < keyList.size(); i++)
                returnMap.put(keyList[i], theMap.get(keyList[i]));
        return returnMap;
    }
}