<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Grid Category Field</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Grid Category Fields</value>
    </caseValues>
    <fields>
        <help><!-- Minimum Level of Access Restrictions for this field.  Leave blank for no restrictions.  Special fields look at permission sets to see if the user has access --></help>
        <label><!-- Access Level --></label>
        <name>Access_Level__c</name>
        <picklistValues>
            <masterLabel>DM</masterLabel>
            <translation><!-- DM --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RVP</masterLabel>
            <translation><!-- RVP --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Special (Standard Cost)</masterLabel>
            <translation><!-- Special (Standard Cost) --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- left, center, or right justification for data within the grid column --></help>
        <label><!-- Column Justification --></label>
        <name>Column_Justification__c</name>
        <picklistValues>
            <masterLabel>center</masterLabel>
            <translation><!-- center --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>left</masterLabel>
            <translation><!-- left --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>right</masterLabel>
            <translation><!-- right --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Width of the grid column. Specify values like 40px or 10em --></help>
        <label><!-- Column Width --></label>
        <name>Column_Width__c</name>
    </fields>
    <fields>
        <help><!-- What type of data is captured/displayed in this field --></help>
        <label><!-- Data Type --></label>
        <name>Data_Type__c</name>
        <picklistValues>
            <masterLabel>Boolean</masterLabel>
            <translation><!-- Boolean --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Currency</masterLabel>
            <translation><!-- Currency --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Currency Integer</masterLabel>
            <translation><!-- Currency Integer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Custom</masterLabel>
            <translation><!-- Custom --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Date</masterLabel>
            <translation><!-- Date --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Datetime</masterLabel>
            <translation><!-- Datetime --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Number</masterLabel>
            <translation><!-- Number --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Text</masterLabel>
            <translation><!-- Text --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- API name of the field to reference --></help>
        <label><!-- Field API Name --></label>
        <name>Field_API_Name__c</name>
    </fields>
    <fields>
        <help><!-- Display name of a field when it is shown --></help>
        <label><!-- Field Label --></label>
        <name>Field_Label__c</name>
    </fields>
    <fields>
        <help><!-- Special handling functions for primary filters.  E.g. &quot;Warehouse&quot; or &quot;Price Zone&quot; - -  this tells the code which functions to call to populate the picklist with data. --></help>
        <label><!-- Filter Values Identifier --></label>
        <name>Filter_Values_Identifier__c</name>
    </fields>
    <fields>
        <help><!-- Which Product Category this field belongs to --></help>
        <label><!-- Grid Product Category --></label>
        <name>Grid_Product_Category__c</name>
        <relationshipLabel><!-- Grid Category Fields --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- left, center, or right justification for the header of the grid column --></help>
        <label><!-- Header Justification --></label>
        <name>Header_Justification__c</name>
        <picklistValues>
            <masterLabel>center</masterLabel>
            <translation><!-- center --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>left</masterLabel>
            <translation><!-- left --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>right</masterLabel>
            <translation><!-- right --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Is Accessory Field --></label>
        <name>Is_Accessory_Field__c</name>
    </fields>
    <fields>
        <help><!-- Whether or not to render the field in the grid on mobile --></help>
        <label><!-- Is Available On Mobile --></label>
        <name>Is_Available_On_Mobile__c</name>
    </fields>
    <fields>
        <help><!-- Whether or not the field is searched when a user types into the search box on a mobile device --></help>
        <label><!-- Is Mobile Search --></label>
        <name>Is_Mobile_Search__c</name>
    </fields>
    <fields>
        <help><!-- Whether the field shows in the primary set of columns in the result list --></help>
        <label><!-- Is Primary Display --></label>
        <name>Is_Primary_Display__c</name>
    </fields>
    <fields>
        <help><!-- Whether or not this field presents as a filter that must be filled prior to loading any data --></help>
        <label><!-- Is Primary Filter --></label>
        <name>Is_Primary_Filter__c</name>
    </fields>
    <fields>
        <help><!-- Whether this field is available as a filter that shows in the modal filter window available after the initial grid has been populated --></help>
        <label><!-- Is Secondary Filter --></label>
        <name>Is_Secondary_Filter__c</name>
    </fields>
    <fields>
        <help><!-- Whether this field allows the user to click it to get to the secondary display window --></help>
        <label><!-- Is Secondary Link --></label>
        <name>Is_Secondary_Link__c</name>
    </fields>
    <fields>
        <label><!-- Is Secondary Modal Header --></label>
        <name>Is_Secondary_Modal_Header__c</name>
    </fields>
    <fields>
        <help><!-- Sort order for the primary section of fields in the secondary information modal --></help>
        <label><!-- Modal Primary Sort Order --></label>
        <name>Modal_Primary_Sort_Order__c</name>
    </fields>
    <fields>
        <help><!-- Sort order for the secondary section of fields in the secondary information modal --></help>
        <label><!-- Modal Secondary Sort Order --></label>
        <name>Modal_Secondary_Sort_Order__c</name>
    </fields>
    <fields>
        <help><!-- Which section the field appears under in the secondary modal.  Leave blank if field should not be displayed in the secondary modal. --></help>
        <label><!-- Modal Section --></label>
        <name>Modal_Section__c</name>
        <picklistValues>
            <masterLabel>Primary Information</masterLabel>
            <translation><!-- Primary Information --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Secondary Information</masterLabel>
            <translation><!-- Secondary Information --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Order in which the primary fields display from left to right --></help>
        <label><!-- Primary Display Order --></label>
        <name>Primary_Display_Order__c</name>
    </fields>
    <fields>
        <help><!-- Order in which filters in the modal window display from left to right, top to bottom --></help>
        <label><!-- Secondary Display Order --></label>
        <name>Secondary_Display_Order__c</name>
    </fields>
    <fields>
        <label><!-- Short Label --></label>
        <name>Short_Label__c</name>
    </fields>
    <fields>
        <help><!-- Routing for special fields, like Local Stock, that have to call additional functions to derive their data. --></help>
        <label><!-- Special Handling --></label>
        <name>Special_Handling__c</name>
        <picklistValues>
            <masterLabel>Indicator</masterLabel>
            <translation><!-- Indicator --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Local Stock</masterLabel>
            <translation><!-- Local Stock --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Price_Grid__c</masterLabel>
            <translation><!-- Price_Grid__c --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Whether or not to truncate the data if it exceeds the width defined for this field --></help>
        <label><!-- Truncate At Max Width --></label>
        <name>Truncate_At_Max_Width__c</name>
    </fields>
    <startsWith>Consonant</startsWith>
</CustomObjectTranslation>
