<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CPL - Carpet Tile</label>
    <protected>false</protected>
    <values>
        <field>Grid_Type__c</field>
        <value xsi:type="xsd:string">Customer_Price_List</value>
    </values>
    <values>
        <field>Product_Category_Field_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Product_Category__c</field>
        <value xsi:type="xsd:string">Carpet Tile</value>
    </values>
    <values>
        <field>SObject_Name__c</field>
        <value xsi:type="xsd:string">CPL_Carpet_Tile__c</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
