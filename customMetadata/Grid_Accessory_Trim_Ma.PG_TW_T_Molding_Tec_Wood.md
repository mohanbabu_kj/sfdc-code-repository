<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PG TW T Molding Tec Wood</label>
    <protected>false</protected>
    <values>
        <field>Accessory_Code__c</field>
        <value xsi:type="xsd:string">WET</value>
    </values>
    <values>
        <field>Accessory_Type_Name__c</field>
        <value xsi:type="xsd:string">T-Molding</value>
    </values>
    <values>
        <field>Grid_Product_Category__c</field>
        <value xsi:type="xsd:string">Price_Grid_TecWood</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
</CustomMetadata>
