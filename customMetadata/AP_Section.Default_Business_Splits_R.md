<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Business Splits</label>
    <protected>false</protected>
    <values>
        <field>Always_Show__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Brand_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Business_Type__c</field>
        <value xsi:type="xsd:string">Retail</value>
    </values>
    <values>
        <field>Display_As__c</field>
        <value xsi:type="xsd:string">Block</value>
    </values>
    <values>
        <field>ExpandableDefaultON_PHONE__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ExpandableDefaultON_TABLET__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ExpandableDefaultON__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parent_Section__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_Object__c</field>
        <value xsi:type="xsd:string">Account Profile Setting</value>
    </values>
    <values>
        <field>Section_Type__c</field>
        <value xsi:type="xsd:string">Total</value>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
</CustomMetadata>
