<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PG_Tile_Selling_Style_Num</label>
    <protected>false</protected>
    <values>
        <field>Access_Level__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Column_Justification__c</field>
        <value xsi:type="xsd:string">center</value>
    </values>
    <values>
        <field>Column_Width__c</field>
        <value xsi:type="xsd:string">6.5%</value>
    </values>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Product__r.Product_Style_Number__c</value>
    </values>
    <values>
        <field>Field_Label__c</field>
        <value xsi:type="xsd:string">Sell #</value>
    </values>
    <values>
        <field>Filter_Values_Identifier__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Grid_Product_Category__c</field>
        <value xsi:type="xsd:string">Price_Grid_Tile</value>
    </values>
    <values>
        <field>Header_Justification__c</field>
        <value xsi:type="xsd:string">center</value>
    </values>
    <values>
        <field>Is_Accessory_Field__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Available_On_Mobile__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Is_Mobile_Search__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Is_Primary_Display__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Is_Primary_Filter__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Secondary_Filter__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Secondary_Link__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Secondary_Modal_Header__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Modal_Primary_Sort_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Modal_Secondary_Sort_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Modal_Section__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Primary_Display_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Secondary_Display_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Short_Label__c</field>
        <value xsi:type="xsd:string">Sell #</value>
    </values>
    <values>
        <field>Special_Handling__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Truncate_At_Max_Width__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
