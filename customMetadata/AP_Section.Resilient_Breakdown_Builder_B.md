<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Resilient Breakdown</label>
    <protected>false</protected>
    <values>
        <field>Always_Show__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Brand_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Business_Type__c</field>
        <value xsi:type="xsd:string">Builder</value>
    </values>
    <values>
        <field>Display_As__c</field>
        <value xsi:type="xsd:string">Table</value>
    </values>
    <values>
        <field>ExpandableDefaultON_PHONE__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ExpandableDefaultON_TABLET__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ExpandableDefaultON__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parent_Section__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_Object__c</field>
        <value xsi:type="xsd:string">Account Profile</value>
    </values>
    <values>
        <field>Section_Type__c</field>
        <value xsi:type="xsd:string">Resilient</value>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
</CustomMetadata>
