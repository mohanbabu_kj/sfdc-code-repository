<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Merchandizing Vehicle Price List</label>
    <protected>false</protected>
    <values>
        <field>Column_Font_Size_Mobile__c</field>
        <value xsi:type="xsd:string">14px</value>
    </values>
    <values>
        <field>Column_Font_Size__c</field>
        <value xsi:type="xsd:string">16px</value>
    </values>
    <values>
        <field>Font_Family__c</field>
        <value xsi:type="xsd:string">SalesforceSans</value>
    </values>
    <values>
        <field>Grid_Type__c</field>
        <value xsi:type="xsd:string">Merch Vehicle Price List</value>
    </values>
    <values>
        <field>Header_Font_Size_Mobile__c</field>
        <value xsi:type="xsd:string">14px</value>
    </values>
    <values>
        <field>Header_Font_Size__c</field>
        <value xsi:type="xsd:string">14px</value>
    </values>
    <values>
        <field>Number_of_Records_per_Page__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>Row_Height_Tablet__c</field>
        <value xsi:type="xsd:string">60</value>
    </values>
    <values>
        <field>Row_height__c</field>
        <value xsi:type="xsd:string">33</value>
    </values>
    <values>
        <field>SObject_Name__c</field>
        <value xsi:type="xsd:string">Product2</value>
    </values>
    <values>
        <field>Show_Account_Field__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
