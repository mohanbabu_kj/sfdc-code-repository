<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ChangeOwner_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">ChangeOwner_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id, AccountId, OwnerId FROM Opportunity WHERE AccountId IN :accIds AND OwnerId IN :ownerIds</value>
    </values>
</CustomMetadata>
