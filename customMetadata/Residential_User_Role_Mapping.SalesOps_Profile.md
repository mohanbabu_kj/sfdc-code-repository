<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SalesOps - Profile</label>
    <protected>false</protected>
    <values>
        <field>Grid_Permission_Role_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Grid_Permission_Role__c</field>
        <value xsi:type="xsd:string">SalesOps</value>
    </values>
    <values>
        <field>Profile_Name_to_Match__c</field>
        <value xsi:type="xsd:string">Residential Sales Operation</value>
    </values>
    <values>
        <field>Territory_Role_to_Match__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
