<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CreateSamplePerformanceInventory_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">CreateSamplePerformanceInventory_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id,Customer_Group_Number__c,Business_Type__c,RecordTypeId,Global_Account_Number__c FROM Account WHERE (LastModifiedDate = TODAY OR LastModifiedDate = LAST_N_DAYS:1) AND Business_Type__c = &apos;Residential&apos; AND Type = &apos;Invoicing&apos;</value>
    </values>
</CustomMetadata>
