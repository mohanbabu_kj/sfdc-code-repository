<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Cushion</label>
    <protected>false</protected>
    <values>
        <field>Brands__c</field>
        <value xsi:type="xsd:string">Aladdin;Horizon;Karastan;Portico;Aladdin Commercial;Hard Surface</value>
    </values>
    <values>
        <field>Product_Category__c</field>
        <value xsi:type="xsd:string">Cushion</value>
    </values>
</CustomMetadata>
