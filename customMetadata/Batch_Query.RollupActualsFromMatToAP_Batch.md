<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RollupActualsFromMatToAP_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">RollupActualsFromMatToAP_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id,Account_Profile__c,Account_Profile__r.Multi_Channel__c,Actuals_Sent_from_BI__c,Brands__c,Product_Types__c,
BMF_Team_Member__c,
Actual_Mohawk_Purchases_Carpet__c , 
Actual_Mohawk_Purchases_Aladdin__c , 
Actual_Mohawk_Purchases_Aladdin_Comm__c , 
Actual_Mohawk_Purchases_Horizon__c , 
Actual_Mohawk_Purchases_Karastan__c , 
Act_MHK_Purch_Car_PP__c , 
Actual_Mohawk_Purchases_Cushion__c , 
Act_MHK_Purch_CU_ALA__c , 
Act_MHK_Purch_CU_HOR__c , 
Act_MHK_Purch_CU_KAR__c , 
Act_MHK_Purch_CU_ALA_Comm__c , 
Act_MHK_Purch_CU_PP__c ,
Actual_Mohawk_Purchases_Hardwood__c , 
Act_MHK_Purch_HW_RT__c , 
Act_MHK_Purch_HW_PP__c , 
Actual_Mohawk_Purchases_Laminate__c ,
Act_MHK_Purch_Lam_RT__c ,
Act_MHK_Purch_Lam_PP__c , 
Actual_Mohawk_Purchases_Tile__c ,
Act_MHK_Purch_Tile_RT__c , 
Act_MHK_Purch_Tile_PP__c , 
Actual_Mohawk_Purchases_Resilient__c , 
Act_MHK_Purch_Resi_PP__c , 
Actual_Mohawk_Purch_Resilient_Comm__c , 
Actual_Mohawk_Purch_Resilient_Retail__c , 
Actual_Mohawk_Purch_Aladdin_Comm_Total__c,Actual_Mohawk_Purchases_Total__c,Mohawk_Account_Team__c.User__c FROM Mohawk_Account_Team__c WHERE (LastModifiedDate = LAST_N_DAYS:10 OR LastModifiedDate = TODAY) AND Actuals_Sent_from_BI__c = true AND Account_Profile__r.Multi_Channel__c = true ALL ROWS</value>
    </values>
</CustomMetadata>
