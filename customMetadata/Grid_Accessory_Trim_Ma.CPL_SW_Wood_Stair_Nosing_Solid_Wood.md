<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CPL SW Wood Stair Nosing Solid Wood</label>
    <protected>false</protected>
    <values>
        <field>Accessory_Code__c</field>
        <value xsi:type="xsd:string">WES</value>
    </values>
    <values>
        <field>Accessory_Type_Name__c</field>
        <value xsi:type="xsd:string">Stairnose (Flush)</value>
    </values>
    <values>
        <field>Grid_Product_Category__c</field>
        <value xsi:type="xsd:string">CPL_SolidWood</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
</CustomMetadata>
