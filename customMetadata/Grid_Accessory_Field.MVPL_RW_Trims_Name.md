<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MVPL_RW_Trims_Name</label>
    <protected>false</protected>
    <values>
        <field>Accessory_Grid_Label__c</field>
        <value xsi:type="xsd:string">Trims and Molding</value>
    </values>
    <values>
        <field>Accessory_Grid__c</field>
        <value xsi:type="xsd:string">TRIMS_AND_MOLDING</value>
    </values>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Name</value>
    </values>
    <values>
        <field>Field_Label__c</field>
        <value xsi:type="xsd:string">Name</value>
    </values>
    <values>
        <field>Grid_Display_Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>Grid_Product_Category_Object_Name__c</field>
        <value xsi:type="xsd:string">CPL_Rev_Wood__c</value>
    </values>
    <values>
        <field>Grid_Product_Category__c</field>
        <value xsi:type="xsd:string">MVPL_RevWood</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
