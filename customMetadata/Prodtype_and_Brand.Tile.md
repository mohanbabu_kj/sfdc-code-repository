<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Tile</label>
    <protected>false</protected>
    <values>
        <field>Brands__c</field>
        <value xsi:type="xsd:string">Portico;Hard Surface</value>
    </values>
    <values>
        <field>Product_Category__c</field>
        <value xsi:type="xsd:string">Tile</value>
    </values>
</CustomMetadata>
