<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BG Grid - Cushion</label>
    <protected>false</protected>
    <values>
        <field>Grid_Type__c</field>
        <value xsi:type="xsd:string">Buying_Group_Grid</value>
    </values>
    <values>
        <field>Product_Category_Field_API_Name__c</field>
        <value xsi:type="xsd:string">Salesforce_Product_Category__c</value>
    </values>
    <values>
        <field>Product_Category__c</field>
        <value xsi:type="xsd:string">Cushion</value>
    </values>
    <values>
        <field>SObject_Name__c</field>
        <value xsi:type="xsd:string">Price_Grid__c</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
</CustomMetadata>
