<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MVPL_RT_Adhesives_Selling_Style_Num</label>
    <protected>false</protected>
    <values>
        <field>Accessory_Grid_Label__c</field>
        <value xsi:type="xsd:string">Adhesives Sealers</value>
    </values>
    <values>
        <field>Accessory_Grid__c</field>
        <value xsi:type="xsd:string">ADHESIVES_SEALERS</value>
    </values>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Product__r.Product_Style_Number__c</value>
    </values>
    <values>
        <field>Field_Label__c</field>
        <value xsi:type="xsd:string">Style #</value>
    </values>
    <values>
        <field>Grid_Display_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Grid_Product_Category_Object_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Grid_Product_Category__c</field>
        <value xsi:type="xsd:string">MVPL_Resilient_Tile</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
