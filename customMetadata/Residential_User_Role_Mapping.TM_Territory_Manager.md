<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TM - Territory Manager</label>
    <protected>false</protected>
    <values>
        <field>Grid_Permission_Role_Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Grid_Permission_Role__c</field>
        <value xsi:type="xsd:string">TM</value>
    </values>
    <values>
        <field>Profile_Name_to_Match__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Territory_Role_to_Match__c</field>
        <value xsi:type="xsd:string">Territory Manager</value>
    </values>
</CustomMetadata>
