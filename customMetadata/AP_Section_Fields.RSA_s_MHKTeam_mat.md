<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RSA&apos;s</label>
    <protected>false</protected>
    <values>
        <field>AP_Section__c</field>
        <value xsi:type="xsd:string">Primary_Inputs_MHKTeam_mat</value>
    </values>
    <values>
        <field>Alignment__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Account_Profile__r.Number_of_Sales_Associates__c</value>
    </values>
    <values>
        <field>Label_L__c</field>
        <value xsi:type="xsd:string">RSA&apos;s</value>
    </values>
    <values>
        <field>Label_M__c</field>
        <value xsi:type="xsd:string">RSA&apos;s</value>
    </values>
    <values>
        <field>Label_S__c</field>
        <value xsi:type="xsd:string">RSA&apos;s</value>
    </values>
    <values>
        <field>Length__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Other</value>
    </values>
    <values>
        <field>Typex__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>isShow__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
