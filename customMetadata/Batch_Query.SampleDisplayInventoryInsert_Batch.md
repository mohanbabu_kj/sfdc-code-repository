<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SampleDisplayInventoryInsert_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">SampleDisplayInventoryInsert_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT id,Product__c,Customer_Group_Number__c,Product__r.Residential_Product_Unique_Key__c,Product__r.Brand_Code__c,Product__r.Brand_Description__c, Product__r.ERP_Product_Type__c , Product__r.Name FROM Product_Customer_Group_Relationship__c WHERE Is_Product_Active__c = true AND Product__r.Trackable_Display_asset__C = true AND Product__r.RecordtypeId =: recordTypeId</value>
    </values>
</CustomMetadata>
