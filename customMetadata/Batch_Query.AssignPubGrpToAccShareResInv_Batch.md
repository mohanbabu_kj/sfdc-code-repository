<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AssignPubGrpToAccShareResInv_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">AssignPubGrpToAccShareResInv_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id, IsDeleted, Name, RecordTypeId, Account__c, User__c, Territory__c, Territory_User_Number__c  FROM Mohawk_Account_Team__c WHERE  RecordTypeId = : invMATRTId  AND  ( LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY ) LIMIT 500000</value>
    </values>
</CustomMetadata>
