<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CPL_CU_RVP_Direct_Price</label>
    <protected>false</protected>
    <values>
        <field>Access_Level__c</field>
        <value xsi:type="xsd:string">RVP</value>
    </values>
    <values>
        <field>Column_Justification__c</field>
        <value xsi:type="xsd:string">right</value>
    </values>
    <values>
        <field>Column_Width__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">Currency</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">RVP_200_Plus_Price__c</value>
    </values>
    <values>
        <field>Field_Label__c</field>
        <value xsi:type="xsd:string">RVP Direct</value>
    </values>
    <values>
        <field>Filter_Values_Identifier__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Grid_Product_Category__c</field>
        <value xsi:type="xsd:string">CPL_Cushion</value>
    </values>
    <values>
        <field>Header_Justification__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Is_Accessory_Field__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Available_On_Mobile__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Is_Mobile_Search__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Primary_Display__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Primary_Filter__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Secondary_Filter__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Secondary_Link__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Secondary_Modal_Header__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Modal_Primary_Sort_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Modal_Secondary_Sort_Order__c</field>
        <value xsi:type="xsd:double">20.0</value>
    </values>
    <values>
        <field>Modal_Section__c</field>
        <value xsi:type="xsd:string">Secondary Information</value>
    </values>
    <values>
        <field>Primary_Display_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Secondary_Display_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Short_Label__c</field>
        <value xsi:type="xsd:string">RVP Direct</value>
    </values>
    <values>
        <field>Special_Handling__c</field>
        <value xsi:type="xsd:string">Price_Grid__c</value>
    </values>
    <values>
        <field>Truncate_At_Max_Width__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
