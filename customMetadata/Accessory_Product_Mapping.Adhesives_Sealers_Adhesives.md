<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Adhesives &amp; Sealers - Adhesives</label>
    <protected>false</protected>
    <values>
        <field>Accessory_Type__c</field>
        <value xsi:type="xsd:string">ADHESIVES_SEALERS</value>
    </values>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">FLOORINGNAINSTALLATIONPRODUCT</value>
    </values>
    <values>
        <field>Sub_Category__c</field>
        <value xsi:type="xsd:string">Adhesives</value>
    </values>
</CustomMetadata>
