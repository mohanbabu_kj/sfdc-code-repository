trigger productRelationshipTrigger on Product_Relationship__c ( After insert,After update ) {
    /*********************************************
    string productNames;
    set<string>relatedProductIds = new set<string>();
    for( Product_Relationship__c PR : trigger.new ){        
        if( PR.Relations_Type__c == 'DISPLAY' ){
            relatedProductIds.add( PR.Related_Product__c );
        }
    }
    
    if( !relatedProductIds.isEmpty() ){
        List<Product2>updateRelatedProductList = new List<Product2>();
        Map<string,Set<string>>productNameMap = new Map<string,Set<string>>();
        for( Product_Relationship__c PR :  [ SELECT id,Product__r.Name,Related_Product__c FROM  Product_Relationship__c WHERE Relations_Type__c =: 'DISPLAY' AND product__r.Trackable_Display_Asset__c =: true AND Related_Product__c IN : relatedProductIds ] ){
            if( productNameMap.ContainsKey( PR.Related_Product__c ) ){
                productNameMap.get( PR.Related_Product__c ).add( PR.Product__r.Name );
            }else{
                productNameMap.put( PR.Related_Product__c,new set<string>{ PR.Product__r.Name } );
            }
            
        }
        
        for( Product2 prod : [ SELECT id,Display__c FROM Product2 WHERE id IN : relatedProductIds ] ){
            prod.Display__c = String.join( new List<string>( productNameMap.get( prod.id ) ),';' );
            updateRelatedProductList.add( prod );
        }
        
        if( !updateRelatedProductList.isEmpty() )
            update updateRelatedProductList;
		
    }
	*********************************************/
    productRelationshipTriggerHandler.updateDisplay( trigger.new );
}