/**************************************************************************

Name : Claim_ApproversTrigger

===========================================================================
Purpose : Trigger to post on chatter when a Claim approval is inserted
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha       10/Nov/2017    Created         

***************************************************************************/
trigger Claim_ApproversTrigger on Claim_Approvers__c (after insert) {
    
    if(Trigger.isAfter && Trigger.isInsert){
        Claim_ApproversTrigger_Handler.postOnChatter(Trigger.new);
    }
    
}