trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert,before update, after insert, after update, before delete, after delete) {

    
    if(Trigger.isBefore){
        
         if(Trigger.isInsert){
            OpportunityLineItemTriggerHandler.updateLineItemNumber(Trigger.new,false,true);
        }
    /* Commented by Suresh - Integration issue from LDS    
        if(Trigger.isUpdate){
            OpportunityLineItemTriggerHandler.updateLineItemNumber(Trigger.new,true,false);
        }
    */ 
    
  /*      if(Trigger.isUpdateDelete){
            //Method to update Account Total Pipeline
            OpportunityLineItemTriggerHandler.UpdateAccountTotalPipeline(null,trigger.old,null,false,false,true);
        } */
       
    }
    
    if(Trigger.isAfter){
        if(Trigger.isDelete){
            //Method to insert deleted line items to custom object Opportunity_Line_Item__c 
            OpportunityLineItemTriggerHandler.insertOppLineItem(Trigger.old); 
        }
    } 
}