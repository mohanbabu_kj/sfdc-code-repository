trigger CPLResidentialBroadloomTrigger on CPL_Residential_Broadloom__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Residential_Broadloom__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}