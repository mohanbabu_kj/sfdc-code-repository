/**************************************************************************

Name : TerritoryUserTrigger 

===========================================================================
Purpose : This class is used for Terriotry User Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0       Sasi Naik      10/4/2017       Created
============================================================================ */
trigger TerritoryUserTrigger on Territory_User__c(before insert,before Update, after insert, after update, after delete) {
    
    //if(UtilityCls.isTriggerExecute('Territory_User__c')){ // Based on custom setting(Default_Configuration__c) value trigger will be executed  
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            //TerritoryUserTriggerHandler.validateDuplicate(trigger.New,false);
            //TerritoryUserTriggerHandler.updateUserGeography(trigger.New); - Commented as part of Bug 59306 - MB - 5/21/18 - Business will manually maintain Geography
        }
        
        if(Trigger.isupdate){
            //TerritoryUserTriggerHandler.validateDuplicate(trigger.New,true);
            //TerritoryUserTriggerHandler.updateUserGeography(trigger.New); - Commented as part of Bug 59306 - MB - 5/21/18 - Business will manually maintain Geography
        }
    }
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            TerritoryUserTriggerHandler.validateDuplicate(trigger.NewMap);
            TerritoryUserTriggerHandler.collectNewIds(trigger.new); //Added by MB - Bug 70120 - 1/29/19
        }
        /* Start of Code - MB - Bug 70120 - 1/29/19 */
        if(Trigger.isUpdate){
            TerritoryUserTriggerHandler.updateUserAssociationInTerritoryModel(trigger.newMap, trigger.oldMap);
        }
        if(Trigger.isDelete){
            TerritoryUserTriggerHandler.collectDeletedIds(trigger.old);
        }
        /* End of Code - MB - Bug 70120 - 1/29/19 */
    }
    //}
    
}