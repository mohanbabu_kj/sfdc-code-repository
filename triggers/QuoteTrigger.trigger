trigger QuoteTrigger on Quote (before insert, before update, after insert) {
    
    Default_Configuration__c defConfig = Default_Configuration__c.getOrgDefaults();
    
    if(defConfig.Quote_Trigger__c){
        if(trigger.isUpdate && trigger.isBefore){
            //QuoteTriggerHandler.validateStatusChange(trigger.newMap, trigger.oldMap);
        }
        
        if(trigger.isInsert && trigger.isAfter){
            QuoteTriggerHandler.chatterPostForSAM(trigger.new);
        }
    }
}