trigger CPLResilientSheetTrigger on CPL_Resilient_Sheet__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Resilient_Sheet__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}