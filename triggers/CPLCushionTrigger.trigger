trigger CPLCushionTrigger on CPL_Cushion__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Cushion__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}