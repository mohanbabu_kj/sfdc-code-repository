trigger CustomActivityTrigger on Custom_Activities__c ( Before insert, Before update ) {
    
    if( trigger.isBefore ){
        if( trigger.isInsert ){
            CustomActivityTriggerHandler.insertFields( trigger.new );
        }
        
        if( trigger.isUpdate ){
            CustomActivityTriggerHandler.insertFields( trigger.new );
        }
    }
}