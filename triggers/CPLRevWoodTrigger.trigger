trigger CPLRevWoodTrigger on CPL_Rev_Wood__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Rev_Wood__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}