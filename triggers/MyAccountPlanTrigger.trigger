trigger MyAccountPlanTrigger on My_Account_Plan__c (after delete,after update, after insert, after undelete) {
    if(Trigger.isAfter){
    	if(Trigger.isInsert){
    		MyAccountPlanTriggerHandler.insertAccountData(Trigger.new);
    	}

    	if(Trigger.isUpdate){
    		MyAccountPlanTriggerHandler.updateAccountData(Trigger.new, trigger.OldMap);
    	}

    	if(Trigger.isDelete) {
    		MyAccountPlanTriggerHandler.deleteAccountData(Trigger.old);
    	}

    	if(Trigger.isUnDelete) {
    		MyAccountPlanTriggerHandler.undeleteAccountData(Trigger.new);
    	}
    }
}