/**************************************************************************

Name : SamplePerformanceInventoryTrigger

===========================================================================
Purpose : This class is used for Sample Performance Inventory Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0       Sasi Naik      27/Apr/2017    Created
============================================================================*/
trigger SamplePerformanceInventoryTrigger on Sample_Display_Inventory__c(after insert,after delete) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){   
            system.debug('Null Body');
            //SamplePerformanceInventoryHandler.UpdateAccountCustomerGroupValues(Trigger.newMap);
        }
        if(Trigger.isDelete){
            OutBoundDeletion.onDeleteSampleDisplayInventory(trigger.Old);//Added by - Nagendra 12-20-2018
        }
     }
}