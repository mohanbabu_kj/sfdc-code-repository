trigger AccountCompetitorTrigger on Account_Competitor__c (before insert, after insert, before update, after update) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            AccountCompetitorTriggerHandler.updateAccountCompetitorOnCreate(trigger.new);
        }
    }
}