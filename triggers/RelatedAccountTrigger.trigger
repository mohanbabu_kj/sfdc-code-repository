/**************************************************************************

Name : RelatedAccountTrigger

===========================================================================
Purpose : This class is used for Project Related Account Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand          03/02/2017    Created
***************************************************************************/

trigger RelatedAccountTrigger on Related_Account__c(after insert,after update, after delete, before update,before insert,before delete) {
    
    if(UtilityCls.isTriggerExecute('Related_Account__c')){ // Based on custom setting(Default_Configuration__c) value trigger will be executed  
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                //Add Related Account owners to Opprotunity team.
                RelatedAccountTriggerHandler.createRelatedAccounts(trigger.newMap); 
                RelatedAccountTriggerHandler.UpdateAccountTotalPipeline(trigger.new,null,null,true,false,false);          
            }
            if(Trigger.isUpdate){
                RelatedAccountTriggerHandler.updateOppContactRoles(trigger.newMap, trigger.oldMap); // Added by MB - 07/19/17
                RelatedAccountTriggerHandler.UpdateAccountTotalPipeline(trigger.new,null,trigger.OldMap,false,true,false);
            }
        }
        
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                //Check the Related Account has any duplicate records.
                //RelatedAccountTriggerHandler.ValidateDuplicate(trigger.new); - Commented as multiple contacts for same account is possible - MB - 2/1/18
                RelatedAccountTriggerHandler.updateExternalId(trigger.new,false,true);
            }
            if(Trigger.isUpdate){
                RelatedAccountTriggerHandler.updateExternalId(trigger.new,true,false);  // sb added 10-18-2017
            }
            if(Trigger.isDelete){
                RelatedAccountTriggerHandler.deleteOppContactRoles(trigger.old); // Added by MB - 07/26/17
                RelatedAccountTriggerHandler.UpdateAccountTotalPipeline(null,trigger.old,null,false,false,true);
            }
        }
    } 
}