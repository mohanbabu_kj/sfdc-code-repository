trigger CPLTecWoodTrigger on CPL_Tec_Wood__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Tec_Wood__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}