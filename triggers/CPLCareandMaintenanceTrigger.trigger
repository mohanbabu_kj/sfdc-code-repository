trigger CPLCareandMaintenanceTrigger on CPL_Care_and_Maintenance__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Care_and_Maintenance__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}