/**************************************************************************

Name : OpportunityTrigger

===========================================================================
Purpose : This class is used for Opportunity Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand          02/02/2017    Created
***************************************************************************/
trigger OpportunityTrigger on Opportunity(after insert, after update ,before insert, before update) {
    
    if(UtilityCls.isTriggerExecute('Opportunity') || Test.isRunningTest()){ // Based on custom setting(Default_Configuration__c) value trigger will be executed
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                //Create related accounts based on opportunity Account's partner accounts.
                OpportunityTriggerHandler.createRelatedAccounts(Trigger.New, Trigger.newMap);
            }
            if(Trigger.isUpdate){
                OpportunityTriggerHandler.updateAccountPipeline(Trigger.newMap, Trigger.oldMap); // Added by MB - 10/25/17
            }
        }  // start change by SB 9-28-2016  
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                OpportunityTriggerHandler.updateOppOnBefore(Trigger.new, true, false); // Added by MB - 10/16/17
                OpportunityTriggerHandler.ldsFlagAndLostToCompetitor(Trigger.new,false,true);
            }
            if(Trigger.isUpdate){
                OpportunityTriggerHandler.updateOppOnBefore(Trigger.new, false, true); // Added by MB - 10/16/17
                OpportunityTriggerHandler.ldsFlagAndLostToCompetitor(Trigger.new,true,false);
            }
            
        }  // End change by SB 9-28-2016  
    }
}