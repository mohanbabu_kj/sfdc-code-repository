trigger CPLCommercialBroadloomTrigger on CPL_Commercial_Broadloom__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Commercial_Broadloom__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}