trigger ContactTrigger on Contact (before insert,before update,after insert,after update) {
	if(UtilityCls.isTriggerExecute('Contact') ||  Test.isRunningTest()){ // Based on custom setting(Default_Configuration__c) value trigger will be executed
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                ContactTriggerHandler.updateContact(Trigger.new, Trigger.old, true, false);
                ContactTriggerHandler.ldsFlag(Trigger.new,false,true);
            }
            if(Trigger.isUpdate) {
                ContactTriggerHandler.updateContact(Trigger.new, Trigger.old, false, true);
                System.Debug('Contact Update LDS Flags :' );
                ContactTriggerHandler.ldsFlag(Trigger.new,true,false);  
            }
        }
     }
}