trigger UserTrigger on User (before insert, before update) {
    
    if(UtilityCls.isTriggerExecute('User')){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                UserTriggerHandler.updateUserInfo(Trigger.new, Trigger.oldMap, true, false);
            }
            if(Trigger.isUpdate){
                UserTriggerHandler.updateUserInfo(Trigger.new, Trigger.oldMap, false, true);
            }
        }
    }
    
}