/**************************************************************************

Name : MohawkAccountTeamTrigger 

===========================================================================
Purpose : This class is used for Mohawk Account TeamTrigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         27/Feb/2017    Created
2.0       Sasi Naik      16/3/2017       Updated
***************************************************************************/
trigger MohawkAccountTeamTrigger on Mohawk_Account_Team__c (before insert,before update, before delete,after insert,after update,after delete,after undelete) {
    
    //if(UtilityCls.isTriggerExecute('Mohawk_Account_Team__c')){ // Based on custom setting(Default_Configuration__c) value trigger will be executed
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            MohawkAccountTeamTriggerHandler.validateDuplicate(trigger.New);
            //MohawkAccountTeamTriggerHandler.createAccountTeam(trigger.New);
            MohawkAccountTeamTriggerHandler.initAccountprofile(trigger.New);
            //Update BMF User Checkbox(Anand, Pwc- 16/Aug/2017)
            MohawkAccountTeamTriggerHandler.updateBMFUserCheckBox(trigger.New); 
            
            //Added by Mohan - 25/01/2018 - US-53354
            MohawkAccountTeamTriggerHandler.updateRecommendation(Trigger.new, null);          
            MohawkAccountTeamTriggerHandler.updateWithAcProfileValuesNewLogic(Trigger.new); //Added by Susmitha on FEB 20,2018 for US 53351 for insert
            //Added by Nagendra - 28-01-2019
            UpdateMainProfile.checkMainProfileCheckBox(Trigger.new);
        }
        
        if(Trigger.isupdate && !MohawkAccountTeamService.updateFromEvent){ //Added condition to check if the update is not from Event - MB - Bug 64410 - 08/20/18
            MohawkAccountTeamTriggerHandler.updateBMFUserCheckBox(trigger.New); 
            // Added By Suresh - 04/17/2018 - Bug 59305
            MohawkAccountTeamTriggerHandler.initAccountprofile(trigger.New);
            
            //MohawkAccountTeamTriggerHandler.updatedAccountTeamMembers(trigger.New,trigger.Oldmap); //Commented by MB - Bug 68923 - 12/13/18 - Will add in after udpate
            //Update Account Profile formulas  with Account Profile data (Anand, Pwc- 18/Aug/2017)
            
            
            MohawkAccountTeamTriggerHandler.updateWithAcProfileValuesNewLogic(Trigger.new); //Added by Susmitha on FEB 20,2018 for US 53351 for Update
            //Class for testing
            //MohawkAccountTeamTriggerHandler_NEW.updateWithAcProfileValuesNewLogic(Trigger.new);
            //MohawkAccountTeamTriggerHandler.createAccountTeam(trigger.New);
            //Update BMF User Checkbox(Anand, Pwc- 16/Aug/2017)
            // MohawkAccountTeamTriggerHandler.updateBMFUserCheckBox(trigger.New);  // for existing records
            
            //Added by Mohan - 25/01/2018 - US-53354
            MohawkAccountTeamTriggerHandler.updateRecommendation(Trigger.new, Trigger.oldMap);
        }
        
        if(Trigger.isDelete) {
            //MohawkAccountTeamTriggerHandler.deleteAccountTeam(trigger.Old); //Commented by MB - Bug 68923 - 12/13/18 - Will add in after delete
            
        }
        
        
        //  if(Trigger.isUpdate) {
        //   //MohawkAccountTeamTriggerHandler.validateDuplicate(trigger.new);
        //   MohawkAccountTeamTriggerHandler.createAccountTeamOnCheckUpdate(trigger.newMap, trigger.oldMap);
        // };
    } 
    
    if(Trigger.isAfter){
        if(Trigger.isInsert) {
            MohawkAccountTeamTriggerHandler.createAccountTeam(trigger.new);
            MohawkAccountTeamTriggerHandler.updateAccountMHKOwner(trigger.NewMap);
        }
        
        //update Account profile's No_Karastan checkbox based on Brands
        if(Trigger.isInsert || (Trigger.isUpdate && !MohawkAccountTeamService.updateFromEvent)){ //Added condition to check if the update is not from Event - MB - Bug 64410 - 08/20/18
            // Multi team sale calculations 
            MohawkAccountTeamTriggerHandler.CalculatetwoSalesTeamSplit(trigger.NewMap, trigger.OldMap);
            
            //MohawkAccountTeamTriggerHandler.updateAccountProfile(Trigger.new); 
        }
        /*By Susmitha for US 50869 ON JAN 10*/
        if(Trigger.isInsert){
            MohawkAccountTeamTriggerHandler.updateMohawkAccountTeamMemberIds(trigger.new, null,TRUE,FALSE,FALSE,FALSE);
        }
        
        if(Trigger.isUpdate && !MohawkAccountTeamService.updateFromEvent){ //Added condition to check if the update is not from Event - MB - Bug 64410 - 08/20/18
            MohawkAccountTeamTriggerHandler.updateMohawkAccountTeamMemberIds(trigger.new, Trigger.oldMap,FALSE,TRUE,FALSE,FALSE);
            MohawkAccountTeamTriggerHandler.updatedAccountTeamMembers(trigger.New,trigger.Oldmap); //Added by MB - Bug 68923 - 12/13/18
        }
        
        if(Trigger.isDelete){
            MohawkAccountTeamTriggerHandler.updateMohawkAccountTeamMemberIds(trigger.old, null,FALSE,FALSE,TRUE,FALSE);
            MohawkAccountTeamTriggerHandler.deleteAccountTeam(trigger.Old); //Added by MB - Bug 68923 - 12/13/18
            OutBoundDeletion.onDeleteMhkAccountTeam(trigger.Old);//Added by - Nagendra 12-20-2018
            UpdateMainProfile.check_MainProfileCheckBox(Trigger.old); //Added by Nagendra - 28-01-2019
        }
        
        if(Trigger.isUnDelete){
            MohawkAccountTeamTriggerHandler.updateMohawkAccountTeamMemberIds(trigger.new, null,FALSE,FALSE,FALSE,TRUE);
        }
        
        /*End of Code By Susmitha for US 50869 ON JAN 10*/
        
    }
    // if(Trigger.isInsert){
    //     MohawkAccountTeamTriggerHandler.createAccountTeam(trigger.newap);
    // }
    // if(Trigger.isUpdate) {
    //   MohawkAccountTeamTriggerHandler.createAccountTeamOnUpdate(trigger.newMap, trigger.oldMap);
    // }
    //} 
    //}
}