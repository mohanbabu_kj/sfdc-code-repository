/**************************************************************************

Name : CPLSalesTrigger

===========================================================================
Purpose : This class is used for Account Trigger.
===========================================================================

History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Nick W         2/Feb/2018     Created
***************************************************************************/

trigger CPLSalesTrigger on CPL_Sales__c (after insert, after update){
    CPLSalesTriggerHandler handler = new CPLSalesTriggerHandler();

    if(UtilityCls.isTriggerExecute('CPL_Sales__c')  || Test.isRunningTest()){
        /*if(Trigger.isBefore){
            if(Trigger.isInsert || Trigger.isUpdate){
                CPLSalesTriggerHandler.setPriceGrid(Trigger.new);
            }
        }*/

        if(Trigger.isAfter){
            if(Trigger.isInsert){
                CPLSalesTriggerHandler.createCPLObjectRelationships(Trigger.newMap);
                CPLSalesTriggerHandler.createAccountCPLSalesRelationships(Trigger.new);
            }
            if(Trigger.isUpdate){
                List<CPL_Sales__c> listNew = New List<CPL_Sales__c>();
                for (CPL_Sales__c cs : Trigger.newMap.Values()){
                    if (cs.Division_Customer_No_SFX_Id__c != null){
                        if (cs.Division_Customer_No_SFX_Id__c != Trigger.oldMap.get(cs.Id).Division_Customer_No_SFX_Id__c){
                            listNew.add(cs);
                        }
                    }
                }
                CPLSalesTriggerHandler.createAccountCPLSalesRelationships(listNew);
            }

        }
    }
}