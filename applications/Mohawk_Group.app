<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Mohawk Group</label>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>ProductsObsolete</tabs>
    <tabs>standard-CollaborationGroup</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>People</tabs>
    <tabs>ApplicationLog__c</tabs>
    <tabs>Territory_Geo_Code__c</tabs>
</CustomApplication>
