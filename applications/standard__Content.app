<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-Workspace</tabs>
    <tabs>standard-ContentSearch</tabs>
    <tabs>standard-ContentSubscriptions</tabs>
    <tabs>ApplicationLog__c</tabs>
    <tabs>Territory_Geo_Code__c</tabs>
</CustomApplication>
