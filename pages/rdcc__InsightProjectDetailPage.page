<apex:page controller="rdcc.InsightSystemIntegration" >
    <style>
        .spinnerBg {
            width: 100%;
            height: 100%;
            position: absolute;
            background-color: #000;
            opacity: 0.2;
            z-index: 999999;
        }
        .spinner {
            width: 100%;
            height: 100%;
            position: absolute;
            background-image: url("/img/loading32.gif");
            background-size: 16px;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
            z-index: 9999999;
            opacity: 1;
        }
        
        
    </style>
     <apex:actionStatus id="spinnerStatus">
            <apex:facet name="start">
                <div class="spinnerBg" />
                <div class="spinner" />
            </apex:facet>
        </apex:actionStatus>
    <apex:sectionHeader title="{!IF(searchType != 'company','Insight Project','Insight Company')}" subtitle="{!IF(searchType != 'company',insProject['Title'],insCompany['Name'])}"/>
    <apex:form >
        <apex:pageMessages id="pgMsg"/>
        <apex:outputPanel rendered="{!IF(searchType != 'company',true,false)}">
            <apex:pageBlock title="Insight Project Detail" mode="maindetail">            
                
                <apex:pageBlockButtons location="top" id="thePBButtons">
                    <apex:commandButton status="spinnerStatus" reRender="pgMsg,abc"  value="Import" action="{!importRecordFromDetailPage}">
                        <apex:param name="ids"  value="{!IF(searchType == 'company',insCompany.CompanyId,insProject.ProjectId)} "/>
                    </apex:commandButton>
                    <apex:commandButton id="abc" value="Open In Salesforce" rendered="{!IsRenderOpenInSFButton}" action="{!openSalesforceRecord}" />
                </apex:pageBlockButtons>
                
                <apex:pageBlockSection title="Information" columns="2" >
                    <apex:repeat value="{!attributes['Information']}" var="f">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="{!f.label}"/>
                            <apex:outputPanel >                            
                                <apex:outputPanel rendered="{!f.type =='CURRENCY'}">
                                    <apex:outputText value="{0, Number, Currency}"><apex:param value="{!insProject[f.value]}" /></apex:outputText>
                                </apex:outputPanel>
                                <apex:outputText rendered="{!OR(f.type =='DATE',f.type =='DATETIME')}" value="{0,date,dd/MM/yyyy}">
                                    <apex:param value="{!insProject[f.value]}" />
                                </apex:outputText>
                                <apex:outputPanel rendered="{!AND(f.type !='CURRENCY',f.type !='DATE',f.type !='DATETIME')}">
                                    <apex:outputText value="{!insProject[f.value]}"/>
                                </apex:outputPanel>
                            </apex:outputPanel>
                        </apex:pageBlockSectionItem>
                    </apex:repeat>
                </apex:pageBlockSection>
                <apex:pageBlockSection title="Address Information" columns="2">
                    <apex:repeat value="{!attributes['Address Information']}" var="f">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="{!f.label}"/>
                            <apex:outputText value="{!insProject['address'][0][f.value]}" />
                        </apex:pageBlockSectionItem>
                    </apex:repeat>
                </apex:pageBlockSection>
                <apex:pageBlockSection title="Additional Information" columns="2">
                    <apex:repeat value="{!attributes['Additional Information']}" var="f">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="{!f.label}"/>
                            <apex:outputPanel >
                                <apex:outputText rendered="{!OR(f.type =='DATE',f.type =='DATETIME')}" value="{0,date,dd/MM/yyyy}">
                                    <apex:param value="{!insProject[f.value]}" />
                                </apex:outputText>
                                
                                <apex:outputText value="{!insProject[f.value]}" rendered="{!AND(f.type !='DATE',f.type !='DATETIME')}" />
                            </apex:outputPanel>
                        </apex:pageBlockSectionItem>
                    </apex:repeat>
                </apex:pageBlockSection>        
            </apex:pageBlock>
            
            <apex:repeat value="{!relatedLists}" var="r">
                <apex:pageBlock title="{!r}" >
                    <apex:pageBlockTable value="{!insProject[relatedLists[r]]}"  var="det" rendered="{!relatedListsSizeMap[relatedLists[r]] > 0}" rows="5">                   
                        <apex:repeat value="{!attributes[r]}" var="f">                          
                            <apex:column headerValue="{!f.label}" >
                                <apex:outputPanel rendered="{!f.type =='CURRENCY'}">
                                    <apex:outputText value="{0, Number, Currency}"><apex:param value="{!det[f.value]}" /></apex:outputText>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!AND(f.type !='CURRENCY',f.type !='DATE',f.type !='DATETIME')}">
                                    <apex:outputText value="{!det[f.value]}"/>
                                </apex:outputPanel>
                                 <apex:outputText rendered="{!OR(f.type =='DATE',f.type =='DATETIME')}" value="{0,date,dd/MM/yyyy}">
                                    <apex:param value="{!det[f.value]}" />
                                </apex:outputText>
                                
                            </apex:column>
                        </apex:repeat>
                    </apex:pageBlockTable>
                    <apex:outputPanel rendered="{!relatedListsSizeMap[relatedLists[r]] > 5}" >
                        <a href="/apex/InsightProjectRelatedListPage?rln={!r}&id={!requiredId}&type=project&mode=cls">Show More>></a> 
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!relatedListsSizeMap[relatedLists[r]]  == 0}" >
                        <apex:outputText value="*No records to display" ></apex:outputText>
                    </apex:outputPanel>
                </apex:pageBlock>
            </apex:repeat>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(searchType == 'company',true,false)}">
            <apex:pageBlock title="Insight Company Detail" mode="maindetail">
                <apex:pageBlockButtons location="top" id="thePBButtons">
                    <apex:commandButton status="spinnerStatus" reRender="pgMsg,abc"  value="Import" action="{!importRecordFromDetailPage}">
                        <apex:param name="ids"  value="{!IF(searchType == 'company',insCompany.CompanyId,insProject.ProjectId)} "/>
                    </apex:commandButton>
                    <apex:commandButton id="abc" value="Open In Salesforce" rendered="{!IsRenderOpenInSFButton}" action="{!openSalesforceRecord}" />
                </apex:pageBlockButtons>
                <apex:pageBlockSection title="Information" columns="2" >
                    <apex:repeat value="{!attributes['Information']}" var="f">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="{!f.label}"/>
                            <apex:outputPanel >
                                <apex:outputText rendered="{!OR(f.type =='DATE',f.type =='DATETIME')}" value="{0,date,dd/MM/yyyy}">
                                    <apex:param value="{!insCompany[f.value]}" />
                                </apex:outputText>
                                
                                <apex:outputText value="{!insCompany[f.value]}" rendered="{!AND(f.type !='DATE',f.type !='DATETIME')}" />
                            </apex:outputPanel>
                            
                        </apex:pageBlockSectionItem>
                    </apex:repeat>
                </apex:pageBlockSection>
                <apex:pageBlockSection title="Address Information" columns="2">
                    <apex:repeat value="{!attributes['Address Information']}" var="f">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="{!f.label}"/>
                            <apex:outputText value="{!insCompany['address'][0][f.value]}" />
                        </apex:pageBlockSectionItem>
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlock>
            
            <apex:pageBlock title="Contacts">
                <apex:pageBlockTable value="{!insCompany['contacts']}"  var="contact"  rows="5" rendered="{!relatedListsSizeMap != null && relatedListsSizeMap['contacts'] != null && relatedListsSizeMap['contacts'] > 0}">
                    <apex:repeat value="{!attributes['Contact Information']}" var="f">
                        <apex:column headerValue="{!f.label}" >
                            <apex:outputPanel rendered="{!f.type =='CURRENCY'}">
                                <apex:outputText value="{0, Number, Currency}"><apex:param value="{!contact[f.value]}" /></apex:outputText>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!f.type !='CURRENCY'}">
                                <apex:outputText value="{!contact[f.value]}"/>
                            </apex:outputPanel>
                        </apex:column>
                    </apex:repeat>
                </apex:pageBlockTable>
                <apex:outputPanel rendered="{!relatedListsSizeMap != null && relatedListsSizeMap['contacts'] != null && relatedListsSizeMap['contacts'] > 5}" >
                    <a href="/apex/InsightProjectRelatedListPage?rln=contacts&id={!requiredId}&type=company&mode=cls">Show More>></a> 
                </apex:outputPanel>
                <apex:outputPanel rendered="{!relatedListsSizeMap != null && relatedListsSizeMap['contacts'] != null && relatedListsSizeMap['contacts']  == 0}" >
                    <apex:outputText value="*No records to display" ></apex:outputText>
                </apex:outputPanel>
            </apex:pageBlock>
        </apex:outputPanel>
    </apex:form>
    
</apex:page>