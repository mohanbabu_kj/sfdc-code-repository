<apex:page standardController="rdcc__Insight_Company__c" extensions="rdcc.converAccount_Ctrl" tabStyle="Account" docType="html-5.0">
    <apex:stylesheet value="{!URLFOR($Resource.rdcc__SLDS202, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.rdcc__ConstructConnect, 'css/converAccount.css')}"/>
    <apex:includeScript value="{!URLFOR($Resource.rdcc__ConstructConnect, 'js/converAccount.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.rdcc__ConstructConnect, 'js/jquery-1.11.1.min.js')}"/>
   
    <script>
    var previousOnload = window.onload;        
    window.onload = function() { 
        if (previousOnload) { 
            previousOnload();
        }
        init();
        
    }
    
    function init(){
        var selectList = document.getElementsByTagName("select");
        if(selectList != null){
            for(var i=0;i<selectList.length;i++){
                if(selectList[i].id.includes("_selected") || selectList[i].id.includes("_unselected")){
                    selectList[i].style += "max-width:200px !important;";
                }                
            }            
        }
        
        var className = "picklistArrowLeft dummyRequired";
        var elements = document.getElementsByClassName("picklistArrowLeft");
        for(var i=0;i<elements.length;i++){
            elements[i].className = className; 
             elements[i].style.height = '20px';
          
        }
        className = "picklistArrowRight dummyRequired";
        elements = document.getElementsByClassName("picklistArrowRight");
        for(var i=0;i<elements.length;i++){
             elements[i].className = className; 
             elements[i].style.height = '20px';
        }
        
        //Set Current Owner Id for Lightning Experirence
        if({!$User.UIThemeDisplayed != 'Theme3'}){
            select('owner', '{!currentUser.Name}', 'undefined', '{!currentUser.Id}');
        } 
    }
    
    function loadSpinner(){
        var elements = document.getElementsByClassName("dummyRequired");
        var loadSpinner = true;
        if(elements != null){
            for(var i=0; i<elements.length;i++){
                if(elements[i].value == null || elements[i].value == ''){
                    loadSpinner = false;
                }
            }
        }
        if(loadSpinner){
            onClick();
        }
    }
    </script>
 
  
  <apex:form rendered="{!$User.UIThemeDisplayed == 'Theme3'}">
     
     <apex:sectionHeader title="Account Edit" subtitle="New Account"/>
     
      <apex:pageMessages />
      <apex:pageBlock title="Account Edit" mode="edit">
          <br/>
          
          <apex:pageBlockTable value="{!duplicateRecords}" var="item" rendered="{!hasDuplicateResult}">
              <apex:column >
                  <apex:facet name="header">Name</apex:facet>
                  <apex:outputLink value="/{!item['Id']}">{!item['Name']}</apex:outputLink>
              </apex:column>
             
              <apex:column >
                  <apex:facet name="header">Owner</apex:facet>
                  <apex:outputField value="{!item['OwnerId']}"/>
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Last Modified Date</apex:facet>
                  <apex:outputField value="{!item['LastModifiedDate']}"/>
              </apex:column>
          </apex:pageBlockTable>
          <br/>
          <apex:pageBlockSection rendered="{!showFutureUpdatePicklist}">
              <apex:pageBlockSectionItem >
                  <apex:outputLabel >Allow Automatic Future Updates to this Account by ConstructConnect?</apex:outputLabel>
                  <apex:outputpanel >
                      <div class="requiredInput">
                          <apex:outputpanel layout="block" styleClass="requiredBlock"/> 
                          <apex:selectList size="1" label="Allow Automatic Future Updates to this Account by ConstructConnect?" value="{!insightUpdatesValue}" id="autoInsightUpdates" required="true">
                              <apex:selectOptions value="{!yesNoPicklistValues}" />
                          </apex:selectList><br/>
                          <apex:message title="Please Enter a value" for="autoInsightUpdates" style="color:#d74c3b;"/>
                      </div>                         
                  </apex:outputpanel>
              </apex:pageBlockSectionItem>
          </apex:pageBlockSection>
          
          <apex:pageBlockSection title="Account Information">   
              <apex:inputField value="{!insComp.name}" required="true"/>
              <apex:outputField label="Insight Company Detail URL" value="{!insComp.rdcc__URL__c}"/>
              <apex:inputField value="{!accTemp.OwnerId}" required="true"/>
              <apex:repeat value="{!accCustomMandatoryFieldMap}" var="mandField">
                  <apex:inputField value="{!accTemp[mandField]}"/>
              </apex:repeat>
          </apex:pageBlockSection>
          
          <apex:pageBlockSection columns="2" collapsible="true" title="Additional Information" rendered="{!IF(additionalAccFields.size>0,true,false)}">
                <apex:repeat value="{!additionalAccFields}" var="f">                    
                    <apex:inputField value="{!accTemp[f]}"   />
                </apex:repeat>
          </apex:pageBlockSection>
          
          <!--<apex:pageBlockSection title="Address Information" columns="1">
              <apex:inputField label="Billing Street" value="{!insComp.Shipping_Street__c}"/>
              <apex:inputField label="Billing City" value="{!insComp.Shipping_City__c}"/>
              <apex:inputField label="Billing State" value="{!insComp.Shipping_State__c}"/>
              <apex:inputField label="Billing Postal Code" value="{!insComp.Shipping_Postal_Code__c}"/>
              <apex:inputField label="Billing Country" value="{!insComp.Shipping_Country__c}"/>
             
          </apex:pageBlockSection>-->

          <apex:pageBlockButtons >
                <apex:commandButton value="Save" action="{!saveAll}"  rendered="{!IF(!hasDuplicateResult ,true,false )}"/>
                 <apex:commandButton value="Save (Ignore Alert)" action="{!forceSaveAccount}" rendered="{!isDuplicateSaveAllowed}"/>
                <apex:commandButton value="Cancel" action="{!doCancel}" immediate="true"/>
            </apex:pageBlockButtons>
      </apex:pageBlock>
  </apex:form>
  
  <apex:outputPanel rendered="{!$User.UIThemeDisplayed != 'Theme3'}">
      <style>
          select{
          
          background-color: #fff ;
          color: #16325c;
          border: 1px solid #d8dde6;
          border-radius: .25rem;
          width: 100%;
          transition: border .1s linear, background-color .1s linear;
          display: inline-block;
          padding: 0 1rem 0 .75rem;
          line-height: 1.875rem;    
          min-height: calc(1.875rem + (1px * 2));
          }
      </style>
  <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!--apex:stylesheet value="{!URLFOR($Resource.rdcc__SLDS202, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" /-->
        
        <apex:form styleClass="slds" id="convertAccount">
            <div class="slds-page-header slds-page-header--object-home slds-fixed-header">
                <div class="slds-grid">
                    <div class="slds-col slds-has-flexi-truncate">
                        <div class="slds-media slds-no-space slds-grow">
                            <div class="slds-media__figure">
                                <span class="slds-icon_container">
                                    <svg class="slds-icon slds-icon-standard-lead" aria-hidden="true">
                                        <use xlink:href="{!URLFOR($Resource.SLDS202, '/assets/icons/standard-sprite/svg/symbols.svg#account')}"></use>
                                    </svg>
                                </span>
                            </div>
                            <div class="slds-media__body">
                                <p class="slds-text-title--caps slds-line-height--reset">Account</p>
                                <h1 class="slds-page-header__title slds-p-right--x-small">
                                    <span class="slds-grid slds-has-flexi-truncate slds-grid--vertical-align-center">
                                        <span class="slds-truncate">New Account</span>
                                        <svg class="slds-button__icon slds-button__icon--right slds-no-flex" aria-hidden="true">
                                            <use xlink:href="{!URLFOR($Resource.SLDS202, '/assets/icons/utility-sprite/svg/symbols.svg#down')}"></use>
                                        </svg>
                                    </span>        
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="slds-col slds-no-flex slds-grid slds-align-top slds-p-bottom--xx-small">
                        <div id="loading" style="display:none">
                    <div id="loading-content">
                        <div role="status" class="slds-spinner slds-spinner--medium" id="spinner">
                            <span class="slds-assistive-text">Loading</span>
                            <div class="slds-spinner__dot-a"></div>
                            <div class="slds-spinner__dot-b"></div>
                        </div>
                    </div>
                </div> 
                        <div class="slds-button-group" role="group">
                            <apex:outputPanel id="buttonGroup">
                            <apex:commandButton style="border-radius: .25rem 0 0 .25rem; !important" value="Save" action="{!saveAll}" onclick="onClick();" oncomplete="overridePageMessages();onComplete();" reRender="pgMsg,dupPan,buttonGroup,buttonGroup1"  rendered="{!IF(!hasDuplicateResult ,true,false )}" styleClass="slds-button slds-button--brand" />
                            <apex:commandButton value="Save (Ignore Alert)" action="{!forceSaveAccount}" onclick="onClick();" oncomplete="overridePageMessages();onComplete();" reRender="pgMsg,dupPan,buttonGroup,buttonGroup1" rendered="{!isDuplicateSaveAllowed}" styleClass="slds-button slds-button--neutral" />
                            <apex:commandButton value="Cancel" action="{!doCancel}" immediate="true" styleClass="slds-button slds-button--neutral" />
                            </apex:outputPanel>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="slds-docked-form-footer">
                <apex:outputPanel id="buttonGroup1">
                <div class="slds-button-group" role="group" style="float: right;margin-right: 23px;">
                    <apex:commandButton value="Save" action="{!saveAll}" onclick="loadSpinner();" oncomplete="overridePageMessages();onComplete();" reRender="pgMsg,dupPan,buttonGroup,buttonGroup1" rendered="{!IF(!hasDuplicateResult ,true,false )}" styleClass="slds-button slds-button--brand" />
                    <apex:commandButton value="Save (Ignore Alert)" action="{!forceSaveAccount}" onclick="loadSpinner();" oncomplete="overridePageMessages();onComplete();" reRender="pgMsg,dupPan,buttonGroup,buttonGroup1" rendered="{!isDuplicateSaveAllowed}" styleClass="slds-button slds-button--neutral" />
                    <apex:commandButton value="Cancel" action="{!doCancel}" immediate="true" styleClass="slds-button slds-button--neutral" />
                </div>
                    </apex:outputPanel>
            </div>
            <div id="msgDiv" style="margin-left:20px;top :70px;position:relative; margin-right:20px;">
                     <apex:pageMessages id="pgMsg"></apex:pageMessages>
                </div>
                   
                 <apex:outputPanel id="dupPan">
                <apex:outputPanel rendered="{!hasDuplicateResult}">
                    
                    <table class="slds-table slds-table--bordered slds-table--cell-buffer" id="dup_table" style="top :70px;position:relative;">
                        <thead>
                            <tr class="slds-text-title--caps">
                                <th scope="col">
                                    <div class="slds-truncate" title="Name"><b>Name</b></div>
                                </th>
                                
                                <!--th scope="col">
                                    <div class="slds-truncate slds-medium-show" title="Phone"><b>Phone</b></div>
                                </th!-->
                                <th scope="col">
                                    <div class="slds-truncate slds-medium-show" title="Owner"><b>Owner</b></div>
                                </th>
                                <th scope="col">
                                    <div class="slds-truncate slds-medium-show" title="Last Modified Date"><b>Last Modified Date</b></div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <apex:repeat value="{!duplicateRecords}" var="item" id="theRepeatrow">
                                <tr>
                                    <th scope="row" data-label="Name">
                                        <div class="slds-truncate" title="{!item['Name']}"><apex:outputLink value="/{!item['Id']}">{!item['Name']}</apex:outputLink></div>
                                    </th>
                                    
                                    <!--td data-label="Email">
                                        <div class="slds-truncate slds-medium-show" title="{!item['Phone']}"><apex:outputField value="{!item['Phone']}"/></div>
                                    </td!-->
                                    <td data-label="Owner">
                                        <div class="slds-truncate slds-medium-show" title="{!item['OwnerId']}"><apex:outputField value="{!item['OwnerId']}"/></div>
                                    </td>
                                    <td data-label="Last Modified Date">
                                        <div class="slds-truncate slds-medium-show" title="{!item['LastModifiedDate']}"><apex:outputField value="{!item['LastModifiedDate']}"/></div>
                                    </td>
                                </tr>
                            </apex:repeat>                    
                        </tbody>
                    </table><br/>
                </apex:outputPanel>
                </apex:outputPanel>
            <div class="slds-panel slds-grid slds-grid--vertical slds-nowrap" style="top :70px;position:relative;">
                <div class="slds-form--stacked slds-grow">
                    <div class="slds-panel__section ">
                        
                         <div class="slds-grid slds-wrap slds-grid--pull-padded" style="display:{!IF(showFutureUpdatePicklist,'flex','none')}">                                
                            <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div style="width:80%;float:left;margin-left:5px;padding:5px" >
                                    
                                      <div class="slds-form-element">
                                          <abbr class="slds-required" title="required">*</abbr>
                                          <label class="slds-form-element__label">Allow Automatic Future Updates to this Account by ConstructConnect?</label>
                                            <apex:outputpanel >
                                            <div class="requiredInput">
                                                <apex:outputpanel layout="block" styleClass="requiredBlock"/> 
                                                <apex:selectList size="1" label="Allow Automatic Future Updates to this Account by ConstructConnect?" value="{!insightUpdatesValue}" styleClass="slds-select" title="Allow Insight Updates" id="autoInsightUpdates" required="true">
                                                    <apex:selectOptions value="{!yesNoPicklistValues}" />
                                                </apex:selectList><br/>
                                                <apex:message title="Please Enter a value" for="autoInsightUpdates" style="color:#d74c3b;"/>
                                            </div> 
                                             </apex:outputpanel> 
                                    </div>
                                </div>
                            </div>
                        </div><br/>
                        
                        
                        
                        <h2 class="slds-section-title--divider">Account Information</h2>
                       
                        
                        <div class="slds-grid slds-wrap slds-grid--pull-padded">                                
                            <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div style="width:80%;float:left;margin-left:5px;padding:5px" >                                    
                                        <div class="slds-form-element">
                                        <abbr class="slds-required" title="required">*</abbr>
                                        <label class="slds-form-element__label">Name</label><br/>
                                        <apex:inputField label="Name" value="{!insComp.name}" styleClass="slds-input" required="true"/> 
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div style="width:80%;float:left;margin-left:5px;padding:5px" >
                                    <div class="slds-form-element">
                                        <label class="slds-form-element__label">Company Detail URL</label>
                                        <apex:outputField value="{!insComp.rdcc__URL__c}" styleClass="slds-input"/>
                                    </div>
                                </div>
                            </div>
                            
                            <apex:inputHidden id="ownerId" value="{!accTemp.OwnerId}"  />
                            <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div style="width:80%;float:left;margin-left:5px;padding:5px" >
                                    <div class="slds-form-element">                                        
                                        <c:SLDSDynamicLookup id="ownerField" SLDSResourceName="{!$Resource.rdcc__SLDS202}"
                                                             ObjectApiName="User" DisplayFieldApiNames="Name" DisplayFieldsPattern="Name"  
                                                             photo="" LabelName="Owner" SetValueToField="" SetIdField="" isRequired="true"/>
                                    </div>
                                </div>
                            </div>
                            
                           
                            <apex:repeat value="{!accCustomMandatoryFieldMap}" var="mandField">
                                <!--apex:inputField value="{!accTemp[mandField]}"/-->
                                <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                    <div style="width:80%;float:left;margin-left:5px;padding:5px" >
                                        <div class="slds-form-element">
                                            <abbr class="slds-required" title="required">*</abbr>
                                            <label class="slds-form-element__label">{!$ObjectType.Account.fields[mandField].Label}</label>
                                            <apex:inputField value="{!accTemp[mandField]}" styleClass="slds-input dummyRequired"/>
                                        </div>
                                    </div>
                                </div>
                            </apex:repeat>
                        </div>
                    </div>
                </div>
            </div>
            
            
             
            
            <div class="slds-panel slds-grid slds-grid--vertical slds-nowrap" style="top :70px;">
                <div class="slds-form--stacked slds-grow" style="margin-bottom:80px; margin-top:70px;">
                    <div class="slds-panel__section " style="display:{!IF(additionalFieldsLight.size>0,'block','none')}">
                        <h2 class="slds-section-title--divider">Additional Information</h2>
                        <div class="slds-grid slds-wrap slds-grid--pull-padded">  
                            <apex:repeat value="{!additionalFieldsLight}" var="f">
                                <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                    <div style="width:80%;float:left;margin-left:5px;padding:5px" >
                                        <div class="slds-form-element">
                                            <div class="slds-form-element__control">                                                    
                                                <div style="display:{!If($ObjectType.Account.fields[f.apiName].type !='reference' || f.apiName == 'RecordTypeId','block','none')}">
                                                    <label class="slds-form-element__label"> {!$ObjectType.Account.fields[f.apiName].label}</label><br/>
                                                    <apex:inputField value="{!accTemp[f.apiName]}"  required="false" styleClass="{!If($ObjectType.Account.fields[f.apiName].type !='boolean','slds-input','')}"/>
                                                    
                                                </div>
                                                <apex:outputPanel rendered="{!$ObjectType.Account.fields[f.apiName].type =='reference'} && f.apiName != 'RecordTypeId'">
                                                    <div style="display:{!If($ObjectType.Account.fields[f.apiName].type =='reference','block','none')}">
                                                        <c:SLDSDynamicLookup id="fieldValue" SLDSResourceName="{!$Resource.rdcc__SLDS202}"
                                                                             ObjectApiName="{!finalObjectMap[f.apiName]}" DisplayFieldApiNames="Name" DisplayFieldsPattern="Name"  
                                                                             photo="" LabelName="{!$ObjectType.Account.fields[f.apiName].label}" SetValueToField="{!accTemp[f.apiName]}" SetIdField="{!f.recordValue}"/>
                                                    </div>
                                                </apex:outputPanel>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </apex:repeat>
                            <!--apex:repeat value="{!additionalAccFields}" var="f">                    
                                <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                    <div style="width:80%;float:left;margin-left:5px;padding:5px" >
                                        <div class="slds-form-element">
                                            <label class="slds-form-element__label">{!$ObjectType.Account.fields[f].Label}</label>
                                            <apex:inputField value="{!accTemp[f]}" styleClass="slds-input"/>                                            
                                        </div>
                                    </div>

                                </div>
                            </apex:repeat-->

                        </div>
                    </div>
                </div>
            </div>
            
            
        </apex:form>
    </html>    
   </apex:outputPanel>
</apex:page>