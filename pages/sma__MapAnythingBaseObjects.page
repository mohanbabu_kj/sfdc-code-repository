<apex:page sidebar="false" title="MapAnything Settings" controller="sma.MapAnythingBaseObjects" setup="false" extensions="sma.MARemoteFunctions,sma.MAAdminAJAXResources">
    <apex:slds />
    <script type="text/javascript">
        if (!!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            console.log('detected IE11 ... loading polyfills');
            var head = document.getElementsByTagName('head')[0] || document.body;
            var script = document.createElement('script');
            script.src = "{!URLFOR($Resource.MapAnything, 'js/polyfill.min.js')}";
            head.appendChild(script);
        } else if (!!navigator.userAgent.match(/MSIE 10.0.*/)) {
            window.location = '/apex/Oops?type=UnsupportedBrowser';
        }
        // polyfill remove function for IE
        (function() {
            function remove() { this.parentNode && this.parentNode.removeChild(this); }
            if (!Element.prototype.remove) Element.prototype.remove = remove;
            if (Text && !Text.prototype.remove) Text.prototype.remove = remove;
        })();
    </script>

    <script type='text/javascript'>
        var MARemoting = {
            processAJAXRequest : '{!$RemoteAction.MARemoteFunctions.processAJAXRequest}',
            AdminStartUpAction: '{!$RemoteAction.MAAdminAJAXResources.AdminStartUpAction}'
        }
    </script>

    <style>
        .backdrop {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.4);
            visibility: hidden;
            opacity: 0;
            -webkit-transition: 0.1s opacity linear;
            transition: 0.1s opacity linear;
        }
        .backdrop.visible {
        	visibility: visible;
        }
        .backdrop.active {
        	opacity: 1;
        }
        .slds-scope .slds-spinner {
            position: fixed;
        }
        .slds-notify_toast.ma-toast {
            position: fixed;
            top: 50%;
        }
        .slds-combobox_container, .editname, #mapit-proximity-radius, .color.markercolor {
            width: 300px !important;
        }
    </style> 
    
    <script type='text/javascript' src="{!URLFOR($Resource.MapAnything, 'js/jquery-1.7.1.min.js')}"></script>
    <script type="text/javascript" src="{!URLFOR($Resource.MapAnything, 'toastr/toastr.min.js')}"></script>
    <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.MapAnything, 'toastr/toastr.min.css')}"></link>
    <!-- JS Color -->
    <apex:includeScript value="{!URLFOR($Resource.sma__MapAnything, 'jscolor/jscolor.js')}"/>
    <c:MA />
    <script>
        var baseURL = '{!BaseURL}';
        function getProperty(obj, prop, removeWorkspace) {
            prop = prop || '';
            if(removeWorkspace !== false)
            {
                //needed when working in our packaging org(s)
                if ( MA.Namespace == 'sma')
                {
                    obj = MA.Util.removeNamespace(obj,'sma__');

                    //remove from string prop as well
                    prop = prop.replace(/sma__/g,'');
                }
            }
            var arr = prop.split(".");
            while(arr.length && (obj = obj[arr.shift()]));
            return obj;
        }
        function showModal (modalId) {
            $('#' + modalId + '').addClass('in');
            $('#' + modalId + ' .slds-scope .slds-modal').addClass('slds-fade-in-open');
            /*if ( $($Id).has('.ma-modal-search-input') ) {
            $($Id).find('.ma-modal-search-input').focus();
            } else {}*/
            if ($('.backdrop').length == 0) {
                $('<div class="backdrop"></div>').appendTo('body');
            }
            $('.backdrop').addClass('active visible');
        }
        function showErrors(html) {
            hideModal();
            var popup = MA.Popup.showMAAlert({
                header: 'Field Erros',
                template: html
            });
            popup.then(function() {
                showModal('dialog-createfields');
            });
        }
        function hideModal(modalSelector, hideMask) {
            hideMask = hideMask === false ? false : true;
            if (modalSelector != undefined) {
                //$('#'+modalSelector+'').removeClass('in');
                $('#' + modalSelector + ' .slds-scope .slds-modal').removeClass('slds-fade-in-open');
            } else {
                //hide all modals
                //$('.ma-modal').removeClass('in');
                $('.maModal .slds-scope .slds-modal').removeClass('slds-fade-in-open');
            }

            if (hideMask) {
                $('#modalScreen').removeClass('in');
                $('.backdrop').removeClass('active visible');
            }
        }

        toastr.options = {
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "iconClasses" : {
                "success" : "toast-success",
                "error"   : "toast-error",
                "warning" : "toast-warning",
                "info"    : "toast-info"
            }
        };
        var MAToastMessages = {
            showMessage : function(options,type) {
                type = type || 'info';
                options = $.extend({
                    message : '',
                    subMessage : '',
                    timeOut : 3000,
                    extendedTimeOut : 1000,
                    position : 'toast-bottom-right',
                    closeButton : false,
                    onclick : null
                }, options || {});

                toastr.options.timeOut = options.timeOut;
                toastr.options.extendedTimeOut = options.extendedTimeOut;
                toastr.options.positionClass = options.position;
                toastr.options.closeButton = options.closeButton;
                toastr.options.onclick = options.onclick;

                if(type === 'loading') {
                    var $message = toastr['info'](options.subMessage,options.message);
                    $message.addClass('ma-toast-loading').removeClass('toast-info');
                    return $message;
                }
                else {
                    return toastr[type](options.subMessage,options.message);
                }


            },
            showSuccess : function(options) {
                return MAToastMessages.showMessage(options,'success');
            },
            showLoading : function(options) {
                return MAToastMessages.showMessage(options,'loading');
            },
            showWarning : function(options) {
                return MAToastMessages.showMessage(options,'warning');
            },
            showError : function(options) {
                return MAToastMessages.showMessage(options,'error');
            },
            hideMessage : function (toast) {
                toastr.clear(toast);
                toast.remove();
                toast = null;
            }
        }
        function CreateBaseObjectFields(options)
        {
            //send requests to create the needed fields
            var partnerURL = MASystem.MergeFields.partnerServerURL280;
            var sessionId = MASystem.MergeFields.Session_Id;
            var fieldRequests = options.fieldRequests;
            var resultMap = {};
            
            $('#dialog-createfields .createablefield-row .createablefield-create').each(function () {
                //grab stored field info
                var fieldData = $(this).closest('.createablefield-row').data('createableField');
                
                //create a field request so we can track it and determine completion
                var fieldRequest = {
                    done: false,
                    error: false,
                    label: fieldData.label,
                    apiName: fieldData.name
                };
                fieldRequests.push(fieldRequest);
                
                //create field
                if ($(this).is(':checked')) {
                    
                    //create json object based on type
                    var JSONData = {
                        sessionid: sessionId, 
                        serverurl: partnerURL,
                        type: fieldData.type,
                        object: options.baseObject,
                        name: fieldData.name,
                        label: fieldData.label,
                    };
                    
                    //add correct options for field creation
                    if(fieldData.type == 'Percent' || fieldData.type == 'Number')
                    {
                        JSONData.precision = fieldData.precision;
                        JSONData.scale = fieldData.scale;
                    }
                    else if (fieldData.type == 'Text')
                    {
                        JSONData.length = fieldData.length;
                    }
                    
                    //send request
                    $.getJSON(
                        "https://product-api-prod.cloudbilt.com/createfield.php?callback=?",
                        JSONData,
                        function (response) {
                            if (response.success) {
                                fieldRequest.id = response.data.result.id;
                            }
                            else {
                                fieldRequest.done = fieldRequest.error = true;
                                fieldRequest.message = response.message || 'Unknown Error.';
                                fieldRequest.apiName = JSONData.name;
                                fieldRequest.label = JSONData.label;
                            }
                        }
                    );
                } else {
                    fieldRequest.done = true;
                }
            });
            //start an interval to check for completion of field creation
            var fieldCreationInterval = setInterval(function () {
                //loop over each field request checking for completion
                var done = true;
                var error = false;
                $.each(fieldRequests, function (index, fieldRequest) {
                    if (!fieldRequest.done) {
                        //not done
                        done = false;
                        //send a request to check status if we have an id
                        if (fieldRequest.id) {
                            $.getJSON(
                                "https://product-api-prod.cloudbilt.com/status.php?callback=?", 
                                { 
                                    sessionid: sessionId, 
                                    serverurl: partnerURL,
                                    id: fieldRequest.id
                                },
                                function(response) {
                                    if (response && response.success) {
                                        if (response.data.result.done) {
                                            if (response.data.result.state == 'Error') {
                                                if (response.data.result.statusCode == 'DUPLICATE_DEVELOPER_NAME') {
                                                    //the field already exists so we'll just use it
                                                    fieldRequest.done = true;
                                                } else {
                                                    fieldRequest.message = response.data.result.message || 'Unknown Error';
                                                    fieldRequest.done = fieldRequest.error = true;
                                                }
                                            } else {
                                                fieldRequest.done = true;	//success
                                            }
                                        }
                                    } else {
                                        fieldRequest.done = fieldRequest.error = true;	//unknown error
                                    }
                                }
                            );
                        }
                    } else if (fieldRequest.error) {
                        error = true;
                    }
                });
                // done?
                if (done) {
                    clearInterval(fieldCreationInterval);
                    if(error) {
                        options.complete({ success: false, data : fieldRequests });
                    } else {
                        options.complete({ success: true, data : fieldRequests });
                    }
                }
            }, 2000);
        }
    </script> 
    <apex:stylesheet value="{!URLFOR($Resource.sma__MapAnythingJS, 'styles/styles.css')}"/>

    <!-- MapAnything Configuration Page Styling -->
    <apex:stylesheet value="{!URLFOR($Resource.sma__MapAnythingJS, 'styles/css/ma-configuration-ui.css')}"/>

    <div class="slds-scope">
        <div class="slds-brand-band slds-brand-band_cover slds-brand-band_medium slds-p-around_medium">
            <div class="flex-column full-height">
                <!-- Header -->
                <div class="slds-scope">
                    <div class="slds-page-header">
                        <div class="slds-grid">
                            <div class="slds-col slds-has-flexi-truncate">
                                <div class="slds-media slds-no-space slds-grow">
                                    <div class="slds-media__figure ma-slds-media__figure">
                                        <span class="slds-icon ma-icon ma-icon-mapanything"></span>
                                    </div>
                                    <div class="slds-media__body">
                                        <p class="slds-text-title--caps slds-line-height--reset">MapAnything</p>
                                        <h1 class="slds-page-header__title slds-m-right--small slds-align-middle slds-truncate" title="this should match the Record Title">Base Object Configuration</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 

                <div class="flex-row flex-grow-1">
                    <div class="flex-shrink-0"> 
                        <!--navigation-->
                        <c:MAAdminHeader activeTab="baseObjects"/>
                    </div>
                    <div class="flex-grow-1">
                        <div id="ma-vue-root"></div>
                        <link type="text/css" rel="stylesheet" href="{!resourcePaths['MapAnythingJS']}/dist/admin/base-objects/styles.css" />
                        <script type="text/javascript" src="{!resourcePaths['MapAnythingJS']}/dist/admin/base-objects/bundle.js"></script>
                    </div>
                </div>
            </div>
            <!-- Create Fields Popup -->
            <div class="maModal" id="dialog-createfields">
                <div class="slds-scope">
                    <section aria-describedby="modal-content-id-1" aria-labelledby="modal-heading-01" class="slds-modal" role="dialog" tabindex="-1">
                        <div class="slds-modal__container">
                            <div class="loadingMask slds-spinner_container" style="display: none;height: 60%;margin-top: 20%;">
                                <div role="status" class="slds-spinner slds-spinner--medium slds-spinner--brand">
                                    <span class="slds-assistive-text">Loading</span>
                                    <div class="slds-spinner__dot-a"></div>
                                    <div class="slds-spinner__dot-b"></div>
                                </div>
                            </div>
                            <header class="slds-modal__header">
                                <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse js-close-this-slds-modal" onclick="hideModal();" title="Close">
                                    <span aria-hidden="true" class="slds-button__icon slds-button__icon--large ma-icon ma-icon-close"></span>
                                    <span class="slds-assistive-text">Close</span>
                                </button>
                                <h2 class="slds-text-heading--medium slds-hyphenate maPopup-title" id="modal-heading-01">{!$Label.MA_Create_Fields}</h2>
                            </header>

                            <div class="slds-modal__content slds-p-around_medium">
                                <!-- Field Table -->
                                <div id='dialog-createfields-table-wrapper' class='loadmask-wrapper'>
                                    <table id='dialog-createfields-table' class='slds-table slds-table_bordered'>
                                        <thead>
                                            <tr class="slds-text-title_caps">
                                                <th><div class="slds-truncate" title="Field">{!$Label.MA_Field}</div></th>
                                                <th style='text-align: center;'><button class='slds-button selectall'>{!$Label.MA_All}</button> |<button class='slds-button selectnone'>{!$Label.MA_None}</button></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <footer class="slds-modal__footer">
                                <div class="float-right footer-buttons customDis-buttons">
                                    <button class="slds-button slds-button_neutral customDis-cancel" onclick="hideModal();">Cancel</button>
                                    <button class="slds-button slds-button_brand createFields">{!$Label.MA_Create}</button>
                                </div>
                            </footer>
                        </div>
                    </section>
                </div>
            </div>
            <div id="templates" style="display:none;">
                <table>
                    <tr class='createablefield-row'>
                        <td class='createablefield-label'></td>
                        <td style="display:none; text-align: center;" class='createablefield-exists'>
                            <span class="slds-icon ma-icon-check"></span>
                        </td>
                        <td style="display:none; text-align: center;" class='createablefield-wrap'>
                            <span class="slds-checkbox">
                                <input class="createablefield-create" type="checkbox" checked="true"/>
                                <label class="slds-checkbox__label">
                                    <span class="slds-checkbox_faux"></span>
                                </label>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
</apex:page>