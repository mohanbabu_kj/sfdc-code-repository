<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This holds the working hours for users</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>mamd__Days_of_Week__c</fullName>
        <deprecated>false</deprecated>
        <description>These are the days of the week that the working hours are valid for</description>
        <externalId>false</externalId>
        <label>Day(s) of Week</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Days_of_Week</valueSetName>
        </valueSet>
        <visibleLines>7</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Default__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This is used to determine the default settings</description>
        <externalId>false</externalId>
        <label>Default</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>mamd__End_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the end date for a working hours exception</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>mamd__End_Time__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the end time of the working hours</description>
        <externalId>false</externalId>
        <label>End Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Frequency_Types__c</fullName>
        <deprecated>false</deprecated>
        <description>These are the frequency types that the working hours apply to</description>
        <externalId>false</externalId>
        <label>Frequency Type(s)</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Frequency_Types</valueSetName>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Lunch_Break_End__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the end of the lunch break</description>
        <externalId>false</externalId>
        <label>Lunch Break End</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Lunch_Break_Start__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the start of the lunch break</description>
        <externalId>false</externalId>
        <label>Lunch Break Start</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Multiday_Route_Template_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This is a lookup to the route template user</description>
        <externalId>false</externalId>
        <label>*DEPRECATED* MA Guide Template User</label>
        <referenceTo>mamd__Multiday_Route_Template_User__c</referenceTo>
        <relationshipLabel>MapAnything Guide User Working Hours</relationshipLabel>
        <relationshipName>Multiday_User_Working_Hours</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__Multiday_Route_Template__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This is a lookup to the route template</description>
        <externalId>false</externalId>
        <label>*DEPRECATED* MA Guide Route Template</label>
        <referenceTo>mamd__Multiday_Route_Template__c</referenceTo>
        <relationshipLabel>MapAnything Guide User Working Hours</relationshipLabel>
        <relationshipName>Multiday_User_Working_Hours</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the start date for a working hours exception</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>mamd__Start_Time__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the start time of the working hours</description>
        <externalId>false</externalId>
        <label>Start Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Determines how the working hours will be used</description>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Weekly</fullName>
                    <default>false</default>
                    <label>Weekly</label>
                </value>
                <value>
                    <fullName>Exception</fullName>
                    <default>false</default>
                    <label>Exception</label>
                </value>
                <value>
                    <fullName>Non-Working Day</fullName>
                    <default>false</default>
                    <label>Non-Working Day</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__User_End_Point_End__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>User End Point End</label>
        <referenceTo>mamd__Multiday_User_End_Point__c</referenceTo>
        <relationshipName>Multiday_User_Working_Hours1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__User_End_Point_Start__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to the MultidayUser End Point object</description>
        <externalId>false</externalId>
        <label>User End Point Start</label>
        <referenceTo>mamd__Multiday_User_End_Point__c</referenceTo>
        <relationshipName>Multiday_User_Working_Hours</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Multiday_User_Working_Hours</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>MapAnything Guide User Working Hours</label>
    <nameField>
        <label>MapAnything Planner User Working Hours N</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>MapAnything Guide User Working Hours</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
