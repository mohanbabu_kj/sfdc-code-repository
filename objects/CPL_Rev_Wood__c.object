<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Customer Price List object for Rev Wood</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Chain_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Chain_Number__c</formula>
        <label>Account Chain Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>CPL Rev Wood</relationshipLabel>
        <relationshipName>CPL_Rev_Wood</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Backing__c</fullName>
        <externalId>false</externalId>
        <label>Backing</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Billing_Price__c</fullName>
        <externalId>false</externalId>
        <label>Billing Price</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billing_Vs_Net_Price_Indicator__c</fullName>
        <externalId>false</externalId>
        <formula>if (Billing_Price__c != Net_Price__c, &apos;$&apos;,&apos;&apos;)</formula>
        <label>Billing Vs. Net Price Indicator</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Brand_Code__c</fullName>
        <externalId>false</externalId>
        <label>Brand Code</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Buying_Group_Price__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Buying Group Price</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>CPLSales_External_ID__c</fullName>
        <externalId>true</externalId>
        <label>CPL Sales External ID</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPL_Price_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>CPL Price Id</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>CPL_Product_Territory_External_Id__c</fullName>
        <externalId>true</externalId>
        <label>CPL Product Territory External Id</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPL_Product_Territory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>CPL Product Territory</label>
        <referenceTo>Account_Product_Territory_Relationship__c</referenceTo>
        <relationshipLabel>Rev Wood</relationshipLabel>
        <relationshipName>Rev_Wood</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPL_Sales__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>CPL Sales</label>
        <referenceTo>CPL_Sales__c</referenceTo>
        <relationshipLabel>CPL Rev Wood</relationshipLabel>
        <relationshipName>CPL_Rev_Wood</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <externalId>false</externalId>
        <label>Company</label>
        <length>1</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Currency__c</fullName>
        <externalId>false</externalId>
        <label>Currency</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>District_Code__c</fullName>
        <externalId>false</externalId>
        <formula>CPL_Product_Territory__r.District_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>District Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>District_Description__c</fullName>
        <externalId>false</externalId>
        <formula>CPL_Product_Territory__r.District_Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>District Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ERP_Product_Type__c</fullName>
        <externalId>false</externalId>
        <label>ERP Product Type</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Effective_Date_Concat__c</fullName>
        <externalId>false</externalId>
        <formula>IF(YEAR(Start_Date__c)==4000, &quot;&quot;, 

TEXT(MONTH(Start_Date__c)) &amp; &quot;/&quot; 
&amp; TEXT(DAY(Start_Date__c))&amp; &quot;/&quot; 
&amp; RIGHT(TEXT(YEAR(Start_Date__c)), 2)) 
&amp; &quot; - &quot; &amp; 

IF(YEAR(End_Date__c)==4000, &quot;&quot;, 
TEXT(MONTH(End_Date__c)) &amp; &quot;/&quot; 
&amp; TEXT(DAY(End_Date__c))&amp; &quot;/&quot; 
&amp; RIGHT(TEXT(YEAR(End_Date__c)), 2))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Effective Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Group_Price_Indicator__c</fullName>
        <externalId>false</externalId>
        <formula>if (Buying_Group_Price__c=true,&apos;G&apos;,&apos;&apos;)</formula>
        <label>Group Price Indicator</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Indicators__c</fullName>
        <externalId>false</externalId>
        <label>Indicators</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inherited_Price_Indicator__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Inherited Price Indicator</inlineHelpText>
        <label>Inherited Price Indicator</label>
        <length>1</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inventory_Style_Concat__c</fullName>
        <externalId>false</externalId>
        <formula>Product__r.Inventory_Style_Name__c &amp; &quot; (&quot; &amp;  Product__r.Inventory_Style_Num__c &amp; &quot;)&quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Inventory Style</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Accessory__c</fullName>
        <description>59870 - Hide &quot;View Accessory&quot; link for Accessory Products from RevWood, Solid Wood, TecWood, and Resilient Tile &amp; Plank Grids</description>
        <externalId>false</externalId>
        <formula>NOT(ISBLANK(Product__r.Accessory_Identifier_Code__c))</formula>
        <inlineHelpText>Checked if the related Product is an Accessory</inlineHelpText>
        <label>Is Accessory</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Current_Price__c</fullName>
        <externalId>false</externalId>
        <formula>Start_Date__c &lt;= TODAY() &amp;&amp; TODAY() &lt;= End_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Current Price</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Limited_Time_Yes_No__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Limited_Time__c,&apos;Yes&apos;,&apos;No&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Limited Time Yes/No</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Limited_Time__c</fullName>
        <externalId>false</externalId>
        <formula>If( End_Date__c = DATE(4000,12,31), False, True)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Limited Time</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Local_Stock__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Local Stock</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Min_Order_Qty_Num__c</fullName>
        <externalId>false</externalId>
        <label>Min Order Qty #</label>
        <precision>9</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Min_Order_Qty__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Min Order Qty</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Minimum_Quantity_Indicator__c</fullName>
        <externalId>false</externalId>
        <formula>if (Min_Order_Qty__c=true,&apos;Q&apos;,&apos;&apos;)</formula>
        <label>Minimum Quantity Indicator</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Net_Price__c</fullName>
        <externalId>false</externalId>
        <label>Net Price</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Order_Warehouse__c</fullName>
        <externalId>false</externalId>
        <label>Order Warehouse</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Price_Grid_External_Id__c</fullName>
        <externalId>false</externalId>
        <formula>if ( CPL_Product_Territory__r.Region_Code__c != &apos;&apos; &amp;&amp; CPL_Product_Territory__r.Zone_Code__c != &apos;&apos;, 
if ( Product__r.Independent_Aligned_Buying_Group__c != &apos;&apos;, 
$Label.RegionPreFix + CPL_Product_Territory__r.Region_Code__c + &apos;.&apos; + $Label.RegionPreFix + CPL_Product_Territory__r.Region_Code__c + &apos;.&apos; + Product_Unique_Key__c 
, 
$Label.RegionPreFix + CPL_Product_Territory__r.Region_Code__c + &apos;.&apos; + CPL_Product_Territory__r.Zone_Code__c + &apos;.&apos; + Product_Unique_Key__c ) 
,&apos;&apos;)</formula>
        <label>Price Grid External Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Price_Level__c</fullName>
        <externalId>false</externalId>
        <label>Price Level</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Price_Levels</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Price_Load__c</fullName>
        <externalId>false</externalId>
        <formula>if ( Billing_Price__c !=  Net_Price__c , &apos;True&apos;, &apos;&apos;)</formula>
        <label>Price Load</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Pricing_UOM__c</fullName>
        <externalId>false</externalId>
        <label>Pricing UOM</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Unique_Key__c</fullName>
        <externalId>true</externalId>
        <label>Product Unique Key</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>CPL Rev Wood</relationshipLabel>
        <relationshipName>CPL_Rev_Wood</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Promo_Price__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Promo Price</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Region_Code__c</fullName>
        <externalId>false</externalId>
        <formula>CPL_Product_Territory__r.Region_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Region Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Region_Description__c</fullName>
        <externalId>false</externalId>
        <formula>CPL_Product_Territory__r.Region_Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Region Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Selling_Style_Concat__c</fullName>
        <externalId>false</externalId>
        <formula>Product__r.Name &amp; &quot; (&quot; &amp; Selling_Style_Num__c &amp; &quot;)&quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Selling Style</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Selling_Style_Num__c</fullName>
        <externalId>true</externalId>
        <label>Selling Style #</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Size__c</fullName>
        <externalId>false</externalId>
        <label>Size</label>
        <length>6</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>TM_Number__c</fullName>
        <externalId>false</externalId>
        <formula>CPL_Product_Territory__r.Sales_Group_TM__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TM Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Rev Wood</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>CPRW-{00000000}</displayFormat>
        <label>Rev Wood</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Rev Wood</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
