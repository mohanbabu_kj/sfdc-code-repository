<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>An object to store the Project Company details from Insight Platform.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Insight Companies</relationshipLabel>
        <relationshipName>Insight_Companies</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>rdcc__Company_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>The ID for the company listing in ConstructConnect.</description>
        <externalId>true</externalId>
        <inlineHelpText>The ID for the company listing in ConstructConnect.</inlineHelpText>
        <label>Company ID</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>rdcc__Fax__c</fullName>
        <deprecated>false</deprecated>
        <description>The fax number for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The fax number for the company.</inlineHelpText>
        <label>Fax</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>rdcc__Insight_Participant_Total_Project_Value__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Insight Participant Total Project Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>rdcc__Is_Converted__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Do not show on layout- Internal use only</description>
        <externalId>false</externalId>
        <label>Is Converted (Don&apos;t show on layout)</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>rdcc__Is_Manually_Created__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Is Manually Created</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>rdcc__Last_Updated__c</fullName>
        <deprecated>false</deprecated>
        <description>The date of the most recent update to the company listing.</description>
        <externalId>false</externalId>
        <formula>DATEVALUE(LastModifiedDate)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The date of the most recent update to the company listing.</inlineHelpText>
        <label>Last Updated</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>rdcc__Parent_Role__c</fullName>
        <deprecated>false</deprecated>
        <description>The role played by the parent company, such as building owner or architect.</description>
        <externalId>false</externalId>
        <inlineHelpText>The role played by the parent company, such as building owner or architect.</inlineHelpText>
        <label>Parent Role</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Phone__c</fullName>
        <deprecated>false</deprecated>
        <description>The phone number for the company, such as Architect or General Contractor.</description>
        <externalId>false</externalId>
        <inlineHelpText>The phone number for the company, such as Architect or General Contractor.</inlineHelpText>
        <label>Phone</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>rdcc__Role__c</fullName>
        <deprecated>false</deprecated>
        <description>The role played by the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The role played by the company.</inlineHelpText>
        <label>Role</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Shipping_City__c</fullName>
        <deprecated>false</deprecated>
        <description>The shipping address city for the company, such as Bidders or Engineer.</description>
        <externalId>false</externalId>
        <inlineHelpText>The shipping address city for the company, such as Bidders or Engineer.</inlineHelpText>
        <label>City</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Shipping_Country__c</fullName>
        <deprecated>false</deprecated>
        <description>The shipping address country for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The shipping address country for the company.</inlineHelpText>
        <label>Country</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Shipping_County__c</fullName>
        <deprecated>false</deprecated>
        <description>The shipping address county for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The shipping address county for the company.</inlineHelpText>
        <label>County</label>
        <length>60</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Shipping_Postal_Code__c</fullName>
        <deprecated>false</deprecated>
        <description>The shipping address zip or postal code code for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The shipping address zip or postal code code for the company.</inlineHelpText>
        <label>Postal Code</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Shipping_State__c</fullName>
        <deprecated>false</deprecated>
        <description>The shipping address state for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The shipping address state for the company.</inlineHelpText>
        <label>State/Province</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Shipping_Street__c</fullName>
        <deprecated>false</deprecated>
        <description>The shipping address street for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The shipping address street for the company.</inlineHelpText>
        <label>Street</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Total_Open_Project_Material_Demand_Value__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Open Project Material Demand Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>rdcc__Type__c</fullName>
        <deprecated>false</deprecated>
        <description>The role classification type for the company, such as Accommodations/Hospitality or Utilities</description>
        <externalId>false</externalId>
        <inlineHelpText>The role classification type for the company, such as Accommodations/Hospitality or Utilities</inlineHelpText>
        <label>Type</label>
        <length>32</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__URL__c</fullName>
        <deprecated>false</deprecated>
        <description>The URL for the company listing in ConstructConnect</description>
        <externalId>false</externalId>
        <inlineHelpText>The URL for the company listing in ConstructConnect</inlineHelpText>
        <label>Insight Company Detail URL</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <label>Insight Company</label>
    <listViews>
        <fullName>rdcc__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Insight Companies</pluralLabel>
    <searchLayouts>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>rdcc__URL__c</searchFilterFields>
        <searchFilterFields>rdcc__Last_Updated__c</searchFilterFields>
        <searchFilterFields>rdcc__Parent_Role__c</searchFilterFields>
        <searchFilterFields>rdcc__Role__c</searchFilterFields>
        <searchFilterFields>rdcc__Shipping_City__c</searchFilterFields>
        <searchFilterFields>rdcc__Shipping_Country__c</searchFilterFields>
        <searchFilterFields>rdcc__Shipping_County__c</searchFilterFields>
        <searchFilterFields>rdcc__Shipping_Postal_Code__c</searchFilterFields>
        <searchFilterFields>rdcc__Shipping_State__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Comm_Cannot_Edit_Name</fullName>
        <active>true</active>
        <description>User should not edit the name of the record</description>
        <errorConditionFormula>AND(
OR( 
$User.Profile_Name__c &lt;&gt; &apos;System Administrator&apos;,
$User.Profile_Name__c &lt;&gt; &apos;Integration User&apos;,
$User.Profile_Name__c &lt;&gt; &apos;Commercial Sales Operation&apos;),

ISCHANGED( Name )
)</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>Name cannot be changed.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>rdcc__Create_SF_Account</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Create Account</masterLabel>
        <openType>sidebar</openType>
        <page>rdcc__converAccount</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>rdcc__Material_Demand_Value_Update</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Update Material Demand Value</masterLabel>
        <openType>sidebar</openType>
        <page>rdcc__CompanyMaterialDemandValueUpdate</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
