<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used for Residential Pricing Grids (Price Grid, Buying Group Grid)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Backing__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Selling Back</inlineHelpText>
        <label>Backing</label>
        <length>2</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Buying_Group_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Buying_Group__r.Buying_Group_Number__c</formula>
        <label>Buying Group Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Buying_Group_Price_Concat__c</fullName>
        <description>Buying Group Roll Price / Buying Group Cut Price</description>
        <externalId>false</externalId>
        <formula>&quot;$&quot; &amp;  TEXT(Group_Roll_Price__c) 
&amp; IF (ROUND(Group_Roll_Price__c, 0) == Group_Roll_Price__c, &quot;.00&quot;, IF (ROUND(Group_Roll_Price__c, 1) == Group_Roll_Price__c, &quot;0&quot;, &quot;&quot;))
&amp; &quot; / $&quot; &amp;  TEXT(Group_Cut_Price__c)
&amp; IF (ROUND(Group_Cut_Price__c, 0) == Group_Cut_Price__c, &quot;.00&quot;, IF (ROUND(Group_Cut_Price__c, 1) == Group_Cut_Price__c, &quot;0&quot;, &quot;&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Buying Group Roll Price / Buying Group Cut Price</inlineHelpText>
        <label>Buying Group Price</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Buying_Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Buying Group</label>
        <referenceTo>Buying_Group__c</referenceTo>
        <relationshipLabel>Price Grids</relationshipLabel>
        <relationshipName>Price_Grids</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Color_Num__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Selling Color</inlineHelpText>
        <label>Color #</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Company</inlineHelpText>
        <label>Company</label>
        <length>1</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Container_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Container Price</inlineHelpText>
        <label>Container Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Cross_Over_Style_Concat__c</fullName>
        <externalId>false</externalId>
        <formula>Crossover_Style_Name__c &amp; &quot; (&quot; &amp;  Cross_Over_Style_Num__c &amp; &quot;)&quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cross Over Style</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cross_Over_Style_Num__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Crossover Style Code</inlineHelpText>
        <label>Cross Over Style #</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Crossover_Style_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Crossover Style Name</inlineHelpText>
        <label>Crossover Style Name</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DM_100_to_199_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>DM (100-199 Rolls) Price</inlineHelpText>
        <label>DM (100-199) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DM_1_to_24_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>DM (1-24 Rolls) Price</inlineHelpText>
        <label>DM (1-24) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DM_200_Plus_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>DM (200+ Rolls) Price</inlineHelpText>
        <label>DM (200+) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DM_25_to_50_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>DM (25-50 Rolls) Price</inlineHelpText>
        <label>DM (25-50) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DM_Cut_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>DM Cut Price</inlineHelpText>
        <label>DM Cut Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DM_Price_Concat__c</fullName>
        <description>DM Roll Price / DM Cut Price</description>
        <externalId>false</externalId>
        <formula>&quot;$&quot; &amp; TEXT(DM_Roll_Price__c)
&amp; IF (ROUND(DM_Roll_Price__c, 0) == DM_Roll_Price__c, &quot;.00&quot;, IF (ROUND(DM_Roll_Price__c, 1) == DM_Roll_Price__c, &quot;0&quot;, &quot;&quot;))
&amp; &quot; / $&quot; &amp;TEXT(DM_Cut_Price__c)
&amp; IF (ROUND(DM_Cut_Price__c, 0) == DM_Cut_Price__c, &quot;.00&quot;, IF (ROUND(DM_Cut_Price__c, 1) == DM_Cut_Price__c, &quot;0&quot;, &quot;&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>DM Roll Price / DM Cut Price</inlineHelpText>
        <label>DM Price</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DM_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>DM Price</inlineHelpText>
        <label>DM Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DM_Roll_Price__c</fullName>
        <externalId>false</externalId>
        <label>DM Roll Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Effective_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Effective Date</inlineHelpText>
        <label>Effective Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Grid_Type__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Grid Type</inlineHelpText>
        <label>Grid Type</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>MBP</fullName>
                    <default>false</default>
                    <label>MBP</label>
                </value>
                <value>
                    <fullName>BGP</fullName>
                    <default>false</default>
                    <label>BGP</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Group_Cut_Price__c</fullName>
        <externalId>false</externalId>
        <label>Group Cut Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Group_Price__c</fullName>
        <externalId>false</externalId>
        <label>Group Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Group_Roll_Price__c</fullName>
        <externalId>false</externalId>
        <label>Group Roll Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Import_External_Id__c</fullName>
        <externalId>true</externalId>
        <inlineHelpText>Price Id</inlineHelpText>
        <label>Import_External_Id</label>
        <length>40</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inventory_Style_Concat__c</fullName>
        <externalId>false</externalId>
        <formula>Product__r.Inventory_Style_Name__c &amp; &quot; (&quot; &amp; Inventory_Style_Num__c &amp; &quot;)&quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Inventory Style</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inventory_Style_Num__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Inventory Style Code</inlineHelpText>
        <label>Inventory Style #</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Local_Stock__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Local Stock</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Master_Style_Concat__c</fullName>
        <externalId>false</externalId>
        <formula>Master_Style_Name__c &amp; &quot; (&quot; &amp;Master_Style_Num__c &amp; &quot;)&quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Master Style</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Master_Style_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Master Selling Style Name</inlineHelpText>
        <label>Master Style Name</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Master_Style_Num__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Master Selling Style Code</inlineHelpText>
        <label>Master Style #</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Over_Bill_Cut__c</fullName>
        <externalId>false</externalId>
        <label>Over Bill Cut</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Over_Bill_Roll__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Over Bill</inlineHelpText>
        <label>Over Bill Roll</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Over_Bill__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Over Bill</inlineHelpText>
        <label>Over Bill</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Overbill_Concat__c</fullName>
        <description>Overbill Roll Price / Overbill Cut Price</description>
        <externalId>false</externalId>
        <formula>&quot;$&quot; &amp; TEXT( Over_Bill_Roll__c ) 
&amp; IF (ROUND(Over_Bill_Roll__c, 0) == Over_Bill_Roll__c, &quot;.00&quot;, IF (ROUND(Over_Bill_Roll__c, 1) == Over_Bill_Roll__c, &quot;0&quot;, &quot;&quot;)) 
&amp;
&quot; / $&quot; &amp; TEXT ( Over_Bill_Cut__c)
&amp; IF (ROUND(Over_Bill_Cut__c, 0) == Over_Bill_Cut__c, &quot;.00&quot;, IF (ROUND(Over_Bill_Cut__c, 1) == Over_Bill_Cut__c, &quot;0&quot;, &quot;&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Overbill Roll Price / Overbill Cut Price</inlineHelpText>
        <label>Overbill</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Pallet_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Pallet Price</inlineHelpText>
        <label>Pallet Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Price_Grid_Unique_Key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <inlineHelpText>Price Grid Unique Key</inlineHelpText>
        <label>Price Grid Unique Key</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Product_Unique_Key__c</fullName>
        <externalId>true</externalId>
        <inlineHelpText>Product Unique Key</inlineHelpText>
        <label>Product Unique Key</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Product</inlineHelpText>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Price Grids</relationshipLabel>
        <relationshipName>Price_Grids</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RVP_100_to_199_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>RVP (100-199 Rolls) Price</inlineHelpText>
        <label>RVP (100-199) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RVP_1_to_24_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>RVP (1-24 Rolls) Price</inlineHelpText>
        <label>RVP (1-24) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RVP_200_Plus_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>RVP (200+ Rolls) Price</inlineHelpText>
        <label>RVP (200+) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RVP_25_to_50_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>RVP (25-50 Rolls) Price</inlineHelpText>
        <label>RVP (25-50) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RVP_Cut_Price__c</fullName>
        <externalId>false</externalId>
        <label>RVP Cut Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RVP_Price_Concat__c</fullName>
        <description>RVP Roll Price / RVP Cut Price</description>
        <externalId>false</externalId>
        <formula>&quot;$&quot; &amp; TEXT(RVP_Roll_Price__c) 
&amp; IF (ROUND(RVP_Roll_Price__c, 0) == RVP_Roll_Price__c, &quot;.00&quot;, IF (ROUND(RVP_Roll_Price__c, 1) == RVP_Roll_Price__c, &quot;0&quot;, &quot;&quot;)) 
&amp; &quot; / $&quot; &amp;TEXT(RVP_Cut_Price__c) 
&amp; IF (ROUND(RVP_Cut_Price__c, 0) == RVP_Cut_Price__c, &quot;.00&quot;, IF (ROUND(RVP_Cut_Price__c, 1) == RVP_Cut_Price__c, &quot;0&quot;, &quot;&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>RVP Roll Price / RVP Cut Pric</inlineHelpText>
        <label>RVP Price</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RVP_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>RVP Price</inlineHelpText>
        <label>RVP Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RVP_Roll_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>RVP Roll Price</inlineHelpText>
        <label>RVP Roll Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Region__c</fullName>
        <externalId>true</externalId>
        <inlineHelpText>Region Code</inlineHelpText>
        <label>Region</label>
        <length>5</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Salesforce_Product_Category__c</fullName>
        <externalId>false</externalId>
        <formula>Text(Product__r.Salesforce_Product_Category__c)</formula>
        <label>Salesforce Product Category</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Segment__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Segment</inlineHelpText>
        <label>Segment</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Selling_Style_Concat__c</fullName>
        <externalId>false</externalId>
        <formula>Product__r.Name &amp; &quot; (&quot; &amp; Selling_Style_Num__c &amp; &quot;)&quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Selling Style</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Selling_Style_Num__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Selling Style</inlineHelpText>
        <label>Selling Style #</label>
        <length>5</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Size__c</fullName>
        <externalId>false</externalId>
        <label>Size</label>
        <length>6</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TM1_Cut_Price__c</fullName>
        <externalId>false</externalId>
        <label>TM1 Cut Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM1_Price_Concat__c</fullName>
        <description>TM1 Roll Price / TM1 Cut Price</description>
        <externalId>false</externalId>
        <formula>&quot;$&quot; &amp; TEXT(TM1_Roll_Price__c) 
&amp; IF (ROUND(TM1_Roll_Price__c, 0) == TM1_Roll_Price__c, &quot;.00&quot;, IF (ROUND(TM1_Roll_Price__c, 1) == TM1_Roll_Price__c, &quot;0&quot;, &quot;&quot;)) 
&amp; &quot; / $&quot; &amp;TEXT(TM1_Cut_Price__c) 
&amp; IF (ROUND(TM1_Cut_Price__c, 0) == TM1_Cut_Price__c, &quot;.00&quot;, IF (ROUND(TM1_Cut_Price__c, 1) == TM1_Cut_Price__c, &quot;0&quot;, &quot;&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>TM1 Roll Price / TM1 Cut Price</inlineHelpText>
        <label>TM1 Price</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TM1_Roll_Price__c</fullName>
        <externalId>false</externalId>
        <label>TM1 Roll Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM2_Cut_Price__c</fullName>
        <externalId>false</externalId>
        <label>TM2 Cut Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM2_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TM2 Price</inlineHelpText>
        <label>TM2 Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM2_Roll_Price__c</fullName>
        <externalId>false</externalId>
        <label>TM2 Roll Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM3_Cut_Price__c</fullName>
        <externalId>false</externalId>
        <label>TM3 Cut Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM3_Price_Concat__c</fullName>
        <description>TM3 Roll Price / TM3 Cut Price</description>
        <externalId>false</externalId>
        <formula>&quot;$&quot; &amp; TEXT(TM3_Roll_Price__c) 
&amp; IF (ROUND(TM3_Roll_Price__c, 0) == TM3_Roll_Price__c, &quot;.00&quot;, IF (ROUND(TM3_Roll_Price__c, 1) == TM3_Roll_Price__c, &quot;0&quot;, &quot;&quot;)) 
&amp; &quot; / $&quot; &amp;TEXT(TM3_Cut_Price__c) 
&amp; IF (ROUND(TM3_Cut_Price__c, 0) == TM3_Cut_Price__c, &quot;.00&quot;, IF (ROUND(TM3_Cut_Price__c, 1) == TM3_Cut_Price__c, &quot;0&quot;, &quot;&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>TM3 Roll Price / TM3 Cut Price</inlineHelpText>
        <label>TM3 Price</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TM3_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TM3 Price</inlineHelpText>
        <label>TM3 Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM3_Roll_Price__c</fullName>
        <externalId>false</externalId>
        <label>TM3 Roll Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM_100_to_199_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TM (100-199 Rolls) Price</inlineHelpText>
        <label>TM (100-199) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM_1_to_24_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TM (1-24 Rolls) Price</inlineHelpText>
        <label>TM (1-24) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM_200_Plus_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TM (200+ Rolls) Price</inlineHelpText>
        <label>TM (200+) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM_25_to_50_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TM (25-50 Rolls) Price</inlineHelpText>
        <label>TM (25-50) Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TM_2_Price_Concat__c</fullName>
        <description>TM2 Roll Price / TM2 Cut Price</description>
        <externalId>false</externalId>
        <formula>&quot;$&quot; &amp; TEXT(TM2_Roll_Price__c) 
&amp; IF (ROUND(TM2_Roll_Price__c, 0) == TM2_Roll_Price__c, &quot;.00&quot;, IF (ROUND(TM2_Roll_Price__c, 1) == TM2_Roll_Price__c, &quot;0&quot;, &quot;&quot;)) 
&amp; &quot; / $&quot; &amp;TEXT(TM2_Cut_Price__c) 
&amp; IF (ROUND(TM2_Cut_Price__c, 0) == TM2_Cut_Price__c, &quot;.00&quot;, IF (ROUND(TM2_Cut_Price__c, 1) == TM2_Cut_Price__c, &quot;0&quot;, &quot;&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>TM2 Roll Price / TM2 Cut Price</inlineHelpText>
        <label>TM2 Price</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TM_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TM1 Price</inlineHelpText>
        <label>TM1 Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Unit_of_Measure__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Unit of Measure</inlineHelpText>
        <label>Unit of Measure</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Zone__c</fullName>
        <externalId>true</externalId>
        <inlineHelpText>Zone</inlineHelpText>
        <label>Zone</label>
        <length>4</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Price Grid</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Grid_Type__c</columns>
        <columns>Zone__c</columns>
        <columns>Import_External_Id__c</columns>
        <columns>Over_Bill_Roll__c</columns>
        <columns>Group_Roll_Price__c</columns>
        <columns>Price_Grid_Unique_Key__c</columns>
        <columns>Product_Unique_Key__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_0401D</fullName>
        <columns>Price_Grid_Unique_Key__c</columns>
        <columns>NAME</columns>
        <columns>Grid_Type__c</columns>
        <columns>Zone__c</columns>
        <columns>Salesforce_Product_Category__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Salesforce_Product_Category__c</field>
            <operation>equals</operation>
            <value>Residential Broadloom</value>
        </filters>
        <filters>
            <field>Selling_Style_Num__c</field>
            <operation>equals</operation>
            <value>1592P</value>
        </filters>
        <filters>
            <field>Grid_Type__c</field>
            <operation>equals</operation>
            <value>MBP</value>
        </filters>
        <label>All 0401D</label>
    </listViews>
    <listViews>
        <fullName>Buying_Group_Records</fullName>
        <columns>NAME</columns>
        <columns>Buying_Group_Price_Concat__c</columns>
        <columns>Buying_Group__c</columns>
        <columns>Buying_Group_Number__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Grid_Type__c</field>
            <operation>equals</operation>
            <value>BGP</value>
        </filters>
        <label>Buying Group Records</label>
    </listViews>
    <nameField>
        <displayFormat>PG-{00000000}</displayFormat>
        <label>Price Grid Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Price Grids</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
