<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Represents a Grid Type</description>
    <fields>
        <fullName>Column_Font_Size_Mobile__c</fullName>
        <description>Font size of the content within grid columns on Mobile.  Put in values like 10px or 6em</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Font size of the content within grid columns on Mobile.  Put in values like 10px or 6em</inlineHelpText>
        <label>Column Font Size (Mobile)</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Column_Font_Size__c</fullName>
        <description>Font size of the content within grid columns on Desktop/Tablet.  Put in values like 10px or 6em</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Font size of the content within grid columns on Desktop/Tablet.  Put in values like 10px or 6em</inlineHelpText>
        <label>Column Font Size</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Font_Family__c</fullName>
        <description>The font family to use for the grid</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The font family to use for the grid</inlineHelpText>
        <label>Font Family</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Grid_Type__c</fullName>
        <description>The type of grid this corresponds to</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The type of grid this corresponds to</inlineHelpText>
        <label>Grid Type</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Price Grid</fullName>
                    <default>true</default>
                    <label>Price Grid</label>
                </value>
                <value>
                    <fullName>Buying Group Grid</fullName>
                    <default>false</default>
                    <label>Buying Group Grid</label>
                </value>
                <value>
                    <fullName>Customer Price List</fullName>
                    <default>false</default>
                    <label>Customer Price List</label>
                </value>
                <value>
                    <fullName>Merch Vehicle Price List</fullName>
                    <default>false</default>
                    <label>Merch Vehicle Price List</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Header_Font_Size_Mobile__c</fullName>
        <description>Font size of the headers of the grid columns on Mobile.  Put in values like 10px or 6em</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Font size of the headers of the grid columns on Mobile.  Put in values like 10px or 6em</inlineHelpText>
        <label>Header Font Size (Mobile)</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Header_Font_Size__c</fullName>
        <description>Font size of the headers of the grid columns on Desktop/Tablet.  Put in values like 10px or 6em</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Font size of the headers of the grid columns on Desktop/Tablet.  Put in values like 10px or 6em</inlineHelpText>
        <label>Header Font Size</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_Records_per_Page__c</fullName>
        <defaultValue>&quot;25&quot;</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Number of Records per Page</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Row_Height_Tablet__c</fullName>
        <defaultValue>&quot;50&quot;</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Row Height Tablet</label>
        <length>10</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Row_height__c</fullName>
        <defaultValue>&quot;25&quot;</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Row height</label>
        <length>10</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SObject_Name__c</fullName>
        <description>Object to query records from</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Object to query records from</inlineHelpText>
        <label>SObject Name</label>
        <length>50</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Show_Account_Field__c</fullName>
        <defaultValue>false</defaultValue>
        <description>For grids that exist in the context of an Account Id (CPL, MVPL), this should be marked true</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>For grids that exist in the context of an Account Id (CPL, MVPL), this should be marked true</inlineHelpText>
        <label>Show Account Field</label>
        <type>Checkbox</type>
    </fields>
    <label>Grid Type</label>
    <pluralLabel>Grid Types</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
