<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>An object to store the information of deleted records and which will be used to avoid re-import the same records again into Salesforce.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>rdcc__InsightRecordId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>InsightRecordId</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__RecordType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>RecordType</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>ACCOUNT</fullName>
                    <default>false</default>
                    <label>ACCOUNT</label>
                </value>
                <value>
                    <fullName>CONTACT</fullName>
                    <default>false</default>
                    <label>CONTACT</label>
                </value>
                <value>
                    <fullName>PROJECT</fullName>
                    <default>false</default>
                    <label>PROJECT</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>rdcc__SFDCCustomerId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>SFDCCustomerId</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Deleted Record List</label>
    <listViews>
        <fullName>rdcc__Accounts</fullName>
        <columns>NAME</columns>
        <columns>rdcc__RecordType__c</columns>
        <columns>rdcc__InsightRecordId__c</columns>
        <columns>rdcc__SFDCCustomerId__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>rdcc__RecordType__c</field>
            <operation>equals</operation>
            <value>ACCOUNT</value>
        </filters>
        <label>Accounts</label>
    </listViews>
    <listViews>
        <fullName>rdcc__All_Custom</fullName>
        <columns>NAME</columns>
        <columns>rdcc__RecordType__c</columns>
        <columns>rdcc__InsightRecordId__c</columns>
        <columns>rdcc__SFDCCustomerId__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>rdcc__Contacts</fullName>
        <columns>NAME</columns>
        <columns>rdcc__RecordType__c</columns>
        <columns>rdcc__InsightRecordId__c</columns>
        <columns>rdcc__SFDCCustomerId__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>rdcc__RecordType__c</field>
            <operation>equals</operation>
            <value>CONTACT</value>
        </filters>
        <label>Contacts</label>
    </listViews>
    <listViews>
        <fullName>rdcc__Projects</fullName>
        <columns>NAME</columns>
        <columns>rdcc__RecordType__c</columns>
        <columns>rdcc__InsightRecordId__c</columns>
        <columns>rdcc__SFDCCustomerId__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>rdcc__RecordType__c</field>
            <operation>equals</operation>
            <value>PROJECT</value>
        </filters>
        <label>Projects</label>
    </listViews>
    <nameField>
        <label>Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Deleted Record List</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>rdcc__RecordType__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>rdcc__InsightRecordId__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>rdcc__SFDCCustomerId__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OBJECT_ID</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>UPDATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>LAST_UPDATE</customTabListAdditionalFields>
        <listViewButtons>rdcc__Delete</listViewButtons>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>rdcc__Delete</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>page</linkType>
        <masterLabel>Delete</masterLabel>
        <openType>replace</openType>
        <page>rdcc__MassDeletion</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
</CustomObject>
