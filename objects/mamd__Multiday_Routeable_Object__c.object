<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This will hold any multiday defaults needed for an object</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>mamd__Available_Hours_Defaults__c</fullName>
        <deprecated>false</deprecated>
        <description>This holds any defaults needed to determine the available hours of records</description>
        <externalId>false</externalId>
        <label>Available Hours Defaults</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Core_Base_Object_Id__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the id of the base object associated to the routeable object</description>
        <externalId>false</externalId>
        <label>Core Base Object Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_City__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Default City Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_Country__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Default Country Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_End_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the default end date used for promo windows</description>
        <externalId>false</externalId>
        <label>Default End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>mamd__Default_Frequency_Length_Units__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the default frequency length units</description>
        <externalId>false</externalId>
        <label>Default Frequency Length Units</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>weeks</fullName>
                    <default>false</default>
                    <label>weeks</label>
                </value>
                <value>
                    <fullName>months</fullName>
                    <default>false</default>
                    <label>months</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Default_Frequency_Length__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the default value used for frequency length</description>
        <externalId>false</externalId>
        <label>Default Frequency Length</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_Latitude__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Default Latitude Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_Longitude__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Default Longitude Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_Min_Days_Between_Visits__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the default value used for minimum days between visits</description>
        <externalId>false</externalId>
        <label>Default Min Days Between Visits</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_PostalCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Default PostalCode Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the default start date used for promo windows</description>
        <externalId>false</externalId>
        <label>Default Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>mamd__Default_State_Province__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Default State/Province Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_Street__c</fullName>
        <deprecated>false</deprecated>
        <description>leave as a text area; this is used when filtering out available picklist options</description>
        <externalId>false</externalId>
        <label>Default Street Field</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>mamd__Default_Target_Visits__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the default value used for target number of visits</description>
        <externalId>false</externalId>
        <label>Default Target Visits</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Default_Visit_Duration__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the default value used when determining the duration needed for a visit</description>
        <externalId>false</externalId>
        <label>Default Visit Duration</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__End_Date_Field__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the date field to use as the end date for promo windows</description>
        <externalId>false</externalId>
        <label>End Date Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Frequency_Length_Field__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the field used for frequency length</description>
        <externalId>false</externalId>
        <label>Frequency Length Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__IsPromo__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This determines if the routeable object is to be used as a promo window or not</description>
        <externalId>false</externalId>
        <label>Is Promo</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>mamd__Min_Days_Between_Visits_Field__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the field used to get the minimum number of days between visits</description>
        <externalId>false</externalId>
        <label>Min Days Between Visits Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Object__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the api of the object the record belongs to</description>
        <externalId>false</externalId>
        <label>Object</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Promo_Available_For__c</fullName>
        <deprecated>false</deprecated>
        <description>This associates the promo window with the routeable objects it is available for</description>
        <externalId>false</externalId>
        <label>Promo Available For</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Event</fullName>
                    <default>false</default>
                    <label>Event</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Promo_Field_Association__c</fullName>
        <deprecated>false</deprecated>
        <description>This tells us how the promo window object connects to the routeable object</description>
        <externalId>false</externalId>
        <label>Promo Field Association</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Promo_Object__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the api of the object used for promo windows if different from the routeable object&apos;s object</description>
        <externalId>false</externalId>
        <label>Promo Object</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Start_Date_Field__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the date field to use as the start date for promo windows</description>
        <externalId>false</externalId>
        <label>Start Date Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Target_Visits_Field__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the field to use for the target number of visits</description>
        <externalId>false</externalId>
        <label>Target Visits Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>mamd__Tooltip_Field_Apis__c</fullName>
        <deprecated>false</deprecated>
        <description>This is a JSON array of field apis used to create tooltips</description>
        <externalId>false</externalId>
        <label>Tooltip Field Apis</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Visit_Duration_Field__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the field used to determine the visit duration</description>
        <externalId>false</externalId>
        <label>Visit Duration Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>MapAnything Guide Routeable Object</label>
    <nameField>
        <label>MapAnything Planner Routeable Object Nam</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>MapAnything Guide Routeable Objects</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
