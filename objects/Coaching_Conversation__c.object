<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>Coaching_Conversation_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Coaching_Conversation_Compact_Layout</fullName>
        <fields>Subject1__c</fields>
        <label>Coaching Conversation Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Actions_To_Take__c</fullName>
        <externalId>false</externalId>
        <label>Actions To Take</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Assigned_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicates Assigned To user</inlineHelpText>
        <label>Assigned To</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Coaching_Conversations</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Behaviors_Observed__c</fullName>
        <externalId>false</externalId>
        <label>Behaviors Observed</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Due_Date__c</fullName>
        <externalId>false</externalId>
        <label>Due Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <defaultValue>Today()</defaultValue>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
                <value>
                    <fullName>Open</fullName>
                    <default>true</default>
                    <label>Open</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Subject1__c</fullName>
        <externalId>false</externalId>
        <formula>&quot;Coaching - &quot;+ Owner:User.FirstName +&quot; &quot;+ Owner:User.LastName + &quot; &quot; +
TEXT(MONTH(Start_Date__c)) +&quot;/&quot;+ 
TEXT(DAY(Start_Date__c)) +&quot;/&quot;+
TEXT(YEAR(Start_Date__c))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Subject</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic__c</fullName>
        <externalId>false</externalId>
        <label>Topic</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Travel Review</fullName>
                    <default>false</default>
                    <label>Travel Review</label>
                </value>
                <value>
                    <fullName>Business Review</fullName>
                    <default>false</default>
                    <label>Business Review</label>
                </value>
                <value>
                    <fullName>Initiatives Review</fullName>
                    <default>false</default>
                    <label>Initiatives Review</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Coaching Conversation</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Subject1__c</columns>
        <columns>Topic__c</columns>
        <columns>Status__c</columns>
        <columns>Due_Date__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>My_Assigned_Coaching_Conversations</fullName>
        <columns>Subject1__c</columns>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Assigned_To__c</columns>
        <filterScope>Everything</filterScope>
        <label>My Assigned Coaching Conversations</label>
        <sharedTo>
            <role>RES</role>
            <role>Residential_Sales_Ops</role>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>My_Coaching_Conversations</fullName>
        <columns>Subject1__c</columns>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>My Coaching Conversations</label>
        <sharedTo/>
    </listViews>
    <nameField>
        <displayFormat>CC-{00000}</displayFormat>
        <label>Coaching Conversation Number</label>
        <trackFeedHistory>false</trackFeedHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Coaching Conversations</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Subject1__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Topic__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Due_Date__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <validationRules>
        <fullName>Owner_Can_Only_Edit_the_Comments</fullName>
        <active>true</active>
        <errorConditionFormula>if(OwnerId ==  $User.Id &amp;&amp; 
(PRIORVALUE(Name) != Name||
PRIORVALUE(Actions_To_Take__c) != Actions_To_Take__c || 
PRIORVALUE(Behaviors_Observed__c) != Behaviors_Observed__c ||
ISCHANGED(Due_Date__c) ||
ISCHANGED(Start_Date__c) ||
ISCHANGED(Topic__c)
),true, false)</errorConditionFormula>
        <errorMessage>Cannot edit any fields except for Status and Comments</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
