<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>These are the different visit windows for a record</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>VW_Mohawk_Account_Team__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Mohawk Account Team</label>
        <referenceTo>Mohawk_Account_Team__c</referenceTo>
        <relationshipLabel>MapAnything Guide Visit Windows</relationshipLabel>
        <relationshipName>MapAnything_Guide_Visit_Windows</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__Alternate_Visit_Windows_Map__c</fullName>
        <deprecated>false</deprecated>
        <description>This is a JSON map for the non default visit windows</description>
        <externalId>false</externalId>
        <label>Alternate Visit Windows Map</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Days_of_Week__c</fullName>
        <deprecated>false</deprecated>
        <description>These are the days of the week that this window is valid</description>
        <externalId>false</externalId>
        <label>Day(s) of Week</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Days_of_Week</valueSetName>
        </valueSet>
        <visibleLines>7</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Default__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>this field will determine if the saved record is a default</description>
        <externalId>false</externalId>
        <label>Default</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>mamd__End_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The end date that this window is valid for</description>
        <externalId>false</externalId>
        <inlineHelpText>(optional) If the Start Date is populated, then the End Date is required</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>mamd__Frequency_Types__c</fullName>
        <deprecated>false</deprecated>
        <description>These are the different frequency types that the window is valid for</description>
        <externalId>false</externalId>
        <label>Frequency Type(s)</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Frequency_Types</valueSetName>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Repeats_As__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the frequency at which the visit will repeat</description>
        <externalId>false</externalId>
        <label>Repeats As</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Daily</fullName>
                    <default>false</default>
                    <label>Daily</label>
                </value>
                <value>
                    <fullName>Times Through Month</fullName>
                    <default>false</default>
                    <label>Times Through Month</label>
                </value>
                <value>
                    <fullName>Single Day</fullName>
                    <default>false</default>
                    <label>Single Day</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The start date that this window is valid for</description>
        <externalId>false</externalId>
        <inlineHelpText>(optional)</inlineHelpText>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>mamd__Time_Window_End_1__c</fullName>
        <deprecated>false</deprecated>
        <description>end time of first time window option</description>
        <externalId>false</externalId>
        <label>Time Window End 1</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Time_Window_End_2__c</fullName>
        <deprecated>false</deprecated>
        <description>end time of second time window option</description>
        <externalId>false</externalId>
        <inlineHelpText>(optional) Time Window 2 cannot overlap Time Window 1</inlineHelpText>
        <label>Time Window End 2</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Time_Window_Start_1__c</fullName>
        <deprecated>false</deprecated>
        <description>start time of first time window option</description>
        <externalId>false</externalId>
        <label>Time Window Start 1</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Time_Window_Start_2__c</fullName>
        <deprecated>false</deprecated>
        <description>start time of second time window option</description>
        <externalId>false</externalId>
        <inlineHelpText>(optional) Time Window 2 cannot overlap Time Window 1</inlineHelpText>
        <label>Time Window Start 2</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>mamd__Time_Windows</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__Times_Through_Month__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Times Through Month</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1st Time Through</fullName>
                    <default>false</default>
                    <label>1st Time Through</label>
                </value>
                <value>
                    <fullName>2nd Time Through</fullName>
                    <default>false</default>
                    <label>2nd Time Through</label>
                </value>
                <value>
                    <fullName>3rd Time Through</fullName>
                    <default>false</default>
                    <label>3rd Time Through</label>
                </value>
                <value>
                    <fullName>4th Time Through</fullName>
                    <default>false</default>
                    <label>4th Time Through</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>mamd__Type__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the window type</description>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Standard</fullName>
                    <default>false</default>
                    <label>Standard</label>
                </value>
                <value>
                    <fullName>Exception</fullName>
                    <default>false</default>
                    <label>Exception</label>
                </value>
                <value>
                    <fullName>Non-Working Day</fullName>
                    <default>false</default>
                    <label>Non-Working Day</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>mamd__VW_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This is the default lookup to associate the Account</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Multiday_Visit_Windows</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__VW_Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This is the default lookup to associate the Case</description>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipName>Multiday_Visit_Windows</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__VW_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This is the default lookup to associate the Contact</description>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Multiday_Visit_Windows</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__VW_Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This is the default lookup to associate the Lead</description>
        <externalId>false</externalId>
        <label>Lead</label>
        <referenceTo>Lead</referenceTo>
        <relationshipName>Multiday_Visit_Windows</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__VW_Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This is the default lookup to associate the Opportunity</description>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipName>Multiday_Visit_Windows</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mamd__Visit_Window_Strictness__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Visit Window Strictness</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Strict</fullName>
                    <default>false</default>
                    <label>Strict</label>
                </value>
                <value>
                    <fullName>Fluid</fullName>
                    <default>false</default>
                    <label>Fluid</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>MapAnything Guide Visit Windows</label>
    <nameField>
        <label>MapAnything Planner Visit Windows Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>MapAnything Guide Visit Windows</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
