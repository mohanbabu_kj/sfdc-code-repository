<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Notification_to_Sales_Ops</fullName>
        <description>Send Notification to Sales Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>bryan_armstrong@mohawkind.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tyler_payne@mohawkind.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/New_Product_Creation_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Deactivate_Product</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Deactivate Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deactivate Product and Send Email Notification</fullName>
        <actions>
            <name>Send_Notification_to_Sales_Ops</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Deactivate_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>equals</operation>
            <value>Commercial Product,Commercial Competitor,Commercial Display</value>
        </criteriaItems>
        <description>Deactivate Product and Send Email Notification</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
