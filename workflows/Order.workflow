<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Final_Order_Approved_Email_Alert</fullName>
        <description>Final Order Approved Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/Final_Order_Approval_User_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Final_Order_Rejected_Email_Alert</fullName>
        <description>Final Order Rejected Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/Final_Order_Rejected_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Approval_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_With_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status With Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_With_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status With Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Architecture_Folder_Order_False</fullName>
        <field>Architecture_Folder_Order__c</field>
        <literalValue>0</literalValue>
        <name>Update Architecture Folder Order False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unlock_Value_to_False</fullName>
        <field>Unlock__c</field>
        <literalValue>0</literalValue>
        <name>Update Unlock Value to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Order_Approval_Required_False</fullName>
        <description>When the order Approval Process is Rejected this will update the Order Approval Required = false</description>
        <field>Order_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update the Order Approval Required False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Updated Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Approval_Status_to_Recalled</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Updated Approval Status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_the_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Updated the Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_the_Approval_Status_to_SFA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Updated the Approval Status to SFA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_the_Order_Approval_Status_to_SFA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Updated the Order Approval Status to SFA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_to_Unlock_the_Record_to_True</fullName>
        <field>Unlock__c</field>
        <literalValue>1</literalValue>
        <name>Updated to Unlock the Record to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Outbound_Message_to_Hybris_Price_Update</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Outbound Message to Hybris Price Update</description>
        <endpointUrl>https://pubtst.virtualservices.mohawkind.com/SOAT305/PUB.MHK.SalesForce.AccountListener.svc</endpointUrl>
        <fields>AccountId</fields>
        <fields>Approval_Notes__c</fields>
        <fields>Approval_Status__c</fields>
        <fields>Architecture_Folder_Order__c</fields>
        <fields>Carrier_Account_Number__c</fields>
        <fields>ContractId</fields>
        <fields>CreatedDate</fields>
        <fields>Delivery_Date__c</fields>
        <fields>Id</fields>
        <fields>OpportunityId</fields>
        <fields>OrderNumber</fields>
        <fields>Pricing_Comments__c</fields>
        <fields>Project__c</fields>
        <fields>Reason_Code__c</fields>
        <fields>Reason_for_Approval__c</fields>
        <fields>Reason_for_Expedited_Shipping__c</fields>
        <fields>Salesperson__c</fields>
        <fields>Shipping_Options__c</fields>
        <fields>Shipping_Type__c</fields>
        <fields>Status</fields>
        <fields>Tracking_Number__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>Outbound Message to Hybris Price Update</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Order Outbound Message to Hybris</fullName>
        <actions>
            <name>Outbound_Message_to_Hybris_Price_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>When the Status to Approved or Rejected the WF will fire the Outbound Message to Hybris</description>
        <formula>NOT(ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
