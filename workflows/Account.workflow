<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_notify_to_approve_Strategic_Account_approved</fullName>
        <description>Send Email to notify to approve Strategic Account approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/Strategic_Account_has_approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status_Approved</fullName>
        <description>Update Account Approval Status as &quot;Approved&quot;</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Rejected</fullName>
        <description>Update Approval Status as &quot;Rejected&quot;</description>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Sent_for_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Approval Status Sent for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Submitted</fullName>
        <description>Update Approval Status as Submitted</description>
        <field>Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Approval Status Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Billing_City</fullName>
        <description>Copy Billing City</description>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>Copy Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Billing_Country</fullName>
        <description>Copy Billing Country</description>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <name>Copy Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Billing_State</fullName>
        <description>Copy Billing State</description>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>Copy Billing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Billing_Street</fullName>
        <description>Copy Billing Street</description>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Copy Billing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Billing_Zip_Code</fullName>
        <description>Copy Billing Zip Code</description>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>Copy Billing Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDS_Sent_Update</fullName>
        <field>Comments__c</field>
        <formula>&apos;Outbound message triggered&apos;</formula>
        <name>LDS Sent Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Mark_Deactivation_as_True</fullName>
        <description>to Update Account &quot;Mark For Deactivation&quot; field as True</description>
        <field>Marked_For_Deactivation__c</field>
        <literalValue>1</literalValue>
        <name>Update Account Mark Deactivation as True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LDS_Create_Flg</fullName>
        <description>Update LDS Create Flag</description>
        <field>LDS_Create__c</field>
        <literalValue>0</literalValue>
        <name>Update LDS Create Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LDS_Update_flg</fullName>
        <field>LDS_Update__c</field>
        <literalValue>1</literalValue>
        <name>Update LDS update flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Updated</fullName>
        <field>Last_Updated__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update Last Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Updated_By</fullName>
        <field>AF_Last_Updated_By__c</field>
        <formula>LastModifiedBy.FirstName &amp; &quot; &quot; &amp; LastModifiedBy.LastName</formula>
        <name>Update Last Updated By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Status__c</field>
        <literalValue>Deactivated</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_for_Deactivated</fullName>
        <field>Status__c</field>
        <literalValue>Deactivated</literalValue>
        <name>Update Status for Deactivated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Strategic_Account_checkbox_to_T</fullName>
        <field>Strategic_Account__c</field>
        <literalValue>1</literalValue>
        <name>Update Strategic Account checkbox to T</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Account_Non_Invoicing_Outbound_to_Hybris</fullName>
        <apiVersion>39.0</apiVersion>
        <description>This will be used by ESB for any data integration required  for Non-Invoicing Account</description>
        <endpointUrl>https://pubprd.virtualservices.mohawkind.com/SOAP105/PUB.MHK.SalesForce.Listener.svc</endpointUrl>
        <fields>ABC_Classification__c</fields>
        <fields>AccountNumber</fields>
        <fields>AccountSource</fields>
        <fields>Account_Focus__c</fields>
        <fields>Account_Geography__c</fields>
        <fields>Account_Level__c</fields>
        <fields>Account_Owner_Name__c</fields>
        <fields>Account_Prioritization__c</fields>
        <fields>Account_has_existing_agreement__c</fields>
        <fields>AnnualRevenue</fields>
        <fields>Annual_Business_Plan__c</fields>
        <fields>Area_code__c</fields>
        <fields>Bill_To__c</fields>
        <fields>BillingCity</fields>
        <fields>BillingCountry</fields>
        <fields>BillingCountryCode</fields>
        <fields>BillingGeocodeAccuracy</fields>
        <fields>BillingLatitude</fields>
        <fields>BillingLongitude</fields>
        <fields>BillingPostalCode</fields>
        <fields>BillingState</fields>
        <fields>BillingStateCode</fields>
        <fields>BillingStreet</fields>
        <fields>Builder_Flag__c</fields>
        <fields>Builder_Type__c</fields>
        <fields>Business_Type__c</fields>
        <fields>Business_Unit__c</fields>
        <fields>CAMS_Employee_ID_of_Logged_in_User__c</fields>
        <fields>Chain_Name__c</fields>
        <fields>Chain_Number__c</fields>
        <fields>Child_Account_to_Strategic_Account__c</fields>
        <fields>CoStar_Unique_ID__c</fields>
        <fields>Commission_Type_Core_Only__c</fields>
        <fields>Company__c</fields>
        <fields>Competitor_Name__c</fields>
        <fields>Construct_Connect_Unique_ID__c</fields>
        <fields>Contract_End_Date__c</fields>
        <fields>Contract_Scope__c</fields>
        <fields>Contract_Start_date__c</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>Custom_Blanket_Order_Program__c</fields>
        <fields>Customer_Group__c</fields>
        <fields>Customer_Number__c</fields>
        <fields>Customer_Terms__c</fields>
        <fields>Description</fields>
        <fields>Distributor_Brands__c</fields>
        <fields>Distributor_Company__c</fields>
        <fields>Distributor_Product_Types__c</fields>
        <fields>Distributor__c</fields>
        <fields>Division_Number__c</fields>
        <fields>Dodge_Unique_ID__c</fields>
        <fields>Dunn_Bradstreet_Number__c</fields>
        <fields>Est_of_Orders_in_the_next_6_months__c</fields>
        <fields>External_Unique_ID__c</fields>
        <fields>Facilities__c</fields>
        <fields>Facility_type__c</fields>
        <fields>Fax</fields>
        <fields>Freight__c</fields>
        <fields>General_Comments__c</fields>
        <fields>Global_Account_Number__c</fields>
        <fields>HybrisID__c</fields>
        <fields>Id</fields>
        <fields>Industry</fields>
        <fields>Installation_Responsibility__c</fields>
        <fields>Institutional_Hard_Market_Share__c</fields>
        <fields>Institutional_Soft_Market_Share__c</fields>
        <fields>Institutional_Top_Products__c</fields>
        <fields>International_Access_Code__c</fields>
        <fields>IsDeleted</fields>
        <fields>Jigsaw</fields>
        <fields>JigsawCompanyId</fields>
        <fields>LEED_Cert_Products__c</fields>
        <fields>LastActivityDate</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>LastReferencedDate</fields>
        <fields>LastViewedDate</fields>
        <fields>Last_Year_YTD__c</fields>
        <fields>Lifeplace_Hard_Market_Share__c</fields>
        <fields>Lifeplace_Soft_Market_Share__c</fields>
        <fields>Lifeplace_Top_Products__c</fields>
        <fields>List_of_any_DBAs_Brands_Flags__c</fields>
        <fields>Location_Comments__c</fields>
        <fields>Marked_For_Deactivation__c</fields>
        <fields>Market_Segment_Type__c</fields>
        <fields>Market_Segments__c</fields>
        <fields>Marketplace_Brands__c</fields>
        <fields>MasterRecordId</fields>
        <fields>Material_Estimating_Core_Only__c</fields>
        <fields>Maximum_Units_Allowed__c</fields>
        <fields>MohawkXchange_Account__c</fields>
        <fields>Mohawknet_Web_Account__c</fields>
        <fields>Name</fields>
        <fields>No_Guaranteed_Pricing_Acct_Subject_To__c</fields>
        <fields>Notes_about_Sales_Team__c</fields>
        <fields>NumberOfEmployees</fields>
        <fields>Object_Type__c</fields>
        <fields>Opportunity__c</fields>
        <fields>OwnerId</fields>
        <fields>Ownership</fields>
        <fields>PVC_free_options__c</fields>
        <fields>ParentId</fields>
        <fields>Parent_Account_Global_Number__c</fields>
        <fields>Phone</fields>
        <fields>PhotoUrl</fields>
        <fields>Pricing_Comments__c</fields>
        <fields>Pricing_Guaranteed_Until__c</fields>
        <fields>Primary_Contact__c</fields>
        <fields>Product_Information__c</fields>
        <fields>Projection_of_annual_yards_purchased__c</fields>
        <fields>Proprietary_Designs_With_Copyright__c</fields>
        <fields>Purchasing_Plans__c</fields>
        <fields>Rating</fields>
        <fields>RecordTypeId</fields>
        <fields>Regional_Alignment_Builder_Multi_Fami__c</fields>
        <fields>Regional_Alignment_Retail_Hard_Surface__c</fields>
        <fields>Regional_Alignment_Retail_Soft_Surface__c</fields>
        <fields>Reward_Group__c</fields>
        <fields>Role_C__c</fields>
        <fields>Role_R__c</fields>
        <fields>SAP_Account_Number__c</fields>
        <fields>Service_Needs__c</fields>
        <fields>Ship_To__c</fields>
        <fields>ShippingCity</fields>
        <fields>ShippingCountry</fields>
        <fields>ShippingCountryCode</fields>
        <fields>ShippingGeocodeAccuracy</fields>
        <fields>ShippingLatitude</fields>
        <fields>ShippingLongitude</fields>
        <fields>ShippingPostalCode</fields>
        <fields>ShippingState</fields>
        <fields>ShippingStateCode</fields>
        <fields>ShippingStreet</fields>
        <fields>Sic</fields>
        <fields>SicDesc</fields>
        <fields>Site</fields>
        <fields>Sold_Product_Types__c</fields>
        <fields>Special_Services_Comments__c</fields>
        <fields>Status__c</fields>
        <fields>Strategic_Account_Number__c</fields>
        <fields>Strategic_Account__c</fields>
        <fields>Strengths__c</fields>
        <fields>Suffic__c</fields>
        <fields>SystemModstamp</fields>
        <fields>Terms_Discount_Type__c</fields>
        <fields>Territory__c</fields>
        <fields>TickerSymbol</fields>
        <fields>Total_Previous_Years_Revenue__c</fields>
        <fields>Type</fields>
        <fields>User_Profile_Name__c</fields>
        <fields>Weaknesses__c</fields>
        <fields>Website</fields>
        <fields>Workplace_Hard_Market_Share__c</fields>
        <fields>Workplace_Soft_Market_Share__c</fields>
        <fields>Workplace_Top_Products__c</fields>
        <fields>YTD_Sales_Revenue__c</fields>
        <fields>of_Green_Label_Plus_products__c</fields>
        <fields>of_Red_List_Free_Products__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>Account Non Invoicing Outbound to Hybris</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>LDS_Account_Notification_OutBound</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://mohawk-sf.liquidanalytics.com/salesforce/Account</endpointUrl>
        <fields>ABC_Classification__c</fields>
        <fields>Account_Focus__c</fields>
        <fields>CAMS_Account_number__c</fields>
        <fields>CAMS_Employee_ID_of_Logged_in_User__c</fields>
        <fields>CAMS_Employee_Id_of_Owner__c</fields>
        <fields>Created_Date_Time__c</fields>
        <fields>Fax</fields>
        <fields>Id</fields>
        <fields>LDS_Create__c</fields>
        <fields>LDS_Update__c</fields>
        <fields>LastModifiedDate</fields>
        <fields>Market_Segments__c</fields>
        <fields>Name</fields>
        <fields>OwnerId</fields>
        <fields>Phone</fields>
        <fields>Role_C__c</fields>
        <fields>SAP_Account_GUID__c</fields>
        <fields>ShippingCity</fields>
        <fields>ShippingCountryCode</fields>
        <fields>ShippingPostalCode</fields>
        <fields>ShippingStateCode</fields>
        <fields>ShippingStreet</fields>
        <fields>Type</fields>
        <fields>Website</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>LDS_Account_Notification_OutBound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>LDS_Account_Update_Notification_OutBnd</fullName>
        <apiVersion>41.0</apiVersion>
        <endpointUrl>https://mohawk-sf.liquidanalytics.com/salesforce/AccountUpdates</endpointUrl>
        <fields>ABC_Classification__c</fields>
        <fields>AF_Last_Updated_By__c</fields>
        <fields>APAC_SY__c</fields>
        <fields>AccountNumber</fields>
        <fields>AccountSource</fields>
        <fields>Account_Focus__c</fields>
        <fields>Account_Geography__c</fields>
        <fields>Account_Level__c</fields>
        <fields>Account_Owner_Name__c</fields>
        <fields>Account_Prioritization__c</fields>
        <fields>Account_Profile__c</fields>
        <fields>Account_has_existing_agreement__c</fields>
        <fields>Americas_SY__c</fields>
        <fields>AnnualRevenue</fields>
        <fields>Annual_Business_Plan__c</fields>
        <fields>Annual_Closings_Units__c</fields>
        <fields>Annual_Sales__c</fields>
        <fields>Approval_Status__c</fields>
        <fields>Architech_Folder__c</fields>
        <fields>Area_code__c</fields>
        <fields>B2B__c</fields>
        <fields>Bill_To__c</fields>
        <fields>BillingCity</fields>
        <fields>BillingCountry</fields>
        <fields>BillingCountryCode</fields>
        <fields>BillingGeocodeAccuracy</fields>
        <fields>BillingLatitude</fields>
        <fields>BillingLongitude</fields>
        <fields>BillingPostalCode</fields>
        <fields>BillingState</fields>
        <fields>BillingStateCode</fields>
        <fields>BillingStreet</fields>
        <fields>Builder_Flag__c</fields>
        <fields>Builder_Type__c</fields>
        <fields>Business_Type__c</fields>
        <fields>Business_Unit__c</fields>
        <fields>Buying_Group__c</fields>
        <fields>CAMS_Account_number__c</fields>
        <fields>CAMS_Employee_ID_of_Logged_in_User__c</fields>
        <fields>CAMS_Employee_Id_of_Owner__c</fields>
        <fields>Chain_Name__c</fields>
        <fields>Chain_Number__c</fields>
        <fields>Child_Account_to_Strategic_Account__c</fields>
        <fields>City_State__c</fields>
        <fields>CoStar_Unique_ID__c</fields>
        <fields>Comments__c</fields>
        <fields>Commission_Type_Core_Only__c</fields>
        <fields>Company__c</fields>
        <fields>Competitor_Name__c</fields>
        <fields>Construct_Connect_Unique_ID__c</fields>
        <fields>Contract_End_Date__c</fields>
        <fields>Contract_Pricing__c</fields>
        <fields>Contract_Scope__c</fields>
        <fields>Contract_Start_date__c</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>Created_from_LDS__c</fields>
        <fields>Created_from_ODS__c</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>Custom_Blanket_Order_Program__c</fields>
        <fields>Customer_Group_Family__c</fields>
        <fields>Customer_Group__c</fields>
        <fields>Customer_Number__c</fields>
        <fields>Customer_Terms__c</fields>
        <fields>Description</fields>
        <fields>Distributor_Brands__c</fields>
        <fields>Distributor_Company__c</fields>
        <fields>Distributor_Product_Types__c</fields>
        <fields>Distributor__c</fields>
        <fields>Division_Number__c</fields>
        <fields>Dodge_Unique_ID__c</fields>
        <fields>Dunn_Bradstreet_Number__c</fields>
        <fields>EDI__c</fields>
        <fields>EMEAI_SY__c</fields>
        <fields>Est_of_Orders_in_the_next_6_months__c</fields>
        <fields>External_Unique_ID__c</fields>
        <fields>Facilities__c</fields>
        <fields>Facility_type__c</fields>
        <fields>Fax</fields>
        <fields>Forecasted_Revenue__c</fields>
        <fields>Freight__c</fields>
        <fields>GPO_Name__c</fields>
        <fields>General_Comments__c</fields>
        <fields>Global_Account_Number__c</fields>
        <fields>Group_Purchasing_Organization__c</fields>
        <fields>HybrisID__c</fields>
        <fields>Id</fields>
        <fields>Industry</fields>
        <fields>Influencer_Open_Order__c</fields>
        <fields>Installation_Responsibility__c</fields>
        <fields>Institutional_Hard_Market_Share__c</fields>
        <fields>Institutional_Soft_Market_Share__c</fields>
        <fields>Institutional_Top_Products__c</fields>
        <fields>International_Access_Code__c</fields>
        <fields>IsDeleted</fields>
        <fields>Jigsaw</fields>
        <fields>JigsawCompanyId</fields>
        <fields>LDS_Create__c</fields>
        <fields>LDS_Update__c</fields>
        <fields>LEED_Cert_Products__c</fields>
        <fields>LastActivityDate</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>LastReferencedDate</fields>
        <fields>LastViewedDate</fields>
        <fields>Last_Updated__c</fields>
        <fields>Last_Year_YTD__c</fields>
        <fields>Lifeplace_Hard_Market_Share__c</fields>
        <fields>Lifeplace_Soft_Market_Share__c</fields>
        <fields>Lifeplace_Top_Products__c</fields>
        <fields>List_of_any_DBAs_Brands_Flags__c</fields>
        <fields>Location_Comments__c</fields>
        <fields>MVP_Coordinator__c</fields>
        <fields>MVP_Email__c</fields>
        <fields>MVP_Phone__c</fields>
        <fields>Marked_For_Deactivation__c</fields>
        <fields>Market_Segment_R__c</fields>
        <fields>Market_Segment_Type__c</fields>
        <fields>Market_Segments__c</fields>
        <fields>Marketplace_Brands__c</fields>
        <fields>MasterRecordId</fields>
        <fields>Material_Estimating_Core_Only__c</fields>
        <fields>Maximum_Units_Allowed__c</fields>
        <fields>MohawkNet_User__c</fields>
        <fields>MohawkXchange_Account__c</fields>
        <fields>Mohawk_Accoun_Team_Owner__c</fields>
        <fields>Mohawknet_Web_Account__c</fields>
        <fields>Name</fields>
        <fields>No_Guaranteed_Pricing_Acct_Subject_To__c</fields>
        <fields>Notes_about_Sales_Team__c</fields>
        <fields>NumberOfEmployees</fields>
        <fields>Number_of_AE_s__c</fields>
        <fields>Number_of_Sales_Associates__c</fields>
        <fields>Number_of_Selling_Locations__c</fields>
        <fields>Object_Type__c</fields>
        <fields>Open_Orders__c</fields>
        <fields>Opportunity__c</fields>
        <fields>OwnerId</fields>
        <fields>Owner_Details__c</fields>
        <fields>Ownership</fields>
        <fields>PVC_free_options__c</fields>
        <fields>ParentId</fields>
        <fields>Parent_Account_Global_Number__c</fields>
        <fields>Phone</fields>
        <fields>PhotoUrl</fields>
        <fields>Pricing_Comments__c</fields>
        <fields>Pricing_Guaranteed_Until__c</fields>
        <fields>Primary_Contact__c</fields>
        <fields>Product_Information__c</fields>
        <fields>Projection_of_annual_yards_purchased__c</fields>
        <fields>Proprietary_Designs_With_Copyright__c</fields>
        <fields>Purchasing_Plans__c</fields>
        <fields>Rating</fields>
        <fields>RecordTypeId</fields>
        <fields>Regional_Alignment_Builder_Multi_Fami__c</fields>
        <fields>Regional_Alignment_Retail_Hard_Surface__c</fields>
        <fields>Regional_Alignment_Retail_Soft_Surface__c</fields>
        <fields>Reward_Group__c</fields>
        <fields>Role_C__c</fields>
        <fields>Role_R__c</fields>
        <fields>SAP_Account_GUID__c</fields>
        <fields>SAP_Account_Number__c</fields>
        <fields>Sent_to_BI__c</fields>
        <fields>Service_Needs__c</fields>
        <fields>Ship_To__c</fields>
        <fields>ShippingCity</fields>
        <fields>ShippingCountry</fields>
        <fields>ShippingCountryCode</fields>
        <fields>ShippingGeocodeAccuracy</fields>
        <fields>ShippingLatitude</fields>
        <fields>ShippingLongitude</fields>
        <fields>ShippingPostalCode</fields>
        <fields>ShippingState</fields>
        <fields>ShippingStateCode</fields>
        <fields>ShippingStreet</fields>
        <fields>Sic</fields>
        <fields>SicDesc</fields>
        <fields>Site</fields>
        <fields>Sold_Product_Types__c</fields>
        <fields>Special_Services_Comments__c</fields>
        <fields>Status__c</fields>
        <fields>Strategic_Account_Number__c</fields>
        <fields>Strategic_Account__c</fields>
        <fields>Strengths__c</fields>
        <fields>Suffic__c</fields>
        <fields>SystemModstamp</fields>
        <fields>Tableau_Report_URL__c</fields>
        <fields>Terms_Discount_Type__c</fields>
        <fields>Territory__c</fields>
        <fields>TickerSymbol</fields>
        <fields>Total_Account_Chain_Sales__c</fields>
        <fields>Total_Account_Plan_Goal__c</fields>
        <fields>Total_Pipeline__c</fields>
        <fields>Total_Previous_Years_Revenue__c</fields>
        <fields>Total_Project_Pipeline__c</fields>
        <fields>Type</fields>
        <fields>User_Profile_Name__c</fields>
        <fields>Weaknesses__c</fields>
        <fields>Website</fields>
        <fields>Workplace_Hard_Market_Share__c</fields>
        <fields>Workplace_Soft_Market_Share__c</fields>
        <fields>Workplace_Top_Products__c</fields>
        <fields>YTD_Influencer_Sales__c</fields>
        <fields>YTD_Sales_Revenue__c</fields>
        <fields>of_Green_Label_Plus_products__c</fields>
        <fields>of_Red_List_Free_Products__c</fields>
        <fields>sma__MAAssignmentRule__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>LDS_Account_Update_Notification_OutBnd</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Account non-Invoicing  outbound to Hybris</fullName>
        <actions>
            <name>Account_Non_Invoicing_Outbound_to_Hybris</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-Invoicing,Invoicing</value>
        </criteriaItems>
        <description>Account Invoicing/ Non-Invoicing  outbound to Hybris</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Architect Folder Changed</fullName>
        <actions>
            <name>Update_Last_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Updated_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Architect folder is changed update &quot;Last Updated&quot; and &quot;Updated By&quot;</description>
        <formula>ISCHANGED(Architech_Folder__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDS Account Update flag</fullName>
        <actions>
            <name>Update_LDS_Create_Flg</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_LDS_Update_flg</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>LDS Account Update flag</description>
        <formula>AND ( NOT ( AND ( $Profile.Name == &apos;System Administrator&apos;, PRIORVALUE( LDS_Create__c ) != LDS_Create__c , LDS_Create__c = true  ) ),  OR ( AND ( Global_Account_Number__c != null, PRIORVALUE( Global_Account_Number__c ) == Global_Account_Number__c , LDS_Create__c = true, LDS_Update__c = false ),  AND ( Global_Account_Number__c == null, LDS_Create__c = true, (( LastModifiedDate - CreatedDate ) * 1400 &gt; 1) , LDS_Update__c = false )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDS_Account_Create_Reprocess</fullName>
        <actions>
            <name>LDS_Account_Notification_OutBound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-Invoicing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Business_Type__c</field>
            <operation>equals</operation>
            <value>Commercial</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Profile_Name__c</field>
            <operation>equals</operation>
            <value>System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Strategic_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LDS_Create__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LDS_Update__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Re-process the Account Create LDS</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDS_Account_Update_Notifcation_Create</fullName>
        <actions>
            <name>LDS_Account_Notification_OutBound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-Invoicing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Business_Type__c</field>
            <operation>equals</operation>
            <value>Commercial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LDS_Create__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Strategic_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Integration User</value>
        </criteriaItems>
        <description>Send non-invoicing Accounts to LDS for Create</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LDS_Account_Update_Notifcation_Update</fullName>
        <actions>
            <name>LDS_Account_Update_Notification_OutBnd</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-Invoicing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Business_Type__c</field>
            <operation>equals</operation>
            <value>Commercial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LDS_Update__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Strategic_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Integration User</value>
        </criteriaItems>
        <description>Send non-invoicing Accounts to LDS for Update</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead convert to  Account Shipping address</fullName>
        <actions>
            <name>Copy_Billing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Billing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Billing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Billing_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Lead conversion because  it default to &quot;Billing address&quot; instead of &quot;Shipping address&quot;.</description>
        <formula>AND( RecordType.DeveloperName  = &quot;Non_Invoicing&quot;,  ISBLANK(ShippingCity), ISBLANK(ShippingState), ISBLANK(ShippingStreet), ISBLANK(ShippingPostalCode), OR ( NOT(ISBLANK( BillingCity )), NOT(ISBLANK( BillingState )), NOT(ISBLANK( BillingStreet )), NOT(ISBLANK( BillingPostalCode ))) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>To Update Account Record Mark For Deactivation as True</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Marked_For_Deactivation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>if account is not having any update upto 18 months (approx. 547 Days After Account: Last Modified Date). time based workflow will set &quot;Mark For Deactivation&quot; field as checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_Mark_Deactivation_as_True</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.LastModifiedDate</offsetFromField>
            <timeLength>547</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>To Update Account Status when Mark For Deactivation as True</fullName>
        <actions>
            <name>Update_Status_for_Deactivated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Marked_For_Deactivation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Mark for Deactivation is true then update the status as &quot;Deactivated&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Account.LastModifiedDate</offsetFromField>
            <timeLength>547</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
