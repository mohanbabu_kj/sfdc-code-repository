<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Final_Approved_Email_Alert</fullName>
        <description>Final Approved Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/Final_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Final_Rejected_Email_Alert</fullName>
        <description>Final Rejected Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/Final_Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Followup_on_Quotes</fullName>
        <description>Followup on Quotes</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/Quotes_Followup_HTML</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Create_Follow_up_Task_Field</fullName>
        <field>Create_Task__c</field>
        <literalValue>1</literalValue>
        <name>Update Create Follow-up Task Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status</fullName>
        <description>Update Quote to Expired when date passes</description>
        <field>Status</field>
        <literalValue>Expired</literalValue>
        <name>Update Quote Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unlock_Value_to_False</fullName>
        <field>Unlock__c</field>
        <literalValue>0</literalValue>
        <name>Update Unlock Value to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Quote_Approval_Required_False</fullName>
        <description>When the quote Approval Process is Rejected this will update the Quote Approval Required = false</description>
        <field>Quote_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update the Quote Approval Required False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Updated Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Approval_Status_to_Recalled</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Updated Approval Status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Quote_Approval_to_None</fullName>
        <field>Quote_Approval_Level__c</field>
        <name>Updated Quote Approval to None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_the_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Updated the Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_the_Approval_Status_to_SFA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Updated the Approval Status to SFA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_to_Unlock_the_Record_to_True</fullName>
        <field>Unlock__c</field>
        <literalValue>1</literalValue>
        <name>Updated to Unlock the Record to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Outbound_Message_to_Hybris_Price_Update</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Outbound Message to Hybris Price Update</description>
        <endpointUrl>https://pubtst.virtualservices.mohawkind.com/SOAT305/PUB.MHK.SalesForce.AccountListener.svc</endpointUrl>
        <fields>Approval_Notes__c</fields>
        <fields>Id</fields>
        <fields>Product_Manager_1__c</fields>
        <fields>Product_Manager_2__c</fields>
        <fields>Product_Manager_3__c</fields>
        <fields>Product_Manager_4__c</fields>
        <fields>QuoteNumber</fields>
        <fields>Quote_Approval_Required__c</fields>
        <fields>RVP_Approver_User__c</fields>
        <fields>Reason_For_Approval__c</fields>
        <fields>SAM_Approval_Required__c</fields>
        <fields>SVP_Approver_User__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>Outbound Message to Hybris Price Update</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_ProjectId_to_Hybris_by_ExQuoteId</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://pubtst.virtualservices.mohawkind.com/SOAT305/PUB.MHK.SalesForce.AccountListener.svc</endpointUrl>
        <fields>External_Quote_Id__c</fields>
        <fields>Id</fields>
        <fields>OpportunityId</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>Send ProjectId to Hybris by ExQuoteId</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Follow-up Task for Quote</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Quote.ExpirationDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Create_Follow_up_Task_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Quote.ExpirationDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Quote Outbound Message to Hybris</fullName>
        <actions>
            <name>Outbound_Message_to_Hybris_Price_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When the Status to Approved or Rejected the WF will fire the Outbound Message to Hybris</description>
        <formula>NOT(ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote has expired</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Quote.ExpirationDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the expiration date has passed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Quote_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Quote.ExpirationDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>When Record Created From RequestToCreate</fullName>
        <actions>
            <name>Send_ProjectId_to_Hybris_by_ExQuoteId</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Is_Converted_From_RequestToQuote__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>send outbound message to hybris when record created from RequestToQuote object</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
