<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Lead_Record_Type</fullName>
        <description>Update Lead Record Type</description>
        <field>Lead_Record_Type_Hidden__c</field>
        <formula>RecordType.Name</formula>
        <name>Update Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Title</fullName>
        <field>Title</field>
        <formula>TEXT(Title__c)</formula>
        <name>Update Lead Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MohawkGroup_com_User</fullName>
        <description>Update MohawkGroup.com User</description>
        <field>MohawkGroup_User__c</field>
        <literalValue>1</literalValue>
        <name>Update MohawkGroup.com User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Territory_Name</fullName>
        <description>Workflow to update the territory name from lookup field in order to use in Lead sharing rule (criteria based)</description>
        <field>Territory_Name__c</field>
        <formula>Territory__r.Name</formula>
        <name>Update Territory Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Enable_for_Order_checkbox</fullName>
        <description>Update Lead field enable for Order check box to true</description>
        <field>Enable_Order_on_MHKgroup__c</field>
        <literalValue>1</literalValue>
        <name>Update_the_Enable_for_Order_checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Enable order for Mhk group check box true if MHK Group</fullName>
        <actions>
            <name>Update_Lead_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_MohawkGroup_com_User</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_the_Enable_for_Order_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Hide_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>MHK Group</value>
        </criteriaItems>
        <description>Update Enable order for Mhk group check box true if MHK Group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Territory Name</fullName>
        <actions>
            <name>Update_Territory_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update the territory name from lookup field in order to use in Lead sharing rule (criteria based)</description>
        <formula>( ISNULL(PostalCode) = FALSE &amp;&amp; ISNULL(Territory__c) = FALSE) || ISCHANGED( Territory__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
