<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>User_Outbound_message_to_sitecore</fullName>
        <apiVersion>40.0</apiVersion>
        <description>User Outbound message to sitecore</description>
        <endpointUrl>https://pubtst.virtualservices.mohawkind.com/SOAT305/PUB.MHK.SalesForce.AccountListener.svc</endpointUrl>
        <fields>Area_Code__c</fields>
        <fields>CAMS_Employee_Number__c</fields>
        <fields>City</fields>
        <fields>Country</fields>
        <fields>Email</fields>
        <fields>FirstName</fields>
        <fields>FullPhotoUrl</fields>
        <fields>Id</fields>
        <fields>IsActive</fields>
        <fields>LastName</fields>
        <fields>MobilePhone</fields>
        <fields>PostalCode</fields>
        <fields>Segment__c</fields>
        <fields>State</fields>
        <fields>Street</fields>
        <fields>Sub_Business_Unit__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>User Outbound message to sitecore</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Contact Outbound Message to Sitecore</fullName>
        <actions>
            <name>User_Outbound_message_to_sitecore</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.Business_Type__c</field>
            <operation>equals</operation>
            <value>Commercial</value>
        </criteriaItems>
        <description>Send Contact information outbound message to Sitecore</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
