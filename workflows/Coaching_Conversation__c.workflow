<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Coaching_Conversation_Owner_Notification</fullName>
        <description>Coaching Conversation Owner Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/Coaching_Converation_Owner_Notify</template>
    </alerts>
</Workflow>
