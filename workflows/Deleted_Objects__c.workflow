<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Send_Deletion_Notification_to_ESB</fullName>
        <apiVersion>44.0</apiVersion>
        <endpointUrl>https://pubqas.virtualservices.mohawkind.com/SOAQ405/PUB.MHK.SalesForce.Listener.svc</endpointUrl>
        <fields>BI_External_Id__c</fields>
        <fields>CAMS_External_Id__c</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>ESB_Error_in_Processing__c</fields>
        <fields>ESB_picked_Up_At__c</fields>
        <fields>External_Id__c</fields>
        <fields>Hybris_External_Id__c</fields>
        <fields>ID__c</fields>
        <fields>Id</fields>
        <fields>IsDeleted</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>Name</fields>
        <fields>Object_Name__c</fields>
        <fields>OwnerId</fields>
        <fields>Pickup_by_ESB__c</fields>
        <fields>Reprocess__c</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>Send Deletion Notification to ESB</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Sent Notification of Deletion to ESB</fullName>
        <actions>
            <name>Send_Deletion_Notification_to_ESB</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Sent Notification of Deletion to ESB.  the Records in this Object are created by Triggers on Actual Deleted objects</description>
        <formula>(ID__c != null  ||   ID__c != &apos; &apos;) &amp;&amp; (Object_Name__c != null  ||   Object_Name__c != &apos; &apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
