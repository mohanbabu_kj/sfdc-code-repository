<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>LDS_Opp_RelatedAcc_OutBound</fullName>
        <apiVersion>41.0</apiVersion>
        <endpointUrl>https://mohawk-sf.liquidanalytics.com/salesforce/OpportunityRelatedAccount</endpointUrl>
        <fields>Account__c</fields>
        <fields>CAMS_Employee_ID_of_logged_in_User__c</fields>
        <fields>Comment_Migration__c</fields>
        <fields>Contact__c</fields>
        <fields>Id</fields>
        <fields>Opportunity__c</fields>
        <fields>Project_Role__c</fields>
        <fields>Record_type_of_Oppt__c</fields>
        <fields>SAP_Account_Id__c</fields>
        <fields>SAP_Contact_Id__c</fields>
        <fields>SAP_Opportunity_Id__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>LDS_Opp_RelatedAcc_OutBound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>LDS_Opportunity_RelatedAccount_Update_Notifcation</fullName>
        <actions>
            <name>LDS_Opp_RelatedAcc_OutBound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Related_Account__c.Record_type_of_Oppt__c</field>
            <operation>equals</operation>
            <value>Commercial Traditional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Related_Account__c.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Integration User</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
