<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approval_Status_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Sent_for_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Approval Status Sent for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>PartnerAcc_Outbound_message_to_CAMS</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://pubtst.virtualservices.mohawkind.com/SOAT305/PUB.MHK.SalesForce.AccountListener.svc</endpointUrl>
        <fields>Id</fields>
        <fields>Invoicing_Account_Name__c</fields>
        <fields>Invoicing_Account__c</fields>
        <fields>Name</fields>
        <fields>Non_Invoicing_Account__c</fields>
        <fields>Role__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>PartnerAcc Outbound message to CAMS</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Partner Account to CAMS</fullName>
        <actions>
            <name>PartnerAcc_Outbound_message_to_CAMS</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partner_Account__c.Invoicing_Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This is used to send Partner Accounts related to Residential non-invoicing Account to CAMS
US 40086</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
