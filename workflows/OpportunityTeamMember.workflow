<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>LDS_Opportunity_Team_OutBound</fullName>
        <apiVersion>41.0</apiVersion>
        <endpointUrl>https://mohawk-sf.liquidanalytics.com/salesforce/OpportunityTeam</endpointUrl>
        <fields>CAMS_Employee_ID__c</fields>
        <fields>CAMS_Employee_ID_of_Logged_in_User__c</fields>
        <fields>Comment_Migration__c</fields>
        <fields>Id</fields>
        <fields>OpportunityId</fields>
        <fields>SAP_Opportunity_Id__c</fields>
        <fields>TeamMemberRole</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>LDS_Opportunity_Team_OutBound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>LDS_Opportunity_TeamMember_Update_Notifcation</fullName>
        <actions>
            <name>LDS_Opportunity_Team_OutBound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityTeamMember.UserId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityTeamMember.Record_type_of_Oppt__c</field>
            <operation>equals</operation>
            <value>Commercial Traditional</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityTeamMember.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Integration User</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
