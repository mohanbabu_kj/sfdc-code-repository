<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Outbound_Message_to_Hybris_SDI</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Samples Inventory Performance Outbound Message</description>
        <endpointUrl>https://pubtst.virtualservices.mohawkind.com/SOAT305/PUB.MHK.SalesForce.AccountListener.svc</endpointUrl>
        <fields>Account__c</fields>
        <fields>Architech_Folder__c</fields>
        <fields>Brand__c</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>Customer_Group_Number__c</fields>
        <fields>Customer_Group__c</fields>
        <fields>Dealer__c</fields>
        <fields>Display_Code_CSV__c</fields>
        <fields>Display_External_ID__c</fields>
        <fields>Display_Performance__c</fields>
        <fields>Display_Version__c</fields>
        <fields>External_Id__c</fields>
        <fields>Fit__c</fields>
        <fields>Global_account_Number__c</fields>
        <fields>Id</fields>
        <fields>Inventory__c</fields>
        <fields>IsDeleted</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>LastReferencedDate</fields>
        <fields>LastViewedDate</fields>
        <fields>MTD_Sales__c</fields>
        <fields>Name</fields>
        <fields>Order__c</fields>
        <fields>OwnerId</fields>
        <fields>Placement_Status__c</fields>
        <fields>Product_Type__c</fields>
        <fields>R12_Sales__c</fields>
        <fields>Recommended_Selling_Price__c</fields>
        <fields>RecordTypeId</fields>
        <fields>Sample_Inventory_Category__c</fields>
        <fields>SystemModstamp</fields>
        <fields>YTD_Sales__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>iuser@mohawkind.com</integrationUser>
        <name>Outbound Message to Hybris SDI</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>SDI Salesforce to Hybris Communication</fullName>
        <actions>
            <name>Outbound_Message_to_Hybris_SDI</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When the record is created and the status got changed the WF will fire and send the field values to hybris.</description>
        <formula>OR(ISNEW(),ISCHANGED(Placement_Status__c) &amp;&amp; NOT(ISNULL(TEXT(Placement_Status__c)))) &amp;&amp; $User.Full_Name__c != &apos;Integration User&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
